
# Comenzi utile#

```
    php artisan migrate:refresh --seed
```

## Ma, 20-Oct-2015

*Time = 2h*

```
    - Fix ReCaptcha Keys
    - Fix Migrations
    - Fix DB to enable registration
```

## Vi, 23-Oct-2015

*Time = 2h*

```
    - Fix employer thumbs
```

## Sa, 24-Oct-2015

*Time = 3h*

```
    - Update logo & profile image
    - Update table cols 4 thumbs
    - Styling Upload files
    - Show admin user role
    - js inProgress error
```

## Du, 25-Oct-2015

*Time = 0.5h*

```
    - Send email when admin approves an account
```


## Lu, 26-Oct-2015

*Time = 2.5h*

```
    - Resend confirmation email
    - Redirect to profile.edit after registration or after login if profile is not complete
```

## Ma, 27-Oct-2015

*Time = 1h*

```
    Correct email config, auth redirect, trans(account_activated_login)
    Profile not visible when user is not approved
```

## Mi, 28-Oct-2015

*Time = 1.5h*

```
    Redirect to dashboard after login if user is admin or superadmin
    Fix Admin Employer Edit link
    Fix Admin Actor Edit link
    Fix Admin User Needapprove Edit link
    Fix EyeColour & HairColour actor relation 
    Fix Sidebar Hardcoded Links
    Fix admin user listing table
```


## Vi, 30-Oct-2015

*Time = 2h*

```
    Fix slider home link
    Create admin links in slider, banda, navbar
    Fix actors and employers in admin panel
    Fixed $this->ethnicities->names(); in admin\ActorController:53
    Created new employer page
    Created getRolesByYourRole (only superadmin can change admin roles)
```

## Lu, 02-Nov-2015

*Time = 2h*

```
    Functionalitate actorland
```

## Joi, 05-Nov-2015

*Time = 6.5h*

```
    fix actors_actor_type_id_foreign
    employer_type_id column in employers table and employers w/ employer_types relation
    Traits' seeders now make less db insert calls
    added timestamps to actor_types table 
    Fix Table Migrations, seeds and added ModelFactory (now we have specific users)
    Fix admin hardcoded image links (using asset())
    Fix need approve view return 
    Fix admin hardcoded links to admin
    Fix Site Top Menu
```        

## Samb, 07-Nov-2015

*Time = 6h*

```
    Fix showActor view and Profile controller show method
    Fix sex in seeds seed more entries
    RCC loads romanian faker now
    User parameter in sendConfirmationEmail
    Move recaptcha keys in config
    Seed random_unapproved_employers
    Country default seed: romania
    Show Employers that need approvment in admin
    Correct Actors & Employers get and show them by user id, not by actor or employer id
    Seed unapproved actors
    Show actors that need approval in admin
    Added view website button in admin
    Added view profile on website button in admin
    Added crud view
    Reorganised admin view tables
    Show actors' and employers' contact data only to registered users
    Redirect to edit profile only if logged and it's your profile
```

## Joi, 12-Nov-2015

*Time = 7.5h*

```
    Functionality for New User & Edit User
    Moved methods form Rcc\Http\Controllers\Admin\UserController to Rcc\Repositories\RccRepository
    Moved requests form Rcc\Http\Controllers\Admin\UserController to namespace Rcc\Http\Requests\AddUserRequest
    Readable characteristics in admin
    Cleaned up admin\user\index.blade.php
    Added calendar in admin
    Show actor type & employer type correctly in admin/user/edit
    Send message to user after admin creates account for him
    Use Aliases.
    Use Facades for Repositories
```

## Luni, 16-Nov-2015

*Time = 1.5h*

```
    Cleaned and reconfigured routes.php si page controller
    Contact Request
```

## Mi, 18-Nov-2015

*Time = 3.5h*

```
    Cleaned show & edit method from ProfileController
    Made middleware for Approved user, CheckAdminer, Owner
    Fix owner middleware on ajax request
    Cleaned ajaxUpdateUser & ajaxShowCharasteristics
```

## Vi, 20-Nov-2015

*Time = 5h*

```
    Created Ambassadors and Lunars Migrations & Seeds
    Show Actorul Lunii
    Ambassadors Model, View, Controller
    Ambassadors CRUD
    Lunar CRUD
```

## Sa, 21-Nov-2015

*Time = 5h*

```
    Styling Ambassadors
    Styling Actorul lunii
    Created employer middleware
    Styling Job's General information 
    Translate info job form
    Seed productions & production types
    Edit gets stuff from repository
```

## Du, 22-Nov-2015

*Time = 4h*

```
    Ajax functionality for Adding Jobs/Castings
    Jobs/Castings seeding
```    

## Lu, 23-Nov-2015

*Time = 2h*

```
    View job
```    

## Ma, 24-Nov-2015

*Time = 1h*

```
    Banda galbena homepage cu video ambasadori
    Scos pg parteneri
    Inchidere ceilalti ambasadori cand deschid unul
    PG castinguri poate fi vazute de oricine
```    

## Mi, 25-Nov-2015

*Time = 1h*

```
    Debug RCC - decalare boxuri actori, hovere, fix approved middleware, butoane ambasadori
```    

## Sa, 28-Nov-2015

*Time = 4.5h*

```
    Actorland filters
    Recaptcha in romanian
```    

## Du, 28-Nov-2015

*Time = 1h*

```
    Bigger home videos
    Profile Back Button
    Castings Admin Approve
```    

## Du, 28-Nov-2015

*Time = 7.5h*

```
    Fix adding and listing jobs
    Edit, delete, activate & deactivate jobs
    JobOwner middleware
    Show my castings, apply & unapply to casting
    Show, approve, reject, shortlist applicants
    Pages & language_page migrations, seeds, views
    Admin Pages CRUD
```    

## Sa, 05-Dec-2015

*Time = 4.5h*

```
    Modificari Laura
    Acting range si alte modificari Laura
```    

## Sa, 12-Dec-2015

*Time = 5h*

```
    Modificari Andreea 2
```    

## Ma, 15-Dec-2015

*Time = 3h*

```
    Profile PDF download
```    

## Joi, 17-Dec-2015

*Time = 2.5h*

```
    Messages
```    

## Lu, 28-Dec-2015

*Time = 2.5h*

```
    Admin Actor
```    

## Ma, 19-Ian-2016

*Time = 5h*

```
    Responsive
```    

## Ma, 30-Ian-2016

*Time = 7h*

```
    Email notifications
```    

## Ma, 31-Ian-2016

*Time = 3h*

```
    Email aplicantii noi la casting
```    

## Mi, 03-Feb-2016

*Time = 4h*

```
    Probleme RCC mail
```

## Joi, 04-Feb-2016

*Time = 10h*

```
    Update-uri din mail
    Terms
    New applicants email
    Applicants command
```    

## Lu, 08-Feb-2016

*Time = 2.5h*

```
    Mici update-uri Proiecte (din mail)
    Media in admin
```   

## Mi, 10-Feb-2016

*Time = 3.5h*

```
    Media in admin Working
    Castings to expire
    Expired castings
```    

## Lu, 15-Feb-2016

*Time = 1h*

```
    Mail dupa createa actorului, nu a userului
    Traduceri mailuri
```    

Total: 126h

## Ma, 23-Feb-2016

*Time = 2.5h*

```
    Delete account and selection rules
```    

## Mi, 25-Feb-2016

*Time = 2h*

```
    Adjusting emails part 1
```    

## Sa, 27-Feb-2016

*Time = 3.5h*

```
    Adjusting emails part 2
```    

## Du, 29-Feb-2016

*Time = 3.5h*

```
    Adjusting emails part 3
    Lunars both name and id
    Lunar email
```   

## Mi, 02-Mar-2016

*Time = 3h*

```
    Characters migration, seed, view
```    

## Joi, 03-Mar-2016

*Time = 1.5h*

```
    Characters validation and edit view
```    

## Vi, 05-Mar-2016

*Time = 3h*

```
    Characters CRUD and delete project fix
    Character title
    Sliders CRUD
```    

## Du, 06-Mar-2016

*Time = 4.5h*

```
    Character_user migration & seed, Characters functionality, applying, mails, remove headline from jobs
    Show slider administrabil
    Fix slider migration
```    

## Du, 09-Apr-2016

*Time = 7h*

```
    Modificari mail
```  

## Lu, 18-Apr-2016

*Time = 8h*

```
    Username can only be changed twice
    1/4 of translation functionality & 404
```    

## Ma, 19-Apr-2016

*Time = 4h*

```
    2/4 of translation functionality
```     

## Mi, 20-Apr-2016

*Time = 6h*

```
    3/4 of translation functionality
```    

## Vi, 22-Apr-2016

*Time = 10h*

```
    4/4 of translation functionality
```     

## Sa, 23-Apr-2016 && Du, 24-Apr-2016 

*Time = 8h*

```
    5/4 of translation functionality
```    

## Lu, 25-Apr-2016

*Time = 11h*

```
    Corrected updates part 1 && 2
```    

## Tu, 26-Apr-2016

*Time = 6h*

```
    Corrected company updates
    Corrected castings updates
```    

TODO: 

    pagina aplicanti filtrata
    language administrabil
    Recuperare parola not working /password/email
    
    WIKI: php artisan ide-helper:generate
