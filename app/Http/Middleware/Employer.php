<?php

namespace Rcc\Http\Middleware;

use Closure;
use Rep;

class Employer {

    /**
     * Handle an incoming request.
     * @param $request
     * @param Closure $next
     * @return \Illuminate\Http\RedirectResponse
     */
    public function handle($request, Closure $next)
    {
        if ( ! (Rep::isEmployer() || Rep::isAdminer()) ) {
            return redirect()->home();
        }
        return $next($request);
    }

}
