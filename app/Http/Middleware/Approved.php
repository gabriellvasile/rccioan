<?php

namespace Rcc\Http\Middleware;

use Closure;
use Rep;
use Auth;
use User;

class Approved {

    /**
     * Handle an incoming request.
     * @param $request
     * @param Closure $next
     * @return \Illuminate\Http\RedirectResponse
     */
    public function handle($request, Closure $next)
    {
        $user = User::whereUsername($request->route()->username)->firstOrFail();
        if (!Auth::check() && !$user->approved) {
            return redirect()->route('home')->with('warning_message', trans('messages.user_not_approved'));
        }

        if (Auth::check() && !Rep::isAdminer()) {
            if (Auth::user()->id == $user->id) {
                if (!$user->complete) {
                    return redirect(route('profile.edit', $user->username))->with('warning_message', trans('messages.complete_profile'));
                }
                if (!$user->approved) {
                    return redirect()->route('home')->with('warning_message', trans('messages.your_profile_not_approved'));
                }
            }
            if (!$user->approved) {
                return redirect()->route('home')->with('warning_message', trans('messages.user_not_approved'));
            }
        }

        return $next($request);
    }

}
