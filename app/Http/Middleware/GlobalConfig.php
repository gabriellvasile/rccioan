<?php

namespace Rcc\Http\Middleware;

use Closure;
use Auth;
use Rcc\Models\EmployerType;
use Rcc\Models\Role;
use Rcc\Models\User;
use Page;
use Rep;

class GlobalConfig {

	/**
	 * Create a new controller instance.
	 * @param EmployerType $employerTypes
	 */
	public function __construct(EmployerType $employerTypes) {
		$this->employer_types = $employerTypes;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		if (Auth::check()) {
			$data['logged'] = Auth::user();
			$data['logged']->load('role');

			$data['member_since'] = date('M' , $data['logged']->created_at->timestamp) . '. ' . $data['logged']->created_at->year;

	        $talentRoleId = Role::where('name', 'talent')->first()->id;
	        $employerRoleId = Role::where('name', 'employer')->first()->id;

	        if ($data['logged']->role_id == $talentRoleId)
	            $data['userType'] = 'actor';
	        else if($data['logged']->role_id == $employerRoleId)
	            $data['userType'] = 'employer';

	        if ( in_array($data['logged']->role_id, array(1, 2)) )
	            $data['admin'] = true;
	        else
	        	$data['admin'] = false;

	        $data['talent_role_id'] = Role::where('name', 'talent')->first()->id;
	        $data['employer_role_id'] = Role::where('name', 'employer')->first()->id;
		}

		$data['employer_types'] = $this->employer_types->lists('name', 'id');
		$data['logo'] = route('home') . '/images/logo.png';
		view()->share($data);
		return $next($request);
	}

}
