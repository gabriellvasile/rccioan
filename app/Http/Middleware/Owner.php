<?php

namespace Rcc\Http\Middleware;

use Closure;
use Rep;

class Owner {

	/**
	 * Handle an incoming request.
	 * @param $request
	 * @param Closure $next
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function handle($request, Closure $next)
	{
		if ( ! Rep::isOwner($request->route()->username) && ! Rep::isAdminer() ) {
			return redirect()->back()->with('warning_message', trans('messages.edit_your_own'));
		}
		return $next($request);
	}

}
