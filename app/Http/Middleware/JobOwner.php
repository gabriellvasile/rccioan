<?php

namespace Rcc\Http\Middleware;

use Closure;
use Rep;

class JobOwner {

	/**
	 * Handle an incoming request.
	 * @param $request
	 * @param Closure $next
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function handle($request, Closure $next)
	{
		if ( ! Rep::isJobOwner($request->route()->id) && ! Rep::isAdminer()) {
			return redirect()->route('jobs.list')->with('warning_message', trans('messages.edit_your_own_job'));
		}
		return $next($request);
	}

}
