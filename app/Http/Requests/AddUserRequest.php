<?php

namespace Rcc\Http\Requests;

class AddUserRequest extends Request
{

    protected $rules = [
        'fname' => 'required|max:255|alpha_dash',
        'lname' => 'required|max:255|alpha_dash',
        'sex' => 'required|in:M,F',
        'email' => 'required|email|max:255|unique:users,email',
        'country_id' => 'required|exists:countries,id',
        'birth_date' => 'required',
        'language_id' => 'required|exists:languages,id',
    ];

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = $this->rules;
        if ($this->input('role_id') == 3) {
            $rules['actor_type_id'] = 'required|exists:actor_types,id';
        } elseif ($this->input('role_id') == 4) {
            $rules['employer_type_id'] = 'required|exists:employer_types,id';
        }

        return $rules;
    }

    public function messages(){
        return [
            'actor_type_id.required' => trans('messages.actor_type_required'),
            'actor_type_id.exists' => trans('messages.actor_type_must_exist'),
            'language_id.required' => trans('messages.language_required'),
            'language_id.exists' => trans('messages.language_must_exist'),
        ];
    }
}
