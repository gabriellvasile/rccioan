<?php

namespace Rcc\Http\Requests;

use Auth;

class UpdateCharacteristicsRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'actor.hair_colour_id' => 'required|exists:hair_colours,id',
            'actor.eye_colour_id'  => 'required|exists:eye_colours,id',
            'actor.height'         => 'required|numeric',
            'actor.weight'         => 'required|numeric',
            'actor.ethnicity_id'   => 'required|exists:ethnicities,id',
            'actor.hair_length'    => 'required|in:S,M,L',
            'actor.accent'         => 'required',
            'actor.voice'          => 'required|in:0,1',
        ];
    }
}