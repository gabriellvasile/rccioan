<?php

namespace Rcc\Http\Requests;

class AddPageRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		return [
			'title'        => 'required|max:255',
			'slug'         => 'required',
		];
	}
}
