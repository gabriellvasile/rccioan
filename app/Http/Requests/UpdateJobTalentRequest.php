<?php

namespace Rcc\Http\Requests;

class UpdateJobTalentRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];

        foreach($this->request->get('character') as $key => $val)
        {
            $rules['character.'.$key.'.title']                 = 'required';
            $rules['character.'.$key.'.range_id']              = 'required|exists:ranges,id';
            $rules['character.'.$key.'.sex']                   = 'required|in:M,F';
            $rules['character.'.$key.'.hair_colour_id']        = 'required|exists:hair_colours,id';
            $rules['character.'.$key.'.eye_colour_id']         = 'required|exists:eye_colours,id';
            $rules['character.'.$key.'.from_height']           = 'required|numeric';
            $rules['character.'.$key.'.to_height']             = 'required|numeric';
        }

        return $rules;
    }

    public function messages()
    {
        $messages = [];
        foreach($this->request->get('character') as $key => $val)
        {
            $messages['character.'.$key.'.title.required'] = trans('validation.custom_required', ['character' => $key, 'field' => trans('messages.title')]);

            $messages['character.'.$key.'.range_id.required'] = trans('validation.custom_required', ['character' => $key, 'field' => trans('messages.range')]);
            $messages['character.'.$key.'.range_id.exists'] = trans('validation.custom_required', ['character' => $key, 'field' => trans('messages.range')]);

            $messages['character.'.$key.'.sex.required'] = trans('validation.custom_required', ['character' => $key, 'field' => trans('messages.sex')]);
            $messages['character.'.$key.'.sex.in'] = trans('validation.custom_required', ['character' => $key, 'field' => trans('messages.sex')]);

            $messages['character.'.$key.'.hair_colour_id.required'] = trans('validation.custom_required', ['character' => $key, 'field' => trans('messages.hair_colour')]);
            $messages['character.'.$key.'.hair_colour_id.exists'] = trans('validation.custom_required', ['character' => $key, 'field' => trans('messages.hair_colour')]);

            $messages['character.'.$key.'.eye_colour_id.required'] = trans('validation.custom_required', ['character' => $key, 'field' => trans('messages.eye_colour')]);
            $messages['character.'.$key.'.eye_colour_id.exists'] = trans('validation.custom_required', ['character' => $key, 'field' => trans('messages.eye_colour')]);

            $messages['character.'.$key.'.from_height.required'] = trans('validation.custom_required', ['character' => $key, 'field' => trans('messages.heightfrom')]);
            $messages['character.'.$key.'.from_height.numeric'] = trans('validation.custom_numeric', ['character' => $key, 'field' => trans('messages.heightfrom')]);

            $messages['character.'.$key.'.to_height.required'] = trans('validation.custom_required', ['character' => $key, 'field' => trans('messages.heightto')]);
            $messages['character.'.$key.'.to_height.numeric'] = trans('validation.custom_numeric', ['character' => $key, 'field' => trans('messages.heightto')]);
        }
        return $messages;
    }
}
