<?php

namespace Rcc\Http\Requests;

class AddJobRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		return [
			'production_name'    => 'required|min:3',
			'location'           => 'required|min:3',
			'production_type_id' => 'required|exists:production_types,id',
			'actor_type_id'      => 'required|exists:actor_types,id',

			'from_date_shooting' => 'required|date_format:Y-m-d',
			'to_date_shooting'   => 'required|date_format:Y-m-d',
			'from_date_casting'  => 'required|date_format:Y-m-d',
			'to_date_casting'    => 'required|date_format:Y-m-d',

			'student_project'    => 'required',
			'payment'            => 'required',

			'university'         => 'required_if:student_project,1',
			'year'               => 'required_if:student_project,1',
			'tutor_name'         => 'required_if:student_project,1',
			'department'         => 'required_if:student_project,1',

			'payment'            => 'required',
			'email'              => 'required|email',
			'telephone'          => 'required|mobile',
			'notifying_method'   => 'required|in:0,1',
			'short_description'  => 'required',
		];
	}
}
