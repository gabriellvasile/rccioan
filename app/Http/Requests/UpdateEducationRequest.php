<?php

namespace Rcc\Http\Requests;

use Auth;
use App;

class UpdateEducationRequest extends Request {

    protected $rules = [
        'actor.profesional_studies' => 'required|in:0,1',
    ];

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {

        if ($this->input('locale')) {
            App::setLocale($this->input('locale'));
        }

        $rules = $this->rules;
        if (1 == $this->input('actor.profesional_studies')) {
            $rules['actor.faculty'] = 'required|in:0,1';
            $rules['actor.speciality_studies'] = 'required|in:0,1';

            if (1 == $this->input('actor.faculty')) {
                $rules['actor.faculty_institution'] = 'required|not_in:0';
                $rules['actor.faculty_domain'] = 'required|not_in:0';
                $rules['actor.faculty_year'] = 'required|year';

                if ($this->input('actor.faculty_institution') == trans('messages.other_inst')) {
                    $rules['actor.faculty_other'] = 'required';
                }
            }

            if (1 == $this->input('actor.speciality_studies')) {
                $rules['actor.speciality_institution'] = 'required';
                $rules['actor.speciality_year'] = 'required|year';
            }
        }

        return $rules;
    }

    public function messages() {
        return [
            'actor.profesional_studies.required'    => trans('messages.select_profesional_studies'),
            'actor.faculty.required'                => trans('messages.select_faculty'),
            'actor.speciality_studies.required'     => trans('messages.select_speciality_studies'),
            'actor.faculty_institution.required'    => trans('messages.select_faculty_institution'),
            'actor.faculty_domain.required'         => trans('messages.select_faculty_domain'),
            'actor.faculty_year.required'           => trans('messages.select_faculty_year'),
            'actor.faculty_other.required'          => trans('messages.select_faculty_other'),
            'actor.speciality_institution.required' => trans('messages.select_speciality_institution'),
            'actor.speciality_year.required'        => trans('messages.select_speciality_year'),

            'actor.profesional_studies.in'          => trans('messages.select_profesional_studies'),
            'actor.faculty.in'                      => trans('messages.select_faculty'),
            'actor.speciality_studies.in'           => trans('messages.select_speciality_studies'),
            'actor.faculty_institution.not_in'      => trans('messages.select_faculty_institution'),
            'actor.faculty_domain.not_in'           => trans('messages.select_faculty_domain'),
            'actor.faculty_year.year'               => trans('messages.select_faculty_year'),
            'actor.speciality_institution.in'       => trans('messages.select_speciality_institution'),
            'actor.speciality_year.year'            => trans('messages.select_speciality_year'),
        ];
    }
}