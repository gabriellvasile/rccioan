<?php

namespace Rcc\Http\Requests;

use App;

class UpdateCompanyRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {

        if ($this->input('locale')) {
            App::setLocale($this->input('locale'));
        }

        return [
            'employer.company'   => 'required|max:50',
            'employer.address'   => 'required|max:255',
            'employer.post_code' => 'required|digits:6',
            'city'               => 'required|min:1|max:97',
            'email'              => 'required|email|max:255',
            'country_id'         => 'required|exists:countries,id',
            'password'           => 'confirmed|min:6',
            'mobile'             => 'required|mobile',
        ];
    }
}
