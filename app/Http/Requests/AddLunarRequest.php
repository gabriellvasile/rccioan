<?php

namespace Rcc\Http\Requests;

class AddLunarRequest extends Request {

	protected $rules = [
		// 'actor_id'    => 'required|exists:actors,id',
		'description' => 'required|max:1000',
		'video'       => 'required',
		'month'       => 'required|date_format:m|not_in:0',
		'year'        => 'required|date_format:Y|not_in:0',
	];

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		$rules = $this->rules;
		if (0 == $this->input('actor_id')) {
			$rules['name'] = 'required|min:3|max:255';
		} else {
			$rules['name'] = 'string';
		}

		return $rules;
	}
}
