<?php

namespace Rcc\Http\Requests;

class MessageRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		return [
			'id'      => 'required|exists:users,id',
			'subject' => 'required',
			'message' => 'required',
		];
	}

	/**
	 * Get the error messages for the defined validation rules.
	 *
	 * @return array
	 */
	public function messages() {
		return [
			'id.required' => trans('messages.recipient_required'),
			'id.exists'   => trans('messages.recipient_must_exist'),
		];
	}
}
