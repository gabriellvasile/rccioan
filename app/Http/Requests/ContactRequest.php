<?php

namespace Rcc\Http\Requests;

class ContactRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'last_name' 			=> 'required|max:255|alpha_dash',
            'first_name' 			=> 'required|max:255|alpha_dash',
            'email' 				=> 'required|email|max:255',
            'subject' 				=> 'required|min:1|max:97',
            'message'				=> 'required',
            'g-recaptcha-response' 	=> 'required|recaptcha'
        ];
    }
}
