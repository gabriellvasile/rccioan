<?php

namespace Rcc\Http\Requests;

class UpdateUserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fname'                             => 'required|max:255|alpha_dash',
            'lname'                             => 'required|max:255|alpha_dash',
            'sex'                               => 'required|in:M,F',
            'city'                              => 'required|min:1|max:97',
            'birth_date'                        => 'required|date',
            'email'                             => 'required|email|max:255',
            'password'                          => 'confirmed|min:6',
            'country_id'                        => 'required|exists:countries,id',
            'language_id'                       => 'required|exists:languages,id',
            'mobile'                            => 'required|mobile',
        ];
    }
}
