<?php

namespace Rcc\Http\Controllers;

use Illuminate\Http\Request;
use Ambassador;
use Slider;

class PageController extends Controller
{
    /**
     * @param string $slug
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Request $request, $slug = '') {
        if (!$slug || $slug == 'home') {
            $slug = '/';
            $data = $this->getPageFromSlug($slug);
        }
        else {
            $data = $this->getPageFromSlug(trim($request->getRequestUri(), '/'));
        }

        if ($slug === '/' || $slug === 'en') {
            // We're on homepage
            $data['sliders'] = Slider::orderBy('sort')->get();
            $data['ambassadors'] = Ambassador::limit(6)->get();

            return view('home', $data);
        }
        else {
            // We're on a page
            return view('page', $data);
        }
    }
}
