<?php

namespace Rcc\Http\Controllers;

use Auth;
use User;
use Mails;
use Job;
use App;

class CastingsController extends Controller
{

	/**
	 * @param User $user
	 *
	 * @internal param Job $job
	 */
	public function __construct(User $user) {
		$this->user = $user;
	}

	public function myCastings($locale = 'ro')
	{
		$this->setLocale($locale);
		$data = $this->setLocalisedLinks();
		$data['change_languages']['ro'] = route('casts.mine', 'ro');
		$data['change_languages']['en'] = route('casts.mine', 'en');

		$data['jobs'] = Job::select(
			[
				'jobs.id as job_id',
				'jobs.production_name',
				'characters.id as character_id',
				'characters.title as character_title',
				'character_user.date_applied as date_applied',
				'character_user.is_declined_employer as is_declined_employer',
				'character_user.is_invite as is_invite',
				'character_user.is_seen as is_seen',
				'character_user.is_chosen as is_chosen',
				'character_user.is_shortlist as is_shortlist',
				'production_types.name as production_type',
			]
			)->join('characters', function ($join) {
				$join->on('characters.job_id', '=', 'jobs.id');
			})
			->join('production_types', function ($join) {
				$join->on('production_types.id', '=', 'jobs.production_type_id');
			})
			->join('character_user', function ($join) {
				$join->on('character_user.character_id', '=', 'characters.id')
					 ->where('character_user.user_id', '=', Auth::id());
			})
			->orderBy('job_id')
			->get();

		return view('castings', $data);
	}


	// Casting actions

	public function approve($character_id, $user_id)
	{
		$character = $this->user->findOrFail($user_id)->characters()->find($character_id);
		$casting = $character->pivot;

		$casting->is_chosen = 1;
		$casting->is_declined_employer = 0;
		$casting->save();

		Mails::sendApplicantApproved($this->user->findOrFail($user_id), $character);

		return back();
	}

	public function reject($character_id, $user_id)
	{
		$character = $this->user->findOrFail($user_id)->characters()->find($character_id);
		$casting = $character->pivot;

		$casting->is_chosen = 0;
		$casting->is_declined_employer = 1;
		$casting->save();

		Mails::sendApplicantUnApproved($this->user->findOrFail($user_id), $character);

		return back();
	}

	public function shortlist($job_id, $user_id)
	{
		$character = $this->user->findOrFail($user_id)->characters()->find($job_id);
		$casting = $character->pivot;

		if($casting->is_shortlist == 1)
			$casting->is_shortlist = 0;
		else
			$casting->is_shortlist = 1;

		$casting->save();

		Mails::sendShortlistStatus($this->user->findOrFail($user_id), $character, $casting->is_shortlist);

		return back();
	}

	public function destroy($id, $locale = 'ro')
	{
		$this->setLocale($locale);
		Auth::user()->characters()->detach([$id]);

		return redirect()->route('casts.mine', $locale)->with('success_message', trans('messages.unapply'));
	}
}
