<?php

namespace Rcc\Http\Controllers;

use Request;
use Auth;
use User;
use Role;

use Rep;
use UpdateUserRequest;
use UpdateCompanyRequest;
use UpdateCharacteristicsRequest;
use UpdateEducationRequest;
use PDF;
use Mails;

class ProfileController extends Controller {

    /**
     * @param User $user
     */
    public function __construct(User $user) {
        $this->user     = $user;
    }

    /**
     * @param $username
     * @param string $locale
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View $data
     */
    public function show($username, $locale = 'ro') {
        $this->setLocale($locale);
        $data = $this->setLocalisedLinks();
        $data['change_languages'] = $this->getChangeLanguages($username);
        $data['user']     = Rep::getUserWithRelationsByUsername($username);
        $data['profileType'] = Rep::getProfileType($data['user']->role_id);
        $data += Rep::getUserSelects();

        return view('profile.show' . ucfirst($data['profileType']), $data);
    }

    public function preview($username, $locale = 'ro') {
        $this->setLocale($locale);
        $data = $this->setLocalisedLinks();
        $data['change_languages'] = $this->getChangeLanguages($username);
        $data['user']     = Rep::getUserWithRelationsByUsername($username);
        $data['updates'] = json_decode($data['user']->updates);
        if (!$data['updates']) {
            return redirect()->route('profile.show', [$username, $locale]);
        }
        $data['profileType'] = Rep::getProfileType($data['user']->role_id);
        $data += Rep::getUserSelects();
        return view('profile.show' . ucfirst($data['profileType']), $data);
    }

    /**
     * PDF Download
     * @param $username
     *
     * @param string $locale
     * @return \Illuminate\Http\RedirectResponse
     */
    public function pdf($username, $locale = 'ro') {
        $this->setLocale($locale);
        if (Rep::isAdminer() || Rep::isEmployer()) {
            $data['user']     = Rep::getUserWithRelations($username);
            $data['userType'] = Rep::getProfileType($data['user']->role_id);

            if  ($data['userType'] == 'actor' && $data['user']->approved == 1 && $data['user']->active == 1 && $data['user']->complete == 1) {
                $data += Rep::getUserSelects();
                $pdf = PDF::loadView('profile.pdf', $data);
                // return $pdf->stream(); // view in the browser
                return $pdf->download('RCC-Profil-' . $data['user']->actor->actortype->name . '-'. $data['user']->fname . '-' . $data['user']->lname . '.pdf');
            }
        }
        return redirect()->route('home')->with('warning_message', trans('messages.you_are_not_allowed_pdf'));
    }

    /**
     * @param $username
     * @param string $locale
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     *
     */
    public function edit($username, $locale = 'ro') {
        $this->setLocale($locale);
        $data = $this->setLocalisedLinks();
        $data['change_languages'] = $this->getChangeLanguages($username);
        $data['user'] = Rep::getUserWithRelations($username);
        $data += Rep::getUserSelects();

        return view('profile.edit', $data);
    }

    /**
     * Ajax Update User
     *
     * @param $username
     *
     * @param string $locale
     * @param UpdateUserRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajaxUpdateUser($username, $locale = 'ro', UpdateUserRequest $request) {
        $this->setLocale($locale);
        Rep::setUserUpdates($request, $username);
        return response()->json(compact($request));
    }

    /**
     * Ajax Update Company
     *
     * @param $username
     * @param string $locale
     * @param UpdateCompanyRequest|UpdateCompanyRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @internal param string $locale
     */
    public function ajaxUpdateCompany($username, $locale = 'ro', UpdateCompanyRequest $request) {
        $this->setLocale($locale);
        Rep::setCompanyUpdates($request, $username);
        return response()->json(compact($request));
    }

    public function ajaxShowCharasteristics($username, $locale = 'ro') {
        $this->setLocale($locale);
        $data['user'] = Rep::getUserWithRelations($username);
        $data += Rep::getUserSelects();

        return response()->view('profile._characteristics', $data);
    }

    /**
     * Ajax Update User Characteristics
     *
     * @param $username
     * @param string $locale
     * @param UpdateCharacteristicsRequest $request
     * @return json response
     *
     */
    public function ajaxUpdateCharacteristics($username, $locale = 'ro', UpdateCharacteristicsRequest $request) {
        $this->setLocale($locale);
        Rep::setActorUpdates($request, $username);
        return response()->json(compact($request));
    }

    public function ajaxShowEducation($username, $locale = 'ro') {
        $this->setLocale($locale);
        $data['user'] = Rep::getUserWithRelations($username);
        $data['userType'] = Rep::getProfileType($data['user']->role_id);
        $data['user_productions'] = $data['user']->productions->lists('id')->toArray();
        $data += Rep::getUserSelects();

        return response()->view('profile._educationtab', $data);
    }

    /**
     * Ajax Update User Education
     *
     * @param $username
     * @param string $locale
     * @param UpdateEducationRequest $request
     * @return json response
     *
     */
    public function ajaxUpdateEducation($username, $locale = 'ro', UpdateEducationRequest $request) {
        $this->setLocale($locale);
        Rep::setActorUpdates($request, $username);
        return response()->json(compact($request));
    }

    public function ajaxShowFiles($username, $locale = 'ro') {
        $user = $this->user->whereUsername($username)->firstOrFail();

        $this->setLocale($locale);

        $talentRoleId   = Role::where('name', 'talent')->first()->id;
        $employerRoleId = Role::where('name', 'employer')->first()->id;

        if ($user->role_id == $talentRoleId) {
            $data['user']     = $user->load('actor');
            $data['userType'] = 'actor';
        }
        else if ($user->role_id == $employerRoleId) {
            $data['user']     = $user->load('employer');
            $data['userType'] = 'employer';
        }

        $data['user'] = $user;

        return response()->view('profile._files', $data);
    }

    /**
     * Ajax Update User Files
     *
     * @param $username
     * @param string $locale
     * @return json response
     *
     */
    public function ajaxUpdateFiles($username, $locale = 'ro') {
        $this->setLocale($locale);

        $user           = $this->user->whereUsername($username)->firstOrFail();

        $data = [];
        if ((isset($_FILES['images']) && ! empty($_FILES['images'])) ||
            (isset($_FILES['profile']) && ! empty($_FILES['profile'])) ||
            (isset($_FILES['logo']) && ! empty($_FILES['logo']))
        ) {
            $images = $filenames = [];
            if (isset($_FILES['images'])) {
                $images = $_FILES['images'];
            }
            if (isset($_FILES['profile'])) {
                $images += $_FILES['profile'];
            }
            if (isset($_FILES['logo'])) {
                $images += $_FILES['logo'];
            }

            // get file names
            $filenames = $images['name'];
            $paths     = [];
            // loop and process files
            $len = count($filenames);
            if (is_array($filenames)) {
                for ($i = 0; $i < $len; $i++) {
                    $ext    = explode('.', basename($filenames[ $i ]));
                    $target = md5(uniqid()) . "." . array_pop($ext);
                    if (move_uploaded_file($images['tmp_name'][ $i ], public_path('uploads/') . $target)) {
                        $data['success'] = true;
                        $paths[]         = $target;
                    }
                    else {
                        $data['success'] = false;
                        break;
                    }
                }
            }
            else {
                $ext    = explode('.', basename($filenames));
                $target = md5(uniqid()) . "." . array_pop($ext);
                if (move_uploaded_file($images['tmp_name'], public_path('uploads/') . $target)) {
                    $data['success'] = true;
                    $paths[]         = $target;
                }
                else {
                    $data['success'] = false;
                }
            }

            if ($data['success'] === true) {
                $data['paths'] = $paths;
                if (isset($_POST['profile']) && $_POST['profile']) {
                    $this->user->saveImages($user->id, $paths, 'profile');
                }
                elseif (isset($_POST['logo']) && $_POST['logo']) {
                    $this->user->saveImages($user->id, $paths, 'logo');
                }
                else {
                    $this->user->saveImages($user->id, $paths);
                }
            }
            elseif ($data['success'] === false) {
                $data['messages']['images'] = 'Error while uploading images. Contact the system administrator';
                // delete any uploaded files
                foreach ($paths as $file) {
                    unlink($file);
                }
            }
            else {
                $data['messages']['images'] = 'No files were processed.';
            }
        }

        if ( ! isset($data['success']) || $data['success'] == true) {

            Rep::unCompleteProfile($username);

            $talentRoleId   = Role::where('name', 'talent')->first()->id;
            $employerRoleId = Role::where('name', 'employer')->first()->id;

            if ( ! in_array(Auth::user()->role_id, [$talentRoleId, $employerRoleId])) {
                $admin = true;
            }
            else {
                $admin = false;
            }

            if ( ! $admin) {
                $json = ['success' => true];
            }
            else {
                $json = ['_redirect' => true, '_url' => route('profile.show', [$username, $locale])];
            }

            return response()->json($json);
        }
        else {
            return response()->json($data);
        }

    }

    public function ajaxNewProject($username, $locale = 'ro') {
        $this->setLocale($locale);
        $data['key'] = Request::input('key');

        $user = $this->user->whereUsername($username)->firstOrFail();

        $talentRoleId   = Role::where('name', 'talent')->first()->id;
        $employerRoleId = Role::where('name', 'employer')->first()->id;

        if ($user->role_id == $talentRoleId) {
            $userType = 'actor';
        }
        else if ($user->role_id == $employerRoleId) {
            $userType = 'employer';
        }

        $data['userType'] = $userType;

        return response()->view('profile._newproject', $data);
    }

    public function ajaxNewWorkshop($username, $locale = 'ro') {
        $this->setLocale($locale);
        $data['key'] = Request::input('key');

        $user = $this->user->whereUsername($username)->firstOrFail();

        $talentRoleId   = Role::where('name', 'talent')->first()->id;
        $employerRoleId = Role::where('name', 'employer')->first()->id;

        if ($user->role_id == $talentRoleId) {
            $userType = 'actor';
        }
        else if ($user->role_id == $employerRoleId) {
            $userType = 'employer';
        }

        $data['userType'] = $userType;

        return response()->view('profile._newworkshop', $data);
    }

    public function ajaxNewVimeo($username, $locale = 'ro') {

        $user = $this->user->whereUsername($username)->firstOrFail();
        $this->setLocale($locale);
        $data = [];
        if (Request::input('type') == 'vimeos') {
            $data['link'] = substr(Request::input('link'), strpos(Request::input('link'), 'https://vimeo.com/') +
                                                           strlen('https://vimeo.com/'));
            $data['id']   = $this->user->addVimeo($user->id, $data['link']);

            return response()->view('profile._newvimeo', $data);
        }
        elseif (Request::input('type') == 'youtubes') {
            $data['link'] =
                substr(Request::input('link'), strpos(Request::input('link'), 'https://www.youtube.com/watch?v=') +
                                               strlen('https://www.youtube.com/watch?v='));
            $data['id']   = $this->user->addYotube($user->id, $data['link']);

            return response()->view('profile._newyoutube', $data);
        }
        elseif (Request::input('type') == 'soundclouds') {
            $link = substr(Request::input('link'), strpos(Request::input('link'), 'api.soundcloud.com/tracks/') +
                                                   strlen('api.soundcloud.com/tracks/'));
            $link = substr($link, 0, strpos($link, '&'));

            $data['link'] = $link;
            $data['id']   = $this->user->addSoundcloud($user->id, $data['link']);

            return response()->view('profile._newsoundcloud', $data);
        }

    }

    public function removeFromProfile($username) {

        $user = $this->user->whereUsername($username)->firstOrFail();
        $data['key']  = Request::input('key');
        $data['type'] = Request::input('type');

        if ('project' == Request::input('type')) {
            $this->user->deleteProject($user->id, Request::input('key'));
        }
        elseif ('workshop' == Request::input('type')) {
            $this->user->deleteWorkshop($user->id, Request::input('key'));
        }
        elseif ('files' == Request::input('type')) {
            $this->user->deleteImage($user->id, Request::input('key'));
        }
        elseif ('vimeos' == Request::input('type')) {
            $this->user->deleteVimeo($user->id, Request::input('key'));
        }
        elseif ('youtubes' == Request::input('type')) {
            $this->user->deleteYoutube($user->id, Request::input('key'));
        }
        elseif ('soundclouds' == Request::input('type')) {
            $this->user->deleteSoundcloud($user->id, Request::input('key'));
        }

        return response()->json($data);
    }

    public function ajaxComplete($username) {

        $user           = $this->user->whereUsername($username)->firstOrFail();

        Mails::accountModifyRequest($user);
        Mails::accountUpdated($user);

        return response()->json([]);
    }

    public function initiateDelete($username, $locale = 'ro') {
        $this->setLocale($locale);

        $user = $this->user->whereUsername($username)->firstOrFail();
        $user->code = str_random(60);
        $user->save();

        Mails::sendConfirmDeleteEmail($user);
        return redirect()->back()->with('success_message', trans('messages.sent_confirm_delete_account'));
    }

    public function confirmDelete($code) {
        if ($code != null) {
            if ($this->user->where('code', $code)->delete()) {
                return redirect()->route('home')->with('success_message', trans('messages.account_deleted'));
            }
            else {
                return redirect()->route('home')->with('warning_message', trans('messages.link_expired'));
            }
        }
        return redirect()->route('home')->with('error_message', trans('messages.error_delete'));
    }
}

