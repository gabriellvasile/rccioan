<?php

namespace Rcc\Http\Controllers\Auth;

use Auth;
use User;
use Employer;
use Country;
use Validator;
use Rcc\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Mails;
use Page;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    protected $redirectAfterLogout;
    protected $redirectTo;

    /**
     * Create a new authentication controller instance.
     * @param Guard $auth
     * @param Request $request
     * @internal param string $locale
     */
    public function __construct(Guard $auth, Request $request)
    {
        $this->middleware('guest', ['except' => 'getLogout']);
        $this->auth = $auth;

        $this->setLocale($request->getLocale());

        if ($this->locale == 'ro') {
            $this->setRedirectTo(route('home'));
            $this->setRedirectAfterLogout(route('home'));
        }
        else {
            $this->setRedirectTo(route('page.show', 'en'));
            $this->setRedirectAfterLogout(route('page.show', 'en'));
        }
    }

    /**
     * Show form for registration
     *
     * @param string $locale
     * @return \Illuminate\View\View
     * @internal param Request $request
     */
    public function getRegister($locale = 'ro')
    {
        $this->setLocale($locale);
        $data = $this->setLocalisedLinks();
        $data['change_languages'] = $this->getChangeLanguages();

        if ($locale = 'ro') {
            $slug = 'terms';
        }
        else {
            $slug = 'termeni';
        }
        $data['terms'] = Page::join('language_page', 'pages.id', '=', 'language_page.page_id')
            ->where('slug', 'LIKE', '%' . $slug .'%')
            ->first()->slug;
        return view('auth.register', $data);
    }


    /**
     * Registration function
     *
     * @param string $locale
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postRegister($locale = 'ro', Request $request)
    {
        $this->setLocale($locale);
        $data = $this->setLocalisedLinks();

        $data += $request->all();
        $validator = $this->validator($data);

        if ( $validator->fails() )
            $this->throwValidationException($request, $validator);

        $user = new User;
        $employer = new Employer;

        $user->country_id = Country::where('name', 'Romania')->pluck('id'); //Romania id
        $user->role_id = 4; // This is employer
        $user->language_id = 1; // Ro
        $user->fname = $data['fname'];
        $user->lname = $data['lname'];
        $user->email = $data['email'];
        $user->active = 0;
        $user->code = str_random(60);
        $user->password = bcrypt($data['password']);

        if( !empty($data['employer_type']) )
            $employer->employer_type_id = $data['employer_type'];

        $user->save(); // First save

        $user->username = $data['fname'] . '.' . $data['lname'] . $user->id;
        $user->save(); // Second save

        $employer->user_id = $user->id;
        $employer->save();

        $this->confirmEmailAddress($user, $locale);
       
        return redirect()->route('login', $locale)->with('success_message', trans('auth.account_created') . ' ' . trans('auth.account_sent_activation_email'));
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'fname' => 'required|max:255',
            'lname' => 'required|max:255',
            'employer_type' => 'required|exists:employer_types,id',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
            'g-recaptcha-response' => 'required|recaptcha',
            'terms' => 'accepted'
        ]);
    }

    /**
     * Send confirmation email after register.
     *
     * @param $user
     *
     * @param string $locale
     * @return \Illuminate\Http\RedirectResponse
     */
    public function confirmEmailAddress($user, $locale = 'ro') {
        $this->setLocale($locale);
        Mails::sendConfirmationEmail($user);
        if (!$user->active) {
            return redirect()->route('login', $locale)->with('success_message', trans('auth.account_sent_activation_email'));
        }
    }

    /**
     * If confirmed, save user, login user and redirect it to profile
     * @param  string $code registration code
     * @param string $locale
     * @param Request $request
     * @return redirect to profile if successful, or else to home
     */
    public function getConfirm($code, Request $request, $locale = 'ro') {
        $this->setLocale($locale);

        if ($this->locale == 'ro') {
            $this->setRedirectTo(route('home'));
            $this->setRedirectAfterLogout(route('home'));
        }
        else {
            $this->setRedirectTo(route('page.show', 'en'));
            $this->setRedirectAfterLogout(route('page.show', 'en'));
        }

        if ($code != null) {
            $user = User::where('code', $code);

            if ($user->count()) {
                $user = $user->first();
                $user->active = 1;
                $user->code = null;

                if ( $user->save() ) {

                    $request->only('email', 'password');

                    return redirect()->route('login', $locale)->with('success_message', trans('auth.account_activated_login'));
                }

            }
            else {
                return redirect($this->redirectAfterLogout)->with('warning_message', trans('messages.link_expired'));
            }
        }
        return redirect($this->redirectAfterLogout)->with('error_message', trans('messages.error_activated'));
    }

    /**
     * Show the application login form.
     *
     * @param string $locale
     * @return \Illuminate\Http\Response
     * @internal param Request $request
     */
    public function getLogin($locale = 'ro')
    {
        $this->setLocale($locale);
        $data = $this->setLocalisedLinks();
        $data['change_languages'] = $this->getChangeLanguages();
        return view('auth.login', $data);
    }

    /**
     * Handle a login request to the application.
     *
     * @param string $locale
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function postLogin($locale = 'ro', Request $request) {

        $this->setLocale($locale);

        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
            'g-recaptcha-response' => 'required|recaptcha',
        ]);

        $credentials = $request->only('email', 'password');

        if ($this->auth->attempt($credentials, $request->has('remember')))
        {
            // Is admin or superadmin
            if( in_array($this->auth->user()->role_id,[1,2]) ) {
                return redirect()->route('admin.dashboard', $this->locale);
            }

            $id = $this->auth->user()->id;
            if( !$this->auth->user()->active ) {
                $this->auth->logout();
                return redirect()->route('login', $this->locale)
                     ->withErrors([
                         'error_message' => trans('auth.account_not_activated') .
                                             ' <a href="' . route('confirmEmailAddress', [$id, $locale]) . '">' . trans('auth.account_resend') . '</a>'
                     ]);
            }
            if (!$this->auth->user()->complete) {
                return redirect()->route('profile.edit', [$this->auth->user()->username, $locale]);
            }

            return redirect()->intended($this->redirectPath())->with('success_message', trans('auth.login_successful'));
        }

        return redirect()->route('login', $this->locale)
                    ->withInput($request->only('email', 'remember'))
                    ->withErrors([
                        'email' => $this->getFailedLoginMessage()
                    ]);
    }

    /**
     * Log the user out of the application.
     *
     * @param string $locale
     * @return \Illuminate\Http\Response
     */
    public function getLogout($locale = 'ro')
    {
        $this->setLocale($locale);
        Auth::logout();

        if ($this->locale == 'ro') {
            $this->setRedirectTo(route('home'));
            $this->setRedirectAfterLogout(route('home'));
        }
        else {
            $this->setRedirectTo(route('page.show', 'en'));
            $this->setRedirectAfterLogout(route('page.show', 'en'));
        }

        return redirect(property_exists($this, 'redirectAfterLogout') ? $this->redirectAfterLogout : '/')->with('success_message', trans('auth.logout_successful'));
    }

    /**
     * @return string
     */
    public function getRedirectAfterLogout() {
        return $this->redirectAfterLogout;
    }

    /**
     * @param string $redirectAfterLogout
     */
    public function setRedirectAfterLogout($redirectAfterLogout) {
        $this->redirectAfterLogout = $redirectAfterLogout;
    }

    /**
     * @return string
     */
    public function getRedirectTo() {
        return $this->redirectTo;
    }

    /**
     * @param string $redirectTo
     */
    public function setRedirectTo($redirectTo) {
        $this->redirectTo = $redirectTo;
    }

}
