<?php

namespace Rcc\Http\Controllers\Admin;

use Rcc\Http\Requests;
use Rcc\Http\Controllers\Controller;

use User;
use AddUserRequest;
use EditUserRequest;
use Rep;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $data['admin_languages'] = $this->data['admin_languages'];
        $data['users'] = User::all();
        return view('admin.user.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $data = Rep::getUserSelects();
        $data['admin_languages'] = $this->data['admin_languages'];
        return view('admin.user.create', $data);
    }

    /**
     * Show the form for editing the specified resource.
     * @param $username
     * @return \Illuminate\View\View
     * @internal param $id
     */
    public function edit($username)
    {
        $data['admin_languages'] = $this->getAdminLanguages($username);
        $data['user'] = Rep::getUserWithRelations($username);
        $data += Rep::getUserSelects();

        return view('admin.user.edit', $data);
    }

    /**
     * Display a listing of non-approved users.
     * @return \Illuminate\View\View
     */
    public function needApprove()
    {
        $data['admin_languages'] = $this->data['admin_languages'];
        $data['users'] = Rep::getUnApprovedUsers();
        return view('admin.user.index', $data);
    }

    /**
     * Store a newly created resource in storage.
     * @param AddUserRequest $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function store(AddUserRequest $request)
    {
        $user = Rep::createUserFromRequest($request);

        return redirect()->route('admin.user.edit', [$user->username, 'locale' => $this->locale])->with('success_message', trans('messages.user_created'));
    }

    /**
     * Update the specified resource in storage.
     * @param $id
     * @param EditUserRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id, EditUserRequest $request)
    {
        $user = Rep::updateUserFromRequest($id, $request);
        return back()->with('success_message', trans('messages.user_updated'));
    }

    /**
     * Remove the specified resource from storage.
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        User::destroy($id);
        return back()->with('success_message', trans('messages.user_deleted'));
    }

    /**
     * Approve user
     * @param $username
     * @return \Illuminate\Http\RedirectResponse
     */
    public function approve($username)
    {
        Rep::approveUser($username);
        return back()->with('success_message', trans('messages.approved'));
    }

    /**
     * Uncomplete user
     * @param $username
     * @return \Illuminate\Http\RedirectResponse
     */
    public function uncomplete($username)
    {
        Rep::unCompleteProfile($username, $notify = true);
        return back()->with('success_message', trans('messages.unapproved'));
    }

}