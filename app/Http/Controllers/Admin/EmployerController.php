<?php

namespace Rcc\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Rcc\Http\Controllers\Controller;
use Employer;
use Rep;
use User;

class EmployerController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @param User $user
	 * @param Employer $employer
	 * @param Request $request
	 * @internal param Language $language
	 */
	public function __construct(User $user, Employer $employer, Request $request) {
		parent::__construct($request);

		$this->employer = $employer;
		$this->user = $user;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index() {
		$data['admin_languages'] = $this->data['admin_languages'];
		$data['employers'] = $this->employer->with('user')->get();

		return view('admin.employer.index', $data);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $username
	 *
	 * @return Response
	 */
	public function edit($username) {
		$data['admin_languages'] = $this->getAdminLanguages($username);
		$data['employer'] = $this->employer->EmployerWithUser($username);
		$data += Rep::getUserSelects();

		return view('admin.employer.edit', $data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int $username
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function update($username, Request $request) {

		$employer = $this->employer->EmployerWithUser($username);

		// Update user
		$employer->user()->update($request->input('user'));
		if(0 === $employer->user->password) {
			Rep::setPassAndSendMail($employer->user);
		}

		// Update employer
		$employer->update($request->except(['_method', '_token', 'user']));

		return back()->with('success_message', trans('messages.user_updated'));
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id) {
		$this->user->destroy($id);

		return back()->with('success_message', trans('messages.user_deleted'));
	}

	/**
	 * Display a listing of non-approved employers.
	 *
	 * @return Response
	 */
	public function needApprove() {
		$data['admin_languages'] = $this->data['admin_languages'];
		$data['employers'] = $this->employer->UnapprovedEmployers();

		return view('admin.employer.index', $data);
	}

}
