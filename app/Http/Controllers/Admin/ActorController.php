<?php

namespace Rcc\Http\Controllers\Admin;


use Illuminate\Http\Request;
use Rcc\Http\Controllers\Controller;

use Actor;
use User;
use Rep;

class ActorController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @param User $user
     * @param Actor $actor
     * @param Request $request
     */
    public function __construct(User $user, Actor $actor, Request $request) {
        parent::__construct($request);
        $this->user = $user;
        $this->actor = $actor;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $data['admin_languages'] = $this->data['admin_languages'];
        $data['actors'] = $this->actor->with(['ethnicity', 'eyecolour', 'haircolour', 'user'])->get();
        return view('admin.actor.index', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $username
     * @return Response
     * @internal param int $id
     */
    public function edit($username)
    {
        $data['actor'] = $this->actor->ActorWithUser($username);
        $data['admin_languages'] = $this->getAdminLanguages($username);
        $data += Rep::getUserSelects();
        $data['current_licences'] = $data['actor']->user->licences->lists('id')->toArray();
        $data['user_productions'] = $data['actor']->user->productions->lists('id')->toArray();
        $data['current_ranges'] = $data['actor']->user->ranges->lists('id')->toArray();
        $data['user'] = $data['actor']->user;

        return view('admin.actor.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $username
     * @param Request $request
     * @return Response
     */
    public function update($username, Request $request)
    {
        // Get Model
        $actor = $this->actor->ActorWithUser($username);

        // Update user
        $actor->user()->update($request->input('user'));
        if(!$actor->user->password) {
            Rep::setPassAndSendMail($actor->user);
        }

        // Update actor
        $actor->update($request->except(['_method', '_token', 'user', 'ethnicity', 'eyecolour', 'haircolour']));

        // Update actor foreigh keys
        $actor->ethnicity_id = $request->input('ethnicity')['id'];
        $actor->eye_colour_id = $request->input('eyecolour')['id'];
        $actor->hair_colour_id = $request->input('haircolour')['id'];
        $actor->save();

        $actor->user->licences()->sync((null !== $request->input('licences') ? $request->input('licences') : []));
        $actor->user->productions()->sync((null !== $request->input('productions') ? $request->input('productions') : []));
        $actor->user->ranges()->sync((null !== $request->input('ranges') ? $request->input('ranges') : []));

        Rep::updateProjects($actor->user, (null !== $request->input('projects') ? $request->input('projects') : []));

        return back()->with('success_message', trans('messages.user_updated'));
    }

    public function ajaxNewProject(Request $request) {
        $data['key'] = $request->input('key');

        return response()->view('admin.actor._newproject', $data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->user->destroy($id);
        return back()->with('success_message', trans('messages.user_deleted'));
    }

    /**
     * Display a listing of non-approved employers.
     *
     * @return Response
     */
    public function needApprove()
    {
        $data['admin_languages'] = $this->data['admin_languages'];
        $data['actors'] = $this->actor->UnapprovedActors();
        return view('admin.actor.index', $data);
    }

}
