<?php

namespace Rcc\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Rcc\Http\Requests;
use Rcc\Http\Controllers\Controller;

use Page;
use AddPageRequest;
use Rep;

class PageController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\View\View
	 */
	public function index() {
		$data['admin_languages'] = $this->data['admin_languages'];
		$data['pages'] = Page::
			select(
				[
					'language_page.*',
					'pages.*',
					'languages.code'
				]
			)
			->join('language_page', 'pages.id', '=', 'language_page.page_id')
			->join('languages', 'languages.id', '=', 'language_page.language_id' )
			->get();

		return view('admin.page.index', $data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\View\View
	 */
	public function create() {
		$data['admin_languages'] = $this->data['admin_languages'];
		return view('admin.page.create', $data);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param $id
	 *
	 * @param Request $request
	 * @return \Illuminate\View\View
	 */
	public function edit($id, Request $request) {
		$data['admin_languages'] = $this->getAdminLanguages(['id' => $id, 'lang' => $request->get('lang'), 'locale' => $request->get('locale')]);
		$data['page'] = Rep::findPageById($id, $request->get('lang'));

		return view('admin.page.edit', $data);
	}

	/**
	 * @param AddPageRequest $request
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function store(AddPageRequest $request) {
		$page = Rep::createPageFromRequest($request);

		return redirect()->route('admin.page.edit', [$page->id, 'locale' => $this->locale])->with('success_message', trans('messages.page_created'));
	}

	/**
	 * @param                $id
	 * @param AddPageRequest $request
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function update($id, AddPageRequest $request) {
		$this->setLocale($request->get('lang'));
		$page = Rep::updatePageFromRequest($id, $request, $this->locale);

		return back()->with('success_message', trans('messages.page_updated'));
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param $id
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function destroy($id) {
		Page::destroy($id);

		return back()->with('success_message', trans('messages.page_deleted'));
	}

	/**
	 * @param $id
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function removeFromPage($id, Request $request) {
		$data['key']  = $request->input('key');
		$data['type'] = $request->input('type');

		$page = Page::findOrFail($id);
		$page->image = '';
		$page->save();

		return response()->json($data);
	}

}