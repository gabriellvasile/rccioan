<?php

namespace Rcc\Http\Controllers\Admin;

use Rcc\Http\Requests;
use Rcc\Http\Controllers\Controller;

use Ambassador;
use AddAmbassadorRequest;
use Rep;

class AmbassadorController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $data['admin_languages'] = $this->data['admin_languages'];
        $data['ambassadors'] = Ambassador::all();
        return view('admin.ambassador.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $data['admin_languages'] = $this->data['admin_languages'];
        return view('admin.ambassador.create', $data);
    }

    /**
     * Show the form for editing the specified resource.
     * @param $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $data['ambassador'] = Ambassador::find($id);
        $data['admin_languages'] = $this->getAdminLanguages($id);
        return view('admin.ambassador.edit', $data);
    }

    /**
     * @param AddAmbassadorRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(AddAmbassadorRequest $request)
    {
        $ambassador = Rep::createAmbassadorFromRequest($request);
        return redirect()->route('admin.ambassador.edit', [$ambassador->id, 'locale' => $this->locale])->with('success_message', trans('messages.ambassador_created'));
    }

    /**
     * @param $id
     * @param AddAmbassadorRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id, AddAmbassadorRequest $request)
    {
        $ambassador = Rep::updateAmbassadorFromRequest($id, $request);
        return back()->with('success_message', trans('messages.ambassador_updated'));
    }

    /**
     * Remove the specified resource from storage.
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        Ambassador::destroy($id);
        return back()->with('success_message', trans('messages.ambassador_deleted'));
    }

}