<?php

namespace Rcc\Http\Controllers\Admin;

use Rcc\Http\Requests;
use Rcc\Http\Controllers\Controller;

use Slider;
use AddSliderRequest;
use Rep;

class SliderController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index() {
        $data['admin_languages'] = $this->data['admin_languages'];
        $data['sliders'] = Slider::all();

        return view('admin.slider.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create() {
        $data['admin_languages'] = $this->data['admin_languages'];
        return view('admin.slider.create', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id) {
        $data['slider'] = Slider::findOrFail($id);
        $data['admin_languages'] = $this->getAdminLanguages($id);

        return view('admin.slider.edit', $data);
    }

    /**
     * @param AddSliderRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(AddSliderRequest $request) {
        $slider = Rep::createSliderFromRequest($request);

        return redirect()->route('admin.sliders.edit', [$slider->id, 'locale' => $this->locale])->with('success_message', trans('messages.slider_created'));
    }

    /**
     * @param                $id
     * @param AddSliderRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id, AddSliderRequest $request) {
        $slider = Rep::updateSliderFromRequest($id, $request);

        return back()->with('success_message', trans('messages.slider_updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id) {
        Rep::tryDelete(Slider::findOrFail($id)->image);
        Slider::destroy($id);

        return back()->with('success_message', trans('messages.slider_deleted'));
    }

}