<?php

namespace Rcc\Http\Controllers\Admin;

use Rcc\Http\Controllers\Controller;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param string $locale
     * @return Response
     */
    public function index($locale = 'ro')
    {
        $this->setLocale($locale);
        return view('admin.home', $this->data);
    }
}
