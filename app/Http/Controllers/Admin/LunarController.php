<?php

namespace Rcc\Http\Controllers\Admin;

use Rcc\Http\Requests;
use Rcc\Http\Controllers\Controller;

use Lunar;
use AddLunarRequest;
use Actor;
use Rep;

class LunarController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $data['admin_languages'] = $this->data['admin_languages'];
        $data['lunars'] = Lunar::all();
        return view('admin.lunar.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $data = Rep::getLunarData();
        $data['admin_languages'] = $this->data['admin_languages'];
        return view('admin.lunar.create', $data);
    }

    /**
     * Show the form for editing the specified resource.
     * @param $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $data['admin_languages'] = $this->getAdminLanguages($id);
        $data['lunar'] = Lunar::with('actor.user')->find($id);
        $data += Rep::getLunarData();
        return view('admin.lunar.edit', $data);
    }

    /**
     * @param AddLunarRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(AddLunarRequest $request)
    {
        $lunar = Rep::createLunarFromRequest($request);
        return redirect()->route('admin.lunar.edit', [$lunar->id, 'locale' => $this->locale])->with('success_message', trans('messages.lunar_created'));
    }

    /**
     * @param $id
     * @param AddLunarRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id, AddLunarRequest $request)
    {
        $lunar = Rep::updateLunarFromRequest($id, $request);
        return back()->with('success_message', trans('messages.lunar_updated'));
    }

    /**
     * Remove the specified resource from storage.
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        Lunar::destroy($id);
        return back()->with('success_message', trans('messages.lunar_deleted'));
    }

}