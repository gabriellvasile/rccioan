<?php

namespace Rcc\Http\Controllers\Admin;

use Rcc\Http\Requests;
use Rcc\Http\Controllers\Controller;

use Job;
use AddJobRequest;
use Rep;

class JobController extends Controller
{
	/**
	 * Display a listing of the resource.
	 * @return \Illuminate\View\View
	 */
	public function index()
	{
		$data['admin_languages'] = $this->data['admin_languages'];
		$data['jobs'] = Job::all();
		return view('admin.job.index', $data);
	}

	/**
	 * Show the form for editing the specified resource.
	 * @param $id
	 * @return \Illuminate\View\View
	 */
	public function edit($id)
	{
		$data['admin_languages'] = $this->getAdminLanguages($id);
		$data['job'] = Job::find($id);
		return view('admin.job.edit', $data);
	}

	/**
	 * @param AddJobRequest $request
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function store(AddJobRequest $request)
	{
		$job = Rep::createJobFromRequest($request);
		return back()->with('success_message', trans('messages.job_created'));
	}

	/**
	 * @param $id
	 * @param AddJobRequest $request
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function update($id, AddJobRequest $request)
	{
		$job = Rep::updateJobFromRequest($id, $request);
		return back()->with('success_message', trans('messages.job_updated'));
	}

	/**
	 * Remove the specified resource from storage.
	 * @param $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function destroy($id)
	{
		Job::destroy($id);
		return back()->with('success_message', trans('messages.job_deleted'));
	}

	/**
	 * Display a listing of non-approved jobs.
	 * @return \Illuminate\View\View
	 */
	public function needApprove()
	{
		$data['admin_languages'] = $this->data['admin_languages'];
		$data['jobs'] = Rep::getUnApprovedJobs();
		return view('admin.job.index', $data);
	}

	/**
	 * Approve job
	 * @param $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function approve($id)
	{
		Rep::approveJob($id);
		return back()->with('success_message', trans('messages.job_approved'));
	}

	/**
	 * Uncomplete job
	 * @param $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function uncomplete($id)
	{
		Rep::unCompleteJob($id, $notify = true);
		return back()->with('success_message', trans('messages.unapproved'));
	}

}