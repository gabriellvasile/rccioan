<?php

namespace Rcc\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App;
use Page;

abstract class Controller extends BaseController
{
    use DispatchesJobs, ValidatesRequests;

    protected $locale = 'ro';
    protected $data;

    public function __construct(Request $request) {
        $this->setLocale($request->input('locale'));
        $this->getAdminLanguages();
    }

    public function getPageFromSlug($slug) {
        $this->data['page'] = Page::select(['language_page.*', 'pages.*', 'languages.code'])
            ->join('language_page', 'pages.id', '=', 'language_page.page_id')
            ->join('languages', 'language_page.language_id', '=', 'languages.id')
            ->groupBy('pages.id')
            ->where('slug', $slug)
            ->first();

        if ($slug != '/' && !$this->data['page']) {
            abort(404);
        }

        $this->setLocale($this->data['page']->code);
        $this->setLocalisedLinks();

        return $this->data;
    }

    public function setLocale($locale) {
        $this->locale = $locale;
        App::setLocale($this->locale);
    }

    public function setLocalisedLinks() {
        $this->data['base_url'] = $this->locale == 'ro' ? route('page.show', '/') : route('page.show', 'en');
        $this->data['topmenu'] = Page::headerPages($this->locale);
        $this->data['pages'] = Page::footerPages($this->locale);


        if (isset($this->data['page'])) {
            if ($this->data['page']->parent) {
                $this->data['children'] = Page::getLocalisedChildren($this->data['page']->parent->id, $this->locale);
                $this->data['filter'] = Page::getLocalisedFilterPage($this->data['page']->parent->id, $this->locale);
            }
            $this->data['change_languages'] = [];
            foreach($this->data['page']->languages as $lang) {
                $this->data['change_languages'][$lang->code] = route('page.show', $lang->pivot->slug);
            }
        }

        return $this->data;
    }

    public function getChangeLanguages($parameter = null) {
        $langs = config('app.languages');
        foreach($langs as $lang) {
            if ($parameter) {
                $this->data['change_languages'][$lang] = route($this->getRouter()->getCurrentRoute()->getName(), [$parameter, $lang]);
            }
            else {
                $this->data['change_languages'][$lang] = route($this->getRouter()->getCurrentRoute()->getName(), $lang);
            }
        }
        return $this->data['change_languages'];
    }

    public function getAdminLanguages($parameters = []) {
        $langs = config('app.languages');
        $parameters = is_array($parameters) ? $parameters : [$parameters];

        foreach($langs as $lang) {
            if ($parameters) {
                $parameters['locale'] = $lang;
                $this->data['admin_languages'][$lang] = route($this->getRouter()->getCurrentRoute()->getName(), $parameters);
            }
            else {
                $this->data['admin_languages'][$lang] = route($this->getRouter()->getCurrentRoute()->getName(), ['locale' => $lang]);
            }
        }
        return $this->data['admin_languages'];
    }
}
