<?php

namespace Rcc\Http\Controllers;

use ContactRequest;
use Illuminate\Http\Request;
use Mails;

class ContactController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function index(Request $request) {
		$data = $this->getPageFromSlug(trim($request->getRequestUri(), '/'));
		$data['change_languages']['ro'] = route('contact');
		$data['change_languages']['en'] = route('contact-us');
		return view('contact', $data);
	}

	public function post(ContactRequest $request) {
		Mails::contact($request);
		return response()->json(compact($request));
	}
}
