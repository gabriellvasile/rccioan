<?php

namespace Rcc\Http\Controllers;

use ActorType;
use Actor;
use Illuminate\Http\Request;
use Lunar;
use Rep;

class ActorLandController extends Controller {
    public function index(Request $request)
    {
        $data = $this->getPageFromSlug($request->route()->getUri());

        $data['actor_types'] = ActorType::all();
        $data['current_lunar'] = Lunar::orderBy('date', 'desc')->first();
        return view('actorland' , $data);
    }

    public function listActors($slug, Request $request) {
        $actor_type = trans('messages.actor_type_' . $slug);
        $data = $this->getPageFromSlug(trim($request->getRequestUri(), '/'));

        $data['actor_type_active'] = ActorType::where('slug', '=', $actor_type)->with('actors.user')->first();
        $data['actors'] = $this->getActiveActorsWithType($actor_type);
        $data += $this->getActors();
        $request->flashOnly('');
        return view('profile.list', $data);
    }

    public function filterActors(Request $request) {
        $data = $this->getPageFromSlug(trim($request->getPathInfo(), '/'));
        $data['actors'] = $this->filter($request)->get();
        $data['actor_type_active'] = ActorType::where('slug', '=', 'actori')->with('actors.user')->first();
        $data += $this->getActors();
        $request->flash();
        return view('profile.list', $data);
    }

    public function listLunars(Request $request) {
        $data = $this->getPageFromSlug(trim($request->getRequestUri(), '/'));
        $data['actor_types'] = ActorType::all();
        $data['lunars'] = Lunar::desc()->with('actor.user')->get();
        return view('profile.lunars', $data);
    }

    private function getActors() {
        $data['actor_types'] = ActorType::all();
        $data += Rep::getUserSelects();
        $data += Rep::getJobSelects();
        return $data;
    }

    private function filter(Request $request) {

        $query =  Actor::join('users', 'users.id', '=', 'actors.user_id');

        if ($request->input('range_id')) {
            $query->join('range_user', function ($join) use ($request) {
                $join->on('range_user.user_id', '=', 'users.id')
                     ->where('range_user.range_id', '=', $request->input('range_id'));
            })
            ->select('actors.*', 'users.country_id', 'range_user.range_id', 'range_user.user_id');
        }

        $query->where('users.active', 1);
        $query->where('users.approved', 1);
        $query->where('users.complete', 1);

        if ($request->input('hair_length')) {
            $query->where('hair_length', $request->input('hair_length'));
        }
        if ($request->input('hair_colour_id')) {
            $query->where('hair_colour_id', $request->input('hair_colour_id'));
        }
        if ($request->input('eye_colour_id')) {
            $query->where('eye_colour_id', $request->input('eye_colour_id'));
        }
        if ($request->input('ethnicity_id')) {
            $query->where('ethnicity_id', $request->input('ethnicity_id'));
        }
        if ($request->input('role_abroad')) {
            $query->where('role_abroad', 1);
        }
        if ($request->input('faculty_student')) {
            $query->where('faculty_student', 1);
        }
        if ($request->input('sex')) {
            $query->where('sex', $request->input('sex'));
        }
        return $query;
    }

    public function getActiveActorsWithType($slug){
        $query =  Actor::join('users', 'users.id', '=', 'actors.user_id');
        $query->join('actor_types', function ($join) use ($slug) {
            $join->on('actor_types.id', '=', 'actors.actor_type_id')
                 ->where('actor_types.slug', '=', $slug);
        });
        $query->where('users.active', 1);
        $query->where('users.approved', 1);
        $query->where('users.complete', 1);
        return $query->get();
    }
}
