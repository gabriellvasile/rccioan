<?php

namespace Rcc\Http\Controllers;

use Auth;

use Illuminate\Http\Request;
use Rcc\Models\User;
use Rcc\Models\Message;

use Input;
use MessageRequest;
use Mails;

class MessagesController extends Controller {

	/**
	 * @param Message $message
	 * @param User    $user
	 */
	public function __construct(Message $message, User $user) {
		$this->message = $message;
		$this->user    = $user;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @param string $locale
	 * @return Response
	 */
	public function index($locale = 'ro') {
		$this->setLocale($locale);
		$user                             = Auth::user();
		$data = $this->setLocalisedLinks();

		$data['change_languages'] = $this->getChangeLanguages();
		$data['messages']['by_sender']    = $this->message->allBySender($user->id);
		$data['messages']['by_recipient'] = $this->message->allByRecipient($user->id);

		$data['unread'] = 0;
		foreach ($data['messages']['by_recipient'] as $message) {
			if (!$message->is_read)
				$data['unread']++;
		}

		$data['messages_users'] = [
			'by_sender'    => [],
			'by_recipient' => [],
		];

		foreach ($data['messages'] as $messageBox => $messages) {
			foreach ($messages as $key => $message) {
				$data['messages_users'][ $messageBox ][ $key ]['sender']    = $this->user->find($message->sender_id);
				$data['messages_users'][ $messageBox ][ $key ]['recipient'] = $this->user->find($message->recipient_id);
			}
		}

		return view('messages', $data);
	}

	public function newmessage($id = null, $locale = 'ro') {

		$this->setLocale($locale);
		$data = $this->setLocalisedLinks();
		$data['change_languages'] = $this->getChangeLanguages($id);

		if (isset($id) && $id) {
			$data['user']     = $this->user->find($id);
			$data['fullName'] = $data['user']->fname . ' ' . $data['user']->lname;
		}

		return view('messagesnew', $data);
	}

	public function searchRecipient(Request $request) {
		$users = $this->user->whereIn('role_id', [3, 4])
		                    ->where(function ($query) use ($request) {
			                    $query->where('fname', 'like', '%' . $request->input('text') . '%')
			                          ->orWhere('lname', 'like', '%' . $request->input('text') . '%')
			                          ->orWhere('email', 'like', '%' . $request->input('text') . '%');
		                    })->get()->toArray();

		return response()->json($users);
	}

	public function sendMessage(MessageRequest $request) {
		$data                 = $request->except(['_token', 'id']);
		$data['sender_id']    = Auth::id();
		$data['recipient_id'] = $request->input('id');
		$data['is_important'] = ( ! empty($data['important'])) ? 1 : 0;

		// Send mesage
		$message = $this->message->create($data);

		Mails::sendInboxEmail($message);

		return redirect()->route('messages.list')->with('success_message', trans('messages.message_sent'));
	}

	public function reply($id,$locale = 'ro') {

		$this->setLocale($locale);
		$data = $this->setLocalisedLinks();
		$data['message']            = $this->message->find($id);
		$data['message']['subject'] = 'Re: ' . $data['message']['subject'];
		$data['reply']              =
			"<br><br><br><br><br><br> <em>On " . $data['message']->created_at . ", " . $data['message']->sender->fname .
			" " . $data['message']->sender->lname . " " .
			"wrote:</em> <br><br> ----------------- <br><br>";

		return view('messagesreply', $data);
	}

	public function doreply($id) {
		$message = $this->message->find($id);

		$data                 = Input::all();
		$data['sender_id']    = Auth::id();
		$data['recipient_id'] = (int) $message->sender_id;

		// Send mesage
		$this->message->create($data);

		Mails::sendInboxEmail($message);

		return redirect('messages')->with('success_message', trans('messages.message_sent'));
	}

	public function forward($id) {
		$data['message']            = $this->message->find($id);
		$data['message']['subject'] = 'Fwd: ' . $data['message']['subject'];

		return view('messagesforward', $data);
	}

	public function markread(Request $request){
		$m = $this->message->findOrFail($request->input('id'));
		if (!$m->is_read) {
			$m->is_read = 1;
			$m->save();
			return response()->json(['modify' => 1]);
		}
		return response()->json(['modify' => 0]);
	}

	public function doforward($id) {
		$message = $this->message->find($id);

		$data           = Input::all();
		$data['sender'] = Auth::id();

		// Send mesage
		$this->message->create($data);
	}

	public function trash($id) {
		$this->message->find($id)->delete();

		session()->flash('success_message', trans('messages.message_trashed'));

		return redirect('messages');
	}


}
