<?php

namespace Rcc\Http\Controllers;

use Illuminate\Http\Request;
use Page;
use Ambassador;

class AmbassadorsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $data = $this->getPageFromSlug(trim($request->getRequestUri(), '/'));
        $data['change_languages']['ro'] = route('ambasadori');
        $data['change_languages']['en'] = route('ambassadors');
        $data['ambassadors'] = Ambassador::all();

        return view('ambassadors' , $data);
    }
}
