<?php

namespace Rcc\Http\Controllers;

use Rcc\Http\Requests\AddJobRequest;
use Illuminate\Http\Request;
use Rcc\Http\Requests\UpdateJobTalentRequest;

use Job;
use Rcc\Models\ActorType;
use Role;
use Carbon;
use Auth;
use Rep;


class JobsController extends Controller
{

    public function __construct(Job $job) {
        $this->job          = $job;
    }

    public function index(Request $request) {
        $data = $this->getPageFromSlug(trim($request->getRequestUri(), '/'));
        $data['change_languages']['ro'] = route('castinguri.list');
        $data['change_languages']['en'] = route('castings.list');

        $data['jobs'] = $this->job->active()->approved()->complete()->simplePaginate(10);

        return view('job.list', $data);
    }

    public function mine($locale = 'ro')
    {
        $this->setLocale($locale);
        $data = $this->setLocalisedLinks();
        $data['change_languages'] = $this->getChangeLanguages();
        $data['jobs'] = $this->job->where('user_id', Auth::user()->id)->simplePaginate(10);
        $data['mine'] = true;
        return view('job.list', $data);
    }

    public function status( $id, $status ){
        $job = $this->job->whereUserId(Auth::user()->id)->whereId($id)->firstOrFail();
        $job->status = $status;
        $job->save();
        return redirect()->back()->with('success_message', $status ? trans('messages.job_activated') : trans('messages.job_deactivated'));
    }

    public function destroy( $id ){
        $this->job->destroy($id);
        return redirect()->route('jobs.mine')->with('success_message', trans('messages.job_deleted'));
    }

    public function show($id, $locale = 'ro') {
        $this->setLocale($locale);
        $data = $this->setLocalisedLinks();
        $data['job'] = $this->job->find($id);
        $data['change_languages'] = $this->getChangeLanguages($id);


        if( $this->checkUserIsTalent() )
            $data['joinJob'] = true;
        else
            $data['joinJob'] = false;

        return view('job.show', $data);
    }

    public function preview($id, $locale = 'ro') {
        $this->setLocale($locale);
        $data = $this->setLocalisedLinks();
        $data['change_languages'] = $this->getChangeLanguages($id);
        $data['job']     = $this->job->find($id);
        $data['updates'] = json_decode($data['job']->updates);
        $data['joinJob'] = false;
        $data += Rep::getJobSelects();

        if (isset($data['updates']->from_date_shooting)) {
            $data['updates']->from_date_shooting = new Carbon($data['updates']->from_date_shooting);
        }
        if (isset($data['updates']->to_date_shooting)) {
            $data['updates']->to_date_shooting = new Carbon($data['updates']->to_date_shooting);
        }
        if (isset($data['updates']->from_date_casting)) {
            $data['updates']->from_date_casting = new Carbon($data['updates']->from_date_casting);
        }
        if (isset($data['updates']->to_date_casting)) {
            $data['updates']->to_date_casting = new Carbon($data['updates']->to_date_casting);
        }

        if (!$data['updates']) {
            return redirect()->route('jobs.show', [$id, $locale]);
        }
        return view('job.show', $data);
    }

    /**
     * @param string $locale
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @internal param Request $request
     */
    public function create($locale = 'ro') {
        $this->setLocale($locale);
        $data = $this->setLocalisedLinks();
        $data += Rep::getJobSelects();
        $data['change_languages'] = $this->getChangeLanguages();

        return view('job.create', $data);
    }

    /**
     * @param AddJobRequest $request
     * @return mixed
     */
    public function ajaxCreateJob(AddJobRequest $request) {
        return Rep::createOrUpdateJobFromRequest($request);
    }

    public function ajaxUpdateJobTalent(UpdateJobTalentRequest $request) {
        return Rep::updateJobCharacters($request);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajaxComplete(Request $request) {
        Rep::jobComplete($request);
        return response()->json([]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function ajaxShowTalent(Request $request) {
        $data['job'] = Rep::getJob($request->input('id'));
        $data['key'] = 1;
        $data += Rep::getTalentSelects();
        return response()->view('job._talent', $data);
    }

    public function ajaxNewCharacter($id, Request $request) {
        $data['key'] = $request->input('key');
        $data += Rep::getTalentSelects();

        return response()->view('job._newcharacter', $data);
    }

    public function removeCharacter($id, Request $request) {
        $data['key']  = $request->input('key');
        Rep::removeCharacter($data['key'], $id);
        return response()->json($data);
    }

    public function edit($id, $locale = 'ro')
    {
        $this->setLocale($locale);
        $data = $this->setLocalisedLinks();
        $data['change_languages'] = $this->getChangeLanguages($id);
        $data['job'] = Job::find($id);
        $data += Rep::getJobSelects();

        return view('job.create', $data);
    }

    public function apply($id, $locale = 'ro')
    {
        $this->setLocale($locale);
        $user = $this->checkUserIsTalent();

        if( $user )
        {
            $checkExists = $user->characters()->where('character_id', $id)->first();

            if($checkExists)
            {
                session()->flash('warning_message', trans('messages.character_already_applied'));
            }
            else
            {
                $user->characters()->attach($id, [
                        'is_chosen' => 0,
                        'is_shortlist' => 0,
                        'is_seen' => 0,
                        'date_applied' => Carbon::now(),
                        'is_accepted' => 0,
                        'is_invite'  => 0,
                        'is_declined_employer' => 0
                    ]
                );

                session()->flash('success_message', trans('messages.character_applied'));
            }

        }
        if ($locale == 'ro') {
            return redirect()->route('castinguri.list');
        }
        return redirect()->route('castings.list');
    }

    public function applicants($id, $locale = 'ro')
    {
        $this->setLocale($locale);
        $data = $this->setLocalisedLinks();
        $data['change_languages'] = $this->getChangeLanguages($id);
        $data['characters'] = Job::find($id)->characters;

        return view('job.applicants', $data);
    }

    private function checkUserIsTalent()
    {
        if( Auth::check() )
        {
            $talentRoleId = Role::where('name', 'talent')->first()->id;
            if( Auth::user()->role->id == $talentRoleId )
                return Auth::user();
            else
                return false;

        }
        else
            return false;
    }
}
