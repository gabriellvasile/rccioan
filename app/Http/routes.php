<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Auth
//Route::controllers(['auth' => 'Auth\AuthController']);

get('/login/{locale?}',                     ['uses' => 'Auth\AuthController@getLogin',              'as' => 'login']);
post('/login/{locale?}',                    ['uses' => 'Auth\AuthController@postLogin',             'as' => 'postLogin']);

get('/register/{locale?}',                  ['uses' => 'Auth\AuthController@getRegister',           'as' => 'register']);
post('/register/{locale?}',                 ['uses' => 'Auth\AuthController@postRegister',          'as' => 'postRegister']);

get('/logout/{locale?}',                    ['uses' => 'Auth\AuthController@getLogout',             'as' => 'getLogout']);
get('/confirmEmailAddress/{id}/{locale?}',  ['uses' => 'Auth\AuthController@confirmEmailAddress',   'as' => 'confirmEmailAddress']);
get('/auth/confirm/{code}/{locale?}',       ['uses' => 'Auth\AuthController@getConfirm',            'as' => 'getConfirm']);

Route::group(['prefix' => 'insula-actorilor', 'as' => 'insula-actorilor'], function() {
    get('/',                  ['uses' => 'ActorLandController@index']);
    get('/actorul-lunii',     ['uses' => 'ActorLandController@listLunars','as' => '.lunars']);
    get('/filtru',            ['uses' => 'ActorLandController@filterActors', 'as' => '.filter']);
    get('/{slug}',            ['uses' => 'ActorLandController@listActors','as' => '.list']);
});
Route::group(['prefix' => 'actorland', 'as' => 'actorland'], function() {
    get('/',                  ['uses' => 'ActorLandController@index']);
    get('/monthly-actor',             ['uses' => 'ActorLandController@listLunars','as' => '.lunars']);
    get('/filter',            ['uses' => 'ActorLandController@filterActors', 'as' => '.filter']);
    get('/{slug}',            ['uses' => 'ActorLandController@listActors','as' => '.list']);
});

Route::group(['prefix' => 'ambassadors', 'as' => 'ambassadors'], function() {
    get('/',                  ['uses' => 'AmbassadorsController@index']);
});
Route::group(['prefix' => 'ambasadori', 'as' => 'ambasadori'], function() {
    get('/',                  ['uses' => 'AmbassadorsController@index']);
});

// Admin
Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'adminer'], 'namespace' => 'Admin'], function() {
    Route::group(['as' => 'admin.'], function() {
        get('user/needapprove',          ['as' => 'user.needapprove',     'uses' => 'UserController@needApprove']);
        get('user/approve/{id}',         ['as' => 'user.approve',   'uses' => 'UserController@approve']);
        get('user/uncomplete/{id}',      ['as' => 'user.uncomplete',   'uses' => 'UserController@uncomplete']);
        get('employer/needapprove',      ['as' => 'employer.needapprove', 'uses' => 'EmployerController@needApprove']);
        get('actor/needapprove',         ['as' => 'actor.needapprove', 'uses' => 'ActorController@needApprove']);
        get('job/needapprove',           ['as' => 'job.needapprove', 'uses' => 'JobController@needApprove']);
        get('job/approve/{id}',          ['as' => 'job.approve', 'uses' => 'JobController@approve']);
        get('job/uncomplete/{id}',       ['as' => 'job.uncomplete',   'uses' => 'JobController@uncomplete']);
        get('ajaxNewProject/{locale?}',           ['uses' => 'ActorController@ajaxNewProject',  'as' => 'ajaxNewProject']);
        delete('/removeFromPage/{page}',        ['uses' => 'PageController@removeFromPage',         'as' => 'removeFromPage']);

        get('user/create',    ['as' => 'user.create',  'uses' => 'UserController@create']);
        post('user',          ['as' => 'user.store',   'uses' => 'UserController@store']);
        get('user/{username}/edit', ['as' => 'user.edit',    'uses' => 'UserController@edit']);
        patch('user/{id}',    ['as' => 'user.update',  'uses' => 'UserController@update']);
        delete('user/{id}',   ['as' => 'user.destroy', 'uses' => 'UserController@destroy']);
        get('user',           ['as' => 'user.index',   'uses' => 'UserController@index']);

        get('actor/create',             ['as' => 'actor.create',  'uses' => 'ActorController@create']);
        post('actor',                   ['as' => 'actor.store',   'uses' => 'ActorController@store']);
        get('actor/{username}/edit',    ['as' => 'actor.edit',    'uses' => 'ActorController@edit']);
        patch('actor/{username}/edit',  ['as' => 'actor.update',  'uses' => 'ActorController@update']);
        delete('actor/{id}',            ['as' => 'actor.destroy', 'uses' => 'ActorController@destroy']);
        get('actor',                    ['as' => 'actor.index',   'uses' => 'ActorController@index']);

        get('employer/create',             ['as' => 'employer.create',  'uses' => 'EmployerController@create']);
        post('employer',                   ['as' => 'employer.store',   'uses' => 'EmployerController@store']);
        get('employer/{username}/edit',    ['as' => 'employer.edit',    'uses' => 'EmployerController@edit']);
        patch('employer/{username}/edit',  ['as' => 'employer.update',  'uses' => 'EmployerController@update']);
        delete('employer/{id}',            ['as' => 'employer.destroy', 'uses' => 'EmployerController@destroy']);
        get('employer',                    ['as' => 'employer.index',   'uses' => 'EmployerController@index']);

        get('ambassador/create',             ['as' => 'ambassador.create',  'uses' => 'AmbassadorController@create']);
        post('ambassador',                   ['as' => 'ambassador.store',   'uses' => 'AmbassadorController@store']);
        get('ambassador/{username}/edit',    ['as' => 'ambassador.edit',    'uses' => 'AmbassadorController@edit']);
        patch('ambassador/{username}/edit',  ['as' => 'ambassador.update',  'uses' => 'AmbassadorController@update']);
        delete('ambassador/{id}',            ['as' => 'ambassador.destroy', 'uses' => 'AmbassadorController@destroy']);
        get('ambassador',                    ['as' => 'ambassador.index',   'uses' => 'AmbassadorController@index']);

        get('lunar/create',             ['as' => 'lunar.create',  'uses' => 'LunarController@create']);
        post('lunar',                   ['as' => 'lunar.store',   'uses' => 'LunarController@store']);
        get('lunar/{username}/edit',    ['as' => 'lunar.edit',    'uses' => 'LunarController@edit']);
        patch('lunar/{username}/edit',  ['as' => 'lunar.update',  'uses' => 'LunarController@update']);
        delete('lunar/{id}',            ['as' => 'lunar.destroy', 'uses' => 'LunarController@destroy']);
        get('lunar',                    ['as' => 'lunar.index',   'uses' => 'LunarController@index']);

    });
    resource('job',             'JobController');
    resource('page',            'PageController');
    resource('sliders',         'SliderController');
    get('/{locale?}',  ['as' => 'admin.dashboard', 'uses' => 'HomeController@index']);
});

get('/confirmDelete/{code}',                ['uses' => 'ProfileController@confirmDelete',              'as' => 'profile.confirmDelete']);

Route::group(['middleware' => ['auth', 'owner']], function() {

    Route::resource('profile', 'ProfileController', ['except' => ['show', 'destroy', 'store', 'index', 'create', 'edit']]);
    // Ajax
    Route::group(['prefix' => 'profile', 'as' => 'profile.'], function() {
        get('{username}/initiateDelete/{locale?}',              ['uses' => 'ProfileController@initiateDelete',            'as' => 'initiateDelete']);

        patch('{username}/ajaxUpdateUser/{locale?}',            ['uses' => 'ProfileController@ajaxUpdateUser',            'as' => 'ajaxUpdateUser']);
        patch('{username}/ajaxUpdateCompany/{locale?}',         ['uses' => 'ProfileController@ajaxUpdateCompany',         'as' => 'ajaxUpdateCompany']);
        patch('{username}/ajaxUpdateCharacteristics/{locale?}', ['uses' => 'ProfileController@ajaxUpdateCharacteristics', 'as' => 'ajaxUpdateCharacteristics']);

        patch('{username}/ajaxUpdateEducation/{locale?}',       ['uses' => 'ProfileController@ajaxUpdateEducation',       'as' => 'ajaxUpdateEducation']);
        post('{username}/ajaxUpdateFiles/{locale?}',            ['uses' => 'ProfileController@ajaxUpdateFiles',           'as' => 'ajaxUpdateFiles']);
        post('{username}/ajaxComplete/{locale?}',               ['uses' => 'ProfileController@ajaxComplete',              'as' => 'ajaxComplete']);

        get('{username}/ajaxNewProject/{locale?}',              ['uses' => 'ProfileController@ajaxNewProject',            'as' => 'ajaxNewProject']);
        get('{username}/ajaxNewWorkshop/{locale?}',             ['uses' => 'ProfileController@ajaxNewWorkshop',           'as' => 'ajaxNewWorkshop']);
        get('{username}/ajaxNewVimeo/{locale?}',                ['uses' => 'ProfileController@ajaxNewVimeo',              'as' => 'ajaxNewVimeo']);
        delete('{username}/removeFromProfile/{locale?}',        ['uses' => 'ProfileController@removeFromProfile',         'as' => 'removeFromProfile']);

        get('{username}/ajaxShowCharasteristics/{locale?}',     ['uses' => 'ProfileController@ajaxShowCharasteristics',   'as' => 'ajaxShowCharasteristics']);
        get('{username}/ajaxShowEducation/{locale?}',           ['uses' => 'ProfileController@ajaxShowEducation',         'as' => 'ajaxShowEducation']);
        get('{username}/ajaxShowFiles/{locale?}',      ['uses' => 'ProfileController@ajaxShowFiles',             'as' => 'ajaxShowFiles']);

        post('{username}/ajaxShowUser/{locale?}',               ['uses' => 'ProfileController@ajaxShowUser',              'as' => 'ajaxShowUser']);
    });
});

get('/profile/download/{username}/{locale?}',   ['uses' => 'ProfileController@pdf',     'as' => 'profile.pdf', 'middleware' => ['approved', 'auth']]);
get('/profile/preview/{username}/{locale?}',    ['uses' => 'ProfileController@preview', 'as' => 'profile.preview', 'middleware' => ['auth', 'adminer']]);
get('/profile/{username}/edit/{locale?}',       ['uses' => 'ProfileController@edit',    'as' => 'profile.edit', 'middleware' => ['auth', 'owner']]);
get('/profile/{username}/{locale?}',            ['uses' => 'ProfileController@show',    'as' => 'profile.show', 'middleware' => ['approved']]);

// Castings
Route::group(['middleware' => 'auth', 'prefix' => 'castings', 'as' => 'casts.'], function() {
    get( '/mine/{locale?}',             ['uses' => 'CastingsController@myCastings', 'as' => 'mine' ] );
    get('/approve/{job}/{user}',        ['uses' => 'CastingsController@approve',    'as' => 'approve']);
    get('/reject/{job}/{user}',         ['uses' => 'CastingsController@reject',     'as' => 'reject']);
    get('/shortlist/{job}/{user}',      ['uses' => 'CastingsController@shortlist',  'as' => 'shortlist']);
    delete('/{id}/{locale?}',           ['uses' => 'CastingsController@destroy',    'as' => 'destroy']);
});

Route::group(['prefix' => 'jobs', 'as' => 'jobs.'], function() {
    get('/', ['uses' => 'JobsController@index',          'as' => 'list']);
    get('/job/{id}/{locale?}',                   ['uses' => 'JobsController@show',           'as' => 'show']);

    Route::group(['middleware' => ['auth']], function() {
        post('/apply/{id}/{locale?}',                ['uses' => 'JobsController@apply',          'as' => 'apply']);

        Route::group(['middleware' => ['employer']], function() {
            get('/create/{locale?}',                      ['uses' => 'JobsController@create',             'as' => 'create']);
            patch('/create',                    ['uses' => 'JobsController@ajaxCreateJob',      'as' => 'ajaxCreateJob']);
            get('/show/ajaxShowTalent/{locale?}',         ['uses' => 'JobsController@ajaxShowTalent',     'as' => 'ajaxShowTalent']);
            patch('/ajaxUpdateJobTalent/',      ['uses' => 'JobsController@ajaxUpdateJobTalent','as' => 'ajaxUpdateJobTalent']);
            post('/ajaxComplete',               ['uses' => 'JobsController@ajaxComplete',       'as' => 'ajaxComplete']);

            Route::group(['middleware' => ['jobowner']], function() {
                get('/preview/{id}/{locale?}',         ['uses' => 'JobsController@preview',        'as' => 'preview']);
                get( '/edit/{id}/{locale?}',                ['uses' => 'JobsController@edit',           'as' => 'edit' ] );
                patch('/status/{id}/{status}',              ['uses' => 'JobsController@status',         'as' => 'status']);
                delete('/destroy/{id}/',                    ['uses' => 'JobsController@destroy',        'as' => 'destroy']);
                get('/applicants/{id}/{locale?}',           ['uses' => 'JobsController@applicants',     'as' => 'applicants']);
                get('/ajaxNewCharacter/{id}/{locale?}',     ['uses' => 'JobsController@ajaxNewCharacter', 'as' => 'ajaxNewCharacter']);
                delete('/removeCharacter/{id}',             ['uses' => 'JobsController@removeCharacter',  'as' => 'removeCharacter']);
            });
            get('/mine/{locale?}',                    ['uses' => 'JobsController@mine',           'as' => 'mine']);
        });
    });
});
Route::group(['prefix' => 'castings', 'as' => 'castings.'], function() {
    get('/', ['uses' => 'JobsController@index',          'as' => 'list']);
    get('/job/{id}/',                   ['uses' => 'JobsController@show',           'as' => 'show']);

    Route::group(['middleware' => ['auth']], function() {
        post('/apply/{id}/{locale?}',                ['uses' => 'JobsController@apply',          'as' => 'apply']);

        Route::group(['middleware' => ['employer']], function() {
            get('/create',                      ['uses' => 'JobsController@create',             'as' => 'create']);
            patch('/create',                    ['uses' => 'JobsController@ajaxCreateJob',      'as' => 'ajaxCreateJob']);
            get('/show/ajaxShowTalent/{locale?}',         ['uses' => 'JobsController@ajaxShowTalent',     'as' => 'ajaxShowTalent']);
            patch('/ajaxUpdateJobTalent/',      ['uses' => 'JobsController@ajaxUpdateJobTalent','as' => 'ajaxUpdateJobTalent']);
            post('/ajaxComplete',               ['uses' => 'JobsController@ajaxComplete',       'as' => 'ajaxComplete']);

            Route::group(['middleware' => ['jobowner']], function() {
                get( '/edit/{id}',              ['uses' => 'JobsController@edit',           'as' => 'edit' ] );
                patch('/status/{id}/{status}',  ['uses' => 'JobsController@status',         'as' => 'status']);
                delete('/destroy/{id}/',        ['uses' => 'JobsController@destroy',        'as' => 'destroy']);
                get('/applicants/{id}',         ['uses' => 'JobsController@applicants',     'as' => 'applicants']);
                get('/ajaxNewCharacter/{id}/{locale?}',   ['uses' => 'JobsController@ajaxNewCharacter', 'as' => 'ajaxNewCharacter']);
                delete('/removeCharacter/{id}', ['uses' => 'JobsController@removeCharacter',  'as' => 'removeCharacter']);
            });
            get('/mine',                    ['uses' => 'JobsController@mine',           'as' => 'mine']);
        });
    });
});
Route::group(['prefix' => 'castinguri', 'as' => 'castinguri.'], function() {
    get('/', ['uses' => 'JobsController@index',          'as' => 'list']);
    get('/job/{id}/',                   ['uses' => 'JobsController@show',           'as' => 'show']);

    Route::group(['middleware' => ['auth']], function() {
        post('/apply/{id}/{locale?}',                ['uses' => 'JobsController@apply',          'as' => 'apply']);

        Route::group(['middleware' => ['employer']], function() {
            get('/create',                      ['uses' => 'JobsController@create',             'as' => 'create']);
            patch('/create',                    ['uses' => 'JobsController@ajaxCreateJob',      'as' => 'ajaxCreateJob']);
            get('/show/ajaxShowTalent/{locale?}',         ['uses' => 'JobsController@ajaxShowTalent',     'as' => 'ajaxShowTalent']);
            patch('/ajaxUpdateJobTalent/',      ['uses' => 'JobsController@ajaxUpdateJobTalent','as' => 'ajaxUpdateJobTalent']);
            post('/ajaxComplete',               ['uses' => 'JobsController@ajaxComplete',       'as' => 'ajaxComplete']);

            Route::group(['middleware' => ['jobowner']], function() {
                get( '/edit/{id}',              ['uses' => 'JobsController@edit',           'as' => 'edit' ] );
                patch('/status/{id}/{status}',  ['uses' => 'JobsController@status',         'as' => 'status']);
                delete('/destroy/{id}/',        ['uses' => 'JobsController@destroy',        'as' => 'destroy']);
                get('/applicants/{id}',         ['uses' => 'JobsController@applicants',     'as' => 'applicants']);
                get('/ajaxNewCharacter/{id}/{locale?}',   ['uses' => 'JobsController@ajaxNewCharacter', 'as' => 'ajaxNewCharacter']);
                delete('/removeCharacter/{id}', ['uses' => 'JobsController@removeCharacter',  'as' => 'removeCharacter']);
            });
            get('/mine',                    ['uses' => 'JobsController@mine',           'as' => 'mine']);
        });
    });
});

Route::group(['middleware' => ['auth']], function() {
    // Mesages
    Route::group(['prefix' => 'messages', 'as' => 'messages.'], function() {
        post('/markread',           ['uses' => 'MessagesController@markread',   'as' => 'markread']);
        delete('/{id}/trash/',      ['uses' => 'MessagesController@trash',      'as' => 'trash']);
        get('/new/{id?}/{locale?}', ['uses' => 'MessagesController@newmessage', 'as' => 'new']);
        delete('/destroy/{id}',     ['uses' => 'MessagesController@destroy',    'as' => 'destroy']);
        post('/send',               ['uses' => 'MessagesController@sendMessage','as' => 'send']);
        get('/reply/{id}/{locale?}',['uses' => 'MessagesController@reply',      'as' => 'reply']);
        post('/doreply/user/{id}',  ['uses' => 'MessagesController@doreply',    'as' => 'doreply']);
        get('/forward/{id}',        ['uses' => 'MessagesController@forward',    'as' => 'forward']);
        post('/doforward/{id}',     ['uses' => 'MessagesController@doforward',  'as' => 'doforward']);
        get('/searchRecipient',     ['uses' => 'MessagesController@searchRecipient',      'as' => 'searchRecipient']);
        get('/{locale?}',           ['uses' => 'MessagesController@index',      'as' => 'list']);
    });
});

// Pagini
get('/partners',        ['uses' => 'PageController@partners',       'as' => 'partners']);

// Contact
get('/contact',         ['uses' => 'ContactController@index',       'as' => 'contact']);
get('/contact-us',      ['uses' => 'ContactController@index',       'as' => 'contact-us']);
post('/contact',        ['uses' => 'ContactController@post',        'as' => 'contactPost']);

get('/',                ['uses' => 'PageController@show',           'as' => 'home']);
get('/home',            ['uses' => 'PageController@show',           'as' => 'oldhome']);
get('/{slug?}',         ['uses' => 'PageController@show',           'as' => 'page.show']);
