<?php

namespace Rcc\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \Rcc\Http\Middleware\EncryptCookies::class,
        \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
        \Illuminate\Session\Middleware\StartSession::class,
        \Illuminate\View\Middleware\ShareErrorsFromSession::class,
        \Rcc\Http\Middleware\VerifyCsrfToken::class,
        \Rcc\Http\Middleware\GlobalConfig::class,
    ];

    /**
     * The application's route middleware.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \Rcc\Http\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'guest' => \Rcc\Http\Middleware\RedirectIfAuthenticated::class,
        'adminer' => \Rcc\Http\Middleware\CheckAdminer::class,
        'owner' => \Rcc\Http\Middleware\Owner::class,
        'approved' => \Rcc\Http\Middleware\Approved::class,
        'employer' => \Rcc\Http\Middleware\Employer::class,
        'jobowner' => \Rcc\Http\Middleware\JobOwner::class,
    ];
}
