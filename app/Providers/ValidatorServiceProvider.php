<?php

namespace Rcc\Providers;

use Illuminate\Support\ServiceProvider;

class ValidatorServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app['validator']->extend('mobile', function ($attribute, $value, $parameters) {
            if (!preg_match('/^[\d\s\(\)\.\-\+]+$/', $value)) { // Digits, spaces, "(", ")", ".", "-", "\+"
                return false;
            }
            return true;
        });

        $this->app['validator']->extend('year', function ($attribute, $value, $parameters) {
            if (!preg_match('/^[\d]+$/', $value) || $value < 1970 || $value > 2020) { // Digits and between 1970 & 2020
                return false;
            }
            return true;
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
