<?php

namespace Rcc\Providers;

use Illuminate\Support\ServiceProvider;
use Rcc\Repositories\RccRepository;
use Rcc\Repositories\Mails;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('mails', function ($app) {
            return new Mails();
        });
        $this->app->singleton('rep', function ($app) {
            return new RccRepository();
        });
    }
}
