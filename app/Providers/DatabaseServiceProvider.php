<?php

namespace Rcc\Providers;

use Illuminate\Database\DatabaseServiceProvider as MainDatabaseServiceProvider;
use Faker\Generator as FakerGenerator;
use Faker\Factory as FakerFactory;

class DatabaseServiceProvider extends MainDatabaseServiceProvider {
    /**
     * Register the Eloquent factory instance in the container.
     *
     * @return void
     */
    protected function registerEloquentFactory()
    {
        parent::registerEloquentFactory();
        $this->app->singleton(FakerGenerator::class, function () {
            return FakerFactory::create('ro_RO');
        });
    }
}