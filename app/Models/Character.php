<?php

namespace Rcc\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Character extends Model {

    protected $fillable = [ 'job_id', 'range_id', 'hair_colour_id', 'eye_colour_id', 'sex', 'from_height', 'to_height', 'extra_details'];

    public function Users() {
        return $this->belongsToMany( 'Rcc\Models\User' )
            ->withPivot( 'is_chosen', 'is_shortlist', 'is_seen', 'date_applied', 'is_accepted', 'is_invite', 'is_declined_employer' )
            ->withTimestamps();
    }

    public function Job() {
        return $this->belongsTo( 'Rcc\Models\Job' );
    }

    public function Range() {
        return $this->belongsTo( 'Rcc\Models\Range' );
    }

    public function EyeColour() {
        return $this->belongsTo( 'Rcc\Models\EyeColour' );
    }

    public function HairColour() {
        return $this->belongsTo( 'Rcc\Models\HairColour' );
    }

    public function Applied() {
        return $this->users()->where('user_id', Auth::id())->first();
    }

}
