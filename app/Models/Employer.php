<?php

namespace Rcc\Models;

use Illuminate\Database\Eloquent\Model;

class Employer extends Model
{
	protected $fillable = ['logo_id', 'company', 'address', 'post_code', 'website', 'info'];

    public function User()
	{
		return $this->belongsTo('Rcc\Models\User');
	}

	public function EmployerType() {
		return $this->belongsTo('Rcc\Models\EmployerType');
	}

	public function UnapprovedEmployers() {
		return $this->whereHas('user', function($query) {
			$query->whereNotNull('updates');
		})->with('user')->get();
	}

	public function EmployerWithUser($username) {
		return $this->whereHas('user', function($query) use ($username) {
			$query->where('username', $username);
		})->with('user')->first();
	}

}
