<?php

namespace Rcc\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

use Rcc\Models\Employer;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'country_id', 'role_id', 'language_id',
        'active', 'approved', 'complete',
        'email',
        'fname', 'lname', 'city', 'mobile', 'sex', 'birth_date', 'thumb_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    public function Actor()
    {
        return $this->hasOne('Rcc\Models\Actor');
    }

    public function Employer()
    {
        return $this->hasOne('Rcc\Models\Employer');
    }

    public function EmployerType()
    {
        return $this->belongsTo('Rcc\Models\EmployerType');
    }

    public function ActorType()
    {
        return $this->belongsTo('Rcc\Models\ActorType');
    }

    public function Role()
    {
        return $this->belongsTo('Rcc\Models\Role');
    }

    public function Country()
    {
        return $this->belongsTo('Rcc\Models\Country');
    }

    public function Ethnicity()
    {
        return $this->belongsTo('Rcc\Models\Ethnicity');
    }

    public function Licences()
    {
        return $this->belongsToMany('Rcc\Models\Licence')->withTimestamps();
    }

    public function Ranges()
    {
        return $this->belongsToMany('Rcc\Models\Range')->withTimestamps();
    }

    public function Productions()
    {
        return $this->belongsToMany('Rcc\Models\ProductionType');
    }

    public function Characters()
    {
        return $this->belongsToMany('Rcc\Models\Character')->withPivot('is_chosen', 'is_shortlist', 'is_seen', 'date_applied', 'is_accepted', 'is_invite', 'is_declined_employer')->withTimestamps();
    }

    public function Projects()
    {
        return $this->hasMany('Rcc\Models\Project');
    }
    
    public function Workshops()
    {
        return $this->hasMany('Rcc\Models\Workshop');
    }
    
    public function Images()
    {
        return $this->hasMany('Rcc\Models\Image');
    }
    
    public function Soundclouds()
    {
        return $this->hasMany('Rcc\Models\Soundcloud');
    }
    
    public function Tracks()
    {
        return $this->hasMany('Rcc\Models\Track');
    }
    
    public function Vimeos()
    {
        return $this->hasMany('Rcc\Models\Vimeo');
    }
    
    public function Youtubes()
    {
        return $this->hasMany('Rcc\Models\Youtube');
    }

    public function addVimeo($userid, $link) {
        $vimeo = new Vimeo;
        $vimeo['user_id'] = $userid;
        $vimeo['code'] = $link;
        $vimeo->save();
        return $vimeo->id;
    }

    public function addYotube($userid, $link) {
        $youtube = new Youtube;
        $youtube['user_id'] = $userid;
        $youtube['code'] = $link;
        $youtube->save();
        return $youtube->id;
    }

    public function addSoundcloud($userid, $link) {
        $soundcloud = new Soundcloud;
        $soundcloud['user_id'] = $userid;
        $soundcloud['code'] = $link;
        $soundcloud->save();
        return $soundcloud->id;
    }

    public function deleteProject($userid, $projectId) {
        Project::whereRaw('id = ? and user_id = ?', [$projectId, $userid])->delete();
    }

        public function deleteWorkshop($userid, $workshopId) {
        Workshop::whereRaw('id = ? and user_id = ?', [$workshopId, $userid])->delete();
    }

    public function deleteImage($userid, $imageId) {
        $file = Image::whereRaw('id = ? and user_id = ?', [$imageId, $userid])->first();
        $user = User::find($userid);
        if ( $user->thumb_id == $imageId ) {
          $user->thumb_id = 0;
          $user->save();
        }
     unlink(public_path() . $file->path);
     $file->delete();
    }

    public function deleteVimeo($userid, $vimeoId) {
        Vimeo::whereRaw('id = ? and user_id = ?', [$vimeoId, $userid])->delete();
    }

    public function deleteYoutube($userid, $youtubeId) {
        Youtube::whereRaw('id = ? and user_id = ?', [$youtubeId, $userid])->delete();
    }

    public function deleteSoundcloud($userid, $soundcloudId) {
        Soundcloud::whereRaw('id = ? and user_id = ?', [$soundcloudId, $userid])->delete();
    }

    public function saveImages($userId, $paths, $type = null) {
        foreach ( $paths as $path ) {
            $image = new Image;
            $image->user_id = $userId;
            $image->path    = '/uploads/' . $path;
            $image->thumb   = '/uploads/' . $path;
            $image->save();

            if ( $type == 'profile' )
                User::where('id', $userId)->update(['thumb_id' => $image->id]);
            elseif( $type == 'logo' )
                Employer::where('user_id', $userId)->update(['logo_id' => $image->id]);
        }
    }

    public function scopeActive( $query ) {
        return $query->where( 'active', 1 );
    }
    public function scopeApproved( $query ) {
        return $query->where( 'approved', 1 );
    }
    public function scopeComplete( $query ) {
        return $query->where( 'complete', 1 );
    }

}
