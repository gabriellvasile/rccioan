<?php

namespace Rcc\Models;

use Illuminate\Database\Eloquent\Model;

class Actor extends Model
{

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['birth_date'];

    protected $fillable = ['ethnicity_id', 'eye_colour_id', 'hair_colour_id', 'height', 'weight', 'accent', 'awards', 'workshops', 'hair_length', 'spoken_languages', 'facial_traits', 'voice', 'dance', 'instruments', 'sports', 'profesional_studies', 'speciality_studies', 'speciality_year', 'speciality_institution', 'faculty', 'faculty_student', 'faculty_year', 'faculty_institution', 'faculty_domain', 'website', 'info', 'production_types', 'role_abroad'];

	public function User()
	{
		return $this->belongsTo('Rcc\Models\User');
	}
	public function Lunars()
	{
		return $this->hasMany('Rcc\Models\Lunar');
	}

    public function Ethnicity()
    {
        return $this->belongsTo('Rcc\Models\Ethnicity');
    }

    public function EyeColour()
    {
        return $this->belongsTo('Rcc\Models\EyeColour');
    }

    public function HairColour()
    {
        return $this->belongsTo('Rcc\Models\HairColour');
    }

    public function ActorType()
    {
        return $this->belongsTo('Rcc\Models\ActorType');
    }

    public function UnapprovedActors() {
        return $this->whereHas('user', function($query) {
            $query->whereNotNull('updates');
        })->with('user')->get();
    }

    public function ActorWithUser($username) {
        return $this->whereHas('user', function($query) use ($username) {
            $query->where('username', $username);
        })->with('ethnicity', 'eyecolour', 'haircolour', 'user')->first();
    }
}
