<?php

namespace Rcc\Models;

use Illuminate\Database\Eloquent\Model;

class Ambassador extends Model
{
    protected $fillable = ['name', 'video', 'quote', 'description', 'image'];

}
