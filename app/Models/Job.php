<?php

namespace Rcc\Models;

use Illuminate\Database\Eloquent\Model;

class Job extends Model {

	protected $dates    = [ 'from_date_casting', 'to_date_casting', 'from_date_shooting', 'to_date_shooting' ];
	protected $fillable = [ 'production_name', 'producer_name', 'location', 'production_type_id', 'from_date_shooting', 'to_date_shooting',
		'from_date_casting', 'to_date_casting', 'student_project', 'payment', 'email', 'telephone', 'exclusive', 'guest_visible', 'notifying_method',
		'short_description', 'university', 'year', 'tutor_name', 'department' ];

	public function Characters() {
		return $this->hasMany( 'Rcc\Models\Character' );
	}
	public function Owner() {
		return $this->belongsTo( 'Rcc\Models\User', 'user_id' );
	}

	public function ProductionType() {
		return $this->belongsTo( 'Rcc\Models\ProductionType' );
	}

	public function ActorType() {
		return $this->belongsTo( 'Rcc\Models\ActorType' );
	}

	public function Employer() {
		return $this->belongsTo( 'Rcc\Models\Employer' );
	}

	/**
	 * @param $query
	 *
	 * @return mixed
	 */
	public function scopeActive( $query ) {
		return $query->where( 'status', 1 );
	}
	public function scopeApproved( $query ) {
		return $query->where( 'approved', 1 );
	}
	public function scopeComplete( $query ) {
		return $query->where( 'complete', 1 );
	}
}
