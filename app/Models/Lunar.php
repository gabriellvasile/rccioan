<?php

namespace Rcc\Models;

use Illuminate\Database\Eloquent\Model;

class Lunar extends Model
{
    protected $fillable = ['actor_id', 'video', 'description', 'date', 'name'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['date'];

    /**
     * Scope a query to only include popular users.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeDesc($query)
    {
        return $query->orderBy('date', 'DESC');
    }

    public function Actor()
    {
        return $this->belongsTo('Rcc\Models\Actor');
    }
}
