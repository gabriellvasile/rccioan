<?php

namespace Rcc\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    public function User()
    {
        return $this->hasMany('Rcc\Models\Role');
    }
}
