<?php

namespace Rcc\Models;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model {

    protected $fillable = [ 'path', 'thumb', 'sort', 'title1_en', 'title2_en', 'desc_en', 'title1_ro', 'title2_ro', 'desc_ro'];

}
