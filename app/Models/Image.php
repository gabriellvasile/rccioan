<?php

namespace Rcc\Models;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    public function User() {
		return $this->belongsTo('Rcc\Models\User');
	}
}
