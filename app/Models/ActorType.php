<?php

namespace Rcc\Models;

use Illuminate\Database\Eloquent\Model;

class ActorType extends Model
{
   	public function Actors()
    {
        return $this->hasMany('Rcc\Models\Actor');
    }
}
