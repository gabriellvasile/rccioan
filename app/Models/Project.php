<?php

namespace Rcc\Models;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    /**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'house', 'year', 'director', 'role', 'description'];

	public function User() {
		return $this->belongsTo('Rcc\Models\User');
	}

}
