<?php namespace Rcc\Models;

use Illuminate\Database\Eloquent\Model;

class Page extends Model {


	protected $fillable = ['parent_id', 'status', 'order', 'image'];

	public function languages() {
		return $this->belongsToMany('Rcc\Models\Language')->withPivot('slug', 'title', 'content', 'excerpt', 'meta_title', 'meta_description', 'meta_keywords')->withTimestamps();
	}

	public function parent() {
		return $this->belongsTo('Rcc\Models\Page', 'parent_id');
	}

	public function children() {
		return $this->hasMany('Rcc\Models\Page', 'parent_id');
	}

	public static function getLocalisedChildren($page_id, $locale = 'ro') {
		return Page::join('language_page', 'pages.id', '=', 'language_page.page_id')
			->join('languages', 'languages.id', '=', 'language_page.language_id')
			->where('languages.code', $locale)
			->where('pages.parent_id', $page_id)
			->where('pages.status', 1)
			->orderBy('pages.order', 'asc')
			->get();
	}

	public static function getLocalisedFilterPage($page_id, $locale = 'ro') {
		return Page::join('language_page', 'pages.id', '=', 'language_page.page_id')
			->join('languages', 'languages.id', '=', 'language_page.language_id')
			->where('languages.code', $locale)
			->where('pages.parent_id', $page_id)
			->where('pages.status', 0)
			->orderBy('pages.order', 'asc')
			->first();
	}

	public static function headerPages($locale) {
		return Language::whereCode($locale)
			->join('language_page', 'languages.id', '=', 'language_page.language_id')
			->join('pages', 'pages.id', '=', 'language_page.page_id')
			->where('header', 1)
			->orderBy('pages.order', 'asc')
			->get();
	}

	public static function footerPages($locale) {
		return Page::join('language_page', 'pages.id', '=', 'language_page.page_id')
			->join('languages', 'languages.id', '=', 'language_page.language_id')
			->where('footer', 1)
			->where('languages.code', $locale)
			->orderBy('pages.order', 'asc')
			->get();
	}

}