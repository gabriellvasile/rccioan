<?php

namespace Rcc\Models;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['message', 'is_read', 'is_important', 'subject', 'status', 'sent', 'sender_id', 'recipient_id'];
	protected $dates = ['deleted_at', 'created_at'];

	public function Sender() {
		return $this->belongsTo('Rcc\Models\User', 'sender_id');
	}
	public function Recipient() {
		return $this->belongsTo('Rcc\Models\User', 'recipient_id');
	}


	public function allBySender($id) {
		return Message::where('sender_id', $id)->with('sender')->orderBy('created_at', 'desc')->get();
	}
	public function allByRecipient($id) {
		return Message::where('recipient_id', $id)->with('recipient')->orderBy('created_at', 'desc')->get();
	}

	public function allTrashed($id) {
		return Message::onlyTrashed()->where(function($query) use ($id)
		        {
		            $query->where('sender_id', $id)
		                  ->orWhere('recipient_id', $id);
		        })
		->get();
	}

}
