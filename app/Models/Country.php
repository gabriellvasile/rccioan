<?php

namespace Rcc\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'countries';

    public function User()
    {
    	return $this->hasOne('Rcc\Models\User');
    }
}
