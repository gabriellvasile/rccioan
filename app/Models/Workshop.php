<?php

namespace Rcc\Models;

use Illuminate\Database\Eloquent\Model;

class Workshop extends Model
{
    /**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'award', 'year', 'description'];

	public function User() {
		return $this->belongsTo('Rcc\Models\User');
	}

}
