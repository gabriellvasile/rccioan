<?php

namespace Rcc\Models;

use Illuminate\Database\Eloquent\Model;

class EmployerType extends Model
{
   	public function Employers() {
        return $this->hasMany('Rcc\Models\Employer');
    }
}
