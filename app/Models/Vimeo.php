<?php

namespace Rcc\Models;

use Illuminate\Database\Eloquent\Model;

class Vimeo extends Model
{
    public function User() {
		return $this->belongsTo('Rcc\Models\User');
	}
}
