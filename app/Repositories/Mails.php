<?php

namespace Rcc\Repositories;

use Mail;
use Rcc\Models\Message;
use Rcc\Models\User;
use Rcc\Models\Job;
use Rcc\Models\Lunar;
use Rcc\Models\Character;
use ContactRequest;

class Mails
{

    public function sendConfirmationEmail($user){
        Mail::send('emails.confirm', ['fname' => $user->fname, 'link' => $user->code], function($message) use ($user) {
            $message->to($user->email, $user->fname . ' ' . $user->lname)->subject(trans('messages.please_activate'));
        });
    }

    public function sendConfirmDeleteEmail($user){
        Mail::send('emails.confirm_delete', ['fname' => $user->fname, 'code' => $user->code], function($message) use ($user) {
            $message->to($user->email, $user->fname . ' ' . $user->lname)->subject(trans('messages.please_confirm_delete'));
        });
    }
    /**
     * Send approved email after approving the account.
     * @param User $user
     */
    public function sendApprovedEmail(User $user)
    {
        Mail::send('emails.approve', ['user' => $user], function ($message) use ($user) {
            $message->to($user->email, $user->fname . ' ' . $user->lname)->subject(trans('messages.approved'));
        });
    }
    /**
     * Send approved email after approving the account.
     * @param User $user
     */
    public function sendApprovedUpdatesEmail(User $user)
    {
        Mail::send('emails.approve_updates', ['user' => $user], function ($message) use ($user) {
            $message->to($user->email, $user->fname . ' ' . $user->lname)->subject(trans('messages.profile_modifications'));
        });
    }
    /**
     * Send approved email after approving the account.
     * @param User $user
     */
    public function sendUnApprovedUpdatesEmail(User $user)
    {
        Mail::send('emails.unapprove_updates', ['user' => $user], function ($message) use ($user) {
            $message->to($user->email, $user->fname . ' ' . $user->lname)->subject(trans('messages.profile_modifications'));
        });
    }

    /**
     * Send approved email after approving a job.
     * @param Job $job
     * @internal param User $user
     */
    public function sendApprovedJobEmail(Job $job)
    {
        $user = $job->owner;
        Mail::send('emails.job_approve', ['user' => $user, 'job' => $job], function ($message) use ($user) {
            $message->to($user->email, $user->fname . ' ' . $user->lname)->subject(trans('messages.approved_casting'));
        });
    }

    /**
     * Send approved email after approving a job.
     * @param Job $job
     * @internal param User $user
     */
    public function sendApprovedJobUpdatesEmail(Job $job)
    {
        $user = $job->owner;
        Mail::send('emails.job_updates_approved', ['user' => $user, 'job' => $job], function ($message) use ($user) {
            $message->to($user->email, $user->fname . ' ' . $user->lname)->subject(trans('messages.casting_modif'));
        });
    }

    /**
     * Send approved email after approving a job.
     * @param Job $job
     * @param User $user
     */
    public function sendCastingUpdatesNotice(Job $job, User $user)
    {
        Mail::send('emails.job_updates_notice', ['user' => $user, 'job' => $job], function ($message) use ($user) {
            $message->to($user->email, $user->fname . ' ' . $user->lname)->subject(trans('messages.casting_modif'));
        });
    }

    /**
     * Send unapproved email after didapproving modifications.
     * @param Job $job
     * @internal param User $user
     */
    public function sendUncompleteJobEmail(Job $job)
    {
        $user = $job->owner;
        Mail::send('emails.job_uncomplete', ['user' => $user, 'job' => $job], function ($message) use ($user) {
            $message->to($user->email, $user->fname . ' ' . $user->lname)->subject(trans('messages.casting_modif'));
        });
    }

    public function newApplicantsEmail($user){
        Mail::send('emails.new_applicants', ['user' => $user, 'jobs' => $user['jobs']], function ($message) use ($user) {
            $message->to(env('MAIL_TEST'))->subject(trans('messages.new_applicants')); // TODO: change to $user['email']
        });
    }

    public function castingsToExpire($user, $hours){
        if (24 == $hours) {
            $subject = trans('messages.castings_to_expire_24');
        }
        else {
            $subject = trans('messages.castings_to_expire_48');
        }

        Mail::send('emails.castings_to_expire', ['user' => $user, 'jobs' => $user['jobs'], 'hours' => $hours], function ($message) use ($user, $subject) {
            $message->to(env('MAIL_TEST'))->subject($subject); // TODO: change to $user['email']
        });
    }

    public function expiredCastings($user){
        Mail::send('emails.expired_castings', ['user' => $user, 'jobs' => $user['jobs']], function ($message) use ($user) {
            $message->to(env('MAIL_TEST'))->subject(trans('messages.expired_castings')); // TODO: change to $user['email']
        });
    }

    /**
     * Send email to notice user that it has an account on website
     * @param User $user
     * @param $password
     */
    public function accountCreated(User $user, $password)
    {
        $vars = [
            'user' => $user,
            'password' => $password
        ];

        Mail::send('emails.created', $vars, function ($message) use ($user) {
            $message->to($user->email, $user->fname . ' ' . $user->lname)->subject(trans('messages.account_new'));
        });
    }

    /**
     * Send `tact email
     * @param ContactRequest $request
     */
    public function contact(ContactRequest $request)
    {
        Mail::send('emails.contact', ['mail' => $request],
            function($message) use ($request) {
                $message->to(env('MAIL_RCC'), $request['first_name'] . ' ' . $request['last_name'])->subject($request['subject']);
            }
        );
    }

    /**
     * User account updated
     * @param User $user
     */
    public function accountUpdated(User $user){
        if ($user->role_id == 4) {
            $subject = trans('messages.company_account_updated');
        }
        else {
            $subject = trans('messages.user_account_updated');
        }

        Mail::send('emails.updated', ['user' => $user],
            function($message) use ($user, $subject) {
                $message->to(env('MAIL_RCC'), $user['first_name'] . ' ' . $user['last_name'])->subject($subject);
            }
        );
    }

    /**
     * User account updated
     * @param User $user
     */
    public function accountModifyRequest(User $user){
        Mail::send('emails.account_modify_request', ['user' => $user],
            function($message) use ($user) {
                $message->to($user->email, $user['first_name'] . ' ' . $user['last_name'])->subject(trans('messages.account_modify_request'));
            }
        );
    }

    /**
     * User account updated
     * @param Job $job
     */
    public function jobPostedOrUpdated(Job $job){
        Mail::send('emails.job_updated', ['job' => $job],
            function($message) use ($job) {
                $message->to(env('MAIL_RCC'))->subject(trans('messages.job_created_or_updated'));
            }
        );
    }

    /**
     * Updates disapproved
     * @param User $user
     */
    public function sendUncompleteEmail(User $user){
        Mail::send('emails.uncomplete', ['user' => $user],
            function($message) use ($user) {
                $message->to($user->email, $user->fname . ' ' . $user->lname)->subject(trans('messages.uncompleted'));
            }
        );
    }

    public function sendApplicantApproved(User $user, Character $character){
        Mail::send('emails.applicant_approved', ['user' => $user, 'character' => $character],
            function($message) use ($user, $character) {
                $message->to($user->email, $user->fname . ' ' . $user->lname)->subject(trans('messages.applicant_answer'));
            }
        );
    }

    public function sendApplicantUnApproved(User $user, Character $character){
        Mail::send('emails.applicant_unapproved', ['user' => $user, 'character' => $character],
            function($message) use ($user, $character) {
                $message->to($user->email, $user->fname . ' ' . $user->lname)->subject(trans('messages.applicant_answer'));
            }
        );
    }

    public function sendShortlistStatus(User $user, Character $character, $shortlist ){
        Mail::send('emails.applicant_shortlist', ['user' => $user, 'character' => $character, 'shortlist' => $shortlist],
            function($message) use ($user, $character) {
                $message->to($user->email, $user->fname . ' ' . $user->lname)->subject(trans('messages.applicant_answer'));
            }
        );
    }

    public function sendLunarEmail(Lunar $lunar){
        Mail::send('emails.lunar_email', ['lunar' => $lunar],
            function($message) use ($lunar) {
                $message->to($lunar->actor->user->email, $lunar->actor->user->fname . ' ' . $lunar->actor->user->lname)->subject(trans('messages.congrats'));
            }
        );
    }

    public function sendInboxEmail(Message $m){
        Mail::send('emails.inbox', ['m' => $m],
            function($message) use ($m) {
                $message->to($m->recipient->email, $m->recipient->fname . ' ' . $m->recipient->lname)->subject(trans('messages.you_ve_got_mail'));
            }
        );
    }

}