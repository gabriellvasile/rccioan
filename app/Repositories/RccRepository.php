<?php

namespace Rcc\Repositories;

use Auth;
use Carbon\Carbon;
use App;

use Rcc\Models\ActorType;
use Rcc\Models\EmployerType;
use Rcc\Models\Ethnicity;
use Rcc\Models\EyeColour;
use Rcc\Models\HairColour;
use Rcc\Models\Licence;
use Rcc\Models\User;
use Rcc\Models\Language;
use Rcc\Models\Country;
use Rcc\Models\Role;
use Rcc\Models\Actor;
use Rcc\Models\Employer;
use Rcc\Models\Ambassador;
use Rcc\Models\Lunar;
use Rcc\Models\Production;
use Rcc\Models\ProductionType;
use Rcc\Models\Job;
use Rcc\Models\Page;
use Rcc\Models\Range;
use Rcc\Models\Project;
use Rcc\Models\Character;
use Rcc\Models\Slider;

use Rcc\Http\Requests\AddUserRequest;
use Rcc\Http\Requests\EditUserRequest;
use Rcc\Http\Requests\AddAmbassadorRequest;
use Rcc\Http\Requests\AddLunarRequest;
use Rcc\Http\Requests\AddPageRequest;
use Rcc\Http\Requests\AddSliderRequest;
use Rcc\Http\Requests\UpdateUserRequest;
use Rcc\Http\Requests\UpdateCompanyRequest;

use Mails;
use Storage;

class RccRepository {

	public function getProfileType($role_id) {
		$type = $this->getRoleName($role_id);

		return ($type == 'talent') ? 'actor' : $type;
	}

	/**
	 * @param $id
	 *
	 * @return mixed
	 */
	public function getRoleName($id) {
		return Role::where('id', $id)->pluck('name');
	}

	/**
	 * @return mixed
	 */
	public function getCapRoleName($role_id) {
		return ucfirst($this->getRoleName($role_id));
	}

	public function approveUser($username) {
		$user           = User::whereUsername($username)->firstOrFail();
		$updates = collect(json_decode($user->updates));
		$user->update($updates->toArray());

		if ($user->actor) {
			$user->actor->update($updates->toArray());
		}
		if ($user->employer) {
			$user->employer->update(collect($updates['employer'])->toArray());
		}
		if (isset($updates['productions']) && $updates['productions']) {
			$user->productions()->sync(collect($updates['productions'])->toArray());
		}
		if (isset($updates['ranges']) && $updates['ranges']) {
			$user->ranges()->sync(collect($updates['ranges'])->toArray());
		}
		if (isset($updates['licences']) && $updates['licences']) {
			$user->licences()->sync(collect($updates['licences'])->toArray());
		}
		$user->updates = null;
		$user->save();

		Mails::sendApprovedEmail($user);
	}

	public function unCompleteProfile($username, $notify = false) {
		$user           = User::whereUsername($username)->firstOrFail();
		$user->updates = null;
		$user->save();

		if ($notify) {
			Mails::sendUncompleteEmail($user);
		}
	}

	public function approveJob($id) {
		$job           = Job::findOrFail($id);
		$updates = collect(json_decode($job->updates));
		$job->update($updates->toArray());
		$job->updates = null;
		$job->save();

		Mails::sendApprovedJobEmail($job);
	}

	public function unCompleteJob($id) {
		$job           = Job::findOrFail($id);
		if ($job->updates) {
			$job->updates = null;
		}
		else {
			$job->complete = 0;
		}
		$job->save();

		Mails::sendUncompleteJobEmail($job);
	}

	/**
	 * @return array
	 */
	public function getUserSelects() {
		$data                   = [];
		$data['roles']          = $this->getRolesByYourRole();
		$data['actorTypes']     = $this->listAllById(ActorType::class);
		$data['haircolours']    = $this->listAllById(HairColour::class);
		$data['eyecolours']     = $this->listAllById(EyeColour::class);
		$data['ethnicities']    = $this->listAllById(Ethnicity::class);
		$data['licences']       = $this->listAllById(Licence::class, 'name', 'id', false);
		$data['productions']    = $this->listAllById(ProductionType::class, 'name', 'id', false);
		$data['employerTypes']  = $this->listAllById(EmployerType::class);
		$data['languages']      = $this->listAllById(Language::class);
		$data['countries']      = $this->listAllById(Country::class);
		$data['ranges']         = $this->listAllById(Range::class);
		$data['talentRoleId']   = $this->getRoleId('talent');
		$data['employerRoleId'] = $this->getRoleId('employer');
		$data['institutions']   = $this->getInstitutions();
		$data['years']   		= $this->getInstitutionYears();
		$data['hairlengths'] = [trans('messages.select'), 'S' => trans('messages.small'), 'M' => trans('messages.medium'), 'L' => trans('messages.large')];

		return $data;
	}

	/**
	 * Get Institutions
	 *
	 * @return array Institutions
	 */
	protected function getInstitutions() {
		$institutions = [
			trans('messages.select_institution'),
			trans('messages.institution_1'),
			trans('messages.institution_2'),
			trans('messages.institution_3'),
			trans('messages.institution_4'),
			trans('messages.institution_5'),
			trans('messages.institution_6'),
			trans('messages.institution_7'),
			trans('messages.institution_8'),
			trans('messages.other_inst'),
		];
		$institutions = array_combine($institutions, $institutions);
		foreach ($institutions as $key => $inst) {
			array_unshift($institutions, $institutions[ $key ]);
			unset($institutions[ $key ]);
			break;
		}

		return $institutions;
	}

	/**
	 * Get Graduation Institution Years
	 *
	 * @return array
	 */
	protected function getInstitutionYears() {
		$years = [trans('messages.select_year')] + range(2021, 1970);

		return array_combine($years, $years);
	}

	public function setUserUpdates(UpdateUserRequest $request, $username ) {
		$user = User::whereUsername($username)->firstOrFail();
		$differences = array_diff_assoc((array)$request->except(['_method', '_token', 'password', 'password_confirmation', 'username', 'locale']), $user->toArray());

		if ($user->username_changed < 2 && $request->input('username') != $user->username) {
			$user->username = $request->input('username');
			$user->username_changed = $user->username_changed + 1;
			$user->save();
		}

		$this->savePasswordIfSet($request, $user);
		return $this->mergeUpdates($user, $differences);
	}

	public function setCompanyUpdates(UpdateCompanyRequest $request, $username) {
		$user = User::whereUsername($username)->firstOrFail();
		$user_differences = array_diff_assoc((array)$request->except(['_method', '_token', 'password', 'password_confirmation', 'username', 'employer', 'locale']), $user->toArray());
		$user_differences['employer'] = array_diff_assoc((array)$request->input('employer'), $user->employer->toArray());

		if (!$user_differences['employer']) {
			unset($user_differences['employer']);
		}
		if ($user->username_changed < 2 && $request->input('username') != $user->username) {
			$user->username = $request->input('username');
			$user->username_changed = $user->username_changed + 1;
			$user->save();
		}

		$this->savePasswordIfSet($request, $user);
		return $this->mergeUpdates($user, $user_differences);
	}

	public function setActorUpdates($request, $username) {
		$user = User::whereUsername($username)->firstOrFail();
		$differences = array_diff_assoc((array) $request->input('actor'), $user->actor->toArray());

		$ranges_differences = array_diff_assoc((array) $request->input('ranges'), $user->ranges->pluck('id')->toArray());
		$licences_differences = array_diff_assoc((array) $request->input('licences'), $user->licences->pluck('id')->toArray());
		$productions_differences = array_diff_assoc((array) $request->input('productions'), $user->productions->pluck('id')->toArray());

		if ($ranges_differences) {
			$differences['ranges'] = $ranges_differences;
		}

		if ($licences_differences) {
			$differences['licences'] = $licences_differences;
		}

		if ($productions_differences) {
			$differences['productions'] = $productions_differences;
		}

		if ($request->input('projects')) {
			foreach ($request->input('projects') as $project) {
				if (isset($project['id']) && $project['id']) {
					$query = Project::where('user_id', $id)->where('id', $project['id']);

					if ($query->first()) {
						$query->update($project);
					}
					else {
						$project          = new Project($project);
						$project->user_id = $id;
						$project->save();
					}
				}
				else {
					$project          = new Character($project);
					$project->user_id = $id;
					$project->save();
				}
			}
		}

		return $this->mergeUpdates($user, $differences);

	}

	public function mergeUpdates($user, $differences) {
		if ($differences) {
			$merge = array_merge((array) json_decode($user->updates), $differences);
			$user->updates = json_encode($merge);
		}
		$user->save();
		return $user;
	}

	public function mergeJobUpdates($job, $differences) {
		if ($differences) {
			$merge = array_merge((array) json_decode($job->updates), $differences);
			$job->updates = json_encode($merge);
		}
		$job->save();
		return $job;
	}

	/**
	 * Show admin and superadmin role only for superadmin
	 *
	 * @return static
	 */
	public function getRolesByYourRole() {
		if ($this->isSuperAdmin()) {
			return $this->listAllById('Role');
		}
		else {
			return $this->getAlLRolesExceptAdmins();
		}
	}

	/**
	 * @return bool
	 */
	public function isSuperAdmin() {
		return Auth::check() && Auth::user()->role_id == 2;
	}

	/**
	 * List all models' db entries
	 *
	 * @param        $class
	 *
	 * @param string $column1
	 * @param string $column2
	 * @param bool   $select
	 *
	 * @return mixed
	 */
	public function listAllById($class, $column1 = 'name', $column2 = 'id', $select = true) {

		if (($class == 'Rcc\Models\HairColour' || $class == 'Rcc\Models\EyeColour') && App::getLocale() == 'ro') {
			$column1 = 'nume';
		}
		$list = $class::all()->lists($column1, $column2);
		if ($select) {
			$list->prepend(trans('messages.select'));
		}

		return $list;
	}

	/**
	 * @return mixed
	 */
	public function getAlLRolesExceptAdmins() {
		return Role::whereBetween('id', [3, 4])->lists('name', 'id');
	}

	/**
	 * @param $name
	 *
	 * @return mixed
	 */
	public function getRoleId($name) {
		return Role::where('name', $name)->pluck('id');
	}

	/**
	 * @return mixed
	 */
	public function getJobSelects() {
		$data['productions'] = $this->listAllById(ActorType::class);
		$data['categories']  = $this->listAllById(ProductionType::class);

		return $data;
	}

	public function createOrUpdateJobFromRequest($request) {
		if (! $this->isAdminer()) {
			$job = Job::where('user_id', Auth::user('id')->id)->where('id', $request->input('id'))->first();
		}
		else {
			$job = Job::where('id', $request->input('id'))->first();
		}


		if ($job === null) {
			$job = new Job();
		}
		$fields =
			$request->except(['_token', '_method', 'category']) + ['production_type_id' => $request->get('category')];


		if ($job === null) {
			foreach ($fields as $field => $value) {
				if ($value !== null) {
					$job->{$field} = $value;
				}
			}
			if (! $this->isAdminer()) {
				$job->user_id     = Auth::user('id')->id;
				$job->employer_id = Auth::user('id')->employer->id;
			}
		}
		else {
			if ($fields['from_date_casting']) {
				$fields['from_date_casting'] = date('Y-m-d H:i:s', strtotime($fields['from_date_casting']));
			}
			if ($fields['to_date_casting']) {
				$fields['to_date_casting'] = date('Y-m-d H:i:s', strtotime($fields['to_date_casting']));
			}
			if ($fields['from_date_shooting']) {
				$fields['from_date_shooting'] = date('Y-m-d H:i:s', strtotime($fields['from_date_shooting']));
			}
			if ($fields['to_date_shooting']) {
				$fields['to_date_shooting'] = date('Y-m-d H:i:s', strtotime($fields['to_date_shooting']));
			}
			$differences = array_diff_assoc((array) $fields, $job->toArray());
			return $this->mergeJobUpdates($job, $differences);
		}

		$job->save();
		return $job;
	}

	public function updateJobCharacters($request) {
		$characters = $request->get('character');
		$job_id = $request->get('job_id');

		foreach ($characters as $character) {
			if (isset($character['id']) && $character['id']) {
				$query = Character::where('job_id', $job_id)->where('id', $character['id']);

				if ($query->first()) {
					$query->update($character);
				}
				else {
					$character          = new Character($character);
					$character->job_id = $job_id;
					$character->save();
				}
			}
			else {
				$character          = new Character($character);
				$character->job_id = $job_id;
				$character->save();
			}
		}
		return Job::findOrFail($request->get('job_id'));
	}

	public function removeCharacter($character_id, $job_id) {
		Character::whereRaw('id = ? and job_id = ?', [$character_id, $job_id])->delete();
	}

	public function jobComplete($request) {
		$job           = Job::findOrFail($request->input('id'));
		$job->complete = 1;
		$job->save();

		Mails::jobPostedOrUpdated($job);

		return $job;
	}

	public function getJob($id) {
		return Job::findOrFail($id);
	}

	/**
	 * @return array
	 */
	public function getTalentSelects() {
		$data                = [];
		$data['haircolours'] = $this->listAllById(HairColour::class);
		$data['eyecolours']  = $this->listAllById(EyeColour::class);
		$data['ranges']      = $this->listAllById(Range::class);

		return $data;
	}

	/**
	 * @param $username Username or ID
	 * @return mixed
	 *
	 */
	public function getUserWithRelations($username) {
		if (!$user = User::find($username)) {
			$user = User::whereUsername($username)->firstOrFail();
		}

		if ($user->role_id == $this->getRoleId('talent')) {
			$user->load('actor.actortype', 'actor.ethnicity', 'actor.eyecolour', 'actor.haircolour', 'country');
		}
		elseif ($user->role_id == $this->getRoleId('employer')) {
			$user->load('employer.employertype', 'country');
		}

		return $user;
	}

	/**
	 * @param $username
	 * @return mixed
	 * @internal param $id
	 *
	 */
	public function getUserWithRelationsByUsername($username) {
		if (!$user = User::find($username)) {
			$user = User::whereUsername($username)->firstOrFail();
		}

		if ($user->role_id == $this->getRoleId('talent')) {
			$user->load('actor.actortype', 'actor.ethnicity', 'actor.eyecolour', 'actor.haircolour', 'country');
		}
		elseif ($user->role_id == $this->getRoleId('employer')) {
			$user->load('employer.employertype', 'country');
		}

		return $user;
	}

	/**
	 * @return mixed
	 */
	public function getUnApprovedUsers() {
		return User::whereNotNull('updates')->get();
	}

	/**
	 * @return mixed
	 */
	public function getUnApprovedJobs() {
		return Job::whereNotNull('updates')->get();
	}

	/**
	 * @return bool
	 */
	public function isAdminer() {
		return Auth::check() && (Auth::user()->role_id == 1 || Auth::user()->role_id == 2);
	}

	/**
	 * @return bool
	 */
	public function isOwner($username) {
		return Auth::check() && (Auth::user()->username == $username);
	}

	public function isJobOwner($id) {
		$job = Job::findOrFail($id);

		return Auth::check() && (Auth::user()->id == $job->user_id);
	}

	/**
	 * @return bool
	 */
	public function isActor() {
		return Auth::check() && (Auth::user()->role_id == 3);
	}

	/**
	 * @return bool
	 */
	public function isEmployer() {
		return Auth::check() && (Auth::user()->role_id == 4);
	}

	/**
	 * @param AddUserRequest $request
	 *
	 * @return User
	 */
	public function createUserFromRequest(AddUserRequest $request) {
		$user           = new User($request->all());
		$user->active   = 1;
		$user->approved = 1;
		$user->password = 0;
		$user->save();

		$this->createUsername($user);

		if ($request->input('employer_type_id')) {
			$this->saveTalentOrEmployerByUser('Employer', $user->id);
		}
		elseif ($request->input('actor_type_id')) {
			$this->saveTalentOrEmployerByUser('Actor', $user->id);
		}

		return $user;
	}

	public function setPassAndSendMail($user) {
		$password       = $this->generateRandomPassword();
		$user->password = bcrypt($password);
		$user->save();
		Mails::accountCreated($user, $password);
	}

	private function generateRandomPassword($length = 18) {
		return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*()_+<>?,./-="), 0, $length);
	}

	/**
	 * @param $user
	 *
	 * @return string
	 */
	public function createUsername(User $user) {
		$user->username = $user->fname . '.' . $user->lname . $user->id;
		$user->save();
	}

	public function saveTalentOrEmployerByUser($modelName, $id) {
		$model = new $modelName;

		if ($this->getModelNameFromClass($model) == 'actor') {
			$model->ethnicity_id   = 144;
			$model->eye_colour_id  = 4;
			$model->hair_colour_id = 1;
		}
		$model->user_id = $id;
		$model->save();
	}

	/**
	 * @example getModelNameFromClass(Rcc\Models\ActorType) will return actorType
	 *
	 * @param $class
	 *
	 * @return mixed
	 */
	private function getModelNameFromClass($class) {
		$model = get_class($class);
		$name  = explode('\\', $model);

		return lcfirst(array_pop($name));
	}

	/**
	 * @param AddAmbassadorRequest $request
	 * @return Ambassador
	 */
	public function createAmbassadorFromRequest(AddAmbassadorRequest $request) {
		$ambassador = new Ambassador($request->all());
		$ambassador->save();
		$this->upload_files($ambassador, $request->file('image'));
		return $ambassador;
	}

	/**
	 * @param AddLunarRequest|AddPageRequest $request
	 * @return Page
	 */
	public function createPageFromRequest(AddPageRequest $request) {
		$fields = $request->except(['_token', '_method']);
		$page   = new Page($fields);
		$page->save();
		$page->languages()->attach(1, $fields);
		$this->upload_files($page, $request->file('image'));
		return $page;
	}

	/**
	 * @param AddLunarRequest $request
	 * @return Lunar
	 */
	public function createLunarFromRequest(AddLunarRequest $request) {
		$lunar       = new Lunar($request->all());
		$lunar->date = new Carbon($request->get('year') . '-' . $request->get('month'));
		$lunar->save();

		if ($request->input('actor_id')) {
			Mails::sendLunarEmail($lunar);
		}

		return $lunar;
	}

	/**
	 * @param AddLunarRequest $request
	 */
	public function updateLunarFromRequest($id, AddLunarRequest $request) {
		$lunar = Lunar::find($id);
		$lunar->update($request->all() + ['date' => new Carbon($request->get('year') . '-' . $request->get('month'))]);
		$lunar->save();

		if ($request->input('actor_id')) {
			Mails::sendLunarEmail($lunar);
		}

		return $lunar;
	}

	/**
	 * @param                                $id
	 * @param AddLunarRequest|AddPageRequest $request
	 * @param string $locale
	 * @return
	 */
	public function updatePageFromRequest($id, AddPageRequest $request, $locale = 'ro') {
		$page = Page::findOrFail($id);
		$languagePage   = $page->languages()->where('code', $locale)->first()->pivot;

		$fields = $request->except(['_token', '_method', 'image', 'lang', 'header', 'footer']);
		foreach ($fields as $field => $value) {
			if ($value !== null) {
				$languagePage->{$field} = $value;
			}
		}
		$languagePage->save();

		$page->header = (boolean) $request->get('header');
		$page->footer = (boolean) $request->get('footer');
		$page->save();

		if ($request->file('image')) {
			$this->tryDelete($page->image);
			$this->upload_files($page, $request->file('image'));
		}

		return $page;
	}

	public function findPageById($id, $locale) {
		$page = Page::select(
			[
				'language_page.*',
				'pages.*',
				'languages.code'
			]
			)
			->join('language_page', 'pages.id', '=', 'language_page.page_id')
			->join('languages', 'languages.id', '=', 'language_page.language_id' )
			->where('pages.id', $id)
			->where('code', $locale)
			->firstOrFail();
		return $page;
	}

	/**
	 * @param                      $id
	 * @param AddAmbassadorRequest $request
	 *
	 * @return mixed
	 */
	public function updateAmbassadorFromRequest($id, AddAmbassadorRequest $request) {

		$ambassador = Ambassador::find($id);
		$ambassador->update($request->except(['image']));

		if ($request->file('image')) {
			$this->tryDelete($ambassador->image);
			$this->upload_files($ambassador, $request->file('image'));
		}

		return $ambassador;
	}

    /**
     * @param AddSliderRequest $request
     * @return Slider $slider
     */
    public function createSliderFromRequest(AddSliderRequest $request) {
        $slider = new Slider($request->all());
        $slider->save();
        $this->upload_files($slider, $request->file('image'));
        return $slider;
    }

    /**
     * @param                      $id
     * @param AddSliderRequest $request
     * @return mixed
     */
    public function updateSliderFromRequest($id, AddSliderRequest $request) {
        $slider = Slider::find($id);
		$slider->update($request->except('image'));
		if ($request->file('image')) {
			$this->tryDelete($slider->image);
			$this->upload_files($slider, $request->file('image'));
		}

        return $slider;
    }

	public function tryDelete($image) {
		try {
			Storage::delete($image);
		}
		catch(\Exception $e) {}
	}

	/**
	 * @param                $id
	 * @param EditUserRequest $request
	 * @return mixed
	 */
	public function updateUserFromRequest($id, EditUserRequest $request) {
		$user = User::find($id);
		$user->update($request->all());

		if ($request->input('employer_type_id')) {
			$this->saveTalentOrEmployerByUser('Employer', $user->id);
		}
		elseif ($request->input('actor_type_id')) {
			$this->saveTalentOrEmployerByUser('Actor', $user->id);
		}

		return $user;
	}

	/**
	 * @param $model
	 * @param $file
	 */
	public function upload_files($model, $file) {
		if ($file && $file->isValid()) {
			$filename = $file->getClientOriginalName() . '-' . md5(uniqid()) . "." . $file->getClientOriginalExtension();
			$file->move(public_path('uploads'), $filename);
			$model->image = '/uploads/' . $filename;
			$model->update();
		}
	}

	/**
	 * If password empty, don't change it. Else encode it
	 *
	 * @param $request
	 * @param $user
	 */
	public function savePasswordIfSet($request, $user) {
		if ($request['password']) {
			$user->password = bcrypt($request['password']);
			$user->save();
		}
	}

	public function getLunarData() {
		$actors            = Actor::with('user')->get();
		$data['months'][0] = trans('messages.select');
		$data['actors'][0] = trans('messages.select_or');

		foreach ($actors as $actor) {
			$data['actors'][ $actor->id ] = $actor->user->fname . ' ' . $actor->user->lname;
		}
		for ($i = 1; $i <= 12; $i++) {
			$data['months'][ $i ] = trans('messages.' . date("F", mktime(0, 0, 0, $i, 10)));
		}
		$r                = range(2005, date('Y'));
		$data['years'][0] = trans('messages.select');
		$data['years'] += array_combine($r, $r);

		return $data;
	}

	/**
	 * @param User $user
	 * @param $projects
	 */
	public function updateProjects(User $user, $projects){
		foreach ($projects as $projectId => $project) {
			$query = Project::whereRaw('id = ? and user_id = ?', [$projectId, $user->id]);
			if ($query->first()) {
				$query->update($project);
			}
			else {
				$project          = new Project($project);
				$project->user_id = $user->id;
				$project->save();
			}
		}
	}

	public function castingsToExpire($starting, $ending){
		$users = User::where('users.active',1)->where('users.approved',1)->where('users.complete',1)->select(
			[
				'users.id',
				'users.email',

				'jobs.id as job_id',
				'jobs.production_name as job_production_name',
				'jobs.to_date_casting as job_to_date_casting',
			]
		)
			->join('jobs', function ($join) use ($starting, $ending) {
				$join->on('jobs.user_id', '=', 'users.id')
					->where('jobs.to_date_casting', '>',$starting)
					->where('jobs.to_date_casting', '<=',$ending);
			})
			->get();

		$usersArr = [];
		foreach ($users as $user) {
			$usersArr[$user->id]['email'] = $user->email;

			$usersArr[$user->id]['jobs'][$user->job_id]['production_name'] = $user->job_production_name;
		}
		return $usersArr;
	}

	public function expiredCastings(){

		$users = User::where('users.active',1)->where('users.approved',1)->where('users.complete',1)->select(
			[
				'users.id',
				'users.email',

				'jobs.id as job_id',
				'jobs.production_name as job_production_name',
				'jobs.to_date_casting as job_to_date_casting',
			]
		)
			->join('jobs', function ($join) {
				$join->on('jobs.user_id', '=', 'users.id')
					->where('jobs.to_date_casting', '<',Carbon::today())
					->where('jobs.to_date_casting', '>=',Carbon::yesterday());
			})
			->get();

		$usersArr = [];
		foreach ($users as $user) {
			$usersArr[$user->id]['email'] = $user->email;

			$usersArr[$user->id]['jobs'][$user->job_id]['production_name'] = $user->job_production_name;
		}
		return $usersArr;
	}

	public function newApplicants(){
		$dt = Carbon::yesterday();

		$users = User::where('users.active',1)->where('users.approved',1)->where('users.complete',1)->select(
			[
				'users.id',
				'users.email',

				'jobs.id as job_id',
				'jobs.production_name as job_production_name',

				'characters.title as character_title',
				'characters.id as character_id',

				'applicants.id as applicant_id',
				'applicants.username as applicant_username',
				'applicants.fname as applicant_fname',
				'applicants.lname as applicant_lname',
				'applicants.birth_date as applicant_birth_date',
				'applicants.city as applicant_city',
				'countries.name as applicant_country',
			]
		)
			->join('jobs', function ($join) use ($dt) {
				$join->on('jobs.user_id', '=', 'users.id');
			})
			->join('characters', function ($join) use ($dt) {
				$join->on('characters.job_id', '=', 'jobs.id');
			})
			->join('character_user', function ($join) use ($dt) {
			  $join->on('character_user.character_id', '=', 'characters.id')
			       ->where('character_user.date_applied', '>=',$dt->subDays(1)); // TODO: Change to $dt
			})
			->join('users as applicants', function ($join) use ($dt) {
			  $join->on('character_user.user_id', '=', 'applicants.id');
			})
			->join('countries', function ($join) use ($dt) {
			  $join->on('applicants.country_id', '=', 'countries.id');
			})
			->get();

		$usersArr = [];
		foreach ($users as $user) {
			$usersArr[$user->id]['email'] = $user->email;

			$usersArr[$user->id]['jobs'][$user->job_id]['production_name'] = $user->job_production_name;

			$usersArr[$user->id]['jobs'][$user->job_id]['characters'][$user->character_id]['character_title'] = $user->character_title;

			$usersArr[$user->id]['jobs'][$user->job_id]['characters'][$user->character_id]['applicants'][$user->applicant_username]['fname'] = $user->applicant_fname;
			$usersArr[$user->id]['jobs'][$user->job_id]['characters'][$user->character_id]['applicants'][$user->applicant_username]['lname'] = $user->applicant_lname;
			$usersArr[$user->id]['jobs'][$user->job_id]['characters'][$user->character_id]['applicants'][$user->applicant_username]['birth_date'] = $user->applicant_birth_date;
			$usersArr[$user->id]['jobs'][$user->job_id]['characters'][$user->character_id]['applicants'][$user->applicant_username]['city'] = $user->applicant_city;
			$usersArr[$user->id]['jobs'][$user->job_id]['characters'][$user->character_id]['applicants'][$user->applicant_username]['country'] = $user->applicant_country;
		}
		return $usersArr;
	}
}