<?php

namespace Rcc\Facades;

use Illuminate\Support\Facades\Facade;

class Rep extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() { return 'rep'; }
}