<?php

namespace Rcc\Console\Commands;

use Illuminate\Console\Command;
use Rep;
use Mails;
use Carbon\Carbon;

class CastingsToExpireMail extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'emails:casting:expiring';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Send email before an expiring casting with 24h/48h ahead';

	/**
	 * Create a new command instance.
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		// 24h
		$this->info('Getting castings to expire in 24h...');
		$users = Rep::castingsToExpire(Carbon::today(), Carbon::tomorrow());

		$this->info('Creating emails...');
		$bar = $this->output->createProgressBar(count($users));

		foreach ($users as $user) {
			Mails::castingsToExpire($user, 24);
			$bar->advance();
		}

		// 48h
		$this->info("\nGetting castings to expire in 48h...");
		$users = Rep::castingsToExpire(Carbon::tomorrow(), Carbon::tomorrow()->addDay());

		$this->info('Creating emails...');
		$bar = $this->output->createProgressBar(count($users));

		foreach ($users as $user) {
			Mails::castingsToExpire($user, 48);
			$bar->advance();
		}

		$this->info("\nDone.");
	}
}
