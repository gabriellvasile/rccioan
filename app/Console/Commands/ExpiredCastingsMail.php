<?php

namespace Rcc\Console\Commands;

use Illuminate\Console\Command;
use Rep;
use Mails;

class ExpiredCastingsMail extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'emails:casting:expired';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Send email once casting has expired';

	/**
	 * Create a new command instance.
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$this->info('Getting applicants...');
		$users = Rep::expiredCastings();

		$this->info('Creating emails...');
		$bar = $this->output->createProgressBar(count($users));

		foreach ($users as $user) {
			Mails::expiredCastings($user);
			$bar->advance();
		}
		$this->info("\nDone.");
	}
}
