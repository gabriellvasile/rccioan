<?php

namespace Rcc\Console\Commands;

use Illuminate\Console\Command;
use Rep;
use Mails;

class NewApplicantsEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'emails:applicants';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send email at the end of the day with new applicants to jobs foreach employer';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Getting applicants...');
        $users = Rep::newApplicants();

        $this->info('Creating emails...');
        $bar = $this->output->createProgressBar(count($users));

        foreach ($users as $user) {
            Mails::newApplicantsEmail($user);
            $bar->advance();
        }
        $this->info("\nDone.");
    }
}
