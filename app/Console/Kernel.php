<?php

namespace Rcc\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \Rcc\Console\Commands\Inspire::class,
        \Rcc\Console\Commands\NewApplicantsEmail::class,
        \Rcc\Console\Commands\CastingsToExpireMail::class,
        \Rcc\Console\Commands\ExpiredCastingsMail::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('emails:applicants')
                 ->everyMinute();
//                 ->sendOutputTo(storage_path() . "/logs/mail.recent");

        $schedule->command('emails:casting:expiring')
                 ->dailyAt('00:01');

        $schedule->command('emails:casting:expired')
                 ->dailyAt('23:00');
    }
}
