@extends('layouts.page')

@section('content')

	<div class="job" data-type="">
		<h2>{{ trans('messages.edit_profile') }} </h2>

		@include('errors._validation')

		<div class="la-anim-10"></div> {{-- Loading will be shown when an ajax request is made --}}

		<div role="tabpanel">
		
		  	<!-- Nav tabs -->
		    <ul id="jobTabsControllers" class="nav nav-tabs" role="tablist">
			    <li role="presentation" class="active"><a id="jobGeneralC" href="#jobGeneral" aria-controls="jobGeneral" role="tab">Informatii generale</a></li>
			    <li role="presentation"><a id="jobActorC" href="#jobActor" aria-controls="jobActor" role="tab">Informatii personaj</a></li>
		    </ul> 

		    {!! Form::model($job, ['route' => ['job.update', $job->id]]) !!}
		
				@include('job._general')
		  			
			{!! Form::close() !!}

		</div>
	</div>

@stop

@section('afterfooter')
	<script src="{{ asset('/js/job.js') }}"></script>
	<script>
	jQuery(document).ready(function($) {
		function evaluate(){
		    var relatedItem = $('#studentInfo');

		    if($(this).attr('id') == 'student_yes'){
		        relatedItem.fadeIn();
		    }else{
		        relatedItem.fadeOut();   
		    }
		}

		$('input[name="student_project"]').click(evaluate);

		if( $('#student_yes').is(':checked') )
		{
			$('#studentInfo').fadeIn();
		}
	});
		
	</script>


@stop