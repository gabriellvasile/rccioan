<div class="well character" data-id="{{ $key }}">
    <div class="btn btn-danger deleteFromProfile" title="{{ trans('messages.delete_character') }}">-</div>

    <h2>{{ trans('messages.character') }} {{ $key }}</h2>

    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                {!! Form::label('character['.($key).'][title]', trans('messages.title')) !!}
                {!! Form::text('character['.($key).'][title]', null, ['class' => 'form-control']) !!}
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                {!! Form::label('character['.($key).'][range_id]', trans('messages.range')) !!}
                {!! Form::select('character['.($key).'][range_id]', $ranges, null, ['class' => 'form-control']) !!}
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                {!! Form::label('character['.($key).'][sex]', trans('messages.sex')) !!}
                {!! Form::select('character['.($key).'][sex]', [trans('messages.select'), 'M' => 'M', 'F' => 'F'], null, ['class' => 'form-control']) !!}
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('character['.($key).'][hair_colour_id]', trans('messages.hair_colour')) !!}
                {!! Form::select('character['.($key).'][hair_colour_id]', $haircolours, null, ['class' => 'form-control']) !!}
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('character['.($key).'][eye_colour_id]', trans('messages.eye_colour')) !!}
                {!! Form::select('character['.($key).'][eye_colour_id]', $eyecolours, null, ['class' => 'form-control']) !!}
            </div>
        </div>
    </div>

    <div class="jobFormTitle">{{ trans('messages.height') }}</div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('character['.($key).'][from_height]', trans('messages.from')) !!}
                {!! Form::text('character['.($key).'][from_height]', null, ['class' => 'form-control']) !!}
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('character['.($key).'][to_height]', trans('messages.to')) !!}
                {!! Form::text('character['.($key).'][to_height]', null, ['class' => 'form-control']) !!}
            </div>
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('character['.($key).'][extra_details]', trans('messages.other_info'), ['class' => 'jobFormTitle']) !!}
        {!! Form::textarea('character['.($key).'][extra_details]', null, ['class' => 'form-control', 'placeholder' => trans('messages.other_info')]) !!}
    </div>
</div>

