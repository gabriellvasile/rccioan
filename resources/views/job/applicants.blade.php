
@extends('layouts.page')

@section('content')

	<div class="jobsWrap">
		<h1>{{ trans('messages.applicants') }}</h1>
	</div>
	
	@if ($characters)
		@foreach ($characters as $character)
			@if (!$character->users->isEmpty())
				<h3>{{ $character->title }}</h3>
				<div class="table-responsive">
					<table class="table table-hover">
						<tr>
							<th>{{ trans('messages.name') }}</th>
							<th>{{ trans('messages.birth_date') }}</th>
							<th>{{ trans('messages.city') }}</th>
							<th>{{ trans('messages.actions') }}</th>
						</tr>
						@foreach ($character->users as $user)
							<tr>
								<td><a href="{{ route('profile.show', [$user->username, App::getLocale()]) }}">{{ $user->fname . ' ' . $user->lname }}</a></td>
								<td>{{ $user->birth_date }}</td>
								<td>{{ $user->city }} - {{ $user->country->name }}</td>
								<td class="actions" width="350">
									@if($user->pivot->is_chosen)
										{!! link_to_route('casts.reject', trans('messages.reject'), [$character->id, $user->id], ['class' => 'btn applicantsBtn']) !!}
									@elseif($user->pivot->is_declined_employer)
										{!! link_to_route('casts.approve', trans('messages.approve'), [$character->id, $user->id], ['class' => 'btn applicantsBtn']) !!}
									@else
										{!! link_to_route('casts.approve', trans('messages.approve'), [$character->id, $user->id], ['class' => 'btn applicantsBtn']) !!}
										{!! link_to_route('casts.reject', trans('messages.reject'), [$character->id, $user->id], ['class' => 'btn applicantsBtn']) !!}
									@endif

									@if($user->pivot->is_shortlist)
										{!! link_to_route('casts.shortlist', trans('messages.remove_from_shortlist'), [$character->id, $user->id], ['class' => 'btn applicantsBtn']) !!}
									@else
										{!! link_to_route('casts.shortlist', trans('messages.add_to_shortlist'), [$character->id, $user->id], ['class' => 'btn applicantsBtn']) !!}
									@endif
								</td>
							</tr>
						@endforeach
					</table>
				</div>
			@endif
		@endforeach
	@endif

@endsection