<div role="tabpanel" class="tab-pane fade in active" id="jobGeneral">

    @if(isset($job))
        {!! Form::model( $job, ['route' => ['jobs.ajaxCreateJob'], 'method' => 'PATCH', 'id' => 'jobform', 'data-show' => route('jobs.ajaxShowTalent')] ) !!}
        {!! Form::hidden('id', $job->id, [ 'id' => 'id' ]) !!}
    @else
	    {!! Form::open( ['route' => ['jobs.ajaxCreateJob'], 'method' => 'PATCH', 'id' => 'jobform', 'data-show' => route('jobs.ajaxShowTalent')] ) !!}
		{!! Form::hidden('id', Request::input('id'), [ 'id' => 'id' ]) !!}
    @endif

		<div class="form-group form-group-smaller">
			{!! Form::text('production_name', null , ['placeholder' => trans('messages.production_name'), 'class' => 'form-control form-control-simple', 'id' => 'production_name']) !!}
		</div>

		<div class="form-group form-group-smaller">
			{!! Form::text('location', null , ['placeholder' => trans('messages.location'), 'class' => 'form-control form-control-simple', 'id' => 'location']) !!}
		</div>

		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					{!! Form::label('actor_type_id', trans('messages.category'), ['class' => 'ttu']) !!}
					{!! Form::select('actor_type_id', $productions, null, ['class' => 'form-control', 'id' => 'actor_type_id']) !!}
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					{!! Form::label('production_type_id', trans('messages.production_type'), ['class' => 'ttu']) !!}
					{!! Form::select('production_type_id', $categories, null, ['class' => 'form-control', 'id' => 'production_type_id']) !!}
				</div>
			</div>
		</div>

		<div class="jobFormTitle">{{ trans('messages.shooting_data') }}</div>

		<div class="row">
			<div class="col-md-6">
				<div class="form-group jobdatefgroup">
					{!! Form::label('from_date_shooting', trans('messages.starting'), ['class' => 'jobdatelabel']) !!}
					{!! Form::text('from_date_shooting', isset($job) ? $job->from_date_shooting->toDateString() : null, ['class' => 'date-input form-control jobdate']) !!}
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group jobdatefgroup">
					{!! Form::label('to_date_shooting', trans('messages.ending'), ['class' => 'jobdatelabel']) !!}
					{!! Form::text('to_date_shooting', isset($job) ? $job->to_date_shooting->toDateString() : null, ['class' => 'date-input form-control jobdate']) !!}
				</div>
			</div>
		</div>

		<div class="jobFormTitle">{{ trans('messages.casting_data') }}</div>

		<div class="row">
			<div class="col-md-6">
				<div class="form-group jobdatefgroup">
					{!! Form::label('from_date_casting', trans('messages.starting'), ['class' => 'jobdatelabel']) !!}
					{!! Form::text('from_date_casting', isset($job) ? $job->from_date_casting->toDateString() : null, ['class' => 'date-input form-control jobdate']) !!}
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group jobdatefgroup">
					{!! Form::label('to_date_casting', trans('messages.ending'), ['class' => 'jobdatelabel']) !!}
					{!! Form::text('to_date_casting', isset($job) ? $job->to_date_casting->toDateString() : null, ['class' => 'date-input form-control jobdate']) !!}
				</div>
			</div>
		</div>

		<div>
			<div class="form-group form-group-radio">
				<div class="text-center" id="student_project">
					<div class="jobFormTitle">{{ trans('messages.student_project') }}</div>
					{!! Form::radio('student_project', 1 , null, ['id' => 'student_yes']) !!}
					{!! Form::label('student_yes', trans('messages.yes')) !!}
					{!! Form::radio('student_project', 0 , null, ['id' => 'student_no']) !!}
					{!! Form::label('student_no', trans('messages.no')) !!}
				</div>
			</div>
		</div>

		<div class="row" id="studentInfo">
			<div class="col-md-6">
				<div class="form-group">
					{!! Form::label('university', trans('messages.faculty')) !!}
					{!! Form::text('university', null , ['placeholder' => trans('messages.faculty'), 'class' => 'form-control']) !!}
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					{!! Form::label('year', trans('messages.year')) !!}
					{!! Form::text('year', null , ['placeholder' => trans('messages.year'), 'class' => 'form-control']) !!}
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					{!! Form::label('tutor_name', trans('messages.professor')) !!}
					{!! Form::text('tutor_name', null , ['placeholder' => trans('messages.professor'), 'class' => 'form-control']) !!}
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					{!! Form::label('department', trans('messages.department')) !!}
					{!! Form::text('department', null , ['placeholder' => trans('messages.department'), 'class' => 'form-control']) !!}
				</div>
			</div>
		</div>

		<div class="form-group form-group-radio">
			<div class="text-center" id="payment">
				<div class="jobFormTitle">{{ trans('messages.fee') }}</div>
				{!! Form::radio('payment', 1 , null, ['id' => 'payment_yes']) !!}
				{!! Form::label('payment_yes', trans('messages.yes')) !!}
				{!! Form::radio('payment', 0 , null, ['id' => 'payment_no']) !!}
				{!! Form::label('payment_no', trans('messages.no')) !!}
			</div>
		</div>


		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					{!! Form::label('email', trans('messages.email'), ['class' => 'ttu']) !!}
					{!! Form::text('email', null , ['placeholder' => trans('messages.email'), 'class' => 'form-control']) !!}
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					{!! Form::label('telephone', trans('messages.phone'), ['class' => 'ttu']) !!}
					{!! Form::text('telephone', null , ['placeholder' => trans('messages.phone'), 'class' => 'form-control']) !!}
				</div>
			</div>
		</div>

		<div class="form-group">
			<div class="text-center">
				{!! Form::checkbox('exclusive', 1, null, ['id' => 'exclusive']) !!}
				{!! Form::label('exclusive', trans('messages.exclusive_job')) !!}
			</div>
		</div>
		<div class="form-group">
			<div class="text-center">
				{!! Form::checkbox('guest_visible', 1, null, ['id' => 'guest_visible']) !!}
				{!! Form::label('guest_visible', trans('messages.job_visible_for_guests')) !!}
			</div>
		</div>

		<div class="form-group form-group-select-smaller">
			{!! Form::label('notifying_method', trans('messages.notifying_method'), ['class' => 'jobFormTitle']) !!}
			{!! Form::select('notifying_method', ['0' => trans('messages.inbox'), '1' => trans('messages.inbox_and_email')], null, ['class' => 'form-control']) !!}
		</div>

		<div class="form-group">
			{!! Form::label('short_description', trans('messages.description'), ['class' => 'jobFormTitle']) !!}
			{!! Form::textarea('short_description', null , ['class' => 'form-control']) !!}
		</div>

		{!! Form::button(trans('messages.next'), ['class' => 'btn btn-primary profilesubmit', 'data-nexttab' => 'jobActor']) !!}
	{!! Form::close() !!}

</div>