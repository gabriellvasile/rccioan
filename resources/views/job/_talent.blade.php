<div role="tabpanel" class="tab-pane fade in" id="jobActor">

    {!! Form::model($job, ['route' => ['jobs.ajaxUpdateJobTalent'], 'method' => 'PATCH', 'id' => 'jobtalentform', 'data-show' => route('jobs.ajaxComplete')] ) !!}

        {!! Form::hidden('job_id', $job->id) !!}

        {{-- TODO: Change status !!! --}}
        {!! Form::hidden('status', 1) !!}

        <div class="editable" data-type="character" data-remove="{{ route('jobs.removeCharacter', $job->id) }}">
            @foreach ($job->characters as $key => $character)
                <div class="well character">
                    <div class="btn btn-danger deleteFromProfile" data-id="{{ $character->id }}">-</div>
                    <h2>{{ trans('messages.character') }} {{ $key + 1 }}</h2>
                    {!! Form::hidden('character['.($key + 1).'][id]', $character->id) !!}

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                {!! Form::label('character['.($key + 1).'][title]', trans('messages.title')) !!}
                                {!! Form::text('character['.($key + 1).'][title]', $character->title, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                {!! Form::label('character['.($key + 1).'][range_id]', trans('messages.range')) !!}
                                {!! Form::select('character['.($key + 1).'][range_id]', $ranges, $character->range_id, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                {!! Form::label('character['.($key + 1).'][sex]', trans('messages.sex')) !!}
                                {!! Form::select('character['.($key + 1).'][sex]', [trans('messages.select'), 'M' => 'M', 'F' => 'F'], $character->sex, ['class' => 'form-control']) !!}
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('character['.($key + 1).'][hair_colour_id]', trans('messages.hair_colour')) !!}
                                {!! Form::select('character['.($key + 1).'][hair_colour_id]', $haircolours, $character->hair_colour_id, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('character['.($key + 1).'][eye_colour_id]', trans('messages.eye_colour')) !!}
                                {!! Form::select('character['.($key + 1).'][eye_colour_id]', $eyecolours, $character->eye_colour_id, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                    </div>

                    <div class="jobFormTitle">{{ trans('messages.height') }}</div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('character['.($key + 1).'][from_height]', trans('messages.from')) !!}
                                {!! Form::text('character['.($key + 1).'][from_height]', $character->from_height, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('character['.($key + 1).'][to_height]', trans('messages.to')) !!}
                                {!! Form::text('character['.($key + 1).'][to_height]', $character->to_height, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('character['.($key + 1).'][extra_details]', trans('messages.other_info'), ['class' => 'jobFormTitle']) !!}
                        {!! Form::textarea('character['.($key + 1).'][extra_details]', $character->extra_details, ['class' => 'form-control', 'placeholder' => trans('messages.other_info')]) !!}
                    </div>
                </div>
            @endforeach
        </div>

        <legend class="clearfix legendWithBtn">
            <div class="btn btn-success pull-right addToProfile" data-url="{{ route('jobs.ajaxNewCharacter', $job->id) }}">+ {{ trans('messages.new_character') }} </div>
        </legend>

        {!! Form::button(trans('messages.publish'), ['class' => 'btn btn-primary profilesubmit', 'data-nexttab' => 'none']) !!}
        {!! Form::button(trans('messages.back'), ['class' => 'btn btn-primary profilesubmit', 'data-nexttab' => 'jobGeneral']) !!}
    {!! Form::close() !!}
</div>