
@extends('layouts.page')

@section('content')

	<h2 class="text-center">{{ trans('messages.job_details') }}</h2>
	<hr>
	<div class="jobPage">
		<div role="tabpanel">

			<!-- Nav tabs -->
			<ul class="nav nav-tabs" role="tablist" id="jobTabsControllers">
				<li role="presentation" class="active"><a href="#jobGeneral" aria-controls="jobGeneral" role="tab" data-toggle="tab">{{ trans('messages.general_info') }}</a></li>
				@foreach($job->characters as $character)
					<li role="presentation"><a href="#jobActor-{{ $character->id }}" aria-controls="jobActor" role="tab" data-toggle="tab">{{ $character->title }}</a></li>
				@endforeach
			</ul>
			<div class="text-center">
				<h1>{{ (isset($updates->production_name) ? $updates->production_name : $job->production_name) }}</h1>
				<h4 class="jobProductions">
					{{ (isset($updates->actor_type_id) ? $productions[$updates->actor_type_id] : $job->ActorType->name) }} |
					{{ (isset($updates->production_type_id) ? $categories[$updates->production_type_id] : $job->ProductionType->name) }} |
					{{ (isset($updates->location) ? $updates->location : $job->location) }}
				</h4>
			</div>

			<!-- Tab panes -->
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane fade in active" id="jobGeneral">
					<div class="row generalDetails">
						<div class="col-sm-1 dateRow">
							<div class="day">{{ (isset($updates->from_date_casting->day) ? $updates->from_date_casting->day : $job->from_date_casting->day) }}</div>
							<div class="month">@include('includes._month', ['month' => (isset($updates->from_date_casting->month) ? $updates->from_date_casting->month : $job->from_date_casting->month), 'format' => 'M'])</div>
							<div class="year">{{ (isset($updates->from_date_casting->year) ? $updates->from_date_casting->year : $job->from_date_casting->year) }}</div>
						</div>
						<div class="col-sm-3">
							
							<div class="title">{{ trans('messages.fee') }}:</div>
							<div class="ceare">{{ trans('messages.' . (isset($updates->payment) ? $updates->payment : $job->payment)) }}</div>

							<div class="title">{{ trans('messages.student_project') }}:</div>
							<div class="ceare">{{ trans('messages.' . (isset($updates->student_project) ? $updates->student_project : $job->student_project)) }}</div>

						</div>
						<div class="col-sm-4">
							@if(Auth::check())
								<div class="title">{{ trans('messages.phone') }}</div>
								<div class="ceare">{{ (isset($updates->telephone) ? $updates->telephone : $job->telephone) }}</div>
								<div class="title">{{ trans('messages.email') }}</div>
								<div class="ceare">{{ (isset($updates->email) ? $updates->email : $job->email) }}</div>
							@endif
						</div>
						<div class="col-sm-4">
							<div class="title">{{ trans('messages.shooting_data') }}:</div>

							<div class="ceare">
								{{ (isset($updates->from_date_shooting->day) ? $updates->from_date_shooting->day : $job->from_date_shooting->day) }}
								@include('includes._month', ['month' => (isset($updates->from_date_shooting->month) ? $updates->from_date_shooting->month : $job->from_date_shooting->month), 'format' => 'M'])
								{{ (isset($updates->from_date_shooting->year) ? $updates->from_date_shooting->year : $job->from_date_shooting->year) }} -


								{{ (isset($updates->to_date_shooting->day) ? $updates->to_date_shooting->day : $job->to_date_shooting->day) }}
								@include('includes._month', ['month' => (isset($updates->to_date_shooting->month) ? $updates->to_date_shooting->month : $job->to_date_shooting->month), 'format' => 'M'])
								{{ (isset($updates->to_date_shooting->year) ? $updates->to_date_shooting->year : $job->to_date_shooting->year) }}
							</div>
							
							<div class="title">{{ trans('messages.casting_data') }}:</div>
							<div class="ceare">
								{{ (isset($updates->from_date_casting->day) ? $updates->from_date_casting->day : $job->from_date_casting->day) }}
								@include('includes._month', ['month' => (isset($updates->from_date_casting->month) ? $updates->from_date_casting->month : $job->from_date_casting->month), 'format' => 'M'])
								{{ (isset($updates->from_date_casting->year) ? $updates->from_date_casting->year : $job->from_date_casting->year) }} -


								{{ (isset($updates->to_date_casting->day) ? $updates->to_date_casting->day : $job->to_date_casting->day) }}
								@include('includes._month', ['month' => (isset($updates->to_date_casting->month) ? $updates->to_date_casting->month : $job->to_date_casting->month), 'format' => 'M'])
								{{ (isset($updates->to_date_casting->year) ? $updates->to_date_casting->year : $job->to_date_casting->year) }}
							</div>
						</div>
					</div>

					<div class="anotherTitle">{{ trans('messages.description') }}</div>
					<div class="description">
						{{ (isset($updates->short_description) ? $updates->short_description : $job->short_description) }}
					</div>

				</div>

				@foreach($job->characters as $character)
					<div role="tabpanel" class="tab-pane fade in" id="jobActor-{{ $character->id }}">
						<div class="row talentDetails">
							<div class="col-sm-3">
								<div class="ceare">
									{{ trans('messages.sex' . $character->sex) }}
									| {{ $character->range->name }} {{ trans('messages.years') }}
								</div>

								<div class="title">{{ trans('messages.height') }}:</div>
								<div class="ceare">{{ $character->from_height }} cm - {{ $character->to_height }} cm</div>

								<div class="title">{{ trans('messages.eye_colour') }}:</div>
								<div class="ceare">{{ $character->EyeColour->name }}</div>

								<div class="title">{{ trans('messages.hair_colour') }}:</div>
								<div class="ceare">{{ $character->HairColour->name }}</div>
							</div>
							<div class="col-sm-9">
								<div class="anotherTitle">{{ trans('messages.other_info') }}</div>
								<div class="description">{{ $character->extra_details }}</div>
							</div>
						</div>

						@if($joinJob)
							<div class="applyToJob">
								@if ($character->applied())
									{{ trans('messages.character_applied') }}
								@else
									{!! Form::open( ['route' => ['jobs.apply', $character->id, App::getLocale()]] ) !!}
									{!! Form::button( trans('messages.character_apply'), ['class' => 'btn', 'type' => 'submit'] ) !!}
									{!! Form::close() !!}
								@endif
							</div>
						@endif
					</div>
				@endforeach
			</div>

			@if (isset($admin) && $admin)
				@if (isset($updates) && $updates)
					<div class="text-center">
						<br>
						<a href="{{ route('admin.job.approve', $job->id) }}" class="btn">{{ trans('messages.approve_complete') }}</a>
						<a href="{{ route('admin.job.uncomplete', $job->id) }}" class="btn">{{ trans('messages.uncomplete') }}</a>
					</div>
				@endif
			@endif
		</div>

	</div>
	
@endsection