
@extends('layouts.page')

@section('content')

	<div class="jobsWrap">
		<h1>Castingurile Mele</h1>
		<hr>
		<p>România Casting Call este unica platformă de casting din România, care acoperă multiple profesii din industria media, oferind o paleta largă de job-uri.</p>
	</div>

	{!! link_to_route('jobs.create', 'Add job', [], ['class' => 'btn']) !!}
	
	@if ($jobs)
		<div class="table-responsive jobsList">
			<table class="table table-hover">
				<tr>
					<th>{{ trans('messages.production_name') }}</th>
					{{--<th>Tipul productiei</th>--}}
					<th>{{ trans('messages.added_date') }}</th>
					<th>{{ trans('messages.expiration_date') }}</th>
					<th>{{ trans('messages.location') }}</th>
					<th>Status</th>
					<th>Actiuni</th>
				</tr>

				@foreach ($jobs as $job)
					<tr>
						<td><a href="{{ route('jobs.show', $job->id) }}">{{ $job->production_name }}</a></td>
						{{--<td>{{ $job->ProductionType->name }}</td>--}}
						<td>
							<a href="{{ route('jobs.show', $job->id) }}">
								{{ $job->from_date_casting->day }}
								@include('includes._month', ['month' => $job->from_date_casting->month, 'format' => 'M'])
								{{ $job->from_date_casting->year }}
							</a>
						</td>
						<td>
							<a href="{{ route('jobs.show', $job->id) }}">
								{{ $job->to_date_casting->day }}
								@include('includes._month', ['month' => $job->to_date_casting->month, 'format' => 'M'])
								{{ $job->to_date_casting->year }}
							</a>
						</td>
						<td><a href="{{ route('jobs.show', $job->id) }}">{{ $job->location }}</a></td>
						<td>
							<a href="{{ route('jobs.show', $job->id) }}">
								@unless( $job->status )
									Inactiv
								@else
									Activ
								@endunless
							</a>
						</td>
						<td>
							{{--{!! link_to_route('job.applicants', 'Applicants', $job->id, ['class' => 'btn']) !!}--}}
							{{--{!! link_to_route('job.show.edit', 'Edit', $job->id, ['class' => 'btn']) !!}--}}

							@unless( $job->status )
								<button class="btn changeStatus" data-tostatus="1" data-jobid="{{ $job->id }}">Activare</button>
							@else
								<button class="btn changeStatus" data-tostatus="0" data-jobid="{{ $job->id }}">Dezactivare</button>
							@endunless
						</td>
					</tr>
				@endforeach
			</table>
		</div>
	@endif

@endsection

@section('afterfooter')
	<script>

	$('.changeStatus').click(function(event) {
		var jobId = $(this).data('jobid');
		var toStatus = $(this).data('tostatus');

		$.ajax({
			headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
			url: '{{ route("jobs.status") }}',
			type: 'POST',
			data: {jobId: jobId, toStatus: toStatus},
		})
		.done(function(data) {
			if(data.success)
				location.reload();
		})
		.fail(function() {
			alert('Error to change state of your article. Please contact the administrator.');
		});
	});

	</script>
@endsection