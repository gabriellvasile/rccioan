
@extends('layouts.page')

@section('content')

	<div class="jobsWrap">
		@if(isset($mine))
			<h1>{{ trans('messages.my_castings') }}</h1>
		@else
			<h1>{!! $page->title !!}</h1>
			<hr>
			{!! $page->content !!}
		@endif
	</div>
	
	<div class="jobsList">
		@if ($jobs)
			<div class="table-responsive">
				<table class="table table-hover">
					<tr>
						<th>{{ trans('messages.production_name') }}</th>
						<th>{{ trans('messages.added_date') }}</th>
						<th>{{ trans('messages.expiration_date') }}</th>
						<th>{{ trans('messages.location') }}</th>
						@if(isset($mine))
							<th>{{ trans('messages.status') }}</th>
							<th>{{ trans('messages.actions') }}</th>
						@endif
					</tr>

					@foreach ($jobs as $job)
						<tr>
							<td><a href="{{ route('jobs.show', [$job->id, App::getLocale()]) }}">{{ $job->production_name }}</a></td>
							<td>
								<a href="{{ route('jobs.show', [$job->id, App::getLocale()]) }}">
									{{ $job->from_date_casting->day }}
									@include('includes._month', ['month' => $job->from_date_casting->month])
									{{ $job->from_date_casting->year }}
								</a>
							</td>
							<td>
								<a href="{{ route('jobs.show', [$job->id, App::getLocale()]) }}">
									{{ $job->to_date_casting->day }}
									@include('includes._month', ['month' => $job->to_date_casting->month])
									{{ $job->to_date_casting->year }}
								</a>
							</td>
							<td><a href="{{ route('jobs.show', [$job->id, App::getLocale()]) }}">{{ $job->location }}</a></td>
							@if(isset($mine))
								<td>
									<a href="{{ route('jobs.show', $job->id) }}">
										{{ trans('messages.status' . $job->status) }}
									</a>
								</td>
								<td class="actions">
									<div class="btn jobBtn"><a href="{{ route('jobs.show', [$job->id, App::getLocale()	]) }}">{{ trans('messages.view_job') }}</a></div>
									<div class="btn jobBtn"><a href="{{ route('jobs.applicants', [$job->id, App::getLocale()	]) }}">{{ trans('messages.view_applicants') }}</a></div>
									<div class="btn jobBtn"><a href="{{ route('jobs.edit', [$job->id, App::getLocale()	]) }}">{{ trans('messages.edit') }}</a></div>
									<div class="btn jobBtn changeJobStatus">
										{!! Form::open( ['route' => ['jobs.status', $job->id, 1], 'method' => 'patch'] ) !!}
											{!! Form::submit(trans('messages.activate') ) !!}
										{!! Form::close() !!}
									</div>
									<div class="btn jobBtn">
										{!! Form::open( ['route' => ['jobs.status', $job->id, 0], 'method' => 'patch'] ) !!}
											{!! Form::submit(trans('messages.deactivate') ) !!}
										{!! Form::close() !!}
									</div>
									<div class="btn jobBtn">
										{!! Form::open( ['route' => ['jobs.destroy', $job->id], 'method' => 'delete'] ) !!}
											{!! Form::submit( trans('messages.delete') ) !!}
										{!! Form::close() !!}
									</div>
								</td>
							@endif
						</tr>
					@endforeach
				</table>
			</div>
		@endif
	</div>

	@include('pagination.default', ['paginator' => $jobs])

@endsection