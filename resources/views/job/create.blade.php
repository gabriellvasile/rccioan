@extends('layouts.page')

@section('content')

	<div class="addJob">

		@if(isset($job))
			<h1>{{ trans('messages.edit_job') }} </h1>
		@else
			<h1>{{ trans('messages.new_job') }} </h1>
		@endif

		@include('errors._validation')

		<div class="la-anim-10"></div>

		<div role="tabpanel">
		
		  	<!-- Nav tabs -->
		    <ul id="jobTabsControllers" class="nav nav-tabs" role="tablist">
			    <li role="presentation" class="active"><a id="jobGeneralC" href="#jobGeneral" aria-controls="jobGeneral" role="tab">{{ trans('messages.general_info') }}</a></li>
			    <li role="presentation"><a id="jobActorC" href="#jobActor" aria-controls="jobActor" role="tab">{{ trans('messages.talent_info') }}</a></li>
		    </ul>

			<!-- Tab panes -->
			<div class="tab-content">
				@include('job._general')
			</div>
		</div>
	</div>

	@include('includes._success', ['stitle' => trans('messages.thank_you_for_creating_job'), 'sdescription' => trans('messages.job_check')])

@stop

@section('afterfooter')
	<script src="{{ asset('/js/job.js') }}"></script>
	<script src="{{ asset('/js/profile.js') }}"></script>
@stop