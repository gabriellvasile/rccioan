
@extends('layouts.page')

@section('content')

	<div class="aboutCastingCall">
		<h1>{!! $page->content !!}</h1>
		<hr>
		{!! $page->excerpt !!}

		<h3>Astfel, categoriile acoperite de RCC sunt:</h3>

		<div class="circles">
			<div class="circleRow">
				<div class="circle"><div class="circleInner">Actor</div></div>
				<div class="circle"><div class="circleInner">Actor Amator</div></div>
				<div class="circle"><div class="circleInner">Fotograf</div></div>
				<div class="circle"><div class="circleInner">Voice over</div></div>
				<div class="circle"><div class="circleInner">Dansator</div></div>
				<div class="circle"><div class="circleInner">Echipa de filmare</div></div>
				<div class="circle"><div class="circleInner">Copii</div></div>
				<div class="circle"><div class="circleInner">Alte talente</div></div>
			</div>
		</div>

		<h3>Acestea pot fi gasite si in sectiunea Categorii, care poate fi gasita pe prima pagina.</h3>

	</div>
	
	<div class="row afterCircles">
		<div class="col-sm-6">
			<p>Pentru a putea beneficia de serviciile platformei, crearea unui cont este obligatorie, atat pentru angajator, cat si pentru prestator. Pentru angajatori (ex: director casting, producator etc), crearea unui cont este gratuita, restul profilelor oferind niste variante de plata. Acestea se pot gasi in ultimul pas al configurarii profilului sau pe pagina de servicii.</p>
			
			<p>"Am creat această platformă pentru că mi se părea esențială și îmi lipsea foarte tare. Pe vremea când eram la Londra, foloseam plaftorme de casting.</p>
		</div>
		<div class="col-sm-6">
			<p>pentru orice proiect aveam și niciodată nu m-au dezamăgit. O platforma creată inteligent și modern, cu metode bine gândite de căutare, poate scuti multe case de producție și agenții de casting de timp și bani." - Laura Musat, fondator. </p>

			<p>Dacă doriți informații suplimentare, va rugăm să accesați pagina de servicii. Pentru întrebări, advertising sau parteneriate, ne puteți găsi pe pagina de contact.</p>
		</div>
	</div>

@endsection