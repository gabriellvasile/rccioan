
@extends('layouts.special')

@section('content')

	<div class="text-center">
		<h1 class="titluPeSlider">{!! $page->title !!}</h1>

		<div class="circles">
			<div class="circleRow">
				@foreach($actor_types as $actor_type)
					<a href="{{ route( Route::getCurrentRoute()->getUri() . '.list', [strtolower(trans('messages.actor_type_' . $actor_type->slug))]) }}" class="circle">
						<div class="circleInner">{{ trans('messages.actor_type_' . $actor_type->slug) }}</div>
					</a>
				@endforeach
				<a href="{{ route(Route::getCurrentRoute()->getUri() . '.lunars') }}" class="circle">
					<div class="circleInner">{{ trans('messages.lunar_title') }}</div>
				</a>
			</div>
		</div>

		<div class="actorAlLunii">
			<div class="lunar nobd lunarOne">
				@if (isset($current_lunar->actor->user))
					<h3><span>{{ $current_lunar->actor->user->fname }} {{ $current_lunar->actor->user->lname }}</span></h3>
				@else
					<h3><span>{{ $current_lunar->name }}</span></h3>
				@endif
				<div class="video" data-src="{{ $current_lunar->video }}"></div>
			</div>
			<h2 class="actLunaTitle">{{ trans('messages.actor_month') }}</h2>
			<div class="actLunaDesc">{{ $current_lunar->description }}</div>
		</div>
	</div>

@endsection