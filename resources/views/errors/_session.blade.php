@if (session('success_message') || session('status') || session('error_message') || session('warning_message'))

	<div class="modal fade" id="sessionModal" tabindex="-1" role="dialog" aria-labelledby="sessionModal">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">
						@if (session('success_message'))
							{{ trans('messages.success') }}
						@endif

						@if (session('status'))
								{{ session('status') }}
						@endif

						@if (session('error_message'))
							{{ trans('messages.error') }}
						@endif

						@if (session('warning_message'))
							{{ trans('messages.warning') }}
						@endif
					</h4>
				</div>
				<div class="modal-body">
					@if (session('success_message'))
						<div class="alert alert-success alert-dismissible" role="alert">
							{{ session('success_message') }}
						</div>
					@endif

					@if (session('status'))
						<div class="alert alert-success alert-dismissible" role="alert">
						</div>
					@endif

					@if (session('error_message'))
						<div class="alert alert-error alert-dismissible" role="alert">
							{{ session('error_message') }}
						</div>
					@endif

					@if (session('warning_message'))
						<div class="alert alert-warning alert-dismissible" role="alert">
							{{ session('warning_message') }}
						</div>
					@endif
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('messages.close') }}</button>
				</div>
			</div>
		</div>
	</div>

@endif