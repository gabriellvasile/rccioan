
@extends('layouts.page')

@section('content')

	<div class="castingsWrap">
		<h1>{{ trans('messages.my_castings') }}</h1>
		<hr>
	</div>

	<div class="table-responsive jobsList">
	  <table class="table table-hover">
		<tr>
			<th>{{ trans('messages.production_name') }}</th>
			<th>{{ trans('messages.production_type') }}</th>
			<th>{{ trans('messages.character') }}</th>
			<th>{{ trans('messages.date_applied') }}</th>
			<th>{{ trans('messages.chosen') }}</th>
			<th>{{ trans('messages.accepted') }}</th>
			<th>{{ trans('messages.declined') }}</th>
			<th>{{ trans('messages.actions') }}</th>
		</tr>
		@foreach ($jobs as $job)
			<tr>
				<td>
					<a href="{{ route('jobs.show', [$job->job_id, App::getLocale()]) }}">
						{{ $job->production_name }}
					</a>
				</td>
				<td>
					<a href="{{ route('jobs.show', [$job->job_id, App::getLocale()]) }}">
						{{ $job->production_type }}
					</a>
				</td>
				<td>
					<a href="{{ route('jobs.show', [$job->job_id, App::getLocale()]) }}">
						{{ $job->character_title }}
					</a>
				</td>
				<td><a href="{{ route('jobs.show', [$job->job_id, App::getLocale()]) }}">{{ $job->date_applied }}</a></td>
				<td>
					<a href="{{ route('jobs.show', [$job->job_id, App::getLocale()]) }}">
						@if ($job->is_chosen == 1)
							{{ trans('messages.yes') }}
						@else
							{{ trans('messages.no') }}
						@endif
					</a>
				</td>

				<td>
					<a href="{{ route('jobs.show', [$job->job_id, App::getLocale()]) }}">
						@if ($job->is_accepted == 1)
							{{ trans('messages.yes') }}
						@else
							{{ trans('messages.no') }}
						@endif
					</a>
				</td>

				<td>
					<a href="{{ route('jobs.show', [$job->job_id, App::getLocale()]) }}">
						@if ($job->is_declined_employer == 1)
							{{ trans('messages.yes') }}
						@else
							{{ trans('messages.no') }}
						@endif
					</a>
				</td>

				<td class="actions">
					<div class="actions">
						{!! Form::open( ['route' => ['casts.destroy', $job->character_id, App::getLocale()], 'method' => 'delete'] ) !!}
							{!! Form::button('', ['class' => 'delete', 'type' => 'submit', 'title' => trans('messages.resign')] ) !!}
						{!! Form::close() !!}
					</div>
				</td>
			</tr>
		@endforeach
	  </table>
	</div>



@endsection