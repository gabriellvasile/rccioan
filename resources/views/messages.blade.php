
@extends('layouts.page')

@section('content')

	<div class="messagesWrap" data-uri="{{ route('messages.markread') }}">
		<h1>{{ trans('messages.messages') }}</h1>
		<hr>

		<div role="tabpanel row messagesTabs">

		  <!-- Nav tabs -->
		  <div class="col-sm-3">
		  	<div class="myMessages">{{ trans('messages.my_messages') }}</div>
		  	<ul class="nav nav-tabs nav-stacked" role="tablist">
		  	  <li role="presentation" class="active"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">{{ trans('messages.received_messages') }} <span class="unread">{{ $unread }}</span></a></li>
		  	  <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">{{ trans('messages.sent_messages') }}</a></li>
		  	</ul>
		  </div>

		  <!-- Tab panes -->
		  <div class="col-sm-9">
		  	<div class="tab-content">

		  	  <div role="tabpanel" class="tab-pane active" id="profile">

		  	  	<div class="table-responsive">
		  	  	  <table class="table">
			  	  	<tr>
			  	  		<th>{{ trans('messages.from') }}:</th>
			  	  		<th>{{ trans('messages.subject') }}:</th>
			  	  		<th>{{ trans('messages.date') }}:</th>
			  	  		<th>{{ trans('messages.actions') }}</th>
			  	  	</tr>

		  	  		@foreach ($messages['by_recipient'] as $key => $message)
		  	  		<?php
			    		$sender = $messages_users['by_recipient'][$key]['sender'];
			    		$senderFullName = $sender->fname . ' ' . $sender->lname;
				    ?>
		  	  		<tr>
		  	  			<td class="{{ ($message->is_important) ? 'importantMessage' : '' }}">{{ $senderFullName }}</td>
		  	  			<td>
		  	  				<a href="#" data-toggle="modal" class="@if ($message->is_read) readMessage @else messageLink @endif message{{ $message->id }}" data-target="#messageModal-by_recipient-{{ $message->id }}">{{ $message->subject }}</a>

		  	  				<!-- Modal -->
		  	  				<div class="modal fade messageModal" id="messageModal-by_recipient-{{ $message->id }}" data-id="{{ $message->id }}" tabindex="-1" role="dialog" aria-labelledby="messageModal-by_recipient-{{ $message->id }}Label" aria-hidden="true">
		  	  				  <div class="modal-dialog">
		  	  				    <div class="modal-content">
		  	  				      <div class="modal-header">
		  	  				        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		  	  				        <h4 class="modal-title" id="messageModal-by_recipient-{{ $message->id }}Label">{{ $message->subject }} - {{ $message->created_at }}</h4>
		  	  				      </div>
		  	  				      <div class="modal-body">
		  	  				        {!! $message->message !!}
		  	  				      </div>
		  	  				      <div class="modal-footer">
									<a href="{{ route('messages.reply', [$message->id, App::getLocale()]) }}" class="btn reply">{{ trans('messages.reply') }}</a>
		  	  				        <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('messages.close') }}</button>
		  	  				      </div>
		  	  				    </div>
		  	  				  </div>
		  	  				</div>

		  	  			</td>
		  	  			<td>{{ $message->created_at }}</td>
			  	  		<td class="tdactions">
			  	  			<div class="actions">
			  	  				<a href="{{ route('messages.reply', [$message->id, App::getLocale()]) }}" class="reply"></a>

			  	  				{!! Form::open( ['route' => ['messages.trash', $message->id], 'method' => 'delete'] ) !!}
			  	  				    {!! Form::button('<i class="fa fa-times"></i>', ['class' => 'delete', 'type' => 'submit'] ) !!}
			  	  				{!! Form::close() !!}
			  	  			</div>
			  	  		</td>
			  	  	</tr>
		  	  		@endforeach
			  	  </table>
			  	</div>
			  	
		  	  </div>
		  	  <div role="tabpanel" class="tab-pane" id="messages">
		  	  	<div class="table-responsive">
		  	  	  <table class="table">
			  	  	<tr>
						<th>{{ trans('messages.to') }}:</th>
						<th>{{ trans('messages.subject') }}:</th>
						<th>{{ trans('messages.date') }}:</th>
						<th>{{ trans('messages.actions') }}</th>
			  	  	</tr>

		  	  		@foreach ($messages['by_sender'] as $key => $message)
		  	  		<?php
		  	  			$recipient = $messages_users['by_sender'][$key]['recipient'];
			    		$recipientFullName = $recipient->fname . ' ' . $recipient->lname;
				    ?>
			  	  		<tr>
			  	  			<td>{{ $recipientFullName }}</td>
			  	  			<td>
			  	  				<a href="#" class="@if ($message->is_read) readMessage @else messageLink @endif" data-toggle="modal" data-target="#messageModal-by_sender-{{ $message->id }}">{{ $message->subject }}</a>

			  	  				<!-- Modal -->
			  	  				<div class="modal fade" id="messageModal-by_sender-{{ $message->id }}" tabindex="-1" role="dialog" aria-labelledby="messageModal-by_sender-{{ $message->id }}Label" aria-hidden="true">
			  	  				  <div class="modal-dialog">
			  	  				    <div class="modal-content">
			  	  				      <div class="modal-header">
			  	  				        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			  	  				        <h4 class="modal-title" id="messageModal-by_sender-{{ $message->id }}Label">{{ $message->subject }} - {{ $message->created_at }}</h4>
			  	  				      </div>
			  	  				      <div class="modal-body">
			  	  				        {!! $message->message !!}
			  	  				      </div>
			  	  				      <div class="modal-footer">
			  	  				        <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('messages.close') }}</button>
			  	  				      </div>
			  	  				    </div>
			  	  				  </div>
			  	  				</div>

			  	  			</td>
			  	  			<td>{{ $message->created_at }}</td>
				  	  		<td class="tdactions">
				  	  			<div class="actions">
				  	  				<a href="{{ route('messages.reply', [$message->id, App::getLocale()]) }}" class="reply"></a>
				  	  				<!-- <a href="{{ route('messages.forward', $message->id) }}" class="forward"></a> -->
				  	  				{!! Form::open( ['route' => ['messages.trash', $message->id], 'method' => 'delete'] ) !!}
				  	  				    {!! Form::button('<i class="fa fa-times"></i>', ['class' => 'delete', 'type' => 'submit'] ) !!}
				  	  				{!! Form::close() !!}
				  	  			</div>
				  	  		</td>
				  	  	</tr>
		  	  		@endforeach
			  	  </table>
			  	</div>
		  	  </div>
		  	</div>
		  </div>

		</div>

		<a href="{{ route('messages.new', [0, App::getLocale()]) }}" class="btn">{{ trans('messages.newmessage') }}</a>
	</div>

@endsection

@section('afterfooter')
	<script>
		$('.messageModal').on('shown.bs.modal', function () {
			var id = $(this).data('id');
			$.ajax({
				url: $('.messagesWrap').data('uri'),
				method: 'post',
				dataType: 'json',
				data: {
					'id' 	 : id,
				},
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				success: function (data) {
					unread = parseInt($('.unread').text());
					if (unread > 0 && data['modify']) {
						$('.unread').text(unread - 1);
					}
					$('.message' + id).removeClass('messageLink').addClass('readMessage');
				},
			});
		})
	</script>
@endsection