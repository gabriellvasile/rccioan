
@extends('layouts.page')

@section('content')

	<div class="partnersWrap">
		<h1>Despre Romania Casting Call</h1>
		<hr>
		<h3>Instituții de invățământ</h3>
		
		<p>Pentru a va putea face un cont care sa fie acceptat, anumite categorii (ex: actor/ echipa filmare) necesita studii universitare sau de specialitate.</p>
		<p>Romania Casting Call colaboreaza cu cele mai bune institutii de invatamant de scurta durata, care formeaza profesionisti in domenii precum: animatie / sunet / camera / regie / actorie / productie si multe altele</p>
		<p>Asadar, daca sunteti interesat de studii de specialitate si doriti de asemenea un cont pe platform noastra, va recomandam urmatoarele scoli si cursuri:</p>

		<div class="partners">
			<div class="row">
				<div class="col-sm-4 partner">
					<img src="{{ route('home') . '/images/partner1.jpg' }}" alt="">
					<h3>Actorie de film.ro - Bucuresti</h3>
					<a href="http://www.actoriedefilm.ro" target="_blank">www.actoriedefilm.ro</a>
				</div>
				<div class="col-sm-4 partner">
					<img src="{{ route('home') . '/images/partner2.jpg' }}" alt="">
					<h3>Asociatia culturala [ctrl+n] - Bucuresti</h3>
					<a href="http://www.controln.ro" target="_blank">www.controln.ro</a>
				</div>
				<div class="col-sm-4 partner">
					<img src="{{ route('home') . '/images/partner3.jpg' }}" alt="">
					<h3>Mandragora Movies</h3>
					<a href="http://www.mandragoramovies.com" target="_blank">www.mandragoramovies.com</a>
				</div>
			</div>
		</div>

		<p>Daca doriti asistenta pentru a gasi cel mai potrivit curs pentru dumneavoastra, ne puteti contacta la adresele disponibile in pagina de contact.</p>
	</div>

@endsection