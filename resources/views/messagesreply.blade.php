
@extends('layouts.page')

@section('content')

	<div class="replyWrap">
		<h1>{{ trans('messages.reply') }}</h1>
		<hr>

		{!! Form::open( ['route' => ['messages.doreply', $message->id]] ) !!}
			<div class="form-group">
				{!! Form::label('subject', trans('messages.subject')) !!}
				{!! Form::text( 'subject', $message->subject, ['placeholder' => trans('messages.subject'), 'class' => 'form-control', 'id' => 'subject']) !!}
			</div>
			
			<div class="form-group">
				{!! Form::label('message', trans('messages.message')) !!}
				{!! Form::textarea( 'message', $reply . $message->message, ['placeholder' => trans('messages.message'), 'class' => 'form-control', 'id' => 'message']) !!}
			</div>

			<div class="form-group">
				{!! Form::submit(trans('messages.send'), ['class' => 'btn']) !!}
			</div>

		{!! Form::close() !!}
	</div>

@endsection

@section('afterfooter')
	<script src="//tinymce.cachefly.net/4.1/tinymce.min.js"></script>

    <script type="text/javascript">
      $(function () {

        tinymce.init({
            selector: "textarea"
		});
        
      });
    </script>
@stop