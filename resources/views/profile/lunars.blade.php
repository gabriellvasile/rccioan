@extends('layouts.page')

<?php $listActors = true; ?>

@section('content')
    <div class="text-center topTitle">
        <h1>{{ trans('messages.lunar_title') }}</h1>
        <hr>
        <p>{{ trans('messages.list_actors_message') }}</p>
    </div>

    <?php $year = $month = ''; $i=0; ?>
    @forelse($lunars as $lunar)

        <div class="lunar @if ($i == 0) nobd @endif">
            @unless ($year == $lunar->date->year)
                <h1>{{ $lunar->date->year }}</h1>
                <?php $year  =  $lunar->date->year; ?>
            @endunless

            @unless ($month == $lunar->date->month)
                <h2>@include('includes._month', ['month' => $lunar->date->month])</h2>
                <?php $month  =  $lunar->date->month; ?>
            @endunless

            @if ($i == 0) </div></div> <div class="bgLunar"><div class="container"><div class="lunar"> @endif
                <div class="video" data-src="{{ $lunar->video }}"></div>
            @if ($i == 0) </div></div></div><div class="container"><div class="lunar"> @endif

            <div class="sharing clearfix">
                <span class='st_sharethis_hcount' displayText='ShareThis'></span>
                <span class='st_fblike_large' displayText='Facebook Like'></span>
            </div>

            @if (isset($lunar->actor->user))
                <h3><span>{{ $lunar->actor->user->fname }} {{ $lunar->actor->user->lname }}</span></h3>
            @else
                <h3><span>{{ $lunar->name }}</span></h3>
            @endif

            <div>
                {{ $lunar->description }}
            </div>
        </div>

        <?php $i++; ?>
    @empty
        <h1 class="text-center" style="margin:100px 0">{{ trans('messages.no_actors') }}.</h1>
    @endforelse

@stop

@section('afterfooter')
@stop