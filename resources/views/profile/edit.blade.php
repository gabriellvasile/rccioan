@extends('layouts.page')

@section('content')

	<div class="profile" data-type="">
		<h2>{{ trans('messages.edit_profile') }} </h2>

		<div class="la-anim-10"></div> {{-- Loading will be shown when an ajax request is made --}}

		<div role="tabpanel">
		
		  	<!-- Nav tabs -->
		    <ul class="nav nav-tabs hidden" role="tablist">
			    <li role="presentation" class="active"><a href="#usertab" aria-controls="usertab" role="tab" data-toggle="tab"></a></li>
			    <li role="presentation"><a href="#characteristicstab" aria-controls="characteristicstab" role="tab" data-toggle="tab"></a></li>
			    <li role="presentation"><a href="#educationtab" aria-controls="educationtab" role="tab" data-toggle="tab"></a></li>
			    <li role="presentation"><a href="#filestab" aria-controls="filestab" role="tab" data-toggle="tab"></a></li>
		    </ul> 
		
		  	<!-- Tab panes -->
		    <div class="tab-content">
				@include('profile._user')
			</div>
		
		</div>
	</div>

	@include('includes._success', ['stitle' => trans('messages.thank_you'), 'sdescription' => trans('messages.profile_check'), ])

@stop

@section('afterfooter')
	<script src="{{ asset('/js/profile.js') }}"></script>
@stop