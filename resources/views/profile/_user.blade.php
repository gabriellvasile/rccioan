<?php 
	if ( $userType == 'actor' ) {
		$nexttab = 'characteristicstab';
		$show 	 = route('profile.ajaxShowCharasteristics', [$user->username, App::getLocale()]);
		$route = ['profile.ajaxUpdateUser', $user->username, App::getLocale()];
	}
	elseif ( $userType == 'employer' ) {
		$nexttab = 'filestab';
		$show = route('profile.ajaxShowFiles', [$user->username, App::getLocale()]);
		$route = ['profile.ajaxUpdateCompany', $user->username, App::getLocale()];
	}
?>

<div role="tabpanel" class="tab-pane fade in active" id="usertab">
	{!! Form::model( $user, ['route' => $route, 'method' => 'PATCH', 'id' => 'userform', 'data-show' => $show] ) !!}

		<legend class="legendWithBtn">
			{{ trans('messages.user_data') }}
			{!! Form::button(trans('messages.next'), ['class' => 'btn btn-primary profilesubmit', 'data-nexttab' => $nexttab]) !!}
		</legend>

		{!! Form::hidden('locale', App::getLocale()) !!}

		@if ( $userType != 'employer' )

			<div class="row">
				<div class="col-sm-6">

					<div class="form-group">
						{!! Form::label('fname', trans('messages.first_name')) !!}
						{!! Form::text('fname', null , ['placeholder' => trans('messages.first_name'), 'class' => 'form-control']) !!}
					</div>

					<div class="form-group">
						{!! Form::label('sex', trans('messages.sex')) !!}
						{!! Form::select('sex', [trans('messages.select_sex'), 'M' => 'M', 'F' => 'F'], null, ['placeholder' => trans('messages.sex'), 'class' => 'form-control']) !!}
					</div>

					<div class="form-group">
						{!! Form::label('country_id', trans('messages.country')) !!}
						{!! Form::select('country_id', $countries, null, ['placeholder' => trans('messages.country'), 'class' => 'form-control']) !!}
					</div>

					<div class="form-group">
						{!! Form::label('language_id', trans('messages.profile_lang')) !!}
						{!! Form::select('language_id', [trans('messages.select_lang'), 'Romana', 'English'], null, ['placeholder' => trans('messages.profile_lang'), 'class' => 'form-control']) !!}
					</div>

				</div>
				<div class="col-sm-6">
					<div class="form-group">
						{!! Form::label('lname', trans('messages.last_name')) !!}
						{!! Form::text('lname', null , ['placeholder' => trans('messages.last_name'), 'class' => 'form-control']) !!}
					</div>

					<div class="form-group">
						{!! Form::label('birth_date', trans('messages.birth_date')) !!}
						{!! Form::text('birth_date', null, ['placeholder' => trans('messages.birth_date'), 'class' => 'form-control']) !!}
					</div>

					<div class="form-group">
						{!! Form::label('city', trans('messages.city')) !!}
						{!! Form::text('city', null, ['placeholder' => trans('messages.city'), 'class' => 'form-control']) !!}
					</div>

					<div class="form-group">
						{!! Form::label('mobile', trans('messages.mobile')) !!}
						{!! Form::text('mobile', null, ['placeholder' => trans('messages.mobile'), 'class' => 'form-control']) !!}
					</div>
				</div>
			</div>

			<hr>

		@endif

		<div class="row">
			<div class="col-sm-6">
				<div class="form-group">
				    {!! Form::label('email', trans('messages.email')) !!}
				    {!! Form::email('email', null, ['placeholder' => trans('messages.email'), 'class' => 'form-control']) !!}
				</div>
			</div>
			
			<div class="col-sm-6">
				<div class="form-group">
				    {!! Form::label('username', trans('messages.username')) !!}
					@if ($user->username_changed < 2)
				    	{!! Form::text('username', null , ['placeholder' => trans('messages	.username'), 'class' => 'form-control']) !!}
						<div class="usernameChangesNotification">
							{{ trans('messages.username_same_as_profile') }}
							<ul>
								<li>{{ route('profile.show', $user->username) }} <br></li>
							</ul>
							{{ trans_choice('messages.username_can_be_changed', $user->username_changed) }}
						</div>
					@else
				    	{!! Form::text('username', null , ['placeholder' => trans('messages.username'), 'class' => 'form-control', 'disabled' => 'disabled', 'title' => trans('messages.username_changed_twice')]) !!}
						<div class="usernameChangesNotification">
							{{ trans('messages.username_same_as_profile') }}
							<ul>
								<li>{{ route('profile.show', $user->username) }}<br></li>
							</ul>
							{{ trans('messages.username_changed_twice') }}
						</div>
					@endif
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-6">
				<div class="form-group">
				    {!! Form::label('password', trans('messages.password')) !!}
				    {!! Form::password('password', ['class' => 'form-control']) !!}
				</div>
			</div>
			
			<div class="col-sm-6">
				<div class="form-group">
				    {!! Form::label('password_confirmation', trans('messages.confirm_password')) !!}
				    {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
				</div>
			</div>
		</div>

		@if ( $userType == 'employer' )

			<hr>
			<legend>{{ trans('messages.company_details') }}</legend>

			<div class="row">
				<div class="col-sm-6">
					<div class="form-group">
					    {!! Form::label('company', trans('messages.company_name')) !!}
					    {!! Form::text( $userType . '[company]', null, ['placeholder' => trans('messages.company_name'), 'class' => 'form-control', 'id' => 'company']) !!}
					</div>
				</div>
				<div class="col-sm-6">
					<div class="form-group">
					    {!! Form::label('address', trans('messages.address')) !!}
					    {!! Form::text( $userType . '[address]', null, ['placeholder' => trans('messages.address'), 'class' => 'form-control', 'id' => 'address']) !!}
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-6">
					<div class="form-group">
						{!! Form::label('country_id', trans('messages.country')) !!}
						{!! Form::select('country_id', $countries, null, ['placeholder' => trans('messages.country'), 'class' => 'form-control']) !!}
					</div>
				</div>

				<div class="col-sm-6">
					<div class="form-group">
						{!! Form::label('city', trans('messages.city')) !!}
						{!! Form::text('city', null, ['placeholder' => trans('messages.city'), 'class' => 'form-control']) !!}
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-4">
					<div class="form-group">
					    {!! Form::label('post_code', trans('messages.post_code')) !!}
					    {!! Form::text( $userType . '[post_code]', null, ['placeholder' => trans('messages.post_code'), 'class' => 'form-control', 'id' => 'post_code']) !!}
					</div>
				</div>


				<div class="col-sm-4">
					<div class="form-group">
						{!! Form::label('mobile', trans('messages.mobile')) !!}
						{!! Form::text('mobile', null, ['placeholder' => trans('messages.mobile'), 'class' => 'form-control']) !!}
					</div>
				</div>

				{{--<div class="col-sm-6">--}}
					{{--<div class="form-group">--}}
						{{--{!! Form::label('email', trans('messages.email')) !!}--}}
						{{--{!! Form::email('email', null, ['placeholder' => trans('messages.email'), 'class' => 'form-control']) !!}--}}
					{{--</div>--}}
				{{--</div>--}}

				<div class="col-sm-4">
					<div class="form-group">
						{!! Form::label('website', trans('messages.website')) !!}
						{!! Form::text( $userType . '[website]', null, ['placeholder' => trans('messages.website'), 'class' => 'form-control', 'id' => 'website']) !!}
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-12">
					<div class="form-group">
					    {!! Form::label('website', trans('messages.company_info')) !!}
					    {!! Form::textarea( $userType . '[info]', null, ['placeholder' => trans('messages.company_info'), 'class' => 'form-control', 'id' => 'info']) !!}
					</div>
				</div>
			</div>
		@endif

		{!! Form::button(trans('messages.next'), ['class' => 'btn btn-primary profilesubmit', 'data-nexttab' => $nexttab]) !!}

	{!! Form::close() !!}
</div>