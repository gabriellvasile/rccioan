<div class="well workshop" data-workshopid="{{ $key }}">
	<div class="btn btn-danger deleteFromProfile">-</div>
	<div class="row">
		<div class="col-sm-6">
			<div class="form-group">
				<label for="workshop_name{{ $key }}">{{ trans('messages.workshop_name') }}</label>
				<input id="workshop_name{{ $key }}" class="form-control" type="text" value="" name="{{ $userType }}[workshop][{{ $key }}][name]" placeholder="{{ trans('messages.workshop_name') }}">
			</div>
		</div>
		<div class="col-sm-6">
			<div class="form-group">
				<label for="workshop_duration{{ $key }}">{{ trans('messages.workshop_award') }}</label>
				<input id="workshop_duration{{ $key }}" class="form-control" type="text" value="" name="{{ $userType }}[workshop][{{ $key }}][duration]" placeholder="{{ trans('messages.workshop_award') }}">
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<div class="form-group">
				<label for="workshop_produced_under{{ $key }}">{{ trans('messages.workshop_year') }}</label>
				<input id="workshop_produced_under{{ $key }}" class="form-control" type="text" value="" name="{{ $userType }}[workshop][{{ $key }}][produced_under]" placeholder="{{ trans('messages.workshop_year') }}">
			</div>
		</div>
		<div class="col-sm-6">
			<div class="form-group">
				<label for="workshop_production_type{{ $key }}">{{ trans('messages.workshop_year') }}</label>
				<input id="workshop_production_type{{ $key }}" class="form-control" type="text" value="" name="{{ $userType }}[workshop][{{ $key }}][production_type]" placeholder="{{ trans('messages.workshop_year') }}">
			</div>
		</div>
	</div>
</div>