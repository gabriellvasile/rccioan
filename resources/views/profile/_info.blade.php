<div class="info">
	<legend class="clearfix">{{ trans('messages.other_info') }}</legend>

	@if ( $userType == 'actor' )
		<div class="row">
			<div class="col-sm-6">
				<div class="form-group">
				    {!! Form::label('website', trans('messages.website')) !!}
				    {!! Form::text('actor[website]', null , ['placeholder' => trans('messages.website'), 'class' => 'form-control', 'id' => 'website']) !!}
				</div>
			</div>
			<div class="col-sm-6">
				<div class="form-group">
				    {!! Form::label('role_abroad', trans('messages.role_abroad')) !!}
				    {!! Form::select('actor[role_abroad]', ['None' => trans('messages.select_answer'), '0' => trans('messages.no'), '1' => trans('messages.yes')], null , ['placeholder' => trans('messages.role_abroad'), 'class' => 'form-control', 'id' => 'role_abroad']) !!}
				</div>
			</div>
		</div>
	@else
		<div class="form-group">
		    {!! Form::label('website', trans('messages.website')) !!}
		    {!! Form::text('actor[website]', null , ['placeholder' => trans('messages.website'), 'class' => 'form-control', 'id' => 'website']) !!}
		</div>
	@endif

	<div class="form-group">
	    {!! Form::label('info', trans('messages.other_info')) !!}
	    {!! Form::textarea('actor[info]', null , ['placeholder' => trans('messages.other_info'), 'class' => 'form-control', 'id' => 'info', 'rows' => 3]) !!}
	</div>


	<legend class="clearfix">{{ trans('messages.interested_in') }}</legend>

	<div class="row">
		<div class="form-group">
			<div id="productions">
				@foreach ($productions as $id => $production)
					<div class="col-sm-4">
						{!! Form::checkbox('actor[productions][' . $id . ']', $id, in_array($id, $user_productions) ? true : false , ['id' => 'actor[productions][' . $id . ']']) !!}
						{!! Form::label('actor[productions][' . $id . ']',  $production) !!}
					</div>
				@endforeach
			</div>
		</div>
	</div>

</div>