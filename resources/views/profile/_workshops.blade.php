<div class="workshops editable" data-type="workshop" data-remove="{{ route('profile.removeFromProfile', $user->username) }}">

	<legend class="clearfix legendWithBtn">
		{{ trans('messages.workshops_title') }}
		<div class="btn btn-success pull-right addToProfile" data-url="{{ route('profile.ajaxNewWorkshop', [$user->username, App::getLocale()]) }}">+ {{ trans('messages.new_workshop') }}</div>
	</legend>
	
	@foreach ($user->workshops as $workshop)
		<div class="well workshop">
			<div class="btn btn-danger deleteFromProfile" data-id="{{ $workshop->id }}">-</div>
			<div class="row">
				<div class="col-sm-6">
					<div class="form-group">
						{!! Form::label('workshop_name' . $workshop->id, trans('messages.workshop_name')) !!}
						{!! Form::text( $userType . '[workshop][' . $workshop->id . '][name]' , $workshop->name , ['placeholder' => trans('messages.workshop_name'), 'class' => 'form-control', 'id' => 'workshop_name'  . $workshop->id]) !!}
					</div>
				</div>
				
				<div class="col-sm-6">
					<div class="form-group">
						{!! Form::label('workshop_award' . $workshop->id, trans('messages.workshop_award')) !!}
						{!! Form::text( $userType . '[workshop][' . $workshop->id . '][award]' , $workshop->award , ['placeholder' => trans('messages.workshop_award'), 'class' => 'form-control', 'id' => 'workshop_award'  . $workshop->id]) !!}
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-6">
					<div class="form-group">
						{!! Form::label('workshop_year' . $workshop->id, trans('messages.workshop_year')) !!}
						{!! Form::text( $userType . '[workshop][' . $workshop->id . '][year]' , $workshop->year , ['placeholder' => trans('messages.workshop_year'), 'class' => 'form-control', 'id' => 'workshop_year'  . $workshop->id]) !!}
					</div>
				</div>
				
				<div class="col-sm-6">
					<div class="form-group">
						{!! Form::label('workshop_description' . $workshop->id, trans('messages.workshop_desc')) !!}
						{!! Form::text( $userType . '[workshop][' . $workshop->id . '][description]' , $workshop->description , ['placeholder' => trans('messages.workshop_desc'), 'class' => 'form-control', 'id' => 'workshop_description'  . $workshop->id]) !!}
					</div>
				</div>
			</div>
		</div>
	@endforeach
</div>