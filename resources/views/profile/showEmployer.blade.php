@extends('layouts.page')

@section('content')

	<div class="profile">
		<div class="text-center">
			<h1>{{ isset($updates->employer->company) ? $updates->employer->company : $user->employer->company }}</h1>
		</div>

		<hr class="smallHr">

		<div class="row profilePicAndData">
			<div class="col-md-3 col-md-offset-2 col-sm-4">
				<?php $x = false; ?>
				@if ($user->images)
					<div class="row">
						@foreach ($user->images as $image)
							<?php if ($image->id == $user->thumb_id) { ?>
								<div class="file">
									<div>
										<a href="{{ $image->path }}" class="fancybox" rel="group">
											<img src="{{ $image->thumb }}" class="img-responsive">
										</a>
									</div>
								</div>
								<?php
								$x = true;
							} ?>
						@endforeach
					</div>
				@endif

				<?php if ( !$x ) { ?>
					<a href="{{ route('home') . '/images/user.jpg' }}" class="fancybox" rel="group">
						<img src="{{ route('home') . '/images/user.jpg' }}" alt="{{ $user->employer->company  }}" id="userImage" class="img-responsive">
					</a>
				<?php } ?>
			</div>
			<div class="col-md-5 col-sm-8">
				<div class="row">
					<div class="col-md-6">
						<div class="title">{{ ucwords(trans('messages.company')) }}:</div>
						<div class="ceare">{{ (isset($updates->employer->company) ? $updates->employer->company : $user->employer->company) }}</div>

						<div>{{ $user->employer->employertype->name }}</div>

						@if (Auth::check())
							<div class="title">{{ trans('messages.city') }}:</div>
							<div class="ceare">{{ (isset($updates->city) ? $updates->city : $user->city) }} - {{ (isset($updates->country_id) ? $countries[$updates->country_id] : $user->country->name) }}</div>
						@endif
					</div>
					<div class="col-md-6">
						@if (Auth::check())
							<div class="title">{{ trans('messages.phone') }}:</div>
							<div class="ceare">{{ (isset($updates->mobile) ? $updates->mobile : $user->mobile) }}</div>
						@endif
					</div>
				</div>

				@if (Auth::check())
					<div class="title">{{ trans('messages.address') }}:</div>
					<div class="ceare">{{ (isset($updates->employer->address) ? $updates->employer->address : $user->employer->address) }}</div>

					<div class="row">
						<div class="col-md-6">
							<div class="title">{{ trans('messages.post_code') }}:</div>
							<div class="ceare">{{ (isset($updates->employer->post_code) ? $updates->employer->post_code : $user->employer->post_code) }}</div>
						</div>
						<div class="col-md-6">
							<div class="title">{{ trans('messages.website') }}:</div>
							<div class="ceare">{{ (isset($updates->employer->website) ? $updates->employer->website : $user->employer->website) }}</div>
						</div>
					</div>
				@endif
				<hr>

				@if (Auth::check() && Auth::user()->id != $user->id)
					<div class="profileLinks">
						<div>
							<a href="{{ route('messages.new', [$user->id, App::getLocale()]) }}" class="message">{{ trans('messages.send_message') }}</a>
						</div>
					</div>
				@endif
			</div>
		</div>

		<hr class="smallHr">

		<div class="row profilePicAndData">
			<div class="col-md-3 col-md-offset-2 col-sm-4">
				<?php $x = false; ?>
				@if ($user->images)
					<div class="row">
						@foreach ($user->images as $image)
							<?php if ($image->id != $user->$profileType->logo) { ?>
								<div class="file">
									<div>
										<a href="{{ $image->path }}" class="fancybox" rel="group"><img src="{{ $image->thumb }}" class="img-responsive"></a>
									</div>
								</div>
								<?php
								$x = true;
							} ?>
						@endforeach
					</div>
				@endif

				<?php if ( !$x ) { ?>
					<a href="{{ route('home') . '/images/user.jpg' }}" class="fancybox img-responsive" rel="group">
						<img src="{{ route('home') . '/images/user.jpg' }}" alt="{{ $user->employer->company  }}" id="userImage" class="img-responsive">
					</a>
				<?php } ?>
			</div>
			<div class="col-md-5 col-sm-8">
				<div class="ceare">{{ trans('messages.about_company') }}:</div>
				<div class="title">{{ (isset($updates->employer->company) ? $updates->employer->company : $user->employer->company) }}</div>
				<br>
				<div class="description">
					{{ (isset($updates->employer->info) ? $updates->employer->info : $user->employer->info) }}
				</div>
			</div>
		</div>

		<hr>

		<div class="text-center">
			<a href="{{ URL::previous() }}"  class="btn">{{ trans('messages.back') }}</a>

			@if (Auth::user()->id != $user->id)
				<a href="{{ route('messages.new', [$user->id, App::getLocale()]) }}"  class="btn">{{ trans('messages.send_message') }}</a>
			@endif

			@if (isset($admin) && $admin)
				@if (isset($updates) && $updates)
					<a href="{{ route('admin.user.approve', $user->username) }}" class="btn">{{ trans('messages.approve_complete') }}</a>
					<a href="{{ route('admin.user.uncomplete', $user->username) }}" class="btn">{{ trans('messages.uncomplete') }}</a>
				@endif
			@endif
		</div>
	</div>


@stop