@extends('layouts.page')

@section('content')

	<div class="profile">
		<div class="text-center">
			<h1>{{ (isset($updates->fname) ? $updates->fname : $user->fname) . ' ' . (isset($updates->lname) ? $updates->lname : $user->lname) }}</h1>
		</div>
		
		<hr class="smallHr">
		
		<div class="row profilePicAndData">
			<div class="col-md-3 col-md-offset-2 col-sm-4">
				<?php $x = false; ?>
				@if ($user->images)
					<div class="row">
						@foreach ($user->images as $image)
							<?php if ($image->id == $user->thumb_id) { ?>
								<div class="file">
									<a href="{{ $image->path }}" class="fancybox" rel="group"><img src="{{ $image->thumb }}" class="img-responsive"></a>
								</div>
								<?php
								$x = true;
							} ?>
						@endforeach
					</div>
				@endif

				<?php if ( !$x ) { ?>
					<a class="fancybox" rel="group" href="{{ route('home') . '/images/user.jpg' }}">
						<img src="{{ route('home') . '/images/user.jpg' }}" alt="{{ $user->fname . ' ' . $user->lname }}" id="userImage" class="img-responsive">
					</a>
				<?php } ?>

			</div>
			<div class="col-md-5 col-sm-8">
				<div class="row">
					<div class="col-md-6">
						<div class="title">{{ trans('messages.name') }}:</div>
						<div class="name">{{ (isset($updates->fname) ? $updates->fname : $user->fname) . ' ' . (isset($updates->lname) ? $updates->lname : $user->lname) }}</div>

						<div class="title">{{ trans('messages.kind') }}:</div>
						<div class="ceare">{{ trans('messages.' . strtolower($user->actor->actortype->name) . '_sg') }}</div>

						<div class="title">{{ trans('messages.city') }}:</div>
						<div class="ceare">{{ (isset($updates->city) ? $updates->city : $user->city) }} - {{ (isset($updates->country_id) ? $countries[$updates->country_id] : $user->country->name) }}</div>
					</div>
					<div class="col-md-6">
						@if (Auth::check())
							<div class="title">{{ trans('messages.email') }}:</div>
							<div class="ceare">{{ (isset($updates->email) ? $updates->email : $user->email) }}</div>

							<div class="title">{{ trans('messages.phone') }}:</div>
							<div class="ceare">{{ (isset($updates->mobile) ? $updates->mobile : $user->mobile) }}</div>
						@endif

						@if (isset($updates->ranges))
							<div class="title">{{ trans('messages.range') }}:</div>
							@foreach ($updates->ranges as $range)
								<div class="ceare">
									{{ $ranges[$range] }}
								</div>
							@endforeach

						@else
							@if(!$user->ranges->isEmpty())
								<div class="title">{{ trans('messages.range') }}:</div>
								<div class="ceare">
									@foreach($user->ranges as $range)
										{{ $range->name }}
									@endforeach
								</div>
							@endif
						@endif

					</div>
				</div>
				<hr>

				@if (Auth::check() && !isset($updates->ranges))
					<div class="profileLinks">
						<div>
							<a href="{{ route('profile.pdf', [$user->username, App::getLocale()]) }}" class="download">{{ trans('messages.download_profile') }}</a>
						</div>
						@if (Auth::user()->id != $user->id)
							<div>
								<a href="{{ route('messages.new', [$user->id, App::getLocale()]) }}" class="message">{{ trans('messages.send_message') }}</a>
							</div>
						@endif
					</div>
				@endif
			</div>
		</div>
		
		<hr>
		
		<div class="row">
			<div class="col-sm-6">
				<div class="proTitle">{{ trans('messages.education') }}</div>

				<div class="wrapDescr">
					<strong>{{ trans('messages.professional_studies') }}:</strong>
					@if (isset($updates->profesional_studies))
						@if ( $updates->profesional_studies == 1 )
							{{ trans('messages.yes') }} <br>
							<strong>{{ trans('messages.faculty') }}:</strong>

							@if ( $updates->faculty == 1 )
								{{ trans('messages.yes') }}
							@else
								{{ trans('messages.no') }}
							@endif
							<br>
							<strong>Anul absolvirii:</strong> {{ $updates->faculty_year }} <br>
							<strong>{{ trans('messages.is_still_student') }}?</strong>
							@if ( isset($updates->faculty_student) && $updates->faculty_student == 1 )
								{{ trans('messages.yes') }}
							@else
								{{ trans('messages.no') }}
							@endif

							<br>
						@else
							{{ trans('messages.no') }} <br>
						@endif
					@else
						@if ( $user->$profileType->profesional_studies == 1 )
							{{ trans('messages.yes') }} <br>
							<strong>{{ trans('messages.faculty') }}:</strong>

							@if ( $user->$profileType->faculty == 1 )
								{{ trans('messages.yes') }}
							@else
								{{ trans('messages.no') }}
							@endif
							<br>
							<strong>Anul absolvirii:</strong> {{ $user->$profileType->faculty_year }} <br>
							<strong>{{ trans('messages.is_still_student') }}?</strong>
							@if ( $user->$profileType->faculty_student == 1 )
								{{ trans('messages.yes') }}
							@else
								{{ trans('messages.no') }}
							@endif

							<br>
						@else
							{{ trans('messages.no') }} <br>
						@endif
					@endif

					<hr>
					<strong>{{ trans('messages.speciality_studies') }}:</strong>

					@if (isset($updates->speciality_studies))
						@if ( $updates->speciality_studies == 1 )
							{{ trans('messages.yes') }} <br>
							<strong>{{ trans('messages.speciality_year') }}:</strong>  {{ $updates->speciality_year }} <br>
							<strong>{{ trans('messages.speciality_institution') }}:</strong>  {{ $updates->speciality_institution }} <br>
						@else
							{{ trans('messages.no') }} <br>
						@endif
					@else
						@if ( $user->$profileType->speciality_studies == 1 )
							{{ trans('messages.yes') }} <br>
							<strong>{{ trans('messages.speciality_year') }}:</strong>  {{ $user->$profileType->speciality_year }} <br>
							<strong>{{ trans('messages.speciality_institution') }}:</strong>  {{ $user->$profileType->speciality_institution }} <br>
						@else
							{{ trans('messages.no') }} <br>
						@endif
					@endif
					<br/>
				</div>

				<div class="proTitle">{{ trans('messages.projects_title') }}</div>

				<div class="wrapDescr">
					@if (isset($updates->projects))
						@forelse($updates->projects as $project)
							<div class="proiect">
								@if ($project->name)
									<strong>{{ trans('messages.project_name') }}:</strong>  {{ $project->name }} <br>
								@endif
								@if ($project->house)
									<strong>{{ trans('messages.project_house') }}:</strong>  {{ $project->house }} <br>
								@endif
								@if ($project->year)
									<strong>{{ trans('messages.project_year') }}:</strong>  {{ $project->year }} <br>
								@endif
								@if ($project->director)
									<strong>{{ trans('messages.project_director') }}:</strong>  {{ $project->director }} <br>
								@endif
								@if ($project->role)
									<strong>{{ trans('messages.project_role') }}:</strong>  {{ $project->role }} <br>
								@endif
								@if ($project->description)
									<strong>{{ trans('messages.project_desc') }}:</strong>  {{ $project->description }} <br>
								@endif
							</div>
							<hr><br/>
						@empty
							{{ trans('messages.no_projects') }}
							<hr><br/>
						@endforelse
					@else
						@forelse($user->projects as $project)
							<div class="proiect">
								@if ($project->name)
									<strong>{{ trans('messages.project_name') }}:</strong>  {{ $project->name }} <br>
								@endif
								@if ($project->house)
									<strong>{{ trans('messages.project_house') }}:</strong>  {{ $project->house }} <br>
								@endif
								@if ($project->year)
									<strong>{{ trans('messages.project_year') }}:</strong>  {{ $project->year }} <br>
								@endif
								@if ($project->director)
									<strong>{{ trans('messages.project_director') }}:</strong>  {{ $project->director }} <br>
								@endif
								@if ($project->role)
									<strong>{{ trans('messages.project_role') }}:</strong>  {{ $project->role }} <br>
								@endif
								@if ($project->description)
									<strong>{{ trans('messages.project_desc') }}:</strong>  {{ $project->description }} <br>
								@endif
							</div>
							<hr><br/>
						@empty
							{{ trans('messages.no_projects') }}
							<hr><br/>
						@endforelse
					@endif
				</div>

				@if ( isset($user->$profileType->workshops) || isset($updates->workshops) )
					<div class="proTitle">{{ trans('messages.workshops') }}</div>
					<div class="wrapDescr">
						@if (isset($updates->workshops))
							@if ($updates->workshops)
								{{ $updates->workshops }}
							@else
								{{ trans('messages.no_workshops') }}
							@endif
						@else
							@if ($user->$profileType->workshops)
								{{ $user->$profileType->workshops }}
							@else
								{{ trans('messages.no_workshops') }}
							@endif
						@endif
					</div>
					<hr>
				@endif

				@if ( isset($user->$profileType->awards) || isset($updates->awards) )
					<div class="proTitle">{{ trans('messages.awards') }}</div>
					<div class="wrapDescr">
						@if (isset($updates->awards))
							@if ($updates->awards)
								{{ $updates->awards }}
							@else
								{{ trans('messages.no_awards') }}
							@endif
						@else
							@if ($user->$profileType->awards)
								{{ $user->$profileType->awards }}
							@else
								{{ trans('messages.no_awards') }}
							@endif
						@endif
					</div>
					<hr>
				@endif

				@if ( isset($user->$profileType->info) || isset($updates->info) )
					<div class="proTitle">{{ trans('messages.extra_info') }}</div>
					<div class="wrapDescr">
						@if (isset($updates->info))
							@if ($updates->info)
								{{ $updates->info }}
							@else
								{{ trans('messages.no_extra_info') }}
							@endif
						@else
							@if ($user->$profileType->info)
								{{ $user->$profileType->info }}
							@else
								{{ trans('messages.no_extra_info') }}
							@endif
						@endif
					</div>
				@endif

			</div>
			<div class="col-sm-6">
				<div class="proTitle">{{ trans('messages.details') }}</div>
		
				<div class="wrapDescr">
					<div class="row">
						<div class="col-sm-6">

							<strong>{{ trans('messages.hair_colour') }}:</strong> {{ (isset($updates->hair_colour_id) ? $haircolours[$updates->hair_colour_id] : $user->$profileType->haircolour->name) }} <br>
							<strong>{{ trans('messages.eye_colour') }}:</strong>  {{ (isset($updates->eye_colour_id) ? $eyecolours[$updates->eye_colour_id] : $user->$profileType->eyecolour->name) }} <br>
							<strong>{{ trans('messages.height') }}:</strong> {{ (isset($updates->height) ? $updates->height : $user->$profileType->height) }} <br>
							<strong>{{ trans('messages.weight') }}:</strong> {{ (isset($updates->weight) ? $updates->weight : $user->$profileType->weight) }} <br>

							@if ( isset($user->$profileType->role_abroad) || isset($updates->role_abroad) )
								<strong>{{ trans('messages.role_abroad') }}</strong>

								@if (isset($updates->role_abroad))
									@if ($updates->role_abroad == 1)
										{{ trans('messages.yes') }}
									@else
										{{ trans('messages.no') }}
									@endif
								@else
									@if ($user->$profileType->role_abroad == 1)
										{{ trans('messages.yes') }}
									@else
										{{ trans('messages.no') }}
									@endif
								@endif
							@endif
						</div>
						<div class="col-sm-6">
							@if ($user->$profileType->hair_length || isset($updates->hair_length))
								<strong>{{ trans('messages.hair_length') }}:</strong> {{ (isset($updates->hair_length) ? $updates->hair_length : $user->$profileType->hair_length) }} <br>
							@endif
							@if ($user->sex || isset($updates->sex))
								<strong>{{ trans('messages.sex') }}:</strong> {{ (isset($updates->sex) ? $updates->sex : $user->sex) }} <br>
							@endif
							@if ($user->$profileType->ethnicity->name || isset($updates->ethnicity_id))
								<strong>{{ trans('messages.ethnicity') }}:</strong> {{ (isset($updates->ethnicity_id) ? $ethnicities[$updates->ethnicity_id] : $user->$profileType->ethnicity->name) }} <br>
							@endif
							@if ($user->$profileType->accent || isset($updates->accent))
								<strong>{{ trans('messages.accent') }}:</strong> {{ (isset($updates->accent) ? $updates->accent : $user->$profileType->accent) }} <br>
							@endif
						</div>
					</div>
				</div>

				<hr>

				<div class="proTitle">{{ trans('messages.general_skills') }}</div>

				<div class="wrapDescr">
					@if ( isset($user->$profileType->voice) || isset($updates->voice) )
						<strong>{{ trans('messages.voice') }}</strong>

						@if (isset($updates->voice))
							@if ($updates->voice == 1)
								{{ trans('messages.yes') }}
							@else
								{{ trans('messages.no') }}
							@endif
						@else
							@if ($user->$profileType->voice == 1)
								{{ trans('messages.yes') }}
							@else
								{{ trans('messages.no') }}
							@endif
						@endif
					@endif

					@if ($user->$profileType->dance || isset($updates->dance))
						<br><strong>{{ trans('messages.dance') }}:</strong>  {{ (isset($updates->dance) ? $updates->dance : $user->$profileType->dance) }}
					@endif

					@if (isset($updates->licences))
						<br><strong>{{ trans('messages.licence') }}:</strong>
						@foreach ($updates->licences as $licence)
							{{ $licences[$licence] }}
						@endforeach
					@else
						@if(!$user->licences->isEmpty())
							<br><strong>{{ trans('messages.licence') }}:</strong>
							@foreach ($user->licences->lists('name') as $licence)
								{{ $licence }}
							@endforeach
						@endif
					@endif

					@if (isset($updates->productions))
						<br><strong>{{ trans('messages.interested_in') }}:</strong>
						@foreach ($updates->productions as $production)
							{{ $productions[$production] }}
						@endforeach
					@else
						@if(!$user->productions->isEmpty())
							<br><strong>{{ trans('messages.production') }}:</strong>
							@foreach ($user->productions->lists('name') as $production)
								{{ $production }}
							@endforeach
						@endif
					@endif
				</div>
		
				<hr>
		
				<div class="proTitle">{{ trans('messages.media') }}</div>
		
				<div class="wrapDescr">
					<div class="strTitle">
					 	<strong>{{ trans('messages.images') }}</strong>
					 </div>

					@if (!$user->images->isEmpty())
						<div class="row">
							@foreach ($user->images as $image)
								@unless ($image->id == $user->thumb_id)
									<div class="col-sm-4 file">
										<div>
											<a href="{{ $image->path }}" class="fancybox" rel="group"><img src="{{ $image->thumb }}" class="img-responsive"></a>
										</div>
									</div>
								@endunless
							@endforeach
						</div>
					@else
						<p>{{ trans('messages.no_images') }}</p>
					@endif

					<div class="strTitle">
					 	<strong>{{ trans('messages.sound') }}</strong>
					 </div>
					
					@if (!$user->soundclouds->isEmpty())
						@foreach ($user->soundclouds as $soundcloud)
							<div class="scaudio">
								<iframe width="100%" height="450" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/{{ $soundcloud->code }}&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe>
							</div>
						@endforeach
					@else
						<p>{{ trans('messages.no_audio') }}</p>
					@endif

					<div class="strTitle">
						<strong>{{ trans('messages.video') }}</strong>
					</div>
					
					@if (!$user->vimeos->isEmpty() || !$user->youtubes->isEmpty())
						@foreach ($user->vimeos as $vimeo)
							<iframe src="https://player.vimeo.com/video/{{ $vimeo->code }}" width="555" height="300" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
						@endforeach

						@foreach ($user->youtubes as $youtube)
							<iframe width="555" height="300" src="https://www.youtube.com/embed/{{ $youtube->code }}" frameborder="0" allowfullscreen></iframe>
						@endforeach
					@else
						<p>{{ trans('messages.no_video') }}</p>
					@endif

				</div>

			</div>
		</div>

		<hr>

		<div class="text-center">
			<a href="{{ URL::previous() }}"  class="btn">{{ trans('messages.back') }}</a>
			@if (Auth::check())
				@if (Auth::user()->id != $user->id)
					<a href="{{ route('messages.new', [$user->id, App::getLocale()]) }}"  class="btn">{{ trans('messages.send_message') }}</a>
				@endif
				@if (isset($admin) && $admin)
					@if (isset($updates) && $updates)
						<a href="{{ route('admin.user.approve', $user->username) }}" class="btn">{{ trans('messages.approve_complete') }}</a>
						<a href="{{ route('admin.user.uncomplete', $user->username) }}" class="btn">{{ trans('messages.uncomplete') }}</a>
					@endif
				@endif
			@endif
		</div>
	</div>
	

@stop
