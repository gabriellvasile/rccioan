<div class="projects editable" data-type="project" data-remove="{{ route('profile.removeFromProfile', $user->username) }}">

	<legend class="clearfix legendWithBtn">
		{{ trans('messages.projects_title') }}
		<div class="btn btn-success pull-right addToProfile" data-url="{{ route('profile.ajaxNewProject', [$user->username, App::getLocale()]) }}">+ {{ trans('messages.new_project') }} </div>
	</legend>
	
	@foreach ($user->projects as $project)
		<div class="well project">
			<div class="btn btn-danger deleteFromProfile" data-id="{{ $project->id }}">-</div>
			<div class="row">
				<div class="col-sm-6">
					<div class="form-group">
						{!! Form::label('project_name' . $project->id, trans('messages.project_name')) !!}
						{!! Form::text(  'projects[' . $project->id . '][name]' , $project->name , ['placeholder' => trans('messages.project_name'), 'class' => 'form-control', 'id' => 'project_name'  . $project->id]) !!}
					</div>
				</div>
				
				<div class="col-sm-6">
					<div class="form-group">
						{!! Form::label('project_house' . $project->id, trans('messages.project_house')) !!}
						{!! Form::text(  'projects[' . $project->id . '][house]' , $project->house , ['placeholder' => trans('messages.project_house'), 'class' => 'form-control', 'id' => 'project_house'  . $project->id]) !!}
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-6">
					<div class="form-group">
						{!! Form::label('project_year' . $project->id, trans('messages.project_year')) !!}
						{!! Form::text(  'projects[' . $project->id . '][year]' , $project->year , ['placeholder' => trans('messages.project_year'), 'class' => 'form-control', 'id' => 'project_year'  . $project->id]) !!}
					</div>
				</div>
				
				<div class="col-sm-6">
					<div class="form-group">
						{!! Form::label('project_director' . $project->id, trans('messages.project_director')) !!}
						{!! Form::text(  'projects[' . $project->id . '][director]' , $project->director , ['placeholder' => trans('messages.project_director'), 'class' => 'form-control', 'id' => 'project_director'  . $project->id]) !!}
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-6">
					<div class="form-group">
						{!! Form::label('project_role' . $project->id, trans('messages.project_role')) !!}
						{!! Form::text(  'projects[' . $project->id . '][role]' , $project->role , ['placeholder' => trans('messages.project_role'), 'class' => 'form-control', 'id' => 'project_role'  . $project->id]) !!}
					</div>
				</div>

				<div class="col-sm-6">
					<div class="form-group">
						{!! Form::label('project_description' . $project->id, trans('messages.project_desc')) !!}
						{!! Form::textarea(  'projects[' . $project->id . '][description]' , $project->description , ['placeholder' => trans('messages.project_desc'), 'class' => 'form-control', 'rows' => 3, 'id' => 'project_description'  . $project->id]) !!}
					</div>
				</div>
			</div>
		</div>
	@endforeach
</div>