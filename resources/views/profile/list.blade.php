@extends('layouts.page')

<?php $listActors = true; ?>

@section('content')

	@include('includes._filter')

	<?php $i = 1; ?>
	@forelse($actors as $actor)

		@if ($i % 4 == 1)
			</div><div class="wrapBgGrayActori visible-lg"><div class="container wrapListareActori"><div class="row">
		@endif
			<div class="col-md-3">
				<div class="actor">
					@if ($actor->user->images)
						<div class="actorImageWrap">
							@foreach ($actor->user->images as $image)
								@if ($image->id == $actor->user->thumb_id)
									<div class="file">
										<a href="{{ $image->path }}"><img src="{{ $image->thumb }}" class="img-responsive"></a>
									</div>
								@endif
							@endforeach
						</div>
					@endif

					<div class="actorDetailsWrap">
						{{--<div class="actorType">{{ trans('messages.' . $actor->ActorType->slug . '_sg') }}</div>--}}
						<div class="userName"><a href="{{ route('profile.show', [$actor->user->username, App::getLocale()]) }}">{{ $actor->user->fname . ' ' . $actor->user->lname }}</a></div>

						<div class="wrapRanges">
							@if(!$actor->user->ranges->isEmpty())
								<div>{{ trans('messages.range') }}:</div>

								<div>
									@foreach($actor->user->ranges as $range)
										{{ $range->name }}
									@endforeach
								</div>
							@endif
						</div>

						<div class="country">{{ $actor->user->city }}, {{ $actor->user->country->name }}</div>

						<div class="row extraInfo">
							{{--<div class="col-md-6">--}}
								{{--<div class="m-title">{{ trans('messages.ethnicity') }}</div>--}}
								{{--<div class="m-desc">{{ $actor->user->actor->ethnicity->name }}</div>--}}
							{{--</div>--}}

							{{--<div class="col-md-6">--}}
								{{--<div class="m-title">{{ trans('messages.accent') }}</div>--}}
								{{--<div class="m-desc">{{ $actor->user->actor->accent }}</div>--}}
							{{--</div>--}}

							<div class="btns">
								<a href="{{ route('profile.show', [$actor->user->username, App::getLocale()]) }}"><i class="fa fa-user"></i></a>
								<a href="{{ route('messages.new', [$actor->user->id, App::getLocale()]) }}"><i class="fa fa-envelope-o"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		@if ($i % 4 == 0)
			</div></div>
		@endif
		<?php $i++; ?>
	@empty
		<h1 class="text-center" style="margin:100px 0">{{ trans('messages.no_actors') }}.</h1>
	@endforelse

	@if ($i % 4 != 1)
		</div></div>
	@endif
</div>

<div class="hidden-lg container mobileListing">
	<div class="row">
		@forelse($actors as $actor)
			<div class="col-sm-4 col-xs-6">
				<div class="actor">
					@if ($actor->user->images)
						<div class="actorImageWrap">
							@foreach ($actor->user->images as $image)
								@if ($image->id == $actor->user->thumb_id)
									<div class="file">
										<a href="{{ $image->path }}"><img src="{{ $image->thumb }}" class="img-responsive"></a>
									</div>
								@endif
							@endforeach
						</div>
					@endif

					<div class="actorDetailsWrap">
						<div class="userName"><a href="{{ route('profile.show', [$actor->user->username, App::getLocale()]) }}">{{ $actor->user->fname . ' ' . $actor->user->lname }}</a></div>

						@if(!$actor->user->ranges->isEmpty())
							<div>{{ trans('messages.range') }}:</div>

							<div>
								@foreach($actor->user->ranges as $range)
									{{ $range->name }}
								@endforeach
							</div>
						@endif
						<div class="country">{{ $actor->user->city }}, {{ $actor->user->country->name }}</div>

						<div class="row extraInfo">
							<div class="btns">
								<a href="{{ route('profile.show', [$actor->user->username, App::getLocale()]) }}"><i class="fa fa-user"></i></a>
								<a href="{{ route('messages.new', [$actor->user->id, App::getLocale()]) }}"><i class="fa fa-envelope-o"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		@empty
			<h1 class="text-center">{{ trans('messages.no_actors') }}.</h1>
		@endforelse
	</div>
</div>

@stop

@section('afterfooter')
@stop