
<div role="tabpanel" class="tab-pane fade" id="characteristicstab">
	{!! Form::model( $user, ['route' => ['profile.ajaxUpdateCharacteristics', $user->username, App::getLocale()], 'method' => 'PATCH', 'id' => 'characteristicsform', 'data-show' => route('profile.ajaxShowEducation', [$user->username,  App::getLocale()])] ) !!}

		<legend class="legendWithBtn">
			{{ trans('messages.characteristics') }}
			{!! Form::button(trans('messages.next'), ['class' => 'btn btn-primary profilesubmit', 'data-nexttab' => 'educationtab']) !!}
			{!! Form::button(trans('messages.back'), ['class' => 'btn btn-primary profilesubmit', 'data-nexttab' => 'usertab']) !!}
		</legend>

		<div class="row">
			<div class="col-sm-6">
			
				<div class="form-group">
				    {!! Form::label('hair_colour_id', trans('messages.hair_colour')) !!}
				    {!! Form::select(  $userType . '[hair_colour_id]', $haircolours, null , ['placeholder' => trans('messages.hair_colour'), 'class' => 'form-control', 'id' => 'hair_colour_id']) !!}
				</div>
				
				<div class="form-group">
				    {!! Form::label('eye_colour_id', trans('messages.eye_colour')) !!}
				    {!! Form::select( $userType . '[eye_colour_id]', $eyecolours, null , ['placeholder' => trans('messages.eye_colour'), 'class' => 'form-control', 'id' => 'eye_colour_id']) !!}
				</div>
				
			</div>

			<div class="col-sm-6">
				<div class="form-group">
				    {!! Form::label('height', trans('messages.height')) !!}
				    {!! Form::text( $userType . '[height]', null, ['placeholder' => trans('messages.height'), 'class' => 'form-control', 'id' => 'height']) !!}
				</div>

				<div class="form-group">
				    {!! Form::label('weight', trans('messages.weight')) !!}
				    {!! Form::text( $userType . '[weight]', null, ['placeholder' => trans('messages.weight'), 'class' => 'form-control', 'id' => 'weight']) !!}
				</div>
			</div>
		</div>
		<hr>
		<div class="row">
			<div class="col-sm-6">
			
				<div class="form-group">
				    {!! Form::label('ethnicity_id', trans('messages.ethnicity')) !!}
				    {!! Form::select(  $userType . '[ethnicity_id]', $ethnicities, null , ['placeholder' => trans('messages.ethnicity'), 'class' => 'form-control', 'id' => 'ethnicity_id']) !!}
				</div>

				<div class="form-group">
				    {!! Form::label('ranges', trans('messages.range')) !!}
					{!! Form::select('ranges[]', $ranges, $user->ranges->lists('id')->toArray() , ['class' => 'form-control', 'id' => 'ranges', 'multiple' => '']) !!}
				</div>
				
			</div>

			<div class="col-sm-6">
				<div class="form-group">
				    {!! Form::label('accent', trans('messages.accent')) !!}
				    {!! Form::text( $userType . '[accent]', null , ['placeholder' => trans('messages.accent'), 'class' => 'form-control', 'id' => 'accent']) !!}
				</div>

				<div class="form-group">
					{!! Form::label('hair_length', trans('messages.hair_length')) !!}
					{!! Form::select( $userType . '[hair_length]', $hairlengths, null, ['placeholder' => trans('messages.hair_length'), 'class' => 'form-control', 'id' => 'hair_length']) !!}
				</div>
			</div>
		</div>


		@if ($userType == 'actor')

		<div class="form-group">
			{!! Form::label('facial_traits', trans('messages.facial_traits')) !!}
			{!! Form::textarea( $userType . '[facial_traits]', null, ['placeholder' => trans('messages.facial_traits'), 'class' => 'form-control', 'id' => 'facial_traits', 'rows' => 2]) !!}
		</div>

		<div class="form-group">
			{!! Form::label('spoken_languages', trans('messages.spoken_lang')) !!}
			{!! Form::textarea( $userType . '[spoken_languages]', null, ['placeholder' => trans('messages.spoken_lang'), 'class' => 'form-control', 'id' => 'spoken_languages', 'rows' => 2]) !!}
		</div>
		
		<hr>

		<legend>{{ trans('messages.aptitudes') }}</legend>

		<div class="row">
			<div class="col-sm-6">
				<div class="form-group">
				    {!! Form::label('voice', trans('messages.voice')) !!}
				    {!! Form::select(  $userType . '[voice]', ['none' => trans('messages.select_answer'), trans('messages.no'), trans('messages.yes')], null , ['placeholder' => trans('messages.voice'), 'class' => 'form-control', 'id' => 'voice']) !!}
				</div>
			</div>
			<div class="col-sm-6">
				<div class="form-group">
					{!! Form::label('dance', trans('messages.dance')) !!}
					{!! Form::textarea( $userType . '[dance]', null, ['placeholder' => trans('messages.dance'), 'class' => 'form-control', 'id' => 'dance', 'rows' => 2]) !!}
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-6">
				<div class="form-group">
					{!! Form::label('instruments', trans('messages.instruments')) !!}
					{!! Form::textarea( $userType . '[instruments]', null, ['placeholder' => trans('messages.instruments'), 'class' => 'form-control', 'id' => 'instruments', 'rows' => 2]) !!}
				</div>
			</div>
			<div class="col-sm-6">
				<div class="form-group">
					{!! Form::label('sports', trans('messages.sports')) !!}
					{!! Form::textarea( $userType . '[sports]', null, ['placeholder' => trans('messages.sports'), 'class' => 'form-control', 'id' => 'sports', 'rows' => 2]) !!}
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-6">
				<div class="form-group">
				    {!! Form::label('licences', trans('messages.licence')) !!}
				    {!! Form::select('licences[]', $licences, $user->licences->lists('id')->toArray() , ['class' => 'form-control', 'id' => 'licences', 'multiple' => '']) !!}
				</div>
			</div>
		</div>

		@endif


		<div class="form-group">
			{!! Form::button(trans('messages.next'), ['class' => 'btn btn-primary profilesubmit', 'data-nexttab' => 'educationtab']) !!}
			{!! Form::button(trans('messages.back'), ['class' => 'btn btn-primary profilesubmit', 'data-nexttab' => 'usertab']) !!}
		</div>

	{!! Form::close() !!}
</div>