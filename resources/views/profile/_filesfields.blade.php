<div class="files editable" data-type="files" data-remove="{{ route('profile.removeFromProfile', $user->username) }}">
    <div class="alert alert-info fade in">
        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">x</button>
        <h4>{{ trans('messages.important') }}</h4>
        <p>{{ trans('messages.every_image') }}</p>
    </div>

    <legend class="legendWithBtn">
        {{ trans('messages.images') }}
        {!! Form::button(trans('messages.next'), ['class' => 'btn btn-primary profilesubmit', 'data-nexttab' => 'none']) !!}
        {!! Form::button(trans('messages.back'), ['class' => 'btn btn-primary profilesubmit', 'data-nexttab' => 'educationtab']) !!}
    </legend>

    <div class="row">

        <div class="col-sm-6">

            @if ($userType == 'actor')
                <div class="form-group">
                    {!! Form::label('sounds', trans('messages.upload_audio')) !!}
                    {!! Form::file('sounds[]', ['placeholder' => trans('messages.upload_audio'), 'id' => 'sounds', 'type' => 'file', 'class' => 'file', 'multiple' => 'multiple']) !!}
                </div>
            @endif
            <div class="form-group">
                {!! Form::label('images', trans('messages.new_image')) !!}
                {!! Form::file('images[]', ['placeholder' => trans('messages.new_image'), 'id' => 'images', 'type' => 'file', 'class' => 'file', 'multiple' => 'multiple']) !!}
            </div>

            @if ($user->images)
                <div class="row">
                    @foreach ($user->images as $image)
                        @unless ($image->id == $user->thumb_id)
                            <div class="col-sm-4 file">
                                <div>
                                    <div class="btn btn-danger deleteFromProfile" data-id="{{ $image->id }}">-</div>
                                    <a href="{{ $image->path }}"><img src="{{ $image->thumb }}" class="img-responsive"></a>
                                </div>
                            </div>
                        @endunless
                    @endforeach
                </div>
            @endif
        </div>

        <div class="col-sm-6">
            <div class="form-group">
                {!! Form::label('profile', trans('messages.profile_image')) !!}
                {!! Form::file('profile', ['placeholder' => trans('messages.profile_image'), 'id' => 'profile', 'type' => 'file', 'class' => 'file']) !!}
            </div>

            @if ($user->images)
                <div class="row">
                    @foreach ($user->images as $image)
                        @if ($image->id == $user->thumb_id)
                            <div class="col-sm-4 file">
                                <div>
                                    <div class="btn btn-danger deleteFromProfile" data-id="{{ $image->id }}">-</div>
                                    <a href="{{ $image->path }}"><img src="{{ $image->thumb }}" class="img-responsive"></a>
                                </div>
                            </div>
                        @endif
                    @endforeach
                </div>
            @endif
        </div>

        @if ($userType != 'actor')
            <div class="col-sm-6">
                <div class="form-group">
                    {!! Form::label('logo', 'Logo') !!}
                    {!! Form::file('logo', ['placeholder' => 'Logo', 'id' => 'logo', 'type' => 'file', 'class' => 'file']) !!}
                </div>

                @if ($user->images)
                    <div class="row">
                        @foreach ($user->images as $image)
                            @if ($image->id == $user->logo_id)
                                <div class="col-sm-4 file">
                                    <div>
                                        <div class="btn btn-danger deleteFromProfile" data-id="{{ $image->id }}">-</div>
                                        <a href="{{ $image->path }}"><img src="{{ $image->thumb }}" class="img-responsive"></a>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    </div>
                @endif
            </div>
        @endif
    </div>
</div>

@if ($userType == 'actor')
    <hr>

    <legend>{{ trans('messages.video') }}</legend>

    <div class="vimeos editable" data-type="vimeos" data-remove="{{ route('profile.removeFromProfile', $user->username) }}" data-link="{{ route('profile.ajaxNewVimeo', $user->username) }}">
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    {!! Form::label('vimeo', trans('messages.add_vimeo')) !!}
                    {!! Form::text('vimeo', null, ['placeholder' => trans('messages.add_vimeo'), 'class' => 'form-control videoinput', 'id' => 'vimeo']) !!}
                </div>
            </div>

            <div class="col-sm-6">
                <div class="form-group">
                    {!! Form::label('addVimeo',trans('messages.vimeo_help')) !!}
                    {!! Form::button('+ ' . trans('messages.add_vimeo'), ['class' => 'btn btn-success addVideo', 'id' => 'addVimeo' ]) !!}
                </div>
            </div>
        </div>

        <div class="row vmvideos addVideosHere">
            @foreach ($user->vimeos as $vimeo)
                <div class="col-sm-6 vmvideo">
                    <div class="btn btn-danger deleteFromProfile" data-id="{{ $vimeo->id }}">-</div>
                    <iframe src="https://player.vimeo.com/video/{{ $vimeo->code }}" width="555" height="300" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                </div>
            @endforeach
        </div>
    </div>

    <hr>

    <div class="youtubes editable" data-type="youtubes" data-remove="{{ route('profile.removeFromProfile', $user->username) }}" data-link="{{ route('profile.ajaxNewVimeo', $user->username) }}">
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    {!! Form::label('youtube', trans('messages.add_youtube')) !!}
                    {!! Form::text('youtube', null, ['placeholder' => trans('messages.add_youtube'), 'class' => 'form-control videoinput', 'id' => 'youtube']) !!}
                </div>
            </div>

            <div class="col-sm-6">
                <div class="form-group">
                    {!! Form::label('addYotube', trans('messages.youtube_help')) !!}
                    {!! Form::button('+ ' . trans('messages.add_youtube'), ['class' => 'btn btn-success addVideo', 'id' => 'addYoutube']) !!}
                </div>
            </div>
        </div>

        <div class="row ytvideos addVideosHere">
            @foreach ($user->youtubes as $youtube)
                <div class="col-sm-6 ytvideo">
                    <div class="btn btn-danger deleteFromProfile" data-id="{{ $youtube->id }}">-</div>
                    <iframe width="555" height="300" src="https://www.youtube.com/embed/{{ $youtube->code }}" frameborder="0" allowfullscreen></iframe>
                </div>
            @endforeach
        </div>
    </div>

    <hr>
    <legend>{{ trans('messages.upload_audio') }}</legend>
    <div class="soundclouds editable" data-type="soundclouds" data-remove="{{ route('profile.removeFromProfile', $user->username) }}" data-link="{{ route('profile.ajaxNewVimeo', $user->username) }}">
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    {!! Form::label('soundcloud', trans('messages.add_soundcloud')) !!}
                    {!! Form::text('soundcloud', null, ['placeholder' => trans('messages.add_soundcloud'), 'class' => 'form-control videoinput', 'id' => 'soundcloud']) !!}
                </div>
            </div>

            <div class="col-sm-6">
                <div class="form-group">
                    {!! Form::label('addSoundcloud', trans('messages.soundcloud_help')) !!}
                    {!! Form::button('+ ' . trans('messages.add_soundcloud'), ['class' => 'btn btn-success addVideo', 'id' => 'addSoundcloud']) !!}
                </div>
            </div>
        </div>

        <div class="row scaudios addVideosHere">
            @foreach ($user->soundclouds as $soundcloud)
                <div class="col-sm-6 scaudio">
                    <div class="btn btn-danger deleteFromProfile" data-id="{{ $soundcloud->id }}">-</div>
                    <iframe width="100%" height="450" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/{{ $soundcloud->code }}&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe>
                </div>
            @endforeach
        </div>
    </div>
@endif