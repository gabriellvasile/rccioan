<div class="well project" data-id="{{ $key }}">
	<div class="btn btn-danger deleteFromProfile">-</div>
	<div class="row">
		<div class="col-sm-6">
			<div class="form-group">
				<label for="project_name{{ $key }}">{{ trans('messages.project_name') }}</label>
				<input id="project_name{{ $key }}" class="form-control" type="text" value="" name="projects[{{ $key }}][name]" placeholder="{{ trans('messages.project_name') }}">
			</div>
		</div>
		<div class="col-sm-6">
			<div class="form-group">
				<label for="project_house{{ $key }}">{{ trans('messages.project_house') }}</label>
				<input id="project_house{{ $key }}" class="form-control" type="text" value="" name="projects[{{ $key }}][house]" placeholder="{{ trans('messages.project_house') }}">
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<div class="form-group">
				<label for="project_year{{ $key }}">{{ trans('messages.project_year') }}</label>
				<input id="project_year{{ $key }}" class="form-control" type="text" value="" name="projects[{{ $key }}][year]" placeholder="{{ trans('messages.project_year') }}">
			</div>
		</div>
		<div class="col-sm-6">
			<div class="form-group">
				<label for="project_director{{ $key }}">{{ trans('messages.project_director') }}</label>
				<input id="project_director{{ $key }}" class="form-control" type="text" value="" name="projects[{{ $key }}][director]" placeholder="{{ trans('messages.project_director') }}">
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-6">
			<div class="form-group">
				<label for="project_role{{ $key }}">{{ trans('messages.project_role') }}</label>
				<input id="project_role{{ $key }}" class="form-control" type="text" value="" name="projects[{{ $key }}][role]" placeholder="{{ trans('messages.project_role') }}">
			</div>
		</div>
		<div class="col-sm-6">
			<div class="form-group">
				<label for="project_description{{ $key }}">{{ trans('messages.project_desc') }}</label>
				<textarea id="project_description{{ $key }}" rows="3" class="form-control" type="text" value="" name="projects[{{ $key }}][description]" placeholder="{{ trans('messages.project_desc') }}"></textarea>
			</div>
		</div>
	</div>
</div>

