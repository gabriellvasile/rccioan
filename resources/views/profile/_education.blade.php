<div class="row">
	<div class="col-sm-6">
		<div class="form-group">
		    {!! Form::label('profesional_studies', trans('messages.professional_studies')) !!}
		    {!! Form::select(   'actor[profesional_studies]', ['None' => trans('messages.select_answer'), '0' => trans('messages.no'), '1' => trans('messages.yes')], null , ['placeholder' => trans('messages.professional_studies'), 'class' => 'form-control', 'id' => 'profesional_studies']) !!}
		</div>
	</div>
</div>

<div class="well optionalrow facultyrow">
	<div class="row">
		<div class="col-sm-6">
			<div class="form-group">
			    {!! Form::label('faculty', trans('messages.faculty')) !!}
			    {!! Form::select(   'actor[faculty]', ['None' => trans('messages.select_answer'), '0' => trans('messages.no'), '1' => trans('messages.yes')], null , ['placeholder' => trans('messages.faculty'), 'class' => 'form-control', 'id' => 'faculty']) !!}
			</div>
		</div>
	</div>
	<div class="row facultyOptions">
		<div class="col-sm-6">
			<div class="form-group">
			    {!! Form::label('faculty_institution', trans('messages.faculty_inst')) !!}
			    {!! Form::select(   'actor[faculty_institution]', $institutions, null , ['placeholder' => trans('messages.faculty_inst'), 'class' => 'form-control', 'id' => 'faculty_institution']) !!}
			</div>
			<div class="form-group">
			    {!! Form::label('faculty_other', trans('messages.other_inst')) !!}
			    {!! Form::text(   'actor[faculty_other]', null , ['placeholder' => trans('messages.other_inst'), 'class' => 'form-control', 'id' => 'faculty_other']) !!}
			</div>
		</div>
		<div class="col-sm-6">
			<div class="form-group">
			    {!! Form::label('faculty_domain', trans('messages.field')) !!}
			    {!! Form::text(   'actor[faculty_domain]', null , ['placeholder' => trans('messages.field'), 'class' => 'form-control', 'id' => 'faculty_domain']) !!}
			</div>
			<div class="form-group" id="toHideFacultyYear">
			    {!! Form::label('faculty_year', trans('messages.faculty_year')) !!}
			    {!! Form::select(   'actor[faculty_year]', $years, null , ['placeholder' => trans('messages.faculty_year'), 'class' => 'form-control', 'id' => 'faculty_year']) !!}
			</div>
			<div class="form-group">
			    {!! Form::checkbox(   'actor[faculty_student]', 1, null , ['id' => 'faculty_student']) !!}
			    {!! Form::label('faculty_student', trans('messages.still_student')) !!}
			</div>
		</div>
	</div>
</div>

<div class="well optionalrow specialityrow">
	<div class="row">
		<div class="col-sm-6">
			<div class="form-group">
			    {!! Form::label('speciality_studies', trans('messages.speciality_studies')) !!}
			    {!! Form::select(   'actor[speciality_studies]', ['None' => trans('messages.select_answer'), '0' => trans('messages.no'), '1' => trans('messages.yes')], null , ['placeholder' => trans('messages.speciality_studies'), 'class' => 'form-control', 'id' => 'speciality_studies']) !!}
			</div>
		</div>
	</div>
	<div class="row specialityOptions">
		<div class="col-sm-6">
			<div class="form-group">
			    {!! Form::label('speciality_institution', trans('messages.speciality_institution')) !!}
			    {!! Form::text(   'actor[speciality_institution]', null , ['placeholder' => trans('messages.speciality_institution'), 'class' => 'form-control', 'id' => 'speciality_institution']) !!}
			</div>
		</div>
		<div class="col-sm-6">
			<div class="form-group">
			    {!! Form::label('speciality_year', trans('messages.speciality_year')) !!}
			    {!! Form::select(   'actor[speciality_year]', $years, null , ['placeholder' => trans('messages.speciality_year'), 'class' => 'form-control', 'id' => 'speciality_year']) !!}
			</div>
		</div>
	</div>
</div>

<div class="awardsAndWorksopsrow">
	<div class="form-group">
		{!! Form::label('awards', trans('messages.awards')) !!}
		{!! Form::textarea('actor[awards]', null , ['placeholder' => trans('messages.awards'), 'class' => 'form-control', 'id' => 'awards', 'rows' => 3]) !!}
	</div>
	<div class="form-group">
		{!! Form::label('workshops', trans('messages.workshops')) !!}
		{!! Form::textarea('actor[workshops]', null , ['placeholder' => trans('messages.workshops'), 'class' => 'form-control', 'id' => 'workshops', 'rows' => 3]) !!}
	</div>
</div>

<hr>