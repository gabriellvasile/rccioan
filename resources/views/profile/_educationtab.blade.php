
<div role="tabpanel" class="tab-pane fade" id="educationtab">
	{!! Form::model( $user, ['route' => ['profile.ajaxUpdateEducation', $user->username, 'locale' => App::getLocale()], 'method' => 'PATCH', 'id' => 'educationform', 'data-show' => route('profile.ajaxShowFiles', [$user->username, App::getLocale()])] ) !!}

		{!! Form::hidden('locale', App::getLocale()) !!}

		<legend class="legendWithBtn">
			{{ trans('messages.education') }}
			{!! Form::button(trans('messages.next'), ['class' => 'btn btn-primary profilesubmit', 'data-nexttab' => 'filestab']) !!}
			{!! Form::button(trans('messages.back'), ['class' => 'btn btn-primary profilesubmit', 'data-nexttab' => 'characteristicstab']) !!}
		</legend>

		@include('profile._education')
		@include('profile._projects')
		{{--@include('profile._workshops')--}}
		@include('profile._info')
		
		<div class="form-group">
			{!! Form::button(trans('messages.next'), ['class' => 'btn btn-primary profilesubmit', 'data-nexttab' => 'filestab']) !!}
			{!! Form::button(trans('messages.back'), ['class' => 'btn btn-primary profilesubmit', 'data-nexttab' => 'characteristicstab']) !!}
		</div>
	{!! Form::close() !!}
</div>