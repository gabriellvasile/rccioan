<div role="tabpanel" class="tab-pane fade in" id="filestab">
	{!! Form::model( $user, ['route' => ['profile.ajaxUpdateFiles', $user->username], 'method' => 'POST', 'id' => 'filesform', 'files'=>true, 'data-show' => route('profile.ajaxComplete', $user->username)] ) !!}

		@include('profile._filesfields')

		<div class="form-group">
			{!! Form::button(trans('messages.next'), ['class' => 'btn btn-primary profilesubmit', 'data-nexttab' => 'none']) !!}
			{!! Form::button(trans('messages.back'), ['class' => 'btn btn-primary profilesubmit', 'data-nexttab' => 'educationtab']) !!}
		</div>
	{!! Form::close() !!}
</div>