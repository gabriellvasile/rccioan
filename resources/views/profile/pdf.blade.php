<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<div class="profile">
    <table class="row">
        <tr>
            <td style="padding-right: 30px;">
                <table class="row">
                    <tr>
                        <td colspan="2"><h1><div class="name">{{ $user->fname . ' ' . $user->lname }}</div></h1></td>
                    </tr>
                    <tr>
                        <td>Tip: </td> <td>{{ $user->actor->actortype->name }}</td>
                    </tr>
                    <tr>
                        <td style="padding-right: 80px;">Localitate: </td> <td>{{ $user->city }} - {{ $user->country->name }}</td>
                    </tr>
                    <tr>

                        <td>Email: </td> <td>{{ $user->email }}</td>
                    </tr>
                    <tr>
                        <td>Telefon: </td> <td>{{ $user->mobile }}</td>
                    </tr>
                    <tr>
                        @if(!$user->ranges->isEmpty())
                            <td>{{ trans('messages.range') }}: </td>
                            <td>
                                @foreach($user->ranges as $range)
                                    {{ $range->name }}
                                @endforeach
                            </td>
                        @endif
                    </tr>
                </table>

                <h3 class="proTitle">Detalii</h3>

                <table>
                    <tr>
                        <td style="padding-right: 150px;">Culoare păr</td> <td>{{ $user->$userType->haircolour->name }} </td>
                    </tr>
                    <tr>
                        <td>Culoare ochi</td> <td> {{ $user->$userType->eyecolour->name }} </td>
                    </tr>
                    <tr>
                        <td>Inaltime</td> <td>{{ $user->$userType->height }} </td>
                    </tr>
                    <tr>
                        <td>Greutate</td> <td>{{ $user->$userType->weight }} </td>
                    </tr>
                    <tr>
                        @if ( isset($user->$userType->role_abroad) )
                            <td>Acceptati roluri in strainatate</td>
                            <td>
                                @if ($user->$userType->role_abroad == 1)
                                    {{ trans('messages.yes') }}
                                @else
                                    {{ trans('messages.no') }}
                                @endif
                            </td>
                        @endif
                    </tr>
                    @if ($user->$userType->hair_length)
                        <tr>
                            <td>Lungimea parului</td> <td>{{ $user->$userType->hair_length }} </td>
                        </tr>
                    @endif
                    @if ($user->sex)
                        <tr>
                            <td>Sex</td> <td>{{ $user->sex }} </td>
                        </tr>
                    @endif
                    @if ($user->$userType->ethnicity->name)
                        <tr>
                            <td>Etnie</td> <td>{{ $user->$userType->ethnicity->name }} </td>
                        </tr>
                    @endif
                    @if ($user->$userType->accent)
                        <tr>
                            <td>Accent nativ</td> <td>{{ $user->$userType->accent }} </td>
                        </tr>
                    @endif
                </table>

            </td>

            <td>
                <?php $x = false; ?>
                @if ($user->images)
                    <div class="row">
                        @foreach ($user->images as $image)
                            <?php if ($image->id == $user->thumb_id) { ?>
                            <div class="col-sm-4 file">
                                <div>
                                    <a href="{{ $image->path }}"><img src="{{ $image->thumb }}" class="img-responsive"></a>
                                </div>
                            </div>
                            <?php
                            $x = true;
                            } ?>
                        @endforeach
                    </div>
                @endif

                <?php if ( !$x ) { ?>
                <img src="{{ route('home') . '/images/user.jpg' }}" alt="{{ $user->fname . ' ' . $user->lname }}" id="userImage" class="img-responsive">
                <?php } ?>
            </td>

        </tr>
    </table>

    <h3 class="proTitle">Aptitudini generale</h3>

    <div class="wrapDescr">
        @if ($user->$userType->dance)
            <br><strong>Dans:</strong>  {{ $user->$userType->dance }}
        @endif

        @if (!$user->licences->isEmpty())
            <br><strong>Permis conducere:</strong>
            @foreach ($user->licences->lists('name') as $licence)
                {{ $licence . ', '}}
            @endforeach
        @endif


        @if(!$user->productions->isEmpty())
            <br><strong>Interesat mai ales de:</strong>

            @foreach ($user->productions->lists('name') as $production)
                {{ $production . ', '}}
            @endforeach
        @endif
    </div>

    <div class="row">
        <div class="col-sm-6">
            <h3 class="proTitle">Premii</h3>

            <div class="wrapDescr">
                @if(count($user->$userType->awards))
                    {{ $user->$userType->awards }}
                @else
                    Acest utilizator nu are premii
                @endif
            </div>

            <h3 class="proTitle">Cursuri de specialitate &amp; workshop-uri</h3>

            <div class="wrapDescr">
                @if($user->$userType->workshops)
                    {{ $user->$userType->workshops }}
                @else
                    Acest utilizator nu a participat la Workshop-uri &amp; Festivaluri
                @endif
            </div>

            <div style="page-break-after:always;"></div>

            <h3 class="proTitle">Studii</h3>

            <div class="wrapDescr">
                <strong>Studii profesionale:</strong>
                @if ( $user->$userType->profesional_studies == 1 )
                    {{ trans('messages.yes') }} <br>
                    <strong>Studii universare:</strong>
                    @if ( $user->$userType->faculty == 1 )
                        {{ trans('messages.yes') }}
                    @else
                        {{ trans('messages.no') }}
                    @endif

                    <br>
                    <strong>Anul absolvirii:</strong> {{ $user->$userType->faculty_year }} <br>
                    <strong>Inca student?</strong>
                    @if ( $user->$userType->faculty_student == 1 )
                        {{ trans('messages.yes') }}
                    @else
                        {{ trans('messages.no') }}
                    @endif

                    <br>
                @else
                    {{ trans('messages.no') }} <br>
                @endif

                <strong>Studii de specialitate:</strong>
                @if ( $user->$userType->speciality_studies == 1 )
                    {{ trans('messages.yes') }} <br>
                    <strong>Anul absolvirii:</strong>  {{ $user->$userType->speciality_year }} <br>
                    <strong>Institutie:</strong>  {{ $user->$userType->speciality_institution }} <br>
                @else
                    {{ trans('messages.no') }} <br>
                @endif
            </div>

            <h3 class="proTitle">Informatii suplimentare</h3>
            <div class="wrapDescr">
                @if( $user->$userType->info )
                    {{ $user->$userType->info }}
                @else
                    Acest utilizator nu a adaugat informatii suplimentare
                @endif
            </div>

        </div>
    </div>

</div>