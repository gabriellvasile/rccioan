
@extends('layouts.page')

@section('content')

	<div class="replyWrap">
		<h1>{{ trans('messages.message') }}</h1>
		<hr>

		@include('errors._validation')

		{!! Form::open( ['route' => 'messages.send'] ) !!}

			<div class="row">
				<div class="col-sm-10">
					<div class="form-group" data-uri="{{ route('messages.searchRecipient') }}">
						{!! Form::label('recipient', trans('messages.recipient')) !!}
						@if(isset($fullName))
							{!! Form::text( 'recipient', $fullName, [ 'class' => 'form-control', 'disabled' => 'disabled', 'id' => 'recipient', 'autocomplete' => 'off']) !!}
						@else
							{!! Form::text( 'recipient', '', [ 'class' => 'form-control', 'id' => 'recipient', 'placeholder' => trans('messages.start_typing'), 'autocomplete' => 'off']) !!}
						@endif
                        {!! Form::hidden('id', isset($user->id) ? $user->id : null, ['id' => 'id']) !!}
						<div id="recipients"></div>
					</div>
				</div>
				<div class="col-sm-2">
					<div class="form-group importantWrap">
						{!! Form::checkbox( 'important', null, null, ['id' => 'important']) !!}	
						{!! Form::label('important', trans('messages.important') . '?') !!}
					</div>
				</div>
			</div>

			<div class="form-group">
				{!! Form::label('subject', trans('messages.subject')) !!}
				{!! Form::text( 'subject', null, ['placeholder' => trans('messages.subject'), 'class' => 'form-control', 'id' => 'subject']) !!}
			</div>
			
			<div class="form-group">
				{!! Form::label('message', trans('messages.message')) !!}
				{!! Form::textarea( 'message', null, ['placeholder' => trans('messages.message'), 'class' => 'form-control', 'id' => 'message']) !!}
			</div>

			<div class="form-group">
				{!! Form::submit(trans('messages.send'), ['class' => 'btn']) !!}
			</div>

		{!! Form::close() !!}
	</div>

@endsection

@section('afterfooter')
	<script src="//tinymce.cachefly.net/4.1/tinymce.min.js"></script>

    <script type="text/javascript">
      $(function () {

        tinymce.init({
            selector: "textarea"
		});

		$('#recipient').on('keyup', function() {
			var $this = $(this),
				value = $this.val();
			if ($this.attr('selected') == undefined && value.length > 1) {
				$.ajax({
					url: $(this).parent().data('uri'),
					method: 'get',
					dataType: 'json',
					data: {
						'text' 	 : $this.val(),
					},

					beforeSend: function() {
						//_showLoading();
					},
					success: function (users) {
						html = '<ul class="ajaxRecipients">';
						$.each(users, function(index, user) {
							html += '<li data-id=' + user['id'] + '>' + user['fname'] + ' ' + user['lname'] + '</li>';
						});
						html += '</ul>';
						$('#recipients').html(html);
						// parent.find('.addVideosHere').append(data);
						//_hideLoading();
					},
				});
			}
		});

        $('.replyWrap').on('click', '.ajaxRecipients li', function() {
            $('#recipient').val($(this).text());
            $('#id').val($(this).data('id'));
            $('#recipients').html('');
        });
        
      });
    </script>
@stop