
@extends('layouts.page')

@section('content')

	<div class="forwardWrap">
		<h1>Raspuns mesaj</h1>
		<hr>

		{!! Form::open( ['route' => ['messages.doforward', $message->id]] ) !!}
			<div class="form-group">
				{!! Form::label('recipient', trans('messages.recipient')) !!}
				{!! Form::text( 'recipient', null, ['placeholder' => trans('messages.recipient'), 'class' => 'form-control', 'id' => 'recipient']) !!}
			</div>

			<div class="form-group">
				{!! Form::label('subject', trans('messages.subject')) !!}
				{!! Form::text( 'subject', $message->subject, ['placeholder' => trans('messages.subject'), 'class' => 'form-control', 'id' => 'subject']) !!}
			</div>
			
			<div class="form-group">
				{!! Form::label('message', trans('messages.message')) !!}
				{!! Form::textarea( 'message', null, ['placeholder' => trans('messages.message'), 'class' => 'form-control', 'id' => 'message']) !!}
			</div>

			<div class="form-group">
				{!! Form::submit(trans('messages.send'), ['class' => 'btn']) !!}
				{!! Form::submit(trans('messages.save'), ['class' => 'btn btn-blue']) !!}
			</div>

		{!! Form::close() !!}
	</div>

@endsection

@section('afterfooter')
	<script src="//tinymce.cachefly.net/4.1/tinymce.min.js"></script>

    <script type="text/javascript">
      $(function () {

        tinymce.init({
            selector: "textarea"
         });
        
      });
    </script>
@stop