
<img src="{{ $message->embed(asset('images/logo.png')) }}" />

<h1>{{ trans('messages.hello') }} {{ $user->fname }} !</h1>

<p>
    {{ trans('messages.be_notified') }}
    <a href="{{ route('jobs.show', $job->id) }}">{{ $job->production_name }}</a>
    {{ trans('messages.verify_them') }}
    <a href="{{ route('jobs.show', $job->id) }}">{{ trans('messages.view_casting') }}</a>
</p>

<p>
    <br/>
    <strong>{{ trans('messages.rcc_team') }}</strong>
</p>