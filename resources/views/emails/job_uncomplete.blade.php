
<img src="{{ $message->embed(asset('images/logo.png')) }}" />

<h1>{{ trans('messages.hello') }}
    @if ($user->role_id == 4 && $user->employer->company)
        {{ $user->employer->company }}
    @else
        {{ $user->fname }}
    @endif
    !</h1>

<p>
    {{ trans('messages.sorry_job_updates') }}
    <a href="{{ route('jobs.show', $job->id) }}">{{ $job->production_name }}</a>
    {{ trans('messages.were_disapproved') }}.
    {{ trans('messages.for_more_more_details') }}
    {!! link_to_route('page.show', trans('messages.contact_us'), ['contact']) !!}
    {{ trans('messages.anytime') }}.
</p>

<p>
    <br/>
    <strong>{{ trans('messages.rcc_team') }}</strong>
</p>