
<img src="{{ $message->embed(asset('images/logo.png')) }}" />

<h1>{{ trans('messages.hello') }}
    @if ($user->role_id == 4 && $user->employer->company)
        {{ $user->employer->company }}
    @else
        {{ $user->fname }}
    @endif
    !</h1>

<p>
    {{ trans('messages.the_job_modifs') }}
    <a href="{{ route('jobs.show', $job->id) }}">{{ $job->production_name }}</a>
    {{ trans('messages.were_approved') }} <br>
</p>

<p>
    <br/>
    <strong>{{ trans('messages.rcc_team') }}</strong>
</p>