
<img src="{{ $message->embed(asset('images/logo.png')) }}" />

<h1>{{ trans('messages.hello') }}
    @if ($user->role_id == 4 && $user->employer->company)
        {{ $user->employer->company }}
    @else
        {{ $user->fname }}
    @endif
!</h1>

<p>{{ trans('messages.profile_approved') }}</p>

<strong>{!! link_to_route('login', trans('messages.login')) !!}</strong> ! </p>

<a href="{{ route('login') }}" target="_blank">{{ route('login') }}</a>

<p>
    <br/>
    <strong>{{ trans('messages.rcc_team') }}</strong>
</p>
