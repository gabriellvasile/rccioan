
<img src="{{ $message->embed(asset('images/logo.png')) }}" />

<h1>{{ trans('messages.hello') }} {{ $fname }}!</h1>

<p>{{ trans('messages.confirm_registration') }} {{ url('auth/confirm/'.$link) }}</p>

<p>
    <br/>
    <strong>{{ trans('messages.rcc_team') }}</strong>
</p>