
<img src="{{ $message->embed(asset('images/logo.png')) }}" />

<h1>{{ trans('messages.hello') }} {{ $user->fname }}!</h1>

<p>
    {{ trans('messages.account_welcome') }} <br/>
    {{ trans('messages.account_created') }}
</p>
<p>
    <em>e-mail:</em> <strong>{{ $user->email }}</strong> <br/>
    <em>{{ trans('messages.password') }}:</em> <strong>{{ $password }}</strong>
</p>

<h3>
    {{ trans('messages.change_password') }}
    <strong>{!! link_to_route('login', trans('messages.logit')) !!}</strong> !
</h3>

<p>
    <br/>
    <strong>{{ trans('messages.rcc_team') }}</strong>
</p>
