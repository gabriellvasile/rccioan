
<img src="{{ $message->embed(asset('images/logo.png')) }}" />

<h1>{{ trans('messages.hello') }} {{ $user->fname }}!</h1>

<p>
    {{ trans('messages.account_modify_request_registered') }} <br/>
</p>
<p>
    {{ trans('messages.thank_you_for_understanding') }} <br/>
</p>

<p>
    <br/>
    <strong>{{ trans('messages.rcc_team') }}</strong>
</p>
