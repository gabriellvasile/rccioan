
<img src="{{ $message->embed(asset('images/logo.png')) }}" />

<h1>{{ trans('messages.hello') }} {{ $user->fname }}!</h1>

<p>{{ trans('messages.profile_approved_to') }} <em>{{ $character->title }}</em> {{ trans('messages.at_job') }}<em>"{!! link_to_route('jobs.show', $character->job->production_name, [$character->job->id]) !!}"</em> ! </p>

{{ trans('messages.for_more_details') }} <a href="{{ route('jobs.show', $character->job->id) }}" target="_blank">{{ trans('messages.vizualiza_job') }}</a>

<p>
    <br/>
    <strong><em>{{ trans('messages.rcc_team') }}</em></strong>
</p>