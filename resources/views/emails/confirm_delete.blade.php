
<img src="{{ $message->embed(asset('images/logo.png')) }}" />

<h1>{{ trans('messages.hello') }} {{ $fname }}!</h1>

<p>
    {{ trans('messages.confirm_delete') }} <br/>
    {!! link_to_route('profile.confirmDelete', trans('messages.delete_account'), ['code' => $code]) !!}
</p>

<p>
    <br/>
    <strong>{{ trans('messages.rcc_team') }}</strong>
</p>
