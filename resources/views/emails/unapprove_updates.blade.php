
<img src="{{ $message->embed(asset('images/logo.png')) }}" />

<h1>{{ trans('messages.hello') }} {{ $user->fname }}!</h1>

<p>
    {{ trans('messages.updates_unapproved') }}
    {!! link_to_route('page.show', trans('messages.contact_us'), ['contact']) !!}
    {{ trans('messages.anytime') }}
</p>

<p>
    <br/>
    <strong>{{ trans('messages.rcc_team') }}</strong>
</p>
