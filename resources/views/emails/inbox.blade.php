
<img src="{{ $message->embed(asset('images/logo.png')) }}" />

<h1>{{ trans('messages.hello') }} {{ $m->recipient->fname }}!</h1>

<p>
    {{ trans('messages.got_mail_from') }} <strong>{{ $m->sender->fname }}</strong> {{ trans('messages.with_subject') }} <em>"{{ $m->subject }}"</em>
    {{ trans('messages.access_inbox') }}
</p>

<strong>{!! link_to_route('messages.list', trans('messages.view_inbox')) !!}</strong> !

<p>
    <br/>
    <strong>{{ trans('messages.rcc_team') }}</strong>
</p>
