<h1>{{ trans('messages.expired_castings') }}</h1>

@if ($jobs)
    @foreach ($jobs as $job_id => $job)
        <h2 style="border-bottom: 1px solid #000; margin-bottom: 10px;">{{ $job['production_name'] }}</h2>
        <h3 style="margin-top: 10px"><a href="{{ route('jobs.applicants', $job_id) }}">{{ trans('messages.view_applicants') }}</a></h3>
        <br/><br/>
    @endforeach
@endif
