
<img src="{{ $message->embed(asset('images/logo.png')) }}" />

<h1>{{ trans('messages.hello') }},</h1>
<p>
    {{ trans('messages.the_user') }}
    <a href="{{ route('profile.preview', $user->username) }}">
        @if ($user->role_id == 4 && $user->employer->company)
            {{ $user->employer->company }}
        @else
            {{ $user->fname . ' ' . $user->lname }}
        @endif
    </a>
    {{ trans('messages.updated_his_account') }}
</p>

<p>
    <a href="{{ route('profile.preview', $user->username) }}">{{ trans('messages.view_updates') }}</a>
</p>

<p>
    <strong>{{ trans('messages.five_days_tops') }}</strong>
</p>