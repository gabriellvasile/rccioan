
<img src="{{ $message->embed(asset('images/logo.png')) }}" />

<h1>{{ trans('messages.hello') }} {{ $user->fname }}!</h1>

<p>
    {{ trans('messages.updates_approved') }}
</p>

<strong>{!! link_to_route('login', trans('messages.login')) !!}</strong>

<p>
    <br/>
    <strong>{{ trans('messages.rcc_team') }}</strong>
</p>
