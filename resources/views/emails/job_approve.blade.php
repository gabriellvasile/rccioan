
<img src="{{ $message->embed(asset('images/logo.png')) }}" />

<h1>{{ trans('messages.hello') }}
    @if ($user->role_id == 4 && $user->employer->company)
        {{ $user->employer->company }}
    @else
        {{ $user->fname }}
    @endif
    !</h1>

<p>
    {{ trans('messages.the_job') }}
    <a href="{{ route('jobs.show', $job->id) }}">{{ $job->production_name }}</a>
    {{ trans('messages.was_approved') }} <br>
    {{ trans('messages.till_end') }}
    <a href="{{ route('page.show', 'reguli-de-selectie') }}">{{ trans('messages.platform_rules') }}</a>
    {{ trans('messages.to_view_applicants') }}
</p>

<p>
    <a href="{{ route('jobs.show', $job->id) }}">{{ trans('messages.view_casting_and_applicants') }}</a>
</p>

<p>
    <br/>
    <strong>{{ trans('messages.rcc_team') }}</strong>
</p>