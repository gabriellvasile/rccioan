
<img src="{{ $message->embed(asset('images/logo.png')) }}" />

<h3>{{ trans('messages.good_day') }}</h3>

@if ($jobs)
    @foreach ($jobs as $job_id => $job)

        @if ($job['characters'])
            @foreach ($job['characters'] as $character_id => $character)
                @if ($character['applicants'])

                    <h4>{{ trans('messages.list_bellow_applicats') }} <em>{{ $character['character_title'] }} - {{ $job['production_name'] }} </em></h4>
                    <table cellpadding="10">
                        <tr>
                            <th align="left">{{ trans('messages.name') }}</th>
                        </tr>
                        @foreach ($character['applicants'] as $applicant_username => $applicant)
                            <tr>
                                <td align="left"><a href="{{ route('profile.show', $applicant_username) }}">{{ $applicant['fname'] . ' ' . $applicant['lname'] }}</a></td>
                            </tr>
                        @endforeach
                    </table>
                    <h3 style="margin-top: 10px"><a href="{{ route('jobs.applicants', $job_id) }}">{{ trans('messages.approve_disapprove_applicants') }}</a></h3>
                    <br/><br/>
                @endif
            @endforeach
        @endif
    @endforeach
@endif

<p>
    {{ trans('messages.please_answer_all') }}
    <a href="{{ route('jobs.mine') }}">{{ trans('messages.my_castings') }}</a>
</p>

<p>
    <br/>
    <strong>{{ trans('messages.rcc_team') }}</strong>
</p>