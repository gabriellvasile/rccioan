
<img src="{{ $message->embed(asset('images/logo.png')) }}" />

<p>{{ $job->owner->employer->company }} {{ trans('messages.created_or_updated_a_job') }}. <strong>{{ trans('messages.three_days_tops') }}</strong></p>

@if ($job->updates)
    <p>
        {{ trans('messages.view_updates') }}
        <a href="{{ route('jobs.preview', $job->id) }}">{{ $job->production_name }}</a>
    </p>
@else
    <p>
        {{ trans('messages.access_casting') }}
        <a href="{{ route('jobs.show', $job->id) }}">{{ $job->production_name }}</a>
    </p>
@end