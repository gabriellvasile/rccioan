
<img src="{{ $message->embed(asset('images/logo.png')) }}" />

<h2>{{ trans('messages.you_have') }} {{ $hours }} {{ trans('messages.hours_till_castings_expire') }}. {{ trans('messages.please_answer_all2') }}</h2>

@if ($jobs)
    @foreach ($jobs as $job_id => $job)
        <h3 style="border-bottom: 1px solid #000; margin-bottom: 10px;">{{ $job['production_name'] }}</h3>
        <h4 style="margin-top: 10px"><a href="{{ route('jobs.applicants', $job_id) }}">{{ trans('messages.approve_disapprove_applicants') }}</a></h4>
        <br/><br/>
    @endforeach
@endif

<p>
    {{ trans('messages.thank_you_for_understanding') }}
    <br/>
    <strong>{{ trans('messages.rcc_team') }}</strong>
</p>