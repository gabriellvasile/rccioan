
<h1>{{ trans('messages.uncompleted') }}</h1>

<p>{{ trans('messages.your_updates_were_unapproved') }} <strong>{!! link_to_route('login', trans('messages.login')) !!}</strong> ! </p>

<a href="{{ route('login') }}" target="_blank">{{ route('login') }}</a>