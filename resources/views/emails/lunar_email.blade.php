
<img src="{{ $message->embed(asset('images/logo.png')) }}" />

<p>{{ trans('messages.congrats') }} {{ $lunar->actor->user->fname }}! {{ trans('messages.lunar_chosen') }} @include('includes._month', ['month' => $lunar->date->month])!</p>

<p>
    <br/>
    <strong><em>{{ trans('messages.rcc_team') }}</em></strong>
</p>