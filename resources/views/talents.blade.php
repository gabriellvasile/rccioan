
@extends('layouts.page')

@section('content')

	<div class="talentsWrap">
		<div class="text-center">
			<h1>Cauta talent</h1>
		</div>
		<hr>

		@foreach ($users as $user)

			<div class="row">
				<div class="col-sm-3 text-center">
					@foreach ($user['images'] as $image)
						@if ($image->id == $user->thumb_id)
							<img class="img-responsive" src="{{ $image->thumb }}" alt="">
						@endif
					@endforeach

					<h3>{{ ucwords($talent) }}</h3>

					<a href="{{ route('messages.new', [$user->id, App::getLocale()]) }}" class="btn">{{ trans('messages.send_message') }}</a>

				</div>
				<div class="col-sm-9">
					<h2>{{ $user->fname . ' ' . $user->lname }}</h2>
					<div class="row">
						<div class="col-sm-4">
							<strong>{{ trans('messages.country') }}: </strong> {{ $user['country']->name }}
						</div>
						<div class="col-sm-4">
							<strong>{{ trans('messages.ethnicity') }}: </strong> {{ $user['ethnicities'] }}
						</div>
						<div class="col-sm-4">
							<strong>{{ trans('messages.professional_studies') }}</strong>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-4">
							<strong>{{ trans('messages.city') }}: </strong> {{ $user->city }}
						</div>
						<div class="col-sm-4">
							<strong>{{ trans('messages.accent') }}: </strong> {{ $user['talent'][$talent]['accent'] }}
						</div>
						<div class="col-sm-4">
							<strong>{{ trans('messages.speciality_studies') }}</strong>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-4">
							<strong>{{ trans('messages.sex') }}</strong> {{ $user['talent'][$talent]['sex'] }}
						</div>
						<div class="col-sm-4">
							<strong>{{ trans('messages.facial_traits') }}</strong>
						</div>
						<div class="col-sm-4">
							<strong>{{ trans('messages.faculty') }}</strong> {{ $user['talent'][$talent]['faculty'] }}
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<strong>{{ trans('messages.interested_in') }}</strong> {{ $user['talent'][$talent]['interested_in'] }}
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<strong>{{ trans('messages.other_info') }}</strong> {{ $user['talent'][$talent]['other_info'] }}
						</div>
					</div>
				</div>
			</div>

			<hr>
		@endforeach
	</div>

@endsection