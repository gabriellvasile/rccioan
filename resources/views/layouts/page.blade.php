@include('includes._head')

	@include('includes._header')


	@include('includes._bannertop')
	@include('includes._banda')

	<br>
	
	@include('errors._session')

	<div class="container">
		@yield('content')
	</div>

@include('includes._footer')