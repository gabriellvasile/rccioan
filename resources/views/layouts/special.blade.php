@include('includes._head')

	@include('includes._header')

	<div class="specialLayout">
		@include('errors._session')

		<div class="container">
			@yield('content')
		</div>
	</div>

@include('includes._footer')