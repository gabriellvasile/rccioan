
@include('includes._head')

	@include('includes._header')

	@include('errors._session')

	@yield('slider')

	<div class="container">
		@yield('content')
	</div>

@include('includes._footer')