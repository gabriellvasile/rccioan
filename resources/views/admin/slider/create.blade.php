
@extends('admin.layouts.app')

@section('pagehead')
    {{ trans('messages.add_new_slider') }}
@stop

@section('content')

    {!! Form::open( ['route' => ['admin.sliders.store', 'locale' => App::getLocale()], 'files'=>true]) !!}
        @include('admin.includes.sliders.form')
    {!! Form::close() !!}

@stop
