
@extends('admin.layouts.app')

@section('pagehead')
    {{ trans('messages.sliders') }}
    {!! link_to_route('admin.sliders.create', trans('messages.add_new'), ['locale' => App::getLocale()], ['class' => 'btn bg-purple margin']) !!}
@stop

@section('content')

    <div class="box box-danger">
        <div class="box-header">
            <h3 class="box-title">{{ trans('messages.all_sliders') }}</h3>
        </div>
        <div class="box-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th width="25">ID</th>
                    <th>{{ trans('messages.images') }}</th>
                </tr>
                </thead>

                <tbody>
                @foreach ( $sliders as $slider )
                    <tr>
                        <td class="text-center">{{ $slider->id }}</td>
                        <td>
                            @if($slider->image)
                                <a href="{{ route('admin.sliders.edit', [$slider->id, 'locale' => App::getLocale()]) }}">
                                    <img width="240" src="{{ asset($slider->image) }}" alt=""/>
                                </a>
                                @include('admin.includes._crud', ['id' => $slider->id, 'slug' => $slider->slug, 'type' => 'sliders'])
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>

            </table>
        </div> {{-- // box-body --}}
    </div>

@stop


