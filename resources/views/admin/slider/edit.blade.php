
@extends('admin.layouts.app')

@section('pagehead')
    {{ trans('messages.edit_slider') }}
    {!! link_to_route('admin.sliders.create', trans('messages.add_new'), ['locale' => App::getLocale()], ['class' => 'btn bg-purple margin']) !!}
@stop

@section('content')

    {!! Form::model( $slider, ['route' => ['admin.sliders.update', $slider->id, 'locale' => App::getLocale()], 'method' => 'PATCH', 'files'=>true, 'id' => 'ambassadorForm'] ) !!}
        @include('admin.includes.sliders.form')
    {!! Form::close() !!}

@stop


