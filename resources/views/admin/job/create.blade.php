
@extends('admin.layouts.app')

@section('pagehead')
    New Casting
@stop

@section('content')

    {!! Form::open( ['route' => 'admin.job.store', 'files'=>true]) !!}
        @include('admin.includes.jobs.form')
    {!! Form::close() !!}

@stop
