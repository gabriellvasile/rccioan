
@extends('admin.layouts.app')

@section('pagehead')
    {{ trans('messages.jobs') }}
@stop

@section('content')

    <div class="box box-danger">
        <div class="box-header">
            <h3 class="box-title">{{ trans('messages.all_jobs') }}</h3>
        </div>
        <div class="box-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th width="25">ID</th>
                    <th>{{ trans('messages.name') }}</th>
                    <th>{{ trans('messages.company_name') }}</th>
                    <th>{{ trans('messages.the_user') }}</th>
                    <th>{{ trans('messages.production_type') }}</th>
                    <th>{{ trans('messages.city') }}</th>
                </tr>
                </thead>

                <tbody>
                @foreach ( $jobs as $job )
                    <tr>
                        <td class="text-center">{{ $job->id }}</td>
                        <td>
                            {!! link_to_route('jobs.edit', $job->production_name, [$job->id, App::getLocale()]) !!}
                            @include('admin.includes._crud', ['id' => $job->id, 'type' => 'job'])
                        </td>
                        <td>{{ $job->employer->company }}</td>
                        <td>{{ $job->employer->user->fname . ' ' . $job->employer->user->lname }}</td>
                        <td>{{ $job->ProductionType->name }}</td>
                        <td>{{ $job->location }}</td>
                    </tr>
                @endforeach
                </tbody>

            </table>
        </div> {{-- // box-body --}}
    </div>

@stop


