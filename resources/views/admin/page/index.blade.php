
@extends('admin.layouts.app')

@section('pagehead')
    {{ trans('messages.pages') }}
    {!! link_to_route('admin.page.create', trans('messages.add_new'), ['locale' => App::getLocale()], ['class' => 'btn bg-purple margin']) !!}
@stop

@section('content')

    <div class="box box-danger">
        <div class="box-header">
            <h3 class="box-title">{{ trans('messages.all_pages') }}</h3>
        </div>
        <div class="box-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th width="25">ID</th>
                    <th>{{ trans('messages.titlu') }}</th>
                    <th>{{ trans('messages.excerpt') }}</th>
                    <th>{{ trans('messages.description') }}</th>
                    <th>{{ trans('messages.images') }}</th>
                </tr>
                </thead>

                <tbody>
                @foreach ( $pages as $page )

                    <tr>
                        <td class="text-center">{{ $page->id }}</td>
                        <td>
                            {!! link_to_route('admin.page.edit', $page->title, [$page->id, 'locale' => App::getLocale(), 'lang' => $page->code]) !!}
                            @include('admin.includes._crud', ['id' => $page->id, 'slug' => $page->slug, 'type' => 'page'])
                        </td>
                        <td>{!! $page->excerpt  !!}</td>
                        <td>{!! $page->content  !!}</td>
                        <td>
                            @if($page->image)
                                <img width="40" src="{{ asset($page->image) }}" alt=""/>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>

            </table>
        </div> {{-- // box-body --}}
    </div>

@stop


