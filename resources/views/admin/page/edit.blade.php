
@extends('admin.layouts.app')

@section('pagehead')
    {{ trans('messages.edit_page') }}
    {!! link_to_route('admin.page.create', trans('messages.add_new'), ['locale' => App::getLocale()], ['class' => 'btn bg-purple margin']) !!}
    {!! link_to_route('page.show', trans('messages.view'), [$page->slug], ['class' => 'btn bg-green', 'target' => 'blank']) !!}
@stop

@section('content')

    {!! Form::model( $page, ['route' => ['admin.page.update', $page->id, 'locale' => App::getLocale(), 'lang' => App::getLocale()], 'method' => 'PATCH', 'files'=>true, 'id' => 'ambassadorForm'] ) !!}
        @include('admin.includes.page.form')
    {!! Form::close() !!}

@stop


