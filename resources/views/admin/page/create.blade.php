
@extends('admin.layouts.app')

@section('pagehead')
    {{ trans('messages.add_new_page') }}
@stop

@section('content')

    {!! Form::open( ['route' => ['admin.page.store', 'locale' => App::getLocale()], 'files'=>true]) !!}
        @include('admin.includes.page.form')
    {!! Form::close() !!}

@stop
