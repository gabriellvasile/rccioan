
@extends('admin.layouts.app')
@section('pagehead')
	{{ trans('messages.edit_employer') }}
	{!! link_to_route('admin.user.create', trans('messages.add_new'), ['locale' => App::getLocale()], ['class' => 'btn bg-purple margin']) !!}
	{!! link_to_route('profile.show', trans('messages.view_profile'), [$employer->user->username, 'locale' => App::getLocale()], ['class' => 'btn bg-green', 'target' => 'blank']) !!}
@stop

@section('content')
    
{!! Form::model( $employer, ['route' => ['admin.employer.update', $employer->user->username, 'locale' => App::getLocale()], 'method' => 'PATCH'] ) !!}
   @include('admin.includes.employers.form')
{!! Form::close() !!}

<div id="mediaContent" class="employerMedia">
	<ul class="nav nav-tabs" role="tablist">
		<li role="presentation">
			<a href="#filestab" id="showMedia" aria-controls="media" role="tab" data-show="{{ route('profile.ajaxShowFiles', [$employer->user->username, App::getLocale()])  }}" data-toggle="tab">{{ trans('messages.media') }}</a>
		</li>
	</ul>
	<div class="tab-content">
	</div>
</div>

@stop


