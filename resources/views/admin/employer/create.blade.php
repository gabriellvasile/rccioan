
@extends('admin.layouts.app')

@section('pagehead')
	{{ trans('messages.add_new_employer') }}
@stop

@section('content')
    
{!! Form::open( ['route' => 'admin.employer.store', 'files'=>true]) !!}
   @include('admin.includes.employers.form')
{!! Form::close() !!}

@stop


