
@extends('admin.layouts.app')

@section('pagehead')
    {{ trans('messages.all_employers') }}
    {!! link_to_route('admin.user.create', trans('messages.add_new'), ['locale' => App::getLocale()], ['class' => 'btn bg-purple margin']) !!}
@stop


@section('content')

	<div class="box box-danger">
        <div class="box-header">
          <h3 class="box-title">{{ trans('messages.all_employers') }}</h3>
        </div>
        <div class="box-body">
          	<table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th width="50">Employer&nbsp;ID&nbsp;/ User&nbsp;ID</th>
                        <th>{{ trans('messages.the_user') }}</th>
                        <th>{{ trans('messages.company_name') }}</th>
                        <th>{{ trans('messages.address') }}</th>
                        <th>{{ trans('messages.post_code') }}</th>
                        <th>{{ trans('messages.website') }}</th>
                        {{--<th>Logo</th>--}}
                        {{--<th>{{ trans('messages.images') }}</th>--}}
                    </tr>
                </thead>

                <tbody>
                	@foreach ( $employers as $employer )
                        <tr>
                            <td class="text-center">{{ $employer->id }} / {{ $employer->user->id }}</td>
                            <td>
                                <h4 class="numeUser">{!! link_to_route('admin.employer.edit', $employer->user->fname . ' ' . $employer->user->lname, [$employer->user->username,  'locale' => App::getLocale()]) !!}</h4>
                                @include('admin.includes._crud', ['id' => $employer->user->id, 'type' => 'employer', 'username' => $employer->user->username])
                            </td>
                            <td>{{ $employer->company }}</td>
                            <td>{{ $employer->address }}</td>
                            <td>{{ $employer->post_code }}</td>
                            <td><a href="http://{{ $employer->website }}" target="_blank">{{ $employer->website }}</a></td>
                            {{--<td>--}}
                                {{--<img src="{{ $employer->logo }}" width="100" alt="">--}}
                            {{--</td>--}}
                            {{--<td>--}}
                                {{--@if (isset ($employer->user->images[0]))--}}
                                    {{--<img src="{{ $employer->user->images[0]->path }}" width="100" alt="">--}}
                                {{--@endif--}}
                            {{--</td>--}}
                        </tr>
                	@endforeach
                </tbody>

            </table>
        </div> {{-- // box-body --}}
	</div>

@stop


