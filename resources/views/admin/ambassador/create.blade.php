
@extends('admin.layouts.app')

@section('pagehead')
    {{ trans('messages.add_new_ambassador') }}
@stop

@section('content')

    {!! Form::open( ['route' => ['admin.ambassador.store', 'locale' => App::getLocale()], 'files'=>true]) !!}
        @include('admin.includes.ambassadors.form')
    {!! Form::close() !!}

@stop
