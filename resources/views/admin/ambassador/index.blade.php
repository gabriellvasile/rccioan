
@extends('admin.layouts.app')

@section('pagehead')
    {{ trans('messages.ambassadors') }}
    {!! link_to_route('admin.ambassador.create', trans('messages.add_new'), ['locale' => App::getLocale()], ['class' => 'btn bg-purple margin']) !!}
@stop

@section('content')

    <div class="box box-danger">
        <div class="box-header">
            <h3 class="box-title">{{ trans('messages.all_ambassadors') }}</h3>
        </div>
        <div class="box-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th width="25">ID</th>
                    <th>{{ trans('messages.name') }}</th>
                    <th>{{ trans('messages.quote') }}</th>
                    <th>{{ trans('messages.description') }}</th>
                    <th>{{ trans('messages.images') }}</th>
                    <th>{{ trans('messages.video') }}</th>
                </tr>
                </thead>

                <tbody>
                @foreach ( $ambassadors as $ambassador )
                    <tr>
                        <td class="text-center">{{ $ambassador->id }}</td>
                        <td>
                            {!! link_to_route('admin.ambassador.edit', $ambassador->name, [$ambassador->id, 'locale' => App::getLocale()]) !!}
                            @include('admin.includes._crud', ['id' => $ambassador->id, 'type' => 'ambassador'])
                        </td>
                        <td>{{ $ambassador->quote }}</td>
                        <td>{{ $ambassador->description }}</td>
                        <td>
                            @if ($ambassador->image != null)
                                <img src="{{ $ambassador->image }}" width="80" alt=""/>
                            @endif
                        </td>
                        <td>{{ $ambassador->video }}</td>
                    </tr>
                @endforeach
                </tbody>

            </table>
        </div> {{-- // box-body --}}
    </div>

@stop


