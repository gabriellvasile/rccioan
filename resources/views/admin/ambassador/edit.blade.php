
@extends('admin.layouts.app')

@section('pagehead')
    {{ trans('messages.edit_ambassador') }}
    {!! link_to_route('admin.ambassador.create', trans('messages.add_new'), ['locale' => App::getLocale()], ['class' => 'btn bg-purple margin']) !!}
@stop

@section('content')

    {!! Form::model( $ambassador, ['route' => ['admin.ambassador.update', $ambassador->id, 'locale' => App::getLocale()], 'method' => 'PATCH', 'files'=>true, 'id' => 'ambassadorForm'] ) !!}
        @include('admin.includes.ambassadors.form')
    {!! Form::close() !!}

@stop


