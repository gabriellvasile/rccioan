
@include('admin.includes.head')

	@include('admin.includes.header')
	@include('admin.includes.sidebar')
	
	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
	 	<!-- Content Header (Page header) -->
	  	<section class="content-header">
		    <h1>
				@yield('pagehead')
		  	</h1>
		    
		    @include('admin.includes.breadcrumbs')
		</section>

		<!-- Main content -->
		<section class="content">

			@if (Session::has('success_message'))
			    <div class="alert alert-success alert-dismissible" role="alert">
			      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			      <strong>{{ trans('messages.success') }}!</strong>
			      {{ session('success_message') }}
			    </div>
			@endif

			@yield('content')
	  	</section><!-- /.content -->
	</div><!-- /.content-wrapper -->

@include('admin.includes.footer')
@yield('extrafooter')