
@extends('admin.layouts.app')

@section('pagehead')
	{{ trans('messages.all_users') }}
	{!! link_to_route('admin.user.create', trans('messages.add_new'), ['locale' => App::getLocale()], ['class' => 'btn bg-purple margin']) !!}
@stop

@section('content')

	<div class="box box-danger">
        <div class="box-header">
          <h3 class="box-title">{{ trans('messages.all_users') }}</h3>
        </div>
        <div class="box-body">
          	<table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th width="25">ID</th>
                        <th>{{ trans('messages.name') }}</th>
                        <th>{{ trans('messages.username') }}</th>
                        <th>{{ trans('messages.kind') }}</th>
                        <th>{{ trans('messages.email') }}</th>
                        <th>{{ trans('messages.mobile') }}</th>
                        <th>{{ trans('messages.country') }}</th>
                        <th>{{ trans('messages.city') }}</th>
                    </tr>
                </thead>

                <tbody>
                	@foreach ( $users as $user )
                        <?php
                            $type = 'special';

                            if ($user->role_id == $talent_role_id)
                                $type = 'actor';
                            else if($user->role_id == $employer_role_id)
                                $type = 'employer';
                        ?>
                        <tr>
                            <td class="text-center">{{ $user->id }}</td>
                            <td>
                                {!! link_to_route('admin.user.edit', $user->fname . ' ' . $user->lname, [$user->username, 'locale' => App::getLocale()]) !!}
                                @include('admin.includes._crud', ['id' => $user->id, 'type' => 'user', 'username' => $user->username])
                            </td>

                            <td>{{ $user->username }}</td>
                            <td>{{ $type }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->mobile }}</td>
                            <td>{{ $user->country->name }}</td>
                            <td>{{ $user->city }}</td>
                        </tr>
                	@endforeach
                </tbody>

            </table>
        </div> {{-- // box-body --}}
	</div>

@stop


