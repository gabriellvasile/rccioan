
@extends('admin.layouts.app')

@section('pagehead')
	{{ trans('messages.edit_user') }}
	{!! link_to_route('admin.user.create', trans('messages.add_new'), ['locale' => App::getLocale()], ['class' => 'btn bg-purple margin']) !!}

	@if ($user->actor)
		{!! link_to_route('admin.actor.edit', trans('messages.edit_actor'), [$user->username, 'locale' => App::getLocale()], ['class' => 'btn bg-orange margin']) !!}
	@elseif ($user->employer)
		{!! link_to_route('admin.employer.edit', trans('messages.edit_employer'), [$user->username, 'locale' => App::getLocale()], ['class' => 'btn bg-orange margin']) !!}
	@endif

	{!! link_to_route('profile.show', trans('messages.view_profile'), [$user->username, 'locale' => App::getLocale()], ['class' => 'btn bg-green margin', 'target' => 'blank']) !!}

@stop

@section('content')
    
{!! Form::model( $user, ['route' => ['admin.user.update', $user->id, 'locale' => App::getLocale()], 'method' => 'PATCH'] ) !!}
   @include('admin.includes.users.form')
{!! Form::close() !!}

@stop


