
@extends('admin.layouts.app')

@section('pagehead')
	{{ trans('messages.add_new_user') }}
@stop

@section('content')
    
{!! Form::open( ['route' => ['admin.user.store', 'locale' => App::getLocale()]]) !!}
   @include('admin.includes.users.form')
{!! Form::close() !!}

@stop




