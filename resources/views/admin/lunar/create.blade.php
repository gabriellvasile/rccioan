
@extends('admin.layouts.app')

@section('pagehead')
    {{ trans('messages.add_new_monthly_actor') }}
@stop

@section('content')

    {!! Form::open( ['route' => ['admin.lunar.store', 'locale' => App::getLocale()], 'files'=>true]) !!}
        @include('admin.includes.lunars.form')
    {!! Form::close() !!}

@stop
