
@extends('admin.layouts.app')

@section('pagehead')
    {{ trans('messages.edit_monthly_actor') }}
    {!! link_to_route('admin.lunar.create', trans('messages.add_new'), ['locale' => App::getLocale()], ['class' => 'btn bg-purple margin']) !!}
@stop

@section('content')

    {!! Form::model( $lunar, ['route' => ['admin.lunar.update', $lunar->id, 'locale' => App::getLocale()], 'method' => 'PATCH'] ) !!}
        @include('admin.includes.lunars.form')
    {!! Form::close() !!}

@stop


