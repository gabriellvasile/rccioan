
@extends('admin.layouts.app')

@section('pagehead')
    {{ trans('messages.actor_month') }}
    {!! link_to_route('admin.lunar.create', trans('messages.add_new'), ['locale' => App::getLocale()], ['class' => 'btn bg-purple margin']) !!}
@stop

@section('content')

    <div class="box box-danger">
        <div class="box-header">
            <h3 class="box-title">{{ trans('messages.all_actors_month') }}</h3>
        </div>
        <div class="box-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th width="25">ID</th>
                    <th>{{ trans('messages.name') }}</th>
                    <th>{{ trans('messages.video') }}</th>
                    <th>{{ trans('messages.description') }}</th>
                    <th>{{ trans('messages.date') }}</th>
                </tr>
                </thead>

                <tbody>
                @foreach ( $lunars as $lunar )
                    <tr>
                        <td class="text-center">{{ $lunar->id }}</td>
                        <td>
                            {!! link_to_route('admin.lunar.edit', $lunar->name, [$lunar->id, 'locale' => App::getLocale()]) !!}

                            @if (isset($lunar->actor->user))
                                @include('admin.includes._crud', ['id' => $lunar->actor->user->id, 'type' => 'lunar', 'lunar_id' => $lunar->id])
                            @else
                                @include('admin.includes._crud', ['id' => 0, 'type' => 'lunar', 'lunar_id' => $lunar->id])
                            @endif

                        </td>
                        <td><a href="{{ $lunar->video }}" target="_blank">{{ $lunar->video }}</a></td>
                        <td>{{ $lunar->description }}</td>
                        <td>{{ trans('messages.' . date("F", mktime(0, 0, 0, $lunar->date->month, 10))) }} {{ $lunar->date->year }}</td>
                    </tr>
                @endforeach
                </tbody>

            </table>
        </div> {{-- // box-body --}}
    </div>

@stop


