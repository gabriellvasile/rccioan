
@extends('admin.layouts.app')

@section('pagehead')
	{{ trans('messages.add_new_actor') }}
@stop

@section('content')
    
{!! Form::open( ['route' => 'admin.actor.store', 'files'=>true]) !!}
   @include('admin.includes.actors.form')
{!! Form::close() !!}

@stop


