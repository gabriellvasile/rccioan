
@extends('admin.layouts.app')

@section('pagehead')
	{{ trans('messages.edit_actor') }}
	{!! link_to_route('admin.user.create', trans('messages.add_new'), ['locale' => App::getLocale()], ['class' => 'btn bg-purple margin']) !!}
	{!! link_to_route('profile.show', trans('messages.view_profile'), [$actor->user->username, 'locale' => App::getLocale()], ['class' => 'btn bg-green', 'target' => 'blank']) !!}
@stop

@section('content')
    
{!! Form::model( $actor, ['route' => ['admin.actor.update', $actor->user->username, 'locale' => App::getLocale()], 'method' => 'PATCH', 'files' => true] ) !!}
   @include('admin.includes.actors.form')
{!! Form::close() !!}

<div id="mediaContent">
	<ul class="nav nav-tabs" role="tablist">
		<li role="presentation">
			<a href="#filestab" id="showMedia" aria-controls="media" role="tab" data-show="{{ route('profile.ajaxShowFiles', [$user->username, App::getLocale()])  }}" data-toggle="tab">{{ trans('messages.media') }}</a>
		</li>
	</ul>
	<div class="tab-content">
	</div>
</div>


@stop


