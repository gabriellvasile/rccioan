
@extends('admin.layouts.app')

@section('pagehead')
    {{ trans('messages.all_actors') }}
    {!! link_to_route('admin.user.create', trans('messages.add_new'), ['locale' => App::getLocale()], ['class' => 'btn bg-purple margin']) !!}
@stop

@section('content')

	<div class="box box-danger">
        <div class="box-header">
          <h3 class="box-title">{{ trans('messages.all_actors') }}</h3>
        </div>
        <div class="box-body">
          	<table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th width="50">Actor&nbsp;ID&nbsp;/ User&nbsp;ID</th>
                        <th>{{ trans('messages.the_user') }}</th>
                        <th>{{ trans('messages.ethnicity') }}</th>
                        <th>{{ trans('messages.eye_colour') }}</th>
                        <th>{{ trans('messages.hair_colour') }}</th>
                        <th>{{ trans('messages.height') }}</th>
                        <th>{{ trans('messages.weight') }}</th>
                        <th>{{ trans('messages.accent') }}</th>
                        <th>{{ trans('messages.hair_length') }}</th>
                        <th>{{ trans('messages.voice') }}</th>
                        <th>{{ trans('messages.role_abroad') }}</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach ( $actors as $actor )
                        @if (isset($actor->user))
                            <tr>
                                <td class="text-center">{{ $actor->id }} / {{ $actor->user->id }}</td>
                                <td>
                                    <h4 class="numeUser">{!! link_to_route('admin.actor.edit', $actor->user->fname . ' ' . $actor->user->lname, [$actor->user->username,  'locale' => App::getLocale()]) !!}</h4>
                                    @include('admin.includes._crud', ['id' => $actor->user->id, 'type' => 'actor', 'username' => $actor->user->username])
                                </td>
                                <td>{{ $actor->ethnicity->name }}</td>
                                <td>{{ $actor->eyecolour->name }}</td>
                                <td>{{ $actor->haircolour->name }}</td>

                                <td>{{ $actor->height }}</td>
                                <td>{{ $actor->weight }}</td>
                                <td>{{ $actor->accent }}</td>

                                <td>{{ trans('messages.' . $actor->hair_length) }}</td>
                                <td>{{ $actor->voice ? trans('messages.yes') : trans('messages.no') }}</td>
                                <td>{{ $actor->role_abroad ? trans('messages.yes') : trans('messages.no') }}</td>
                            </tr>
                        @endif
                	@endforeach
                </tbody>

            </table>
        </div> {{-- // box-body --}}
	</div>

@stop


