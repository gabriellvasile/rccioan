<div class="well project">
    <div class="btn btn-danger deleteFromProfile" data-id="{{ $key }}">-</div>
    <div class="row">
        <div class="col-sm-4">
            <div class="form-group">
                {!! Form::label('project_name' . $key, trans('messages.project_name')) !!}
                {!! Form::text('projects[' . $key . '][name]' , null, ['placeholder' => trans('messages.project_name'), 'class' => 'form-control', 'id' => 'project_name'  . $key]) !!}
            </div>
        </div>

        <div class="col-sm-4">
            <div class="form-group">
                {!! Form::label('project_house' . $key, trans('messages.project_house')) !!}
                {!! Form::text('projects[' . $key . '][house]' , null, ['placeholder' => trans('messages.project_house'), 'class' => 'form-control', 'id' => 'project_house'  . $key]) !!}
            </div>
        </div>

        <div class="col-sm-4">
            <div class="form-group">
                {!! Form::label('project_year' . $key, trans('messages.project_year')) !!}
                {!! Form::text('projects[' . $key . '][year]' , null, ['placeholder' => trans('messages.project_year'), 'class' => 'form-control', 'id' => 'project_year'  . $key]) !!}
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-4">
            <div class="form-group">
                {!! Form::label('project_director' . $key, 'Director') !!}
                {!! Form::text('projects[' . $key . '][director]' , null, ['placeholder' => trans('messages.project_director'), 'class' => 'form-control', 'id' => 'project_director'  . $key]) !!}
            </div>
        </div>

        <div class="col-sm-4">
            <div class="form-group">
                {!! Form::label('project_role' . $key, trans('messages.project_role')) !!}
                {!! Form::text('projects[' . $key . '][role]' , null, ['placeholder' => trans('messages.project_role'), 'class' => 'form-control', 'id' => 'project_role'  . $key]) !!}
            </div>
        </div>

        <div class="col-sm-4">
            <div class="form-group">
                {!! Form::label('project_description' . $key, trans('messages.project_desc')) !!}
                {!! Form::textarea('projects[' . $key . '][description]' , null, ['placeholder' => trans('messages.project_desc'), 'class' => 'form-control', 'rows' => 3, 'id' => 'project_description'  . $key]) !!}
            </div>
        </div>
    </div>
</div>

<hr/>