<div class="crud">
    @if($type == 'user')
        @if($$type->updates)
            <a href="{{ route('profile.preview', [$$type->username, App::getLocale()]) }}" target="_blank" title="{{ trans('messages.approve') }}" class="approve"><i class="fa fa-eye"></i></a>
        @endif
    @elseif($type == 'job')
        @if($$type->updates)
            <a href="{{ route('jobs.preview', [$$type->id, App::getLocale()]) }}" target="_blank" title="{{ trans('messages.approve') }}" class="approve"><i class="fa fa-eye"></i></a>
        @endif
    @elseif ($type == 'ambassador' || $type == 'lunar' || $type == 'page' || $type == 'sliders')
    @else
        @if($$type->user->updates)
            <a href="{{ route('profile.preview', [$$type->user->username, App::getLocale()]) }}" target="_blank" title="{{ trans('messages.approve') }}" class="approve"><i class="fa fa-eye"></i></a>
        @endif
    @endif

    @if ($type == 'ambassador' || $type == 'lunar' || $type == 'sliders')
    @elseif ($type == 'page')
        <a href="{{ route( 'page.show', $slug) }}" title="{{ trans('messages.view') }}" target="_blank" class="view"><i class="fa fa-eye"></i></a>
    @elseif ($type == 'job')
        <a href="{{ route( 'jobs.show', [$id, App::getLocale()]) }}" title="{{ trans('messages.view') }}" target="_blank" class="view"><i class="fa fa-eye"></i></a>
    @else
        <a href="{{ route('profile.show', [isset($username) ? $username : $id, 'locale' => App::getLocale()]) }}" title="{{ trans('messages.view') }}" target="_blank" class="view"><i class="fa fa-eye"></i></a>
    @endif

    @if ($type == 'lunar')
        <a href="{{ route('admin.'.$type.'.edit', [$lunar_id, 'locale' => App::getLocale()]) }}" title="{{ trans('messages.edit_profile') }}" class="edit"><i class="fa fa-pencil-square-o"></i></a>
    @elseif ($type == 'job')
        <a href="{{ route('jobs.edit', [$$type->id, App::getLocale()]) }}" title="{{ trans('messages.edit_job') }}" class="edit"><i class="fa fa-pencil-square-o"></i></a>
    @elseif ($type == 'page')
        <a href="{{ route('admin.'.$type.'.edit', [$page->id, 'locale' => App::getLocale(), 'lang' => $page->code]) }}" title="{{ trans('messages.edit_profile') }}" class="edit"><i class="fa fa-pencil-square-o"></i></a>
    @else
        <a href="{{ route('admin.'.$type.'.edit', [isset($username) ? $username : $id, 'locale' => App::getLocale()]) }}" title="{{ trans('messages.edit_profile') }}" class="edit"><i class="fa fa-pencil-square-o"></i></a>
    @endif

    @if ($type == 'lunar')
        {!! Form::open( ['route' => ['admin.'.$type.'.destroy', $lunar_id], 'method' => 'delete'] ) !!}
    @else
        {!! Form::open( ['route' => ['admin.'.$type.'.destroy', $id], 'method' => 'delete'] ) !!}
    @endif
            {!! Form::button('<i class="fa fa-times"></i>', ['class' => 'btn btn-xs btn-danger', 'type' => 'submit', 'title' => trans('messages.delete')] ) !!}
        {!! Form::close() !!}
</div>