
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <strong>{{ trans('messages.validation_error') }}</strong> <br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box box-danger">
    <div class="box-body clearfix">

        <div class="col-sm-5">
            <div class="form-group">
                {!! Form::label('name', trans('messages.actori_sg')) !!}
                <div class="row">
                    <div class="col-sm-6">
                        {!! Form::select('actor_id', $actors , isset($lunar->actor->id) ? $lunar->actor->id : '',  array('placeholder' => trans('messages.actori_sg'), 'class' => 'form-control')) !!}
                    </div>
                    <div class="col-sm-6">
                        {!! Form::text('name', null,  array('placeholder' => trans('messages.type'), 'class' => 'form-control')) !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-4">
            <div class="form-group">
                {!! Form::label('video', trans('messages.video')) !!}
                {!! Form::text('video', null , array('placeholder' => trans('messages.video'), 'class' => 'form-control')) !!}
            </div>
        </div>

        <div class="col-sm-3">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        {!! Form::label('month', trans('messages.month')) !!}
                        {!! Form::select('month', $months, isset($lunar->date->month) ? $lunar->date->month : '' , array('placeholder' => trans('messages.month'), 'class' => 'form-control')) !!}
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        {!! Form::label('year', trans('messages.year')) !!}
                        {!! Form::select('year', $years, isset($lunar->date->year) ? $lunar->date->year : '' , array('placeholder' => trans('messages.year'), 'class' => 'form-control')) !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-12">
            <div class="form-group">
                {!! Form::label('description', trans('messages.description')) !!}
                {!! Form::textarea('description', null , array('placeholder' => trans('messages.description'), 'class' => 'form-control')) !!}
            </div>

            {!! Form::submit(trans('messages.save'), array( 'class' => 'btn btn-success pull-right') ) !!}
        </div>

    </div>
</div>