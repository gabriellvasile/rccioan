<div class="box box-danger">
    <div class="box-header">
      <h3 class="box-title">{!! $title !!}</h3>
    </div>
    <div class="box-body clearfix">
        {!! $content !!}
    </div>
</div>