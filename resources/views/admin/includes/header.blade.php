
<!-- Main Header -->
<header class="main-header">
    <!-- Logo -->
    <a href="{{ route('admin.dashboard', App::getLocale()) }}" class="logo"><b>Quart</b>CMS</a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        @if (App::getLocale() == 'ro')
            <a href="{{ route('home') }}" target="_blank" class="topbarLink">{{ trans('messages.view_website') }}</a>
        @else
            <a href="{{ route('page.show', 'en') }}" target="_blank" class="topbarLink">{{ trans('messages.view_website') }}</a>
        @endif

        <ul id="langSelect">
            @foreach ($admin_languages as $lang_code => $lang_uri)
                <li>
                    <a href="{{ $lang_uri }}"
                       @if ($lang_code == App::getLocale()) class="active" @endif
                            >
                        {{ $lang_code }}
                    </a>
                </li>
            @endforeach
        </ul>

      @include('admin.includes.navbarlinks')

    </nav>
</header>

<div class="la-anim-10"></div>