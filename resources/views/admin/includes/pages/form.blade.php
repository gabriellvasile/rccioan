<div class="row">
    <div class="col-sm-9">

        <div role="tabpanel">
          <!-- Nav tabs -->
          <ul class="nav nav-tabs" role="tablist">
            @if (isset( $page->languages ))
                <?php $langs = $page->languages;  ?>
            @else
                <?php $langs = $page; ?>
            @endif

            @foreach ( $langs as $key => $lang )
                <li role="presentation"  @unless ($key) class="active" @endunless>
                    <a href="#{{ $lang->name }}" aria-controls="{{ $lang->name }}" role="tab" data-toggle="tab">{{ $lang->name }}</a>
                </li>
            @endforeach
          </ul>
          <!-- Tab panes -->
          <div class="tab-content">
            @foreach ( $langs as $key => $lang )
                <div role="tabpanel" class="tab-pane @unless ($key) active @endunless" id="{{ $lang->name }}">
                    <div class="box box-danger">
                        <div class="box-body clearfix">
                            <div class="form-group">
                                {!! Form::label('languages[' . $lang->id . '][title]', 'Title') !!}
                                {!! Form::text('languages[' . $lang->id . '][title]', null , array('placeholder' => 'Enter title here', 'class' => 'form-control')); !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label('languages[' . $lang->id . '][slug]', 'Slug') !!}
                                {!! Form::text('languages[' . $lang->id . '][slug]', null , array('placeholder' => 'Slug', 'class' => 'form-control')); !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label('languages[' . $lang->id . '][content]', 'Content') !!}
                                {!! Form::textarea('languages[' . $lang->id . '][content]', null, array('placeholder' => 'Content', 'class' => 'form-control')); !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label('languages[' . $lang->id . '][excerpt]', 'Excerpt') !!}
                                {!! Form::textarea('languages[' . $lang->id . '][excerpt]', null, array('placeholder' => 'Excerpt', 'class' => 'form-control')); !!}
                            </div>
                        </div>
                    </div>
                </div>
             @endforeach
          </div>

        </div>
    </div>

    <div class="col-sm-3">

        <?php 
            $content = Form::submit('Publish!', array( 'class' => 'btn btn-success pull-right') );
            $content .= Form::button('Save Draft', array( 'class' => 'btn btn-default pull-left') );
            $content .= Form::button('Preview', array( 'class' => 'btn btn-default pull-left') );
        ?>
        @include('admin.includes.box', array( 'title' => 'Publish', 'content' => $content ) )


        <?php 
            $content = '<div class="form-group">';
            $content .= Form::label('parent', 'Parent' );
            $content .= Form::select('parent', array('(no parent', 'a', 'b', 'c'), null, array( 'class' => 'form-control') );
            $content .= '</div>';

            $content = '<div class="form-group">';
            $content .= Form::label('order', 'Order' );
            $content .= Form::text('order', null, array( 'class' => 'form-control') );
            $content .= '</div>';
        ?>
        @include('admin.includes.box', array( 'title' => 'Page Attributes', 'content' => $content ) )

        
        <?php 
            $content = isset( $page->image ) ? '<img class="img-responsive" src="' . $page->image . '" alt="">' : '';
            $content .= HTML::link('#', 'Set featured image');
        ?>
        @include('admin.includes.box', array( 'title' => 'Featured Images', 'content' => $content ) )

        <input name="file" type="file" multiple />
        {!! Form::hidden('image', '') !!}

    </div>
</div>