
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <strong>{{ trans('messages.validation_error') }}</strong> <br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box box-danger">
    <div class="box-body clearfix editable" data-type="page" @if (isset($page)) data-remove="{{ route('admin.removeFromPage', $page->id) }}" @endif>

        <div class="col-sm-6">
            <div class="form-group">
                {!! Form::label('title', trans('messages.titlu')) !!}
                {!! Form::text('title', null , array('placeholder' => trans('messages.titlu'), 'class' => 'form-control')) !!}
            </div>
            <div class="form-group">
                {!! Form::label('excerpt', trans('messages.excerpt')) !!}
                {!! Form::textarea('excerpt', null , array('placeholder' => trans('messages.excerpt'), 'class' => 'form-control tinymce')) !!}
            </div>
            <div class="form-group">
                {!! Form::checkbox('header', null, null , array('placeholder' => trans('messages.header'))) !!}
                {!! Form::label('header', trans('messages.header')) !!}
            </div>
            <div class="form-group">
                {!! Form::checkbox('footer', null, null, array('placeholder' => trans('messages.footer'))) !!}
                {!! Form::label('footer', trans('messages.footer')) !!}
            </div>

        </div>

        <div class="col-sm-6">
            <div class="form-group">
                {!! Form::label('slug', trans('messages.slug')) !!}
                {!! Form::text('slug', null , array('placeholder' => trans('messages.slug'), 'class' => 'form-control')) !!}
            </div>
            <div class="form-group">
                {!! Form::label('content', trans('messages.description')) !!}
                {!! Form::textarea('content', null , array('placeholder' => trans('messages.description'), 'class' => 'form-control tinymce')) !!}
            </div>

        </div>

        <div class="col-sm-12">
            <div class="form-group">
                {!! Form::label('image', trans('messages.images')) !!}
                {!! Form::file('image', ['placeholder' => trans('messages.images'), 'id' => 'ambassadorImage', 'type' => 'file', 'class' => 'file']) !!}

                <br/>
                @if (isset($page->image) && $page->image)
                    <div class="rel file">
                        <div class="btn btn-danger deleteFromProfile" data-id="{{ $page->id }}">-</div>
                        <img src="{{ $page->image }}" alt="{{ $page->title }}" class="img-responsive"/>
                    </div>
                @endif
            </div>

            {!! Form::submit(trans('messages.save'), array( 'class' => 'btn btn-success pull-right') ) !!}
        </div>

    </div>
</div>