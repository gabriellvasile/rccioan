<div role="tabpanel" class="tab-pane active" id="user">
    <div class="box box-danger">
        <div class="box-body clearfix">

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        {!! Form::hidden('user[complete]', 1) !!}
                        {!! Form::hidden('user[approved]', 1) !!}

                        {!! Form::label('user[fname]', trans('messages.first_name')) !!}
                        {!! Form::text('user[fname]', null , array('placeholder' => trans('messages.first_name'), 'class' => 'form-control')) !!}
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        {!! Form::label('user[lname]', trans('messages.last_name')) !!}
                        {!! Form::text('user[lname]', null , array('placeholder' => trans('messages.last_name'), 'class' => 'form-control')) !!}
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        {!! Form::label('user[username]', trans('messages.username')) !!}
                        {!! Form::text('user[username]', null , array('placeholder' => trans('messages.username'), 'class' => 'form-control')) !!}
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        {!! Form::label('user[email]', trans('messages.email')) !!}
                        {!! Form::email('user[email]', null, array('placeholder' => trans('messages.email'), 'class' => 'form-control')) !!}
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        {!! Form::label('user[mobile]', trans('messages.mobile')) !!}
                        {!! Form::text('user[mobile]', null, array('placeholder' => trans('messages.mobile'), 'class' => 'form-control')) !!}
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        {!! Form::label('user[birth_date]', trans('messages.birth_date')) !!}
                        {!! Form::text('user[birth_date]', null, array('placeholder' => trans('messages.birth_date'), 'class' => 'form-control')) !!}
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        {!! Form::label('user[sex]', trans('messages.sex')) !!}
                        {!! Form::select('user[sex]', ['M' => 'M', 'F' => 'F'], null, array('placeholder' => trans('messages.sex'), 'class' => 'form-control')) !!}
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        {!! Form::label('user[country_id]', trans('messages.country')) !!}
                        {!! Form::select('user[country_id]', $countries, null, array('placeholder' => trans('messages.country'), 'class' => 'form-control')) !!}
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        {!! Form::label('user[city]', trans('messages.city')) !!}
                        {!! Form::text('user[city]', null, array('placeholder' => trans('messages.city'), 'class' => 'form-control')) !!}
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        {!! Form::label('user[language_id]', trans('messages.language')) !!}
                        {!! Form::select('user[language_id]', $languages, null, array('placeholder' => trans('messages.language'), 'class' => 'form-control')) !!}
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>