
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <strong>{{ trans('messages.validation_error') }}</strong> <br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box box-danger">
    <div class="box-body clearfix">
        <div class="row">
            <div class="col-sm-6">
                <h3>Ro</h3>
                <div class="form-group">
                    {!! Form::label('title1_ro', (trans('messages.titlu') . 1)) !!}
                    {!! Form::text('title1_ro', null, ['placeholder' => (trans('messages.titlu') . 1), 'class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('title2_ro', (trans('messages.titlu') . 2)) !!}
                    {!! Form::text('title2_ro', null, ['placeholder' => (trans('messages.titlu') . 1), 'class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('desc_ro', (trans('messages.titlu') . 3)) !!}
                    {!! Form::text('desc_ro', null, ['placeholder' => (trans('messages.titlu') . 3), 'class' => 'form-control']) !!}
                </div>
            </div>
            <div class="col-sm-6">
                <h3>En</h3>
                <div class="form-group">
                    {!! Form::label('title1_en', (trans('messages.titlu') . 1)) !!}
                    {!! Form::text('title1_en', null, ['placeholder' => (trans('messages.titlu') . 1), 'class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('title2_en', (trans('messages.titlu') . 2)) !!}
                    {!! Form::text('title2_en', null, ['placeholder' => (trans('messages.titlu') . 1), 'class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('desc_en', (trans('messages.titlu') . 3)) !!}
                    {!! Form::text('desc_en', null, ['placeholder' => (trans('messages.titlu') . 3), 'class' => 'form-control']) !!}
                </div>
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('image', trans('messages.images')) !!}
            {!! Form::file('image', ['placeholder' => trans('messages.images'), 'id' => 'ambassadorImage', 'type' => 'file', 'class' => 'file']) !!}

            @if (isset($slider))
                <br/>
                <img src="{{ $slider->image }}" alt="{{ $slider->id }}" class="img-responsive"/>
            @endif
        </div>

        {!! Form::submit(trans('messages.save'), array( 'class' => 'btn btn-success pull-right') ) !!}

    </div>
</div>