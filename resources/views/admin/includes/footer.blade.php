
		<!-- Main Footer -->
		<footer class="main-footer">
		  <!-- To the right -->
		  <div class="pull-right hidden-xs">
		    <strong>Quart CMS</strong>  v 0.0.1
		  </div>
		  <!-- Default to the left --> 
		  <div class="copyright">
		  	<strong>© {{ date('Y') }} Quart Website.</strong> {{ trans('messages.all_rights_reserved') }}. {{ trans('messages.solution_by') }} <a title="web design" target="_blank" href="http://www.quart.ro/">Quart - Creative Agency</a>.
		  </div>
		</footer>


	</div> <!-- ./wrapper -->

	<!-- Scripts -->
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>

	<script src="{{ asset('/adm/plugins/datatables/jquery.dataTables.js') }}"></script>
	<script src="{{ asset('/adm/plugins/datatables/dataTables.bootstrap.js') }}"></script>
	<script src="{{ asset('/js/bootstrap-multiselect.js') }}"></script>

	<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>

	<script src="{{ asset('/adm/js/jquery/jquery.slimscroll.min.js') }}"></script>
    <script src="{{ asset('/adm/js/AdminLTE.min.js') }}"></script>
	<script src="{{ asset('/js/jquery-ui.min.js') }}"></script>
	<script src="{{ asset('/js/fileinput.min.js') }}"></script>

	@if (App::getLocale() == 'ro')
		<script src="{{ asset('/js/fileinput_locale_ro.js') }}"></script>
	@endif

	<script src="{{ asset('/adm/js/admin.js') }}"></script>

</body>
</html>
