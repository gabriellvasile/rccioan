<div role="tabpanel" class="tab-pane" id="projects">
    <div class="box box-danger">
        <div class="box-body clearfix projects editable" data-type="project" data-remove="{{ route('profile.removeFromProfile', $actor->user->username) }}">

            <div class="clearfix">
                <div class="btn btn-success pull-right addToProfile" data-url="{{ route('admin.ajaxNewProject', $actor->user->username) }}">+ {{ trans('messages.new_project') }} </div>
            </div>
            <br/>

            <?php $i = 1; ?>
            @foreach ($actor->user->projects as $key => $project)

                <div class="well project">
                    <div class="btn btn-danger deleteFromProfile" data-id="{{ $project->id }}">-</div>

                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                {!! Form::label('projects[' . $project->id . '][name]', 'Name') !!}
                                {!! Form::text('projects[' . $project->id . '][name]', $project->name , array('placeholder' => 'Name', 'class' => 'form-control')) !!}
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                {!! Form::label('projects[' . $project->id . '][house]', 'Production company / Theatre') !!}
                                {!! Form::text('projects[' . $project->id . '][house]', $project->house , array('placeholder' => 'Production company / Theatre', 'class' => 'form-control')) !!}
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                {!! Form::label('projects[' . $project->id . '][year]', 'Production Year / Season') !!}
                                {!! Form::text('projects[' . $project->id . '][year]', $project->year , array('placeholder' => 'Production Year / Season', 'class' => 'form-control')) !!}
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                {!! Form::label('projects[' . $project->id . '][director]', 'Director') !!}
                                {!! Form::text('projects[' . $project->id . '][director]', $project->director , array('placeholder' => 'Director', 'class' => 'form-control')) !!}
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                {!! Form::label('projects[' . $project->id . '][role]', 'Role') !!}
                                {!! Form::text('projects[' . $project->id . '][role]', $project->role , array('placeholder' => 'Role', 'class' => 'form-control')) !!}
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                {!! Form::label('projects[' . $project->id . '][description]', 'Description') !!}
                                {!! Form::textarea('projects[' . $project->id . '][description]', $project->description , array('placeholder' => 'Description', 'class' => 'form-control', 'rows' => 3)) !!}
                            </div>
                        </div>
                    </div>
                </div>

                <hr/>

            @endforeach

        </div>
    </div>
</div>