<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

<!-- sidebar: style can be found in sidebar.less -->
<section class="sidebar">

  <!-- Sidebar user panel (optional) -->
  <div class="user-panel">
    <div class="pull-left image">
      <img src="{{ asset('adm/dist/img/user2-160x160.jpg')  }}" class="img-circle" alt="User Image" />
    </div>
    <div class="pull-left info">
      <p>{{ $logged->fname . ' ' . $logged->lname }}</p>
      <!-- Status -->
      {{--<a href="#"><i class="fa fa-circle text-success"></i> Online</a>--}}
    </div>
  </div>

  <!-- search form (Optional) -->
  {{--<form action="#" method="get" class="sidebar-form">--}}
    {{--<div class="input-group">--}}
      {{--<input type="text" name="q" class="form-control" placeholder="Search..."/>--}}
      {{--<span class="input-group-btn">--}}
        {{--<button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>--}}
      {{--</span>--}}
    {{--</div>--}}
  {{--</form>--}}
  <!-- /.search form -->

  <!-- Sidebar Menu -->
  <ul class="sidebar-menu">
    <li>
        <a href="{{ route('admin.dashboard', ['locale' => App::getLocale()]) }}"><i class="fa fa-dashboard fa-fw"></i>{{ trans('messages.go_to_dashboard') }}</a>
    </li>
    <li class="treeview">
        <a href="{{ route('admin.user.index', ['locale' => App::getLocale()]) }}">
          <i class="fa fa-users"></i><span>{{ trans('messages.users') }}</span> <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{ route('admin.user.create', ['locale' => App::getLocale()]) }}"><i class="fa fa-circle-o"></i>{{ trans('messages.add_new') }}</a></li>
          <li><a href="{{ route('admin.user.needapprove', ['locale' => App::getLocale()]) }}"><i class="fa fa-circle-o"></i>{{ trans('messages.approve') }}</a></li>
          <li><a href="{{ route('admin.user.index', ['locale' => App::getLocale()]) }}"><i class="fa fa-circle-o"></i>{{ trans('messages.all_users') }}</a></li>
        </ul>
    </li>
    <li class="treeview">
        <a href="{{ route('admin.actor.index', ['locale' => App::getLocale()]) }}">
          <i class="fa fa-smile-o"></i><span>{{ trans('messages.actors') }}</span> <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          {{--<li><a href="{{ route('admin.actor.creat[e',, localelang' => App::getLocale()) })]"><i class="fa fa-circle-o"></i>{{ trans('messages.add_new') }}</a></li>--}}
          <li><a href="{{ route('admin.actor.needapprove', ['locale' => App::getLocale()]) }}"><i class="fa fa-circle-o"></i>{{ trans('messages.approve') }}</a></li>
          <li><a href="{{ route('admin.actor.index', ['locale' => App::getLocale()]) }}"><i class="fa fa-circle-o"></i>{{ trans('messages.all_actors') }}</a></li>
        </ul>
    </li>
    <li class="treeview">
        <a href="{{ route('admin.employer.index', ['locale' => App::getLocale()]) }}">
          <i class="fa fa-building"></i><span>{{ trans('messages.employers') }}</span> <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          {{--<li><a href="{{ route('admin.employer.creat[e',, localelang' => App::getLocale()) })]"><i class="fa fa-circle-o"></i>{{ trans('messages.add_new') }}</a></li>--}}
          <li><a href="{{ route('admin.employer.needapprove', ['locale' => App::getLocale()]) }}"><i class="fa fa-circle-o"></i>{{ trans('messages.approve') }}</a></li>
          <li><a href="{{ route('admin.employer.index', ['locale' => App::getLocale()]) }}"><i class="fa fa-circle-o"></i>{{ trans('messages.all_employers') }}</a></li>
        </ul>
    </li>
    <li class="treeview">
        <a href="{{ route('admin.ambassador.index', ['locale' => App::getLocale()]) }}">
            <i class="fa fa-users"></i><span>{{ trans('messages.ambassadors') }}</span> <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
            <li><a href="{{ route('admin.ambassador.create', ['locale' => App::getLocale()]) }}"><i class="fa fa-circle-o"></i>{{ trans('messages.add_new') }}</a></li>
            <li><a href="{{ route('admin.ambassador.index', ['locale' => App::getLocale()]) }}"><i class="fa fa-circle-o"></i>{{ trans('messages.all_ambassadors') }}</a></li>
        </ul>
    </li>
    <li class="treeview">
        <a href="{{ route('admin.lunar.index', ['locale' => App::getLocale()]) }}">
            <i class="fa fa-users"></i><span>{{ trans('messages.actor_month') }}</span> <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
            <li><a href="{{ route('admin.lunar.create', ['locale' => App::getLocale()]) }}"><i class="fa fa-circle-o"></i>{{ trans('messages.add_new') }}</a></li>
            <li><a href="{{ route('admin.lunar.index', ['locale' => App::getLocale()]) }}"><i class="fa fa-circle-o"></i>{{ trans('messages.all_actors_month') }}</a></li>
        </ul>
    </li>
    <li class="treeview">
        <a href="{{ route('admin.job.index', ['locale' => App::getLocale()]) }}">
            <i class="fa fa-users"></i><span>{{ trans('messages.jobs') }}</span> <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
            <li><a href="{{ route('admin.job.needapprove', ['locale' => App::getLocale()]) }}"><i class="fa fa-circle-o"></i>{{ trans('messages.approve') }}</a></li>
            <li><a href="{{ route('admin.job.index', ['locale' => App::getLocale()]) }}"><i class="fa fa-circle-o"></i>{{ trans('messages.all_jobs') }}</a></li>
        </ul>
    </li>
    <li class="treeview">
        <a href="{{ route('admin.page.index', ['locale' => App::getLocale()]) }}">
            <i class="fa fa-users"></i><span>{{ trans('messages.pages') }}</span> <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
            <li><a href="{{ route('admin.page.create', ['locale' => App::getLocale()]) }}"><i class="fa fa-circle-o"></i>{{ trans('messages.add_new') }}</a></li>
            <li><a href="{{ route('admin.page.index', ['locale' => App::getLocale()]) }}"><i class="fa fa-circle-o"></i>{{ trans('messages.all_pages') }}</a></li>
        </ul>
    </li>
    <li class="treeview">
        <a href="{{ route('admin.sliders.index', ['locale' => App::getLocale()]) }}">
            <i class="fa fa-users"></i><span>{{ trans('messages.sliders') }}</span> <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
            <li><a href="{{ route('admin.sliders.create', ['locale' => App::getLocale()]) }}"><i class="fa fa-circle-o"></i>{{ trans('messages.add_new') }}</a></li>
            <li><a href="{{ route('admin.sliders.index', ['locale' => App::getLocale()]) }}"><i class="fa fa-circle-o"></i>{{ trans('messages.all_sliders') }}</a></li>
        </ul>
    </li>
    <!-- Optionally, you can add icons to the links -->
    {{-- <li class="active"><a href="#"><span>Link</span></a><</li> --}}
  </ul>
</section>
<!-- /.sidebar -->
</aside>