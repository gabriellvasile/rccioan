
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <strong>{{ trans('messages.validation_error') }}</strong> <br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box box-danger">
    <div class="box-body clearfix">

        <div class="col-sm-4">
            <div class="form-group">
                {!! Form::label('name', 'Name') !!}
                {!! Form::text('name', null , array('placeholder' => 'Name', 'class' => 'form-control')); !!}
            </div>
            <div class="form-group">
                {!! Form::label('description1', 'Description 1') !!}
                {!! Form::textarea('description1', null , array('placeholder' => 'Description 1', 'class' => 'form-control')); !!}
            </div>
        </div>

        <div class="col-sm-4">
            <div class="form-group">
                {!! Form::label('video', 'Video') !!}
                {!! Form::text('video', null , array('placeholder' => 'Video', 'class' => 'form-control')); !!}
            </div>
            <div class="form-group">
                {!! Form::label('description2', 'Description 2') !!}
                {!! Form::textarea('description2', null , array('placeholder' => 'Description 2', 'class' => 'form-control')); !!}
            </div>
        </div>

        <div class="col-sm-4">
            <div class="form-group">
                {!! Form::label('quote', 'Quote') !!}
                {!! Form::text('quote', null , array('placeholder' => 'Quote', 'class' => 'form-control')); !!}
            </div>
            <div class="form-group">
                {!! Form::label('image', 'Image') !!}
                {!! Form::text('image', null , array('placeholder' => 'Image', 'class' => 'form-control')); !!}
            </div>

            {!! Form::submit(trans('messages.save'), array( 'class' => 'btn btn-success pull-right') ) !!}
        </div>

    </div>
</div>