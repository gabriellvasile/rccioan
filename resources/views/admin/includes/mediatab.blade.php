<div role="tabpanel" class="tab-pane" id="media">
    <div class="box box-danger">
        <div class="box-body clearfix whereToUpload" data-route="{{ route('profile.ajaxUpdateFiles', $user->username) }}">
            @include('profile._filesfields')
        </div>
    </div>
</div>