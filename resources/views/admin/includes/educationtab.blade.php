<div role="tabpanel" class="tab-pane" id="education">
    <div class="box box-danger">
        <div class="box-body clearfix">
            <div class="row">
                <div class="col-sm-2">
                    <div class="form-group">
                        {!! Form::label('profesional_studies', trans('messages.professional_studies')) !!}
                        {!! Form::select('profesional_studies', [trans('messages.no'), trans('messages.yes')], null, array('placeholder' => trans('messages.professional_studies'), 'class' => 'form-control')) !!}
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        {!! Form::label('speciality_studies', trans('messages.speciality_studies')) !!}
                        {!! Form::select('speciality_studies', [trans('messages.no'), trans('messages.yes')], null, array('placeholder' => trans('messages.speciality_studies'), 'class' => 'form-control')) !!}
                    </div>
                </div>
                
                <div class="col-sm-6">
                    <div class="form-group">
                        {!! Form::label('speciality_institution', trans('messages.speciality_institution')) !!}
                        {!! Form::text('speciality_institution', null, array('placeholder' => trans('messages.speciality_institution'), 'class' => 'form-control')) !!}
                    </div>
                </div>
                
                <div class="col-sm-2">
                    <div class="form-group">
                        {!! Form::label('speciality_year', trans('messages.speciality_year')) !!}
                        {!! Form::text('speciality_year', null, array('placeholder' => trans('messages.speciality_year'), 'class' => 'form-control')) !!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-2">
                    <div class="form-group">
                        {!! Form::label('faculty', trans('messages.faculty')) !!}
                        {!! Form::select('faculty', [trans('messages.no'), trans('messages.yes')], null, array('placeholder' => trans('messages.faculty'), 'class' => 'form-control')) !!}
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        {!! Form::label('faculty_institution', trans('messages.faculty_inst')) !!}
                        {!! Form::text('faculty_institution', null, array('placeholder' => trans('messages.faculty_inst'), 'class' => 'form-control')) !!}
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        {!! Form::label('faculty_domain', trans('messages.field')) !!}
                        {!! Form::text('faculty_domain', null, array('placeholder' => trans('messages.field'), 'class' => 'form-control')) !!}
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        {!! Form::label('faculty_student', trans('messages.is_still_student')) !!}
                        {!! Form::select('faculty_student', [trans('messages.no'), trans('messages.yes')], null, array('placeholder' => trans('messages.is_still_student'), 'class' => 'form-control')) !!}
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        {!! Form::label('faculty_year', trans('messages.faculty_year')) !!}
                        {!! Form::text('faculty_year', null, array('placeholder' => trans('messages.faculty_year'), 'class' => 'form-control')) !!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        {!! Form::label('awards', trans('messages.faculty_year')) !!}
                        {!! Form::textarea('awards', null, array('placeholder' => trans('messages.awards'), 'class' => 'form-control')) !!}
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        {!! Form::label('workshops', trans('messages.workshops')) !!}
                        {!! Form::textarea('workshops', null, array('placeholder' => trans('messages.workshops'), 'class' => 'form-control')) !!}
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>