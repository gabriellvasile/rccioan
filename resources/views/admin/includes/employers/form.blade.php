<div role="tabpanel">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active">
            <a href="#user" aria-controls="user" role="tab" data-toggle="tab">{{ trans('messages.the_user') }}</a>
        </li>
        <li role="presentation">
            <a href="#other" aria-controls="other" role="tab" data-toggle="tab">{{ trans('messages.other_info') }}</a>
        </li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        @include('admin.includes.usertab'   )

        <div role="tabpanel" class="tab-pane" id="other">
            <div class="box box-danger">
                <div class="box-body clearfix">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                {!! Form::label('company', trans('messages.company_name')) !!}
                                {!! Form::text('company', null, array('placeholder' => trans('messages.company_name'), 'class' => 'form-control')) !!}
                            </div>
                        </div>
                        <div class="col-sm-6">
                           <div class="form-group">
                                {!! Form::label('address', trans('messages.address')) !!}
                                {!! Form::text('address', null, array('placeholder' => trans('messages.address'), 'class' => 'form-control')) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                {!! Form::label('post_code', trans('messages.post_code')) !!}
                                {!! Form::text('post_code', null, array('placeholder' => trans('messages.post_code'), 'class' => 'form-control')) !!}
                            </div>
                        </div>
                        <div class="col-sm-6">
                           <div class="form-group">
                                {!! Form::label('website', trans('messages.website')) !!}
                                {!! Form::text('website', null, array('placeholder' => trans('messages.website'), 'class' => 'form-control')) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                {!! Form::label('info', trans('messages.company_info')) !!}
                                {!! Form::text('info', null, array('placeholder' => trans('messages.company_info'), 'class' => 'form-control')) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
{!! Form::submit(trans('messages.save'), array( 'class' => 'btn btn-success pull-right') ) !!}