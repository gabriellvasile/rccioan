
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <strong>{{ trans('messages.validation_error') }}</strong> <br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box box-danger">
    <div class="box-body clearfix">

        <div class="col-sm-4">
            <div class="form-group">
                {!! Form::label('name', trans('messages.name')) !!}
                {!! Form::text('name', null , array('placeholder' => trans('messages.name'), 'class' => 'form-control')) !!}
            </div>
        </div>

        <div class="col-sm-4">
            <div class="form-group">
                {!! Form::label('video', trans('messages.video')) !!}
                {!! Form::text('video', null , array('placeholder' => trans('messages.video'), 'class' => 'form-control')) !!}
            </div>
        </div>

        <div class="col-sm-4">
            <div class="form-group">
                {!! Form::label('quote', trans('messages.quote')) !!}
                {!! Form::text('quote', null , array('placeholder' => trans('messages.quote'), 'class' => 'form-control')) !!}
            </div>
        </div>

        <div class="col-sm-6">
            <div class="form-group">
                {!! Form::label('description', trans('messages.description')) !!}
                <br/>
                <br/>
                {!! Form::textarea('description', null , array('placeholder' => trans('messages.description'), 'class' => 'form-control tinymce')) !!}
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                {!! Form::label('image', trans('messages.images')) !!}
                {!! Form::file('image', ['placeholder' => trans('messages.images'), 'id' => 'ambassadorImage', 'type' => 'file', 'class' => 'file']) !!}

                @if (isset($ambassador))
                    <br/>
                    <img src="{{ $ambassador->image }}" alt="{{ $ambassador->name }}" class="img-responsive"/>
                @endif
            </div>

            {!! Form::submit(trans('messages.save'), array( 'class' => 'btn btn-success pull-right') ) !!}
        </div>

    </div>
</div>