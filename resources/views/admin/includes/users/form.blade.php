
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <strong>{{ trans('messages.validation_error') }}</strong> <br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box box-danger">
    <div class="box-body clearfix">

        <div class="col-sm-4">
            <div class="form-group">
                {!! Form::label('fname', trans('messages.first_name')) !!}
                {!! Form::text('fname', null , array('placeholder' => 'Firstname', 'class' => 'form-control')) !!}
            </div>
            
            <div class="form-group">
                {!! Form::label('email', trans('messages.email')) !!}
                {!! Form::email('email', null, array('placeholder' => trans('messages.email'), 'class' => 'form-control')) !!}
            </div>

            <div class="form-group">
                {!! Form::label('language_id', trans('messages.language')) !!}
                {!! Form::select('language_id', $languages, null, array('id' => 'language_id', 'placeholder' => trans('messages.language'), 'class' => 'form-control')) !!}
            </div>

            <div class="form-group">
                {!! Form::label('mobile', trans('messages.mobile')) !!}
                {!! Form::text('mobile', null, array('placeholder' => trans('messages.mobile'), 'class' => 'form-control')) !!}
            </div>

        </div>

        <div class="col-sm-4">
            <div class="form-group">
                {!! Form::label('lname', trans('messages.last_name')) !!}
                {!! Form::text('lname', null , array('placeholder' => trans('messages.last_name'), 'class' => 'form-control')) !!}
            </div>
            
            <div class="form-group">
                {!! Form::label('city', trans('messages.city')) !!}
                {!! Form::text('city', null, array('placeholder' => trans('messages.city'), 'class' => 'form-control')) !!}
            </div>

            <div class="form-group">
                {!! Form::label('country_id', trans('messages.country')) !!}
                {!! Form::select('country_id', $countries, null, array('id' => 'country_id', 'placeholder' => trans('messages.country'), 'class' => 'form-control')) !!}
            </div>

            <div class="form-group">
                {!! Form::label('role_id', trans('messages.rol')) !!}
                {!! Form::select('role_id', $roles, null, array('id' => 'role_id', 'placeholder' => trans('messages.rol'), 'class' => 'form-control')) !!}
            </div>
        </div>

        <div class="col-sm-4">

            <div class="form-group">
                {!! Form::label('sex', trans('messages.sex')) !!}
                {!! Form::select('sex', ['M' => 'M', 'F' => 'F'], null, array('placeholder' => trans('messages.sex'), 'class' => 'form-control')) !!}
            </div>

            <div class="form-group">
                {!! Form::label('birth_date', trans('messages.birth_date')) !!}
                {!! Form::text('birth_date', null, array('placeholder' => trans('messages.birth_date'), 'class' => 'form-control')) !!}
            </div>

            <div class="form-group subrole" style="display: none;" data-role="{{ $talentRoleId }}">
                {!! Form::label('actor_type_id', trans('messages.actor_type')) !!}
                {!! Form::select('actor_type_id', $actorTypes, (isset($user) && $user->role_id == $talentRoleId ? $user->actor->actortype->id : 0), array('placeholder' => 'Actor type', 'class' => 'form-control')) !!}
            </div>

            <div class="form-group subrole" style="display: none;" data-role="{{ $employerRoleId }}">
                {!! Form::label('employer_type_id', trans('messages.employer_type')) !!}
                {!! Form::select('employer_type_id', $employerTypes,  (isset($user) && $user->role_id == $employerRoleId ? $user->employer->employertype->id : 0), array('placeholder' => 'Employer type', 'class' => 'form-control')) !!}
            </div>
            
            {!! Form::submit(trans('messages.save'), array( 'class' => 'btn btn-success pull-right') ) !!}
        </div>

    </div>
</div>

@section('extrafooter')
<script>
    $(document).ready(function($) {
        var selectValue = $('#role_id').val();

        $('.subrole').each(function(index, el) {
            if( $(el).data('role') == selectValue )
            {
                $(el).show();
            }
        });

        $('#role_id').change(function(event) {
            $('.subrole').hide();
            var selectValue = $(this).val();

            $('.subrole').each(function(index, el) {
                if( $(el).data('role') == selectValue )
                {
                    $(el).show();
                }
            });
        });
    });
</script>
@stop