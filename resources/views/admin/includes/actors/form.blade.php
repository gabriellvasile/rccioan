<!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active">
        <a href="#user" aria-controls="user" role="tab" data-toggle="tab">{{ trans('messages.the_user') }}</a>
    </li>
    <li role="presentation">
        <a href="#characteristics" aria-controls="characteristics" role="tab" data-toggle="tab">{{ trans('messages.characteristics') }}</a>
    </li>
    <li role="presentation">
        <a href="#aptitudes" aria-controls="aptitudes" role="tab" data-toggle="tab">{{ trans('messages.aptitudes') }}</a>
    </li>
    <li role="presentation">
        <a href="#education" aria-controls="education" role="tab" data-toggle="tab">{{ trans('messages.education') }}</a>
    </li>
    <li role="presentation">
        <a href="#projects" aria-controls="projects" role="tab" data-toggle="tab">{{ trans('messages.projects_title') }}</a>
    </li>
    <li role="presentation">
        <a href="#other" aria-controls="other" role="tab" data-toggle="tab">{{ trans('messages.other_info') }}</a>
    </li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
    @include('admin.includes.usertab')

    <div role="tabpanel" class="tab-pane" id="characteristics">
        <div class="box box-danger">
            <div class="box-body clearfix">

                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            {!! Form::label('ethnicity[id]', trans('messages.ethnicity')) !!}
                            {!! Form::select('ethnicity[id]', $ethnicities, null , array('placeholder' => trans('messages.ethnicity'), 'class' => 'form-control')) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('eyecolour[id]', trans('messages.eye_colour')) !!}
                            {!! Form::select('eyecolour[id]', $eyecolours, null, array('placeholder' => trans('messages.eye_colour'), 'class' => 'form-control')) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('haircolour[id]', trans('messages.hair_colour')) !!}
                            {!! Form::select('haircolour[id]', $haircolours, null, array('placeholder' => trans('messages.hair_colour'), 'class' => 'form-control')) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('ranges', trans('messages.range')) !!}
                            {!! Form::select('ranges[]', $ranges, isset($current_ranges) ? $current_ranges : null, ['class' => 'form-control', 'id' => 'ranges', 'multiple' => '']) !!}
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            {!! Form::label('height', trans('messages.height')) !!}
                            {!! Form::text('height', null , array('placeholder' => trans('messages.height'), 'class' => 'form-control')) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('weight', trans('messages.weight')) !!}
                            {!! Form::text('weight', null, array('placeholder' => trans('messages.weight'), 'class' => 'form-control')) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('accent', trans('messages.accent')) !!}
                            {!! Form::text('accent', null, array('placeholder' => trans('messages.accent'), 'class' => 'form-control')) !!}
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            {!! Form::label('hair_length', trans('messages.hair_length')) !!}
                            {!! Form::select('hair_length', $hairlengths, null , array('placeholder' => trans('messages.hair_length'), 'class' => 'form-control')) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('spoken_languages', trans('messages.languages')) !!}
                            {!! Form::text('spoken_languages', null, array('placeholder' => trans('messages.languages'), 'class' => 'form-control')) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('facial_traits', trans('messages.facial_traits')) !!}
                            {!! Form::text('facial_traits', null, array('placeholder' => trans('messages.facial_traits'), 'class' => 'form-control')) !!}
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>

    <div role="tabpanel" class="tab-pane" id="aptitudes">
        <div class="box box-danger">
            <div class="box-body clearfix">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            {!! Form::label('voice', trans('messages.voice')) !!}
                            {!! Form::select('voice', [trans('messages.no'), trans('messages.yes')], null, array('placeholder' => trans('messages.voice'), 'class' => 'form-control')) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('dance', trans('messages.dance')) !!}
                            {!! Form::text('dance', null, array('placeholder' => trans('messages.dance'), 'class' => 'form-control')) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('licences', trans('messages.licence')) !!}
                            {!! Form::select('licences[]', $licences, isset($current_licences) ? $current_licences : null, ['class' => 'form-control', 'id' => 'licences', 'multiple' => '']) !!}
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group">
                            {!! Form::label('instruments', trans('messages.instruments')) !!}
                            {!! Form::text('instruments', null, array('placeholder' => trans('messages.instruments'), 'class' => 'form-control')) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('sports', trans('messages.sports')) !!}
                            {!! Form::text('sports', null, array('placeholder' => trans('messages.sports'), 'class' => 'form-control')) !!}
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    @include('admin.includes.educationtab')

    <div role="tabpanel" class="tab-pane" id="other">
        <div class="box box-danger">
            <div class="box-body clearfix">
                <div class="row">
                    <div class="col-sm-6">

                        <div class="form-group">
                            {!! Form::label('role_abroad', trans('messages.role_abroad')) !!}
                            {!! Form::select('role_abroad', [trans('messages.no'), trans('messages.yes')], null, array('placeholder' => trans('messages.role_abroad'), 'class' => 'form-control')) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('info', trans('messages.extra_info')) !!}
                            {!! Form::textarea('info', null, array('placeholder' => trans('messages.extra_info'), 'class' => 'form-control')) !!}
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group">
                            {!! Form::label('website', trans('messages.website')) !!}
                            {!! Form::text('website', null, array('placeholder' => trans('messages.website'), 'class' => 'form-control')) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('production_types', trans('messages.interested_in')) !!}
                            <div id="productions" class="row">
                                @foreach ($productions as $key => $production)
                                    <div class="col-sm-4">
                                        {!! Form::checkbox('productions[' . $key . ']', $key, in_array($key, $user_productions) ? true : false , ['id' => 'productions[' . $key . ']']) !!}
                                        {!! Form::label('productions[' . $key . ']',  $production) !!}
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    @include('admin.includes.projectstab')

</div>
{!! Form::submit(trans('messages.save'), array( 'class' => 'btn btn-success pull-right') ) !!}
