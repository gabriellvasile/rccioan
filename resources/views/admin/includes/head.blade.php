<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	{{-- <meta name="viewport" content="width=device-width, initial-scale=1"> --}}
	<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
	<title>Quart CMS</title>

	<meta name="csrf-token" content="{{ csrf_token() }}" />

	<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />
	<link href="{{ asset('/css/jquery-ui.min.css') }}" rel="stylesheet">

	<link href="{{ asset('/adm/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet" type="text/css" />

	
	<link href="{{ asset('/adm/css/AdminLTE.min.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('/adm/css/skins/skin-red.min.css') }}" rel="stylesheet" type="text/css" />

	<link href="{{ asset('/css/bootstrap-multiselect.css') }}" rel="stylesheet">
	<link href="{{ asset('/css/fileinput.css') }}" rel="stylesheet">

	<link href="{{ asset('/adm/css/admin.css') }}" rel="stylesheet" type="text/css" />

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body  class="skin-red fixed">
	<div class="wrapper">