<ul class="pagination">
    @if ($paginator->currentPage() > 1)
        <li>
            <a href="{{ $paginator->url($paginator->currentPage()-1) }}">{{ trans('messages.back') }}</a>
        </li>
    @endif

    @if ($paginator->hasMorePages())
        <li>
            <a href="{{ $paginator->url($paginator->currentPage()+1) }}" >{{ trans('messages.next') }}</a>
        </li>
    @endif
</ul>
