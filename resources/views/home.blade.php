
@extends('layouts.app')

@section('slider')
	@include('includes._slider')
@endsection

@section('content')

	<div class="homeFunctions">
		<h1>{{ trans('messages.how_does_rcc_works') }}</h1>
		
		<h2>{{ trans('messages.simple') }}</h2>
		
		<hr>

		<div class="row hows">
			<div class="col-sm-3 how">
				<img src="{{ route('home') . '/images/circle1.png' }}" alt="">
				<h3>{{ trans('messages.you_signup') }}</h3>
				<p>{{ trans('messages.you_signup_text') }}</p>
			</div>
			<div class="col-sm-3 how">
				<img src="{{ route('home') . '/images/circle2.png' }}" alt="">
				<h3>{{ trans('messages.you_complete_profile') }}</h3>
				<p>{{ trans('messages.you_complete_profile_text') }}</p>
			</div>
			<div class="col-sm-3 how">
				<img src="{{ route('home') . '/images/circle3.png' }}" alt="">
				<h3>{{ trans('messages.you_apply') }}</h3>
				<p>{{ trans('messages.you_apply_text') }}</p>
			</div>
			<div class="col-sm-3 how">
				<img src="{{ route('home') . '/images/circle4.png' }}" alt="">
				<h3>{{ trans('messages.you_win') }}</h3>
				<p>{{ trans('messages.you_win_text') }}</p>
			</div>
		</div>

		<hr>

		<h2>{{ trans('messages.call_to_action') }}</h2>

		<a href="{{ route('jobs.list') }}" class="btn btn-blue">{{ trans('messages.call_to_action_button') }}</a>

	</div>
</div>
<div class="bandaHome">
	<div class="container">
		<h1>{{ trans('messages.never_so_easy') }}</h1>
		<h2>{{ trans('messages.sign_up_call_to_action') }}</h2>


		<div id="ambasadorCarousel" class="carousel slide" data-ride="carousel">
			<div class="carousel-inner" role="listbox">
				<?php $i = 1; ?>
				@foreach ($ambassadors as $ambassador)
					@if ($i % 3 == 1)
						<div class="item @if ($i == 1) {{ 'active' }} @endif">
					@endif
							<div class="ambasadorVideo">
								<div class="video" data-src="{{ $ambassador->video }}"></div>
							</div>

					@if ($i % 3 == 0)
						</div>
					@endif
					<?php $i++; ?>
				@endforeach

				@if ($i % 3 != 1)
					</div>
				@endif
			</div>
			<a class="left carousel-control" href="#ambasadorCarousel" role="button" data-slide="prev"></a>
			<a class="right carousel-control" href="#ambasadorCarousel" role="button" data-slide="next"></a>
		</div>

		<h3>{{ trans('messages.start_career') }}</h3>
		{!! link_to_action('Auth\AuthController@getRegister', trans('messages.register'), [], ['class' => 'btn btn-white']) !!}
	</div>
</div>

<div class="container">
	<div class="comes row">
		<div class="col-sm-9">
			<h1>{!! $page->excerpt !!}</h1>

			{!! $page->content !!}

			<img class="img-responsive" src="{{ route('home') . '/images/signature.png' }}" alt="">
		</div>
		<div class="col-sm-3">
			<img class="img-responsive" src="{{ route('home') . '/images/laura.png' }}" alt="">
		</div>
	</div>
@endsection