@extends('layouts.special')

@section('content')

	<div class="register">
		<h1>Register</h1>

		@include('errors._validation')

		<form class="form-horizontal" id="postRegister" role="form" method="POST" action="{{ route('postRegister', App::getLocale()) }}">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">

			<div class="form-group">
				{!! Form::label('fname', trans('messages.first_name'), ['class' => 'col-xs-4 control-label']) !!}
				<div class="col-md-4 col-xs-8">
					{!! Form::text('fname', old('fname'), ['class' => 'form-control']) !!}
				</div>
			</div>

			<div class="form-group">
				{!! Form::label('lname', trans('messages.last_name'), ['class' => 'col-xs-4 control-label']) !!}
				<div class="col-md-4 col-xs-8">
					{!! Form::text('lname', old('lname'), ['class' => 'form-control']) !!}
				</div>
			</div>

			<div class="form-group">
				{!! Form::label('email', trans('messages.email'), ['class' => 'col-xs-4 control-label']) !!}
				<div class="col-md-4 col-xs-8">
					{!! Form::email('email', old('email'), ['class' => 'form-control']) !!}
				</div>
			</div>

			<div class="form-group">
				{!! Form::label('employer_type', trans('messages.employer_type'), ['class' => 'col-xs-4 control-label']) !!}
				<div class="col-md-4 col-xs-8">
					{!! Form::select('employer_type', $employer_types, old('employer_type'), ['class' => 'form-control']) !!}
				</div>
			</div>

			<div class="form-group">
				{!! Form::label('password', trans('messages.password'), ['class' => 'col-xs-4 control-label']) !!}
				<div class="col-md-4 col-xs-8">
					{!! Form::password('password', ['class' => 'form-control']) !!}
				</div>
			</div>

			<div class="form-group">
				{!! Form::label('password_confirmation', trans('messages.confirm_password'), ['class' => 'col-xs-4 control-label']) !!}
				<div class="col-md-4 col-xs-8">
					{!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
				</div>
			</div>

			{{-- https://github.com/greggilbert/recaptcha --}}
			<div class="form-group">
				<label class="col-xs-4 control-label" for="">{{ trans('messages.check_captcha_box') }}</label>
				<div class="col-md-4 col-xs-8">
					{!! Recaptcha::render(['lang' => App::getLocale()]) !!}
				</div>
			</div>

			<div class="form-group">
				<label class="col-xs-4 control-label"></label>
				<div class="col-md-4 col-xs-8">
					{!! Form::input('checkbox', 'terms', old('terms'), ['id' => 'terms']) !!}
					{{ trans('messages.read') }} <a href="/{{ $terms }}">{{ trans('messages.terms') }}</a>
				</div>
			</div>

			<div class="form-group">
				<div class="col-md-4 col-xs-8 col-xs-offset-4">
					{!! Form::submit(trans('messages.register'), ['class' => 'btn btn-primary']) !!}
				</div>
			</div>

		</form>
	</div>
				
@endsection

