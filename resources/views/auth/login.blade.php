@extends('layouts.special')

@section('content')

<h1 style="color: #fff;" class="text-center">{{ trans('messages.login') }}</h1>

@include('errors._validation')

<form class="form-horizontal" id="loginForm" role="form" method="POST" action="{{ route('postLogin', App::getLocale()) }}">
	<input type="hidden" name="_token" value="{{ csrf_token() }}">

	<div class="form-group">
		<label class="col-xs-4 control-label">{{ trans('messages.email') }}</label>
		<div class="col-xs-8 col-sm-4">
			<input type="email" class="form-control" name="email" value="{{ old('email') }}">
		</div>
	</div>

	<div class="form-group">
		<label class="col-xs-4 control-label">{{ trans('messages.password') }}</label>
		<div class="col-xs-8 col-sm-4">
			<input type="password" class="form-control" name="password">
		</div>
	</div>

	<div class="form-group">
		<div class="col-xs-8 col-sm-4 col-xs-offset-4">
			<div class="checkbox">
				<label>
					<input type="checkbox" name="remember"> {{ trans('messages.remember_me') }}
				</label>
			</div>
		</div>
	</div>

	{{-- https://github.com/anhskohbo/no-captcha --}}
	<div class="form-group">
		<label class="col-xs-4 control-label" for="">{{ trans('messages.check_captcha_box') }}</label>
		<div class="col-xs-8 col-sm-4">
			{!! Recaptcha::render(['lang' => App::getLocale()]) !!}
		</div>
	</div>

	<div class="form-group">
		<div class="col-xs-8 col-sm-4 col-xs-offset-4">
			<button type="submit" class="btn btn-primary">{{ trans('messages.login') }}</button>

			<a class="btn-link" href="{{ url('/password/email') }}">{{ trans('messages.forgot_password') }}</a>
		</div>
	</div>
</form>
@endsection
			

	
