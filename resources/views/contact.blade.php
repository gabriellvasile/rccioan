
@extends('layouts.page')

@section('content')
	
	<div class="la-anim-10"></div>

	<div class="contact">
		<h1>{!! $page->title !!}</h1>
		<hr>
		<div class="row contactCols">
			<div class="col-sm-6 contactCol">
				<div>
					<strong>{{ trans('messages.for_information') }}:</strong>
				</div>
				<div>
					<a class="email" href="mailto:office@romaniacastingcall.com">office@romaniacastingcall.com</a>
				</div>
				<div>
					<span class="phone">+40 758 08 08 21</span>
				</div>
			</div>
			<div class="col-sm-6 contactCol">
				<div>
					<strong>{{ trans('messages.for_partnerships') }}:</strong>
				</div>
				<div>
					<a class="email" href="mailto:marketing@romaniacastingcall.com">marketing@romaniacastingcall.com</a>
				</div>
				<div class="phone">+40 758 08 08 21</div>
			</div>
		</div>
		<hr class="smallHr">

		<h3>{{ trans('messages.contact_form') }}</h3>
		<div class="contactInfo">
			{!! $page->content !!}
		</div>

		<div class="alert alert-success alert-dismissible" role="alert">
		  <button type="button" class="close" data-dismiss="alert" aria-label="{{ trans('messages.close') }}"><span aria-hidden="true">&times;</span></button>
		  <strong>{{ trans('messages.success') }}</strong>
		  {{ trans('messages.contact_success') }}
		</div>

		{!! Form::open( ['route' => 'contactPost', 'id' => 'contactForm', 'data-redirect' => (App::getLocale() == 'en') ? route('contact-us') : route('contact')] ) !!}

			<div class="row">
		    	<div class="col-sm-6">
			    	<div class="form-group">
			    		{!! Form::text( 'last_name', null , ['placeholder' => trans('messages.last_name'), 'class' => 'form-control', 'id' => 'last_name']) !!}
			    	</div>
			    </div>
				<div class="col-sm-6">
					<div class="form-group">
			    		{!! Form::text( 'first_name', null , ['placeholder' => trans('messages.first_name'), 'class' => 'form-control', 'id' => 'first_name']) !!}
			    	</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<div class="form-group">
						{!! Form::text( 'email', null , ['placeholder' => trans('messages.email'), 'class' => 'form-control', 'id' => 'email']) !!}
					</div>
				</div>
				<div class="col-sm-6">
					<div class="form-group">
						{!! Form::text( 'subject', null , ['placeholder' => trans('messages.subject'), 'class' => 'form-control', 'id' => 'subject']) !!}
					</div>
				</div>
			</div>
			<div class="form-group">
				{!! Form::textarea( 'message', null , ['placeholder' => trans('messages.message'), 'class' => 'form-control', 'id' => 'message']) !!}
			</div>

			<div class="form-group">
				<div class="contactCaptcha">
					<label for="">{{ trans('messages.check_captcha_box') }}</label>
					{!! Recaptcha::render(['lang' => App::getLocale()]) !!}
				</div>
			</div>

			{!! Form::submit(trans('messages.send'), ['class' => 'btn btn-primary']) !!}

			<div>
				{!! $page->excerpt !!}
			</div>


		{!! Form::close() !!}
	</div>
@endsection

@section('afterfooter')
	<script src="{{ asset('/js/contact.js') }}"></script>
@stop