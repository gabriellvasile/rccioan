@extends('layouts.page')

@section('content')

    <div class="container wrapListareAmbasadori">

        <div class="text-center topTitle">
            <h2>{!! $page->title !!}</h2>
            <hr>
        </div>

        <div class="row">
            @forelse($ambassadors as $key => $ambassador)
                <div class="ambasador clearfix">
                    <h1>{{ $ambassador->name }}</h1>
                    <img src="{{ $ambassador->image }}" alt=""/>
                    <div class="quote">"{{ $ambassador->quote }}"</div>
                    <div class="video"></div>
                    <div class="description">{!! $ambassador->description !!}</div>
                    <div class="clearfix">
                        <a href="#" class="btn" data-back="{{ trans('messages.back') }}" data-more="{{ trans('messages.more') }}"  data-video="https://www.youtube.com/embed/Mo4cmTaEDIk">
                            {{ trans('messages.more') }}
                        </a>
                    </div>
                </div>
            @empty
                <h1 class="text-center">No ambassadors to show.</h1>
            @endforelse
        </div>

    </div>

@stop

@section('afterfooter')
@stop