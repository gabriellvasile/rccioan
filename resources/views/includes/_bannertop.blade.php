<div id="bannerTop">
	@if(isset($page) && $page->image)
		<img src="{{ $page->image }}" alt="{{ $page->title }}"  id="{{ $page->id }}">
	@else
		<img src="{{ route('home') . '/images/banner1.jpg' }}">
	@endif

	{{--<div class="bannerDesc">--}}
		{{--<h3>{{ trans('messages.never_so_easy') }}</h3>--}}
		{{--<h3>{{ trans('messages.register_to_casting') }}</h3>--}}
		{{--<div class="secondaryDesc">{{ trans('messages.register_and_start_career') }}</div>--}}
	{{--</div>--}}
</div>