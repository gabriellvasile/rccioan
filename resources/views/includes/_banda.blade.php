
<div id="banda">
	@if( !empty($listActors) )
		@foreach($children as $kid)
			@if (isset($actor_type_active->id) && $kid->parent_id == $actor_type_active->id)
				<a href="/{{ $kid->slug }}" class="btn-profil active">{{ $kid->excerpt }}</a>
			@else
				<a href="/{{ $kid->slug }}" class="btn-profil">{{ $kid->excerpt }}</a>
			@endif
		@endforeach
	@elseif ( isset($logged) )
		@if( $admin )
			{!! link_to_route('admin.dashboard', trans('messages.go_to_dashboard'), [], ['class' => 'btn-profil']) !!}
			{!! link_to_route('admin.user.create', trans('messages.create_actor'), [], ['class' => 'btn-profil']) !!}
			{!! link_to_route('admin.employer.index', trans('messages.list_employers'), [], ['class' => 'btn-profil']) !!}
			{!! link_to_route('admin.user.needapprove', trans('messages.approve_users'), [], ['class' => 'btn-profil']) !!}
			{!! link_to_route('admin.ambassador.create', trans('messages.create_ambassador'), [], ['class' => 'btn-profil']) !!}
		@elseif ($userType == 'actor')
			{!! link_to_route('profile.show', trans('messages.profile'), [$logged->username, App::getLocale()], ['class' => 'btn-profil']) !!}
			{!! link_to_route('profile.edit', trans('messages.edit_profile'), [$logged->username, App::getLocale()], ['class' => 'btn-profil']) !!}
			{!! link_to_route('messages.list', trans('messages.messages'), App::getLocale(), ['class' => 'btn-profil']) !!}
			{!! link_to_route('casts.mine', trans('messages.my_castings'), App::getLocale(), ['class' => 'btn-profil']) !!}
		@elseif( $userType == 'employer' )
			{!! link_to_route('profile.show', trans('messages.profile'), [$logged->username, App::getLocale()], ['class' => 'btn-profil']) !!}
			{!! link_to_route('profile.edit', trans('messages.edit_profile'), [$logged->username, App::getLocale()], ['class' => 'btn-profil']) !!}
			{!! link_to_route('jobs.create', trans('messages.add_job'), App::getLocale(), ['class' => 'btn-profil']) !!}
			{!! link_to_route('jobs.mine', trans('messages.my_jobs'), App::getLocale(), ['class' => 'btn-profil']) !!}
			{!! link_to_route('messages.list', trans('messages.messages'), App::getLocale(), ['class' => 'btn-profil']) !!}
		@endif
	@else
		<div>Pentru a putea accesa toate paginile website-ului te rugam sa te loghezi sau sa iti creezi propriul cont</div>
		
		{!! link_to_action('Auth\AuthController@getLogin', trans('messages.login'), App::getLocale(), ['class' => 'btn btn-blue']) !!}
        {!! link_to_action('Auth\AuthController@getRegister', trans('messages.register'), App::getLocale(), ['class' => 'btn btn-white']) !!}
	@endif
</div>