	
		<footer>
			<div class="container">

				<div class="socials">
					<a href="#"></a>
					<a href="#"></a>
					<a href="#"></a>
					<a href="#"></a>
				</div>


				@if(isset($pages) && $pages)
					<ul class="terms">
						@foreach($pages as $page)
							<li><a href="/{{ $page->slug }}">{{ $page->title }}</a></li>
						@endforeach
					</ul>
				@endif

				<div class="copyright text-center">
					© 2015 Castalia Production. {{ trans('messages.all_rights_reserved') }} {{ trans('messages.solution_by') }} <a title="web design" target="_blank" href="http://www.quart.ro/">Quart - Creative Agency</a>.
				</div>

				<a href="#" id="toTop"></a>
			</div>

		</footer>


		<nav id="menu">
			<ul>
				<li><a href="{{ $base_url }}">{{ trans('messages.home') }}</a></li>
				@foreach ($topmenu as $menu)
					<li><a href="/{{ $menu->slug }}">{{ $menu->title }}</a></li>
				@endforeach
			</ul>
		</nav>

		<div id="menuButtons">
			@if ( isset($logged) )
				@if ( !$admin )
					<div class="menuButton">{!! link_to_route('profile.show', trans('messages.profile'), [$logged->username, App::getLocale()], ['class' => 'btn btn-white']) !!}</div>
					<div class="menuButton">{!! link_to_route('profile.edit', trans('messages.edit_profile'), [$logged->username, App::getLocale()], ['class' => 'btn']) !!}</div>
				@endif
			@else
				<div class="menuButton">
					{!! link_to_action('Auth\AuthController@getLogin', trans('messages.login'), [], ['class' => 'btn btn-blue']) !!}
				</div>

				<div class="menuButton">
					{!! link_to_action('Auth\AuthController@getRegister', trans('messages.register'), [], ['class' => 'btn btn-white']) !!}
				</div>
			@endif

			@if(isset($pages) && $pages)
				<div class="termsMenu">
					@foreach($pages as $page)
						<div><a href="{{ $page->slug }}">{{ $page->title }}</a></div>
					@endforeach
				</div>
			@endif

			<div class="socials2">
				<a href="#"></a>
				<a href="#"></a>
				<a href="#"></a>
				<a href="#"></a>
			</div>

			<div class="copyright">
				© 2015 Castalia Production. <br>
				{{ trans('messages.all_rights_reserved') }} <br>
				{{ trans('messages.solution_by') }}<a href="http://www.quart.ro" target="_blank">Quart Creative Agency</a>.
			</div>

		</div>

		<a class="mmenuLink" href="#menu"></a>
	</div> {{-- mmenu --}}

	<!-- Scripts -->
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
	<script src="{{ asset('/js/jquery-ui.min.js') }}"></script>
	{{-- <script src="{{ asset('/js/datepicker-ro.js') }}"></script> --}}

	<script type="text/javascript">var switchTo5x=true;</script>
	<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
	<script type="text/javascript">stLight.options({publisher: "ed4f43e1-5de8-48ea-8447-eb68c80c55e5", doNotHash: true, doNotCopy: false, hashAddressBar: false});</script>

	<script src="{{ asset('/js/jquery.mmenu.min.all.js') }}"></script>
	<script src="{{ asset('/js/bootstrap-multiselect.js') }}"></script>
	<script src="{{ asset('/js/jquery.fancybox.js') }}"></script>
	<script src="{{ asset('/js/fileinput.min.js') }}"></script>

	@if (App::getLocale() == 'ro')
		<script src="{{ asset('/js/fileinput_locale_ro.js') }}"></script>
	@endif

	<script src="{{ asset('/js/custom.js') }}"></script>

	@yield('afterfooter')
</body>
</html>
