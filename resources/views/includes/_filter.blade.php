<div class="wrapListareActori">

    <div class="text-center topTitle">
        <h1>{{ $filter->excerpt }}</h1>
        <hr>
        <p>{{ $filter->content }}</p>
    </div>
    <div class="wrapAdvancedSearch">
        <h2><div class="clickFilter">{{ trans('messages.advanced_search') }}</div></h2>

        {!! Form::open( ['method' => 'get', 'url' => '/' . $filter->slug, 'id' => 'filterForm'] ) !!}

        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    {!! Form::label('sex', trans('messages.sex')) !!}
                    {!! Form::select('sex', [trans('messages.select'), 'M' => 'M', 'F' => 'F'], old('sex'), ['class' => 'form-control']) !!}
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    {!! Form::label('interested', trans('messages.interested_in')) !!}
                    {!! Form::select('interested', $categories, old('interested'), ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    {!! Form::label('ethnicity_id', trans('messages.ethnicity')) !!}
                    {!! Form::select('ethnicity_id', $ethnicities, old('ethnicity_id'), ['class' => 'form-control']) !!}
                </div>
            </div>
        </div>

        <div class="row">

            <div class="col-md-3">
                <div class="form-group">
                    {!! Form::label('range_id', trans('messages.range')) !!}
                    {!! Form::select('range_id', $ranges, old('range_id'), ['class' => 'form-control']) !!}
                </div>
            </div>

            <div class="col-md-3">
                <div class="form-group">
                    {!! Form::label('hair_length', trans('messages.hair_length')) !!}
                    {!! Form::select('hair_length', [trans('messages.select'), 'S' => trans('messages.small'), 'M' => trans('messages.medium'), 'L' => trans('messages.large')], old('hair_length'), ['class' => 'form-control']) !!}
                </div>
            </div>

            <div class="col-md-3">
                <div class="form-group">
                    {!! Form::label('hair_colour_id', trans('messages.hair_colour')) !!}
                    {!! Form::select('hair_colour_id', $haircolours, old('hair_colour_id'), ['class' => 'form-control']) !!}
                </div>
            </div>

            <div class="col-md-3">
                <div class="form-group">
                    {!! Form::label('eye_colour_id', trans('messages.eye_colour')) !!}
                    {!! Form::select('eye_colour_id', $eyecolours, old('eye_colour_id'), ['class' => 'form-control']) !!}
                </div>
            </div>

        </div>

        <div class="row searchChecks">
            <div class="col-sm-12">
                {!! Form::checkbox('role_abroad', old('role_abroad'), null, ['id' => 'role_abroad']) !!}
                {!! Form::label('role_abroad', trans('messages.accepts_role_abroad')) !!}
                {!! Form::checkbox('faculty_student', old('faculty_student'), null, ['id' => 'faculty_student']) !!}
                {!! Form::label('faculty_student', trans('messages.is_student')) !!}
            </div>
        </div>

        {!! Form::submit(trans('messages.search'), array( 'class' => 'btn btn-green') ) !!}

        {!! Form::close() !!}
    </div>
</div>