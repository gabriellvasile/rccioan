<div id="qrt-slideshow" class="carousel slide">
	<div class="carousel-inner">
		@foreach ( $sliders as $key=>$slider )
			<div class="item item-{{ $key }} <?php if ( 0 == $key ) { echo 'active'; } ?>">
				<img src="{{ $slider['image'] }} " alt="{{ $slider['title1_' . App::getLocale()] }}">

				<div class="carousel-caption">
					<h3>{{ $slider['title1_' . App::getLocale()] }}</h3>
					<h3>{{ $slider['title2_' . App::getLocale()] }}</h3>

					@if ( $slider['desc_' . App::getLocale()] )
						<div class="secondaryDesc">{{ $slider['desc_' . App::getLocale()] }}</div>
					@endif
				</div>
			</div>
		@endforeach
	</div>

	@if ( count($sliders) > 1 )
		<!-- Controls -->
		<a class="left carousel-control" href="#qrt-slideshow" data-slide="prev">
			<span class="icon-prev"></span>
		</a>
		<a class="right carousel-control" href="#qrt-slideshow" data-slide="next">
			<span class="icon-next"></span>
		</a>
		<!-- Indicators -->
		<ol class="carousel-indicators">
			@foreach ( $sliders as $key=>$slider )
	    		<li data-target="#qrt-slideshow" data-slide-to="{{ $key }}" <?php if ( 0 == $key ) { echo 'active'; } ?>></li>
	    	@endforeach
		</ol>
	
		<div id="qrt-buttons" class="qrt-buttons"></div>
	@endif	

	<div class="slideBtns">
		@if ( isset($logged) )

			@if( $admin )
				{!! link_to_route('admin.dashboard', trans('messages.go_to_dashboard'), ['locale' => App::getLocale()], ['class' => 'btn btn-white']) !!}
				{!! link_to_route('admin.user.create', trans('messages.create_actor'), ['locale' => App::getLocale()], ['class' => 'btn']) !!}
				{!! link_to_route('admin.employer.index', trans('messages.list_employers'), ['locale' => App::getLocale()], ['class' => 'btn btn-white']) !!}
				{!! link_to_route('admin.user.needapprove', trans('messages.approve_users'), ['locale' => App::getLocale()], ['class' => 'btn']) !!}
			@else
				{!! link_to_route('profile.show', trans('messages.profile'), [$logged->username, App::getLocale()], ['class' => 'btn btn-white']) !!}
				{!! link_to_route('profile.edit', trans('messages.edit_profile'), [$logged->username, App::getLocale()], ['class' => 'btn']) !!}
			@endif
		{{--@else--}}
			{{--{!! link_to_action('Auth\AuthController@getLogin', trans('messages.login'), [], ['class' => 'btn btn-blue']) !!}--}}
        	{{--{!! link_to_action('Auth\AuthController@getRegister', trans('messages.register'), [], ['class' => 'btn btn-white']) !!}--}}
		@endif
	</div>

	<div id="mouse"></div>

</div>
