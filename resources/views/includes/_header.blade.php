
@include('includes._navbar')

<div class="container">
	<header id="header">
	
		<div class="row" id="tophead">
		  <div class="col-lg-3">
			<ul id="langSelect">
			  @foreach ($change_languages as $lang_code => $lang_uri)
				  <li>
					  <a href="{{ $lang_uri }}"
						 @if ($lang_code == App::getLocale()) class="active" @endif
							  >
						  {{ $lang_code }}
					  </a>
				  </li>
			  @endforeach
			</ul>

		    <a class="brand" href="{{ $base_url }}" 
		       title="" 
		       rel="home">
		      <img src="{{ $logo }}" alt="">
		    </a>
		  </div>
		</div>
	
		<div class="text-center">


		</div>
	</header>
</div>