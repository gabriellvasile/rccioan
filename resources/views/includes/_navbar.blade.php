<nav class="navbar navbar-default topnavbar">
  <div class="container-fluid">

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="loginMenu">

        @if ( isset($logged) )
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
              {{ $logged->fname . ' ' . $logged->lname }}
              <span class="caret"></span>
            </a>
            <ul class="dropdown-menu" role="menu">
              @if( $admin )
                <li>{!! link_to_route('admin.dashboard', trans('messages.go_to_dashboard'), ['locale' => App::getLocale()]) !!}</li>
                <li>{!! link_to_route('admin.user.create', trans('messages.create_user'), ['locale' => App::getLocale()]) !!}</li>
                <li>{!! link_to_route('admin.employer.index', trans('messages.list_employers'), ['locale' => App::getLocale()]) !!}</li>
                <li>{!! link_to_route('admin.user.needapprove', trans('messages.approve_users'), ['locale' => App::getLocale()]) !!}</li>
                <li>{!! link_to_route('admin.ambassador.create', trans('messages.create_ambassador'), ['locale' => App::getLocale()]) !!}</li>
                <li>{!! link_to_route('admin.ambassador.index', trans('messages.list_ambassadors'), ['locale' => App::getLocale()]) !!}</li>
                <li>{!! link_to_route('admin.lunar.create', trans('messages.create_lunar'), ['locale' => App::getLocale()]) !!}</li>
                <li>{!! link_to_route('admin.lunar.index', trans('messages.list_lunars'), ['locale' => App::getLocale()]) !!}</li>
              @else
                <li>{!! link_to_route('profile.show', trans('messages.profile'), [$logged->username, App::getLocale()]) !!}</li>
                <li>{!! link_to_route('profile.edit', trans('messages.edit_profile'), [$logged->username, App::getLocale()]) !!}</li>
                <li>{!! link_to_route('profile.initiateDelete', trans('messages.delete_profile'), [$logged->id, App::getLocale()]) !!}</li>
              @endif
              <li>{!! link_to_action('Auth\AuthController@getLogout', trans('messages.signout'), ['locale' => App::getLocale()]) !!}</li>
            </ul>
          </li>
        @else
          <li>{!! link_to_action('Auth\AuthController@getLogin', trans('messages.login'), App::getLocale(), ['class' => 'login']) !!}</li>
          <li>{!! link_to_action('Auth\AuthController@getRegister', trans('messages.register'), App::getLocale(), ['class' => 'register']) !!}</li>
        @endif

      </ul>
      <ul class="sitemenu">
        @foreach ($topmenu as $menu)
          <li><a href="/{{ $menu->slug }}">{{ $menu->title }}</a></li>
        @endforeach
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>


