@extends('layouts.page')

@section('content')

	<div class="text-center"><h1>{{ $page->title }}</h1></div>
	<hr/>

	{{--@if($page->image)--}}
		{{--<img class="pull-left" src="{{ $page->image }}" alt="{{ $page->title }}" id="{{ $page->id }}">--}}
	{{--@endif--}}


	{!! $page->content !!}

@stop