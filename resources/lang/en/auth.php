<?php

return [

    "account_not_activated"         => "Your acccount has not been yet activated. Please check your email or",
    "account_resend"                => "resend activation link",
    "account_created"               => "Your account has been created!",
    "account_sent_activation_email" => "We have sent you an email to activate your account.",
    "account_activated_login"       => "Account activated! You can login!",
    "login"                         => "You must pe logged in order to see this page!",
    "logout_successful"             => "You have successfully logged out!",
    "login_successful"              => "You have successfully logged in!",

];
