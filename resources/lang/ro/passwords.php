<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Password Reminder Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are the default lines which match reasons
	| that are given by the password broker for a password update attempt
	| has failed, such as for an invalid token or invalid new password.
	|
	*/

	"password" => "Parolele trebuie să aibă cel puțin șase caractere și să fie identice cu confirmarea.",
	"user" => "Nu găsim niciun user cu această adresă de e-mail.",
	"token" => "Acest token de resetare a parolei nu este valid.",
	"sent" => "Ți-am trimis pe e-mail link-ul de resetare a parolei!",
	"reset" => "Parola ta a fost resetată!",

];
