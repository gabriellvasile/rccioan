<?php

return [

    "account_not_activated"         => "Contul tău nu a fost activat încă. Te rugăm să îți verifici adresa de e-mail sau",
    "account_resend"                => "trimite din nou link-ul de activare",
    "account_created"               => "Contul tău a fost creat!",
    "account_sent_activation_email" => "Ți-am trimis un e-mail pentru activarea contului.",
    "account_activated_login"       => "Contul a fost activat! Te poți loga.",
    "login"                         => "Trebuie să fii logat pentru a vedea această pagină!",
    "failed"                        => "User sau parola incorecta",
    "logout_successful"             => "Te-ai delogat cu succes!",
    "login_successful"              => "Te-ai logat cu succes!",

];
