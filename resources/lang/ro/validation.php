<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines contain the default error messages used by
	| the validator class. Some of these rules have multiple versions such
	| as the size rules. Feel free to tweak each of these messages here.
	|
	*/

	"accepted"             => "Atributul :attribute trebuie să fie acceptat",
	"active_url"           => "Atributul :attribute nu este un URL valid.",
	"after"                => "Atributul :attribute trebuie să fie o dată după :date.",
	"alpha"                => "Atributul :attribute poate conține numai litere.",
	"alpha_dash"           => "Atributul :attribute poate conține numai litere, numere și liniuțe.",
	"alpha_num"            => "Atributul :attribute poate conține numai litere și numere.",
	"array"                => "Atributul :attribute trebuie să fie un șir.",
	"before"               => "Atributul :attribute trebuie să fie o dată înainte de :date.",
	"between"              => [
		"numeric" => "Atributul :attribute trebuie să fie între :min și :max.",
		"file"    => "Atributul :attribute trebuie să aibă între :min și :max kilobytes.",
		"string"  => "Atributul :attribute trebuie să aibă între :min și :max caractere.",
		"array"   => "Atributul :attribute trebuie să aibă între :min și :max elemente.",
	],
	"boolean"              => "Câmpul :attribute trebuie să fie adevărat sau fals.",
	"confirmed"            => "Confirmarea :attribute nu se potrivește.",
	"date"                 => "Atributul :attribute nu este o dată validă.",
	"date_format"          => "Atributul :attribute nu se potrivește cu formatul :format.",
	"different"            => "Atributul :attribute și :other trebuie să fie diferiți.",
	"digits"               => "Atributul :attribute trebuie să fie de :digits cifre.",
	"digits_between"       => "Atributul :attribute trebuie să aibă între :min și :max cifre.",
	"email"                => "Atributul :attribute trebuie să fie o adresă de e-mail validă.",
	"filled"               => "Câmpul :attribute este obligatoriu.",
	"exists"               => "Atributul selectat :attribute nu este valid.",
	"image"                => "Atributul :attribute trebuie să fie o imagine.",
	"in"                   => "Atributul selectat :attribute nu este valid.",
	"integer"              => "Atributul :attribute trebuie să fie un număr întreg.",
	"ip"                   => "Atributul :attribute trebuie să fie o adresă IP validă.",
	"max"                  => [
		"numeric" => "Atributul :attribute nu poate fi mai mare decât :max.",
		"file"    => "Atributul :attribute nu poate avea mai mult de :max kilobytes.",
		"string"  => "Atributul :attribute nu poate avea mai mult de :max caractere.",
		"array"   => "Atributul :attribute nu poate avea mai mult de :max elemente.",
	],
	"mimes"                => "Atributul :attribute trebuie să fie un fișier de tipul: :values.",
	"min"                  => [
		"numeric" => "Atributul :attribute trebuie să fie de cel puțin :min.",
		"file"    => "Atributul :attribute trebuie să aibă cel puțin :min kilobytes.",
		"string"  => "Atributul :attribute trebuie să aibă cel puțin :min caractere.",
		"array"   => "Atributul :attribute trebuie să aibă cel puțin :min elemente.",
	],
	"not_in"               => "Atributul selectat :attribute nu este valid.",
	"numeric"              => "Atributul :attribute trebuie să fie un număr.",
	"regex"                => "Formatul :attribute nu este valid.",
	"required"             => "Câmpul :attribute este obligatoriu.",
	"required_if"          => "Câmpul :attribute e obligatoriu atunci când :other este :value.",
	"required_with"        => "Câmpul :attribute e obligatoriu atunci când există :values.",
	"required_with_all"    => "Câmpul :attribute e obligatoriu atunci când există :values.",
	"required_without"     => "Câmpul :attribute e obligatoriu atunci când nu există :values.",
	"required_without_all" => "Câmpul :attribute e obligatoriu atunci când nu există :values.",
	"same"                 => "Câmpul :attribute and :other must match.",
	"size"                 => [
		"numeric" => "Atributul :attribute trebuie să fie :size.",
		"file"    => "Atributul :attribute trebuie să fie de :size kilobytes.",
		"string"  => "Atributul :attribute trebuie să fie de :size caractere.",
		"array"   => "Atributul :attribute trebuie să conțină :size elemente.",
	],
	"unique"               => "Acest :attribute a fost deja folosit.",
	"url"                  => "Formatul :attribute nu este valid.",
	"timezone"             => "Atributul :attribute trebuie să fie o zonă validă.",

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| Here you may specify custom validation messages for attributes using the
	| convention "attribute.rule" to name the lines. This makes it quick to
	| specify a specific custom language line for a given attribute rule.
	|
	*/

	'custom' => [
		'attribute-name' => [
			'rule-name' => 'custom-message',
		],
	    'profile_id' => [
	        'in' => 'Trebuie să îți alegi tipul de profil.',
	    ],
	    'g-recaptcha-response' => [
	        'required' => 'Trebuie bifezi câmpul Captcha.',
	        'captcha'  => 'Te rugăm să revalidezi Captcha.'
	    ],
	    'recaptcha' => [
	        'required' => 'Trebuie să faci click pe câmpul Captcha.',
	        'captcha'  => 'Te rugăm să revalidezi Captcha.'
	    ],
	    'mobile' => [
	    	'mobile' => 'Numărul de mobil nu este valid.'
	    ],
	    'fname' => [
	    	'required' => 'Numele mic este obligatoriu.'
	    ],
	    'lname' => [
	    	'required' => 'Numele de familie este obligatoriu.'
	    ],
	    'country_id' => [
	    	'required' => 'Te rugăm să alegi o țară.',
	    	'exists' => 'Te rugăm să alegi o țară.'
	    ],
	    'profile_language' => [
	    	'required' => 'Te rugăm să alegi limba profilului.',
	    	'exists' => 'Te rugăm să alegi limba profilului.'
	    ],
	    'hair_colour_id' => [
	    	'required' => 'Te rugăm să alegi culoarea părului.',
	    	'exists' => 'Te rugăm să alegi culoarea părului.'
	    ],
	    'eye_colour_id' => [
	    	'required' => 'Te rugăm să alegi culoarea ochilor.',
	    	'exists' => 'Te rugăm să alegi culoarea ochilor.'
	    ],
	    'ethnicity_id' => [
	    	'required' => 'Te rugăm să alegi etnia.',
	    	'exists' => 'Te rugăm să alegi etnia.'
	    ],
	    'hair_length' => [
	    	'required' => 'Te rugăm să alegi lungimea părului.',
	    	'in' => 'Te rugăm să alegi lungimea părului.'
	    ],
	    'voice' => [
	    	'required' => 'Te rugăm să alegi o voce.',
	    	'in' => 'Te rugăm să alegi o voce.'
	    ],
	    'profesional_studies' => [
	    	'required' => 'Te rugăm să alegi studiile.',
	    	'in' => 'Te rugăm să alegi studiile.'
	    ],
	    'agency' => [
	    	'required' => 'Te rugăm să alegi un răspuns.',
	    	'in' => 'Te rugăm să alegi un răspuns.'
	    ],
	    'faculty_year' => [
	    	'required' => 'Te rugăm să alegi anul de facultate.',
	    	'year' => 'Te rugăm să alegi anul de facultate.'
	    ],
	    'speciality_year' => [
	    	'required' => 'Te rugăm să alegi specializarea.',
	    	'year' => 'Te rugăm să alegi specializarea.'
	    ]
	],

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Attributes
	|--------------------------------------------------------------------------
	|
	| The following language lines are used to swap attribute place-holders
	| with something more reader friendly such as E-Mail Address instead
	| of "email". This simply helps us make messages a little cleaner.
	|
	*/

	'attributes' => [],

	'custom_required' => 'Campul :field este necesar pentru personajul nr :character',
	'custom_numeric' => 'Campul :field al personajului :character trebuie sa fie numeric'

];
