/**
 * CSRF validation 
 * @see http://laravel.com/docs/5.0/routing#csrf-protection
 */
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

var f = new RccForm;

/**
 * New Ajax Form
 * @param  event event
 * @param  $jquery element form
 * @param  #string nextTab
 */
function RccForm() {
	
	dis 			= this; //  Keep Current Class in a variable, because "this" is $.ajax will mean something else
	dis.talent 		= $('.profile').data('type');

	$("#usertab #images").fileinput({
		uploadAsync: false,
		uploadUrl: $('.files').data('route'),
		maxFileCount: 5,
		allowedFileTypes: ["image"]
	});
	$("#usertab #profile").fileinput({
		uploadAsync: false,
		uploadUrl: $('.files').data('route'),
		maxFileCount: 1,
		allowedFileTypes: ["image"],
		uploadExtraData: {
			profile: true
		}
	});
	$("#usertab #logo").fileinput({
		uploadAsync: false,
		uploadUrl: $('.files').data('route'),
		maxFileCount: 1,
		allowedFileTypes: ["image"],
		uploadExtraData: {
			logo: true
		}
	});

	/**
	 * Constructor
	 */
	_init = function() {
		_onSubmitForm();
		_onAddToProfile();
		_onDeleteFromProfile();
		_onAddVimeo();
	};

	/**
	 * On submit form
	 */
	_onSubmitForm = function (form, nextTab) {
		$('.profile, .addJob').on('click', '.profilesubmit', function() {
			var $this = $(this),
				form = $this.parents('form');

			dis.submit(form, $this.data('nexttab'));
		});
	};

	/**
	 * Show/Hide Study Section depending on it's select
	 * @param  change [jquery select element]
	 * @param  reaction [jquery element, a study section]
	 */
	_onStudyChange = function(change, reaction) {
		
		if ( 1 != change.val() ) {
			reaction.hide();
		}
		$('.profile').on('change', change, function() {
			if ( 1 != change.val()) {
				reaction.slideUp();
			}
			else {
				reaction.slideDown();
			}
		});
	};

	/**
	 * Show Form Errors
	 * @param  array messages
	 */
	_showFormErrors = function (form, messages) {
		if (messages) {
			html = '<div class="alert alert-danger">';
			html += '<ul>';
			$.each(messages, function(index, val) {
				$('#' + index)
					.after('<span class="error">' + val + '</span>' + 
						   '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>')
					.parent('.form-group')
					.addClass('has-error has-feedback');
				html += '<li>' + val + '</li>';
			});
					
			html +=	'</ul>';
			html +=	'</div>';
			form.prepend(html);
		}
	};

	/**
	 * Remove Form Errors
	 * @param jquery element this.form
	 */
	_removeFormErrors = function (form) {
		form.find('.form-group').removeClass('has-error has-feedback');
		form.find('.error, .glyphicon, .alert').remove();
	};

	/**
	 * Ajax Success
	 * @param  array data Data from ProfileController
	 */
	_showNextTab = function (form, data, nextTab) {
		_hideLoading(true);

		if (typeof data['id'] != 'undefined' && data['id']) {
			$('#id').val(data['id']);
		}

		if ( nextTab == 'none' ) {
			$.ajax({
				url: form.data('show'),
				method: 'post',
				dataType: 'json',
				data: data,

				beforeSend: function() {
					_showLoading();
				},
				success: function (data) {
					$('#thankYou').modal('show');
					_removeFormErrors(form);
					_hideLoading();
				}
			});
		}
		else {

			// Append tab only if it doesn't exist already
			if (! $('.profile, .addJob').find('#' + nextTab).length ) {

				$.ajax({
					url: form.data('show'),
					method: 'get',
					dataType: 'html',
					data: data,

					success: function (data) {

						$('.tab-content').append(data);

						// Show appended tab
						$('a[href="#' + nextTab + '"]').tab('show');

						// Initialize education tabs
						if ( $('#educationtab').length ) {
							_onStudyChange( $('#profesional_studies'), $('.optionalrow') );
							_onStudyChange( $('#faculty'), $('.facultyOptions') );
							_onStudyChange( $('#speciality_studies'), $('.specialityOptions') );
							$('.profile').on('click', '#faculty_student', function() {
								$("#toHideFacultyYear").slideToggle();
							});
						};

						// Initialize multiselects
						$('select[multiple=""]').multiselect();

						// Upload files
						if ( nextTab == 'filestab') {
							$("#images").fileinput({
								uploadAsync: false,
								uploadUrl: $('#filesform').attr('action'),
								maxFileCount: 5,
								allowedFileTypes: ["image"]
							});
							$("#profile").fileinput({
								uploadAsync: false,
								uploadUrl: $('#filesform').attr('action'),
								maxFileCount: 1,
								allowedFileTypes: ["image"],
								uploadExtraData: {
									profile: true
								}
							});
							$("#logo").fileinput({
								uploadAsync: false,
								uploadUrl: $('#filesform').attr('action'),
								maxFileCount: 1,
								allowedFileTypes: ["image"],
								uploadExtraData: {
									logo: true
								}
							});
							$("#sounds").fileinput({
								uploadAsync: false,
								uploadUrl: $('#filesform').attr('action'),
								maxFileCount: 5,
								allowedFileExtensions: ["mp3", "wav"]
							});
						};

						// Agency Details
						$('.profile').on('change', '#agency', function(event) {
							if ( $(this).val() == 1 ) {
								$('.agencyrow').slideDown();
							}
							else {
								$('.agencyrow').slideUp();
							}
						});

						_hideLoading(true);
					}
				});
			}
			else {
				// Show appended tab
				$('a[href="#' + nextTab + '"]').tab('show');
			}
		}
	};

	/**
	 * Show loading
	 */
	_showLoading = function() {
		if( typeof inProgress != 'undefined' && inProgress ) return false;
		$('.la-anim-10').addClass('la-animate');
	};

	/**
	 * Hide Loading
	 * @param  boolean top [Go to top after?]
	 */
	_hideLoading = function(top) {
		$('.la-anim-10').removeClass('la-animate');

		if (typeof top !== "undefined" && top == true) {
			$("html, body").animate({ scrollTop: $("#banda").offset().top }, "slow");
		};
	};

	/**
	 * Add a new project / workshop
	 */
	_onAddToProfile = function() {
		key = 999999;

		$('.profile').on('click', '.addToProfile', function() {
			dis.addToProfile($(this), $(this).parents('.editable'));
		});

		$('.addJob').on('click', '.addToProfile', function() {
			key = $('.character').length;
			dis.addToProfile($(this), $('#jobActor').find('.editable'));
		});
	};

	_onAddVimeo = function() {
		$('.profile').on('click', '.addVideo', function(event) {
			event.preventDefault();
			var parent = $(this).parents('.editable');
			$.ajax({	
				url: parent.data('link'),
				method: 'get',
				dataType: 'html',
				data: {
					'link' 	 : parent.find('.videoinput').val(),
					'type'	 : parent.data('type')
				},

				beforeSend: function() {
					_showLoading();
				},
				success: function (data) {
					parent.find('.addVideosHere').append(data);
					_hideLoading();
				},
			});
		});
	}

	/**
	 * Delete a project / workshop
	 */
	_onDeleteFromProfile = function () {
		$('.profile, .addJob').on('click', '.deleteFromProfile', function() {
			var $this = $(this);
			if ($this.data('id')) {
				var edit = $this.parents('.editable');
				$.ajax({	
					url: edit.data('remove'), 
					method: 'delete',
					dataType: 'json',
					data: {
						'key' 	 : $this.data('id'),
						'type' 	 : edit.data('type'),
					},

					beforeSend: function() {
						_showLoading();
					},
					success: function (data) {
						$this.parent('.well, .vmvideo, .ytvideo, .scaudio').remove();
						$this.parents('.file').remove();
						_hideLoading();
					}
				});
			}
			else {
				$this.parent('.well').remove();
			}
		});
	};

	/**
	 * Submit a form
	 * @param jquery element $form
	 */
	this.submit = function (form, nextTab) {
		inProgress = false;
		$.ajax({	
			url: form.attr('action'), 
			method: 'post',
			dataType: 'json',
			data: form.serialize(),

			beforeSend: function() {
				_showLoading();
				_removeFormErrors(form);
			},
			error: function(data) {
				_showFormErrors(form, data['responseJSON']);
				_hideLoading(true);
			},
			success: function (data) {
				_removeFormErrors(form);
				_showNextTab(form, data, nextTab);
			}
		});
	};

	/**
	 * Add To Profile
	 * @param {jQuery object} clicked [clicked element]
	 * @param {dom element} parent  [parent element, where to append]
	 */
	this.addToProfile = function(clicked, parent) {
		$.ajax({	
			url: clicked.data('url'), 
			method: 'get',
			dataType: 'html',
			data: {
				'key' 	 : ++key,
				'talent' : dis.talent
			},

			beforeSend: function() {
				_showLoading();
			},
			success: function (data) {
				$(parent).append(data);
			},
			complete: function() {
				_hideLoading();
			}
		});
	};

	_init();
};