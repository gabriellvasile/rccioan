/**
 * CSRF validation 
 * @see http://laravel.com/docs/5.0/routing#csrf-protection
 */
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

var job = new Job;

/**
 * New Ajax Form
 * @param  event event
 * @param  $jquery element form
 * @param  #string nextTab
 */
function Job() {

	/**
	 * Constructor
	 */
	_init = function() {
		_smallInit();
	};

	_smallInit = function() {
		$( ".date-input" ).datepicker({
			inline: true,
			dateFormat: "yy-mm-dd",
			changeYear: true,
			changeMonth: true,
			yearRange: "2015:2016"
		});

		$('#jobTabsControllers a').on('click', function(e) {
			e.preventDefault();
		});

		$('#student_yes').on('click', function() {
			$('#studentInfo').slideDown();
		});

		$('#student_no').on('click', function() {
			$('#studentInfo').slideUp();
		});
	}

	_init();
};