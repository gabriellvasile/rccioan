/*!
 * FileInput <ro> Translations
 *
 * This file must be loaded after 'fileinput.js'. Patterns in braces '{}', or
 * any HTML markup tags in the messages must not be converted or translated.
 *
 * @see http://github.com/kartik-v/bootstrap-fileinput
 *
 * NOTE: this file must be saved in UTF-8 encoding.
 */
(function ($) {
    "use strict";

    $.fn.fileinput.locales.ro = {
        fileSingle: 'fisier',
        filePlural: 'fisiere',
        browseLabel: 'Cauta &hellip;',
        removeLabel: 'Sterge',
        removeTitle: 'Elimina fisierele selectate',
        cancelLabel: 'Anuleaza',
        cancelTitle: 'Anuleaza incarcare',
        uploadLabel: 'Incarca',
        uploadTitle: 'Incarca fisierele selectate',
        msgSizeTooLarge: 'Fisierul "{name}" (<b>{size} KB</b>) depaseste dimensiunea maxima de incarcare de <b>{maxSize} KB</b>. Te rugam sa incerci din nou!',
        msgFilesTooLess: 'Trebuie sa selectezi cel putin <b>{n}</b> {files} pentru incarcare. Te rugam sa incerci din nou!',
        msgFilesTooMany: 'Numarul fisierelor selectate pentru incarcare <b>({n})</b> depaseste numarul maxim admis de <b>{m}</b>. Te rugam sa incerci din nou!',
        msgFileNotFound: 'Fisierul "{name}" nu exista!',
        msgFileSecured: 'Restrictiile de securitate impiedica citirea fisierului "{name}".',
        msgFileNotReadable: 'Fisierul "{name}" nu poate fi citit.',
        msgFilePreviewAborted: 'Previzualizarea fisierului "{name}" a fost anulata.',
        msgFilePreviewError: 'A intervenit o eroare in timpul citirii fisierului "{name}".',
        msgInvalidFileType: 'Tip fisier invalid pentru fisierul "{name}". Numai fisierele de tip "{types}" sunt acceptate.',
        msgInvalidFileExtension: 'Extensie invalida pentru fisierul "{name}". Numai extensiile "{extensions}" sunt acceptate.',
        msgValidationError: 'Eroare la incarcare',
        msgLoading: 'Incarcare fisier {index} din {files} &hellip;',
        msgProgress: 'Incarcare fisier {index} din {files} - {name} - {percent}% incarcat.',
        msgSelected: '{n} fisiere selectate',
        msgFoldersNotAllowed: 'Trage NUMAI fisiere! Am evitat incarcarea a {n} dosare(s).',
        dropZoneTitle: 'Trage fisiere aici &hellip;'
    };

    $.extend($.fn.fileinput.defaults, $.fn.fileinput.locales.ro);
})(window.jQuery);