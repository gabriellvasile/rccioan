var f = new ContactForm;

/**
 * New Ajax Form
 * @param  event event
 * @param  $jquery element form
 * @param  #string nextTab
 */
function ContactForm() {

	dis 			= this; //  Keep Current Class in a variable, because "this" is $.ajax will mean something else

	/**
	 * Constructor
	 */
	_init = function() {
		_onSubmitForm();
	};

	_onSubmitForm = function() {
		$('#contactForm').on('submit', function(event) {
			event.preventDefault();

			var form = $(this);
			_removeFormErrors(form);

			$.ajax({	
				url: form.attr('action'), 
				method: 'post',
				dataType: 'json',
				data: form.serialize(),

				beforeSend: function() {
					_showLoading();
				},
				error: function(data) {
					_showFormErrors(form, data['responseJSON']);
					_hideLoading();
				},
				success: function (data) {
					_hideLoading();
					$('.alert-success').fadeIn();
					setTimeout(function(){
						location.href = form.data('redirect');
					}, 1500);
				}
			});
		});
	}

	/**
	 * Show loading
	 */
	_showLoading = function() {
		$('.la-anim-10').addClass('la-animate');
	};

	/**
	 * Hide Loading
	 * @param  boolean top [Go to top after?]
	 */
	_hideLoading = function(top) {
		$('.la-anim-10').removeClass('la-animate');

		if (typeof top !== "undefined" && top == true) {
			$("html, body").animate({ scrollTop: 0 }, "slow");
		};
	};

	/**
	 * Remove Form Errors
	 * @param jquery element this.form
	 */
	_removeFormErrors = function (form) {
		form.find('.form-group').removeClass('has-error has-feedback')
			 .find('.error, .glyphicon').remove();
		form.find('.alert').remove();
	};

	/**
	 * Show Form Errors
	 * @param  array messages
	 */
	_showFormErrors = function (form, messages) {
		if (messages) {
			html = '<div class="alert alert-danger">';
			html += '<ul>';
			$.each(messages, function(index, val) {
				$('#' + index)
					.after('<span class="error">' + val + '</span>' + 
						   '<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>')
					.parent('.form-group')
					.addClass('has-error has-feedback');
				html += '<li>' + val + '</li>';
			});
					
			html +=	'</ul>';
			html +=	'</div>';
			form.prepend(html);
		}
	};

	_init();
};