$(document).ready(function() {

	var rcc = new RccApp;

	function RccApp() {
		
		_ = this; // Keep Current Class in a variable, because "this" in $.ajax will mean something else

		/**
		 * Constructor
		 * @return void
		 */
		_init = function() {
			$(".fancybox").fancybox();
			_modals();
			_datepickers();
			_toTop();
			_mmenu();
			_ambassadors();
			_lunars();
			_filters();
		}

		_ambassadors = function() {
			$('.ambasador').on('click', '.btn', function(e) {
				e.preventDefault();
				var $btn = $(this);

				if ($btn.hasClass('back')) {
					$btn.removeClass('back').parents('.ambasador').removeClass('selected');
					$btn.text($btn.data('more'));
				}
				else {
					$('.ambasador').removeClass('selected').find('.back').removeClass('back').text($btn.data('more'));
					$btn.addClass('back').text($btn.data('back')).parents('.ambasador').addClass('selected');
				}
				$('.video').html('<iframe width="702" height="315" src="' + $btn.data('video') + '" frameborder="0" allowfullscreen></iframe>');
			});

			$(window).scroll(function(){
				var scrollTop = $(window).scrollTop();

				$('.ambasadorVideo').each(function() {
					var $ambassador = $(this);
					if ( scrollTop > $ambassador.offset().top - 600 ) {
						var $video = $ambassador.find('.video'),
							src = $video.data('src');
						if (typeof src != 'undefined' && !$video.html()) {
							$video.html(createVideoIframe(src, 352, 250));
						}
					}
				});

			});
		}

		_lunars = function() {
			_executeLunar(0, $('.lunarOne'));
			$(window).scroll(function(){
				var scrollTop = $(window).scrollTop();
				_executeLunar(scrollTop, $('.lunar'));
			});
		}

		_executeLunar = function(scrollTop, $elem) {
			$elem.each(function() {
				var $lunar = $(this);
				if ( scrollTop > $lunar.offset().top - 600 ) {
					var $video = $lunar.find('.video'),
						src = $video.data('src');
					if (typeof src != 'undefined' && !$video.html()) {
						$video.html(createVideoIframe(src, 590, 315));
					}
				}
			});
		}

		createVideoIframe = function(video, width, height) {
			if (issetAndNotEmpty(video)) {
				var youtube = 'https://www.youtube.com/watch?v=',
					yt_id = video.substring(video.indexOf(youtube) + youtube.length),
					vimeo = 'https://vimeo.com/',
					vm_id = video.substring(video.indexOf(vimeo) + vimeo.length);

				if (video.indexOf(vimeo) != -1) {
					return '<iframe src="https://player.vimeo.com/video/' + vm_id + '" width="' + width + '" height="' + height + '" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
				}
				else if (video.indexOf(youtube) != -1) {
					return '<iframe width="' + width + '" height="' + height + '" src="https://www.youtube.com/embed/' + yt_id + '?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>';
				}
			}

			return false;
		},

		issetAndNotEmpty = function(v) {
			return typeof(v) != "undefined" && $.trim(v);
		},

		/**
		 * Initialize datepickers
		 * @return void
		 */
		_datepickers = function() {
			$( "#birth_date" ).datepicker({
				inline: true,
				dateFormat: "yy-mm-dd",
				changeYear: true,
				changeMonth: true,
				yearRange: "1950:2000"
			});
		}

		_toTop = function() {
			$('#toTop').click(function () {
		        $("html, body").animate({
		            scrollTop: 0
		        }, 600);
		        return false;
		    });
		}

		_mmenu = function() {
			$(function() {
				$('nav#menu').mmenu();
				$('#menuButtons').appendTo('nav#menu');
				$('.brand').clone().appendTo('nav#menu');
			});
		}

		_filters = function() {
			$('#filterForm').hide();
			$('.clickFilter').on('click', function() {
				$('#filterForm').slideToggle();
				$(this).toggleClass('selected');
			});
		}

		_modals = function() {
			if (typeof $('#sessionModal') != 'undefined' && $('#sessionModal')) {
				$('#sessionModal').modal('show')
			}
		}

		_init();
	}
});