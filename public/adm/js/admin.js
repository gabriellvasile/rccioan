(function($) {
	$(document).ready(function($) {

		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});

		$( "#birth_date" ).datepicker({
			inline: true,
			dateFormat: "yy-mm-dd",
			changeYear: true,
			changeMonth: true,
			yearRange: "1950:2000"
		});

		$('select[multiple=""]').multiselect();

		$("#ambassadorImage").fileinput({
			uploadAsync: false,
			uploadUrl: $('#ambassadorForm').attr('action'),
			maxFileCount: 1,
			allowedFileTypes: ["image"],
			uploadExtraData: {
				logo: true
			}
		});

		$("#example1").dataTable();

		tinymce.init({
			selector: ".tinymce"
		});

		$('.projects').on('click', '.addToProfile', function() {
			var key = 999999,
				clicked = $(this),
				parent = $(this).parents('.editable');

			$.ajax({
				url: clicked.data('url'),
				method: 'get',
				dataType: 'html',
				data: {
					'key' 	 : ++key,
				},

				beforeSend: function() {
					_showLoading();
				},
				success: function (data) {
					$(parent).append(data);
				},
				complete: function() {
					_hideLoading();
				}
			});
		});

		$('.projects, #media, .content').on('click', '.deleteFromProfile', function() {
			var $this = $(this);
			if ($this.data('id')) {
				var edit = $this.parents('.editable');
				$.ajax({
					url: edit.data('remove'),
					method: 'delete',
					dataType: 'json',
					data: {
						'key' 	 : $this.data('id'),
						'type' 	 : edit.data('type'),
					},

					beforeSend: function() {
						_showLoading();
					},
					success: function (data) {
						$this.parent('.well, .vmvideo, .ytvideo, .scaudio').remove();
						$this.parents('.file').remove();
						_hideLoading();
					}
				});
			};
		});

		$('.content').on('click', '.addVideo', function(event) {
			event.preventDefault();
			var parent = $(this).parents('.editable');
			$.ajax({
				url: parent.data('link'),
				method: 'get',
				dataType: 'html',
				data: {
					'link' 	 : parent.find('.videoinput').val(),
					'type'	 : parent.data('type')
				},

				beforeSend: function() {
					_showLoading();
				},
				success: function (data) {
					parent.find('.addVideosHere').append(data);
					_hideLoading();
				},
			});
		});

		$('.content').on('click', '.nav-tabs li a', function () {
			$('#mediaContent .tab-pane').hide();
			$('#mediaContent .nav-tabs li').removeClass('active');

			$('form .tab-pane').attr('style', '');
			$('.btn-success').show();
			$('#mediaContent .tab-content').hide();
		});

		$('.content').on('click', '#showMedia', function() {
			$('.tab-pane, .btn-success').hide();
			$('.nav-tabs li').removeClass('active');

			var $this = $(this);

			$.ajax({
				url: $this.data('show'),
				method: 'get',
				dataType: 'html',
				data: {},

				success: function (data) {

					$('#mediaContent .tab-content').show();
					$('#mediaContent .tab-content').append(data);

					// Show appended tab
					$('a[href="#filestab"]').tab('show');
					$('#filestab').show();

					// Upload files
					$("#images").fileinput({
						uploadAsync: false,
						uploadUrl: $('#filesform').attr('action'),
						maxFileCount: 5,
						allowedFileTypes: ["image"]
					});
					$("#profile").fileinput({
						uploadAsync: false,
						uploadUrl: $('#filesform').attr('action'),
						maxFileCount: 1,
						allowedFileTypes: ["image"],
						uploadExtraData: {
							profile: true
						}
					});
					$("#logo").fileinput({
						uploadAsync: false,
						uploadUrl: $('#filesform').attr('action'),
						maxFileCount: 1,
						allowedFileTypes: ["image"],
						uploadExtraData: {
							logo: true
						}
					});
					$("#sounds").fileinput({
						uploadAsync: false,
						uploadUrl: $('#filesform').attr('action'),
						maxFileCount: 5,
						allowedFileExtensions: ["mp3", "wav"]
					});

					_hideLoading(true);
				}
			});
		});

		/**
		 * Show loading
		 */
		_showLoading = function() {
			if( typeof inProgress != 'undefined' && inProgress ) return false;
			$('.la-anim-10').addClass('la-animate');
		};

		/**
		 * Hide Loading
		 * @param  boolean top [Go to top after?]
		 */
		_hideLoading = function(top) {
			$('.la-anim-10').removeClass('la-animate');
		};

	});
})(jQuery);