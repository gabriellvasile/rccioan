<?php
/**
 * An helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace Rcc\Models{
/**
 * Rcc\Models\Actor
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $ethnicity_id
 * @property integer $eye_colour_id
 * @property integer $hair_colour_id
 * @property boolean $height
 * @property integer $weight
 * @property string $accent
 * @property string $hair_length
 * @property string $spoken_languages
 * @property string $facial_traits
 * @property boolean $voice
 * @property string $dance
 * @property string $instruments
 * @property string $sports
 * @property string $nude
 * @property boolean $profesional_studies
 * @property boolean $speciality_studies
 * @property integer $speciality_year
 * @property string $speciality_institution
 * @property boolean $faculty
 * @property boolean $faculty_student
 * @property integer $faculty_year
 * @property string $faculty_institution
 * @property string $faculty_domain
 * @property string $website
 * @property string $info
 * @property string $production_types
 * @property boolean $role_abroad
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property integer $actor_type_id
 * @property-read \Rcc\Models\User $User
 * @property-read \Illuminate\Database\Eloquent\Collection|\Rcc\Models\Lunar[] $Lunars
 * @property-read \Rcc\Models\Ethnicity $Ethnicity
 * @property-read \Rcc\Models\EyeColour $EyeColour
 * @property-read \Rcc\Models\HairColour $HairColour
 * @property-read \Rcc\Models\ActorType $ActorType
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Actor whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Actor whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Actor whereEthnicityId($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Actor whereEyeColourId($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Actor whereHairColourId($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Actor whereHeight($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Actor whereWeight($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Actor whereAccent($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Actor whereHairLength($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Actor whereSpokenLanguages($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Actor whereFacialTraits($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Actor whereVoice($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Actor whereDance($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Actor whereInstruments($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Actor whereSports($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Actor whereNude($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Actor whereProfesionalStudies($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Actor whereSpecialityStudies($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Actor whereSpecialityYear($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Actor whereSpecialityInstitution($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Actor whereFaculty($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Actor whereFacultyStudent($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Actor whereFacultyYear($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Actor whereFacultyInstitution($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Actor whereFacultyDomain($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Actor whereWebsite($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Actor whereInfo($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Actor whereProductionTypes($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Actor whereRoleAbroad($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Actor whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Actor whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Actor whereActorTypeId($value)
 */
	class Actor {}
}

namespace Rcc\Models{
/**
 * Rcc\Models\ActorType
 *
 * @property integer $id
 * @property string $name
 * @property string $slug
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Rcc\Models\Actor[] $Actors
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\ActorType whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\ActorType whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\ActorType whereSlug($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\ActorType whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\ActorType whereUpdatedAt($value)
 */
	class ActorType {}
}

namespace Rcc\Models{
/**
 * Rcc\Models\Ambassador
 *
 * @property integer $id
 * @property string $name
 * @property string $video
 * @property string $quote
 * @property string $description1
 * @property string $description2
 * @property string $image
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Ambassador whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Ambassador whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Ambassador whereVideo($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Ambassador whereQuote($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Ambassador whereDescription1($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Ambassador whereDescription2($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Ambassador whereImage($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Ambassador whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Ambassador whereUpdatedAt($value)
 */
	class Ambassador {}
}

namespace Rcc\Models{
/**
 * Rcc\Models\Country
 *
 * @property integer $id
 * @property string $name
 * @property string $prefix
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Rcc\Models\User $User
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Country whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Country whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Country wherePrefix($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Country whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Country whereUpdatedAt($value)
 */
	class Country {}
}

namespace Rcc\Models{
/**
 * Rcc\Models\Employer
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $logo_id
 * @property string $company
 * @property string $address
 * @property string $post_code
 * @property string $website
 * @property string $info
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property integer $employer_type_id
 * @property-read \Rcc\Models\User $User
 * @property-read \Rcc\Models\EmployerType $EmployerType
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Employer whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Employer whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Employer whereLogoId($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Employer whereCompany($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Employer whereAddress($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Employer wherePostCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Employer whereWebsite($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Employer whereInfo($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Employer whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Employer whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Employer whereEmployerTypeId($value)
 */
	class Employer {}
}

namespace Rcc\Models{
/**
 * Rcc\Models\EmployerType
 *
 * @property integer $id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Rcc\Models\Employer[] $Employers
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\EmployerType whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\EmployerType whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\EmployerType whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\EmployerType whereUpdatedAt($value)
 */
	class EmployerType {}
}

namespace Rcc\Models{
/**
 * Rcc\Models\Ethnicity
 *
 * @property integer $id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Ethnicity whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Ethnicity whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Ethnicity whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Ethnicity whereUpdatedAt($value)
 */
	class Ethnicity {}
}

namespace Rcc\Models{
/**
 * Rcc\Models\EyeColour
 *
 * @property integer $id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\EyeColour whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\EyeColour whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\EyeColour whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\EyeColour whereUpdatedAt($value)
 */
	class EyeColour {}
}

namespace Rcc\Models{
/**
 * Rcc\Models\HairColour
 *
 * @property integer $id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\HairColour whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\HairColour whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\HairColour whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\HairColour whereUpdatedAt($value)
 */
	class HairColour {}
}

namespace Rcc\Models{
/**
 * Rcc\Models\Image
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $path
 * @property string $thumb
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Rcc\Models\User $User
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Image whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Image whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Image wherePath($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Image whereThumb($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Image whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Image whereUpdatedAt($value)
 */
	class Image {}
}

namespace Rcc\Models{
/**
 * Rcc\Models\Job
 *
 * @property integer $id
 * @property string $production_name
 * @property integer $production_type_id
 * @property string $location
 * @property string $experience
 * @property string $from_date_shooting
 * @property string $to_date_shooting
 * @property \Carbon\Carbon $from_date_casting
 * @property \Carbon\Carbon $to_date_casting
 * @property integer $employer_id
 * @property string $short_description
 * @property boolean $student_project
 * @property integer $from_age
 * @property integer $to_age
 * @property integer $hair_colour_id
 * @property integer $from_height
 * @property integer $to_height
 * @property string $body_shape
 * @property string $extra_details
 * @property boolean $payment
 * @property string $telephone
 * @property boolean $notifying_method
 * @property boolean $exclusive
 * @property string $university
 * @property string $tutor_name
 * @property string $department
 * @property string $year
 * @property string $date_added
 * @property integer $user_id
 * @property boolean $status
 * @property string $sex
 * @property string $email
 * @property boolean $subcategory
 * @property boolean $guest_visible
 * @property boolean $needs_nude
 * @property boolean $complete
 * @property integer $eye_colour_id
 * @property boolean $approved
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Rcc\Models\User[] $Users
 * @property-read \Rcc\Models\ProductionType $ProductionType
 * @property-read \Rcc\Models\Employer $Employer
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Job whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Job whereProductionName($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Job whereProductionTypeId($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Job whereLocation($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Job whereExperience($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Job whereFromDateShooting($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Job whereToDateShooting($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Job whereFromDateCasting($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Job whereToDateCasting($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Job whereEmployerId($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Job whereShortDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Job whereStudentProject($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Job whereFromAge($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Job whereToAge($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Job whereHairColourId($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Job whereFromHeight($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Job whereToHeight($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Job whereBodyShape($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Job whereExtraDetails($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Job wherePayment($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Job whereTelephone($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Job whereNotifyingMethod($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Job whereExclusive($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Job whereUniversity($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Job whereTutorName($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Job whereDepartment($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Job whereYear($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Job whereDateAdded($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Job whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Job whereStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Job whereSex($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Job whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Job whereSubcategory($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Job whereGuestVisible($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Job whereKidsProfileType($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Job whereFilmProfileType($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Job whereNeedsNude($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Job whereComplete($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Job whereEyeColourId($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Job whereApproved($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Job whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Job whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Job active()
 */
	class Job {}
}

namespace Rcc\Models{
/**
 * Rcc\Models\Language
 *
 * @property integer $id
 * @property string $name
 * @property string $code
 * @property string $locale
 * @property string $image
 * @property boolean $order
 * @property boolean $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Language whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Language whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Language whereCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Language whereLocale($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Language whereImage($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Language whereOrder($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Language whereStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Language whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Language whereUpdatedAt($value)
 */
	class Language {}
}

namespace Rcc\Models{
/**
 * Rcc\Models\Licence
 *
 * @property integer $id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Rcc\Models\User[] $Users
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Licence whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Licence whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Licence whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Licence whereUpdatedAt($value)
 */
	class Licence {}
}

namespace Rcc\Models{
/**
 * Rcc\Models\Lunar
 *
 * @property integer $id
 * @property integer $actor_id
 * @property string $video
 * @property string $description
 * @property \Carbon\Carbon $date
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Rcc\Models\Actor $Actor
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Lunar whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Lunar whereActorId($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Lunar whereVideo($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Lunar whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Lunar whereDate($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Lunar whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Lunar whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Lunar desc()
 */
	class Lunar {}
}

namespace Rcc\Models{
/**
 * Rcc\Models\Message
 *
 * @property integer $id
 * @property string $message
 * @property boolean $is_read
 * @property boolean $is_important
 * @property integer $sender
 * @property integer $recepient
 * @property string $subject
 * @property boolean $status
 * @property boolean $sent
 * @property \Carbon\Carbon $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Rcc\Models\User $Sender
 * @property-read \Rcc\Models\User $Recepient
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Message whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Message whereMessage($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Message whereIsRead($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Message whereIsImportant($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Message whereSender($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Message whereRecepient($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Message whereSubject($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Message whereStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Message whereSent($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Message whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Message whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Message whereUpdatedAt($value)
 */
	class Message {}
}

namespace Rcc\Models{
/**
 * Rcc\Models\Production
 *
 * @property integer $id
 * @property string $title
 * @property string $titlu
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Rcc\Models\User[] $Users
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Production whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Production whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Production whereTitlu($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Production whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Production whereUpdatedAt($value)
 */
	class Production {}
}

namespace Rcc\Models{
/**
 * Rcc\Models\ProductionType
 *
 * @property integer $id
 * @property string $name
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\ProductionType whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\ProductionType whereName($value)
 */
	class ProductionType {}
}

namespace Rcc\Models{
/**
 * Rcc\Models\Project
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $name
 * @property integer $duration
 * @property string $description
 * @property string $produced_under
 * @property string $production_type
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Rcc\Models\User $User
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Project whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Project whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Project whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Project whereDuration($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Project whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Project whereProducedUnder($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Project whereProductionType($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Project whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Project whereUpdatedAt($value)
 */
	class Project {}
}

namespace Rcc\Models{
/**
 * Rcc\Models\Role
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Rcc\Models\Role[] $User
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Role whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Role whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Role whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Role whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Role whereUpdatedAt($value)
 */
	class Role {}
}

namespace Rcc\Models{
/**
 * Rcc\Models\Soundcloud
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $code
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Rcc\Models\User $User
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Soundcloud whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Soundcloud whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Soundcloud whereCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Soundcloud whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Soundcloud whereUpdatedAt($value)
 */
	class Soundcloud {}
}

namespace Rcc\Models{
/**
 * Rcc\Models\Track
 *
 * @property-read \Rcc\Models\User $User
 */
	class Track {}
}

namespace Rcc\Models{
/**
 * Rcc\Models\User
 *
 * @property integer $id
 * @property integer $country_id
 * @property integer $role_id
 * @property integer $language_id
 * @property boolean $active
 * @property boolean $approved
 * @property boolean $complete
 * @property string $username
 * @property string $email
 * @property string $password
 * @property string $fname
 * @property string $lname
 * @property string $city
 * @property string $mobile
 * @property string $sex
 * @property integer $logins
 * @property string $last_login
 * @property string $birth_date
 * @property string $code
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property integer $thumb_id
 * @property-read \Rcc\Models\Actor $Actor
 * @property-read \Rcc\Models\Employer $Employer
 * @property-read \Rcc\Models\EmployerType $EmployerType
 * @property-read \Rcc\Models\ActorType $ActorType
 * @property-read \Rcc\Models\Role $Role
 * @property-read \Rcc\Models\Country $Country
 * @property-read \Rcc\Models\Ethnicity $Ethnicity
 * @property-read \Illuminate\Database\Eloquent\Collection|\Rcc\Models\Licence[] $Licences
 * @property-read \Illuminate\Database\Eloquent\Collection|\Rcc\Models\Production[] $Productions
 * @property-read \Illuminate\Database\Eloquent\Collection|\Rcc\Models\Job[] $Jobs
 * @property-read \Illuminate\Database\Eloquent\Collection|\Rcc\Models\Project[] $Projects
 * @property-read \Illuminate\Database\Eloquent\Collection|\Rcc\Models\Workshop[] $Workshops
 * @property-read \Illuminate\Database\Eloquent\Collection|\Rcc\Models\Image[] $Images
 * @property-read \Illuminate\Database\Eloquent\Collection|\Rcc\Models\Soundcloud[] $Soundclouds
 * @property-read \Illuminate\Database\Eloquent\Collection|\Rcc\Models\Track[] $Tracks
 * @property-read \Illuminate\Database\Eloquent\Collection|\Rcc\Models\Vimeo[] $Vimeos
 * @property-read \Illuminate\Database\Eloquent\Collection|\Rcc\Models\Youtube[] $Youtubes
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\User whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\User whereCountryId($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\User whereRoleId($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\User whereLanguageId($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\User whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\User whereApproved($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\User whereComplete($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\User whereUsername($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\User whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\User wherePassword($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\User whereFname($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\User whereLname($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\User whereCity($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\User whereMobile($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\User whereSex($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\User whereLogins($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\User whereLastLogin($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\User whereBirthDate($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\User whereCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\User whereRememberToken($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\User whereThumbId($value)
 */
	class User {}
}

namespace Rcc\Models{
/**
 * Rcc\Models\Vimeo
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $code
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Rcc\Models\User $User
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Vimeo whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Vimeo whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Vimeo whereCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Vimeo whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Vimeo whereUpdatedAt($value)
 */
	class Vimeo {}
}

namespace Rcc\Models{
/**
 * Rcc\Models\Workshop
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $name
 * @property string $award
 * @property integer $year
 * @property string $description
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Rcc\Models\User $User
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Workshop whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Workshop whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Workshop whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Workshop whereAward($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Workshop whereYear($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Workshop whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Workshop whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Workshop whereUpdatedAt($value)
 */
	class Workshop {}
}

namespace Rcc\Models{
/**
 * Rcc\Models\Youtube
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $code
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Rcc\Models\User $User
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Youtube whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Youtube whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Youtube whereCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Youtube whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Rcc\Models\Youtube whereUpdatedAt($value)
 */
	class Youtube {}
}

