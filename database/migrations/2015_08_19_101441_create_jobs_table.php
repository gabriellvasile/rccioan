<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('jobs', function (Blueprint $table) {
			$table->increments('id');

			$table->integer('production_type_id')->unsigned()->nullable();
			$table->foreign('production_type_id')->references('id')->on('production_types')->onDelete('cascade');

			$table->integer('actor_type_id')->unsigned()->nullable();
			$table->foreign('actor_type_id')->references('id')->on('actor_types')->onDelete('cascade');

			$table->integer('user_id')->unsigned()->nullable();
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

			$table->integer('employer_id')->unsigned()->nullable();
			$table->foreign('employer_id')->references('id')->on('employers')->onDelete('cascade');

			$table->text('production_name')->nullable();
			$table->text('location')->nullable();

			$table->date('from_date_shooting')->nullable();
			$table->date('to_date_shooting')->nullable();

			$table->date('from_date_casting')->nullable();
			$table->date('to_date_casting')->nullable();

			$table->text('short_description')->nullable();
			$table->boolean('student_project')->nullable();

			$table->boolean('payment')->nullable();
			$table->text('telephone')->nullable();
			$table->boolean('notifying_method')->nullable();
			$table->boolean('exclusive')->nullable();
			$table->text('university')->nullable();
			$table->text('tutor_name')->nullable();
			$table->text('department')->nullable();
			$table->text('year')->nullable();
			$table->datetime('date_added')->nullable();
			$table->text('email')->nullable();

			$table->text('updates')->nullable();

			$table->boolean('guest_visible')->nullable();
			$table->boolean('complete')->nullable()->default(0);
			$table->boolean('status')->nullable()->default(0);
			$table->boolean('approved')->default(0);

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {

		Schema::table('jobs', function(Blueprint $table) {
			$table->dropForeign('jobs_user_id_foreign');
			$table->dropForeign('jobs_employer_id_foreign');
			$table->dropForeign('jobs_production_type_id_foreign');
			$table->dropForeign('jobs_actor_type_id_foreign');
		});

		Schema::drop('jobs');
	}
}
