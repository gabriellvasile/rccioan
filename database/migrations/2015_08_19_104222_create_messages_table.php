<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('messages', function (Blueprint $table) {
			$table->increments('id');
			$table->text('message');
			$table->boolean('is_read');
			$table->boolean('is_important');

			$table->integer('sender_id')->unsigned()->index()->nullable();
			$table->foreign('sender_id')->references('id')->on('users')->onDelete('cascade');

			$table->integer('recipient_id')->unsigned()->index()->nullable();
			$table->foreign('recipient_id')->references('id')->on('users')->onDelete('cascade');

			$table->string('subject');
			$table->boolean('status');
			$table->boolean('sent');

			$table->softDeletes();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table('messages', function(Blueprint $table) {
			$table->dropForeign('messages_sender_id_foreign');
			$table->dropForeign('messages_recipient_id_foreign');
		});
		Schema::drop('messages');
	}
}
