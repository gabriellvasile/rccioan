<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('pages', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('parent_id')->unsigned();
			$table->string('password', 20);
			$table->boolean('status');
			$table->boolean('header')->default(0);
			$table->boolean('footer')->default(0);
			$table->integer('order')->unsigned();
			$table->string('image', 255);
			$table->timestamp('published_at');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::drop('pages');
	}
}
