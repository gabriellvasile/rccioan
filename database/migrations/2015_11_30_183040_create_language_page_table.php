<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLanguagePageTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('language_page', function (Blueprint $table) {
			// Language Foreign Key
			$table->integer('language_id')->unsigned()->index();
			$table->foreign('language_id')->references('id')->on('languages')->onDelete('cascade');

			// Page Foreign Key
			$table->integer('page_id')->unsigned()->index();
			$table->foreign('page_id')->references('id')->on('pages')->onDelete('cascade');

			$table->string('slug', 200)->unique();
			$table->string('title', 100);
			$table->longText('content');
			$table->text('excerpt')->nullable();

			$table->string('meta_title', 100);
			$table->text('meta_description');
			$table->text('meta_keywords');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table('language_page', function (Blueprint $table) {
			$table->dropForeign('language_page_language_id_foreign');
			$table->dropForeign('language_page_page_id_foreign');
		});
		Schema::drop('language_page');
	}
}
