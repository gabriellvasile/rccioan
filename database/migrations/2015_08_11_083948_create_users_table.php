<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function(Blueprint $table)
        {
            $table->increments('id');
            
            $table->integer('country_id')->unsigned()->index()->nullable();
            $table->foreign('country_id')->references('id')->on('countries')->onDelete('cascade');

            $table->integer('role_id')->unsigned();
            $table->foreign('role_id')->references('id')->on('roles')->onDelete('cascade');

            $table->integer('language_id')->unsigned();
            $table->foreign('language_id')->references('id')->on('languages')->onDelete('cascade');

            $table->boolean('active')->default(0);
            $table->boolean('approved')->default(0);
            $table->boolean('complete')->default(0);

            $table->string('username', 32);
            $table->boolean('username_changed')->default(0);
            $table->string('email')->unique();
            $table->string('password', 60);

            $table->string('fname', 60)->nullable();
            $table->string('lname', 60)->nullable();
            $table->string('city', 80)->nullable();

            $table->string('mobile', 30)->nullable();

            $table->enum('sex', ['M', 'F'])->nullable();

            $table->smallInteger('logins')->unsigned()->nullable();
            $table->dateTime('last_login')->nullable();
            $table->date('birth_date')->nullable();

            $table->string('code', 60)->nullable();
            $table->text('updates')->nullable();

            $table->rememberToken();
            $table->timestamps();

            $table->integer('thumb_id')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function(Blueprint $table) {
            $table->dropForeign('users_country_id_foreign');
            $table->dropForeign('users_role_id_foreign');
            $table->dropForeign('users_language_id_foreign');
        });
        Schema::drop('users');
    }
}
