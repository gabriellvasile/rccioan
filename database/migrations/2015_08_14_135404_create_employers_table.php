<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('employers')) {
            Schema::create('employers', function (Blueprint $table) {
                $table->increments('id');

                $table->integer('user_id')->unsigned()->index();
                $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

                $table->string('thumb');
                $table->string('logo');
                $table->string('company', 200)->nullable();
                $table->string('address', 200)->nullable();
                $table->string('post_code', 10)->nullable();
                $table->string('website', 45)->nullable();

                $table->string('info', 400)->nullable();

                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employers', function(Blueprint $table) {
            $table->dropForeign('employers_user_id_foreign');
        });
        Schema::drop('employers');
    }
}
