<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRangeUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('range_user', function (Blueprint $table) {
			$table->integer('range_id')->unsigned()->index();
			$table->foreign('range_id')->references('id')->on('ranges')->onDelete('cascade');
			$table->integer('user_id')->unsigned()->index();
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table('range_user', function(Blueprint $table) {
			$table->dropForeign('range_user_user_id_foreign');
			$table->dropForeign('range_user_range_id_foreign');
		});
		Schema::drop('range_user');
	}
}
