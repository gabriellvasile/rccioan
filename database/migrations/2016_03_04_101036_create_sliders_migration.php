<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSlidersMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sliders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('image', 255);
            $table->string('title1_ro', 255);
            $table->string('title2_ro', 255);
            $table->string('desc_ro', 255);
            $table->string('title1_en', 255);
            $table->string('title2_en', 255);
            $table->string('desc_en', 255);
            $table->tinyInteger('sort')->unsigned()->default(99);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sliders');
    }
}
