<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLicenceUserPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('licence_user', function(Blueprint $table) {
            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('licence_id')->unsigned()->index();
            $table->foreign('licence_id')->references('id')->on('licences')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('licence_user', function(Blueprint $table) {
            $table->dropForeign('licence_user_user_id_foreign');
            $table->dropForeign('licence_user_licence_id_foreign');
        });
        Schema::drop('licence_user');
    }
}
