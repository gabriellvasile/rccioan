<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCharacterUserPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('character_user', function(Blueprint $table) {
            $table->integer('character_id')->unsigned()->index();
            $table->foreign('character_id')->references('id')->on('characters')->onDelete('cascade');
            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->boolean('is_chosen')->nullable();
            $table->boolean('is_shortlist')->nullable();
            $table->boolean('is_seen')->nullable();
            $table->datetime('date_applied')->nullable();

            $table->boolean('is_accepted')->nullable();
            $table->boolean('is_invite')->nullable();
            $table->boolean('is_declined_employer')->nullable();

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('character_user', function(Blueprint $table) {
            $table->dropForeign('character_user_character_id_foreign');
            $table->dropForeign('character_user_user_id_foreign');
        });
        Schema::drop('character_user');
    }
}
