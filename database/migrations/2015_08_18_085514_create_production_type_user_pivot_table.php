<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductionTypeUserPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('production_type_user', function(Blueprint $table) {
            $table->integer('production_type_id')->unsigned()->index();
            $table->foreign('production_type_id')->references('id')->on('production_types')->onDelete('cascade');
            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('production_type_user', function(Blueprint $table) {
            $table->dropForeign('production_type_user_production_type_id_foreign');
            $table->dropForeign('production_type_user_user_id_foreign');
        });
        Schema::drop('production_type_user');
    }
}
