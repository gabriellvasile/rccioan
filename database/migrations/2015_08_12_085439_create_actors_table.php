<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('actors')) {
            Schema::create('actors', function (Blueprint $table) {
                $table->increments('id');

                $table->integer('user_id')->unsigned()->index();
                $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

                $table->integer('ethnicity_id')->unsigned()->index();
                $table->foreign('ethnicity_id')->references('id')->on('ethnicities')->onDelete('cascade');

                $table->integer('eye_colour_id')->unsigned()->index();
                $table->foreign('eye_colour_id')->references('id')->on('eye_colours')->onDelete('cascade');

                $table->integer('hair_colour_id')->unsigned()->index();
                $table->foreign('hair_colour_id')->references('id')->on('hair_colours')->onDelete('cascade');

                $table->tinyInteger('height')->unsigned()->nullable();
                $table->smallInteger('weight')->unsigned()->nullable();
                $table->string('accent', 80)->nullable();
                $table->enum('hair_length', ['S', 'M', 'L'])->nullable();
                $table->string('spoken_languages', 80)->nullable();
                $table->text('facial_traits')->nullable();
                $table->boolean('voice')->nullable();
                $table->string('dance', 128)->nullable();
                $table->string('instruments', 128)->nullable();
                $table->string('sports', 128)->nullable();
                $table->string('ranges', 128)->nullable();

                $table->boolean('profesional_studies')->nullable();
                $table->boolean('speciality_studies')->nullable();
                $table->smallInteger('speciality_year')->unsigned()->nullable();
                $table->string('speciality_institution', 128)->nullable();

                $table->boolean('faculty')->nullable();
                $table->boolean('faculty_student')->nullable();
                $table->smallInteger('faculty_year')->unsigned()->nullable();
                $table->string('faculty_institution', 128)->nullable();
                $table->string('faculty_domain', 80)->nullable();

                $table->string('website', 45)->nullable();
                $table->text('info')->nullable();
                $table->text('awards')->nullable();
                $table->text('workshops')->nullable();
                $table->text('production_types')->nullable();
                $table->boolean('role_abroad')->nullable();


                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('actors', function(Blueprint $table) {
            $table->dropForeign('actors_user_id_foreign');
            $table->dropForeign('actors_ethnicity_id_foreign');
            $table->dropForeign('actors_eye_colour_id_foreign');
            $table->dropForeign('actors_hair_colour_id_foreign');
        });
        Schema::drop('actors');
    }
}
