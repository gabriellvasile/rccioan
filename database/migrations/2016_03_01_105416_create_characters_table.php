<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCharactersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('characters', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('job_id')->unsigned()->nullable();
            $table->foreign('job_id')->references('id')->on('jobs')->onDelete('cascade');

            $table->integer('range_id')->unsigned()->nullable();
            $table->foreign('range_id')->references('id')->on('ranges')->onDelete('cascade');

            $table->integer('hair_colour_id')->unsigned()->nullable();
            $table->foreign('hair_colour_id')->references('id')->on('hair_colours')->onDelete('cascade');

            $table->integer('eye_colour_id')->unsigned()->nullable();
            $table->foreign('eye_colour_id')->references('id')->on('eye_colours')->onDelete('cascade');

            $table->text('title')->nullable();

            $table->enum('sex', ['M', 'F'])->nullable();

            $table->integer('from_height')->unsigned()->nullable();
            $table->integer('to_height')->unsigned()->nullable();

            $table->text('extra_details')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('characters', function (Blueprint $table) {
            $table->dropForeign('characters_range_id_foreign');
            $table->dropForeign('characters_eye_colour_id_foreign');
            $table->dropForeign('characters_job_id_foreign');
            $table->dropForeign('characters_hair_colour_id_foreign');
        });

        Schema::drop('characters');
    }
}
