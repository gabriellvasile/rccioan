<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateEmployerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE `employers` CHANGE `logo` `logo_id` INT(10) NULL DEFAULT NULL;');
        Schema::table('employers', function (Blueprint $table) {
            $table->dropColumn('thumb');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('ALTER TABLE `employers` CHANGE `logo_id` `logo` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;');
        Schema::table('employers', function (Blueprint $table) {
            $table->string('thumb');
        });
    }
}
