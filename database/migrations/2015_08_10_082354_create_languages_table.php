<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLanguagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('languages', function(Blueprint $table) {
            $table->increments('id');

            $table->string('name', 32)->unique();
            $table->string('code', 5)->unique();
            $table->string('locale', 255)->unique();

            $table->string('image', 64);
            $table->tinyInteger('order')->unsigned();
            $table->tinyInteger('status')->unsigned();

            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('languages');
    }
}
