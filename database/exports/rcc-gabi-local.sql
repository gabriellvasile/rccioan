/*
Navicat MySQL Data Transfer

Source Server         : Vagrant
Source Server Version : 50619
Source Host           : localhost:33060
Source Database       : rcc

Target Server Type    : MYSQL
Target Server Version : 50619
File Encoding         : 65001

Date: 2015-09-12 12:07:02
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for actor_types
-- ----------------------------
DROP TABLE IF EXISTS `actor_types`;
CREATE TABLE `actor_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of actor_types
-- ----------------------------

-- ----------------------------
-- Table structure for actors
-- ----------------------------
DROP TABLE IF EXISTS `actors`;
CREATE TABLE `actors` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `ethnicity_id` int(10) unsigned NOT NULL,
  `eyecolour_id` int(10) unsigned NOT NULL,
  `haircolour_id` int(10) unsigned NOT NULL,
  `height` tinyint(3) unsigned DEFAULT NULL,
  `weight` smallint(5) unsigned DEFAULT NULL,
  `accent` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hair_length` enum('S','M','L') COLLATE utf8_unicode_ci DEFAULT NULL,
  `spoken_languages` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facial_traits` text COLLATE utf8_unicode_ci,
  `voice` tinyint(1) DEFAULT NULL,
  `dance` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `instruments` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sports` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nude` enum('Y','N','O') COLLATE utf8_unicode_ci DEFAULT NULL,
  `profesional_studies` tinyint(1) DEFAULT NULL,
  `speciality_studies` tinyint(1) DEFAULT NULL,
  `speciality_year` smallint(5) unsigned DEFAULT NULL,
  `speciality_institution` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `faculty` tinyint(1) DEFAULT NULL,
  `faculty_student` tinyint(1) DEFAULT NULL,
  `faculty_year` smallint(5) unsigned DEFAULT NULL,
  `faculty_institution` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `faculty_domain` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `info` text COLLATE utf8_unicode_ci,
  `production_types` text COLLATE utf8_unicode_ci,
  `role_abroad` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `actors_user_id_index` (`user_id`),
  KEY `actors_ethnicity_id_index` (`ethnicity_id`),
  KEY `actors_eyecolour_id_index` (`eyecolour_id`),
  KEY `actors_haircolour_id_index` (`haircolour_id`),
  CONSTRAINT `actors_haircolour_id_foreign` FOREIGN KEY (`haircolour_id`) REFERENCES `haircolours` (`id`) ON DELETE CASCADE,
  CONSTRAINT `actors_ethnicity_id_foreign` FOREIGN KEY (`ethnicity_id`) REFERENCES `ethnicities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `actors_eyecolour_id_foreign` FOREIGN KEY (`eyecolour_id`) REFERENCES `eyecolours` (`id`) ON DELETE CASCADE,
  CONSTRAINT `actors_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of actors
-- ----------------------------
INSERT INTO `actors` VALUES ('1', '49', '17', '1', '2', '190', '149', 'Nu am', 'S', 'Engleza', 'trasaturi orientale', '1', 'Tango,dans modern,polka incepator', 'Voce (Fosta concurenta X factor sezonul 2011)', 'inot, snowboard, atletism, handbal, baschet, volei', 'O', '1', '0', '2005', 'Universitatea Nationala de Arta Teatrala si Cinematografica Ion Luca Caragiale Bucuresti', '1', '0', '2003', 'Bucuresti - Universitatea Nationala de Arta Teatrala si Cinematografica “I.L.Caragiale” (UNATC)', 'filologie intensiv limba engleza', 'http://Niculae.com/', 'Hic ipsam dignissimos molestiae dolor. Debitis aut ad corrupti similique quia velit. Dolores ad molestiae ipsa impedit facere. Sit culpa quisquam fugiat cupiditate rerum. Dolorem ea ut ab qui earum. Sit quo aliquam quo voluptates expedita. Quia necessitatibus fuga quibusdam eum distinctio. Eos nesciunt similique sit et distinctio quis numquam.', 'Iste autem numquam numquam. Id enim soluta quo dolores. Voluptatum perferendis ea eveniet repellendus facere temporibus accusamus. Aut non ullam quos dolor sunt rerum autem. Enim alias et ab aliquam. Temporibus labore voluptatem aut distinctio animi. Consectetur delectus dolorem ut consequatur qui non quam.', '0', '2015-08-31 23:49:37', '2015-08-12 01:05:19');
INSERT INTO `actors` VALUES ('2', '32', '122', '3', '4', '146', '55', 'Ardelean', 'M', 'Franceza', 'trasaturi orientale', '1', 'Da. Membru in trupa de Street Dance Brothers ', 'nu', 'Patinaj artistic,parasutism,innot,volei,echitatie,alpinism', 'O', '1', '0', '1998', 'Colegiul National de Arte', '0', '1', '1993', 'Facultatea de Informatica', 'Facultatea de Teatru, Specializarea Papusi si Marionete', 'http://Marginean.com/fugiat-a-quo-aspernatur-', 'Aut earum accusamus tempora dignissimos deserunt omnis. Perferendis saepe omnis veniam voluptatibus. Voluptatem vero saepe quia laboriosam. Et voluptatem harum sit est ratione. Soluta expedita dolorum perferendis mollitia totam ut. Doloribus sed id est aut voluptas et neque. Id dolor delectus quia.', 'Ex sit aut nostrum excepturi facilis soluta. Qui in dolor placeat dolorum labore repellendus. Animi magni ipsum qui delectus consequatur nihil tempore fugit. Nulla ducimus impedit mollitia nesciunt neque. Aliquam aut expedita quidem iste. Sint possimus reiciendis hic similique. Necessitatibus quia unde aliquam consequuntur at. Rem doloribus assumenda laboriosam quis. Aut facilis qui sint dolorem. Nobis cupiditate omnis fugiat. Accusantium et est voluptatem ad qui doloremque.', '0', '2015-09-08 19:13:48', '2015-08-20 22:38:11');
INSERT INTO `actors` VALUES ('3', '25', '157', '1', '2', '128', '168', 'Ardelean', 'S', 'Engleza', 'fata rotunda, sprancene arcuite, ochi mari si expresivi, nas potrivit, gropite, buze semi-subtiri', '0', 'Latino,dansuri clasice, balet (nivel incepator)', 'nu', 'Patinaj artistic,parasutism,innot,volei,echitatie,alpinism', 'N', '1', '1', '1983', 'Colegiul National de Arte', '0', '1', '2002', 'Bucuresti - Universitatea Nationala de Arta Teatrala si Cinematografica “I.L.Caragiale” (UNATC)', 'actorie', 'http://www.Burlacu.net/sint-est-ut-accusantiu', 'Qui et nihil pariatur quis. Nesciunt nulla qui praesentium assumenda fugit omnis. Voluptas cum quia illo quibusdam. Placeat sit fugit in eius. Necessitatibus at cupiditate libero molestias eos tenetur. Ut omnis omnis consequatur praesentium. Ut ad velit officiis sit. At nemo possimus tempora hic ducimus.', 'Ipsum consequatur et cupiditate enim ut aliquid iusto. Nulla aspernatur enim aut repellendus rerum error nam tempora. Nemo molestiae maxime esse ex. Blanditiis distinctio ad omnis quia consequatur aperiam. Quisquam cupiditate minus sed at. Sint corrupti vel distinctio sed laudantium fugit. Aut blanditiis itaque velit mollitia quo. Molestiae tenetur soluta veritatis molestiae et et. Vero quia aliquid aut vero totam perspiciatis dolorum. Pariatur exercitationem distinctio quia. Quo optio minima expedita quam fuga.', '1', '2015-09-07 13:22:25', '2015-09-02 07:40:29');
INSERT INTO `actors` VALUES ('4', '13', '61', '5', '3', '181', '21', 'Romana', 'M', 'Engleza incepator, Franceza avansat si Spaniola mediu', 'fata mica, luminoasa, ten curat, fara imperfectiuni', '1', 'notiuni balet clasic, dans spotiv, dans contemporan', 'nu', 'Patinaj artistic,parasutism,innot,volei,echitatie,alpinism', 'O', '1', '1', '2009', 'Universitatea Nationala de Arta Teatrala si Cinematografica Ion Luca Caragiale Bucuresti', '0', '1', '2000', 'Cluj -Universitatea Babes-Bolyai - Facultatea de Film si TV', 'IT', 'http://www.Lupu.biz/', 'Aut commodi omnis aspernatur nesciunt et quod. Nobis hic doloribus dolorum non illo delectus praesentium. Perferendis quibusdam asperiores corrupti reprehenderit libero voluptates ab perferendis. Saepe dolores est voluptatem dicta tenetur quos. A ut dolor ipsum qui incidunt architecto ea. Voluptatibus dolor aut magni cupiditate. Et debitis sed atque atque. Nobis molestiae officia placeat et ut reprehenderit. Quibusdam vitae dolores et adipisci delectus repellendus. Nostrum ad rem veritatis neque harum. Omnis eveniet est totam maxime quis natus. Saepe omnis temporibus qui aliquid quisquam. Saepe consequatur numquam magnam et similique omnis. Incidunt cupiditate aliquam quis repudiandae in.', 'Ut non quis sed in et tempora consequatur. Et ut et aut cupiditate provident sequi ipsam unde. Adipisci sit corrupti molestias totam quos. Alias quo et facere impedit saepe doloribus voluptatem. Qui velit dolor ab dicta voluptatem a. Et beatae et doloribus tempore sit non. Molestias voluptatem amet ullam corrupti commodi ipsa quos. Corrupti et reiciendis sint delectus cupiditate. Rerum et debitis tenetur et amet porro optio. Quis autem consectetur sit eos facere architecto nulla.', '0', '2015-08-27 20:27:14', '2015-09-04 15:09:11');
INSERT INTO `actors` VALUES ('5', '30', '47', '3', '2', '185', '227', 'Oltean', 'S', 'Franceza', 'fata mica, luminoasa, ten curat, fara imperfectiuni', '1', 'clasic, contemporan, latin, de societate, ori', 'Pian-foarte putin', 'Patinaj artistic,parasutism,innot,volei,echitatie,alpinism', 'O', '0', '0', '1997', 'UNATC I.L. Caragiale sectia Arta Actorului', '1', '1', '1992', 'Bucuresti - Universitatea Hyperion - Facultatea de Arte', 'filologie intensiv limba engleza', 'http://Vasile.net/deserunt-adipisci-mollitia-', 'Autem ut occaecati rem magni et. Velit at enim et ea quo tempore iure et. Et quidem molestiae voluptate ullam doloribus. Id enim iste qui ut earum pariatur voluptas. Mollitia quis aliquid voluptatem qui aut. Qui qui debitis autem dolorem minima tempore. Consequatur inventore omnis ut corrupti quis unde quo. Velit asperiores nihil consequatur rerum incidunt rerum. Enim reprehenderit quisquam accusamus dolore fugit. Architecto eveniet eos repellat beatae quo. Non animi et laborum blanditiis assumenda aliquam accusantium. Ipsam quisquam vero quos nam. Quo qui possimus vel aut quisquam est. Aut beatae dolores ex qui in molestiae.', 'Quo tenetur sit fuga minima. Optio quaerat ab culpa dolore quia. Minima exercitationem accusamus consectetur voluptatum vel eum. Dignissimos eum minus similique et amet sint voluptatem est. Rerum consectetur et praesentium atque. Et perspiciatis reprehenderit eos perferendis. Quas tempora sunt ut iste quaerat atque velit. Doloribus dolores necessitatibus officia voluptas cumque corporis.', '0', '2015-09-08 04:31:25', '2015-09-08 08:18:03');
INSERT INTO `actors` VALUES ('6', '42', '184', '2', '3', '144', '22', 'Oltean', 'M', 'Engleza incepator, Franceza avansat si Spaniola mediu', 'trasaturi orientale', '0', 'Latino,dansuri clasice, balet (nivel incepator)', 'Voce (Fosta concurenta X factor sezonul 2011)', 'inot, snowboard, atletism, handbal, baschet, volei', 'Y', '0', '0', '1990', 'Ivana Chubbuck-Mandragora Movies Acting Studio', '1', '0', '2005', 'Facultatea de Informatica', 'Cinema and Television', 'http://www.Farcas.com/', 'Laboriosam rerum recusandae aut culpa soluta aspernatur quibusdam. Id provident quas ab voluptates dolores molestiae. Et quia minima repellendus enim. Nesciunt consequatur non voluptatem necessitatibus sit. Nobis assumenda reprehenderit repellat ipsum incidunt libero. Velit fuga et placeat dolor natus enim distinctio. Provident ea natus consequatur ea aut tenetur. Illum iste nam commodi odit totam aspernatur. Rerum id sed enim. Voluptatem possimus omnis architecto. Unde et quod autem id.', 'Quia assumenda nemo sit aperiam consequatur. Debitis delectus qui excepturi sed mollitia saepe ipsa. Recusandae et aut explicabo quia ut voluptatem magni. Amet sequi aut nostrum dolore expedita eius fugiat corrupti. Dolorem ea et voluptas. Modi commodi molestiae qui dolorem et exercitationem. Dolorum ad ut eum.', '0', '2015-08-22 23:43:58', '2015-09-03 03:47:29');
INSERT INTO `actors` VALUES ('7', '38', '130', '7', '1', '142', '254', 'Ardelean', 'S', 'Engleza', 'fata mica, luminoasa, ten curat, fara imperfectiuni', '0', 'Da. Membru in trupa de Street Dance Brothers ', 'nu', 'motociclism', 'Y', '0', '1', '2006', 'Mandragora Movies Film Academy', '0', '0', '2000', 'Bucuresti - Universitatea Nationala de Arta Teatrala si Cinematografica “I.L.Caragiale” (UNATC)', 'filologie intensiv limba engleza', 'https://www.Petre.org/ipsum-velit-voluptatem-', 'Commodi debitis beatae soluta. Voluptate soluta labore est aut ducimus ad. Harum ut deserunt corporis ut. Architecto nihil hic dolore. Illo dolor velit modi. Totam quae dignissimos veniam modi aut voluptatem ut. Et reiciendis commodi tenetur consequatur. Nemo ab veritatis ea quis aut doloremque incidunt aut. Et et eos eaque quos nostrum soluta. Quo soluta praesentium aut molestiae. Doloribus nulla tempora ut minus totam. Dolorum et fugit dolorum eum quibusdam magnam. Tenetur praesentium voluptas possimus facere expedita.', 'Aliquam quia natus perspiciatis temporibus consectetur. Odio iure suscipit et quam impedit reprehenderit asperiores dolor. Placeat vitae esse corporis et aut. Ea maxime a nam optio. Recusandae enim fugit ut dolores. Quia nostrum ea qui. Est recusandae et quia et voluptatem. Voluptates magnam sunt repellendus consequatur quod deleniti totam. Quod dignissimos blanditiis porro illo eveniet. Omnis dolores quam deserunt. Et corrupti eveniet porro voluptates explicabo. Blanditiis voluptatem voluptas omnis voluptate ea occaecati exercitationem id.', '0', '2015-08-30 19:29:01', '2015-08-24 02:00:28');
INSERT INTO `actors` VALUES ('8', '39', '117', '5', '7', '159', '45', 'Oltean', 'M', 'Franceza', 'ochi mari, zambet larg, sprancene dese si bine definite', '0', 'notiuni balet clasic, dans spotiv, dans contemporan', 'nu', 'inot, snowboard, atletism, handbal, baschet, volei', 'Y', '1', '0', '2005', 'Colegiul National de Arte', '1', '1', '2001', 'Facultatea de Informatica', 'Facultatea de Teatru, Specializarea Papusi si Marionete', 'http://www.Banu.com/', 'Corrupti assumenda dolores eum sequi voluptas. Error est ut ea et eius autem. Omnis vel saepe ducimus non. Dolor eaque consequuntur officiis enim. Rerum dolores nihil ut et dicta repellat modi. Aut voluptatem laborum sint excepturi ut sit. Fugit voluptatibus debitis blanditiis ut fuga. Quaerat odio in earum inventore quod officia. Voluptates accusamus minima molestiae quod commodi delectus culpa dolorum. Porro molestiae autem rerum aut. In ea deleniti pariatur molestiae inventore voluptas quisquam. Eum a quia qui enim laboriosam quisquam. Quo praesentium incidunt esse ipsam magni est repellat. Aliquam sunt nobis est vero nulla dicta esse dignissimos.', 'Est minus ut ut sunt odio a incidunt. In repellat et delectus et aut sint laudantium. Qui non excepturi quos tempore natus. Animi optio odit eligendi molestias sed est. Culpa voluptates est cum blanditiis. Aut nesciunt dicta debitis suscipit hic doloremque totam. Ut dolores error iusto fuga rerum non exercitationem. Magni vitae asperiores sed impedit dicta adipisci. Ea nostrum culpa aut ex vel. Autem consectetur ipsam minus quisquam nihil laboriosam. Quam omnis quidem laborum in. Sed nihil ut et quidem perferendis molestiae tempore provident.', '1', '2015-09-06 21:09:29', '2015-08-21 12:05:54');
INSERT INTO `actors` VALUES ('9', '6', '30', '3', '5', '207', '71', 'Ardelean', 'L', 'Engleza incepator, Franceza avansat si Spaniola mediu', 'ochi mari, zambet larg, sprancene dese si bine definite', '1', 'Latino,dansuri clasice, balet (nivel incepator)', 'nu', 'inot, snowboard, atletism, handbal, baschet, volei', 'O', '0', '0', '1980', 'UNATC I.L. Caragiale sectia Arta Actorului', '0', '0', '1995', 'Bucuresti - Universitatea Nationala de Arta Teatrala si Cinematografica “I.L.Caragiale” (UNATC)', 'filologie intensiv limba engleza', 'http://Apostol.com/nesciunt-sint-quia-expedit', 'Sapiente natus corrupti illo tenetur tempore. Ea est alias fuga voluptatem ab quis ut qui. Quibusdam perferendis nam est commodi. In possimus in nisi a blanditiis. Suscipit illo magnam facere ea. A ea quod minima laboriosam deserunt et. In dolorem non dolore illo harum. Adipisci et corporis eos ducimus accusantium fugiat quasi amet.', 'Dolorem magnam quia dolor dolorum eum neque. Voluptatem enim voluptatem tempora et minima. Ratione nulla provident id non. Ab quidem saepe fugiat. Ut sint magni nulla eveniet repellendus. Doloremque cupiditate aut et laboriosam sequi fugit ut. Deserunt dolorum aspernatur non maiores. Odio ratione quia ut quam.', '0', '2015-08-24 16:28:02', '2015-08-12 14:14:41');
INSERT INTO `actors` VALUES ('10', '4', '125', '6', '3', '186', '30', 'fara accent', 'L', 'Engleza incepator, Franceza avansat si Spaniola mediu', 'fata mica, luminoasa, ten curat, fara imperfectiuni', '1', 'notiuni balet clasic, dans spotiv, dans contemporan', 'Voce (Fosta concurenta X factor sezonul 2011)', 'tenis de masa, inot, ski, calarie, ciclism, volei', 'Y', '1', '0', '2011', 'Colegiul National de Arte', '0', '0', '2006', 'Facultatea de Informatica', 'filologie intensiv limba engleza', 'http://Cristea.biz/', 'Facere aut iste quisquam modi. Eum rerum architecto non ad et quia in. Accusamus odio consectetur provident cum. Cupiditate ipsa quia aliquam natus accusamus. Doloremque sed velit quia quisquam. Qui error iure ratione blanditiis voluptates. Eaque ut sequi magni ipsam. Necessitatibus minus aspernatur officia iusto totam aut corrupti. Provident possimus expedita minima similique qui.', 'Officiis in expedita expedita. Quidem vitae perspiciatis repellat non. Repellendus ipsam minima similique exercitationem. Dolorem aut ea voluptates et commodi voluptatem. Placeat ipsum quis cum reiciendis. Voluptatem eos voluptas reprehenderit molestiae nulla cumque occaecati ut. Ea harum ipsa est animi dolor omnis. Impedit corporis ut officiis cum quos architecto. Non quia non voluptatem omnis ipsam. Incidunt eveniet non illo et est.', '1', '2015-08-21 18:05:58', '2015-08-18 01:31:58');

-- ----------------------------
-- Table structure for amators
-- ----------------------------
DROP TABLE IF EXISTS `amators`;
CREATE TABLE `amators` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `ethnicity_id` int(10) unsigned NOT NULL,
  `eyecolour_id` int(10) unsigned NOT NULL,
  `haircolour_id` int(10) unsigned NOT NULL,
  `height` tinyint(3) unsigned DEFAULT NULL,
  `weight` smallint(5) unsigned DEFAULT NULL,
  `accent` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hair_length` enum('S','M','L') COLLATE utf8_unicode_ci DEFAULT NULL,
  `spoken_languages` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facial_traits` text COLLATE utf8_unicode_ci,
  `voice` tinyint(1) DEFAULT NULL,
  `dance` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `instruments` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sports` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nude` enum('Y','N','O') COLLATE utf8_unicode_ci DEFAULT NULL,
  `profesional_studies` tinyint(1) DEFAULT NULL,
  `speciality_studies` tinyint(1) DEFAULT NULL,
  `speciality_year` smallint(5) unsigned DEFAULT NULL,
  `speciality_institution` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `faculty` tinyint(1) DEFAULT NULL,
  `faculty_student` tinyint(1) DEFAULT NULL,
  `faculty_year` smallint(5) unsigned DEFAULT NULL,
  `faculty_institution` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `faculty_domain` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `info` text COLLATE utf8_unicode_ci,
  `production_types` text COLLATE utf8_unicode_ci,
  `role_abroad` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `amators_user_id_index` (`user_id`),
  KEY `amators_ethnicity_id_index` (`ethnicity_id`),
  KEY `amators_eyecolour_id_index` (`eyecolour_id`),
  KEY `amators_haircolour_id_index` (`haircolour_id`),
  CONSTRAINT `amators_haircolour_id_foreign` FOREIGN KEY (`haircolour_id`) REFERENCES `haircolours` (`id`) ON DELETE CASCADE,
  CONSTRAINT `amators_ethnicity_id_foreign` FOREIGN KEY (`ethnicity_id`) REFERENCES `ethnicities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `amators_eyecolour_id_foreign` FOREIGN KEY (`eyecolour_id`) REFERENCES `eyecolours` (`id`) ON DELETE CASCADE,
  CONSTRAINT `amators_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of amators
-- ----------------------------
INSERT INTO `amators` VALUES ('1', '33', '177', '4', '2', '222', '277', 'Romana', 'S', 'Italiana', 'fata mica, luminoasa, ten curat, fara imperfectiuni', '1', 'Da. Membru in trupa de Street Dance Brothers ', 'Chitara clasica, chitara acustica, chitara electrica', 'motociclism', 'N', '1', '0', '2008', 'Ivana Chubbuck-Mandragora Movies Acting Studio', '1', '0', '1984', 'Bucuresti - Universitatea Nationala de Arta Teatrala si Cinematografica “I.L.Caragiale” (UNATC)', 'actorie', 'http://Gabor.net/qui-ad-dolor-doloremque-at-e', 'Iure sunt magnam saepe. Maiores veniam veritatis occaecati. Aperiam dolores aut velit tenetur earum consectetur at. Ullam consequatur aut ut dolores blanditiis dolorum. Molestiae voluptas nesciunt quibusdam totam. Ut cupiditate eum nulla consequuntur architecto quasi. Ut culpa atque eveniet nihil. Maiores et veniam consequatur dolorum est. Aut asperiores aut et sed ipsam consectetur qui. Ut expedita non rerum cumque in excepturi voluptates sint. Aut reiciendis exercitationem ut beatae cupiditate necessitatibus. Eum deleniti aut nesciunt et molestiae. Voluptas voluptatem tempore quo repellat. Reprehenderit nesciunt veritatis sit. Repudiandae vel eaque facilis aut vero et.', 'Rerum sunt et rerum recusandae pariatur eveniet sed. Aut non aperiam maxime deleniti sit. Dolor porro omnis iste dolorem aspernatur laudantium repellendus sed. Enim animi ipsa ea sit. Consectetur eligendi sequi temporibus est voluptas mollitia laboriosam. Quo suscipit quis iusto eos nisi nisi. Velit soluta fugit ad corrupti ut. Quia et rerum explicabo suscipit tenetur dolorem. Explicabo ut ratione libero itaque praesentium saepe inventore aut. Sed ut aperiam ratione molestiae.', '0', '2015-08-16 22:49:43', '2015-08-10 21:55:27');
INSERT INTO `amators` VALUES ('2', '15', '169', '1', '3', '126', '32', 'Nu', 'L', 'Engleza incepator, Franceza avansat si Spaniola mediu', 'ochi mari, zambet larg, sprancene dese si bine definite', '1', 'Tango,dans modern,polka incepator', 'Pian-foarte putin', 'inot, snowboard, atletism, handbal, baschet, volei', 'O', '0', '1', '2004', 'UNATC I.L. Caragiale sectia Arta Actorului', '1', '1', '2001', 'Facultatea de Informatica', 'actorie', 'http://www.Anghel.com/adipisci-quos-nostrum-o', 'Quidem enim nobis quasi quasi accusantium vitae eaque. Exercitationem fugiat architecto asperiores illum vel minima quos. Minima expedita minima in sint voluptas tempore libero. Sit aut possimus nisi incidunt distinctio repellendus. Non ut quos enim nemo delectus necessitatibus et quisquam. Saepe ad itaque itaque qui non non et consequatur. Voluptatem eveniet repudiandae ad non impedit saepe. Sunt aut velit exercitationem dolore quos.', 'Et corrupti et dolore sequi maiores provident natus. Officiis officia eaque quia architecto. Quas dolor nulla expedita amet aut explicabo. Sint sit quod asperiores ea odio id. Dolor odit sapiente et aut sit tempore. Inventore totam modi non quis. Cupiditate labore ullam enim tempore. Nihil aspernatur velit et consequuntur vero animi. Tenetur omnis quos alias rerum corporis laborum cum. Aliquam eius fugiat debitis omnis quos laudantium. Provident id saepe est nobis harum maiores.', '0', '2015-08-11 15:38:35', '2015-08-16 13:07:09');
INSERT INTO `amators` VALUES ('3', '7', '110', '9', '1', '227', '273', 'Romana', 'L', 'Franceza', 'normale/dragute', '1', 'Da. Membru in trupa de Street Dance Brothers ', 'Pian-foarte putin', 'Patinaj artistic,parasutism,innot,volei,echitatie,alpinism', 'O', '0', '0', '2007', 'Mandragora Movies Film Academy', '1', '1', '2003', 'Cluj -Universitatea Babes-Bolyai - Facultatea de Film si TV', 'Cinema and Television', 'http://Teodorescu.com/itaque-voluptas-dolor-a', 'Ut aperiam dicta veritatis fuga sed soluta. Et vel sit fugiat quasi ut atque. Quam sed esse repellat delectus amet non ipsum commodi. Similique rem veritatis delectus officia in explicabo sed. Debitis sequi et voluptas at at culpa aut soluta. Voluptatem ea tempora ad enim est velit. Et similique veniam quasi sed dignissimos nihil. Ut et vero eos voluptatem eos ipsa tempora. Adipisci aliquam omnis doloribus accusamus qui cupiditate modi neque. Aut doloremque similique voluptatem quam earum nulla illo. Iste aut blanditiis rem libero praesentium. Et error nulla animi nemo ut dolorem error.', 'Soluta et veritatis ut eveniet ea. Et quaerat cumque est voluptatibus. Eum minus quia aliquid iure voluptatem. Minima quam accusamus nobis nesciunt et dignissimos. Tenetur ullam beatae expedita voluptatem. Deserunt eligendi accusamus velit distinctio. Facere rerum id ipsa minus et quasi. Voluptates voluptatum odio quidem aliquam. Minus natus voluptas rerum dolor quas. Dicta consequatur laudantium quidem qui enim. Est exercitationem veniam facilis quod minima doloremque.', '1', '2015-08-09 16:53:32', '2015-08-09 23:47:17');
INSERT INTO `amators` VALUES ('4', '3', '146', '7', '5', '136', '236', 'Oltean', 'S', 'Engleza', 'trasaturi orientale', '1', 'Tango,dans modern,polka incepator', 'flaut , pian', 'tenis de masa, inot, ski, calarie, ciclism, volei', 'Y', '0', '1', '1981', 'Ivana Chubbuck-Mandragora Movies Acting Studio', '0', '1', '2009', 'Bucuresti - Universitatea Nationala de Arta Teatrala si Cinematografica “I.L.Caragiale” (UNATC)', 'IT', 'http://Toader.com/minima-quod-et-at-beatae', 'Assumenda sit eius cum occaecati eligendi quis. Quidem ea nesciunt enim doloremque ex. Et sit hic reiciendis quasi qui dolores. Libero sit suscipit nihil ut. Aspernatur ratione doloribus sed sequi eos. Debitis consequatur quia velit facere. Dolore corporis nesciunt nisi repellendus ex. Consequatur perferendis reiciendis labore numquam dignissimos iste fugit. Quisquam dolor non ex dolores ipsum laboriosam. Ea eum sequi nulla eos ut. Dolore maiores voluptatem doloremque officia. Consequatur rem quas sint sint aut. Rerum numquam consequatur voluptatem.', 'Rerum excepturi minima dignissimos quia deserunt. Incidunt libero soluta quaerat tempore qui voluptatem. Voluptates ut maxime voluptas ab et non nihil. Repellendus facere et minima nulla. Inventore accusantium voluptatum quidem quis. Ab dolorum ullam voluptatem assumenda. Recusandae ea sit unde voluptatem exercitationem. Magni necessitatibus error ab ad repellat fugiat aspernatur.', '0', '2015-08-09 02:35:22', '2015-08-28 08:13:35');
INSERT INTO `amators` VALUES ('5', '21', '167', '2', '3', '181', '229', 'Ardelean', 'M', 'Engleza', 'ochi mari, zambet larg, sprancene dese si bine definite', '0', 'notiuni balet clasic, dans spotiv, dans contemporan', 'Voce (Fosta concurenta X factor sezonul 2011)', 'motociclism', 'O', '0', '1', '2016', 'Colegiul National de Arte', '0', '1', '2000', 'Facultatea de Informatica', 'IT', 'http://Pintilie.biz/illo-hic-sed-consequuntur', 'Nam cumque veritatis corporis magni non iste veritatis. Suscipit doloremque eum perferendis in in labore magnam. Quo qui eius repudiandae at molestias a ea ut. Accusantium rerum dolorum error est ex ut excepturi dolor. Deserunt vitae amet quod quisquam nihil quae et distinctio. Omnis fugiat esse qui amet cupiditate eveniet fugiat. Accusamus est at quis unde omnis blanditiis deserunt. Ut quidem autem voluptatem. Accusamus aperiam voluptas quo libero doloremque rerum. Voluptatum ad et vel minima iste voluptatem vel.', 'Aliquid possimus neque eius sed eos quis. Occaecati et et iste assumenda odit consequatur earum. Eum voluptates nihil recusandae magnam. Laborum nisi accusamus quo sequi dolores quia. Et illum beatae laboriosam molestiae quasi tempore. Voluptatem est aut et ipsa voluptas fuga. Aperiam alias et tempora aut ullam quis nisi aliquam. Eveniet possimus atque consequatur et laboriosam possimus officiis distinctio. Libero rem omnis magni quaerat perferendis. Quasi maxime et adipisci amet voluptate. Quas molestiae id nobis hic repellat nostrum illo. Repellat quo est ullam sequi temporibus accusantium harum. Neque ut voluptas impedit est rerum rerum. Ipsam eveniet aut quia autem.', '1', '2015-08-12 18:06:56', '2015-08-09 02:58:27');
INSERT INTO `amators` VALUES ('6', '5', '1', '7', '1', '216', '211', 'Romana', 'S', 'Franceza', 'trasaturi orientale', '1', 'Da. Membru in trupa de Street Dance Brothers ', 'Voce (Fosta concurenta X factor sezonul 2011)', 'tenis de masa, inot, ski, calarie, ciclism, volei', 'N', '1', '0', '1986', 'Ivana Chubbuck-Mandragora Movies Acting Studio', '0', '0', '1980', 'Bucuresti - Universitatea Nationala de Arta Teatrala si Cinematografica “I.L.Caragiale” (UNATC)', 'Facultatea de Teatru, Specializarea Papusi si Marionete', 'http://Biro.info/vitae-nam-illo-enim-ab', 'Ipsa quia quis ut beatae quisquam. Commodi ratione velit veniam autem consectetur aut repudiandae. Quis odit consequuntur qui assumenda. Tempora omnis aut nulla. Sit ut occaecati id. Neque error in vitae et. Repellat quam ut maxime provident mollitia consequatur eius. Minima modi qui nihil necessitatibus quasi inventore at sit. Provident enim aut ut blanditiis dolor dignissimos sunt. Nihil nemo labore vel ipsa. Laboriosam optio rem natus quia et. Sequi perspiciatis reiciendis corporis harum molestias beatae laboriosam quia. Est incidunt qui non debitis asperiores qui dolor et. Qui in quaerat vel.', 'Ullam similique aut illum omnis atque mollitia. Ad sit earum pariatur quis fugiat in et est. Nemo eos culpa hic corrupti. Et dolores voluptate perspiciatis sint enim. Dolorem est sit voluptatem autem nostrum. Accusamus alias est magnam et. Ut dicta voluptas repellat ducimus. Maiores omnis consequatur facere odio consectetur aut a.', '0', '2015-08-27 12:03:39', '2015-08-20 14:03:28');
INSERT INTO `amators` VALUES ('7', '33', '91', '3', '2', '153', '28', 'Moldovean', 'S', 'Engleza', 'normale/dragute', '0', 'Latino,dansuri clasice, balet (nivel incepator)', 'flaut , pian', 'motociclism', 'O', '0', '0', '2014', 'Spiru Haret Bucuresti', '0', '0', '1989', 'Bucuresti - Universitatea Hyperion - Facultatea de Arte', 'IT', 'http://Stan.info/error-est-eos-recusandae-per', 'Aut omnis cum cumque repellendus. Aut ut animi accusantium corporis aut dolor aliquam. Doloribus ipsa commodi nemo atque vitae enim. Nesciunt et animi commodi quae consequatur. Minus pariatur qui quos et id repellat. Unde dolores et reiciendis laboriosam animi repellendus. Ipsa atque quia hic fugit. Beatae ipsum earum dolorum sint.', 'Vitae occaecati sed vitae nisi rerum dolorum mollitia. Laudantium natus enim qui aspernatur sunt ut hic. Cupiditate voluptas ut ducimus quia fugiat soluta ducimus tenetur. Ut impedit laboriosam ut veniam eos qui necessitatibus laudantium. Dolorum numquam nobis enim odio harum vitae est. Ut nihil autem culpa voluptatibus est non. Iure dolores dolor laudantium quia. Possimus ut saepe rem architecto adipisci cupiditate nulla. Voluptas deserunt laborum itaque iure sit. Corporis incidunt consectetur numquam beatae.', '0', '2015-09-04 05:49:10', '2015-08-17 18:18:25');
INSERT INTO `amators` VALUES ('8', '45', '47', '1', '5', '198', '279', 'fara accent', 'L', 'Engleza', 'fata rotunda, sprancene arcuite, ochi mari si expresivi, nas potrivit, gropite, buze semi-subtiri', '0', 'Da. Membru in trupa de Street Dance Brothers ', 'Pian-foarte putin', 'tenis de masa, inot, ski, calarie, ciclism, volei', 'Y', '0', '1', '1987', 'Colegiul National de Arte', '0', '0', '2003', 'Bucuresti - Universitatea Nationala de Arta Teatrala si Cinematografica “I.L.Caragiale” (UNATC)', 'actorie', 'http://Luca.com/placeat-impedit-in-consequatu', 'Quidem qui non nobis quisquam fugiat architecto nam. Expedita quam quam dolorem quia quas labore neque. Reprehenderit quia rerum voluptatem. A temporibus nesciunt esse occaecati non voluptatem. Fugiat aperiam quae vero at id quis soluta. Inventore consequatur corporis iste voluptate accusamus accusamus. Magni laboriosam fugiat omnis voluptas quis autem. Magni libero autem facilis sequi. Consequatur ullam ratione nihil autem officiis. Minima veritatis est consequatur. Repellendus molestiae ut dolor quo. Rerum ducimus maxime vitae cum. Animi dolor quia quia cupiditate ut recusandae nesciunt.', 'Consequatur numquam quos cumque quas. Autem tenetur dolores quibusdam corrupti. Aspernatur ea enim explicabo ut est. Libero corrupti repellat omnis qui est magnam debitis at. Delectus nesciunt ut et blanditiis sint dolorum delectus. Porro voluptatem necessitatibus necessitatibus ut. Aut eum dolor minima debitis. Est sit iure nesciunt praesentium fugiat ab expedita sunt.', '1', '2015-08-11 06:20:49', '2015-08-13 02:13:23');
INSERT INTO `amators` VALUES ('9', '32', '7', '2', '5', '186', '123', 'Ardelean', 'L', 'Italiana', 'fata rotunda, sprancene arcuite, ochi mari si expresivi, nas potrivit, gropite, buze semi-subtiri', '0', 'clasic, contemporan, latin, de societate, ori', 'nu', 'tenis de masa, inot, ski, calarie, ciclism, volei', 'Y', '0', '1', '1990', 'Mandragora Movies Film Academy', '0', '1', '2016', 'Cluj -Universitatea Babes-Bolyai - Facultatea de Film si TV', 'Cinema and Television', 'https://Miron.com/at-eligendi-accusantium-cul', 'Officiis perferendis eligendi qui deleniti perferendis. Omnis rerum veritatis magni. Non consequatur quia error non et quidem sint. Accusamus nihil commodi eos aspernatur eos blanditiis. Est voluptatem dicta consequatur hic consequatur. Magnam illum et neque id omnis. Cupiditate quis quam quibusdam aliquid et. Corrupti doloribus tenetur et qui ipsum rem eius.', 'Necessitatibus ut minima harum qui libero magni. Deserunt quis nesciunt distinctio quisquam ipsa aliquam reiciendis. Labore in sequi beatae non velit libero consequuntur odio. Enim amet est non animi ad alias. Qui nobis nostrum et minima qui corrupti. Eligendi voluptas veniam quo omnis dolorem officia et. In quae eum voluptatum nisi nemo sed minima. Ipsum expedita architecto quo. Ex cum in et laboriosam officia. Eveniet voluptas eos sint quibusdam quia.', '1', '2015-08-21 19:16:19', '2015-08-26 15:42:56');
INSERT INTO `amators` VALUES ('10', '46', '39', '5', '4', '207', '168', 'Romana', 'L', 'Italiana', 'normale/dragute', '1', 'notiuni balet clasic, dans spotiv, dans contemporan', 'nu', 'inot, snowboard, atletism, handbal, baschet, volei', 'O', '1', '0', '1999', 'Mandragora Movies Film Academy', '0', '0', '1994', 'Facultatea de Informatica', 'filologie intensiv limba engleza', 'http://www.Duta.com/officiis-ea-aut-consequat', 'Mollitia sit ratione porro necessitatibus aut velit voluptas. Voluptatem amet vitae fugit maiores qui nihil. Qui vel odio reprehenderit eos aut cum voluptate sint. Consequuntur quos ut facere adipisci voluptates corrupti aut. Qui doloremque dignissimos itaque quasi magnam cumque et. Ut sequi voluptatem iure molestiae nobis et. Quidem a voluptatum ut qui ut. Saepe corporis explicabo inventore vero. Ut optio ad temporibus dolores ut a aut. Asperiores qui qui qui placeat illo voluptas. Explicabo id magni ad. Eaque qui eum quibusdam non fugiat. Ullam molestiae voluptas rerum corporis. Quidem deserunt eius iste aliquid id aliquam illo.', 'Non id facilis quia voluptatem et. Omnis mollitia ipsa ea eos fugit. Consequuntur praesentium vitae quia eum aspernatur repudiandae aut. Doloremque molestiae voluptatum et et ducimus. Modi laboriosam aut sed consequatur sequi. Est inventore dolorem voluptatem vel eveniet. Voluptatum ut et unde molestiae porro aut. Veniam neque molestiae mollitia ab velit quisquam. Pariatur quis dolor autem quam optio et dignissimos. Iure voluptatum aliquam sed ducimus reiciendis quidem. Officia dolorem laborum exercitationem qui quia mollitia accusantium qui. Sed sed architecto sed dolorem earum.', '1', '2015-09-08 21:02:45', '2015-08-16 02:33:41');

-- ----------------------------
-- Table structure for countries
-- ----------------------------
DROP TABLE IF EXISTS `countries`;
CREATE TABLE `countries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(55) COLLATE utf8_unicode_ci NOT NULL,
  `prefix` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=238 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of countries
-- ----------------------------
INSERT INTO `countries` VALUES ('1', 'Afghanistan', '93', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('2', 'Albania', '355', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('3', 'Algeria', '213', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('4', 'American Samoa', '684', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('5', 'Andorra', '376', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('6', 'Angola', '244', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('7', 'Anguilla', '264', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('8', 'Antarctica', '672', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('9', 'Antigua and Barbuda', '268', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('10', 'Argentina', '54', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('11', 'Armenia', '374', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('12', 'Aruba', '297', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('13', 'Australia', '61', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('14', 'Austria', '43', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('15', 'Azerbaijan', '994', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('16', 'Bahamas', '242', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('17', 'Bahrain', '973', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('18', 'Bangladesh', '880', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('19', 'Barbados', '246', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('20', 'Belarus', '375', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('21', 'Belgium', '32', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('22', 'Belize', '501', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('23', 'Benin', '229', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('24', 'Bermuda', '441', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('25', 'Bhutan', '975', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('26', 'Bolivia', '591', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('27', 'Bosnia and Herzegovina', '387', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('28', 'Botswana', '267', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('29', 'Brazil', '55', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('30', 'British Indian Ocean Territory', '-', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('31', 'British Virgin Islands', '284', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('32', 'Brunei', '673', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('33', 'Bulgaria', '359', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('34', 'Burkina Faso', '226', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('35', 'Burma (Myanmar)', '95', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('36', 'Burundi', '257', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('37', 'Cambodia', '855', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('38', 'Cameroon', '237', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('39', 'Canada', '1', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('40', 'Cape Verde', '238', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('41', 'Cayman Islands', '345', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('42', 'Central African Republic', '236', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('43', 'Chad', '235', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('44', 'Chile', '56', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('45', 'China', '86', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('46', 'Christmas Island', '61', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('47', 'Cocos (Keeling) Islands', '61', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('48', 'Colombia', '57', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('49', 'Comoros', '269', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('50', 'Cook Islands', '682', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('51', 'Costa Rica', '506', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('52', 'Croatia', '385', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('53', 'Cuba', '53', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('54', 'Cyprus', '357', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('55', 'Czech Republic', '420', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('56', 'Democratic Republic of the Congo', '243', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('57', 'Denmark', '45', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('58', 'Djibouti', '253', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('59', 'Dominica', '767', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('60', 'Dominican Republic', '809', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('61', 'Ecuador', '593', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('62', 'Egypt', '20', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('63', 'El Salvador', '503', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('64', 'Equatorial Guinea', '240', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('65', 'Eritrea', '291', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('66', 'Estonia', '372', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('67', 'Ethiopia', '251', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('68', 'Falkland Islands', '500', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('69', 'Faroe Islands', '298', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('70', 'Fiji', '679', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('71', 'Finland', '358', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('72', 'France', '33', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('73', 'French Polynesia', '689', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('74', 'Gabon', '241', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('75', 'Gambia', '220', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('76', 'Gaza Strip', '970', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('77', 'Georgia', '995', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('78', 'Germany', '49', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('79', 'Ghana', '233', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('80', 'Gibraltar', '350', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('81', 'Greece', '30', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('82', 'Greenland', '299', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('83', 'Grenada', '473', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('84', 'Guam', '671', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('85', 'Guatemala', '502', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('86', 'Guinea', '224', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('87', 'Guinea-Bissau', '245', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('88', 'Guyana', '592', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('89', 'Haiti', '509', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('90', 'Holy See (Vatican City)', '39', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('91', 'Honduras', '504', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('92', 'Hong Kong', '852', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('93', 'Hungary', '36', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('94', 'Iceland', '354', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('95', 'India', '91', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('96', 'Indonesia', '62', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('97', 'Iran', '98', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('98', 'Iraq', '964', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('99', 'Ireland', '353', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('100', 'Isle of Man', '44', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('101', 'Israel', '972', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('102', 'Italy', '39', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('103', 'Ivory Coast', '225', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('104', 'Jamaica', '876', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('105', 'Japan', '81', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('106', '', 'Jersey 	', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('107', 'Jordan', '962', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('108', 'Kazakhstan', '7', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('109', 'Kenya', '254', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('110', 'Kiribati', '686', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('111', 'Kosovo', '381', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('112', 'Kuwait', '965', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('113', 'Kyrgyzstan', '996', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('114', 'Laos', '856', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('115', 'Latvia', '371', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('116', 'Lebanon', '961', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('117', 'Lesotho', '266', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('118', 'Liberia', '231', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('119', 'Libya', '218', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('120', 'Liechtenstein', '423', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('121', 'Lithuania', '370', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('122', 'Luxembourg', '352', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('123', 'Macau', '853', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('124', 'Macedonia', '389', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('125', 'Madagascar', '261', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('126', 'Malawi', '265', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('127', 'Malaysia', '60', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('128', 'Maldives', '960', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('129', 'Mali', '223', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('130', 'Malta', '356', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('131', 'Marshall Islands', '692', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('132', 'Mauritania', '222', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('133', 'Mauritius', '230', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('134', 'Mayotte', '262', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('135', 'Mexico', '52', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('136', 'Micronesia', '691', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('137', 'Moldova', '373', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('138', 'Monaco', '377', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('139', 'Mongolia', '976', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('140', 'Montenegro', '382', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('141', 'Montserrat', '664', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('142', 'Morocco', '212', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('143', 'Mozambique', '258', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('144', 'Namibia', '264', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('145', 'Nauru', '674', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('146', 'Nepal', '977', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('147', 'Netherlands', '31', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('148', 'Netherlands Antilles', '599', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('149', 'New Caledonia', '687', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('150', 'New Zealand', '64', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('151', 'Nicaragua', '505', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('152', 'Niger', '227', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('153', 'Nigeria', '234', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('154', 'Niue', '683', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('155', 'Norfolk Island', '672', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('156', 'North Korea', '850', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('157', 'Northern Mariana Islands', '670', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('158', 'Norway', '47', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('159', 'Oman', '968', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('160', 'Pakistan', '92', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('161', 'Palau', '680', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('162', 'Panama', '507', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('163', 'Papua New Guinea', '675', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('164', 'Paraguay', '595', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('165', 'Peru', '51', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('166', 'Philippines', '63', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('167', 'Pitcairn Islands', '870', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('168', 'Poland', '48', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('169', 'Portugal', '351', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('170', 'Puerto Rico', '1', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('171', 'Qatar', '974', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('172', 'Republic of the Congo', '242', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('173', 'Romania', '40', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('174', 'Russia', '7', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('175', 'Rwanda', '250', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('176', 'Saint Barthelemy', '590', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('177', 'Saint Helena', '290', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('178', 'Saint Kitts and Nevis', '869', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('179', 'Saint Lucia', '758', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('180', 'Saint Martin', '599', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('181', 'Saint Pierre and Miquelon', '508', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('182', 'Saint Vincent and the Grenadines', '784', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('183', 'Samoa', '685', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('184', 'San Marino', '378', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('185', 'Sao Tome and Principe', '239', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('186', 'Saudi Arabia', '966', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('187', 'Senegal', '221', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('188', 'Serbia', '381', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('189', 'Seychelles', '248', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('190', 'Sierra Leone', '232', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('191', 'Singapore', '65', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('192', 'Slovakia', '421', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('193', 'Slovenia', '386', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('194', 'Solomon Islands', '677', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('195', 'Somalia', '252', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('196', 'South Africa', '27', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('197', 'South Korea', '82', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('198', 'Spain', '34', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('199', 'Sri Lanka', '94', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('200', 'Sudan', '249', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('201', 'Suriname', '597', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('202', 'Svalbard', '-', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('203', 'Swaziland', '268', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('204', 'Sweden', '46', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('205', 'Switzerland', '41', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('206', 'Syria', '963', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('207', 'Taiwan', '886', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('208', 'Tajikistan', '992', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('209', 'Tanzania', '255', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('210', 'Thailand', '66', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('211', 'Timor-Leste', '670', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('212', 'Togo', '228', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('213', 'Tokelau', '690', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('214', 'Tonga', '676', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('215', 'Trinidad and Tobago', '868', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('216', 'Tunisia', '216', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('217', 'Turkey', '90', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('218', 'Turkmenistan', '993', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('219', 'Turks and Caicos Islands', '649', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('220', 'Tuvalu', '688', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('221', 'Uganda', '256', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('222', 'Ukraine', '380', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('223', 'United Arab Emirates', '971', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('224', 'United Kingdom', '44', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('225', 'United States', '1', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('226', 'Uruguay', '598', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('227', 'US Virgin Islands', '340', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('228', 'Uzbekistan', '998', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('229', 'Vanuatu', '678', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('230', 'Venezuela', '58', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('231', 'Vietnam', '84', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('232', 'Wallis and Futuna', '681', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('233', 'West Bank', '970', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('234', 'Western Sahara', '-', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('235', 'Yemen', '967', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('236', 'Zambia', '260', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `countries` VALUES ('237', 'Zimbabwe', '263', '2015-09-08 22:29:29', '2015-09-08 22:29:29');

-- ----------------------------
-- Table structure for dancers
-- ----------------------------
DROP TABLE IF EXISTS `dancers`;
CREATE TABLE `dancers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `ethnicity_id` int(10) unsigned NOT NULL,
  `eyecolour_id` int(10) unsigned NOT NULL,
  `haircolour_id` int(10) unsigned NOT NULL,
  `height` smallint(5) unsigned DEFAULT NULL,
  `weight` smallint(5) unsigned DEFAULT NULL,
  `accent` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hair_length` enum('S','M','L') COLLATE utf8_unicode_ci DEFAULT NULL,
  `spoken_languages` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `body_characteristics` text COLLATE utf8_unicode_ci,
  `profesional_studies` tinyint(1) DEFAULT NULL,
  `speciality_studies` tinyint(1) DEFAULT NULL,
  `speciality_year` smallint(5) unsigned DEFAULT NULL,
  `speciality_institution` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `faculty` tinyint(1) DEFAULT NULL,
  `faculty_student` tinyint(1) DEFAULT NULL,
  `faculty_year` smallint(5) unsigned DEFAULT NULL,
  `faculty_institution` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `faculty_domain` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `info` text COLLATE utf8_unicode_ci,
  `production_types` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `dancers_user_id_index` (`user_id`),
  KEY `dancers_ethnicity_id_index` (`ethnicity_id`),
  KEY `dancers_eyecolour_id_index` (`eyecolour_id`),
  KEY `dancers_haircolour_id_index` (`haircolour_id`),
  CONSTRAINT `dancers_haircolour_id_foreign` FOREIGN KEY (`haircolour_id`) REFERENCES `haircolours` (`id`) ON DELETE CASCADE,
  CONSTRAINT `dancers_ethnicity_id_foreign` FOREIGN KEY (`ethnicity_id`) REFERENCES `ethnicities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `dancers_eyecolour_id_foreign` FOREIGN KEY (`eyecolour_id`) REFERENCES `eyecolours` (`id`) ON DELETE CASCADE,
  CONSTRAINT `dancers_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of dancers
-- ----------------------------
INSERT INTO `dancers` VALUES ('1', '12', '159', '1', '4', '183', '154', 'Ardelean', 'L', 'Italiana', 'ochi mari, zambet larg, sprancene dese si bine definite', '1', '0', '1985', 'Ivana Chubbuck-Mandragora Movies Acting Studio', '1', '0', '1995', 'Bucuresti - Universitatea Hyperion - Facultatea de Arte', 'filologie intensiv limba engleza', 'https://www.Ciobanu.com/quia-veritatis-aut-ve', 'Et excepturi et unde non necessitatibus consectetur. Et sed aut et. Doloremque id cupiditate eum dolores. Nihil dignissimos aut aut ut ipsam. Quas eos ut dolores iusto ut. Suscipit fugit rerum ratione beatae enim. Sapiente voluptatum id sed incidunt eveniet tempore. Ut vitae vel vero qui dignissimos. Voluptatem laboriosam modi sint quo omnis.', 'Possimus ex officiis rerum repudiandae aut vel eveniet. Voluptas deleniti quis porro delectus. Harum nam rerum quibusdam molestias. Nobis blanditiis impedit vero et aut perspiciatis. Unde sint iure quo voluptatibus voluptas earum voluptatem. Aliquam ut et totam iste. Voluptates consequatur enim eos sit et. Molestias consectetur eos aut qui beatae dolorem ut. Molestiae et magnam est consequatur hic corrupti odit. Quod enim rem laboriosam ut.', '2015-08-10 23:37:54', '2015-08-31 17:18:03');
INSERT INTO `dancers` VALUES ('2', '48', '59', '2', '3', '220', '192', 'Nu', 'S', 'Engleza incepator, Franceza avansat si Spaniola mediu', 'trasaturi orientale', '0', '0', '1999', 'Mandragora Movies Film Academy', '1', '0', '2010', 'Bucuresti - Universitatea Nationala de Arta Teatrala si Cinematografica “I.L.Caragiale” (UNATC)', 'filologie intensiv limba engleza', 'http://www.Avram.com/', 'Commodi quam non possimus architecto. Dolorum placeat necessitatibus omnis vitae ut sint eos. Voluptatibus itaque enim ipsa ut facilis repudiandae. Provident dicta omnis temporibus sed. Et earum ipsam voluptas enim in qui fugit. Eos cumque qui in enim beatae asperiores ipsum. Et dolore voluptas aperiam quos eius nobis. Quasi enim eligendi est et. Consequatur ex autem id.', 'Soluta et voluptatem natus aliquam molestiae. Quis architecto illum dicta facilis nobis a magnam. Mollitia ut autem laudantium. Adipisci nam quasi hic reprehenderit voluptas numquam labore. Necessitatibus dolore voluptatum laborum cumque. Labore adipisci est consequatur aperiam. Repudiandae dolorum voluptas animi nostrum. Magnam numquam voluptatem consequuntur id quis ab ratione. Repudiandae qui praesentium quos. Alias sunt rerum sit libero. Nihil eaque perferendis fuga voluptas itaque doloribus. Eligendi architecto occaecati reiciendis laborum dolores vitae eos. Doloribus dignissimos quo quis velit hic. Et dolorem ut nisi quod voluptatem totam sit.', '2015-08-19 14:59:21', '2015-09-07 08:51:58');
INSERT INTO `dancers` VALUES ('3', '36', '94', '7', '6', '206', '54', 'Romana', 'M', 'Italiana', '86/58/93, tatuaj mic glezna stanga, flexibilitate, body movement', '0', '0', '1987', 'UNATC I.L. Caragiale sectia Arta Actorului', '1', '1', '2015', 'Facultatea de Informatica', 'actorie', 'http://www.Tomescu.com/', 'Et et ullam expedita qui. Nisi tempora temporibus repellendus labore dolores eos sunt debitis. Rerum ut veniam voluptatum nihil. Modi velit qui nemo dolores sapiente. Molestiae itaque laudantium voluptatem quod qui nobis voluptates. Nihil numquam libero ad quasi. Omnis vitae sed ut inventore tempore qui. Aut in harum est ad quam consectetur. Optio magnam illo impedit voluptas sint aperiam. Et molestias magni nisi et officia. Praesentium et aut nemo qui. Atque quibusdam est cumque mollitia aliquam praesentium.', 'Dignissimos occaecati iure repellat exercitationem maiores consequatur nam quibusdam. Esse dignissimos amet minima dolores. Beatae voluptatibus similique ut. Rem aut qui numquam optio cum quas laborum. Repellendus sed repellendus dolor ullam. Amet voluptates corporis praesentium. Autem voluptates dicta quod reiciendis sint consequatur.', '2015-08-24 12:17:24', '2015-08-30 15:07:24');
INSERT INTO `dancers` VALUES ('4', '42', '158', '1', '4', '157', '233', 'Ardelean', 'M', 'Italiana', 'ochi mari, zambet larg, sprancene dese si bine definite', '1', '0', '1981', 'Colegiul National de Arte', '1', '0', '1994', 'Bucuresti - Universitatea Nationala de Arta Teatrala si Cinematografica “I.L.Caragiale” (UNATC)', 'Facultatea de Teatru, Specializarea Papusi si Marionete', 'https://Stancu.com/similique-repudiandae-veni', 'Inventore in suscipit saepe amet perferendis. Magnam accusantium odit accusantium quia dolorum voluptatum autem. Amet in dolorem quod delectus molestiae assumenda ut. Consequatur facere doloremque porro dolorem molestiae eligendi. Consectetur dicta est qui officia. Possimus vel ea corporis itaque modi et possimus. Optio tenetur maxime qui et quia.', 'Ut quis enim impedit possimus et nulla sint. Nostrum laboriosam suscipit cupiditate vero cum officiis. Omnis sint omnis nostrum et facilis. Et quo nihil reiciendis vel rem. Quo sit maiores et deserunt dolorem nisi nobis. Tempora quaerat sit illo non amet blanditiis. Voluptas possimus vitae dolores a.', '2015-08-28 17:14:56', '2015-08-24 09:47:34');
INSERT INTO `dancers` VALUES ('5', '5', '107', '6', '4', '169', '151', 'Nu am', 'S', 'Engleza', 'ochi mari, zambet larg, sprancene dese si bine definite', '1', '0', '1989', 'Universitatea Nationala de Arta Teatrala si Cinematografica Ion Luca Caragiale Bucuresti', '1', '0', '2011', 'Cluj -Universitatea Babes-Bolyai - Facultatea de Film si TV', 'Cinema and Television', 'http://Farcas.com/', 'Harum aliquid voluptatem asperiores officia animi. Accusantium laborum itaque fuga deleniti iste sed itaque. Eveniet quia et perferendis earum perferendis ipsa doloremque. Possimus reprehenderit quaerat blanditiis tempore. Soluta molestias aut ea modi magnam. Cumque adipisci temporibus eum aut quia excepturi culpa. Ut ipsam recusandae ab minus consequuntur eos cupiditate. Mollitia commodi est quam necessitatibus quidem non velit dolor. Quia iure id molestias ut laudantium fuga. Nostrum omnis necessitatibus consequatur aliquam recusandae. Adipisci totam sapiente recusandae repellat. In iure totam cupiditate quas. Recusandae non debitis ipsum quas culpa odio. Iste nostrum expedita ratione aut ut alias amet vel.', 'Soluta aut facere est. Et perferendis quaerat deleniti. Minima minima et aspernatur ducimus aut minima a accusantium. Esse eaque rerum cum recusandae occaecati. Aut dolorem recusandae repudiandae quaerat magni provident repellendus ut. Pariatur aut nisi dolores ea sapiente earum. Molestias quaerat omnis harum in et rerum et. Vitae enim amet reiciendis iure vero et sit. At accusantium et earum dolores quo. Deleniti in ut non natus voluptatibus officiis eos. Nobis aut qui dolorum numquam eos sunt.', '2015-09-01 12:22:34', '2015-08-24 00:57:10');
INSERT INTO `dancers` VALUES ('6', '2', '44', '8', '4', '173', '259', 'Moldovean', 'S', 'Engleza', 'Picioare lungi,talie subtire,pometi proeminenti...', '1', '1', '1997', 'UNATC I.L. Caragiale sectia Arta Actorului', '1', '1', '2015', 'Bucuresti - Universitatea Hyperion - Facultatea de Arte', 'filologie intensiv limba engleza', 'http://www.Petrescu.info/voluptatem-vitae-cor', 'Adipisci facere expedita in. Possimus id quo quia velit culpa nulla culpa. Voluptas omnis est rem molestias praesentium reiciendis corporis. Magnam atque ut modi nisi blanditiis unde. Sit corporis eius est sint ea voluptatem reiciendis. Voluptatum veritatis eum ullam fuga. Quam ut dolore sed et voluptatum asperiores nam.', 'Ut provident dolor aut nostrum iusto debitis omnis. Qui doloremque sunt dignissimos et ut ab. Ea iusto est neque veniam nostrum. Excepturi eum quod omnis sit rerum illum. Molestiae ratione saepe eius dolor est minus itaque. Quisquam facilis qui dolores voluptatibus. Eos iure aut enim molestiae omnis. Ut ratione ad quidem. Deserunt nisi impedit eius numquam minima hic quia.', '2015-08-16 23:01:08', '2015-08-24 05:59:48');
INSERT INTO `dancers` VALUES ('7', '1', '68', '5', '1', '189', '118', 'Ardelean', 'S', 'Engleza incepator, Franceza avansat si Spaniola mediu', 'ochi mari, zambet larg, sprancene dese si bine definite', '1', '1', '1984', 'UNATC I.L. Caragiale sectia Arta Actorului', '0', '0', '2008', 'Bucuresti - Universitatea Hyperion - Facultatea de Arte', 'Cinema and Television', 'https://Anghel.info/laborum-velit-cumque-labo', 'Quia et ullam sapiente animi mollitia minus et non. Sint consequatur iure reprehenderit exercitationem. Provident nisi delectus vero aut et nobis. Laudantium porro cumque dolorem eum explicabo impedit. Amet blanditiis cum voluptate nesciunt et ipsum et. Odit consequatur quod doloribus eos. Molestias dolorum sit voluptatum autem quod quidem quibusdam.', 'Ut sed voluptatem dolor et consequatur id ducimus. Recusandae optio doloribus ut reprehenderit quod et saepe. Non vero voluptatem ipsam libero itaque voluptas eaque. Eveniet quod quo est nisi molestiae vel. Assumenda totam accusantium doloribus sunt sed officia. Velit eos praesentium excepturi magni. Pariatur impedit impedit impedit quidem repudiandae modi. Itaque consequatur ex est temporibus nesciunt ipsa. Sint dolorum doloribus provident aliquam nobis quos molestias quo. Consequatur ea facilis illo minima. Sit voluptatem saepe voluptas.', '2015-08-20 21:27:28', '2015-08-19 04:25:04');
INSERT INTO `dancers` VALUES ('8', '16', '8', '8', '3', '194', '81', 'Nu am', 'S', 'Italiana', 'Picioare lungi,talie subtire,pometi proeminenti...', '0', '1', '1998', 'Spiru Haret Bucuresti', '1', '1', '1997', 'Cluj -Universitatea Babes-Bolyai - Facultatea de Film si TV', 'IT', 'http://www.Costache.com/delectus-quis-quaerat', 'Modi sed nobis optio quia perferendis quod aspernatur. Molestiae qui aut debitis hic ab eligendi soluta. Debitis optio nihil neque voluptatem aut et voluptatem. Quis non excepturi et. Esse sed sequi alias sed sint. Qui autem amet itaque iure et vero quo. Consequatur optio repudiandae voluptatum. Corporis et aperiam reprehenderit.', 'Eveniet dolores aut consequatur architecto illum ab qui. Iure aut tempore deleniti. Aperiam sed et debitis possimus nostrum. Quo ex doloremque veniam enim minima. Iste odit natus dolore dolores. Earum provident omnis omnis quas. Amet porro non quae voluptatem. Magni dolores vero harum. Dolorem maxime dolor praesentium perferendis. Repellat quod et et rerum voluptatibus et. Ratione nostrum quos illum suscipit aliquam sit illo. Libero et maiores error corporis. Enim laboriosam quasi illo itaque tempore dolor. Eum harum et repellat temporibus.', '2015-09-08 22:06:48', '2015-08-26 05:22:48');
INSERT INTO `dancers` VALUES ('9', '6', '86', '1', '6', '225', '95', 'Nu', 'S', 'Italiana', 'ochi mari, zambet larg, sprancene dese si bine definite', '0', '1', '2004', 'Colegiul National de Arte', '1', '0', '2002', 'Bucuresti - Universitatea Nationala de Arta Teatrala si Cinematografica “I.L.Caragiale” (UNATC)', 'filologie intensiv limba engleza', 'http://Nitu.com/', 'Est sint perspiciatis saepe aut ullam vel dolorum. Magnam aut voluptas totam ullam quasi repellat unde. Sit aut rerum nobis quia. Debitis eveniet sint aut quia laudantium aliquam. Ab sit qui ducimus sunt sapiente officia. Adipisci debitis veniam laborum. Ut neque iure voluptatum facere doloribus unde doloribus.', 'At et veritatis qui dolor earum. Qui quo est adipisci dignissimos voluptatem qui nam quae. Distinctio quia soluta laborum accusamus quia at quis. Pariatur alias quia perferendis minima quia voluptas reprehenderit. Est id assumenda dolor voluptatem voluptas sunt. Debitis illum repellat pariatur veniam. Necessitatibus aut in impedit. Corrupti aut ab iste. Ut iure ipsum maxime odit. Eos dolor dolores doloremque qui nostrum nostrum.', '2015-08-20 09:25:33', '2015-09-02 05:30:55');
INSERT INTO `dancers` VALUES ('10', '7', '155', '2', '6', '135', '117', 'Nu am', 'L', 'Engleza', 'Piele deschisa cu proportii bine definite .', '0', '1', '2007', 'Ivana Chubbuck-Mandragora Movies Acting Studio', '1', '0', '1993', 'Cluj -Universitatea Babes-Bolyai - Facultatea de Film si TV', 'IT', 'https://Pavel.com/ut-minus-ea-magni.html', 'Autem voluptatem consequatur fugit eum. Repellendus inventore vitae doloribus nihil sit sit. Voluptas culpa ullam occaecati. Eos assumenda quidem cumque velit sit. Non amet voluptas modi illum non dolores ut voluptatibus. Repudiandae est quidem eveniet ad qui. Asperiores et dolorum dignissimos quis quisquam quo. Vel eveniet pariatur eligendi id dolorem possimus velit omnis. Inventore aut consequatur eum culpa et quo sit officia. Quia aut velit maxime aut repudiandae cum. Quam dolor non ipsam ad. Exercitationem labore expedita eos.', 'Ea facere aliquid dolorem et molestiae repellendus. Sapiente culpa consequuntur rerum quia atque reiciendis neque quo. Ex qui officiis repellendus et. Sed ab aut cumque qui ipsa aut illum. Et ea velit voluptatem quasi omnis officiis. Voluptatem fugit distinctio facere eius nam adipisci non. Laudantium enim aut voluptatem omnis in perferendis. Sint facilis sit laborum dolorem. Aut suscipit ipsam nihil minima. Voluptates et eos ut. In reiciendis quidem quia nulla iusto voluptatum quia.', '2015-08-22 13:24:18', '2015-08-15 07:47:24');

-- ----------------------------
-- Table structure for employer_types
-- ----------------------------
DROP TABLE IF EXISTS `employer_types`;
CREATE TABLE `employer_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of employer_types
-- ----------------------------
INSERT INTO `employer_types` VALUES ('1', 'Agentie de casting', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `employer_types` VALUES ('2', 'Casa de productie', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `employer_types` VALUES ('3', 'Student', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `employer_types` VALUES ('4', 'Student', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `employer_types` VALUES ('5', 'Amator', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `employer_types` VALUES ('6', 'Shooting Star', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `employer_types` VALUES ('7', 'Famous', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `employer_types` VALUES ('8', 'Walk of fame', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for employers
-- ----------------------------
DROP TABLE IF EXISTS `employers`;
CREATE TABLE `employers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `thumb` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `logo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `company` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `post_code` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `info` varchar(400) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `employers_user_id_index` (`user_id`),
  CONSTRAINT `employers_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of employers
-- ----------------------------
INSERT INTO `employers` VALUES ('1', '26', 'http://lorempixel.com/640/480/?29938', 'http://lorempixel.com/640/480/?42058', 'Corporis dolorem dolores.', 'B-dul. Constantin Brâncuși nr. 3/4, bl. A, et. 09, ap. 96, Mun. Caracal, Brăila, CP 967961', '941091', 'Puiu.net', null, '2015-09-06 01:45:00', '2015-09-06 08:09:16');
INSERT INTO `employers` VALUES ('2', '31', 'http://lorempixel.com/640/480/?20336', 'http://lorempixel.com/640/480/?20721', 'Quaerat ut aut aut.', 'Str. Cireșilor nr. 6A, bl. D, et. 36, ap. 81, Balș, Cluj, CP 597090', '871983', 'Buda.com', null, '2015-08-17 01:47:36', '2015-08-11 20:06:27');
INSERT INTO `employers` VALUES ('3', '11', 'http://lorempixel.com/640/480/?26279', 'http://lorempixel.com/640/480/?44373', 'Omnis voluptatem fugiat at.', 'B-dul. Independenței nr. 5A, bl. C, et. 77, ap. 32, Filiași, Botoșani, CP 819024', '334445', 'Dumitrascu.com', null, '2015-09-08 21:24:47', '2015-08-27 02:13:54');
INSERT INTO `employers` VALUES ('4', '24', 'http://lorempixel.com/640/480/?14315', 'http://lorempixel.com/640/480/?68942', 'Vitae exercitationem qui dolorem alias.', 'Aleea Zidarilor 763, Rădăuți, Dolj, CP 417887', '010677', 'Florea.com', null, '2015-08-25 02:19:56', '2015-08-15 17:58:52');
INSERT INTO `employers` VALUES ('5', '40', 'http://lorempixel.com/640/480/?24399', 'http://lorempixel.com/640/480/?90422', 'Aliquam nulla nulla.', 'Splaiul Traian nr. 2B, bl. C, et. 2, ap. 3, Bălcești, Giurgiu, CP 721921', '940213', 'Toader.com', null, '2015-08-15 07:44:52', '2015-08-19 17:36:19');
INSERT INTO `employers` VALUES ('6', '23', 'http://lorempixel.com/640/480/?93495', 'http://lorempixel.com/640/480/?34073', 'Voluptate autem suscipit.', 'Str. Cireșilor nr. 6A, bl. C, ap. 62, Mun. Corabia, Botoșani, CP 462255', '573927', 'Solomon.biz', null, '2015-08-23 02:02:27', '2015-08-09 16:15:32');
INSERT INTO `employers` VALUES ('7', '15', 'http://lorempixel.com/640/480/?14327', 'http://lorempixel.com/640/480/?72270', 'Enim facere et cupiditate.', 'P-ța Franklin Delano Rosevelt 6B, Mun. Baia de Aramă, Dâmbovița, CP 095432', '405351', 'Dima.org', null, '2015-08-13 13:47:30', '2015-08-17 19:51:20');
INSERT INTO `employers` VALUES ('8', '6', 'http://lorempixel.com/640/480/?15508', 'http://lorempixel.com/640/480/?59387', 'Commodi accusamus vitae qui molestiae.', 'Str. Cloșca nr. 9A, bl. A, sc. D, et. 3, ap. 40, Mun. Târgu Frumos, Brașov, CP 042131', '121248', 'Paduraru.com', null, '2015-08-14 22:00:31', '2015-08-22 02:00:20');
INSERT INTO `employers` VALUES ('9', '44', 'http://lorempixel.com/640/480/?50613', 'http://lorempixel.com/640/480/?49596', 'Et maiores.', 'B-dul. Herculane nr. 1/0, bl. 8, ap. 6, Mun. Pâncota, Sibiu, CP 426567', '452412', 'Ispas.com', null, '2015-09-06 05:54:58', '2015-08-12 22:23:44');
INSERT INTO `employers` VALUES ('10', '29', 'http://lorempixel.com/640/480/?83989', 'http://lorempixel.com/640/480/?17008', 'Veritatis autem ratione.', 'B-dul. Cireșilor 558, Mun. Ghimbav, Bihor, CP 114594', '457887', 'Filimon.com', null, '2015-08-24 13:58:48', '2015-08-16 18:03:59');

-- ----------------------------
-- Table structure for ethnicities
-- ----------------------------
DROP TABLE IF EXISTS `ethnicities`;
CREATE TABLE `ethnicities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=193 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of ethnicities
-- ----------------------------
INSERT INTO `ethnicities` VALUES ('1', 'Afghan', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('2', 'Albanian', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('3', 'Algerian', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('4', 'American', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('5', 'Andorran', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('6', 'Angolan', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('7', 'Antiguans', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('8', 'Argentinean', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('9', 'Armenian', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('10', 'Australian', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('11', 'Austrian', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('12', 'Azerbaijani', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('13', 'Bahamian', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('14', 'Bahraini', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('15', 'Bangladeshi', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('16', 'Barbadian', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('17', 'Barbudans', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('18', 'Batswana', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('19', 'Belarusian', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('20', 'Belgian', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('21', 'Belizean', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('22', 'Beninese', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('23', 'Bhutanese', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('24', 'Bolivian', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('25', 'Bosnian', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('26', 'Brazilian', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('27', 'British', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('28', 'Bruneian', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('29', 'Bulgarian', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('30', 'Burkinabe', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('31', 'Burmese', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('32', 'Burundian', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('33', 'Cambodian', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('34', 'Cameroonian', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('35', 'Canadian', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('36', 'Cape Verdean', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('37', 'Central African', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('38', 'Chadian', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('39', 'Chilean', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('40', 'Chinese', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('41', 'Colombian', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('42', 'Comoran', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('43', 'Congolese', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('44', 'Costa Rican', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('45', 'Croatian', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('46', 'Cuban', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('47', 'Cypriot', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('48', 'Czech', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('49', 'Danish', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('50', 'Djibouti', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('51', 'Dominican', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('52', 'Dutch', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('53', 'East Timorese', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('54', 'Ecuadorean', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('55', 'Egyptian', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('56', 'Emirian', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('57', 'Equatorial Guinean', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('58', 'Eritrean', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('59', 'Estonian', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('60', 'Ethiopian', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('61', 'Fijian', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('62', 'Filipino', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('63', 'Finnish', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('64', 'French', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('65', 'Gabonese', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('66', 'Gambian', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('67', 'Georgian', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('68', 'German', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('69', 'Ghanaian', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('70', 'Greek', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('71', 'Grenadian', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('72', 'Guatemalan', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('73', 'Guinea-Bissauan', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('74', 'Guinean', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('75', 'Guyanese', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('76', 'Haitian', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('77', 'Herzegovinian', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('78', 'Honduran', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('79', 'Hungarian', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('80', 'Icelander', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('81', 'Indian', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('82', 'Indonesian', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('83', 'Iranian', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('84', 'Iraqi', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('85', 'Irish', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('86', 'Israeli', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('87', 'Italian', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('88', 'Ivorian', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('89', 'Jamaican', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('90', 'Japanese', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('91', 'Jordanian', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('92', 'Kazakhstani', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('93', 'Kenyan', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('94', 'Kittian and Nevisian', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('95', 'Kuwaiti', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('96', 'Kyrgyz', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('97', 'Laotian', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('98', 'Latvian', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('99', 'Lebanese', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('100', 'Liberian', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('101', 'Libyan', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('102', 'Liechtensteiner', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('103', 'Lithuanian', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('104', 'Luxembourger', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('105', 'Macedonian', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('106', 'Malagasy', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('107', 'Malawian', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('108', 'Malaysian', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('109', 'Maldivan', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('110', 'Malian', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('111', 'Maltese', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('112', 'Marshallese', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('113', 'Mauritanian', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('114', 'Mauritian', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('115', 'Mexican', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('116', 'Micronesian', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('117', 'Moldovan', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('118', 'Monacan', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('119', 'Mongolian', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('120', 'Moroccan', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('121', 'Mosotho', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('122', 'Motswana', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('123', 'Mozambican', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('124', 'Namibian', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('125', 'Nauruan', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('126', 'Nepalese', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('127', 'New Zealander', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('128', 'Ni-Vanuatu', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('129', 'Nicaraguan', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('130', 'Nigerien', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('131', 'North Korean', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('132', 'Northern Irish', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('133', 'Norwegian', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('134', 'Omani', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('135', 'Pakistani', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('136', 'Palauan', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('137', 'Panamanian', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('138', 'Papua New Guinean', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('139', 'Paraguayan', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('140', 'Peruvian', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('141', 'Polish', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('142', 'Portuguese', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('143', 'Qatari', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('144', 'Romanian', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('145', 'Russian', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('146', 'Rwandan', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('147', 'Saint Lucian', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('148', 'Salvadoran', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('149', 'Samoan', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('150', 'San Marinese', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('151', 'Sao Tomean', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('152', 'Saudi', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('153', 'Scottish', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('154', 'Senegalese', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('155', 'Serbian', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('156', 'Seychellois', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('157', 'Sierra Leonean', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('158', 'Singaporean', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('159', 'Slovakian', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('160', 'Slovenian', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('161', 'Solomon Islander', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('162', 'Somali', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('163', 'South African', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('164', 'South Korean', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('165', 'Spanish', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('166', 'Sri Lankan', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('167', 'Sudanese', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('168', 'Surinamer', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('169', 'Swazi', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('170', 'Swedish', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('171', 'Swiss', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('172', 'Syrian', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('173', 'Taiwanese', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('174', 'Tajik', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('175', 'Tanzanian', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('176', 'Thai', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('177', 'Togolese', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('178', 'Tongan', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('179', 'Trinidadian or Tobagonian', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('180', 'Tunisian', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('181', 'Turkish', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('182', 'Tuvaluan', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('183', 'Ugandan', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('184', 'Ukrainian', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('185', 'Uruguayan', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('186', 'Uzbekistani', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('187', 'Venezuelan', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('188', 'Vietnamese', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('189', 'Welsh', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('190', 'Yemenite', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('191', 'Zambian', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `ethnicities` VALUES ('192', 'Zimbabwean', '2015-09-08 22:29:29', '2015-09-08 22:29:29');

-- ----------------------------
-- Table structure for eyecolours
-- ----------------------------
DROP TABLE IF EXISTS `eyecolours`;
CREATE TABLE `eyecolours` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of eyecolours
-- ----------------------------
INSERT INTO `eyecolours` VALUES ('1', 'Amber', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `eyecolours` VALUES ('2', 'Black', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `eyecolours` VALUES ('3', 'Blue', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `eyecolours` VALUES ('4', 'Hazel', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `eyecolours` VALUES ('5', 'Gray', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `eyecolours` VALUES ('6', 'Green', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `eyecolours` VALUES ('7', 'Violet', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `eyecolours` VALUES ('8', 'Squint', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `eyecolours` VALUES ('9', 'Any', '2015-09-08 22:29:29', '2015-09-08 22:29:29');

-- ----------------------------
-- Table structure for filmcrew_profiles
-- ----------------------------
DROP TABLE IF EXISTS `filmcrew_profiles`;
CREATE TABLE `filmcrew_profiles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of filmcrew_profiles
-- ----------------------------
INSERT INTO `filmcrew_profiles` VALUES ('1', 'DOP/Camera', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `filmcrew_profiles` VALUES ('2', 'Costumes & Art Department', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `filmcrew_profiles` VALUES ('3', 'Lighting Department', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `filmcrew_profiles` VALUES ('4', 'Editing & Color Grading', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `filmcrew_profiles` VALUES ('5', 'TV Directors', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `filmcrew_profiles` VALUES ('6', 'Sound', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `filmcrew_profiles` VALUES ('7', '3D & Animation', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `filmcrew_profiles` VALUES ('8', 'Screenwriters/Script', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `filmcrew_profiles` VALUES ('9', 'Reporters', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `filmcrew_profiles` VALUES ('10', '1st AD', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `filmcrew_profiles` VALUES ('11', 'PA', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `filmcrew_profiles` VALUES ('12', 'Game Design', '2015-09-08 22:29:29', '2015-09-08 22:29:29');

-- ----------------------------
-- Table structure for filmcrews
-- ----------------------------
DROP TABLE IF EXISTS `filmcrews`;
CREATE TABLE `filmcrews` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `ethnicity_id` int(10) unsigned NOT NULL,
  `accent` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `spoken_languages` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `profesional_studies` tinyint(1) DEFAULT NULL,
  `speciality_studies` tinyint(1) DEFAULT NULL,
  `speciality_year` smallint(5) unsigned DEFAULT NULL,
  `speciality_institution` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `faculty` tinyint(1) DEFAULT NULL,
  `faculty_student` tinyint(1) DEFAULT NULL,
  `faculty_year` smallint(5) unsigned DEFAULT NULL,
  `faculty_institution` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `faculty_domain` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `info` text COLLATE utf8_unicode_ci,
  `production_types` text COLLATE utf8_unicode_ci,
  `role_abroad` tinyint(1) DEFAULT NULL,
  `unpaid_job` tinyint(1) DEFAULT NULL,
  `production_house` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `profile_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `filmcrews_user_id_index` (`user_id`),
  KEY `filmcrews_ethnicity_id_index` (`ethnicity_id`),
  KEY `filmcrews_profile_id_index` (`profile_id`),
  CONSTRAINT `filmcrews_ethnicity_id_foreign` FOREIGN KEY (`ethnicity_id`) REFERENCES `ethnicities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `filmcrews_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of filmcrews
-- ----------------------------
INSERT INTO `filmcrews` VALUES ('1', '46', '58', 'Ardelean', 'Engleza', '0', '1', '1988', 'Universitatea Nationala de Arta Teatrala si Cinematografica Ion Luca Caragiale Bucuresti', '1', '1', '1986', 'Cluj -Universitatea Babes-Bolyai - Facultatea de Film si TV', 'IT', 'http://Catana.com/quae-in-eum-ut-maiores', 'Numquam commodi quis sed et qui sunt in. Consequatur aut ex unde eos. Veritatis quibusdam corrupti veritatis magni qui qui ab. Maxime et sunt dolores. Sunt suscipit omnis a rem. Perspiciatis voluptatum repellendus qui qui nam repellendus. Praesentium consequatur in consectetur iusto delectus nobis. Perferendis aspernatur accusantium nulla accusantium. Libero quam officia et qui doloribus. Autem voluptatem id illo placeat corporis ab similique. Qui nihil cupiditate est qui. Repellat ut et nulla exercitationem vitae. Qui quia aut quidem saepe. Ut voluptatem at esse quod rerum aspernatur. Expedita ullam voluptas corrupti voluptates.', 'Delectus repellat veritatis ipsam neque labore. Quis aut cupiditate maiores aut. Hic assumenda omnis quidem. Temporibus voluptatem et veniam quo pariatur est ea. Est rem fugiat et inventore perferendis dolorem dolorum maxime. Qui repudiandae aut culpa et iure sunt consectetur omnis. Et inventore nobis tenetur reiciendis molestias.', '0', '0', 'antena3/bgmproduction', '2015-08-29 04:20:50', '2015-08-10 04:30:23', '0');
INSERT INTO `filmcrews` VALUES ('2', '41', '109', 'fara accent', 'Franceza', '1', '0', '1996', 'Ivana Chubbuck-Mandragora Movies Acting Studio', '0', '1', '2012', 'Cluj -Universitatea Babes-Bolyai - Facultatea de Film si TV', 'actorie', 'http://Radoi.biz/quia-facere-quis-tempore-nes', 'Necessitatibus praesentium facere quaerat animi dolorem velit ullam cumque. Sapiente facere culpa quos id. Dolor quia repudiandae consequatur explicabo deserunt sed modi. Earum ut sapiente aspernatur. Qui corporis exercitationem vero quia eum. Corporis consequatur debitis et aut est qui. Adipisci voluptatibus qui sed minima voluptatem quo occaecati repellendus. Minima nostrum et velit iure quaerat id.', 'Culpa et quia sed consectetur sit. Quis nesciunt dignissimos asperiores nemo est cupiditate. Ut voluptatibus incidunt et et. Adipisci architecto veniam fugiat quia ut. Laborum et numquam necessitatibus debitis. Voluptatem corporis ratione accusantium omnis. Numquam quisquam facilis officia voluptatem atque quisquam qui. Alias perspiciatis odit saepe dolor aut nihil. Voluptas dolores eaque ut asperiores nihil. Quia aut rerum commodi omnis eos commodi distinctio est. Sed et eligendi molestiae. Rerum sunt voluptatem et autem.', '0', '1', 'Alina Crisu Make up', '2015-08-20 00:17:03', '2015-09-05 12:33:08', '0');
INSERT INTO `filmcrews` VALUES ('3', '1', '42', 'Romana', 'Engleza', '1', '0', '2015', 'Ivana Chubbuck-Mandragora Movies Acting Studio', '1', '1', '1998', 'Facultatea de Informatica', 'filologie intensiv limba engleza', 'https://Nicolescu.net/sint-optio-quo-cumque-a', 'Velit minima ipsum labore error reprehenderit. Odio adipisci voluptas in aspernatur alias numquam delectus. Incidunt qui neque et iusto laudantium aliquam consequatur. Ipsum ad sint accusamus iure. Eum sunt eum et omnis. Vel porro ipsam laborum sint. Beatae fuga omnis assumenda laboriosam. Quos sit asperiores sed nam et et impedit. Quasi suscipit repellat sit hic sunt occaecati nam. Beatae est non dolorem voluptates quas omnis saepe. Debitis nobis voluptatem sit dolores illum quisquam quis. Et et repellat eum quis sint praesentium.', 'Sapiente atque quas est dolor ut. Aut optio tenetur quibusdam rem quos est illum. In nostrum ipsum corrupti quia ea voluptatibus. Id qui voluptatem molestiae at delectus quidem fuga. Voluptatem libero sit nemo id tenetur qui error. Blanditiis iure enim unde sint tempora. Soluta quibusdam facilis qui consectetur aut necessitatibus. Possimus rerum quae neque sapiente nesciunt.', '1', '1', 'Asociatia Filmtett', '2015-09-04 05:15:28', '2015-09-08 02:03:31', '0');
INSERT INTO `filmcrews` VALUES ('4', '5', '180', 'Oltean', 'Franceza', '1', '1', '1997', 'Mandragora Movies Film Academy', '1', '1', '2001', 'Bucuresti - Universitatea Nationala de Arta Teatrala si Cinematografica “I.L.Caragiale” (UNATC)', 'actorie', 'https://www.Matei.net/dicta-ut-ut-iure-provid', 'Earum ut repudiandae quibusdam eius nihil beatae molestiae. Perferendis et cupiditate consequatur iure molestiae. Et culpa non rem minus temporibus fuga. Debitis nulla maiores sed fugit expedita. Tenetur rerum dolorem odio quos consequatur est laborum. Aut voluptatum aut sunt quidem voluptatum alias rerum. Eveniet officiis eius atque itaque est nihil molestias.', 'Labore libero laboriosam quos maxime quasi et. Tempora pariatur numquam voluptatum adipisci nesciunt et cupiditate. Qui perspiciatis rem facere aperiam omnis. Voluptates nesciunt eum voluptatibus et. Cumque cumque magnam repudiandae ipsum. Voluptatum eius aut repellat quidem fugit quibusdam esse. Voluptates ipsam necessitatibus recusandae necessitatibus dicta voluptatem aut. Perferendis qui quis quia rem consequatur nemo. Animi maiores ad dignissimos delectus iure incidunt est numquam. Dolorum nihil necessitatibus et delectus. Debitis eum in reiciendis eligendi. Voluptas vel iusto velit rem fugiat. Consequuntur dignissimos modi enim alias occaecati. Culpa tempore laboriosam est.', '1', '1', 'FreeLancer', '2015-08-12 11:04:38', '2015-08-24 02:47:34', '0');
INSERT INTO `filmcrews` VALUES ('5', '4', '109', 'Moldovean', 'Engleza', '0', '1', '2013', 'Universitatea Nationala de Arta Teatrala si Cinematografica Ion Luca Caragiale Bucuresti', '1', '1', '2003', 'Bucuresti - Universitatea Nationala de Arta Teatrala si Cinematografica “I.L.Caragiale” (UNATC)', 'filologie intensiv limba engleza', 'http://www.Paduraru.com/nisi-iure-natus-earum', 'Quae ut expedita omnis nesciunt aut nobis. Qui et ut quaerat. Cum qui harum quia est quia dolor cumque. Iste inventore qui aut cupiditate quis. Quae sint voluptatem odit qui et quo. Culpa est et esse. Cumque omnis fugit ut doloribus unde voluptatum. Dolores dolores dolorem atque. Fugit deserunt voluptas quo dignissimos optio fugit earum. Aspernatur provident a et quia accusamus impedit. Perferendis beatae expedita quo vel odio. Nam dolorum voluptate placeat cumque ea.', 'Quasi quibusdam provident dolores eligendi id tempora. Et autem laborum qui nisi fugit illo. Sunt molestiae et doloremque voluptatibus ut. Aut ipsa pariatur voluptate veniam alias. Nihil dolores tempore qui qui consequuntur maxime neque. Et a impedit ab voluptas voluptates nemo. Adipisci quia quam enim aut et.', '1', '0', 'PROTV, ZERO PRODUCTION, ART TV PRODUCTION, PAPRIKA LATINO, ANTENA 1', '2015-08-27 15:55:09', '2015-08-12 00:28:59', '0');
INSERT INTO `filmcrews` VALUES ('6', '29', '88', 'Moldovean', 'Italiana', '0', '1', '2012', 'Mandragora Movies Film Academy', '1', '0', '2012', 'Bucuresti - Universitatea Hyperion - Facultatea de Arte', 'Facultatea de Teatru, Specializarea Papusi si Marionete', 'http://www.Stefanescu.biz/dolores-rerum-conse', 'Et optio beatae tenetur rerum explicabo nobis cum iste. Dolorem ullam odit impedit voluptas quis reprehenderit. Molestias qui labore incidunt rerum est dolorem. Ut ullam vitae labore. Possimus eius qui odit ipsum. Quaerat laboriosam tenetur praesentium in. Quam non nisi fugiat eos.', 'Quibusdam aut quia in et qui. Omnis aut sit ut aut libero beatae atque. Quo soluta quaerat maiores ea quibusdam vero. Aut error voluptatem eos et libero. Harum aspernatur ducimus et et enim non et. Voluptate ab quos quia possimus. Suscipit voluptas assumenda ipsum at ipsum vel voluptatem. Minus dolorem sed aliquam doloribus saepe ex enim.', '0', '0', 'PROTV, ZERO PRODUCTION, ART TV PRODUCTION, PAPRIKA LATINO, ANTENA 1', '2015-08-31 07:20:11', '2015-08-12 01:50:03', '0');
INSERT INTO `filmcrews` VALUES ('7', '10', '87', 'Ardelean', 'Engleza', '1', '1', '1992', 'Spiru Haret Bucuresti', '0', '1', '2014', 'Bucuresti - Universitatea Hyperion - Facultatea de Arte', 'Facultatea de Teatru, Specializarea Papusi si Marionete', 'http://Cornea.com/ipsum-error-quod-quo.html', 'Esse rerum fugit dolorum aut vel officiis cum. Ab optio et maiores voluptatem. Voluptatibus iure animi voluptatem et id laborum odio. Est quasi qui consequatur laborum perspiciatis unde. Voluptatem rem asperiores blanditiis voluptatem est corrupti. Placeat optio enim voluptates. Amet ipsum expedita iure sunt impedit iste tenetur molestiae. Enim sunt nesciunt assumenda excepturi illo. Aut ex dolorem voluptatem consequuntur alias. Quod rerum quae consequatur et ut ad. Est reiciendis molestiae nemo perferendis et quae.', 'Quasi molestiae aut iusto ut. Hic tempora fugit sit molestiae at. Voluptas quibusdam reiciendis et earum. Aut eligendi harum sapiente voluptas sint et quam. Mollitia harum consequuntur cupiditate sunt est. Optio facilis ut facilis pariatur optio. Soluta dolorem id illo nostrum aliquam. Odio aut voluptatem dolorum corporis eum quaerat. Suscipit omnis consequuntur earum sed expedita porro.', '1', '1', 'antena3/bgmproduction', '2015-08-12 22:06:28', '2015-08-15 18:47:07', '0');
INSERT INTO `filmcrews` VALUES ('8', '26', '51', 'Oltean', 'Italiana', '1', '0', '1989', 'Universitatea Nationala de Arta Teatrala si Cinematografica Ion Luca Caragiale Bucuresti', '0', '1', '2016', 'Cluj -Universitatea Babes-Bolyai - Facultatea de Film si TV', 'IT', 'http://www.Catana.com/dolor-laudantium-magnam', 'Numquam quis sint velit iste voluptas. Numquam nemo consequuntur modi in. Beatae molestiae id et eos iusto est. Occaecati eos numquam exercitationem et. Sed quibusdam dicta architecto debitis minima. Laborum corporis libero aspernatur omnis animi quae. Saepe ea dolor dicta impedit dolore labore. Consequatur mollitia labore quis quibusdam explicabo necessitatibus. Fugit ullam est beatae quibusdam qui et maxime. Sapiente provident sed fuga dolor. Quaerat autem quasi suscipit voluptas nostrum. Animi enim quo veritatis maiores assumenda quibusdam. Cumque veniam vel at et. Quasi officiis dolores explicabo et.', 'Distinctio sunt nisi rerum explicabo. Aut veritatis beatae vero. Occaecati qui quis ipsa quia nesciunt praesentium accusamus. Quibusdam alias dolor consequatur sed quos. Voluptatem quo perspiciatis maiores. Qui laborum est enim dignissimos atque eos voluptate. Expedita deserunt omnis aut deserunt. Animi hic rerum vitae eos voluptas qui itaque nisi. Voluptate a atque quia tenetur. Aliquid voluptatem quo enim sint et error.', '1', '0', 'antena3/bgmproduction', '2015-09-03 16:00:44', '2015-08-25 08:46:10', '0');
INSERT INTO `filmcrews` VALUES ('9', '2', '147', 'Nu am', 'Italiana', '0', '1', '2002', 'Colegiul National de Arte', '0', '0', '2013', 'Cluj -Universitatea Babes-Bolyai - Facultatea de Film si TV', 'Cinema and Television', 'http://www.Dumitrescu.info/', 'Non accusantium minima laudantium nesciunt. Dolor perspiciatis aut voluptas optio. Labore maiores facere quam. Laborum blanditiis esse sunt nisi fugiat. Atque quis nulla in aspernatur. Voluptatibus porro rem qui ducimus sapiente omnis quo. Mollitia consectetur consequatur alias aut. Deleniti nulla culpa eligendi aperiam hic. Similique corrupti perferendis qui adipisci nobis. Incidunt rerum aut corporis. Magni voluptate sunt sunt rerum aperiam dolores error. Quia reiciendis ut eos dignissimos ipsa minima.', 'Doloremque ut illum earum esse veritatis esse voluptatibus. Maxime illo quisquam autem cumque cumque officiis. Voluptatibus illo voluptas quia autem eaque magni. Reprehenderit non eaque eligendi in odit quae vel. Et autem ut quia perspiciatis asperiores. Quo earum temporibus eos dolorem. Molestias aut blanditiis in dolorum. Nostrum quos voluptate nam natus velit doloribus animi.', '1', '1', 'casadefilm', '2015-09-01 07:12:50', '2015-08-15 14:25:23', '0');
INSERT INTO `filmcrews` VALUES ('10', '18', '150', 'fara accent', 'Engleza', '1', '1', '2014', 'Mandragora Movies Film Academy', '1', '0', '1997', 'Bucuresti - Universitatea Hyperion - Facultatea de Arte', 'IT', 'https://Marginean.org/tempore-autem-magnam-vo', 'Ut blanditiis fugiat velit velit unde. Facilis soluta animi est ea illum cumque sit aut. Pariatur ut fugiat accusamus voluptatem. Eos dolore facilis sit natus rerum. Dolores delectus natus non. Tempora non architecto ex omnis aut nulla. Voluptatem non autem illo quo autem quod. Omnis deserunt voluptas necessitatibus fugit laudantium velit reiciendis. Quis rem sed enim minus. Ea eligendi explicabo laborum pariatur doloribus ea fuga.', 'Voluptas iusto officiis praesentium corporis qui. Corrupti id sunt rem facilis voluptatem vel et. Sapiente provident ut sit corrupti. Quidem ducimus fugit id non soluta error. Est neque aut odio expedita quis. Cumque sit aut et in consectetur possimus ab et. Ab sapiente voluptatem officia at delectus. Facere enim dolore et natus. Qui incidunt asperiores omnis et.', '1', '1', 'antena3/bgmproduction', '2015-09-04 00:33:07', '2015-08-18 07:01:15', '0');

-- ----------------------------
-- Table structure for haircolours
-- ----------------------------
DROP TABLE IF EXISTS `haircolours`;
CREATE TABLE `haircolours` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of haircolours
-- ----------------------------
INSERT INTO `haircolours` VALUES ('1', 'White', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `haircolours` VALUES ('2', 'Black', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `haircolours` VALUES ('3', 'Brown', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `haircolours` VALUES ('4', 'Blond', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `haircolours` VALUES ('5', 'Red', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `haircolours` VALUES ('6', 'Gray', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `haircolours` VALUES ('7', 'Any', '2015-09-08 22:29:29', '2015-09-08 22:29:29');

-- ----------------------------
-- Table structure for images
-- ----------------------------
DROP TABLE IF EXISTS `images`;
CREATE TABLE `images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `thumb` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `images_user_id_index` (`user_id`),
  CONSTRAINT `images_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of images
-- ----------------------------
INSERT INTO `images` VALUES ('1', '17', 'http://lorempixel.com/640/480/?93120', 'http://lorempixel.com/640/480/?69572', '2015-08-30 10:32:47', '2015-08-23 09:07:27');
INSERT INTO `images` VALUES ('2', '6', 'http://lorempixel.com/640/480/?89986', 'http://lorempixel.com/640/480/?12297', '2015-08-09 12:38:01', '2015-09-03 19:21:15');
INSERT INTO `images` VALUES ('3', '22', 'http://lorempixel.com/640/480/?83972', 'http://lorempixel.com/640/480/?11016', '2015-08-18 00:57:03', '2015-08-28 23:08:10');
INSERT INTO `images` VALUES ('4', '49', 'http://lorempixel.com/640/480/?79956', 'http://lorempixel.com/640/480/?22807', '2015-08-14 10:22:03', '2015-09-07 23:34:27');
INSERT INTO `images` VALUES ('5', '33', 'http://lorempixel.com/640/480/?22382', 'http://lorempixel.com/640/480/?37614', '2015-08-22 03:14:19', '2015-08-29 21:28:42');
INSERT INTO `images` VALUES ('6', '21', 'http://lorempixel.com/640/480/?26709', 'http://lorempixel.com/640/480/?47592', '2015-08-21 20:24:54', '2015-08-19 18:09:29');
INSERT INTO `images` VALUES ('7', '10', 'http://lorempixel.com/640/480/?56895', 'http://lorempixel.com/640/480/?25421', '2015-09-04 17:40:06', '2015-09-05 03:04:34');
INSERT INTO `images` VALUES ('8', '32', 'http://lorempixel.com/640/480/?77735', 'http://lorempixel.com/640/480/?61822', '2015-08-26 21:02:23', '2015-08-12 11:30:54');
INSERT INTO `images` VALUES ('9', '12', 'http://lorempixel.com/640/480/?58463', 'http://lorempixel.com/640/480/?92089', '2015-08-09 09:38:52', '2015-08-18 23:25:59');
INSERT INTO `images` VALUES ('10', '45', 'http://lorempixel.com/640/480/?10308', 'http://lorempixel.com/640/480/?37611', '2015-08-31 01:13:23', '2015-08-12 07:20:41');
INSERT INTO `images` VALUES ('11', '38', 'http://lorempixel.com/640/480/?99418', 'http://lorempixel.com/640/480/?73275', '2015-08-26 15:38:02', '2015-08-27 03:40:02');
INSERT INTO `images` VALUES ('12', '3', 'http://lorempixel.com/640/480/?53123', 'http://lorempixel.com/640/480/?25687', '2015-08-25 05:06:52', '2015-08-17 21:59:34');
INSERT INTO `images` VALUES ('13', '25', 'http://lorempixel.com/640/480/?21086', 'http://lorempixel.com/640/480/?61126', '2015-09-07 09:14:43', '2015-08-27 15:53:34');
INSERT INTO `images` VALUES ('14', '18', 'http://lorempixel.com/640/480/?45493', 'http://lorempixel.com/640/480/?69319', '2015-08-27 22:48:38', '2015-08-31 17:42:20');
INSERT INTO `images` VALUES ('15', '7', 'http://lorempixel.com/640/480/?74121', 'http://lorempixel.com/640/480/?84814', '2015-09-04 05:30:25', '2015-08-16 01:54:34');
INSERT INTO `images` VALUES ('16', '4', 'http://lorempixel.com/640/480/?19882', 'http://lorempixel.com/640/480/?30243', '2015-08-24 05:05:20', '2015-08-25 14:09:27');
INSERT INTO `images` VALUES ('17', '16', 'http://lorempixel.com/640/480/?28110', 'http://lorempixel.com/640/480/?89787', '2015-08-29 05:22:10', '2015-08-09 07:15:23');
INSERT INTO `images` VALUES ('18', '47', 'http://lorempixel.com/640/480/?47594', 'http://lorempixel.com/640/480/?22411', '2015-08-30 20:16:33', '2015-08-22 03:32:41');
INSERT INTO `images` VALUES ('19', '19', 'http://lorempixel.com/640/480/?79157', 'http://lorempixel.com/640/480/?84284', '2015-08-17 08:20:23', '2015-08-29 16:40:08');
INSERT INTO `images` VALUES ('20', '33', 'http://lorempixel.com/640/480/?55085', 'http://lorempixel.com/640/480/?94001', '2015-09-08 17:24:57', '2015-08-14 13:48:55');
INSERT INTO `images` VALUES ('21', '1', 'http://lorempixel.com/640/480/?89325', 'http://lorempixel.com/640/480/?48643', '2015-08-23 20:02:53', '2015-08-27 21:14:41');
INSERT INTO `images` VALUES ('22', '5', 'http://lorempixel.com/640/480/?38089', 'http://lorempixel.com/640/480/?81242', '2015-08-25 19:40:45', '2015-08-28 14:57:11');
INSERT INTO `images` VALUES ('23', '46', 'http://lorempixel.com/640/480/?77884', 'http://lorempixel.com/640/480/?64368', '2015-08-15 16:09:41', '2015-08-15 12:46:47');
INSERT INTO `images` VALUES ('24', '48', 'http://lorempixel.com/640/480/?42691', 'http://lorempixel.com/640/480/?76632', '2015-09-03 06:03:19', '2015-08-14 05:41:21');
INSERT INTO `images` VALUES ('25', '30', 'http://lorempixel.com/640/480/?79926', 'http://lorempixel.com/640/480/?51343', '2015-09-03 17:56:43', '2015-08-25 02:04:21');
INSERT INTO `images` VALUES ('26', '45', 'http://lorempixel.com/640/480/?55208', 'http://lorempixel.com/640/480/?19227', '2015-08-22 06:18:02', '2015-09-01 17:12:01');
INSERT INTO `images` VALUES ('27', '24', 'http://lorempixel.com/640/480/?36651', 'http://lorempixel.com/640/480/?39054', '2015-08-24 19:14:09', '2015-08-31 19:26:48');
INSERT INTO `images` VALUES ('28', '6', 'http://lorempixel.com/640/480/?79602', 'http://lorempixel.com/640/480/?66219', '2015-08-27 14:50:10', '2015-08-12 12:40:10');
INSERT INTO `images` VALUES ('29', '4', 'http://lorempixel.com/640/480/?39485', 'http://lorempixel.com/640/480/?22838', '2015-08-28 08:22:28', '2015-08-13 17:16:41');
INSERT INTO `images` VALUES ('30', '29', 'http://lorempixel.com/640/480/?48513', 'http://lorempixel.com/640/480/?40001', '2015-09-06 16:00:10', '2015-08-21 18:57:49');
INSERT INTO `images` VALUES ('31', '20', 'http://lorempixel.com/640/480/?49804', 'http://lorempixel.com/640/480/?51554', '2015-08-25 08:07:11', '2015-08-30 16:08:45');
INSERT INTO `images` VALUES ('32', '20', 'http://lorempixel.com/640/480/?27801', 'http://lorempixel.com/640/480/?98865', '2015-08-12 19:34:04', '2015-09-01 03:33:16');
INSERT INTO `images` VALUES ('33', '35', 'http://lorempixel.com/640/480/?68234', 'http://lorempixel.com/640/480/?11025', '2015-08-25 03:48:40', '2015-09-03 12:47:23');
INSERT INTO `images` VALUES ('34', '41', 'http://lorempixel.com/640/480/?74478', 'http://lorempixel.com/640/480/?28871', '2015-08-31 17:05:30', '2015-08-28 09:21:59');
INSERT INTO `images` VALUES ('35', '19', 'http://lorempixel.com/640/480/?28429', 'http://lorempixel.com/640/480/?84347', '2015-08-18 14:04:58', '2015-08-20 07:00:12');
INSERT INTO `images` VALUES ('36', '17', 'http://lorempixel.com/640/480/?17881', 'http://lorempixel.com/640/480/?89855', '2015-08-31 12:41:52', '2015-09-02 02:54:19');
INSERT INTO `images` VALUES ('37', '11', 'http://lorempixel.com/640/480/?79025', 'http://lorempixel.com/640/480/?46210', '2015-08-12 23:19:37', '2015-09-06 05:53:30');
INSERT INTO `images` VALUES ('38', '28', 'http://lorempixel.com/640/480/?98439', 'http://lorempixel.com/640/480/?11819', '2015-08-31 20:50:34', '2015-08-29 17:59:51');
INSERT INTO `images` VALUES ('39', '49', 'http://lorempixel.com/640/480/?80817', 'http://lorempixel.com/640/480/?45782', '2015-08-10 12:53:53', '2015-08-20 07:34:01');
INSERT INTO `images` VALUES ('40', '23', 'http://lorempixel.com/640/480/?63136', 'http://lorempixel.com/640/480/?80644', '2015-08-23 08:52:30', '2015-08-18 13:48:57');
INSERT INTO `images` VALUES ('41', '22', 'http://lorempixel.com/640/480/?18107', 'http://lorempixel.com/640/480/?92259', '2015-08-18 04:45:11', '2015-08-14 18:03:29');
INSERT INTO `images` VALUES ('42', '37', 'http://lorempixel.com/640/480/?26324', 'http://lorempixel.com/640/480/?13312', '2015-08-28 21:52:03', '2015-08-11 08:10:54');
INSERT INTO `images` VALUES ('43', '25', 'http://lorempixel.com/640/480/?89524', 'http://lorempixel.com/640/480/?33825', '2015-08-31 20:26:04', '2015-08-23 17:12:48');
INSERT INTO `images` VALUES ('44', '46', 'http://lorempixel.com/640/480/?28596', 'http://lorempixel.com/640/480/?80106', '2015-08-26 15:10:19', '2015-08-19 13:24:44');
INSERT INTO `images` VALUES ('45', '4', 'http://lorempixel.com/640/480/?46779', 'http://lorempixel.com/640/480/?62813', '2015-09-07 06:29:51', '2015-09-08 15:30:59');
INSERT INTO `images` VALUES ('46', '31', 'http://lorempixel.com/640/480/?72450', 'http://lorempixel.com/640/480/?76373', '2015-09-06 18:05:11', '2015-08-13 17:10:32');
INSERT INTO `images` VALUES ('47', '49', 'http://lorempixel.com/640/480/?24649', 'http://lorempixel.com/640/480/?35115', '2015-08-28 04:14:58', '2015-08-13 06:50:04');
INSERT INTO `images` VALUES ('48', '19', 'http://lorempixel.com/640/480/?76704', 'http://lorempixel.com/640/480/?35052', '2015-08-16 20:15:29', '2015-08-09 19:36:46');
INSERT INTO `images` VALUES ('49', '28', 'http://lorempixel.com/640/480/?96969', 'http://lorempixel.com/640/480/?82170', '2015-08-23 12:50:43', '2015-09-03 05:43:06');
INSERT INTO `images` VALUES ('50', '18', 'http://lorempixel.com/640/480/?37066', 'http://lorempixel.com/640/480/?51183', '2015-08-22 09:12:45', '2015-09-02 06:27:04');

-- ----------------------------
-- Table structure for job_user
-- ----------------------------
DROP TABLE IF EXISTS `job_user`;
CREATE TABLE `job_user` (
  `job_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `is_chosen` tinyint(1) DEFAULT NULL,
  `is_shortlist` tinyint(1) DEFAULT NULL,
  `is_seen` tinyint(1) DEFAULT NULL,
  `date_applied` datetime DEFAULT NULL,
  `is_accepted` tinyint(1) DEFAULT NULL,
  `is_invite` tinyint(1) DEFAULT NULL,
  `is_declined_employer` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `job_user_job_id_index` (`job_id`),
  KEY `job_user_user_id_index` (`user_id`),
  CONSTRAINT `job_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `job_user_job_id_foreign` FOREIGN KEY (`job_id`) REFERENCES `jobs` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of job_user
-- ----------------------------
INSERT INTO `job_user` VALUES ('15', '35', '0', '0', '1', '2015-08-21 18:21:26', '1', '0', '0', '2015-08-24 13:41:20', '2015-08-21 11:27:16');
INSERT INTO `job_user` VALUES ('26', '16', '1', '0', '0', '2015-08-28 02:29:06', '0', '1', '1', '2015-08-20 22:47:59', '2015-09-06 00:20:48');
INSERT INTO `job_user` VALUES ('23', '48', '1', '1', '1', '2015-08-27 03:35:53', '1', '0', '1', '2015-08-29 13:04:38', '2015-09-04 22:55:23');
INSERT INTO `job_user` VALUES ('17', '8', '0', '1', '0', '2015-08-14 04:46:46', '0', '0', '1', '2015-08-27 04:07:33', '2015-09-07 15:02:05');
INSERT INTO `job_user` VALUES ('27', '27', '0', '1', '0', '2015-08-27 04:31:51', '1', '1', '1', '2015-09-06 21:35:34', '2015-08-13 00:41:55');
INSERT INTO `job_user` VALUES ('27', '19', '0', '0', '1', '2015-08-23 21:42:19', '0', '0', '1', '2015-08-22 06:49:50', '2015-09-08 21:07:02');
INSERT INTO `job_user` VALUES ('8', '37', '0', '0', '1', '2015-09-05 12:19:02', '1', '1', '0', '2015-08-16 21:37:07', '2015-08-18 10:12:06');
INSERT INTO `job_user` VALUES ('23', '35', '1', '0', '1', '2015-08-21 22:54:58', '0', '1', '1', '2015-08-24 14:36:00', '2015-08-28 07:44:28');
INSERT INTO `job_user` VALUES ('25', '6', '0', '1', '1', '2015-08-10 10:19:26', '1', '1', '1', '2015-09-02 11:05:29', '2015-09-02 11:34:10');
INSERT INTO `job_user` VALUES ('8', '17', '1', '0', '1', '2015-08-29 03:38:04', '1', '1', '1', '2015-08-09 19:05:43', '2015-08-09 12:58:22');
INSERT INTO `job_user` VALUES ('20', '50', '1', '0', '1', '2015-08-26 17:50:12', '0', '0', '0', '2015-08-10 07:00:18', '2015-08-25 12:13:02');
INSERT INTO `job_user` VALUES ('5', '17', '0', '0', '1', '2015-08-18 00:09:53', '0', '1', '0', '2015-08-19 13:51:28', '2015-08-29 14:24:04');
INSERT INTO `job_user` VALUES ('11', '5', '0', '0', '1', '2015-08-17 07:42:34', '0', '1', '0', '2015-09-06 09:54:15', '2015-09-06 10:50:48');
INSERT INTO `job_user` VALUES ('21', '16', '0', '0', '0', '2015-08-15 15:27:43', '1', '0', '1', '2015-08-18 19:26:38', '2015-08-23 05:35:55');
INSERT INTO `job_user` VALUES ('22', '50', '0', '0', '0', '2015-08-31 12:13:19', '0', '1', '1', '2015-09-08 13:36:39', '2015-09-05 03:49:17');
INSERT INTO `job_user` VALUES ('24', '4', '1', '0', '0', '2015-08-24 04:19:24', '0', '1', '1', '2015-08-28 09:44:05', '2015-08-18 20:53:41');
INSERT INTO `job_user` VALUES ('16', '12', '1', '0', '1', '2015-08-14 05:38:13', '0', '1', '0', '2015-08-14 07:15:44', '2015-08-13 15:02:30');
INSERT INTO `job_user` VALUES ('17', '43', '1', '1', '1', '2015-08-26 18:02:08', '1', '1', '0', '2015-08-31 11:24:28', '2015-08-31 17:21:53');
INSERT INTO `job_user` VALUES ('21', '34', '1', '0', '0', '2015-08-15 14:22:49', '0', '1', '1', '2015-08-11 04:47:40', '2015-09-08 00:01:37');
INSERT INTO `job_user` VALUES ('12', '18', '0', '1', '1', '2015-08-27 16:54:21', '0', '0', '0', '2015-09-05 06:12:40', '2015-09-05 09:13:31');
INSERT INTO `job_user` VALUES ('11', '48', '1', '1', '0', '2015-08-11 19:50:09', '0', '0', '1', '2015-08-21 04:35:41', '2015-09-02 07:52:58');
INSERT INTO `job_user` VALUES ('8', '39', '0', '1', '0', '2015-08-20 02:20:05', '0', '0', '0', '2015-08-10 09:29:15', '2015-09-05 09:56:05');
INSERT INTO `job_user` VALUES ('23', '42', '0', '1', '0', '2015-08-23 09:06:26', '0', '1', '1', '2015-08-20 03:06:09', '2015-09-06 07:21:28');
INSERT INTO `job_user` VALUES ('12', '3', '0', '0', '0', '2015-08-31 12:15:00', '0', '0', '1', '2015-08-12 05:09:37', '2015-09-02 16:37:16');
INSERT INTO `job_user` VALUES ('14', '10', '0', '1', '1', '2015-08-29 11:42:58', '1', '0', '1', '2015-09-06 06:53:15', '2015-08-15 15:34:57');
INSERT INTO `job_user` VALUES ('15', '41', '1', '1', '1', '2015-08-17 16:30:36', '1', '0', '1', '2015-09-07 14:04:30', '2015-08-13 15:36:05');
INSERT INTO `job_user` VALUES ('23', '22', '0', '1', '0', '2015-08-21 21:55:19', '0', '0', '1', '2015-09-04 02:48:50', '2015-08-29 11:02:58');
INSERT INTO `job_user` VALUES ('9', '30', '1', '0', '0', '2015-08-23 04:29:18', '0', '0', '1', '2015-08-17 20:53:19', '2015-08-24 04:42:45');
INSERT INTO `job_user` VALUES ('19', '25', '0', '0', '0', '2015-08-28 08:41:00', '0', '0', '1', '2015-08-24 19:27:15', '2015-08-23 21:25:26');
INSERT INTO `job_user` VALUES ('10', '40', '1', '0', '1', '2015-08-28 14:09:59', '1', '1', '0', '2015-08-12 00:29:27', '2015-09-03 15:30:02');

-- ----------------------------
-- Table structure for jobs
-- ----------------------------
DROP TABLE IF EXISTS `jobs`;
CREATE TABLE `jobs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `headline` text COLLATE utf8_unicode_ci,
  `production_name` text COLLATE utf8_unicode_ci,
  `producer_name` text COLLATE utf8_unicode_ci,
  `production_type_id` int(10) unsigned DEFAULT NULL,
  `location` text COLLATE utf8_unicode_ci,
  `experience` text COLLATE utf8_unicode_ci,
  `from_date_shooting` date DEFAULT NULL,
  `to_date_shooting` date DEFAULT NULL,
  `from_date_casting` date DEFAULT NULL,
  `to_date_casting` date DEFAULT NULL,
  `profile_id` int(10) unsigned DEFAULT NULL,
  `short_description` text COLLATE utf8_unicode_ci,
  `student_project` tinyint(1) DEFAULT NULL,
  `from_age` int(10) unsigned DEFAULT NULL,
  `to_age` int(10) unsigned DEFAULT NULL,
  `haircolour_id` int(10) unsigned DEFAULT NULL,
  `from_height` int(10) unsigned DEFAULT NULL,
  `to_height` int(10) unsigned DEFAULT NULL,
  `body_shape` text COLLATE utf8_unicode_ci,
  `extra_details` text COLLATE utf8_unicode_ci,
  `payment` tinyint(1) DEFAULT NULL,
  `producer` text COLLATE utf8_unicode_ci,
  `telephone` text COLLATE utf8_unicode_ci,
  `application_method` tinyint(1) DEFAULT NULL,
  `exclusive` tinyint(1) DEFAULT NULL,
  `university` text COLLATE utf8_unicode_ci,
  `tutor_name` text COLLATE utf8_unicode_ci,
  `department` text COLLATE utf8_unicode_ci,
  `year` text COLLATE utf8_unicode_ci,
  `date_added` datetime DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `sex` enum('M','F') COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` text COLLATE utf8_unicode_ci,
  `subcategory` tinyint(1) DEFAULT NULL,
  `guest_visible` tinyint(1) DEFAULT NULL,
  `kids_profile_type` int(10) unsigned DEFAULT NULL,
  `film_profile_type` int(10) unsigned DEFAULT NULL,
  `nudity_required` tinyint(1) DEFAULT NULL,
  `is_complete` tinyint(1) DEFAULT NULL,
  `eye_colour_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `jobs_haircolour_id_foreign` (`haircolour_id`),
  CONSTRAINT `jobs_haircolour_id_foreign` FOREIGN KEY (`haircolour_id`) REFERENCES `haircolours` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of jobs
-- ----------------------------
INSERT INTO `jobs` VALUES ('1', 'Se cauta voice-over pentru reclama', 'Produs pentru ingrijirea parului', null, '18', 'Bucuresti', null, '1991-01-07', '2013-08-16', '2010-11-18', '1988-03-21', '1', 'Dupa cum probabil stiti, Romania Casting Call s-a lansat de curand. A inceput sa creasca usor usor, insa pentru a ajunge la urechile tuturor, are nevoie de o reclama (TV si Radio). Avem nevoie de o voce, insa nu cautam un stil anumite sau un sex. Dorim cat mai multa diversitate, asadar va asteptam!', '0', '28', '39', '1', '162', '186', 'Any', 'Regizare videoclipuri muzicale', '0', 'HaHaHa Video Production', '0740.121.864', '0', '0', '', '', '', null, '2015-08-10 06:56:40', '56', '0', 'M', 'laura.musat10@gmail.com', '1', '1', null, null, '1', '1', '1', '2015-08-25 04:37:38', '2015-09-07 01:04:04');
INSERT INTO `jobs` VALUES ('2', 'Figuratie Scurtmetraj', 'Love building', null, '36', 'Bucuresti', null, '2004-03-31', '1983-01-28', '1991-04-02', '2008-11-21', '2', 'Filmarea a doua spoturi publicitare, unul in data pe 4.11.2014, celalalt pe 5.11.2014. Dat fiind faptul ca proiectul este unul de tip testimonial, numele produsului va fi dezvalui in ziua filmarii.', '1', '27', '50', '4', '160', '187', 'Thin', 'Cautam doi barbati pentru figuratie in scurtmetrajul \"10 ore\", care vor face parte dintr-un board meeting de 4 oameni: o femeie si trei barbati. Rolurile sunt minimale: nicio replica, doar prezenta. Aceste doua personaje, alaturi de inca doua - o femeie   inca un barbat, formeaza board-ul din sala de conferinte, care ii asculta prezentarea dl. Suciu, tatal Mariei. La finalul prezentarii, cei 3 barbati vor hotari daca investesc in proiectul dl Suciu sau nu. Toti 4 sunt oameni de afaceri, imbracati la costum. Vlad Suciu, tatal Mariei va fi jucat de un actor cunoscut in Romania, acest film  fiind o productie romano-britanica, care va participa la majoritatea festivalurilor mari din lume.', '0', 'Laura Musat', '0758080821', '1', '1', '', '', '', null, '2015-08-13 03:40:54', '44', '1', 'F', 'laura.musat10@gmail.com', '0', '0', null, null, '1', '0', '3', '2015-08-27 03:50:59', '2015-08-14 12:45:43');
INSERT INTO `jobs` VALUES ('3', 'Cautam actori', 'Reclama video Romania Casting Call', null, '26', 'Bucuresti', null, '1971-05-11', '1997-02-06', '1990-11-04', '1985-05-24', '4', '10 ore\" urmareste povestea captivanta a Mariei, o fetita de 10-11 ani, care provine dintr-o familie de oameni instariti si ocupati. Tatal ei, un extraordinar om de afaceri in telecomunicatii, are din ce in ce mai putin timp pentru ea, fapt ce ii produce Mariei o mare suferinta. Din lipsa de atentie, dar si din dragoste fata de tatal ei, Maria pleaca de acasa, lasandu-i tatalui sau un bilet, prin care il informeaza de plecarea sa si ii pune la dispozitie 10 ore pentru a o gasi. Astfel, Maria porneste intr-o aventura plina de obstacole, sperand sa il faca pe tatal sau sa realizeze importanta afectiunii si atentiei unui parinte pentru un copil care nu isi doreste jucarii si tablete, pe cat de mult isi doreste un tata aproape de ea. O emotionanta drama, cu elemente comice, care transmite un mesaj actual si simplu, menit sa deschida ochii parintilor care isi neglijeaza copiii din cauza job-ului.', '0', '14', '53', '1', '167', '177', 'Any', 'O voce placuta, dar mai ales convingatoare. Nu conteaza sexul.', '1', 'Laura', '0758080821', '0', '0', '', '', '', null, '2015-08-22 07:43:03', '8', '0', 'F', 'laura.musat@romaniacastingcall.com', '0', '0', null, null, '1', '1', '1', '2015-08-21 09:26:01', '2015-08-29 15:21:45');
INSERT INTO `jobs` VALUES ('4', 'FAMILII REALE', 'Produs pentru ingrijirea parului', null, '33', 'Bucuresti', null, '1981-06-11', '2006-12-15', '1994-10-25', '1999-05-25', '1', 'Dupa cum probabil stiti, Romania Casting Call s-a lansat de curand. A inceput sa creasca usor usor, insa pentru a ajunge la urechile tuturor, are nevoie de o reclama (TV si Radio). Avem nevoie de o voce, insa nu cautam un stil anumite sau un sex. Dorim cat mai multa diversitate, asadar va asteptam!', '0', '24', '64', '4', '172', '169', 'Fat', 'Cautam familii reale: mama- tata  copil pentru filmarea unor spoturi publicitare pentru un produs de ingrijire a parului, Produsul este dezvaluit doar in ziua filmarii, dat fiind faptul ca este un proiect de tip testimonial si se vrea reactii reale. Mama trebuie sa aiba varsta cuprinsa intre 30-40 ani, sa aiba parul bogat, zambet frumos si sa nu mai fi aparut vreodata in reclame. Copilul ar trebui sa aiba intre 3 si 6 ani, sa nu fie rusinos si sa nu mai fi aparut vreodata in vreo reclama. Pentru tata e foarte important sa aiba par si sa fie carismatic. Pentru detalii, va rog sa trimiteti mail la adresa rionita@sagafilm.ro', '0', 'Laura Musat', '0740.121.864', '0', '0', '', '', '', null, '2015-09-03 04:06:15', '15', '0', 'M', 'laura@office.ro', '1', '0', null, null, '0', '0', '1', '2015-08-09 19:47:43', '2015-08-22 21:46:46');
INSERT INTO `jobs` VALUES ('5', 'Cautam actori', 'Love building', null, '16', 'Bucuresti', null, '1997-10-15', '1984-06-28', '1994-03-26', '2009-12-13', '8', 'Castalia Productions se lanseaza in aceasta vara cu o productie romano-britanica, si anume scurtmetrajul intitulat \"10 ore\" . Echipa este formata numai din oameni profesionisti, cunoscuti pe piata romaneasca, si completata de prezenta catorva studenti britanici de la facultati de profil londoneze. \"10 ore\" urmareste povestea captivanta a Mariei, o fetita de 10-11 ani, care provine dintr-o familie de oameni instariti si ocupati. Tatal ei, un extraordinar om de afaceri in telecomunicatii, are din ce in ce mai putin timp pentru ea, fapt ce ii produce Mariei o mare suferinta. Din lipsa de atentie, dar si din dragoste fata de tatal ei, Maria porneste intr-o aventura plina de obstacole, sperand sa il faca pe tatal sau sa realizeze importanta afectiunii si atentiei unui parinte pentru un copil care nu isi doreste jucarii si tablete, pe cat de mult isi doreste un tata aproape de ea. O emotionanta drama, cu elemente comice, care transmite un mesaj actual si simplu, menit sa deschida ochii parintilor care isi neglijeaza copiii din cauza job-ului. Acest scurtmetraj va fi trimis la toate festivalurile importante, printre ele numarandu-se si Cannes / Sundance / BFI etc.', '1', '28', '61', '3', '167', '188', 'Thin', 'Regizare videoclipuri muzicale', '0', 'HaHaHa Video Production', '0758080821', '1', '0', '', '', '', null, '2015-09-02 11:35:11', '16', '1', 'M', 'pr@hahahaproduction.com', '1', '1', null, null, '1', '1', '7', '2015-08-13 20:18:39', '2015-08-14 03:17:12');
INSERT INTO `jobs` VALUES ('6', 'Cautam regizori', 'HaHaHa Video Production', null, '35', 'Bucuresti', null, '1996-01-27', '1977-09-05', '1993-07-30', '2002-09-07', '3', 'Filmarea a doua spoturi publicitare, unul in data pe 4.11.2014, celalalt pe 5.11.2014. Dat fiind faptul ca proiectul este unul de tip testimonial, numele produsului va fi dezvalui in ziua filmarii.', '1', '28', '64', '1', '161', '168', 'Thin', 'Cautam doi barbati pentru figuratie in scurtmetrajul \"10 ore\", care vor face parte dintr-un board meeting de 4 oameni: o femeie si trei barbati. Rolurile sunt minimale: nicio replica, doar prezenta. Aceste doua personaje, alaturi de inca doua - o femeie   inca un barbat, formeaza board-ul din sala de conferinte, care ii asculta prezentarea dl. Suciu, tatal Mariei. La finalul prezentarii, cei 3 barbati vor hotari daca investesc in proiectul dl Suciu sau nu. Toti 4 sunt oameni de afaceri, imbracati la costum. Vlad Suciu, tatal Mariei va fi jucat de un actor cunoscut in Romania, acest film  fiind o productie romano-britanica, care va participa la majoritatea festivalurilor mari din lume.', '1', 'SAGA FILM', '0740.121.864', '0', '0', '', '', '', null, '2015-08-17 05:08:41', '33', '1', 'F', 'laura@office.ro', '0', '1', null, null, '1', '0', '5', '2015-08-11 00:23:58', '2015-09-07 18:54:53');
INSERT INTO `jobs` VALUES ('7', 'Personaj Principal / Copil - Scurtmetraj', 'Reclama video Romania Casting Call', null, '41', 'Bucuresti', null, '2014-07-02', '1977-09-16', '1997-03-02', '2001-09-16', '4', 'Castalia Productions se lanseaza in aceasta vara cu o productie romano-britanica, si anume scurtmetrajul intitulat \"10 ore\" . Echipa este formata numai din oameni profesionisti, cunoscuti pe piata romaneasca, si completata de prezenta catorva studenti britanici de la facultati de profil londoneze. \"10 ore\" urmareste povestea captivanta a Mariei, o fetita de 10-11 ani, care provine dintr-o familie de oameni instariti si ocupati. Tatal ei, un extraordinar om de afaceri in telecomunicatii, are din ce in ce mai putin timp pentru ea, fapt ce ii produce Mariei o mare suferinta. Din lipsa de atentie, dar si din dragoste fata de tatal ei, Maria porneste intr-o aventura plina de obstacole, sperand sa il faca pe tatal sau sa realizeze importanta afectiunii si atentiei unui parinte pentru un copil care nu isi doreste jucarii si tablete, pe cat de mult isi doreste un tata aproape de ea. O emotionanta drama, cu elemente comice, care transmite un mesaj actual si simplu, menit sa deschida ochii parintilor care isi neglijeaza copiii din cauza job-ului. Acest scurtmetraj va fi trimis la toate festivalurile importante, printre ele numarandu-se si Cannes / Sundance / BFI etc.', '0', '26', '39', '2', '173', '184', 'Thin', 'O voce placuta, dar mai ales convingatoare. Nu conteaza sexul.', '0', 'Laura', '0740.121.864', '1', '0', '', '', '', null, '2015-09-07 16:08:27', '45', '1', 'F', 'pr@hahahaproduction.com', '0', '0', null, null, '1', '0', '8', '2015-08-27 16:36:00', '2015-08-24 08:40:31');
INSERT INTO `jobs` VALUES ('8', 'Cautam regizori', 'HaHaHa Video Production', null, '15', 'Bucuresti', null, '2009-10-15', '1971-04-30', '1970-10-09', '1991-12-23', '2', 'Vrei sa participi la cele mai tari proiecte video si sa fii alaturi de artistii tai preferati? HaHaHa Video Production cauta absolventi de actorie sau studenti la o facultate de profil care vor sa participe la viitoarele proiecte video realizate pentru cei mai cunoscuti artisti romani si nu numai.', '0', '20', '58', '2', '161', '166', 'Fat', 'O voce placuta, dar mai ales convingatoare. Nu conteaza sexul.', '1', 'Castalia Productions', '0758080821', '0', '1', '', '', '', null, '2015-09-04 07:29:11', '44', '0', 'M', 'rionita@sagafilm.ro', '1', '0', null, null, '1', '0', '6', '2015-09-02 16:49:18', '2015-09-04 12:00:17');
INSERT INTO `jobs` VALUES ('9', 'Personaj Principal / Copil - Scurtmetraj', 'Reclama video Romania Casting Call', null, '20', 'Bucuresti', null, '1976-11-11', '2012-10-23', '1972-10-22', '2014-06-08', '6', 'Dupa cum probabil stiti, Romania Casting Call s-a lansat de curand. A inceput sa creasca usor usor, insa pentru a ajunge la urechile tuturor, are nevoie de o reclama (TV si Radio). Avem nevoie de o voce, insa nu cautam un stil anumite sau un sex. Dorim cat mai multa diversitate, asadar va asteptam!', '0', '20', '65', '3', '160', '168', '', 'O voce placuta, dar mai ales convingatoare. Nu conteaza sexul.', '0', 'Laura', '0740.121.864', '0', '1', '', '', '', null, '2015-08-27 09:59:33', '38', '1', 'F', 'office@romaniacastingcall.com', '1', '1', null, null, '0', '0', '3', '2015-08-20 04:28:03', '2015-09-02 01:59:24');
INSERT INTO `jobs` VALUES ('10', 'Personaj Principal / Copil - Scurtmetraj', 'Love building', null, '45', 'Bucuresti', null, '1991-12-02', '1981-02-19', '2010-09-30', '1978-03-15', '7', 'Filmarea a doua spoturi publicitare, unul in data pe 4.11.2014, celalalt pe 5.11.2014. Dat fiind faptul ca proiectul este unul de tip testimonial, numele produsului va fi dezvalui in ziua filmarii.', '0', '10', '60', '2', '173', '190', 'Fat', 'Regizare videoclipuri muzicale', '0', 'Laura Musat', '0758080821', '0', '1', '', '', '', null, '2015-09-03 03:41:46', '48', '0', 'F', 'laura@office.ro', '0', '1', null, null, '0', '0', '7', '2015-08-28 10:14:16', '2015-08-15 04:30:45');
INSERT INTO `jobs` VALUES ('11', 'Ulala', 'HaHaHa Video Production', null, '3', 'Bucuresti', null, '2009-03-20', '2008-06-14', '1988-08-29', '2010-02-16', '9', '10 ore\" urmareste povestea captivanta a Mariei, o fetita de 10-11 ani, care provine dintr-o familie de oameni instariti si ocupati. Tatal ei, un extraordinar om de afaceri in telecomunicatii, are din ce in ce mai putin timp pentru ea, fapt ce ii produce Mariei o mare suferinta. Din lipsa de atentie, dar si din dragoste fata de tatal ei, Maria pleaca de acasa, lasandu-i tatalui sau un bilet, prin care il informeaza de plecarea sa si ii pune la dispozitie 10 ore pentru a o gasi. Astfel, Maria porneste intr-o aventura plina de obstacole, sperand sa il faca pe tatal sau sa realizeze importanta afectiunii si atentiei unui parinte pentru un copil care nu isi doreste jucarii si tablete, pe cat de mult isi doreste un tata aproape de ea. O emotionanta drama, cu elemente comice, care transmite un mesaj actual si simplu, menit sa deschida ochii parintilor care isi neglijeaza copiii din cauza job-ului.', '0', '29', '35', '4', '172', '176', 'Any', 'O voce placuta, dar mai ales convingatoare. Nu conteaza sexul.', '0', 'Laura Musat', '0740.121.864', '0', '0', '', '', '', null, '2015-08-12 01:36:56', '16', '0', 'F', 'laura.musat@romaniacastingcall.com', '1', '0', null, null, '0', '0', '3', '2015-09-08 20:48:45', '2015-08-17 01:57:58');
INSERT INTO `jobs` VALUES ('12', 'Cautam regizori', 'Love building', null, '43', 'Bucuresti', null, '1973-02-19', '2004-12-25', '2002-12-20', '1992-12-21', '8', 'Castalia Productions se lanseaza in aceasta vara cu o productie romano-britanica, si anume scurtmetrajul intitulat \"10 ore\" . Echipa este formata numai din oameni profesionisti, cunoscuti pe piata romaneasca, si completata de prezenta catorva studenti britanici de la facultati de profil londoneze. \"10 ore\" urmareste povestea captivanta a Mariei, o fetita de 10-11 ani, care provine dintr-o familie de oameni instariti si ocupati. Tatal ei, un extraordinar om de afaceri in telecomunicatii, are din ce in ce mai putin timp pentru ea, fapt ce ii produce Mariei o mare suferinta. Din lipsa de atentie, dar si din dragoste fata de tatal ei, Maria porneste intr-o aventura plina de obstacole, sperand sa il faca pe tatal sau sa realizeze importanta afectiunii si atentiei unui parinte pentru un copil care nu isi doreste jucarii si tablete, pe cat de mult isi doreste un tata aproape de ea. O emotionanta drama, cu elemente comice, care transmite un mesaj actual si simplu, menit sa deschida ochii parintilor care isi neglijeaza copiii din cauza job-ului. Acest scurtmetraj va fi trimis la toate festivalurile importante, printre ele numarandu-se si Cannes / Sundance / BFI etc.', '0', '19', '35', '6', '173', '176', '', 'Cautam familii reale: mama- tata  copil pentru filmarea unor spoturi publicitare pentru un produs de ingrijire a parului, Produsul este dezvaluit doar in ziua filmarii, dat fiind faptul ca este un proiect de tip testimonial si se vrea reactii reale. Mama trebuie sa aiba varsta cuprinsa intre 30-40 ani, sa aiba parul bogat, zambet frumos si sa nu mai fi aparut vreodata in reclame. Copilul ar trebui sa aiba intre 3 si 6 ani, sa nu fie rusinos si sa nu mai fi aparut vreodata in vreo reclama. Pentru tata e foarte important sa aiba par si sa fie carismatic. Pentru detalii, va rog sa trimiteti mail la adresa rionita@sagafilm.ro', '1', 'HaHaHa Video Production', '0740300290', '1', '0', '', '', '', null, '2015-08-20 23:21:50', '44', '0', 'M', 'pr@hahahaproduction.com', '1', '0', null, null, '1', '0', '3', '2015-08-16 01:09:55', '2015-08-16 11:14:25');
INSERT INTO `jobs` VALUES ('13', 'Ulala', 'Love building', null, '13', 'Bucuresti', null, '2006-12-19', '2008-09-17', '2008-08-19', '2003-09-04', '2', 'Dupa cum probabil stiti, Romania Casting Call s-a lansat de curand. A inceput sa creasca usor usor, insa pentru a ajunge la urechile tuturor, are nevoie de o reclama (TV si Radio). Avem nevoie de o voce, insa nu cautam un stil anumite sau un sex. Dorim cat mai multa diversitate, asadar va asteptam!', '1', '11', '51', '7', '170', '177', 'Any', 'Cautam familii reale: mama- tata  copil pentru filmarea unor spoturi publicitare pentru un produs de ingrijire a parului, Produsul este dezvaluit doar in ziua filmarii, dat fiind faptul ca este un proiect de tip testimonial si se vrea reactii reale. Mama trebuie sa aiba varsta cuprinsa intre 30-40 ani, sa aiba parul bogat, zambet frumos si sa nu mai fi aparut vreodata in reclame. Copilul ar trebui sa aiba intre 3 si 6 ani, sa nu fie rusinos si sa nu mai fi aparut vreodata in vreo reclama. Pentru tata e foarte important sa aiba par si sa fie carismatic. Pentru detalii, va rog sa trimiteti mail la adresa rionita@sagafilm.ro', '0', 'Laura', '0740.121.864', '1', '1', '', '', '', null, '2015-08-24 21:17:45', '48', '0', 'F', 'office@romaniacastingcall.com', '0', '0', null, null, '1', '0', '3', '2015-09-08 09:14:25', '2015-08-15 16:21:22');
INSERT INTO `jobs` VALUES ('14', 'Cautam actori', 'Produs pentru ingrijirea parului', null, '32', 'Bucuresti', null, '1996-07-29', '2007-10-24', '1993-03-25', '2003-09-11', '5', 'Vrei sa participi la cele mai tari proiecte video si sa fii alaturi de artistii tai preferati? HaHaHa Video Production cauta absolventi de actorie sau studenti la o facultate de profil care vor sa participe la viitoarele proiecte video realizate pentru cei mai cunoscuti artisti romani si nu numai.', '0', '11', '45', '1', '166', '184', 'Any', 'Regizare videoclipuri muzicale', '1', 'Castalia Productions', '0758080821', '1', '0', '', '', '', null, '2015-08-29 16:46:29', '6', '1', 'M', 'laura.musat10@gmail.com', '1', '0', null, null, '1', '1', '9', '2015-08-31 19:08:41', '2015-08-15 03:16:56');
INSERT INTO `jobs` VALUES ('15', 'Se cauta voice-over pentru reclama', 'Reclama video Romania Casting Call', null, '13', 'Bucuresti', null, '1976-04-05', '2009-06-29', '1996-10-22', '1993-09-27', '1', '10 ore\" urmareste povestea captivanta a Mariei, o fetita de 10-11 ani, care provine dintr-o familie de oameni instariti si ocupati. Tatal ei, un extraordinar om de afaceri in telecomunicatii, are din ce in ce mai putin timp pentru ea, fapt ce ii produce Mariei o mare suferinta. Din lipsa de atentie, dar si din dragoste fata de tatal ei, Maria pleaca de acasa, lasandu-i tatalui sau un bilet, prin care il informeaza de plecarea sa si ii pune la dispozitie 10 ore pentru a o gasi. Astfel, Maria porneste intr-o aventura plina de obstacole, sperand sa il faca pe tatal sau sa realizeze importanta afectiunii si atentiei unui parinte pentru un copil care nu isi doreste jucarii si tablete, pe cat de mult isi doreste un tata aproape de ea. O emotionanta drama, cu elemente comice, care transmite un mesaj actual si simplu, menit sa deschida ochii parintilor care isi neglijeaza copiii din cauza job-ului.', '0', '27', '43', '5', '173', '168', 'Fat', 'O voce placuta, dar mai ales convingatoare. Nu conteaza sexul.', '1', 'Laura', '0740.121.864', '1', '1', '', '', '', null, '2015-08-16 12:40:09', '6', '1', 'M', 'office@romaniacastingcall.com', '1', '1', null, null, '0', '0', '6', '2015-08-10 14:10:30', '2015-08-09 07:05:58');
INSERT INTO `jobs` VALUES ('16', 'Cautam actori', 'HaHaHa Video Production', null, '41', 'Bucuresti', null, '1991-03-28', '1981-12-09', '2007-05-07', '2014-02-08', '2', '10 ore\" urmareste povestea captivanta a Mariei, o fetita de 10-11 ani, care provine dintr-o familie de oameni instariti si ocupati. Tatal ei, un extraordinar om de afaceri in telecomunicatii, are din ce in ce mai putin timp pentru ea, fapt ce ii produce Mariei o mare suferinta. Din lipsa de atentie, dar si din dragoste fata de tatal ei, Maria pleaca de acasa, lasandu-i tatalui sau un bilet, prin care il informeaza de plecarea sa si ii pune la dispozitie 10 ore pentru a o gasi. Astfel, Maria porneste intr-o aventura plina de obstacole, sperand sa il faca pe tatal sau sa realizeze importanta afectiunii si atentiei unui parinte pentru un copil care nu isi doreste jucarii si tablete, pe cat de mult isi doreste un tata aproape de ea. O emotionanta drama, cu elemente comice, care transmite un mesaj actual si simplu, menit sa deschida ochii parintilor care isi neglijeaza copiii din cauza job-ului.', '1', '26', '47', '6', '167', '186', 'Thin', 'Cautam familii reale: mama- tata  copil pentru filmarea unor spoturi publicitare pentru un produs de ingrijire a parului, Produsul este dezvaluit doar in ziua filmarii, dat fiind faptul ca este un proiect de tip testimonial si se vrea reactii reale. Mama trebuie sa aiba varsta cuprinsa intre 30-40 ani, sa aiba parul bogat, zambet frumos si sa nu mai fi aparut vreodata in reclame. Copilul ar trebui sa aiba intre 3 si 6 ani, sa nu fie rusinos si sa nu mai fi aparut vreodata in vreo reclama. Pentru tata e foarte important sa aiba par si sa fie carismatic. Pentru detalii, va rog sa trimiteti mail la adresa rionita@sagafilm.ro', '0', 'SAGA FILM', '0740.121.864', '0', '0', '', '', '', null, '2015-09-03 11:11:48', '6', '0', 'M', 'laura.musat@romaniacastingcall.com', '1', '0', null, null, '1', '0', '2', '2015-08-13 13:19:18', '2015-08-24 15:47:43');
INSERT INTO `jobs` VALUES ('17', 'Figuratie Scurtmetraj', 'Produs pentru ingrijirea parului', null, '49', 'Bucuresti', null, '2013-10-23', '1984-03-29', '1981-05-13', '1972-03-11', '4', 'Dupa cum probabil stiti, Romania Casting Call s-a lansat de curand. A inceput sa creasca usor usor, insa pentru a ajunge la urechile tuturor, are nevoie de o reclama (TV si Radio). Avem nevoie de o voce, insa nu cautam un stil anumite sau un sex. Dorim cat mai multa diversitate, asadar va asteptam!', '1', '18', '32', '2', '170', '171', 'Thin', 'Regizare videoclipuri muzicale', '0', 'Castalia Productions', '0740300290', '0', '1', '', '', '', null, '2015-08-11 15:58:02', '55', '1', 'M', 'pr@hahahaproduction.com', '1', '1', null, null, '0', '0', '2', '2015-08-10 18:44:22', '2015-08-13 06:42:57');
INSERT INTO `jobs` VALUES ('18', 'Cautam actori', 'Produs pentru ingrijirea parului', null, '19', 'Bucuresti', null, '1992-03-18', '2011-02-19', '1992-05-26', '2003-06-01', '2', 'Vrei sa participi la cele mai tari proiecte video si sa fii alaturi de artistii tai preferati? HaHaHa Video Production cauta absolventi de actorie sau studenti la o facultate de profil care vor sa participe la viitoarele proiecte video realizate pentru cei mai cunoscuti artisti romani si nu numai.', '1', '23', '32', '1', '174', '167', 'Thin', 'Cautam familii reale: mama- tata  copil pentru filmarea unor spoturi publicitare pentru un produs de ingrijire a parului, Produsul este dezvaluit doar in ziua filmarii, dat fiind faptul ca este un proiect de tip testimonial si se vrea reactii reale. Mama trebuie sa aiba varsta cuprinsa intre 30-40 ani, sa aiba parul bogat, zambet frumos si sa nu mai fi aparut vreodata in reclame. Copilul ar trebui sa aiba intre 3 si 6 ani, sa nu fie rusinos si sa nu mai fi aparut vreodata in vreo reclama. Pentru tata e foarte important sa aiba par si sa fie carismatic. Pentru detalii, va rog sa trimiteti mail la adresa rionita@sagafilm.ro', '0', 'HaHaHa Video Production', '0740.121.864', '0', '1', '', '', '', null, '2015-09-06 19:40:25', '26', '0', 'F', 'laura.musat@romaniacastingcall.com', '1', '1', null, null, '0', '0', '9', '2015-09-01 18:27:43', '2015-08-31 13:12:35');
INSERT INTO `jobs` VALUES ('19', 'Cautam regizori', 'Love building', null, '16', 'Bucuresti', null, '2011-07-23', '1993-03-20', '2001-01-19', '1988-08-29', '9', 'Castalia Productions se lanseaza in aceasta vara cu o productie romano-britanica, si anume scurtmetrajul intitulat \"10 ore\" . Echipa este formata numai din oameni profesionisti, cunoscuti pe piata romaneasca, si completata de prezenta catorva studenti britanici de la facultati de profil londoneze. \"10 ore\" urmareste povestea captivanta a Mariei, o fetita de 10-11 ani, care provine dintr-o familie de oameni instariti si ocupati. Tatal ei, un extraordinar om de afaceri in telecomunicatii, are din ce in ce mai putin timp pentru ea, fapt ce ii produce Mariei o mare suferinta. Din lipsa de atentie, dar si din dragoste fata de tatal ei, Maria porneste intr-o aventura plina de obstacole, sperand sa il faca pe tatal sau sa realizeze importanta afectiunii si atentiei unui parinte pentru un copil care nu isi doreste jucarii si tablete, pe cat de mult isi doreste un tata aproape de ea. O emotionanta drama, cu elemente comice, care transmite un mesaj actual si simplu, menit sa deschida ochii parintilor care isi neglijeaza copiii din cauza job-ului. Acest scurtmetraj va fi trimis la toate festivalurile importante, printre ele numarandu-se si Cannes / Sundance / BFI etc.', '1', '14', '37', '5', '174', '181', 'Thin', 'Regizare videoclipuri muzicale', '0', 'Laura', '0758080821', '0', '0', '', '', '', null, '2015-09-02 02:08:40', '28', '0', 'M', 'pr@hahahaproduction.com', '1', '1', null, null, '0', '0', '7', '2015-08-14 13:39:26', '2015-08-15 06:11:51');
INSERT INTO `jobs` VALUES ('20', 'Cautam actori', 'HaHaHa Video Production', null, '29', 'Bucuresti', null, '2007-03-02', '2008-03-12', '1981-12-09', '1986-08-20', '5', '10 ore\" urmareste povestea captivanta a Mariei, o fetita de 10-11 ani, care provine dintr-o familie de oameni instariti si ocupati. Tatal ei, un extraordinar om de afaceri in telecomunicatii, are din ce in ce mai putin timp pentru ea, fapt ce ii produce Mariei o mare suferinta. Din lipsa de atentie, dar si din dragoste fata de tatal ei, Maria pleaca de acasa, lasandu-i tatalui sau un bilet, prin care il informeaza de plecarea sa si ii pune la dispozitie 10 ore pentru a o gasi. Astfel, Maria porneste intr-o aventura plina de obstacole, sperand sa il faca pe tatal sau sa realizeze importanta afectiunii si atentiei unui parinte pentru un copil care nu isi doreste jucarii si tablete, pe cat de mult isi doreste un tata aproape de ea. O emotionanta drama, cu elemente comice, care transmite un mesaj actual si simplu, menit sa deschida ochii parintilor care isi neglijeaza copiii din cauza job-ului.', '0', '26', '52', '3', '175', '182', 'Fat', 'Regizare videoclipuri muzicale', '0', 'HaHaHa Video Production', '0740300290', '1', '0', '', '', '', null, '2015-08-25 20:15:23', '33', '0', 'M', 'laura@office.ro', '1', '1', null, null, '0', '1', '5', '2015-08-10 14:02:29', '2015-08-15 23:03:02');
INSERT INTO `jobs` VALUES ('21', 'Cautam regizori', 'Love building', null, '5', 'Bucuresti', null, '1978-01-14', '1986-05-12', '1997-01-12', '2009-01-22', '2', '10 ore\" urmareste povestea captivanta a Mariei, o fetita de 10-11 ani, care provine dintr-o familie de oameni instariti si ocupati. Tatal ei, un extraordinar om de afaceri in telecomunicatii, are din ce in ce mai putin timp pentru ea, fapt ce ii produce Mariei o mare suferinta. Din lipsa de atentie, dar si din dragoste fata de tatal ei, Maria pleaca de acasa, lasandu-i tatalui sau un bilet, prin care il informeaza de plecarea sa si ii pune la dispozitie 10 ore pentru a o gasi. Astfel, Maria porneste intr-o aventura plina de obstacole, sperand sa il faca pe tatal sau sa realizeze importanta afectiunii si atentiei unui parinte pentru un copil care nu isi doreste jucarii si tablete, pe cat de mult isi doreste un tata aproape de ea. O emotionanta drama, cu elemente comice, care transmite un mesaj actual si simplu, menit sa deschida ochii parintilor care isi neglijeaza copiii din cauza job-ului.', '0', '30', '64', '1', '174', '180', 'Fat', 'Cautam doi barbati pentru figuratie in scurtmetrajul \"10 ore\", care vor face parte dintr-un board meeting de 4 oameni: o femeie si trei barbati. Rolurile sunt minimale: nicio replica, doar prezenta. Aceste doua personaje, alaturi de inca doua - o femeie   inca un barbat, formeaza board-ul din sala de conferinte, care ii asculta prezentarea dl. Suciu, tatal Mariei. La finalul prezentarii, cei 3 barbati vor hotari daca investesc in proiectul dl Suciu sau nu. Toti 4 sunt oameni de afaceri, imbracati la costum. Vlad Suciu, tatal Mariei va fi jucat de un actor cunoscut in Romania, acest film  fiind o productie romano-britanica, care va participa la majoritatea festivalurilor mari din lume.', '1', 'Laura', '0740.121.864', '0', '1', '', '', '', null, '2015-08-31 08:28:37', '5', '0', 'F', 'pr@hahahaproduction.com', '1', '0', null, null, '0', '1', '4', '2015-09-08 12:02:58', '2015-09-01 15:09:09');
INSERT INTO `jobs` VALUES ('22', 'Se cauta voice-over pentru reclama', 'HaHaHa Video Production', null, '37', 'Bucuresti', null, '1974-09-18', '1995-10-06', '1991-12-16', '1983-05-06', '7', 'Castalia Productions se lanseaza in aceasta vara cu o productie romano-britanica, si anume scurtmetrajul intitulat \"10 ore\" . Echipa este formata numai din oameni profesionisti, cunoscuti pe piata romaneasca, si completata de prezenta catorva studenti britanici de la facultati de profil londoneze. \"10 ore\" urmareste povestea captivanta a Mariei, o fetita de 10-11 ani, care provine dintr-o familie de oameni instariti si ocupati. Tatal ei, un extraordinar om de afaceri in telecomunicatii, are din ce in ce mai putin timp pentru ea, fapt ce ii produce Mariei o mare suferinta. Din lipsa de atentie, dar si din dragoste fata de tatal ei, Maria porneste intr-o aventura plina de obstacole, sperand sa il faca pe tatal sau sa realizeze importanta afectiunii si atentiei unui parinte pentru un copil care nu isi doreste jucarii si tablete, pe cat de mult isi doreste un tata aproape de ea. O emotionanta drama, cu elemente comice, care transmite un mesaj actual si simplu, menit sa deschida ochii parintilor care isi neglijeaza copiii din cauza job-ului. Acest scurtmetraj va fi trimis la toate festivalurile importante, printre ele numarandu-se si Cannes / Sundance / BFI etc.', '0', '20', '56', '4', '171', '180', 'Any', 'Regizare videoclipuri muzicale', '1', 'Laura Musat', '0740.121.864', '0', '0', '', '', '', null, '2015-08-28 07:02:14', '19', '1', 'F', 'laura@office.ro', '0', '0', null, null, '0', '1', '7', '2015-08-19 00:50:55', '2015-08-24 03:24:28');
INSERT INTO `jobs` VALUES ('23', 'Cautam actori', 'HaHaHa Video Production', null, '25', 'Bucuresti', null, '1975-10-29', '1986-05-15', '2002-07-02', '1970-12-19', '2', 'Castalia Productions se lanseaza in aceasta vara cu o productie romano-britanica, si anume scurtmetrajul intitulat \"10 ore\" . Echipa este formata numai din oameni profesionisti, cunoscuti pe piata romaneasca, si completata de prezenta catorva studenti britanici de la facultati de profil londoneze. \"10 ore\" urmareste povestea captivanta a Mariei, o fetita de 10-11 ani, care provine dintr-o familie de oameni instariti si ocupati. Tatal ei, un extraordinar om de afaceri in telecomunicatii, are din ce in ce mai putin timp pentru ea, fapt ce ii produce Mariei o mare suferinta. Din lipsa de atentie, dar si din dragoste fata de tatal ei, Maria porneste intr-o aventura plina de obstacole, sperand sa il faca pe tatal sau sa realizeze importanta afectiunii si atentiei unui parinte pentru un copil care nu isi doreste jucarii si tablete, pe cat de mult isi doreste un tata aproape de ea. O emotionanta drama, cu elemente comice, care transmite un mesaj actual si simplu, menit sa deschida ochii parintilor care isi neglijeaza copiii din cauza job-ului. Acest scurtmetraj va fi trimis la toate festivalurile importante, printre ele numarandu-se si Cannes / Sundance / BFI etc.', '0', '14', '47', '6', '162', '186', '', 'O voce placuta, dar mai ales convingatoare. Nu conteaza sexul.', '0', 'SAGA FILM', '0758080821', '0', '1', '', '', '', null, '2015-08-27 23:25:00', '13', '1', 'M', 'laura.musat10@gmail.com', '0', '1', null, null, '1', '0', '4', '2015-09-01 18:15:54', '2015-08-24 19:03:36');
INSERT INTO `jobs` VALUES ('24', 'Personaj Principal / Copil - Scurtmetraj', 'Love building', null, '49', 'Bucuresti', null, '1978-10-24', '1973-02-13', '1985-09-19', '2003-05-08', '7', 'Castalia Productions se lanseaza in aceasta vara cu o productie romano-britanica, si anume scurtmetrajul intitulat \"10 ore\" . Echipa este formata numai din oameni profesionisti, cunoscuti pe piata romaneasca, si completata de prezenta catorva studenti britanici de la facultati de profil londoneze. \"10 ore\" urmareste povestea captivanta a Mariei, o fetita de 10-11 ani, care provine dintr-o familie de oameni instariti si ocupati. Tatal ei, un extraordinar om de afaceri in telecomunicatii, are din ce in ce mai putin timp pentru ea, fapt ce ii produce Mariei o mare suferinta. Din lipsa de atentie, dar si din dragoste fata de tatal ei, Maria porneste intr-o aventura plina de obstacole, sperand sa il faca pe tatal sau sa realizeze importanta afectiunii si atentiei unui parinte pentru un copil care nu isi doreste jucarii si tablete, pe cat de mult isi doreste un tata aproape de ea. O emotionanta drama, cu elemente comice, care transmite un mesaj actual si simplu, menit sa deschida ochii parintilor care isi neglijeaza copiii din cauza job-ului. Acest scurtmetraj va fi trimis la toate festivalurile importante, printre ele numarandu-se si Cannes / Sundance / BFI etc.', '0', '28', '53', '2', '175', '169', 'Fat', 'O voce placuta, dar mai ales convingatoare. Nu conteaza sexul.', '1', 'Laura', '0740.121.864', '0', '1', '', '', '', null, '2015-08-15 22:11:58', '22', '1', 'F', 'rionita@sagafilm.ro', '0', '0', null, null, '0', '0', '1', '2015-09-01 07:59:55', '2015-08-23 13:43:37');
INSERT INTO `jobs` VALUES ('25', 'Figuratie Scurtmetraj', 'Love building', null, '14', 'Bucuresti', null, '2010-07-23', '2003-09-16', '1972-08-03', '1985-05-26', '7', 'Castalia Productions se lanseaza in aceasta vara cu o productie romano-britanica, si anume scurtmetrajul intitulat \"10 ore\" . Echipa este formata numai din oameni profesionisti, cunoscuti pe piata romaneasca, si completata de prezenta catorva studenti britanici de la facultati de profil londoneze. \"10 ore\" urmareste povestea captivanta a Mariei, o fetita de 10-11 ani, care provine dintr-o familie de oameni instariti si ocupati. Tatal ei, un extraordinar om de afaceri in telecomunicatii, are din ce in ce mai putin timp pentru ea, fapt ce ii produce Mariei o mare suferinta. Din lipsa de atentie, dar si din dragoste fata de tatal ei, Maria porneste intr-o aventura plina de obstacole, sperand sa il faca pe tatal sau sa realizeze importanta afectiunii si atentiei unui parinte pentru un copil care nu isi doreste jucarii si tablete, pe cat de mult isi doreste un tata aproape de ea. O emotionanta drama, cu elemente comice, care transmite un mesaj actual si simplu, menit sa deschida ochii parintilor care isi neglijeaza copiii din cauza job-ului. Acest scurtmetraj va fi trimis la toate festivalurile importante, printre ele numarandu-se si Cannes / Sundance / BFI etc.', '0', '27', '51', '6', '163', '173', 'Fat', 'Regizare videoclipuri muzicale', '1', 'SAGA FILM', '0758080821', '1', '1', '', '', '', null, '2015-08-24 17:45:35', '1', '1', 'M', 'rionita@sagafilm.ro', '1', '1', null, null, '1', '1', '5', '2015-08-15 05:09:38', '2015-09-03 16:49:11');
INSERT INTO `jobs` VALUES ('26', 'Se cauta voice-over pentru reclama', 'Produs pentru ingrijirea parului', null, '36', 'Bucuresti', null, '1987-10-30', '1988-03-01', '1994-02-12', '1970-12-21', '5', 'Filmarea a doua spoturi publicitare, unul in data pe 4.11.2014, celalalt pe 5.11.2014. Dat fiind faptul ca proiectul este unul de tip testimonial, numele produsului va fi dezvalui in ziua filmarii.', '0', '27', '53', '7', '165', '174', '', 'O voce placuta, dar mai ales convingatoare. Nu conteaza sexul.', '0', 'Castalia Productions', '0758080821', '1', '1', '', '', '', null, '2015-08-19 10:52:58', '12', '0', 'F', 'pr@hahahaproduction.com', '0', '0', null, null, '1', '0', '8', '2015-08-26 14:55:30', '2015-08-09 23:16:01');
INSERT INTO `jobs` VALUES ('27', 'Ulala', 'Reclama video Romania Casting Call', null, '23', 'Bucuresti', null, '1993-09-17', '1987-07-28', '2013-05-07', '2006-08-05', '1', 'Castalia Productions se lanseaza in aceasta vara cu o productie romano-britanica, si anume scurtmetrajul intitulat \"10 ore\" . Echipa este formata numai din oameni profesionisti, cunoscuti pe piata romaneasca, si completata de prezenta catorva studenti britanici de la facultati de profil londoneze. \"10 ore\" urmareste povestea captivanta a Mariei, o fetita de 10-11 ani, care provine dintr-o familie de oameni instariti si ocupati. Tatal ei, un extraordinar om de afaceri in telecomunicatii, are din ce in ce mai putin timp pentru ea, fapt ce ii produce Mariei o mare suferinta. Din lipsa de atentie, dar si din dragoste fata de tatal ei, Maria porneste intr-o aventura plina de obstacole, sperand sa il faca pe tatal sau sa realizeze importanta afectiunii si atentiei unui parinte pentru un copil care nu isi doreste jucarii si tablete, pe cat de mult isi doreste un tata aproape de ea. O emotionanta drama, cu elemente comice, care transmite un mesaj actual si simplu, menit sa deschida ochii parintilor care isi neglijeaza copiii din cauza job-ului. Acest scurtmetraj va fi trimis la toate festivalurile importante, printre ele numarandu-se si Cannes / Sundance / BFI etc.', '0', '18', '40', '5', '169', '190', 'Thin', 'O voce placuta, dar mai ales convingatoare. Nu conteaza sexul.', '1', 'Laura Musat', '0740.121.864', '1', '0', '', '', '', null, '2015-08-13 18:24:08', '13', '1', 'F', 'laura.musat10@gmail.com', '0', '0', null, null, '0', '0', '6', '2015-08-28 12:45:09', '2015-08-24 07:25:12');
INSERT INTO `jobs` VALUES ('28', 'Personaj Principal / Copil - Scurtmetraj', 'HaHaHa Video Production', null, '37', 'Bucuresti', null, '1984-12-03', '1998-06-12', '2006-05-28', '1971-06-10', '1', 'Filmarea a doua spoturi publicitare, unul in data pe 4.11.2014, celalalt pe 5.11.2014. Dat fiind faptul ca proiectul este unul de tip testimonial, numele produsului va fi dezvalui in ziua filmarii.', '1', '28', '42', '2', '171', '186', '', 'Cautam familii reale: mama- tata  copil pentru filmarea unor spoturi publicitare pentru un produs de ingrijire a parului, Produsul este dezvaluit doar in ziua filmarii, dat fiind faptul ca este un proiect de tip testimonial si se vrea reactii reale. Mama trebuie sa aiba varsta cuprinsa intre 30-40 ani, sa aiba parul bogat, zambet frumos si sa nu mai fi aparut vreodata in reclame. Copilul ar trebui sa aiba intre 3 si 6 ani, sa nu fie rusinos si sa nu mai fi aparut vreodata in vreo reclama. Pentru tata e foarte important sa aiba par si sa fie carismatic. Pentru detalii, va rog sa trimiteti mail la adresa rionita@sagafilm.ro', '1', 'Laura Musat', '0758080821', '0', '1', '', '', '', null, '2015-09-02 01:02:51', '33', '0', 'M', 'office@romaniacastingcall.com', '0', '1', null, null, '1', '0', '6', '2015-08-11 13:00:03', '2015-09-07 04:35:05');
INSERT INTO `jobs` VALUES ('29', 'Cautam regizori', 'Produs pentru ingrijirea parului', null, '45', 'Bucuresti', null, '1997-08-25', '2011-06-23', '2004-06-07', '1981-06-11', '2', 'Vrei sa participi la cele mai tari proiecte video si sa fii alaturi de artistii tai preferati? HaHaHa Video Production cauta absolventi de actorie sau studenti la o facultate de profil care vor sa participe la viitoarele proiecte video realizate pentru cei mai cunoscuti artisti romani si nu numai.', '1', '21', '43', '4', '162', '181', 'Thin', 'Cautam familii reale: mama- tata  copil pentru filmarea unor spoturi publicitare pentru un produs de ingrijire a parului, Produsul este dezvaluit doar in ziua filmarii, dat fiind faptul ca este un proiect de tip testimonial si se vrea reactii reale. Mama trebuie sa aiba varsta cuprinsa intre 30-40 ani, sa aiba parul bogat, zambet frumos si sa nu mai fi aparut vreodata in reclame. Copilul ar trebui sa aiba intre 3 si 6 ani, sa nu fie rusinos si sa nu mai fi aparut vreodata in vreo reclama. Pentru tata e foarte important sa aiba par si sa fie carismatic. Pentru detalii, va rog sa trimiteti mail la adresa rionita@sagafilm.ro', '1', 'Laura', '0740300290', '0', '0', '', '', '', null, '2015-09-05 07:36:58', '36', '0', 'F', 'rionita@sagafilm.ro', '1', '0', null, null, '0', '0', '2', '2015-08-31 13:43:52', '2015-09-04 15:29:44');
INSERT INTO `jobs` VALUES ('30', 'Personaj Principal / Copil - Scurtmetraj', 'Reclama video Romania Casting Call', null, '33', 'Bucuresti', null, '1997-07-04', '1987-07-30', '1980-02-19', '1980-12-31', '4', 'Vrei sa participi la cele mai tari proiecte video si sa fii alaturi de artistii tai preferati? HaHaHa Video Production cauta absolventi de actorie sau studenti la o facultate de profil care vor sa participe la viitoarele proiecte video realizate pentru cei mai cunoscuti artisti romani si nu numai.', '0', '12', '48', '5', '161', '178', 'Thin', 'Cautam doi barbati pentru figuratie in scurtmetrajul \"10 ore\", care vor face parte dintr-un board meeting de 4 oameni: o femeie si trei barbati. Rolurile sunt minimale: nicio replica, doar prezenta. Aceste doua personaje, alaturi de inca doua - o femeie   inca un barbat, formeaza board-ul din sala de conferinte, care ii asculta prezentarea dl. Suciu, tatal Mariei. La finalul prezentarii, cei 3 barbati vor hotari daca investesc in proiectul dl Suciu sau nu. Toti 4 sunt oameni de afaceri, imbracati la costum. Vlad Suciu, tatal Mariei va fi jucat de un actor cunoscut in Romania, acest film  fiind o productie romano-britanica, care va participa la majoritatea festivalurilor mari din lume.', '0', 'HaHaHa Video Production', '0758080821', '1', '0', '', '', '', null, '2015-08-31 19:11:04', '37', '0', 'M', 'pr@hahahaproduction.com', '0', '0', null, null, '0', '0', '6', '2015-08-27 23:34:34', '2015-08-31 22:45:50');

-- ----------------------------
-- Table structure for language_page
-- ----------------------------
DROP TABLE IF EXISTS `language_page`;
CREATE TABLE `language_page` (
  `language_id` int(10) unsigned NOT NULL,
  `page_id` int(10) unsigned NOT NULL,
  `slug` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `excerpt` text COLLATE utf8_unicode_ci,
  `meta_title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8_unicode_ci NOT NULL,
  `meta_keywords` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  UNIQUE KEY `language_page_slug_unique` (`slug`),
  KEY `language_page_language_id_index` (`language_id`),
  KEY `language_page_page_id_index` (`page_id`),
  CONSTRAINT `language_page_language_id_foreign` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE,
  CONSTRAINT `language_page_page_id_foreign` FOREIGN KEY (`page_id`) REFERENCES `pages` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of language_page
-- ----------------------------
INSERT INTO `language_page` VALUES ('1', '7', 'adipisci-totam-corporis-incidunt-repellendus-repellat-quaerat-aspernatur', 'Officiis quasi voluptatem nemo culpa cupiditate quis.', 'No room!\' they cried out when they met in the kitchen. \'When I\'M a Duchess,\' she said to herself, \'because of his teacup and bread-and-butter, and went on eagerly. \'That\'s enough about lessons,\' the.', 'Stigand, the patriotic archbishop of Canterbury, found it very much,\' said Alice; \'you needn\'t be so stingy about it, so she helped herself to about two feet high: even then she heard the Rabbit in.', '', '', '', '2015-08-20 18:45:14', '2015-08-09 20:51:43');
INSERT INTO `language_page` VALUES ('2', '11', 'aliquam-odio-facere-cupiditate-nobis-repudiandae-provident-earum', 'Ut iste praesentium vero voluptas voluptate architecto.', 'Duchess: you\'d better finish the story for yourself.\' \'No, please go on!\' Alice said with some curiosity. \'What a curious plan!\' exclaimed Alice. \'And where HAVE my shoulders got to? And oh, my poor.', 'And the muscular strength, which it gave to my right size: the next thing was waving its tail about in the pictures of him), while the rest were quite dry again, the Dodo solemnly presented the.', '', '', '', '2015-08-30 13:29:32', '2015-09-08 15:17:43');
INSERT INTO `language_page` VALUES ('2', '10', 'assumenda-officiis-aut-rerum-quo-fugit-repellat', 'Necessitatibus eligendi molestiae aut harum.', 'There are no mice in the last concert!\' on which the words have got in as well,\' the Hatter with a T!\' said the Queen, who were lying on their slates, when the Rabbit in a hurry. \'No, I\'ll look.', 'Alice, and her eyes filled with cupboards and book-shelves; here and there. There was a dead silence. Alice was a queer-shaped little creature, and held it out to the voice of the day; and this was.', '', '', '', '2015-08-22 04:13:54', '2015-09-04 13:02:23');
INSERT INTO `language_page` VALUES ('1', '24', 'atque-omnis-modi-quis-dolorem', 'Et similique et aut accusamus.', 'Bill! I wouldn\'t be so proud as all that.\' \'With extras?\' asked the Mock Turtle said: \'I\'m too stiff. And the Gryphon at the mushroom (she had kept a piece of it now in sight, hurrying down it..', 'I look like it?\' he said, \'on and off, for days and days.\' \'But what am I to do THAT in a low voice. \'Not at first, the two creatures got so much already, that it felt quite strange at first; but.', '', '', '', '2015-08-18 05:01:08', '2015-09-08 03:09:16');
INSERT INTO `language_page` VALUES ('1', '28', 'doloribus-est-autem-laudantium-quaerat-et', 'Dolor repellat reprehenderit qui rerum.', 'CHAPTER XII. Alice\'s Evidence \'Here!\' cried Alice, quite forgetting in the world you fly, Like a tea-tray in the night? Let me think: was I the same tone, exactly as if she were saying lessons, and.', 'I know?\' said Alice, a good deal worse off than before, as the hall was very fond of beheading people here; the great concert given by the carrier,\' she thought; \'and how funny it\'ll seem, sending.', '', '', '', '2015-08-29 04:55:08', '2015-08-13 09:05:02');
INSERT INTO `language_page` VALUES ('1', '11', 'enim-sunt-voluptatem-nam-tenetur', 'Perferendis occaecati officiis animi dolorum.', 'William the Conqueror.\' (For, with all speed back to the door, staring stupidly up into a sort of meaning in it, and behind it, it occurred to her ear. \'You\'re thinking about something, my dear, YOU.', 'She had quite forgotten the little thing sobbed again (or grunted, it was all dark overhead; before her was another puzzling question; and as he spoke, \'we were trying--\' \'I see!\' said the Cat, and.', '', '', '', '2015-08-25 21:43:53', '2015-09-02 02:38:13');
INSERT INTO `language_page` VALUES ('2', '14', 'esse-quia-voluptatem-neque-quaerat-dignissimos-tempore-unde', 'Sed labore in vel assumenda magnam voluptate est et.', 'Hatter. \'You might just as well as she went down to look about her repeating \'YOU ARE OLD, FATHER WILLIAM,\' to the game, feeling very curious to know what \"it\" means well enough, when I learn.', 'This was quite pale (with passion, Alice thought), and it put the hookah out of breath, and said to the other side of WHAT?\' thought Alice; \'but when you throw them, and he says it\'s so useful, it\'s.', '', '', '', '2015-08-15 12:24:28', '2015-08-28 00:59:42');
INSERT INTO `language_page` VALUES ('2', '30', 'est-dolore-esse-quia-doloribus-qui-voluptas-iste-fugiat', 'Voluptates officia ullam perferendis et ea sed hic.', 'I give you fair warning,\' shouted the Queen. First came ten soldiers carrying clubs; these were all shaped like the right size again; and the blades of grass, but she stopped hastily, for the rest.', 'Alice, who had meanwhile been examining the roses. \'Off with her arms round it as a cushion, resting their elbows on it, (\'which certainly was not easy to know your history, you know,\' the Mock.', '', '', '', '2015-08-27 07:39:19', '2015-08-27 15:05:01');
INSERT INTO `language_page` VALUES ('1', '8', 'et-ea-eius-neque-dolorem-omnis-doloribus', 'Est magni ad et.', 'Nile On every golden scale! \'How cheerfully he seems to like her, down here, and I\'m I, and--oh dear, how puzzling it all seemed quite dull and stupid for life to go down the middle, nursing a baby;.', 'Cheshire Cat, she was now about two feet high, and was going to begin with.\' \'A barrowful will do, to begin at HIS time of life. The King\'s argument was, that her shoulders were nowhere to be in a.', '', '', '', '2015-08-12 23:09:21', '2015-08-18 20:25:23');
INSERT INTO `language_page` VALUES ('2', '1', 'et-et-optio-necessitatibus-asperiores', 'Animi ut exercitationem repellendus aliquid similique ipsam alias repudiandae.', 'King, \'unless it was empty: she did not like to show you! A little bright-eyed terrier, you know, and he went on, turning to Alice an excellent plan, no doubt, and very soon came upon a little of.', 'Mock Turtle; \'but it doesn\'t matter a bit,\' she thought it must be what he did with the other paw, \'lives a Hatter: and in another moment, splash! she was nine feet high. \'I wish I had our Dinah.', '', '', '', '2015-08-28 20:39:13', '2015-08-28 16:18:20');
INSERT INTO `language_page` VALUES ('2', '10', 'et-omnis-consequatur-aut-dolorem', 'Dolorem ipsam suscipit illo ea.', 'HAVE their tails in their mouths--and they\'re all over with diamonds, and walked a little way out of its mouth, and its great eyes half shut. This seemed to think to herself, \'Why, they\'re only a.', 'Mock Turtle: \'crumbs would all come wrong, and she went on talking: \'Dear, dear! How queer everything is to-day! And yesterday things went on in a low voice. \'Not at first, the two sides of the.', '', '', '', '2015-09-07 13:05:28', '2015-08-28 18:49:39');
INSERT INTO `language_page` VALUES ('1', '27', 'et-qui-magni-nemo-molestiae-voluptatem-deserunt', 'Aut porro et dolorem doloremque eligendi et saepe.', 'I vote the young lady tells us a story!\' said the Mock Turtle. \'No, no! The adventures first,\' said the Gryphon: and Alice was beginning to end,\' said the Lory. Alice replied eagerly, for she had.', 'The Duchess took no notice of her voice, and the little door was shut again, and Alice looked all round the table, half hoping that the way to change the subject. \'Go on with the grin, which.', '', '', '', '2015-08-16 14:24:55', '2015-08-17 14:37:34');
INSERT INTO `language_page` VALUES ('2', '21', 'expedita-optio-eligendi-doloremque-et-veniam-ratione-eum', 'Voluptates repellat dolorem in excepturi.', 'Hatter said, tossing his head sadly. \'Do I look like it?\' he said, turning to the Cheshire Cat, she was beginning to end,\' said the Cat, \'a dog\'s not mad. You grant that?\' \'I suppose they are the.', 'March Hare. Alice sighed wearily. \'I think I can go back by railway,\' she said this, she was going to remark myself.\' \'Have you guessed the riddle yet?\' the Hatter went on, \'--likely to win, that.', '', '', '', '2015-09-02 01:43:23', '2015-08-15 06:03:07');
INSERT INTO `language_page` VALUES ('1', '26', 'explicabo-non-est-et-facilis-quis', 'Dicta voluptatem esse fuga et magnam.', 'Queen. An invitation for the next moment a shower of little pebbles came rattling in at all?\' said Alice, feeling very curious sensation, which puzzled her too much, so she set to work very.', 'I\'ve seen that done,\' thought Alice. \'I\'m glad I\'ve seen that done,\' thought Alice. \'I\'m glad they\'ve begun asking riddles.--I believe I can remember feeling a little shaking among the branches, and.', '', '', '', '2015-08-24 05:44:46', '2015-08-12 20:08:07');
INSERT INTO `language_page` VALUES ('2', '8', 'fugit-quae-dignissimos-omnis-et-dolorem', 'Qui labore qui fugit fuga aliquam qui illum.', 'Nile On every golden scale! \'How cheerfully he seems to grin, How neatly spread his claws, And welcome little fishes in With gently smiling jaws!\' \'I\'m sure those are not attending!\' said the Lory..', 'King, and he checked himself suddenly: the others took the least notice of her head on her spectacles, and began by producing from under his arm a great hurry. \'You did!\' said the Cat, as soon as.', '', '', '', '2015-08-31 07:46:37', '2015-08-10 23:55:05');
INSERT INTO `language_page` VALUES ('1', '5', 'hic-vero-reprehenderit-corrupti-et', 'Sed voluptatem recusandae nobis et iusto.', 'Cat again, sitting on a little pattering of feet in the act of crawling away: besides all this, there was a dead silence. \'It\'s a Cheshire cat,\' said the last time she went on in a deep sigh, \'I was.', 'Soup, so rich and green, Waiting in a tone of the Lobster; I heard him declare, \"You have baked me too brown, I must have got in your pocket?\' he went on, looking anxiously about as curious as it.', '', '', '', '2015-08-31 11:25:21', '2015-08-23 19:53:40');
INSERT INTO `language_page` VALUES ('2', '22', 'laboriosam-expedita-temporibus-ut-cupiditate-perspiciatis', 'Ut expedita quia et temporibus sit eum et.', 'They all returned from him to be no chance of getting her hands up to Alice, flinging the baby joined):-- \'Wow! wow! wow!\' While the Duchess was sitting next to her. The Cat only grinned when it saw.', 'He looked anxiously at the house, and have next to her. The Cat seemed to be treated with respect. \'Cheshire Puss,\' she began, rather timidly, as she heard the Queen\'s absence, and were quite dry.', '', '', '', '2015-08-15 22:07:05', '2015-08-09 02:15:21');
INSERT INTO `language_page` VALUES ('2', '20', 'laudantium-eos-non-dicta-libero-et', 'Voluptatem voluptatem modi voluptatibus id.', 'I would talk on such a fall as this, I shall never get to twenty at that rate! However, the Multiplication Table doesn\'t signify: let\'s try Geography. London is the driest thing I ever heard!\' \'Yes,.', 'But if I\'m Mabel, I\'ll stay down here! It\'ll be no use in knocking,\' said the Dodo, pointing to the Dormouse, after thinking a minute or two she walked on in the night? Let me see: I\'ll give them a.', '', '', '', '2015-08-19 22:50:57', '2015-08-21 17:57:19');
INSERT INTO `language_page` VALUES ('1', '16', 'magni-veniam-laudantium-quia-nemo-recusandae-porro', 'Ea aut et dolor ut magni.', 'The Rabbit Sends in a ring, and begged the Mouse was bristling all over, and both creatures hid their faces in their mouths--and they\'re all over with fright. \'Oh, I BEG your pardon!\' cried Alice.', 'I\'LL soon make you dry enough!\' They all made of solid glass; there was a long breath, and said \'No, never\') \'--so you can find them.\' As she said to herself. \'Of the mushroom,\' said the Dormouse;.', '', '', '', '2015-09-08 06:56:49', '2015-08-17 01:23:30');
INSERT INTO `language_page` VALUES ('1', '21', 'molestias-et-ipsam-fugit-impedit-et', 'Adipisci necessitatibus ex quod animi ut labore in autem.', 'Hatter asked triumphantly. Alice did not like the look of it in her head, she tried the effect of lying down on one knee. \'I\'m a poor man, your Majesty,\' he began, \'for bringing these in: but I.', 'HAVE you been doing here?\' \'May it please your Majesty,\' said Two, in a twinkling! Half-past one, time for dinner!\' (\'I only wish it was,\' said the Hatter. Alice felt so desperate that she never.', '', '', '', '2015-08-18 19:53:20', '2015-08-21 19:39:22');
INSERT INTO `language_page` VALUES ('1', '20', 'mollitia-inventore-et-praesentium-error-qui-harum', 'Asperiores ut officiis sed molestiae vel odit.', 'Alice said with a growl, And concluded the banquet--] \'What IS a Caucus-race?\' said Alice; not that she looked down into a large cauldron which seemed to rise like a sky-rocket!\' \'So you think.', 'Was kindly permitted to pocket the spoon: While the Duchess asked, with another hedgehog, which seemed to quiver all over crumbs.\' \'You\'re wrong about the crumbs,\' said the March Hare said--\' \'I.', '', '', '', '2015-08-19 03:56:58', '2015-08-10 06:42:39');
INSERT INTO `language_page` VALUES ('2', '14', 'quaerat-consequatur-sed-dolorem-aut-dolor-odit-quia', 'Ut illum omnis a et.', 'Mock Turtle to sing you a couple?\' \'You are all dry, he is gay as a lark, And will talk in contemptuous tones of her voice. Nobody moved. \'Who cares for fish, Game, or any other dish? Who would not.', 'Mouse had changed his mind, and was going off into a tidy little room with a shiver. \'I beg your pardon,\' said Alice a little irritated at the place where it had grown up,\' she said to herself, \'the.', '', '', '', '2015-08-09 05:29:58', '2015-09-02 22:00:14');
INSERT INTO `language_page` VALUES ('1', '23', 'quia-tempora-minus-ea-et-error-corrupti', 'Laborum eum eius quisquam quidem cupiditate rem.', 'Gryphon said, in a whisper, half afraid that it was the first figure!\' said the Duchess: \'flamingoes and mustard both bite. And the moral of that is--\"Oh, \'tis love, that makes them bitter--and--and.', 'Do you think I can do without lobsters, you know. Which shall sing?\' \'Oh, YOU sing,\' said the King, the Queen, and Alice guessed in a whisper.) \'That would be quite as much as serpents do, you.', '', '', '', '2015-08-16 09:49:05', '2015-08-21 03:42:34');
INSERT INTO `language_page` VALUES ('2', '16', 'quod-quia-quibusdam-saepe-ut', 'Rerum mollitia blanditiis tempore rem veniam tempore non.', 'Alice thoughtfully: \'but then--I shouldn\'t be hungry for it, you know--\' \'But, it goes on \"THEY ALL RETURNED FROM HIM TO YOU,\"\' said Alice. \'Off with their hands and feet at once, with a knife, it.', 'Queen, and Alice looked very uncomfortable. The moment Alice felt a very difficult game indeed. The players all played at once took up the fan and the Panther received knife and fork with a table in.', '', '', '', '2015-08-30 17:01:42', '2015-09-02 22:14:02');
INSERT INTO `language_page` VALUES ('1', '17', 'quos-dicta-at-ex-ea-repudiandae-molestiae', 'Similique debitis quo eos quae perferendis.', 'THAT direction,\' waving the other paw, \'lives a March Hare. \'I didn\'t know how to spell \'stupid,\' and that he had come back with the Queen,\' and she hurried out of the trial.\' \'Stupid things!\' Alice.', 'Hatter replied. \'Of course you know that cats COULD grin.\' \'They all can,\' said the Hatter, it woke up again as quickly as she spoke. (The unfortunate little Bill had left off sneezing by this very.', '', '', '', '2015-08-20 11:14:33', '2015-09-02 11:50:30');
INSERT INTO `language_page` VALUES ('2', '4', 'rerum-quibusdam-dolor-omnis-quos-enim-nam', 'Facilis nemo suscipit nesciunt ipsam laudantium.', 'TWO little shrieks, and more sounds of broken glass, from which she had hoped) a fan and two or three times over to the Queen, and Alice rather unwillingly took the least idea what a Gryphon is,.', 'I shall have to ask any more if you\'d rather not.\' \'We indeed!\' cried the Mouse, who was talking. Alice could hardly hear the words:-- \'I speak severely to my right size: the next witness was the.', '', '', '', '2015-09-03 18:53:29', '2015-09-02 14:12:01');
INSERT INTO `language_page` VALUES ('1', '21', 'sit-non-voluptates-quas-dolores-assumenda-sequi-suscipit', 'Ad unde ut et molestiae voluptas.', 'King, rubbing his hands; \'so now let the Dormouse began in a whisper.) \'That would be of very little way forwards each time and a bright idea came into Alice\'s head. \'Is that all?\' said the.', 'If I or she fell past it. \'Well!\' thought Alice to herself. Imagine her surprise, when the Rabbit angrily. \'Here! Come and help me out of the March Hare. Alice sighed wearily. \'I think I should.', '', '', '', '2015-08-31 11:41:31', '2015-08-20 07:35:34');
INSERT INTO `language_page` VALUES ('1', '19', 'sunt-odio-nostrum-aut-est-tempore-accusantium-quis', 'Pariatur aut et quis sit quia error id.', 'So Bill\'s got the other--Bill! fetch it back!\' \'And who are THESE?\' said the Gryphon. \'How the creatures wouldn\'t be in Bill\'s place for a few minutes it puffed away without speaking, but at any.', 'I only knew the right size, that it might not escape again, and all that,\' said the King. The White Rabbit put on his spectacles. \'Where shall I begin, please your Majesty!\' the Duchess and the.', '', '', '', '2015-09-02 07:49:06', '2015-08-17 04:00:51');
INSERT INTO `language_page` VALUES ('1', '29', 'ut-aspernatur-qui-amet-aliquam-qui-adipisci', 'Iste voluptates qui eaque inventore qui.', 'I shall fall right THROUGH the earth! How funny it\'ll seem to put everything upon Bill! I wouldn\'t say anything about it, and on both sides of it, and they went on again:-- \'You may not have lived.', 'Alice. \'What IS a long sleep you\'ve had!\' \'Oh, I\'ve had such a nice little histories about children who had followed him into the wood. \'It\'s the first sentence in her life, and had to ask help of.', '', '', '', '2015-08-13 02:11:48', '2015-08-13 23:21:19');
INSERT INTO `language_page` VALUES ('2', '11', 'ut-qui-ad-qui-et', 'Laudantium debitis aspernatur culpa quo qui suscipit.', 'Alice. \'That\'s very curious.\' \'It\'s all his fancy, that: he hasn\'t got no business of MINE.\' The Queen had ordered. They very soon found out that one of its little eyes, but it just now.\' \'It\'s the.', 'HIS time of life. The King\'s argument was, that she began again. \'I wonder what you\'re at!\" You know the meaning of it now in sight, hurrying down it. There could be beheaded, and that he shook both.', '', '', '', '2015-08-19 23:07:06', '2015-09-03 22:28:30');

-- ----------------------------
-- Table structure for languages
-- ----------------------------
DROP TABLE IF EXISTS `languages`;
CREATE TABLE `languages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `order` tinyint(3) unsigned NOT NULL,
  `status` tinyint(3) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `languages_name_unique` (`name`),
  UNIQUE KEY `languages_code_unique` (`code`),
  UNIQUE KEY `languages_locale_unique` (`locale`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of languages
-- ----------------------------
INSERT INTO `languages` VALUES ('1', 'Romana', 'ro', 'ro_RO.UTF-8', 'ro.png', '1', '1', '2015-09-08 22:29:28', '2015-09-08 22:29:28');
INSERT INTO `languages` VALUES ('2', 'English', 'en', 'en_US.UTF-8,en_US,en-gb,english', 'en.png', '2', '1', '2015-09-08 22:29:28', '2015-09-08 22:29:28');

-- ----------------------------
-- Table structure for licence_user
-- ----------------------------
DROP TABLE IF EXISTS `licence_user`;
CREATE TABLE `licence_user` (
  `user_id` int(10) unsigned NOT NULL,
  `licence_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `licence_user_user_id_index` (`user_id`),
  KEY `licence_user_licence_id_index` (`licence_id`),
  CONSTRAINT `licence_user_licence_id_foreign` FOREIGN KEY (`licence_id`) REFERENCES `licences` (`id`) ON DELETE CASCADE,
  CONSTRAINT `licence_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of licence_user
-- ----------------------------
INSERT INTO `licence_user` VALUES ('16', '14', '2015-09-02 00:25:20', '2015-08-22 04:16:43');
INSERT INTO `licence_user` VALUES ('5', '2', '2015-08-15 05:47:09', '2015-08-17 00:49:20');
INSERT INTO `licence_user` VALUES ('1', '17', '2015-08-24 21:20:58', '2015-09-04 21:40:45');
INSERT INTO `licence_user` VALUES ('23', '8', '2015-09-07 11:20:55', '2015-08-18 10:41:18');
INSERT INTO `licence_user` VALUES ('4', '16', '2015-08-16 18:37:09', '2015-08-10 09:26:36');
INSERT INTO `licence_user` VALUES ('10', '5', '2015-08-31 13:19:42', '2015-08-20 19:00:31');
INSERT INTO `licence_user` VALUES ('13', '12', '2015-08-18 10:48:47', '2015-08-22 19:30:50');
INSERT INTO `licence_user` VALUES ('42', '6', '2015-08-19 13:27:07', '2015-08-29 06:29:00');
INSERT INTO `licence_user` VALUES ('2', '10', '2015-08-27 06:53:26', '2015-08-26 13:26:37');
INSERT INTO `licence_user` VALUES ('14', '15', '2015-08-24 13:42:22', '2015-08-18 13:03:51');

-- ----------------------------
-- Table structure for licences
-- ----------------------------
DROP TABLE IF EXISTS `licences`;
CREATE TABLE `licences` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `licences_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of licences
-- ----------------------------
INSERT INTO `licences` VALUES ('1', 'AM', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `licences` VALUES ('2', 'A1', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `licences` VALUES ('3', 'A2', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `licences` VALUES ('4', 'A', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `licences` VALUES ('5', 'B1', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `licences` VALUES ('6', 'B', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `licences` VALUES ('7', 'BE', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `licences` VALUES ('8', 'C1', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `licences` VALUES ('9', 'C1E', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `licences` VALUES ('10', 'C', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `licences` VALUES ('11', 'CE', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `licences` VALUES ('12', 'D1', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `licences` VALUES ('13', 'D1E', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `licences` VALUES ('14', 'D', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `licences` VALUES ('15', 'DE', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `licences` VALUES ('16', 'Tr', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `licences` VALUES ('17', 'Tb', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `licences` VALUES ('18', 'Tv', '2015-09-08 22:29:29', '2015-09-08 22:29:29');

-- ----------------------------
-- Table structure for menus
-- ----------------------------
DROP TABLE IF EXISTS `menus`;
CREATE TABLE `menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `menus_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of menus
-- ----------------------------
INSERT INTO `menus` VALUES ('1', 'header', '2015-08-08 22:53:30', '2015-08-13 04:01:14');
INSERT INTO `menus` VALUES ('2', 'footer', '2015-08-19 06:39:25', '2015-08-29 10:15:46');
INSERT INTO `menus` VALUES ('3', 'sidebar', '2015-08-30 21:02:29', '2015-08-15 18:35:50');

-- ----------------------------
-- Table structure for messages
-- ----------------------------
DROP TABLE IF EXISTS `messages`;
CREATE TABLE `messages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `message` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_read` tinyint(1) NOT NULL,
  `is_important` tinyint(1) NOT NULL,
  `sender` int(10) unsigned NOT NULL,
  `recepient` int(10) unsigned NOT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `sent` tinyint(1) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of messages
-- ----------------------------
INSERT INTO `messages` VALUES ('1', 'Buna Mihail,<br><br>Spune-mi, te rog, ai timp duminica sau luni de inregistrare?&nbsp;<br><br>Vrem sa stim pentru ca dorim sa te alegem. Merci', '0', '1', '20', '17', 'Consequuntur vel quasi recusandae porro deleniti. Deleniti nobis sit ad saepe nihil. Soluta voluptatum iste nam pariatur. Et quas eos rerum quisquam dolor error.', '0', '1', null, '2015-08-12 15:06:38', '2015-09-02 09:34:24');
INSERT INTO `messages` VALUES ('2', 'Absolut!', '0', '0', '12', '24', 'Aperiam asperiores non sapiente et adipisci inventore. Vel quaerat rerum at molestias eum. Blanditiis fuga aut omnis. Voluptatem ratione et odio labore excepturi qui labore.', '1', '1', null, '2015-08-26 06:12:41', '2015-08-09 23:48:50');
INSERT INTO `messages` VALUES ('3', 'Buna dimineata,<br><br>Costul per spot radio este de 150 Euro, studious, daca il folosim facilitatile mele, este inclus.<br>Maine seara e OK la mine, daca e vorba de un alt loc, incepand cu 12 sunt disponibil.<br><br>Multam,<br>Mihail', '0', '0', '24', '50', 'Ea et eligendi velit doloremque exercitationem error. Quam assumenda quaerat est.', '0', '0', null, '2015-09-01 00:21:55', '2015-09-04 12:00:14');
INSERT INTO `messages` VALUES ('4', 'Si s-ar putea inregistra acolo maine dimineata sau seara, pe la un 6:30?<br><br>Care sunt costurile de inchiriere? Precum si ale tale de V.O? Vorbim de un spot de 20 secunde de radio', '0', '1', '6', '25', 'Consectetur minima quae culpa. Vel ut quia cum. Ut nobis ea iste ut quas impedit. Autem aut animi excepturi consequuntur.', '0', '1', null, '2015-08-17 05:38:08', '2015-08-21 13:10:24');
INSERT INTO `messages` VALUES ('5', 'Buna,<br><br>Stii cumva unde am putea inregistra? Ai vreo varianta pt duminica unde putem suna maine sau cautam noi?', '1', '0', '8', '1', 'Necessitatibus modi et molestiae velit ut animi in vel. Eius laudantium sunt dolor. Labore culpa esse eligendi doloremque et. Velit eaque placeat odit et ratione sint. Dicta nostrum ut et ut qui modi.', '0', '1', null, '2015-08-28 22:32:39', '2015-08-29 17:05:59');
INSERT INTO `messages` VALUES ('6', 'Ok, atunci iti trimit materialul de inregistrat si tot ce iti trebuie si ne trimiti tu 3 variante. Cred ca marti iti trimit. Poate fi gata pana vineri dimineata?<br><br>Merci', '0', '0', '50', '29', 'Cupiditate saepe nemo aut ducimus eos non quibusdam. Voluptatem esse incidunt temporibus dolorem. Earum porro ut et tenetur. Minima nam qui laborum architecto.', '0', '1', null, '2015-08-23 17:34:55', '2015-08-26 12:23:07');
INSERT INTO `messages` VALUES ('7', 'In seara asta (scuze, am dat send).<br>Sau, putem duminica.:)', '1', '1', '14', '16', 'Beatae veritatis aperiam in ut officia. Voluptatum a et quaerat et. Sapiente veritatis repellat modi qui laborum dolor.', '0', '1', null, '2015-08-16 06:54:04', '2015-08-09 16:47:54');
INSERT INTO `messages` VALUES ('8', 'Buna Mihail,<br><br>Spune-mi, te rog, ai timp duminica sau luni de inregistrare?&nbsp;<br><br>Vrem sa stim pentru ca dorim sa te alegem. Merci', '1', '1', '50', '36', 'Architecto veritatis deserunt ea inventore itaque ipsa unde. Doloribus et accusantium sint beatae. Expedita odio nulla beatae unde voluptate libero tenetur. Voluptas rerum quia similique enim et non. Occaecati placeat provident quis itaque nihil eveniet q', '0', '1', null, '2015-09-07 15:29:46', '2015-08-09 12:38:22');
INSERT INTO `messages` VALUES ('9', 'Buna Laura,<br><br>Daca e OK si pentru voi, prefer duminica. Pentru orice detalii, numarul meu este 0745 386 971.<br><br>Multam,<br>Mihail<br><br><br>Buna Mihail,<br><br>Spune-mi, te rog, ai timp duminica sau luni de inregistrare?&nbsp;<br><br>Vrem sa sti', '0', '0', '37', '49', 'Earum tempore et ab nihil consequatur doloribus ea et. Eos nihil soluta rerum ipsum et iusto. Voluptatem consequatur voluptas est dolorem explicabo est. Non eius minus et et minima enim quasi voluptatibus.', '1', '0', null, '2015-08-20 23:23:29', '2015-09-04 06:56:21');
INSERT INTO `messages` VALUES ('10', 'Ok, atunci iti trimit materialul de inregistrat si tot ce iti trebuie si ne trimiti tu 3 variante. Cred ca marti iti trimit. Poate fi gata pana vineri dimineata?<br><br>Merci', '0', '0', '22', '2', 'Tenetur corporis non consequatur aut saepe enim. Qui aspernatur nulla in ut ducimus consequatur ipsam. Quo dolor molestiae et excepturi. Blanditiis qui sit nam amet incidunt voluptatem. Voluptate veniam odio veritatis distinctio dolores sit mollitia.', '1', '0', null, '2015-08-20 17:46:53', '2015-08-22 22:04:12');

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES ('2014_10_12_100000_create_password_resets_table', '1');
INSERT INTO `migrations` VALUES ('2015_03_12_121806_create_pages_table', '1');
INSERT INTO `migrations` VALUES ('2015_03_16_135139_create_menus_table', '1');
INSERT INTO `migrations` VALUES ('2015_03_30_133000_create_profiles_table', '1');
INSERT INTO `migrations` VALUES ('2015_03_30_135500_create_othertalents_table', '1');
INSERT INTO `migrations` VALUES ('2015_03_30_135700_create_countries_table', '1');
INSERT INTO `migrations` VALUES ('2015_08_10_082239_create_roles_table', '1');
INSERT INTO `migrations` VALUES ('2015_08_10_082354_create_languages_table', '1');
INSERT INTO `migrations` VALUES ('2015_08_10_083946_create_actor_types_table', '1');
INSERT INTO `migrations` VALUES ('2015_08_10_083947_create_employer_types_table', '1');
INSERT INTO `migrations` VALUES ('2015_08_10_085316_create_ethnicities_table', '1');
INSERT INTO `migrations` VALUES ('2015_08_10_085408_create_haircolours_table', '1');
INSERT INTO `migrations` VALUES ('2015_08_10_085430_create_productions_table', '1');
INSERT INTO `migrations` VALUES ('2015_08_10_102409_create_licences_table', '1');
INSERT INTO `migrations` VALUES ('2015_08_10_135500_create_eyecolours_table', '1');
INSERT INTO `migrations` VALUES ('2015_08_11_083948_create_users_table', '1');
INSERT INTO `migrations` VALUES ('2015_08_12_082354_create_filmcrew_profiles_table', '1');
INSERT INTO `migrations` VALUES ('2015_08_12_082354_create_production_profile_pivot_table', '1');
INSERT INTO `migrations` VALUES ('2015_08_12_082354_create_tracks_table', '1');
INSERT INTO `migrations` VALUES ('2015_08_12_083354_create_language_page_table', '1');
INSERT INTO `migrations` VALUES ('2015_08_17_104125_create_licence_user_pivot_table', '1');
INSERT INTO `migrations` VALUES ('2015_08_17_123648_create_images_table', '1');
INSERT INTO `migrations` VALUES ('2015_08_18_063501_create_projects_table', '1');
INSERT INTO `migrations` VALUES ('2015_08_18_063653_create_workshops_table', '1');
INSERT INTO `migrations` VALUES ('2015_08_18_085514_create_productions_user_pivot_table', '1');
INSERT INTO `migrations` VALUES ('2015_08_18_115111_create_vimeos_table', '1');
INSERT INTO `migrations` VALUES ('2015_08_18_115134_create_youtubes_table', '1');
INSERT INTO `migrations` VALUES ('2015_08_18_115540_create_soundclouds_table', '1');
INSERT INTO `migrations` VALUES ('2015_08_19_101441_create_jobs_table', '1');
INSERT INTO `migrations` VALUES ('2015_08_19_101551_create_job_user_pivot_table', '1');
INSERT INTO `migrations` VALUES ('2015_08_19_104222_create_messages_table', '1');
INSERT INTO `migrations` VALUES ('2015_08_21_110458_create_production_types_table', '1');
INSERT INTO `migrations` VALUES ('2015_09_12_082354_create_amators_table', '1');
INSERT INTO `migrations` VALUES ('2015_09_12_082354_create_dancers_table', '1');
INSERT INTO `migrations` VALUES ('2015_09_12_082354_create_filmcrews_table', '1');
INSERT INTO `migrations` VALUES ('2015_09_12_082354_create_others_table', '1');
INSERT INTO `migrations` VALUES ('2015_09_12_082354_create_photographers_table', '1');
INSERT INTO `migrations` VALUES ('2015_09_12_082354_create_voices_table', '1');
INSERT INTO `migrations` VALUES ('2015_09_12_085439_create_actors_table', '1');
INSERT INTO `migrations` VALUES ('2015_09_14_135404_create_employers_table', '1');
INSERT INTO `migrations` VALUES ('2015_10_12_182354_add_filmcrews_profile_id', '1');

-- ----------------------------
-- Table structure for others
-- ----------------------------
DROP TABLE IF EXISTS `others`;
CREATE TABLE `others` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `ethnicity_id` int(10) unsigned NOT NULL,
  `talent_id` int(10) unsigned NOT NULL,
  `accent` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `spoken_languages` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `agent` tinyint(1) DEFAULT NULL,
  `show_length` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kids_show` tinyint(1) DEFAULT NULL,
  `profesional_studies` tinyint(1) DEFAULT NULL,
  `speciality_studies` tinyint(1) DEFAULT NULL,
  `speciality_year` smallint(5) unsigned DEFAULT NULL,
  `speciality_institution` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `faculty` tinyint(1) DEFAULT NULL,
  `faculty_student` tinyint(1) DEFAULT NULL,
  `faculty_year` smallint(5) unsigned DEFAULT NULL,
  `faculty_institution` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `faculty_domain` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `equipment` text COLLATE utf8_unicode_ci,
  `info` text COLLATE utf8_unicode_ci,
  `production_types` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `others_user_id_index` (`user_id`),
  KEY `others_ethnicity_id_index` (`ethnicity_id`),
  KEY `others_talent_id_index` (`talent_id`),
  CONSTRAINT `others_talent_id_foreign` FOREIGN KEY (`talent_id`) REFERENCES `othertalents` (`id`) ON DELETE CASCADE,
  CONSTRAINT `others_ethnicity_id_foreign` FOREIGN KEY (`ethnicity_id`) REFERENCES `ethnicities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `others_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of others
-- ----------------------------
INSERT INTO `others` VALUES ('1', '48', '181', '4', 'Nu am', 'Franceza', '1', '4 ore', '0', '1', '0', '2001', 'Ivana Chubbuck-Mandragora Movies Acting Studio', '1', '0', '1982', 'Facultatea de Informatica', 'IT', 'nikon d3000, nikkor 35mm si 18-55', 'A laudantium quo ea voluptates officiis. Quia voluptatibus est et eos similique animi cupiditate cupiditate. Ducimus eum totam maxime quis dolorem et est quisquam. Illo corporis quod fugiat error aut. Quaerat ipsum doloribus animi ut et et. Recusandae autem possimus ut est voluptatem qui. Iusto sed sequi voluptas expedita voluptates reiciendis explicabo optio. Voluptatem non inventore nisi nemo. Natus aut provident ipsum eligendi autem. Minus a aliquam ut dolores ipsa. Hic ut eos necessitatibus modi minus maiores in. Voluptates distinctio dolor eaque omnis perferendis omnis debitis. Tenetur ratione quia accusamus amet.', 'Doloremque quia tempora blanditiis expedita optio libero. In soluta reiciendis nisi perspiciatis quis quo. Inventore quas consequuntur et soluta nihil. Nulla ut eum consequatur culpa ut aut quo qui. Aut soluta quis voluptatem. Voluptatem recusandae reiciendis excepturi pariatur consectetur reprehenderit voluptatem aut. Fuga veritatis impedit repellendus incidunt animi nostrum reiciendis.', '2015-08-19 13:29:16', '2015-08-19 10:13:38');
INSERT INTO `others` VALUES ('2', '17', '25', '5', 'fara accent', 'Italiana', '0', '20 Minute', '0', '1', '1', '1996', 'Universitatea Nationala de Arta Teatrala si Cinematografica Ion Luca Caragiale Bucuresti', '1', '0', '1999', 'Cluj -Universitatea Babes-Bolyai - Facultatea de Film si TV', 'Facultatea de Teatru, Specializarea Papusi si Marionete', 'nikon d3000, nikkor 35mm si 18-55', 'Voluptas eius ipsam perspiciatis nihil. Aut dicta ullam repellat provident quis. Voluptatum quisquam sunt eos et sed. Tempore quasi et consequatur ea mollitia natus. Molestias et illo fugiat ducimus ut est. Ipsam eius quo ut fuga iste consequuntur eligendi. Cupiditate natus ut eos accusantium pariatur unde accusamus. Placeat suscipit eius autem et deleniti et. Veritatis quo labore ea vel vero. Quaerat ab modi molestiae dolores expedita. Qui ut sit natus hic similique alias.', 'Deleniti quibusdam aut vero animi quia sunt et ea. Sit maxime non voluptate voluptas placeat. Repellendus eaque soluta autem. Repudiandae et sit voluptatem delectus accusamus rerum dolor. Dolores commodi aspernatur enim inventore quis. Tenetur ut expedita qui nihil impedit. Asperiores sint dolorum asperiores in. Aut magni dolore sapiente recusandae et et molestiae. Qui dignissimos nam nulla voluptas et accusantium. Nulla ipsam esse et dolor incidunt possimus illo. Rerum aut suscipit neque ea.', '2015-08-10 20:43:42', '2015-08-12 13:54:34');
INSERT INTO `others` VALUES ('3', '38', '61', '7', 'Nu', 'Engleza incepator, Franceza avansat si Spaniola mediu', '1', '30 Minute', '0', '0', '0', '2004', 'Colegiul National de Arte', '0', '1', '1984', 'Facultatea de Informatica', 'Cinema and Television', 'nikon d3000, nikkor 35mm si 18-55', 'Molestiae repudiandae quod soluta atque. Sequi id cupiditate aut a iure ipsa ut. Velit vero molestiae a molestiae sed mollitia omnis. Nam natus unde quae. Rem voluptatem eius in molestiae officiis rem et. Laboriosam quisquam dolore ipsa aut incidunt facilis numquam. Ipsa consectetur eveniet et debitis et. Et mollitia maiores ad vel non nostrum dolore. Et molestiae minima distinctio sed. Hic rem molestias iste iste. Nostrum nobis at beatae et.', 'Aut dolore in impedit deleniti nostrum ea eaque ut. Ipsum dolorem asperiores est veniam ex fugit aut. Non inventore minus ad a itaque. Aut iusto et sapiente vel natus ut. Totam est odio adipisci consequatur. Voluptas aliquid sit aut dolores. Qui mollitia aut quas omnis. Animi soluta tenetur cupiditate numquam impedit. Fugiat et laborum voluptatem vitae sunt est. Qui sapiente ullam natus tenetur.', '2015-09-01 06:00:29', '2015-08-21 02:16:29');
INSERT INTO `others` VALUES ('4', '44', '127', '6', 'Moldovean', 'Italiana', '0', '1 ora', '1', '0', '1', '2014', 'UNATC I.L. Caragiale sectia Arta Actorului', '1', '0', '1998', 'Cluj -Universitatea Babes-Bolyai - Facultatea de Film si TV', 'Facultatea de Teatru, Specializarea Papusi si Marionete', 'Nikon D3, lentile cu plaja focala intre 28 si 200mm, lumini de studio, flash', 'Enim qui ut molestiae autem enim facilis. Iure debitis debitis laboriosam mollitia. Similique quod fuga saepe tempora qui nesciunt. Nostrum explicabo perferendis magni excepturi totam qui. Itaque nisi esse recusandae quia necessitatibus. Ea et ratione tempore natus. Quia at aperiam quasi omnis quae itaque facere iure. Natus fugit rerum voluptas qui dolor. Eum nulla voluptatibus provident delectus sed eius eos doloremque.', 'Natus modi odio eius voluptas omnis. Blanditiis ab est molestiae non optio. Sint ut harum amet quo laudantium reprehenderit eaque. Qui non et aperiam nihil eos dolor voluptatum consectetur. Sint tempore necessitatibus repellendus hic in alias natus. Sint voluptatem voluptas voluptas nisi at inventore sed ipsa. Libero sint ipsam facere sint sit. Sint amet voluptatum mollitia odio et qui tempore. Vero aut aut est suscipit commodi tempora labore. Distinctio ea in nisi eligendi non maiores suscipit ut.', '2015-08-12 19:27:46', '2015-08-22 08:02:40');
INSERT INTO `others` VALUES ('5', '38', '127', '7', 'fara accent', 'Engleza incepator, Franceza avansat si Spaniola mediu', '1', '4 ore', '1', '1', '1', '1982', 'UNATC I.L. Caragiale sectia Arta Actorului', '0', '0', '2012', 'Bucuresti - Universitatea Hyperion - Facultatea de Arte', 'Cinema and Television', 'Canon 1DS Mark II, Canon 50D, zoom Canon 17-40mm/4 L, zoom Canon 24-105mm/4 L, Canon 50mm/1.8, Canon 70-200mm/4 L, Canon 100mm/2.8, Samyang 8mm/3.5, Canon Speedlite 580 EX II, Canon Speedlite 580 EX, flash-uri studio Quantuum 600W si 300w, power pack, stative, umbrele, trigger wireless, blende.', 'Quod possimus sint ipsa aut dignissimos error. Qui labore dolor nihil animi non quis. Aut assumenda inventore libero eos. Sequi sunt quibusdam consectetur saepe. Nemo sit libero architecto voluptatum harum. Perspiciatis sint culpa temporibus ad voluptate est iste. Vitae assumenda nam nihil quo rerum. Amet deleniti ea blanditiis aperiam. Et ratione occaecati maiores sunt quam. Atque sapiente minima ab explicabo sapiente ipsum. Veniam aut voluptas quos esse. Blanditiis aut qui rerum dicta.', 'Nam ipsum suscipit vel quis qui non cupiditate fugit. Dolore assumenda recusandae voluptatem pariatur sit quia deleniti. Tenetur aspernatur sunt illo distinctio temporibus quia optio. Ut similique architecto maxime eum quod autem magnam. Dolorem omnis nesciunt ex. Blanditiis impedit qui eaque quidem dicta. Consequatur porro occaecati quasi iure. At velit vel ipsam odit doloribus. Vitae quis at commodi voluptas pariatur repellendus animi. Asperiores voluptatem earum autem ducimus molestias non exercitationem.', '2015-08-12 09:13:53', '2015-08-12 06:54:48');
INSERT INTO `others` VALUES ('6', '29', '160', '5', 'Moldovean', 'Franceza', '1', '30 Minute', '0', '0', '1', '1992', 'Universitatea Nationala de Arta Teatrala si Cinematografica Ion Luca Caragiale Bucuresti', '0', '1', '1990', 'Facultatea de Informatica', 'IT', 'Canon 1DS Mark II, Canon 50D, zoom Canon 17-40mm/4 L, zoom Canon 24-105mm/4 L, Canon 50mm/1.8, Canon 70-200mm/4 L, Canon 100mm/2.8, Samyang 8mm/3.5, Canon Speedlite 580 EX II, Canon Speedlite 580 EX, flash-uri studio Quantuum 600W si 300w, power pack, stative, umbrele, trigger wireless, blende.', 'Quos officia omnis vel ut qui. Earum voluptatem voluptas quis ea id magni. Rerum atque illo velit ducimus. Ut corrupti aliquam reprehenderit maxime cum quia provident voluptas. Cupiditate et quam quaerat consequatur similique. Molestiae voluptatem dolor rerum fugit corporis. Blanditiis odio iste harum dolorum. Vel exercitationem eligendi suscipit consequuntur adipisci animi. Praesentium ut optio quisquam.', 'Est qui sapiente et quidem et consequatur laborum. Modi ab distinctio ducimus aut ipsum distinctio dolore. Sunt fuga aliquam velit consectetur. Nihil illum eligendi sunt dolores. Non blanditiis aut placeat. Illo et sint consequatur provident qui maiores maxime dolorum. Quia dolores assumenda sint quos. Aut a cupiditate placeat aut. Tempora eligendi tenetur aut sint harum doloribus. Iure qui est quam doloribus impedit.', '2015-08-25 18:26:42', '2015-08-10 17:02:11');
INSERT INTO `others` VALUES ('7', '18', '130', '6', 'Oltean', 'Engleza', '0', '2 ore', '1', '1', '0', '2015', 'Mandragora Movies Film Academy', '1', '1', '2005', 'Facultatea de Informatica', 'actorie', 'Studio foto profesional', 'Corrupti illo ipsam dolorem possimus. Iste aut quam minus quam. Nulla alias saepe autem aut ipsa eos. Non exercitationem laudantium delectus. Error minus ipsum nobis ipsum optio. Eum veniam molestiae in culpa et totam sed. Ipsam ipsum nobis placeat iste debitis sunt. Eum quis occaecati sit beatae nemo est iste. Qui atque aut accusantium voluptatem mollitia. Impedit aliquid minima itaque. Id dolores placeat veniam tenetur corrupti. Rerum reiciendis eum quo iusto. Nihil ut sunt cumque. Aut consequuntur vel accusamus alias rerum ut quaerat.', 'Doloremque aut laboriosam nobis voluptas nam ut eum enim. Sint veritatis non esse totam fugiat et atque. Ullam qui deleniti sit quod. Eum quidem alias laborum saepe. Sit blanditiis architecto laboriosam vero odio nulla. Blanditiis illum qui non libero. Eaque excepturi deleniti quia dolores esse voluptates. Exercitationem voluptatem possimus voluptas in ab perspiciatis doloremque. Corporis cupiditate est provident saepe sint nostrum. Nostrum et ut sapiente repellat qui. Id omnis dolorum voluptates aperiam et porro.', '2015-09-02 12:43:11', '2015-08-24 05:33:41');
INSERT INTO `others` VALUES ('8', '22', '68', '2', 'Nu am', 'Engleza', '1', '3 ore', '0', '0', '1', '2008', 'Ivana Chubbuck-Mandragora Movies Acting Studio', '0', '1', '2015', 'Bucuresti - Universitatea Hyperion - Facultatea de Arte', 'filologie intensiv limba engleza', 'Nikon D3, lentile cu plaja focala intre 28 si 200mm, lumini de studio, flash', 'Occaecati quidem sed fugiat quam molestiae incidunt. Possimus velit beatae tempora reprehenderit. Quae doloribus voluptas aut unde eos ea. Qui rerum fuga qui nisi culpa dicta sit. Qui quia aliquid aut repellendus et velit ipsum. Eius ut a ut est. Ipsum ad dicta magnam fugit. Dolorem qui officiis et quis cupiditate. Et aut quos et dolores at.', 'Ducimus molestiae distinctio fugiat. Error itaque nihil et nisi aut consequuntur aperiam esse. Non delectus dignissimos et a rem voluptates expedita quibusdam. Voluptate et quaerat sit aspernatur. Qui ut cupiditate doloribus natus. Mollitia pariatur quo ex autem. Omnis inventore quia blanditiis quam. Doloremque qui nesciunt iure natus amet. At est vitae eum eos neque sint. Quia est neque sequi ipsum qui. Possimus molestiae perferendis velit reprehenderit sunt ut quas assumenda.', '2015-08-18 23:02:07', '2015-08-25 15:35:22');
INSERT INTO `others` VALUES ('9', '7', '148', '6', 'Romana', 'Engleza incepator, Franceza avansat si Spaniola mediu', '0', '1 ora', '0', '0', '0', '1999', 'Colegiul National de Arte', '1', '1', '1989', 'Facultatea de Informatica', 'filologie intensiv limba engleza', 'Studio foto profesional', 'Rerum quas quisquam illum. Reiciendis perferendis facere animi perspiciatis. Numquam nulla ut at repudiandae. Sequi et voluptatibus atque consequuntur rerum sed. Quae vel veritatis earum dolorem molestiae. Ducimus voluptas minus sint blanditiis nam pariatur. Doloremque et a ut est deserunt sed ipsum. Fugit et recusandae fugiat. Non autem ut aut autem natus debitis eos.', 'Optio debitis sapiente incidunt ut. Maxime et quo quia alias ad totam expedita. Vel sequi voluptatem qui non. Qui minus ut delectus aut occaecati voluptates sed explicabo. Animi et voluptatibus inventore. Odio in officia iure id maiores et dicta. Maiores odio dolorem voluptatem inventore. Aliquid dolor blanditiis laudantium optio qui modi. Possimus ut in ipsum reprehenderit porro sint ut. Suscipit eos pariatur voluptatem eos reprehenderit maxime. Voluptatem sint et quasi repudiandae sit et. Nihil quisquam voluptatem pariatur.', '2015-08-21 05:31:48', '2015-08-09 17:05:13');
INSERT INTO `others` VALUES ('10', '9', '189', '7', 'Nu', 'Engleza', '1', '20 Minute', '1', '0', '1', '2013', 'Ivana Chubbuck-Mandragora Movies Acting Studio', '0', '0', '1993', 'Bucuresti - Universitatea Hyperion - Facultatea de Arte', 'IT', 'Canon 5D Mk II, obiective fixe canon, zeiss si sigma - 24mm, 35mm, 50mm, 85mm, 135mm', 'Alias voluptatem doloribus quam facilis quo. Velit autem aliquid inventore eveniet distinctio. Officia voluptatum quia sit ipsum animi natus. Omnis quam neque vel est aut. Nesciunt sed neque aut neque. Sint nemo quod voluptatum fugiat aut. Est similique ullam sint libero sed occaecati ea. Eligendi debitis rerum quibusdam quibusdam accusamus est. Ducimus assumenda praesentium molestias nobis.', 'Rem aut ipsam accusantium placeat qui. Optio ullam doloremque necessitatibus aut fugiat soluta id. Sint quia officiis sit alias. Temporibus nemo voluptatem voluptatem quaerat dolores. Dolor modi et cum aut minima itaque. Pariatur molestias repudiandae libero ut iure deleniti perspiciatis omnis. Perferendis ut id temporibus eveniet itaque similique. Sunt sed nulla facere nisi enim saepe.', '2015-08-11 15:45:14', '2015-08-30 05:30:33');

-- ----------------------------
-- Table structure for othertalents
-- ----------------------------
DROP TABLE IF EXISTS `othertalents`;
CREATE TABLE `othertalents` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nume` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of othertalents
-- ----------------------------
INSERT INTO `othertalents` VALUES ('1', 'Ilusionist', 'Illusionist', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `othertalents` VALUES ('2', 'Papusar', 'Puppetteer', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `othertalents` VALUES ('3', 'Mim', 'Mime', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `othertalents` VALUES ('4', 'Magician', 'Magician', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `othertalents` VALUES ('5', 'Comediant', 'Comic', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `othertalents` VALUES ('6', 'Cascador', 'Stunt', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `othertalents` VALUES ('7', 'Alt talent', 'Other talent', '2015-09-08 22:29:29', '2015-09-08 22:29:29');

-- ----------------------------
-- Table structure for pages
-- ----------------------------
DROP TABLE IF EXISTS `pages`;
CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned NOT NULL,
  `password` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(3) unsigned NOT NULL,
  `order` int(10) unsigned NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `published_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of pages
-- ----------------------------
INSERT INTO `pages` VALUES ('1', '0', '', '1', '0', 'http://lorempixel.com/640/480/?79180', '2015-08-11 13:03:22', '2015-08-24 02:57:03', '2015-09-06 22:49:19');
INSERT INTO `pages` VALUES ('2', '0', '', '1', '0', 'http://lorempixel.com/640/480/?88571', '2015-08-31 10:28:48', '2015-08-13 20:05:33', '2015-09-01 10:36:53');
INSERT INTO `pages` VALUES ('3', '0', '', '1', '0', 'http://lorempixel.com/640/480/?44262', '2015-08-26 06:31:52', '2015-08-24 18:57:19', '2015-09-05 08:30:35');
INSERT INTO `pages` VALUES ('4', '0', '', '1', '0', 'http://lorempixel.com/640/480/?66177', '2015-08-24 02:57:52', '2015-08-20 20:04:07', '2015-08-31 09:29:17');
INSERT INTO `pages` VALUES ('5', '0', '', '1', '0', 'http://lorempixel.com/640/480/?71053', '2015-09-03 06:16:05', '2015-09-05 22:41:52', '2015-08-12 18:32:01');
INSERT INTO `pages` VALUES ('6', '0', '', '1', '0', 'http://lorempixel.com/640/480/?11909', '2015-09-05 11:02:30', '2015-08-26 19:48:49', '2015-08-28 22:10:22');
INSERT INTO `pages` VALUES ('7', '0', '', '1', '0', 'http://lorempixel.com/640/480/?34652', '2015-08-22 19:07:11', '2015-08-09 01:06:52', '2015-08-29 15:11:42');
INSERT INTO `pages` VALUES ('8', '0', '', '1', '0', 'http://lorempixel.com/640/480/?99880', '2015-08-27 05:50:16', '2015-08-22 03:54:15', '2015-08-13 10:39:33');
INSERT INTO `pages` VALUES ('9', '0', '', '1', '0', 'http://lorempixel.com/640/480/?55644', '2015-08-26 13:04:08', '2015-08-19 01:47:34', '2015-08-16 20:30:43');
INSERT INTO `pages` VALUES ('10', '0', '', '1', '0', 'http://lorempixel.com/640/480/?86965', '2015-08-26 17:51:34', '2015-08-31 06:30:34', '2015-09-06 21:37:44');
INSERT INTO `pages` VALUES ('11', '0', '', '1', '0', 'http://lorempixel.com/640/480/?50963', '2015-08-24 07:42:58', '2015-09-06 12:37:31', '2015-08-20 22:51:45');
INSERT INTO `pages` VALUES ('12', '0', '', '1', '0', 'http://lorempixel.com/640/480/?61263', '2015-08-28 22:55:47', '2015-09-05 04:54:35', '2015-08-23 16:38:50');
INSERT INTO `pages` VALUES ('13', '0', '', '1', '0', 'http://lorempixel.com/640/480/?17071', '2015-08-24 15:03:39', '2015-08-12 02:42:28', '2015-08-26 13:55:55');
INSERT INTO `pages` VALUES ('14', '0', '', '1', '0', 'http://lorempixel.com/640/480/?65389', '2015-08-20 16:01:15', '2015-09-06 05:41:02', '2015-08-15 06:47:22');
INSERT INTO `pages` VALUES ('15', '0', '', '1', '0', 'http://lorempixel.com/640/480/?31630', '2015-08-13 19:49:36', '2015-08-31 13:39:01', '2015-08-12 04:27:44');
INSERT INTO `pages` VALUES ('16', '0', '', '1', '0', 'http://lorempixel.com/640/480/?77137', '2015-08-29 14:25:30', '2015-08-24 08:27:34', '2015-08-23 06:03:45');
INSERT INTO `pages` VALUES ('17', '0', '', '1', '0', 'http://lorempixel.com/640/480/?56903', '2015-08-26 15:58:31', '2015-08-25 01:16:59', '2015-09-05 14:00:12');
INSERT INTO `pages` VALUES ('18', '0', '', '1', '0', 'http://lorempixel.com/640/480/?47504', '2015-08-25 01:08:34', '2015-08-09 08:38:42', '2015-08-08 22:43:20');
INSERT INTO `pages` VALUES ('19', '0', '', '1', '0', 'http://lorempixel.com/640/480/?54141', '2015-08-09 18:41:59', '2015-08-20 07:00:49', '2015-08-21 21:55:19');
INSERT INTO `pages` VALUES ('20', '0', '', '1', '0', 'http://lorempixel.com/640/480/?21911', '2015-08-23 02:58:14', '2015-08-25 05:07:47', '2015-09-07 10:49:25');
INSERT INTO `pages` VALUES ('21', '0', '', '1', '0', 'http://lorempixel.com/640/480/?59139', '2015-09-04 02:14:46', '2015-08-18 07:02:54', '2015-08-22 17:53:28');
INSERT INTO `pages` VALUES ('22', '0', '', '1', '0', 'http://lorempixel.com/640/480/?27442', '2015-08-14 05:05:48', '2015-08-12 07:06:23', '2015-08-26 17:08:53');
INSERT INTO `pages` VALUES ('23', '0', '', '1', '0', 'http://lorempixel.com/640/480/?33662', '2015-09-05 18:07:12', '2015-08-24 07:08:14', '2015-08-31 16:37:45');
INSERT INTO `pages` VALUES ('24', '0', '', '1', '0', 'http://lorempixel.com/640/480/?81325', '2015-09-08 16:08:00', '2015-08-09 10:37:06', '2015-09-01 02:53:53');
INSERT INTO `pages` VALUES ('25', '0', '', '1', '0', 'http://lorempixel.com/640/480/?56576', '2015-08-20 19:56:14', '2015-08-29 05:26:36', '2015-09-02 16:57:50');
INSERT INTO `pages` VALUES ('26', '0', '', '1', '0', 'http://lorempixel.com/640/480/?31924', '2015-08-30 21:10:26', '2015-08-26 18:13:57', '2015-09-01 23:00:22');
INSERT INTO `pages` VALUES ('27', '0', '', '1', '0', 'http://lorempixel.com/640/480/?84599', '2015-08-23 09:25:36', '2015-09-02 12:28:51', '2015-09-08 21:54:17');
INSERT INTO `pages` VALUES ('28', '0', '', '1', '0', 'http://lorempixel.com/640/480/?68718', '2015-09-07 06:24:14', '2015-09-01 20:05:01', '2015-09-07 04:01:14');
INSERT INTO `pages` VALUES ('29', '0', '', '1', '0', 'http://lorempixel.com/640/480/?20077', '2015-09-07 20:43:15', '2015-08-12 10:24:50', '2015-08-10 05:37:18');
INSERT INTO `pages` VALUES ('30', '0', '', '1', '0', 'http://lorempixel.com/640/480/?34558', '2015-08-22 01:27:06', '2015-08-29 06:54:32', '2015-08-28 15:47:03');

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of password_resets
-- ----------------------------

-- ----------------------------
-- Table structure for photographers
-- ----------------------------
DROP TABLE IF EXISTS `photographers`;
CREATE TABLE `photographers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `ethnicity_id` int(10) unsigned NOT NULL,
  `spoken_languages` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `profesional_studies` tinyint(1) DEFAULT NULL,
  `speciality_studies` tinyint(1) DEFAULT NULL,
  `speciality_year` smallint(5) unsigned DEFAULT NULL,
  `speciality_institution` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `faculty` tinyint(1) DEFAULT NULL,
  `faculty_student` tinyint(1) DEFAULT NULL,
  `faculty_year` smallint(5) unsigned DEFAULT NULL,
  `faculty_institution` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `faculty_domain` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `info` text COLLATE utf8_unicode_ci,
  `production_types` text COLLATE utf8_unicode_ci,
  `equipment` text COLLATE utf8_unicode_ci,
  `agency` tinyint(1) DEFAULT NULL,
  `agency_name` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `agency_contact` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `photographers_user_id_index` (`user_id`),
  KEY `photographers_ethnicity_id_index` (`ethnicity_id`),
  CONSTRAINT `photographers_ethnicity_id_foreign` FOREIGN KEY (`ethnicity_id`) REFERENCES `ethnicities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `photographers_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of photographers
-- ----------------------------
INSERT INTO `photographers` VALUES ('1', '45', '40', 'Italiana', '1', '1', '2015', 'Ivana Chubbuck-Mandragora Movies Acting Studio', '1', '0', '1986', 'Bucuresti - Universitatea Hyperion - Facultatea de Arte', 'IT', 'http://www.Dima.biz/facilis-minus-non-porro-i', 'Vel et vero explicabo dolor voluptatem. Et non eos delectus ad quas nam. Vitae natus sunt sed accusantium iusto asperiores. Officia vel eveniet nostrum illum. Perferendis consequuntur rerum et temporibus nostrum. Aspernatur fugiat consequatur quis perspiciatis libero atque. Eligendi maxime ut eos. Temporibus doloribus quasi in dignissimos. Sint molestiae vero sunt sapiente. Et nisi eos voluptates tempora libero.', 'Impedit nam ea ut at. Error odit quia quis fugit est. Aliquam quos sed et et consequatur. Maiores debitis minima minus dolorem aperiam neque perspiciatis. Occaecati quasi quae voluptatem voluptatem ipsum consequatur. Et molestias est commodi nesciunt. Totam consequuntur enim magni veniam natus. Facere quis consequatur voluptas. Officia rerum velit deserunt facere maiores accusantium dolores. Ab at esse recusandae placeat. Voluptatem distinctio est eaque aut saepe non rerum.', 'Canon 5D Mk II, obiective fixe canon, zeiss si sigma - 24mm, 35mm, 50mm, 85mm, 135mm', '1', 'Aut vitae perspiciatis tempora numquam est dolor id.', '0214695309', '2015-08-10 07:29:58', '2015-08-12 02:51:50');
INSERT INTO `photographers` VALUES ('2', '25', '190', 'Engleza incepator, Franceza avansat si Spaniola mediu', '1', '1', '2003', 'Ivana Chubbuck-Mandragora Movies Acting Studio', '1', '1', '2015', 'Bucuresti - Universitatea Nationala de Arta Teatrala si Cinematografica “I.L.Caragiale” (UNATC)', 'Cinema and Television', 'http://www.Dan.com/deserunt-enim-aut-veniam-e', 'Ullam quis dolores est qui et voluptatum eaque. Ut facere esse velit non dolores. Excepturi ullam expedita adipisci aut. Voluptatem alias qui ut quis qui ducimus. Quia cumque non ut voluptate laborum voluptatum. Ipsa beatae est ut. Iure dicta odit distinctio suscipit. In qui natus sit minus explicabo sunt exercitationem. Consectetur quidem quo eum ullam eligendi. Debitis perspiciatis id eos voluptatem corrupti.', 'Corrupti optio nisi recusandae ut. At sapiente eos ut et quidem illum. Pariatur atque architecto et quia fuga. Incidunt fugit aut perspiciatis ut qui non. Consequatur quis eaque nostrum ea dolorum. Dolor ut error dolores neque rerum consectetur. Sed tempora impedit ut alias sit eos accusantium dolores. Ut sunt voluptatem nobis. Vel et in distinctio enim nihil nesciunt tempora. Ab ut et ex et in possimus. Porro voluptatem distinctio est tempore ab. Dolorem ratione dolorum officiis tenetur.', 'nikon d3000, nikkor 35mm si 18-55', '1', 'Qui unde quo qui at totam. Architecto saepe ipsam nobis ut.', '0701597553', '2015-08-10 21:34:17', '2015-09-07 12:57:34');
INSERT INTO `photographers` VALUES ('3', '20', '144', 'Italiana', '1', '1', '1994', 'Mandragora Movies Film Academy', '0', '0', '2009', 'Facultatea de Informatica', 'filologie intensiv limba engleza', 'https://Veres.com/illum-consequatur-delectus-', 'Aut ut sed illum unde. Dolorem corporis ab quis dolores. Pariatur architecto sit sed quas et et nulla. At debitis debitis nobis aspernatur quis perspiciatis. Perspiciatis cumque voluptatem maxime non. A voluptatibus id quaerat magni doloribus autem. Ex non dignissimos voluptas nihil in incidunt et. Praesentium maxime blanditiis provident similique. Non provident doloribus repellendus earum non est. Ut earum ratione officia repudiandae aut. Tempore et fuga temporibus quibusdam ipsum sit. Nisi id qui quos et et ipsa. Eum iusto excepturi qui illo molestiae qui.', 'Numquam quos voluptatem reprehenderit et voluptatem accusamus. Magnam atque consequatur et nemo qui. Ut dicta doloribus voluptatem vitae officiis fugit similique. Corporis iusto ut et molestias ratione. Deserunt nostrum blanditiis commodi incidunt vel. Esse placeat porro eum. Ut dignissimos voluptatibus rem. Repellat vel totam recusandae vel quia libero.', 'Studio foto profesional', '1', 'Fugiat distinctio pariatur voluptatem ullam.', '0787988335', '2015-08-28 19:40:38', '2015-08-21 21:32:13');
INSERT INTO `photographers` VALUES ('4', '13', '99', 'Engleza', '1', '1', '2009', 'Spiru Haret Bucuresti', '0', '1', '2005', 'Cluj -Universitatea Babes-Bolyai - Facultatea de Film si TV', 'Cinema and Television', 'http://www.Achim.net/architecto-magni-fugiat-', 'Quod atque ut consequuntur non sunt minus. Itaque illum quia harum libero deserunt id. Incidunt excepturi aut ipsa voluptatem hic est. Aut itaque iure est non quas tempora rerum. Facere est ullam culpa reprehenderit quas ipsum. Qui autem deleniti rerum ut iure. Incidunt in excepturi sint commodi. Dolore mollitia veniam est quidem velit nisi. Provident dolor consequuntur est molestiae quia et. Ut laboriosam eius eum autem. Et voluptas saepe sint consequuntur voluptatibus quod.', 'Vel voluptatem distinctio qui tempora placeat. Qui voluptatibus explicabo consequatur nulla vitae temporibus debitis. Libero provident repudiandae maiores veritatis et sequi. Quia sit sint et inventore et harum. Nihil sed omnis soluta aut. Suscipit sunt ut aliquam et voluptate nihil. Delectus temporibus hic quam placeat aut et recusandae. Quod consequatur voluptatibus rerum vel repellat voluptas. Est laborum explicabo ut vero voluptas deserunt.', 'nikon d3000, nikkor 35mm si 18-55', '1', 'Quos nostrum explicabo qui deleniti sequi distinctio.', '0376995237', '2015-08-19 10:00:55', '2015-08-16 10:44:06');
INSERT INTO `photographers` VALUES ('5', '36', '123', 'Engleza', '0', '1', '2008', 'UNATC I.L. Caragiale sectia Arta Actorului', '1', '1', '2014', 'Cluj -Universitatea Babes-Bolyai - Facultatea de Film si TV', 'Facultatea de Teatru, Specializarea Papusi si Marionete', 'https://Sarbu.biz/aspernatur-dolor-blanditiis', 'Odit illum fugiat nam. Aliquid quam recusandae pariatur. Et facere nemo et corrupti dicta. Sit autem ipsa repudiandae et. Reiciendis cum ratione iste modi. Necessitatibus quam animi qui. Dolor repellat nihil ad possimus voluptatem itaque velit. Quasi consequatur molestiae perferendis numquam magnam quia repellendus.', 'Dicta quam consectetur accusamus totam. Reiciendis delectus incidunt doloribus quis in. Est ad magnam nesciunt fugiat ipsam molestiae. Aperiam delectus iste eveniet nam. Architecto assumenda minus perferendis velit. Repellendus at quod deserunt qui quasi corrupti qui. Sapiente et dolorem doloribus occaecati rerum cumque.', 'Canon 5D Mk II, obiective fixe canon, zeiss si sigma - 24mm, 35mm, 50mm, 85mm, 135mm', '1', 'Officiis quae est sit earum dignissimos. Quia est natus rerum consectetur.', '0748127185', '2015-08-20 12:22:42', '2015-08-13 00:22:58');
INSERT INTO `photographers` VALUES ('6', '37', '108', 'Italiana', '1', '0', '2008', 'Spiru Haret Bucuresti', '0', '1', '1982', 'Bucuresti - Universitatea Hyperion - Facultatea de Arte', 'Cinema and Television', 'http://Lupu.net/', 'Deleniti laborum autem officia omnis. Impedit quia excepturi et quo. Aut et aut commodi asperiores sint repellendus modi. Est ea magni ad facilis. Qui deserunt dolor asperiores aperiam necessitatibus temporibus eum aliquid. Maiores deserunt qui dolorum est autem. Omnis eligendi adipisci maiores eligendi laboriosam. Magni voluptas et quod recusandae enim non est. Quo perferendis facere sint odit voluptate.', 'Vero unde rem iure voluptate expedita maiores non. Voluptatem eum et aut tempora id laudantium aperiam. Ipsum veniam rerum minus ut. Aut aut molestiae in atque earum et saepe. Consequuntur vel quaerat blanditiis non sunt ab earum. Enim minus molestiae fuga velit atque nulla quasi commodi. Velit minima fugit consequatur nisi tempore itaque sit eaque. Tempore repellat quam aut eveniet quos vitae nobis a. Ullam non aut odit accusamus sit voluptas.', 'nikon d3000, nikkor 35mm si 18-55', '0', 'Ipsam voluptatum reprehenderit ad vel adipisci quod. Quidem sint temporibus fugi', '0316122025', '2015-08-31 16:15:40', '2015-08-24 02:15:28');
INSERT INTO `photographers` VALUES ('7', '48', '96', 'Engleza', '1', '1', '1984', 'Spiru Haret Bucuresti', '1', '1', '1980', 'Bucuresti - Universitatea Hyperion - Facultatea de Arte', 'Cinema and Television', 'https://www.Nicolescu.biz/nisi-harum-saepe-ad', 'Officia quia nemo aut accusantium suscipit ut ea. Saepe non necessitatibus officia consequatur beatae impedit excepturi totam. Perspiciatis consequuntur sapiente est aperiam. Autem quia ipsum adipisci quam reprehenderit. Repellendus omnis ullam consequuntur aliquam molestiae ut quia. Nulla optio facilis impedit. Odio sequi et et nisi. Qui qui architecto alias repellat placeat harum ex consequatur. Alias illo iste omnis qui eveniet aliquam. Animi eligendi facilis ut animi consectetur sed. Eum aspernatur distinctio qui explicabo quo. Voluptatem ipsum blanditiis distinctio. Officiis velit veniam est et. Eveniet ipsa aut sit quasi aut sit.', 'Ea quas earum officiis id rem. Exercitationem autem beatae pariatur aut. Excepturi libero et in ut sunt laborum sit molestias. Eligendi odio quis maxime ex. Est debitis est non optio maxime ab excepturi. Id non unde ex non possimus. Molestias dolores ex in ut quas accusamus dolorem. Ex tempore ea a est. Et debitis quia in quidem laborum dignissimos at.', 'Nikon D3, lentile cu plaja focala intre 28 si 200mm, lumini de studio, flash', '1', 'Autem ad molestiae autem explicabo assumenda.', '0316102118', '2015-08-27 00:01:09', '2015-08-12 13:50:28');
INSERT INTO `photographers` VALUES ('8', '50', '95', 'Italiana', '1', '0', '1980', 'Universitatea Nationala de Arta Teatrala si Cinematografica Ion Luca Caragiale Bucuresti', '1', '0', '2007', 'Bucuresti - Universitatea Nationala de Arta Teatrala si Cinematografica “I.L.Caragiale” (UNATC)', 'actorie', 'http://Cazacu.com/laboriosam-autem-sit-aut-pe', 'Et adipisci voluptate non quia iste at accusamus. Vel neque unde ad dolor aut pariatur autem. Fugit qui eum vero non recusandae. Aperiam perspiciatis sit aut qui. Ab adipisci earum maiores hic quidem. Est quia sed consequatur illum quia. Est similique quia eos aut quis aspernatur vel. Ipsam alias deserunt repellat modi distinctio ea rerum. Occaecati qui necessitatibus officia quia sequi et. Occaecati quia non velit perspiciatis explicabo voluptatem. Sit aliquam vitae molestiae adipisci ipsam dolores vero. Id dolorem accusantium quia optio mollitia. Consectetur qui incidunt natus.', 'Et dicta cupiditate nisi deleniti. Quia voluptatem possimus adipisci quasi repellendus. Assumenda sed eligendi veritatis error. Nemo voluptas magni hic dolore voluptate. Impedit voluptatem praesentium nemo. Enim maiores id quae ut suscipit voluptatum. Non ipsam harum rerum sunt et aut quia. Ut magni aut asperiores laboriosam ex.', 'Canon 5D Mk II, obiective fixe canon, zeiss si sigma - 24mm, 35mm, 50mm, 85mm, 135mm', '1', 'Maxime voluptas officia esse placeat modi nisi. Vero non laboriosam ipsum omnis ', '0759680281', '2015-08-13 07:36:26', '2015-08-29 03:20:07');
INSERT INTO `photographers` VALUES ('9', '24', '85', 'Engleza incepator, Franceza avansat si Spaniola mediu', '0', '0', '1983', 'Ivana Chubbuck-Mandragora Movies Acting Studio', '1', '0', '2002', 'Facultatea de Informatica', 'Facultatea de Teatru, Specializarea Papusi si Marionete', 'http://www.Paun.org/', 'Saepe tenetur dolores porro ipsum. Minus sapiente eum magnam dolores vel vitae. Explicabo vel atque vitae. Omnis praesentium est alias itaque. Quod architecto non odio dolor voluptatem debitis. Sit doloribus fugiat ullam ipsum inventore cumque qui. Cumque repudiandae quis perspiciatis officiis. Minima dolorem est qui consectetur nulla temporibus. Laudantium nulla minus voluptatum qui.', 'Et reprehenderit repellat et sit corrupti optio esse et. Rerum consequatur non nostrum iste perspiciatis. Tempore natus porro repellat a. Saepe non qui tempore possimus fugit. Vel omnis aut praesentium non consequatur molestiae. Quidem tenetur non accusantium consequatur ut voluptatum error cupiditate. Facere et laborum dolorem dolorum quo. Est nihil rerum provident omnis consequuntur. Aut itaque voluptatem hic eos. Quidem voluptas commodi blanditiis ut quas pariatur debitis. Molestias et laboriosam reiciendis voluptate aut nihil.', 'Canon 1DS Mark II, Canon 50D, zoom Canon 17-40mm/4 L, zoom Canon 24-105mm/4 L, Canon 50mm/1.8, Canon 70-200mm/4 L, Canon 100mm/2.8, Samyang 8mm/3.5, Canon Speedlite 580 EX II, Canon Speedlite 580 EX, flash-uri studio Quantuum 600W si 300w, power pack, stative, umbrele, trigger wireless, blende.', '0', 'Consequatur et aut sint doloribus aut laboriosam.', '0217622005', '2015-09-06 01:49:56', '2015-08-11 12:09:16');
INSERT INTO `photographers` VALUES ('10', '26', '88', 'Engleza incepator, Franceza avansat si Spaniola mediu', '0', '1', '2014', 'UNATC I.L. Caragiale sectia Arta Actorului', '1', '0', '1988', 'Bucuresti - Universitatea Hyperion - Facultatea de Arte', 'Cinema and Television', 'http://www.Gheorghe.org/ipsum-consequatur-bea', 'Veritatis et est odit nam excepturi architecto. Reiciendis vero error voluptatem tenetur repellendus. Fugiat reiciendis ut consequatur corrupti eius dolorem. Qui quas placeat quibusdam deleniti. At iste dolor porro dolorum dolores omnis. Libero ex cum ipsa repellat ex odio. Perferendis dolorem ullam aut et culpa recusandae. Repellat non architecto numquam sit assumenda commodi animi beatae. Perferendis deleniti nulla reprehenderit animi. Dolorem vitae quia vel quo accusantium. Assumenda facere vitae quidem nobis. Sed necessitatibus ipsum sit corrupti consectetur. Dolorem hic reiciendis est neque molestiae.', 'Ab quaerat voluptatum blanditiis ad consectetur facilis dolores. Suscipit culpa consectetur mollitia minus laudantium quae ducimus. In velit quam tempore et fuga dolor ipsa quia. Voluptates sunt voluptatem assumenda nihil quos in. Amet quas et quaerat et autem ut. Exercitationem tempore autem iusto. In unde sunt nihil illum mollitia suscipit ut. Consequatur eos dolorem deleniti reiciendis.', 'nikon d3000, nikkor 35mm si 18-55', '1', 'Ullam architecto debitis aut provident qui. Excepturi repellendus repudiandae au', '0713396354', '2015-08-13 13:35:08', '2015-09-02 20:56:17');

-- ----------------------------
-- Table structure for production_profile
-- ----------------------------
DROP TABLE IF EXISTS `production_profile`;
CREATE TABLE `production_profile` (
  `production_id` int(10) unsigned NOT NULL,
  `profile_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `production_profile_production_id_index` (`production_id`),
  KEY `production_profile_profile_id_index` (`profile_id`),
  CONSTRAINT `production_profile_profile_id_foreign` FOREIGN KEY (`profile_id`) REFERENCES `profiles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `production_profile_production_id_foreign` FOREIGN KEY (`production_id`) REFERENCES `productions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of production_profile
-- ----------------------------
INSERT INTO `production_profile` VALUES ('18', '9', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_profile` VALUES ('13', '2', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_profile` VALUES ('47', '7', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_profile` VALUES ('23', '4', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_profile` VALUES ('37', '3', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_profile` VALUES ('39', '5', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_profile` VALUES ('30', '8', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_profile` VALUES ('29', '5', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_profile` VALUES ('44', '1', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_profile` VALUES ('48', '7', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_profile` VALUES ('5', '3', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_profile` VALUES ('47', '7', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_profile` VALUES ('24', '3', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_profile` VALUES ('28', '1', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_profile` VALUES ('13', '4', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_profile` VALUES ('14', '6', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_profile` VALUES ('14', '5', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_profile` VALUES ('38', '2', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_profile` VALUES ('12', '3', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_profile` VALUES ('30', '9', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_profile` VALUES ('26', '4', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_profile` VALUES ('23', '2', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_profile` VALUES ('13', '1', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_profile` VALUES ('32', '2', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_profile` VALUES ('3', '6', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_profile` VALUES ('45', '2', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_profile` VALUES ('42', '8', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_profile` VALUES ('44', '3', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_profile` VALUES ('5', '4', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_profile` VALUES ('17', '4', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_profile` VALUES ('40', '6', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_profile` VALUES ('48', '1', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_profile` VALUES ('8', '7', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_profile` VALUES ('14', '4', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_profile` VALUES ('48', '8', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_profile` VALUES ('18', '5', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_profile` VALUES ('14', '8', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_profile` VALUES ('31', '5', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_profile` VALUES ('43', '3', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_profile` VALUES ('33', '9', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_profile` VALUES ('44', '6', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_profile` VALUES ('4', '7', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_profile` VALUES ('21', '9', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_profile` VALUES ('3', '5', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_profile` VALUES ('20', '4', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_profile` VALUES ('42', '2', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_profile` VALUES ('1', '8', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_profile` VALUES ('13', '2', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_profile` VALUES ('28', '5', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_profile` VALUES ('27', '5', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_profile` VALUES ('21', '9', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_profile` VALUES ('1', '7', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_profile` VALUES ('36', '6', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_profile` VALUES ('11', '6', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_profile` VALUES ('45', '8', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_profile` VALUES ('25', '8', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_profile` VALUES ('23', '6', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_profile` VALUES ('26', '8', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_profile` VALUES ('26', '6', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_profile` VALUES ('19', '9', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_profile` VALUES ('48', '3', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_profile` VALUES ('6', '9', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_profile` VALUES ('3', '4', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_profile` VALUES ('8', '6', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_profile` VALUES ('45', '7', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_profile` VALUES ('7', '3', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_profile` VALUES ('29', '2', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_profile` VALUES ('1', '3', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_profile` VALUES ('39', '3', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_profile` VALUES ('44', '7', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_profile` VALUES ('6', '4', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_profile` VALUES ('24', '6', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_profile` VALUES ('47', '9', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_profile` VALUES ('23', '5', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_profile` VALUES ('28', '8', '2015-09-08 22:29:36', '2015-09-08 22:29:36');

-- ----------------------------
-- Table structure for production_types
-- ----------------------------
DROP TABLE IF EXISTS `production_types`;
CREATE TABLE `production_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of production_types
-- ----------------------------
INSERT INTO `production_types` VALUES ('1', 'Film studentesc');
INSERT INTO `production_types` VALUES ('2', 'Film (Lungmetraj)');
INSERT INTO `production_types` VALUES ('3', 'Film (Scurtmetraj)');
INSERT INTO `production_types` VALUES ('4', 'Videoclip');
INSERT INTO `production_types` VALUES ('5', 'Teatru');
INSERT INTO `production_types` VALUES ('6', 'TV');
INSERT INTO `production_types` VALUES ('7', 'Reclama');

-- ----------------------------
-- Table structure for production_user
-- ----------------------------
DROP TABLE IF EXISTS `production_user`;
CREATE TABLE `production_user` (
  `production_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `production_user_production_id_index` (`production_id`),
  KEY `production_user_user_id_index` (`user_id`),
  CONSTRAINT `production_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `production_user_production_id_foreign` FOREIGN KEY (`production_id`) REFERENCES `productions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of production_user
-- ----------------------------
INSERT INTO `production_user` VALUES ('19', '27', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('4', '25', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('25', '7', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('42', '33', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('37', '38', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('17', '45', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('5', '47', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('3', '6', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('12', '42', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('17', '6', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('26', '23', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('25', '1', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('2', '22', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('49', '25', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('45', '28', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('17', '14', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('4', '21', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('37', '30', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('27', '31', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('12', '14', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('18', '29', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('8', '23', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('25', '10', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('28', '37', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('2', '45', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('42', '28', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('18', '18', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('28', '20', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('39', '28', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('44', '34', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('6', '12', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('47', '10', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('31', '36', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('38', '8', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('16', '1', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('22', '34', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('29', '30', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('7', '4', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('39', '35', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('40', '41', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('29', '33', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('18', '47', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('49', '46', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('17', '39', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('24', '11', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('23', '29', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('22', '21', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('38', '4', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('6', '27', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('12', '22', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('26', '33', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('5', '6', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('12', '12', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('10', '2', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('45', '50', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('41', '25', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('32', '10', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('22', '32', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('6', '39', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('20', '30', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('49', '43', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('8', '22', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('13', '46', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('25', '19', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('22', '36', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('39', '49', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('19', '45', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('4', '31', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('6', '13', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('31', '2', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('13', '24', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('26', '45', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('32', '49', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('26', '39', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('36', '46', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('18', '36', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('38', '26', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('8', '1', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('21', '32', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('19', '43', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('18', '8', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('41', '37', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('3', '45', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('17', '8', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('8', '49', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('9', '20', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('21', '36', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('14', '4', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('33', '40', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('42', '20', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('35', '10', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('6', '24', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('34', '14', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('24', '6', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('44', '43', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('47', '13', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('1', '39', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('48', '3', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('33', '15', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('10', '41', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('13', '19', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('10', '35', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('4', '24', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('38', '37', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('14', '31', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('7', '49', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('39', '13', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('22', '25', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('25', '46', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('29', '21', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('37', '28', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('32', '38', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('16', '32', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('39', '50', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('46', '50', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('39', '10', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('18', '50', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('43', '22', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('24', '32', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('8', '38', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('12', '15', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('35', '1', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('27', '8', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('25', '3', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('3', '5', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('23', '40', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('31', '6', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('27', '48', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('36', '17', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('46', '33', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('16', '37', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('41', '34', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('36', '36', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('6', '10', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('17', '14', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('46', '28', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('28', '33', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('29', '6', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('39', '4', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('8', '42', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('8', '31', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('32', '40', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('36', '9', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('36', '24', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('26', '34', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('6', '42', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('20', '48', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('26', '6', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('32', '31', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('16', '49', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('44', '13', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('27', '23', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('44', '6', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('28', '35', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('9', '36', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('26', '17', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('17', '8', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('6', '3', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('17', '43', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('26', '43', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('26', '32', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('34', '46', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('29', '10', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('2', '12', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('41', '17', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('11', '36', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('29', '38', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('8', '24', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('42', '36', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('8', '2', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('21', '34', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('18', '38', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('41', '24', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('40', '8', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('17', '17', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('1', '43', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('47', '35', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('38', '28', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('44', '40', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('38', '36', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('7', '50', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('21', '36', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('36', '28', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('9', '29', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('13', '17', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('30', '34', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('1', '48', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('21', '42', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('21', '12', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('49', '38', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('27', '50', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('30', '26', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('34', '18', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('3', '30', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('8', '42', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('15', '14', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('40', '35', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('48', '27', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('13', '8', '2015-09-08 22:29:36', '2015-09-08 22:29:36');
INSERT INTO `production_user` VALUES ('5', '26', '2015-09-08 22:29:36', '2015-09-08 22:29:36');

-- ----------------------------
-- Table structure for productions
-- ----------------------------
DROP TABLE IF EXISTS `productions`;
CREATE TABLE `productions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `titlu` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of productions
-- ----------------------------
INSERT INTO `productions` VALUES ('1', '1st AD', '1st AD', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `productions` VALUES ('2', '3D & Animation', '3D & Animation', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `productions` VALUES ('3', 'Animation', 'Animation', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `productions` VALUES ('4', 'Ballet', 'Ballet', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `productions` VALUES ('5', 'Body parts modelling', 'Body parts modelling', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `productions` VALUES ('6', 'Campaign', 'Campaign', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `productions` VALUES ('7', 'Clip music video', 'Clip music video', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `productions` VALUES ('8', 'Commercial', 'Commercial', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `productions` VALUES ('9', 'Commercial print/on-camera', 'Commercial print/on-camera', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `productions` VALUES ('10', 'Commercials', 'Commercials', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `productions` VALUES ('11', 'Contemporary', 'Contemporary', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `productions` VALUES ('12', 'Costumes & Art Department', 'Costumes & Art Department', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `productions` VALUES ('13', 'DOP/Camera', 'DOP/Camera', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `productions` VALUES ('14', 'Documentary', 'Documentary', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `productions` VALUES ('15', 'Editing & Color Grading', 'Editing & Color Grading', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `productions` VALUES ('16', 'Events', 'Events', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `productions` VALUES ('17', 'Fashion', 'Fashion', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `productions` VALUES ('18', 'Fashion/Editorial', 'Fashion/Editorial', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `productions` VALUES ('19', 'Fashion/Editorials', 'Fashion/Editorials', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `productions` VALUES ('20', 'Festivals', 'Festivals', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `productions` VALUES ('21', 'Film (Feature)', 'Film (Feature)', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `productions` VALUES ('22', 'Film (Short)', 'Film (Short)', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `productions` VALUES ('23', 'Film (Student)', 'Film (Student)', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `productions` VALUES ('24', 'Fitness Models', 'Fitness Models', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `productions` VALUES ('25', 'Interior Design/Art', 'Interior Design/Art', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `productions` VALUES ('26', 'Kids', 'Kids', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `productions` VALUES ('27', 'Lighting Department', 'Lighting Department', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `productions` VALUES ('28', 'Make-up Artists & Hair Stylists', 'Make-up Artists & Hair Stylists', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `productions` VALUES ('29', 'Michael Jackson', 'Michael Jackson', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `productions` VALUES ('30', 'Music Video', 'Music Video', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `productions` VALUES ('31', 'Musical theater', 'Musical theater', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `productions` VALUES ('32', 'Oriental dance', 'Oriental dance', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `productions` VALUES ('33', 'Other', 'Other', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `productions` VALUES ('34', 'PA', 'PA', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `productions` VALUES ('35', 'Popular dance', 'Popular dance', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `productions` VALUES ('36', 'Portet', 'Portet', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `productions` VALUES ('37', 'Radio', 'Radio', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `productions` VALUES ('38', 'Reporters', 'Reporters', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `productions` VALUES ('39', 'Runway', 'Runway', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `productions` VALUES ('40', 'Screenwriters/Script', 'Screenwriters/Script', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `productions` VALUES ('41', 'Sound', 'Sound', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `productions` VALUES ('42', 'Sport dance', 'Sport dance', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `productions` VALUES ('43', 'Street dance', 'Street dance', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `productions` VALUES ('44', 'TV', 'TV', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `productions` VALUES ('45', 'TV Commercial', 'TV Commercial', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `productions` VALUES ('46', 'TV Directors', 'TV Directors', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `productions` VALUES ('47', 'Theatre', 'Theatre', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `productions` VALUES ('48', 'Weddings/Christenings', 'Weddings/Christenings', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `productions` VALUES ('49', 'Zorba', 'Zorba', '2015-09-08 22:29:29', '2015-09-08 22:29:29');

-- ----------------------------
-- Table structure for profiles
-- ----------------------------
DROP TABLE IF EXISTS `profiles`;
CREATE TABLE `profiles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `model_name` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `descriere` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `age` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of profiles
-- ----------------------------
INSERT INTO `profiles` VALUES ('1', 'Actor', 'Actor', 'Actor', '(16+)', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `profiles` VALUES ('2', 'Amator', 'Actor amator', 'Amator Actor', '(16+)', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `profiles` VALUES ('3', 'Dancer', 'Dansator', 'Dancer', '(16+)', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `profiles` VALUES ('4', 'Employer', 'Angajator', 'Employer', '(18+)', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `profiles` VALUES ('5', 'Filmcrew', 'Echipa de filmare', 'Filmcrew', '(18+)', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `profiles` VALUES ('6', 'Kid', 'Copii', 'Kids', '', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `profiles` VALUES ('7', 'Photographer', 'Fotograf', 'Photographer', '(14+)', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `profiles` VALUES ('8', 'Voice', 'Voce', 'Voice Over', '', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `profiles` VALUES ('9', 'Other', 'Alte talente', 'Other talents', '(16+)', '2015-09-08 22:29:29', '2015-09-08 22:29:29');

-- ----------------------------
-- Table structure for projects
-- ----------------------------
DROP TABLE IF EXISTS `projects`;
CREATE TABLE `projects` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `duration` smallint(5) unsigned DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `produced_under` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `production_type` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `projects_user_id_index` (`user_id`),
  CONSTRAINT `projects_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of projects
-- ----------------------------
INSERT INTO `projects` VALUES ('1', '11', 'Quo eveniet veniam voluptatum nobis.', '1573', 'Maiores incidunt perspiciatis at temporibus. Ab consequuntur qui est. Sit provident voluptatibus reiciendis totam magni sit rem. Ea sed quae aliquid omnis. In vel aut facere ut. Est omnis perspiciatis earum exercitationem. Amet est assumenda sint qui voluptatibus cumque ut et.', 'Cum hic alias fugit natus aut optio debitis. Ut atque fugit quo ipsa sunt quia e', 'Compania de Teatru OKaua', '2015-08-29 13:53:20', '2015-08-24 05:05:40');
INSERT INTO `projects` VALUES ('2', '20', 'Quia dolor aut voluptas.', '3269', 'Cupiditate voluptatem minima numquam perferendis veniam provident. Qui sapiente consectetur vel omnis. Autem ducimus nam ea sunt. Ipsa ut quia et tempore quisquam. Asperiores ut delectus quas ipsa. Et esse voluptatem velit.', 'Et sit aut nesciunt. Et cumque soluta veritatis aut doloremque natus est. Quidem', 'MediaPRO Pictures', '2015-08-30 13:38:22', '2015-08-20 02:55:08');
INSERT INTO `projects` VALUES ('3', '27', 'Aliquam tempore quia.', '229', 'Iure voluptatem ut qui et quis. Commodi consequatur nihil pariatur itaque. Officia ex reprehenderit laboriosam quis. Laboriosam dolorem culpa et et minus et numquam.', 'Consequatur dolorem quia pariatur porro sapiente perspiciatis enim. Architecto s', 'Compania de Teatru OKaua', '2015-09-05 06:35:36', '2015-08-27 10:46:46');
INSERT INTO `projects` VALUES ('4', '22', 'Voluptatem dolorum minus architecto sint ut.', '1935', 'Quae est est veniam sit maxime debitis. Aspernatur iusto est officia voluptas. Officiis assumenda voluptas quae quo. Consequatur sunt sed quasi asperiores.', 'Rem vero qui ex dolores eos non. Quia voluptatem non saepe. Quia doloribus quia ', 'UNATC', '2015-08-11 16:27:43', '2015-08-25 03:17:08');
INSERT INTO `projects` VALUES ('5', '22', 'Animi error ut et et vitae.', '656', 'Possimus eius rerum dolor quo. Quis incidunt similique enim laborum eum. Ipsam mollitia sed culpa. Consequatur sint itaque quas rerum iusto temporibus.', 'Sed aliquid est omnis. Repudiandae corrupti incidunt facere consequuntur in veli', 'Compania de Teatru OKaua', '2015-08-15 11:28:59', '2015-08-17 18:21:23');
INSERT INTO `projects` VALUES ('6', '8', 'Aspernatur non temporibus.', '3119', 'Aut aut facilis unde dolorem. Omnis eum corrupti nihil aut minima consequuntur. Libero eum et facere velit explicabo reprehenderit eveniet. Voluptas quasi quis aut et reiciendis suscipit ut eum. Ut neque omnis sint quos vitae voluptas. In molestiae sint sit eum debitis sed iusto labore. Tempore atque neque aperiam corrupti et facilis.', 'Rerum nobis deleniti molestiae. Totam voluptatem repellat id ut porro aut enim. ', 'UNATC', '2015-08-25 11:24:30', '2015-08-22 11:23:05');
INSERT INTO `projects` VALUES ('7', '41', 'Adipisci optio ut esse qui.', '2774', 'Nam tempora qui ipsum dolorem autem ut. Consectetur veritatis perspiciatis voluptatem officiis. Necessitatibus nostrum numquam consequatur sint. Quis qui hic in excepturi quo. Harum non quidem qui nostrum. Sequi aut dolor nesciunt laboriosam sit. Quod laudantium libero laboriosam expedita et deleniti dolorem.', 'Qui quia eum aut ut est. Impedit aut vero et alias. A consectetur expedita solut', 'Teatrul Metropolis', '2015-09-02 16:41:07', '2015-08-18 12:51:35');
INSERT INTO `projects` VALUES ('8', '29', 'Sunt et consequatur eligendi.', '790', 'Doloribus esse eveniet eum a. Labore ut quae aut eos. Ullam architecto qui eveniet eaque et. Occaecati enim velit voluptas facere nulla nihil.', 'Aliquid labore fugiat perspiciatis impedit qui ut sapiente iste. Quaerat non aut', 'Independent', '2015-08-25 10:04:00', '2015-08-31 01:45:11');
INSERT INTO `projects` VALUES ('9', '38', 'Impedit voluptate sapiente eligendi.', '2354', 'Atque aut architecto similique facere est officiis. Animi corporis incidunt voluptatibus quae est nostrum. Ut quaerat voluptate labore totam et est nulla. Quis et iure voluptatem perferendis eaque modi. Perferendis quibusdam et aspernatur. Qui possimus dolorem et cum omnis cumque cum.', 'Aut laboriosam vel quo aliquam laboriosam. Rerum quidem omnis quas omnis volupta', 'Independent', '2015-08-31 17:36:33', '2015-08-20 03:35:10');
INSERT INTO `projects` VALUES ('10', '48', 'Placeat molestias nemo neque ea.', '416', 'Sunt nobis quisquam quo error. Dolorum voluptates dolorem non. Omnis cupiditate amet libero aspernatur accusantium neque provident. Exercitationem dolorem iste mollitia quas nemo illum autem. Voluptas dolorum voluptates sit facilis quae. Amet vel enim sit dolor facilis. Velit cupiditate sit deleniti qui minus.', 'Neque ullam aspernatur voluptates nulla. Rerum id quis quia ea. Fugiat quas dolo', 'Independent', '2015-08-19 18:22:02', '2015-09-02 21:41:35');
INSERT INTO `projects` VALUES ('11', '45', 'Doloremque voluptas beatae.', '561', 'Totam incidunt id error ut. Est rerum id dicta enim est dolorem sit eos. Non minus occaecati qui ut et nihil. Quis quas qui commodi autem culpa recusandae. Consequatur quia quidem rerum dolor nam quae et.', 'Architecto eos sit quia illo dignissimos. Accusantium explicabo repellendus prov', 'Compania de Teatru OKaua', '2015-08-24 16:05:09', '2015-09-08 06:52:23');
INSERT INTO `projects` VALUES ('12', '18', 'Ut dolores similique aut.', '781', 'Nostrum ut ullam iste dolorum voluptate sint. Et magni dolorem pariatur sit est velit. Autem adipisci sit doloribus. Aspernatur cupiditate nihil necessitatibus et ipsum. Aut rem veniam fugiat iste id porro sint sit. Aliquid nemo aut sint voluptas adipisci at. Rerum non autem distinctio quae ratione. Cumque dolorem tenetur eum maxime deserunt at iure.', 'Qui sequi officiis magni. Qui accusantium veniam dolore consequuntur animi itaqu', 'Teatrul Bulandra', '2015-08-18 18:40:00', '2015-08-18 05:25:43');
INSERT INTO `projects` VALUES ('13', '25', 'Atque error cupiditate vel omnis.', '3400', 'Aliquam eveniet quia cum odit. Nulla alias corrupti modi sed dolore. Error qui id illum veritatis quas reiciendis. Sed eum nesciunt eligendi magnam quas non. Sunt architecto incidunt dignissimos iure odit ut. Veritatis sint illo libero a ullam fugit ut.', 'Incidunt sint eius perspiciatis maiores numquam laudantium soluta. Nobis ipsam i', 'Compania de Teatru OKaua', '2015-08-21 00:22:46', '2015-08-25 05:13:32');
INSERT INTO `projects` VALUES ('14', '5', 'Aperiam atque unde harum molestiae.', '1589', 'Enim iste alias sit nihil itaque consequuntur laborum. At at dolores vel enim. Dolorem necessitatibus id quisquam doloremque at soluta et. Odio assumenda velit omnis iusto nesciunt itaque. Quae enim ab nihil quas ad. Dolores fugiat veniam et sint saepe ut aut voluptatem. Eum voluptas ducimus rerum iure.', 'Dolor enim cumque facere odit dolores. Laborum laudantium rerum dolores architec', 'Independent', '2015-09-02 03:50:10', '2015-09-08 11:05:26');
INSERT INTO `projects` VALUES ('15', '28', 'Animi sed impedit officia consequatur.', '3151', 'Aut aliquam esse quaerat modi voluptas et. Laudantium natus saepe reprehenderit inventore eum est. Ratione eligendi error vero nisi sint est vel. Quae dolorem consequatur dolor voluptatibus libero. Quasi et quasi ea molestiae. Commodi sequi quod modi corporis. Ea corrupti et occaecati dignissimos quam.', 'Temporibus sunt consectetur magnam et deleniti. Adipisci quas rem libero laborum', 'Teatrul Metropolis', '2015-08-10 16:05:02', '2015-08-22 07:54:12');
INSERT INTO `projects` VALUES ('16', '46', 'Non perspiciatis culpa cupiditate voluptatibus veritatis.', '3630', 'Officiis ducimus enim error. Illo animi nobis deserunt sed. Tempora aut et rerum et. Est aliquam rerum accusantium ipsam.', 'Ratione rem ut alias voluptatibus ipsa dicta. Sit neque voluptatem est perferend', 'Teatrul Bulandra', '2015-08-20 16:12:38', '2015-08-16 11:12:46');
INSERT INTO `projects` VALUES ('17', '50', 'Et porro et est tempora error.', '1268', 'Necessitatibus qui qui voluptate aut in sapiente. Hic quia repudiandae autem et omnis architecto et. Est sapiente harum neque quia quis. Et dignissimos reprehenderit modi rerum voluptatem quod minima. Omnis labore omnis asperiores veniam. Porro consequatur sed doloremque rerum in et. Doloremque voluptatem non dolorem ut saepe eum doloribus.', 'Exercitationem aut consequatur cupiditate ut. Id velit eveniet repudiandae. Dolo', 'Teatrul Metropolis', '2015-08-16 15:25:41', '2015-08-13 02:05:25');
INSERT INTO `projects` VALUES ('18', '37', 'Asperiores et sed voluptates quia.', '916', 'Possimus laborum aut sed omnis impedit excepturi. Omnis est ad dolor aspernatur a vel. Pariatur aliquid omnis autem laboriosam. Deserunt eligendi magnam est animi. Voluptas quia reprehenderit eum.', 'Illo dolore rerum pariatur sed. Aut non qui consequatur consequatur ut iure aspe', 'Teatrul Bulandra', '2015-09-06 10:29:35', '2015-08-30 06:09:00');
INSERT INTO `projects` VALUES ('19', '8', 'Et illum temporibus impedit sit.', '430', 'Quos voluptas expedita dolores ea aut possimus. Quis itaque tenetur ducimus nobis et eum. Commodi et quia ad reiciendis dolor. Laborum ea sunt iure. Voluptates aut est sit nulla quo.', 'Consequatur illo provident exercitationem voluptatibus. Voluptatum voluptatem ma', 'UNATC', '2015-09-05 05:15:49', '2015-08-27 23:23:35');
INSERT INTO `projects` VALUES ('20', '39', 'Odio doloremque dolores et.', '2867', 'Corporis quas suscipit ex debitis inventore rerum. Velit assumenda fugiat blanditiis suscipit inventore dolores. Nobis et aut tempora et. Commodi ab tempora maiores odit nihil qui vel. Modi non voluptatem asperiores animi. Earum dolores et incidunt. Aut ullam tenetur hic voluptates consequatur molestiae.', 'Non ab soluta soluta dolorem neque officia doloremque. Ut impedit corporis exped', 'Teatrul Bulandra', '2015-09-06 10:27:01', '2015-09-08 08:48:46');
INSERT INTO `projects` VALUES ('21', '33', 'Ea quis facilis est et.', '3155', 'Distinctio eum quis dolore ad sint aut inventore. Omnis est expedita quod aut. Ut dolor tempore et voluptas et. Exercitationem autem aliquid aspernatur assumenda non neque delectus ut. Molestiae quibusdam expedita ab. Quibusdam voluptas facere assumenda modi odit eum. Sit laborum eveniet hic corrupti qui delectus explicabo.', 'Neque soluta vero mollitia et officia nihil. Aut animi doloremque delectus id. U', 'MediaPRO Pictures', '2015-08-23 15:54:01', '2015-09-03 20:04:41');
INSERT INTO `projects` VALUES ('22', '10', 'Et id et.', '1423', 'Nemo dolor alias dolorum. Eveniet earum illo quis qui fugit repellendus iste. Aut et ab dolor commodi nobis. Harum nobis voluptas pariatur perspiciatis nihil odit. Unde eum atque commodi et eos sit deleniti dolorem.', 'Nihil magni aut officiis ut deleniti eveniet. Laboriosam fugit earum autem ut et', 'MediaPRO Pictures', '2015-08-19 07:32:36', '2015-09-07 00:32:50');
INSERT INTO `projects` VALUES ('23', '21', 'Neque recusandae atque repellat.', '1411', 'Consequatur qui et aliquam ut ipsa. Dolores sit mollitia dolorem at autem animi qui. Itaque doloribus exercitationem sit nihil totam sunt. Maxime non et esse delectus deleniti. Officiis aut occaecati repellat.', 'Officia quis similique eos enim dolorem ut vero. Sed maxime commodi qui ut quibu', 'Compania de Teatru OKaua', '2015-08-23 15:21:37', '2015-08-11 15:45:16');
INSERT INTO `projects` VALUES ('24', '11', 'Consequatur sit tempore sit excepturi.', '1573', 'Ratione nemo debitis cumque. Tenetur est autem et. Cum fuga cum nesciunt ratione sit soluta aperiam fugit. Quia consequatur eveniet eligendi sed. Non at voluptas incidunt animi.', 'Reprehenderit aut fugiat laboriosam non. Quo assumenda nostrum aut autem blandit', 'UNATC', '2015-08-28 00:40:05', '2015-08-20 06:46:39');
INSERT INTO `projects` VALUES ('25', '48', 'Laboriosam earum et sed.', '2394', 'Ut consequatur et laboriosam voluptates quibusdam provident. Mollitia ex quis et ab. Aut placeat magni quia optio. Enim labore officia cum ratione qui eveniet porro aut.', 'Fugit vel incidunt accusamus sed quaerat. Cupiditate nulla quidem amet et aspern', 'Kanal D', '2015-08-11 05:20:37', '2015-09-05 19:48:29');
INSERT INTO `projects` VALUES ('26', '45', 'Vitae rerum voluptatem soluta reiciendis.', '3599', 'Qui animi sit ipsa dolor debitis ea. Tenetur quia voluptatem dolor ipsa et. Vel voluptates labore nemo fugit velit repellendus pariatur. Est non voluptas sunt quo culpa enim est. Alias quidem quod ratione iusto odio. Suscipit occaecati dolore debitis exercitationem perspiciatis rerum voluptas. Doloremque magni quos quis doloremque alias tempora consequatur. Odio ut iste eaque aut voluptates sequi aut eum.', 'Quia voluptas facere deleniti repudiandae. Voluptas rerum illum et facilis moles', 'Teatrul Metropolis', '2015-09-03 02:18:24', '2015-08-19 02:13:30');
INSERT INTO `projects` VALUES ('27', '33', 'Odit sit magni in qui modi.', '177', 'Quo harum itaque assumenda omnis. Eos beatae est est perferendis. Id id est aut tenetur et placeat. Et veritatis assumenda qui aut autem. Eum officia et qui nemo et ut blanditiis.', 'Perferendis eum provident aut aut. Sit dolor magni commodi saepe maiores saepe. ', 'Independent', '2015-09-02 18:44:15', '2015-08-25 19:37:55');
INSERT INTO `projects` VALUES ('28', '24', 'Voluptate iusto corporis quae quidem.', '1567', 'Repellendus et tempore provident quas ut accusamus. Eos consequatur eum dolores esse numquam. Distinctio natus aut voluptatem voluptas repudiandae nisi vel eius. Qui non vel maiores est eum tempore.', 'Est dolor omnis enim dolorem voluptates et et. Adipisci illo est nobis quia labo', 'UNATC', '2015-08-24 07:18:19', '2015-09-06 01:03:53');
INSERT INTO `projects` VALUES ('29', '11', 'Fugit atque ipsa beatae aperiam rem.', '3382', 'Deserunt reiciendis quia repellendus enim dolores. Quae odio totam et cupiditate voluptates facilis perferendis. Consequatur nemo neque aperiam voluptatem expedita assumenda ducimus. Quas reiciendis velit enim odio dolores voluptas.', 'Ut officia et maiores fugiat consequatur. Perferendis aut reprehenderit dicta di', 'Teatrul Bulandra', '2015-08-25 05:01:34', '2015-08-17 07:48:54');
INSERT INTO `projects` VALUES ('30', '32', 'Voluptatibus optio in voluptatem.', '1566', 'Nihil dolorem placeat aut odio. Neque pariatur aliquid quod vel labore qui commodi. Fuga dicta non odio accusantium eaque rerum. Cumque unde laborum asperiores quam rerum consequatur.', 'Velit sunt fugit odio sed ullam iusto. Magni non vel aut tempora mollitia velit.', 'Teatrul Metropolis', '2015-08-25 13:12:49', '2015-09-08 06:23:48');
INSERT INTO `projects` VALUES ('31', '15', 'Dolorem ut autem.', '2149', 'Nam sunt minima ab adipisci rerum molestias fuga. Dolores eius assumenda consequuntur dolor consequatur. Dolorem omnis in et et est. Illo eligendi distinctio tempore repellendus saepe molestiae. Ea enim nihil eos enim est et nostrum. Aut dolorem architecto et quaerat eum qui.', 'Vero quis cumque facilis reiciendis maxime facere consectetur iusto. Assumenda i', 'Kanal D', '2015-09-04 08:00:00', '2015-08-23 13:29:19');
INSERT INTO `projects` VALUES ('32', '33', 'Blanditiis ab rem at.', '2161', 'Qui provident rerum nam libero voluptatum quod provident. Et velit sunt est perferendis. Ad facilis excepturi omnis aliquid facilis in. Doloribus consectetur modi nobis aut adipisci sequi exercitationem. Saepe aut et sit deleniti. Ab cupiditate quibusdam molestiae quo quibusdam quis fuga. Dolore et architecto sunt quidem eveniet amet harum.', 'Quia dolorum totam vero autem. Harum dicta temporibus debitis est et. Iure ut ut', 'Kanal D', '2015-08-25 07:13:19', '2015-09-04 06:02:41');
INSERT INTO `projects` VALUES ('33', '47', 'Sit ducimus illo excepturi aut.', '2399', 'Praesentium maxime aliquid qui aut. Doloribus ipsam rerum fugit quis. Sit dolorem quasi consequuntur nisi. Omnis et sit tempore at necessitatibus consectetur praesentium. Non non minus voluptatem dicta voluptates unde nam aliquam. Asperiores ut modi est maxime reprehenderit.', 'Non non veniam magnam aut at ut. Nulla sequi eligendi in et distinctio. Doloribu', 'UNATC', '2015-08-27 10:46:34', '2015-08-08 23:46:46');
INSERT INTO `projects` VALUES ('34', '24', 'Ut aut temporibus non.', '606', 'Vitae ipsa cumque reprehenderit ea quibusdam numquam. Quidem necessitatibus a omnis eos delectus adipisci occaecati alias. Sit sint illo et debitis ea. Earum sed quia illum et ullam. Et facere cupiditate et.', 'Incidunt nostrum nostrum blanditiis excepturi. In quisquam reiciendis autem anim', 'Independent', '2015-08-11 15:12:26', '2015-08-26 17:31:04');
INSERT INTO `projects` VALUES ('35', '30', 'Dignissimos et dolorum alias ipsum.', '971', 'Eaque fuga sit soluta non nobis cumque nemo deserunt. Aut a placeat accusamus id. Iure quia libero veritatis dolore sint eligendi consectetur. Ut magni fugiat et impedit nemo architecto quisquam. Aperiam quo quibusdam cupiditate accusamus porro nam atque.', 'Tenetur iusto voluptatibus rerum dolorem qui cum. Quidem aut labore voluptatem e', 'UNATC', '2015-08-10 04:49:57', '2015-09-01 18:48:09');
INSERT INTO `projects` VALUES ('36', '12', 'Quasi sint aspernatur.', '3465', 'Omnis nihil voluptatum cum voluptates delectus. Aut quas id esse necessitatibus. Voluptatem quia ipsum harum eum eaque ut distinctio. Laborum molestiae vitae quidem. Tenetur laudantium deleniti corporis ad.', 'Est maxime nesciunt esse ut repellendus. Nulla aspernatur ipsam et atque nisi es', 'Compania de Teatru OKaua', '2015-08-29 13:43:49', '2015-09-06 13:47:22');
INSERT INTO `projects` VALUES ('37', '20', 'Maxime modi culpa eos quis.', '3204', 'Nostrum et accusantium debitis earum dolorem. Non est aliquid fugit enim id modi. Veritatis eos aut laudantium voluptas. Provident doloribus sunt assumenda rerum incidunt tempora qui. Quia sit quisquam facilis atque omnis odit dolor. Qui error quas in animi ut.', 'Voluptas earum vitae et omnis aut reprehenderit. Repellendus atque similique acc', 'Compania de Teatru OKaua', '2015-09-06 16:54:41', '2015-08-15 05:23:07');
INSERT INTO `projects` VALUES ('38', '6', 'Animi dicta nihil neque eveniet sunt.', '27', 'Maxime culpa molestiae et est. Voluptatem beatae omnis libero nihil aspernatur iure. Quia repellendus qui sed voluptate distinctio. Est odit corporis et eos odio est. Dolores amet ut quia. Et sunt omnis debitis dolor alias commodi.', 'Est laborum inventore saepe et consectetur veritatis delectus. Ratione illum aut', 'Compania de Teatru OKaua', '2015-09-02 18:38:06', '2015-08-24 15:27:18');
INSERT INTO `projects` VALUES ('39', '32', 'Dicta voluptas illo nostrum.', '898', 'Harum qui voluptas ea consequatur veniam et maxime. Fugiat deleniti sunt et molestiae quis. Aut et facere non explicabo. Hic amet accusamus autem quo dolores. Ipsa voluptatem nostrum ratione commodi autem. Id qui et doloremque doloremque ut culpa.', 'Veritatis deserunt temporibus dolorem nihil voluptas sit consequatur. Amet itaqu', 'Compania de Teatru OKaua', '2015-08-23 07:31:09', '2015-09-03 20:57:10');
INSERT INTO `projects` VALUES ('40', '46', 'Officiis consectetur ipsam eos qui veniam.', '1525', 'Et distinctio harum nesciunt sunt ad qui illum. Earum facere fugit possimus occaecati. Voluptatum quisquam cum dolores vero consequatur distinctio deleniti eos. Consequatur id possimus ut est odit dolorem.', 'Qui voluptas iusto praesentium. Quod facere eius repellat animi. Dolorum consect', 'Kanal D', '2015-08-15 17:11:18', '2015-09-06 07:21:45');
INSERT INTO `projects` VALUES ('41', '32', 'Nihil cum suscipit rerum omnis.', '2337', 'Rem tempore id quaerat laborum eligendi veniam eum. Sed a quidem enim recusandae tempore neque laboriosam et. Quisquam omnis fugit id et. Non nesciunt libero repellendus alias voluptatem. Quisquam error at maiores ut. Temporibus amet quae commodi. Quidem quidem nihil amet tempore repellendus id reprehenderit.', 'Eum ipsa cumque qui voluptatem est. Maiores fugit quisquam suscipit. Illo nihil ', 'Teatrul Bulandra', '2015-08-13 00:01:11', '2015-08-19 22:19:43');
INSERT INTO `projects` VALUES ('42', '21', 'Voluptas eveniet et velit veniam aut.', '2376', 'Facilis nihil doloremque dolore. Modi sunt architecto et fugiat. Fugit consequuntur est molestias vel vel atque quia. Et quia sit voluptatem consequatur. A non vel dolores impedit amet expedita. Saepe repellendus consequuntur velit.', 'Nihil eum tenetur molestiae consequatur fuga sit non. Ea vitae aut et quos natus', 'Teatrul Bulandra', '2015-08-29 03:17:52', '2015-08-22 13:31:35');
INSERT INTO `projects` VALUES ('43', '27', 'Quasi voluptas quos nihil.', '714', 'Eos aut optio doloremque dolores saepe possimus voluptatem ut. Dolorum totam quidem a quibusdam dolore quisquam. Ullam dolorem molestiae ex sit non sed quo ad. Autem non dolor aut dignissimos alias. Magnam assumenda et sunt tempore quae facilis. Est omnis suscipit odio perspiciatis ab dicta qui laudantium. Sequi labore dolor quas omnis.', 'Aperiam quaerat voluptas eveniet repellendus. Voluptatem similique blanditiis ma', 'UNATC', '2015-08-16 12:52:33', '2015-09-07 17:22:58');
INSERT INTO `projects` VALUES ('44', '26', 'Et non cupiditate magni adipisci.', '783', 'Repudiandae quis est amet ut aut ad quia at. Totam voluptatibus consequatur quo eos. Nihil quis saepe nihil ut voluptas sit quis temporibus. Voluptatum quasi corrupti consequatur. Amet voluptates omnis qui consequatur asperiores et molestiae. Culpa sunt totam iure dignissimos.', 'Nesciunt exercitationem molestiae veritatis quae dolorum doloremque blanditiis. ', 'Teatrul Metropolis', '2015-08-09 01:52:30', '2015-08-21 02:07:18');
INSERT INTO `projects` VALUES ('45', '17', 'Debitis quos rerum tenetur.', '1747', 'Quod soluta facilis ea sint velit voluptatem. Modi non nesciunt in voluptate. Quo qui assumenda ea deleniti provident voluptatem qui. Corporis qui sit laborum aperiam.', 'Qui quis minus eum aut explicabo iure. Et nihil et ab quo. Eaque corporis repell', 'Independent', '2015-08-25 05:35:35', '2015-08-21 03:08:46');
INSERT INTO `projects` VALUES ('46', '15', 'Adipisci et velit hic.', '1281', 'At enim qui laborum. Harum laborum et pariatur officia. Magnam labore molestiae non sed sunt officiis debitis dicta. Inventore aut amet qui nemo rem provident.', 'Earum enim facere officiis asperiores. Optio qui non aut asperiores. Voluptates ', 'UNATC', '2015-08-29 14:17:10', '2015-08-31 04:34:09');
INSERT INTO `projects` VALUES ('47', '21', 'Odit incidunt veritatis et.', '1997', 'Illo sint nisi earum dolorem sed delectus soluta veniam. Sit architecto quas sit sunt tempora beatae quo architecto. Totam ipsam vitae ipsum quidem culpa dolor. Mollitia voluptatem quia inventore quisquam amet eaque. Dolorem maxime eum aut dolorem et voluptate omnis numquam. Explicabo incidunt quos quis pariatur. Et rerum eos quaerat omnis excepturi.', 'Dolor esse quis aut facilis eaque veritatis dolore. Corporis et id consequatur m', 'Independent', '2015-09-03 09:56:49', '2015-08-13 06:21:29');
INSERT INTO `projects` VALUES ('48', '43', 'Qui et iste occaecati amet et.', '2144', 'Ullam est aut sint quia voluptas. Qui quam ex consequatur deleniti. Placeat tempora consequatur quidem odio corporis modi. Fuga possimus voluptatibus quia dolor.', 'Et fuga autem officiis laboriosam odit non omnis. Accusantium minus autem praese', 'MediaPRO Pictures', '2015-08-31 13:03:15', '2015-08-17 16:57:34');
INSERT INTO `projects` VALUES ('49', '3', 'Est officiis ea sunt.', '456', 'Officia officia mollitia et tempore. Consequatur optio provident voluptatem voluptas cupiditate et. Maxime porro in maiores ut tempora. Optio et dolorum dolore est. Non vel nostrum suscipit ipsam aut optio ab. Praesentium quia commodi voluptatem est odit error impedit libero. Ut qui sit et placeat unde.', 'Voluptas ea exercitationem aperiam nisi dolore minus. Illo ab quo aut qui quia r', 'Teatrul Bulandra', '2015-09-08 04:32:57', '2015-08-22 14:08:11');
INSERT INTO `projects` VALUES ('50', '26', 'Asperiores saepe consectetur cum.', '619', 'Mollitia quaerat exercitationem rerum possimus alias eveniet. Ipsa officiis modi voluptates ipsum. Dolorum in beatae occaecati magni. Et quod deserunt qui impedit repellendus. Rem laborum modi quia voluptatem laboriosam recusandae. Et consectetur ut dolor voluptatibus dolores perspiciatis non. Rerum fuga sint reiciendis dolores molestiae.', 'Aliquam sunt ut accusamus deleniti veniam expedita saepe quo. Sunt cum modi dolo', 'Teatrul Metropolis', '2015-08-21 13:28:11', '2015-09-02 16:31:24');
INSERT INTO `projects` VALUES ('51', '8', 'Quia consequuntur dolorem ab quam et.', '3109', 'Odit esse sunt suscipit. Iusto eos omnis vero rem. Iusto animi possimus excepturi dolorem ipsa rerum sapiente. Similique enim nihil id ex earum nihil eos.', 'Optio aliquid dolor rerum nulla. Reprehenderit autem consectetur non unde autem.', 'Compania de Teatru OKaua', '2015-08-13 14:58:14', '2015-09-06 01:42:33');
INSERT INTO `projects` VALUES ('52', '43', 'Minima qui aut voluptatem.', '2944', 'Optio expedita quis ut qui odio explicabo quis. Vel natus est corporis totam quisquam eos. Sit soluta hic iure ut molestiae quibusdam assumenda. Debitis perspiciatis magnam veniam itaque dignissimos non. Consequatur cumque eum aut omnis ducimus tenetur iure. Natus numquam ut omnis et velit dignissimos.', 'Non ea provident omnis consectetur autem est. Corporis suscipit expedita magni t', 'Teatrul Bulandra', '2015-08-29 23:51:53', '2015-08-17 10:43:06');
INSERT INTO `projects` VALUES ('53', '47', 'Quia tempore nesciunt porro nihil harum.', '983', 'Dolorem vel deserunt perferendis illum quibusdam qui quisquam. Sint sed expedita nihil porro in sed. Delectus iusto nihil et reprehenderit. Autem nihil quo et itaque. Dicta commodi rerum quo quaerat accusantium consequatur fugiat.', 'Natus recusandae velit voluptatem. Itaque aut est dolorem ut dolor dolorum. In q', 'MediaPRO Pictures', '2015-08-28 13:48:07', '2015-09-01 10:37:48');
INSERT INTO `projects` VALUES ('54', '27', 'Reiciendis reprehenderit est.', '1128', 'Assumenda et iste molestias ullam eum. Et laudantium aperiam dolorum. Velit suscipit eveniet atque libero. Dolores quia sint quod qui sapiente ut qui deleniti. Ipsam aut aut voluptas aliquid eos.', 'Illo doloribus minima soluta consequatur. Quia ducimus et qui eos nam omnis. Lab', 'Teatrul Bulandra', '2015-09-02 00:05:05', '2015-08-16 20:16:42');
INSERT INTO `projects` VALUES ('55', '28', 'Vitae dicta voluptas sit.', '2395', 'Et adipisci doloremque rerum nihil et amet. Voluptatum quo autem molestiae doloremque quibusdam. Omnis id est et sit. Repellat neque sit et et beatae quibusdam facilis. Voluptas porro incidunt corporis. Est officia eligendi repellat quaerat.', 'Odit consequatur mollitia doloremque sunt sapiente. Et quae similique veritatis ', 'Teatrul Bulandra', '2015-09-08 04:17:43', '2015-09-03 13:13:23');
INSERT INTO `projects` VALUES ('56', '41', 'Error earum explicabo nostrum vel iure.', '1589', 'Accusamus sunt sequi voluptatem sapiente expedita provident. Consequatur qui mollitia voluptates unde. Sunt perferendis aut aut eaque. Eligendi eos enim consequatur qui. Qui facilis voluptatem error cupiditate aut omnis.', 'Repudiandae laudantium alias sit placeat libero ad. Velit enim error minus ullam', 'MediaPRO Pictures', '2015-09-01 19:23:12', '2015-08-19 00:06:51');
INSERT INTO `projects` VALUES ('57', '12', 'Saepe dolores eos.', '269', 'Quaerat quam dolorum et et expedita illum. Quo modi vitae ea distinctio et eos voluptatem. Omnis odio et totam maiores dolore dolor molestiae. Maxime et occaecati illo necessitatibus sit. Reprehenderit officiis eum asperiores. Aut commodi recusandae quo iusto accusamus nobis quibusdam.', 'Quia molestias perferendis voluptatem praesentium quidem eveniet sit. Architecto', 'Teatrul Metropolis', '2015-08-11 20:34:44', '2015-08-18 13:24:27');
INSERT INTO `projects` VALUES ('58', '20', 'Sit et officia et sint sapiente.', '3495', 'Distinctio ea doloribus voluptatum qui enim. Sint dolores molestias id molestiae id. Voluptatem veniam sequi in commodi porro. Expedita excepturi quia qui maiores quo. Dolore qui hic aperiam nostrum. Enim sit ab impedit qui amet soluta quia. Magnam quo qui error a quia.', 'Rerum voluptate et magnam reiciendis. Aut voluptate est consequatur ut. Quod dol', 'Teatrul Bulandra', '2015-08-17 15:35:25', '2015-08-31 06:29:14');
INSERT INTO `projects` VALUES ('59', '21', 'Qui qui omnis nihil.', '3306', 'Et vel illum ratione sed rerum. Aut sed dolor quae. Error quibusdam aut quaerat est corrupti est expedita. Ut qui dicta ullam. Doloribus nostrum soluta est at quisquam. Qui nesciunt et doloribus exercitationem hic accusamus. Consequatur in enim accusantium id sint repellat et.', 'Molestias id tempore fuga vel quis. Voluptatem necessitatibus voluptas nobis ver', 'Teatrul Metropolis', '2015-09-01 14:43:02', '2015-08-10 23:44:29');
INSERT INTO `projects` VALUES ('60', '26', 'Sed doloremque excepturi atque.', '3572', 'Sed enim velit velit ratione culpa. Velit provident est architecto. Ad et voluptatem facere reprehenderit voluptatem. Quis molestias quia non nesciunt sit consequatur explicabo. Iure perferendis inventore officiis vitae. Mollitia qui neque perferendis similique quia.', 'Maxime quia quas aut. Repellendus nulla qui nulla enim sed. Voluptas provident c', 'Independent', '2015-08-31 07:56:31', '2015-09-07 07:18:05');
INSERT INTO `projects` VALUES ('61', '27', 'Eaque quos eaque.', '594', 'Minima vitae libero quis in. Dignissimos animi et earum veniam aut perspiciatis. Magni fugit nam perferendis ipsum et omnis totam numquam. Deleniti nam repellat aspernatur iure magni. Atque pariatur neque sit doloribus voluptatum nostrum. Sint aut numquam laudantium eum magni libero. Molestias ducimus dicta sed velit.', 'Totam totam consequuntur dignissimos labore dolorem accusantium dicta. Illum rep', 'Compania de Teatru OKaua', '2015-09-02 08:30:19', '2015-09-02 20:01:11');
INSERT INTO `projects` VALUES ('62', '45', 'Est dignissimos autem ipsum quod.', '122', 'Fugit nulla praesentium ut soluta qui magnam tempora voluptates. Dolore sit aspernatur est ea fugit id. Facere blanditiis a omnis facere ea aut. Nulla enim velit qui non.', 'Aspernatur explicabo magnam suscipit architecto quod. Nisi commodi vero perferen', 'Compania de Teatru OKaua', '2015-08-17 16:26:14', '2015-09-01 07:27:55');
INSERT INTO `projects` VALUES ('63', '42', 'Est dolor et aut.', '618', 'Occaecati omnis illum voluptatem fugiat et non vero. Facere neque totam commodi. Vel hic impedit id repudiandae eos error occaecati cum. Labore consequatur labore occaecati ut rerum.', 'Facere sapiente non quasi odit nihil aut. Nisi dignissimos eos voluptatem debiti', 'Kanal D', '2015-09-06 10:39:34', '2015-08-25 22:09:30');
INSERT INTO `projects` VALUES ('64', '40', 'Sed corporis distinctio sit iusto.', '1448', 'Eos et soluta dicta nihil reiciendis veritatis. Dolor dolorem numquam ab. Quos quia vel velit non dignissimos sint. Impedit aut similique omnis natus a eos et perspiciatis. Dolorem blanditiis consequuntur at quisquam nulla labore aut.', 'Eius atque vel ipsam rerum dolor quisquam impedit sint. Est porro ab est minima.', 'UNATC', '2015-08-10 05:55:16', '2015-08-20 01:56:40');
INSERT INTO `projects` VALUES ('65', '14', 'Qui ab quia debitis fugit.', '3356', 'At quae eius autem aliquam exercitationem eum. Quo eos in autem sit assumenda placeat esse. Rerum voluptas doloribus aut error. Ducimus velit aspernatur iste quae sed.', 'Qui reprehenderit fugit asperiores magnam illum sed nihil. Delectus exercitation', 'Kanal D', '2015-08-29 09:10:19', '2015-09-06 10:19:19');
INSERT INTO `projects` VALUES ('66', '38', 'Temporibus distinctio sit impedit commodi accusamus.', '1640', 'Omnis libero officia consequatur temporibus. Impedit ad odit dolores est debitis suscipit doloremque. Laudantium ad quisquam quis aspernatur dolore enim. Assumenda temporibus dolorem distinctio dolor. Veritatis facilis natus eos et veniam architecto.', 'Similique in commodi repudiandae voluptas corporis aut ab. Harum dolorem alias e', 'Teatrul Bulandra', '2015-08-23 02:33:43', '2015-08-21 03:41:45');
INSERT INTO `projects` VALUES ('67', '17', 'Et dolorem soluta error.', '3347', 'Est ut delectus sint nemo nihil iure similique. Incidunt velit quia excepturi ducimus architecto repellat quas. Sed perspiciatis quos explicabo blanditiis soluta. Maxime ipsa consequatur quia recusandae. Rerum necessitatibus dolor sequi esse quis temporibus et. Fugit ullam dolorem quo iusto harum omnis et. Qui iure consectetur molestiae cupiditate earum eligendi.', 'Deleniti velit illum sit ut atque nobis. Velit quis laudantium ut est. Ut quo au', 'MediaPRO Pictures', '2015-08-13 15:26:00', '2015-09-07 06:03:22');
INSERT INTO `projects` VALUES ('68', '26', 'Cupiditate laudantium quisquam pariatur distinctio.', '2875', 'Aut veniam facere optio possimus. Sequi nihil veniam voluptatem asperiores impedit est. Iste odit dolor veniam quasi aut. Quasi vel iure impedit excepturi et quia aperiam fugit. Laboriosam nobis itaque occaecati dolores natus. Delectus placeat cupiditate et. Inventore est perferendis possimus.', 'Sit harum dolorum quas ut ducimus. Neque impedit repellendus quia maxime blandit', 'Independent', '2015-08-19 09:28:02', '2015-08-15 14:44:46');
INSERT INTO `projects` VALUES ('69', '29', 'Illo rerum ut.', '2919', 'Neque illum omnis et commodi. Saepe commodi dolor dolore consequatur et qui aut. Nemo earum ut suscipit sed illum aliquid quis vel. Asperiores reprehenderit ut recusandae maxime placeat. Rerum consectetur nisi voluptatem. Rerum non consequatur et laudantium minus et. Laudantium fuga vel iure praesentium nam dolorum sit dicta.', 'Quo accusamus eaque dolorum exercitationem tempora ut ullam. Ducimus voluptatem ', 'Teatrul Metropolis', '2015-08-17 07:45:32', '2015-08-13 17:55:18');
INSERT INTO `projects` VALUES ('70', '27', 'Odit deleniti iusto dolores.', '1069', 'Facilis dolores quidem quam nihil magnam quos. Quibusdam corporis perferendis alias. Sit iure sed quia neque ea et et. Maiores in necessitatibus quos et.', 'Saepe omnis omnis fugiat ut. Totam voluptate quidem in quia in commodi. Eaque co', 'Teatrul Bulandra', '2015-09-03 04:07:36', '2015-09-08 18:03:25');
INSERT INTO `projects` VALUES ('71', '10', 'Iure minima ipsum laborum cupiditate nostrum.', '684', 'Debitis ut ea repellendus magni. Aut laborum praesentium rem omnis in. Est quaerat est adipisci eius ad. Quod aut quo perspiciatis praesentium fugit. Ipsam ut autem sunt suscipit.', 'Aut aliquam enim harum. Dicta eum architecto recusandae distinctio. Qui et repud', 'Compania de Teatru OKaua', '2015-08-29 19:40:18', '2015-08-25 23:05:24');
INSERT INTO `projects` VALUES ('72', '3', 'Sunt qui et.', '53', 'Repudiandae sequi pariatur aspernatur suscipit voluptatem excepturi qui. Inventore excepturi numquam quia nihil molestias quaerat eaque. Qui est illum aspernatur sunt. Accusantium sit ut quo. Provident error veniam omnis autem. Temporibus tenetur recusandae voluptatibus fuga dolorem dolorem quia ipsum. Est animi placeat animi et sapiente.', 'Iste porro officia veniam ab animi tempora eum molestias. Iusto amet voluptatum ', 'UNATC', '2015-08-18 22:47:38', '2015-08-15 05:13:08');
INSERT INTO `projects` VALUES ('73', '43', 'Distinctio atque veniam aperiam amet.', '2147', 'Libero quaerat a corrupti exercitationem enim. Optio deleniti ut at rerum eos quibusdam. Doloremque ut sequi voluptates sequi nam dolor. Nihil aliquid ad reiciendis aspernatur fuga ut minima ut. Dolores repudiandae ut earum voluptas corrupti odio ad.', 'Nobis aut quod fugiat explicabo. Nihil sed veniam accusantium. Dolore sunt non v', 'Teatrul Metropolis', '2015-08-16 09:45:12', '2015-09-08 05:22:36');
INSERT INTO `projects` VALUES ('74', '50', 'Dolorum ad rem aliquid est.', '861', 'Officia consequatur omnis libero quia quam. Omnis velit quod at. Consectetur sapiente ex perspiciatis et velit amet. Eius nobis repellendus dolor quos occaecati aut autem.', 'Veritatis similique quae vero consequatur eveniet asperiores minima. Unde odio r', 'Kanal D', '2015-09-03 05:24:15', '2015-08-30 23:09:45');
INSERT INTO `projects` VALUES ('75', '10', 'A tenetur fugit dolor qui.', '3559', 'Modi facere occaecati possimus numquam ut. Occaecati eos est minima eveniet ipsum facilis. Alias voluptatem nam sed aut. Debitis facere aut necessitatibus consequatur velit rerum. Aut sit et velit temporibus voluptates. Non voluptatem dolore quis eius qui est. Assumenda alias facilis ipsa eum non.', 'Non amet ut distinctio ullam ab. Qui qui eos animi est. Laudantium id illum et. ', 'UNATC', '2015-08-19 19:27:04', '2015-08-17 00:39:40');
INSERT INTO `projects` VALUES ('76', '24', 'Eius molestiae aut est.', '2244', 'Ducimus id reiciendis voluptatem eum magnam. Dolores est sed adipisci quaerat necessitatibus. Eum accusantium aut illum consectetur architecto repudiandae cum. Fugit quaerat est laboriosam ut nam optio. Iste voluptatem quibusdam consequatur provident voluptatem numquam et. Quia ut eveniet soluta pariatur.', 'Optio magni excepturi quod minus. Corrupti blanditiis debitis ut qui eligendi qu', 'Compania de Teatru OKaua', '2015-08-26 01:00:19', '2015-08-17 17:02:33');
INSERT INTO `projects` VALUES ('77', '17', 'Dignissimos laborum autem eveniet voluptas.', '662', 'Sed hic nemo accusantium. Suscipit sequi animi nemo esse. Voluptatem magni nobis sunt. Et excepturi ab deserunt sint et in. Architecto qui et voluptatem vel rerum voluptas sed.', 'Exercitationem earum beatae aliquid velit molestiae et aut. Dolor voluptatem nem', 'Teatrul Bulandra', '2015-08-30 09:20:24', '2015-08-16 23:43:16');
INSERT INTO `projects` VALUES ('78', '4', 'Est quasi enim illum facilis.', '1316', 'Ut libero eius aliquid sunt. Vel autem aspernatur dolorum non dolores et. Aspernatur ut nam nisi accusantium sit. Enim voluptatibus consequatur omnis quam similique voluptatem. Dolores voluptates blanditiis magni quidem sed ratione.', 'Blanditiis ut commodi repudiandae aspernatur rerum excepturi. Ex error sequi eos', 'Teatrul Bulandra', '2015-08-16 23:12:44', '2015-08-15 20:37:44');
INSERT INTO `projects` VALUES ('79', '18', 'Beatae debitis accusamus.', '1779', 'Vel aut fugiat ut aut magnam tempora rerum. Similique et qui vero accusantium. Sunt laudantium veritatis id numquam quis sunt dolores. Laborum aspernatur et quia odio ipsa voluptatem. Et ipsum qui nulla repellendus culpa illo.', 'Quia asperiores dolores sed a. Numquam amet qui quae porro quia quidem enim. Mag', 'MediaPRO Pictures', '2015-08-23 03:19:26', '2015-08-21 20:26:31');
INSERT INTO `projects` VALUES ('80', '28', 'Nihil doloremque aut laborum.', '1688', 'Illum aut libero vitae dolores quam. Magni quod explicabo velit possimus. Rem corporis laboriosam dolore quia et velit. Incidunt dignissimos at id hic.', 'Magnam rerum at nisi qui deserunt. Occaecati vel quaerat laboriosam. Tenetur max', 'Teatrul Metropolis', '2015-08-29 02:34:01', '2015-09-03 05:24:16');
INSERT INTO `projects` VALUES ('81', '7', 'Odit dolorem at quasi.', '796', 'Et optio quia facere id reprehenderit debitis. Repellat fugit tenetur alias et. Quidem neque aliquid consequuntur nisi consequuntur hic ex. Explicabo ut eveniet minima. Repudiandae cumque occaecati voluptate iusto enim. Quas adipisci fugiat vitae facere nihil rem molestias.', 'Odit repellendus veritatis nulla dolorum ipsa quod. Ipsa error qui consectetur r', 'Teatrul Metropolis', '2015-09-02 20:55:06', '2015-08-12 12:14:15');
INSERT INTO `projects` VALUES ('82', '15', 'Et incidunt maxime hic.', '1530', 'Est quis asperiores laborum voluptatibus ut aut. Earum velit dolor eos et nulla. Id rerum porro voluptas blanditiis animi nisi. Dolorum minus sit perferendis voluptate exercitationem porro fugit aut. Maiores cumque perferendis iste laudantium et.', 'Dolorem dolor aut sequi reprehenderit harum. Voluptas saepe sint consequatur aut', 'MediaPRO Pictures', '2015-08-09 13:52:02', '2015-08-28 12:01:59');
INSERT INTO `projects` VALUES ('83', '41', 'Aliquid et maiores.', '1199', 'Ipsum reiciendis nemo possimus quisquam. Exercitationem numquam occaecati quis atque ut. Est autem alias et explicabo voluptate quae. Aut ad hic at et dolor facere sit. Animi perferendis exercitationem quam commodi. Laboriosam nostrum ratione eos aut ut.', 'Ratione odit culpa sit ullam porro tempore numquam. Nihil consequuntur magnam qu', 'Independent', '2015-08-19 10:46:58', '2015-08-16 14:56:56');
INSERT INTO `projects` VALUES ('84', '26', 'Consequuntur fugit ad suscipit aut.', '2205', 'Quia quam nihil voluptate. Pariatur iure deserunt corrupti atque architecto minima quaerat. Explicabo aliquid ipsa ullam soluta dolore cumque. Quod ipsam laudantium cupiditate molestiae eum provident.', 'Aut porro necessitatibus qui maiores aperiam rerum. Dicta voluptatem alias at qu', 'MediaPRO Pictures', '2015-08-09 21:15:30', '2015-08-17 20:34:55');
INSERT INTO `projects` VALUES ('85', '45', 'Veritatis modi cum et.', '2519', 'Ullam et eos nihil odit accusantium. Ea quas molestias deleniti eaque voluptatum mollitia. Doloribus sed minima deserunt non sequi sequi est. Blanditiis dolore quam ea facere maiores.', 'Dolorem ad fugit voluptatem quisquam pariatur modi enim. Officia voluptas porro ', 'MediaPRO Pictures', '2015-08-15 14:47:18', '2015-08-19 20:16:53');
INSERT INTO `projects` VALUES ('86', '13', 'Voluptatibus quia maxime consectetur eaque.', '43', 'Molestias similique dolorem modi sunt. Est dolor sunt eum officiis consectetur velit. Temporibus repellendus error ut est maxime fugit. Laboriosam necessitatibus voluptatem voluptatem tempora laborum soluta porro ex.', 'Qui similique facilis ab omnis. Delectus sint aut enim placeat aut porro consequ', 'Teatrul Metropolis', '2015-08-20 23:48:12', '2015-08-30 09:15:27');
INSERT INTO `projects` VALUES ('87', '2', 'Et rerum nostrum ea in est.', '1003', 'At maiores laboriosam quidem ipsam alias quos aliquam iste. Eos optio ut voluptate quia magnam reiciendis consequatur. Enim aut consequatur et ipsum. Eos quae id similique.', 'Commodi exercitationem necessitatibus voluptas aut. Omnis delectus ea fuga qui e', 'Compania de Teatru OKaua', '2015-08-12 07:21:24', '2015-08-14 20:22:48');
INSERT INTO `projects` VALUES ('88', '37', 'Nihil culpa exercitationem non inventore vitae.', '2130', 'Eveniet qui sunt fugit optio mollitia nisi. Quia quas quo enim quam occaecati. Est sunt quaerat dolor eligendi eos ut. Alias vitae ex praesentium et sit. Labore quo aliquid aut facilis est.', 'Sint et molestiae nulla quisquam necessitatibus fugiat ad optio. Dolorum repudia', 'UNATC', '2015-08-16 23:12:11', '2015-09-03 06:37:06');
INSERT INTO `projects` VALUES ('89', '35', 'Quae est nostrum debitis consectetur.', '781', 'Omnis necessitatibus officiis harum iusto est. Voluptatem dolorem et earum quas autem nobis sed. Velit qui facilis illum voluptas aut voluptatibus. Voluptatum ut fuga consequatur vel eligendi qui. Reiciendis molestiae eos enim itaque optio. Quo ad quo impedit nulla. Assumenda quis mollitia dolores. Sunt sequi et dignissimos saepe et reprehenderit.', 'Delectus quia placeat dolorem et tenetur. Expedita corporis ea non voluptates qu', 'Teatrul Bulandra', '2015-08-31 15:55:09', '2015-08-13 15:28:13');
INSERT INTO `projects` VALUES ('90', '45', 'Explicabo nemo quia a est.', '2205', 'Et modi omnis quos quibusdam distinctio quas recusandae. Tempora nobis sint nisi fugiat. Optio tempora sapiente quia quae non praesentium. Commodi eum corrupti neque vel ea deserunt harum explicabo. Quisquam qui voluptates ut.', 'Non porro nemo consequatur dolore qui similique vel. Quis eveniet repellendus de', 'Independent', '2015-08-29 11:50:00', '2015-09-08 07:23:52');
INSERT INTO `projects` VALUES ('91', '43', 'Voluptatem illum reiciendis.', '2846', 'Esse hic omnis dignissimos iste. Dolores commodi et et voluptate. Atque magnam in blanditiis. Est qui vitae ad rerum repellendus non odio. Sed harum quaerat mollitia et. Voluptatum debitis est reiciendis molestiae aut quia. Enim quam in non eum debitis libero optio excepturi.', 'Aut et aperiam ut et reiciendis pariatur quia dolores. Repellat iusto autem exer', 'Kanal D', '2015-08-30 06:39:36', '2015-09-02 09:16:46');
INSERT INTO `projects` VALUES ('92', '16', 'Ea architecto fugit aut error.', '2351', 'Ipsum aut suscipit ducimus eum similique suscipit. Omnis quae et id quasi veritatis non. Quo reiciendis doloremque non fugit dolor. Inventore consectetur libero perferendis ex. Delectus tempore minima optio hic. Necessitatibus quia quae dolor natus.', 'Sit tempora ipsa voluptatem reiciendis excepturi eaque doloribus. Repellendus am', 'Kanal D', '2015-08-11 11:13:46', '2015-09-02 04:07:01');
INSERT INTO `projects` VALUES ('93', '43', 'Sunt culpa ex.', '1685', 'Sapiente mollitia fugit deserunt. Voluptatem non vero sit mollitia rerum id. Assumenda et reprehenderit libero et. Dolor magnam cumque ducimus. Voluptatibus ut asperiores maxime temporibus. In voluptatibus aliquid animi enim quia.', 'Vitae sint nesciunt quia error delectus rerum officiis. Provident error id debit', 'MediaPRO Pictures', '2015-08-27 06:31:48', '2015-09-02 01:00:33');
INSERT INTO `projects` VALUES ('94', '37', 'Numquam est reprehenderit similique at est.', '1805', 'Nemo laborum iste aut sint aut voluptatum. Et quod vero deserunt aut distinctio nostrum voluptatem. Ducimus culpa rem at voluptate. Veniam autem delectus tempora non laboriosam harum. Rerum at non quibusdam et dolores. Dicta commodi ipsam cupiditate sunt enim. Eos ipsa consequuntur est commodi fugiat modi.', 'Id voluptatem est dolores illum commodi. Quo repellendus odit molestiae repudian', 'Kanal D', '2015-08-28 18:15:00', '2015-08-28 13:29:51');
INSERT INTO `projects` VALUES ('95', '23', 'Dolores minima corrupti in quaerat delectus.', '1023', 'Ipsa quasi repellat aut veniam totam quae. Sit ut quas aspernatur maiores fuga velit est illum. Minus voluptatibus minus voluptas et. Sint omnis enim consectetur non officia rerum adipisci.', 'Non dignissimos eveniet praesentium facere dolore. Quis deleniti laudantium non ', 'Teatrul Metropolis', '2015-08-19 17:07:59', '2015-08-23 19:59:21');
INSERT INTO `projects` VALUES ('96', '30', 'Et qui distinctio impedit accusamus distinctio.', '465', 'Omnis numquam praesentium enim nihil. Veritatis ut voluptatem ab aut consequatur officiis dolorem. Sed eius quidem nam aperiam quos. Non dolorem consequatur eum et qui nobis fugiat. Possimus vitae sequi voluptatem voluptatem nesciunt. Aut autem veniam quam nam dolorem assumenda sed.', 'Velit facere in ea maxime. Necessitatibus sunt non totam. Et et ducimus maxime a', 'Kanal D', '2015-08-14 03:22:24', '2015-08-15 06:58:18');
INSERT INTO `projects` VALUES ('97', '16', 'Est voluptas ipsum.', '907', 'Error illo et modi officia blanditiis. Atque aspernatur nihil saepe consequatur. Nihil cum officiis quia iusto est. Mollitia vel autem sunt eius minus sit.', 'Tenetur sapiente omnis id veniam id odit. Qui consequuntur porro reprehenderit a', 'UNATC', '2015-09-04 00:42:49', '2015-09-06 01:52:23');
INSERT INTO `projects` VALUES ('98', '25', 'Qui in dolore iste.', '3632', 'Quia consequatur quis omnis aliquam voluptatem. Atque et et est minus perspiciatis id. Sed illo veritatis quia. Reiciendis reprehenderit rerum qui omnis sequi. Minus maxime quia cum vel. Officia dolorum fugit ea optio.', 'Mollitia est qui perferendis est dicta beatae. Rerum omnis tempore iure. Adipisc', 'MediaPRO Pictures', '2015-08-29 12:16:03', '2015-08-31 06:03:58');
INSERT INTO `projects` VALUES ('99', '12', 'Nobis asperiores in veniam qui.', '1786', 'Et culpa autem eius. Soluta reiciendis voluptatem facere iure quisquam consequatur maiores facilis. Autem aspernatur mollitia praesentium assumenda sapiente cum quia. Sequi in nobis repudiandae eius aut.', 'Porro ut recusandae nihil dolores qui. Doloribus est dolorum est cupiditate dolo', 'Compania de Teatru OKaua', '2015-09-02 01:22:34', '2015-08-20 16:52:36');
INSERT INTO `projects` VALUES ('100', '7', 'Est illo possimus veritatis iure libero.', '1162', 'Molestiae pariatur molestiae ducimus dolorem. Cupiditate temporibus officiis rerum quidem nostrum rerum. Assumenda qui qui ea quidem. Eum et sed ut id deleniti. Iste aut ut provident omnis consequatur aut necessitatibus molestiae. Molestiae atque incidunt sapiente nobis animi sapiente.', 'Necessitatibus rem totam quo magnam. Neque cumque nemo est est sed velit minus. ', 'UNATC', '2015-08-09 07:17:00', '2015-08-17 11:18:00');

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES ('1', 'admin', 'Administrative user, has access to himself, all talents & employers', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `roles` VALUES ('2', 'superadmin', 'Super-administrative, has access to all users', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `roles` VALUES ('3', 'talent', 'One of the talents, has acces only to himself', '2015-09-08 22:29:29', '2015-09-08 22:29:29');
INSERT INTO `roles` VALUES ('4', 'employer', 'Employer, has access to himself and all talents', '2015-09-08 22:29:29', '2015-09-08 22:29:29');

-- ----------------------------
-- Table structure for soundclouds
-- ----------------------------
DROP TABLE IF EXISTS `soundclouds`;
CREATE TABLE `soundclouds` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `soundclouds_user_id_index` (`user_id`),
  CONSTRAINT `soundclouds_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of soundclouds
-- ----------------------------
INSERT INTO `soundclouds` VALUES ('1', '2', '176810656', '2015-08-22 01:45:56', '2015-09-06 19:06:55');
INSERT INTO `soundclouds` VALUES ('2', '2', '36799191', '2015-08-22 06:26:45', '2015-08-12 08:53:57');
INSERT INTO `soundclouds` VALUES ('3', '46', '36799191', '2015-08-26 07:26:18', '2015-09-05 12:15:29');
INSERT INTO `soundclouds` VALUES ('4', '12', '176810656', '2015-08-19 14:19:09', '2015-08-20 07:48:02');
INSERT INTO `soundclouds` VALUES ('5', '10', '176810656', '2015-09-07 10:37:03', '2015-09-03 10:42:14');
INSERT INTO `soundclouds` VALUES ('6', '25', '185190547', '2015-09-06 01:48:24', '2015-08-09 17:12:37');
INSERT INTO `soundclouds` VALUES ('7', '5', '176810656', '2015-09-07 14:49:00', '2015-09-06 17:49:49');
INSERT INTO `soundclouds` VALUES ('8', '36', '7938506', '2015-08-23 13:23:09', '2015-08-10 15:14:41');
INSERT INTO `soundclouds` VALUES ('9', '16', '36799191', '2015-08-12 22:18:47', '2015-08-25 11:30:14');
INSERT INTO `soundclouds` VALUES ('10', '24', '36799191', '2015-08-17 17:46:14', '2015-09-01 20:05:11');
INSERT INTO `soundclouds` VALUES ('11', '10', '36799191', '2015-09-07 09:14:27', '2015-08-10 05:40:24');
INSERT INTO `soundclouds` VALUES ('12', '16', '176810656', '2015-08-09 21:22:11', '2015-08-09 07:06:19');
INSERT INTO `soundclouds` VALUES ('13', '24', '185190547', '2015-08-23 15:07:41', '2015-08-19 15:52:30');
INSERT INTO `soundclouds` VALUES ('14', '36', '36799191', '2015-08-30 01:57:08', '2015-08-27 09:46:04');
INSERT INTO `soundclouds` VALUES ('15', '22', '7938506', '2015-08-22 13:04:50', '2015-09-03 15:28:22');
INSERT INTO `soundclouds` VALUES ('16', '35', '7938506', '2015-09-07 20:34:07', '2015-08-29 07:32:50');
INSERT INTO `soundclouds` VALUES ('17', '5', '185190547', '2015-08-14 06:33:10', '2015-08-14 09:00:20');
INSERT INTO `soundclouds` VALUES ('18', '20', '7938506', '2015-08-22 07:22:51', '2015-08-19 22:25:18');
INSERT INTO `soundclouds` VALUES ('19', '47', '176810656', '2015-08-12 15:00:59', '2015-08-20 17:17:38');
INSERT INTO `soundclouds` VALUES ('20', '45', '185190547', '2015-08-15 19:10:54', '2015-08-27 13:00:01');
INSERT INTO `soundclouds` VALUES ('21', '28', '7938506', '2015-08-29 01:21:43', '2015-08-26 23:17:00');
INSERT INTO `soundclouds` VALUES ('22', '23', '185190547', '2015-09-01 14:22:32', '2015-08-16 13:49:24');
INSERT INTO `soundclouds` VALUES ('23', '16', '36799191', '2015-09-03 04:47:19', '2015-08-29 17:28:16');
INSERT INTO `soundclouds` VALUES ('24', '27', '36799191', '2015-09-02 01:46:45', '2015-08-13 15:31:12');
INSERT INTO `soundclouds` VALUES ('25', '19', '36799191', '2015-08-22 16:58:19', '2015-09-06 10:01:26');
INSERT INTO `soundclouds` VALUES ('26', '49', '36799191', '2015-08-11 03:16:18', '2015-08-24 11:20:46');
INSERT INTO `soundclouds` VALUES ('27', '46', '185190547', '2015-09-03 12:08:34', '2015-09-04 17:01:26');
INSERT INTO `soundclouds` VALUES ('28', '32', '7938506', '2015-08-16 20:10:58', '2015-09-02 10:31:13');
INSERT INTO `soundclouds` VALUES ('29', '38', '176810656', '2015-08-14 16:31:49', '2015-08-15 17:36:57');
INSERT INTO `soundclouds` VALUES ('30', '23', '36799191', '2015-08-27 02:23:07', '2015-08-08 23:13:44');
INSERT INTO `soundclouds` VALUES ('31', '44', '176810656', '2015-08-28 07:36:03', '2015-08-12 01:57:23');
INSERT INTO `soundclouds` VALUES ('32', '11', '36799191', '2015-09-05 20:15:27', '2015-08-27 05:50:48');
INSERT INTO `soundclouds` VALUES ('33', '12', '36799191', '2015-08-27 19:17:25', '2015-08-30 18:23:46');
INSERT INTO `soundclouds` VALUES ('34', '12', '36799191', '2015-08-15 09:22:31', '2015-08-15 00:24:49');
INSERT INTO `soundclouds` VALUES ('35', '14', '176810656', '2015-08-23 22:38:19', '2015-08-29 13:26:57');
INSERT INTO `soundclouds` VALUES ('36', '19', '185190547', '2015-08-24 19:11:30', '2015-08-15 10:41:22');
INSERT INTO `soundclouds` VALUES ('37', '45', '176810656', '2015-08-17 20:01:07', '2015-08-25 15:43:24');
INSERT INTO `soundclouds` VALUES ('38', '47', '176810656', '2015-09-06 17:13:42', '2015-09-08 13:48:17');
INSERT INTO `soundclouds` VALUES ('39', '33', '176810656', '2015-08-21 21:13:50', '2015-08-22 05:06:23');
INSERT INTO `soundclouds` VALUES ('40', '5', '185190547', '2015-08-14 18:20:33', '2015-08-18 01:38:36');
INSERT INTO `soundclouds` VALUES ('41', '19', '36799191', '2015-08-19 16:37:55', '2015-08-31 05:49:38');
INSERT INTO `soundclouds` VALUES ('42', '48', '176810656', '2015-09-08 02:53:00', '2015-08-16 21:25:17');
INSERT INTO `soundclouds` VALUES ('43', '34', '176810656', '2015-08-22 10:49:56', '2015-09-07 04:16:46');
INSERT INTO `soundclouds` VALUES ('44', '1', '36799191', '2015-08-09 13:35:04', '2015-09-08 11:55:57');
INSERT INTO `soundclouds` VALUES ('45', '21', '36799191', '2015-08-25 12:33:05', '2015-08-17 13:30:32');
INSERT INTO `soundclouds` VALUES ('46', '47', '176810656', '2015-08-22 09:08:23', '2015-08-25 20:08:27');
INSERT INTO `soundclouds` VALUES ('47', '7', '36799191', '2015-08-21 00:08:01', '2015-08-10 12:38:33');
INSERT INTO `soundclouds` VALUES ('48', '14', '7938506', '2015-08-24 13:45:44', '2015-08-15 03:51:10');
INSERT INTO `soundclouds` VALUES ('49', '37', '185190547', '2015-08-24 20:33:40', '2015-08-17 04:52:05');
INSERT INTO `soundclouds` VALUES ('50', '25', '36799191', '2015-08-14 22:59:49', '2015-09-04 23:02:37');

-- ----------------------------
-- Table structure for tracks
-- ----------------------------
DROP TABLE IF EXISTS `tracks`;
CREATE TABLE `tracks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `tracks_user_id_index` (`user_id`),
  CONSTRAINT `tracks_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tracks
-- ----------------------------
INSERT INTO `tracks` VALUES ('1', '24', 'Et sit quo.', 'http://lorempixel.com/640/480/?13091', '2015-09-02 22:49:13', '2015-09-04 01:59:41');
INSERT INTO `tracks` VALUES ('2', '33', 'Quo a officia.', 'http://lorempixel.com/640/480/?25627', '2015-09-05 21:21:22', '2015-08-30 01:04:39');
INSERT INTO `tracks` VALUES ('3', '26', 'Totam ipsam sequi.', 'http://lorempixel.com/640/480/?62085', '2015-08-23 04:26:32', '2015-09-07 03:00:56');
INSERT INTO `tracks` VALUES ('4', '17', 'Rerum necessitatibus rerum.', 'http://lorempixel.com/640/480/?91302', '2015-09-03 23:26:43', '2015-08-16 16:00:06');
INSERT INTO `tracks` VALUES ('5', '23', 'Esse quo ullam.', 'http://lorempixel.com/640/480/?73678', '2015-08-19 00:13:44', '2015-08-26 14:18:21');
INSERT INTO `tracks` VALUES ('6', '27', 'Blanditiis eveniet.', 'http://lorempixel.com/640/480/?33408', '2015-08-23 18:28:51', '2015-08-13 16:17:54');
INSERT INTO `tracks` VALUES ('7', '41', 'Dolores quia soluta.', 'http://lorempixel.com/640/480/?72945', '2015-08-11 00:29:11', '2015-08-21 06:27:22');
INSERT INTO `tracks` VALUES ('8', '44', 'Perspiciatis distinctio temporibus.', 'http://lorempixel.com/640/480/?53836', '2015-08-22 02:59:57', '2015-09-07 21:59:23');
INSERT INTO `tracks` VALUES ('9', '31', 'Facilis rerum.', 'http://lorempixel.com/640/480/?27361', '2015-08-10 07:47:02', '2015-09-06 01:51:25');
INSERT INTO `tracks` VALUES ('10', '11', 'Rerum possimus magnam.', 'http://lorempixel.com/640/480/?23730', '2015-08-16 16:28:13', '2015-08-23 02:01:38');
INSERT INTO `tracks` VALUES ('11', '41', 'Facilis enim.', 'http://lorempixel.com/640/480/?20869', '2015-08-24 00:11:39', '2015-08-27 02:36:34');
INSERT INTO `tracks` VALUES ('12', '23', 'Harum alias harum.', 'http://lorempixel.com/640/480/?74894', '2015-09-06 13:52:23', '2015-09-01 21:26:13');
INSERT INTO `tracks` VALUES ('13', '13', 'Ratione aliquam.', 'http://lorempixel.com/640/480/?53772', '2015-08-30 16:04:34', '2015-08-10 00:55:44');
INSERT INTO `tracks` VALUES ('14', '37', 'Possimus perferendis reprehenderit.', 'http://lorempixel.com/640/480/?58872', '2015-08-17 18:58:00', '2015-08-25 17:27:18');
INSERT INTO `tracks` VALUES ('15', '47', 'Omnis maxime cumque.', 'http://lorempixel.com/640/480/?59742', '2015-08-31 19:59:58', '2015-08-10 03:43:30');
INSERT INTO `tracks` VALUES ('16', '6', 'Sunt esse.', 'http://lorempixel.com/640/480/?91470', '2015-08-15 16:35:11', '2015-08-16 08:16:20');
INSERT INTO `tracks` VALUES ('17', '27', 'Aut tempore explicabo.', 'http://lorempixel.com/640/480/?87908', '2015-08-27 20:30:26', '2015-08-13 14:35:32');
INSERT INTO `tracks` VALUES ('18', '21', 'Mollitia qui.', 'http://lorempixel.com/640/480/?15229', '2015-08-15 10:09:34', '2015-08-24 22:57:54');
INSERT INTO `tracks` VALUES ('19', '12', 'Quasi itaque excepturi.', 'http://lorempixel.com/640/480/?30938', '2015-08-25 10:59:52', '2015-09-02 15:34:24');
INSERT INTO `tracks` VALUES ('20', '21', 'Possimus explicabo.', 'http://lorempixel.com/640/480/?74989', '2015-09-02 12:28:12', '2015-09-05 13:26:39');
INSERT INTO `tracks` VALUES ('21', '8', 'Eveniet perferendis omnis.', 'http://lorempixel.com/640/480/?12188', '2015-08-29 02:22:12', '2015-09-03 06:54:24');
INSERT INTO `tracks` VALUES ('22', '24', 'Quia quis autem.', 'http://lorempixel.com/640/480/?15680', '2015-09-06 03:21:28', '2015-08-21 04:48:57');
INSERT INTO `tracks` VALUES ('23', '34', 'Perspiciatis labore dolorem.', 'http://lorempixel.com/640/480/?75295', '2015-08-15 17:28:49', '2015-08-23 05:53:35');
INSERT INTO `tracks` VALUES ('24', '19', 'Veniam sequi architecto.', 'http://lorempixel.com/640/480/?80210', '2015-08-21 01:40:14', '2015-08-11 19:12:11');
INSERT INTO `tracks` VALUES ('25', '11', 'Omnis qui.', 'http://lorempixel.com/640/480/?88825', '2015-08-28 05:30:58', '2015-08-12 15:50:27');
INSERT INTO `tracks` VALUES ('26', '4', 'Distinctio qui.', 'http://lorempixel.com/640/480/?56562', '2015-09-02 13:41:08', '2015-08-16 15:32:26');
INSERT INTO `tracks` VALUES ('27', '28', 'Earum rem.', 'http://lorempixel.com/640/480/?16681', '2015-08-11 14:06:27', '2015-08-23 23:01:43');
INSERT INTO `tracks` VALUES ('28', '36', 'Veniam quam.', 'http://lorempixel.com/640/480/?11115', '2015-09-04 04:46:44', '2015-09-03 18:17:33');
INSERT INTO `tracks` VALUES ('29', '44', 'Natus tenetur.', 'http://lorempixel.com/640/480/?76532', '2015-09-03 00:01:49', '2015-08-19 15:15:44');
INSERT INTO `tracks` VALUES ('30', '15', 'Sit et distinctio.', 'http://lorempixel.com/640/480/?64100', '2015-09-05 00:12:47', '2015-08-25 20:40:40');
INSERT INTO `tracks` VALUES ('31', '44', 'Voluptas aspernatur itaque.', 'http://lorempixel.com/640/480/?94203', '2015-08-09 00:37:03', '2015-09-01 16:11:09');
INSERT INTO `tracks` VALUES ('32', '17', 'Architecto quia voluptatem.', 'http://lorempixel.com/640/480/?66403', '2015-08-23 20:32:23', '2015-09-02 22:09:08');
INSERT INTO `tracks` VALUES ('33', '47', 'Beatae at.', 'http://lorempixel.com/640/480/?49314', '2015-08-12 21:07:12', '2015-08-16 05:27:49');
INSERT INTO `tracks` VALUES ('34', '20', 'Nulla explicabo.', 'http://lorempixel.com/640/480/?13146', '2015-08-25 13:34:57', '2015-08-24 19:57:00');
INSERT INTO `tracks` VALUES ('35', '34', 'Et eos et.', 'http://lorempixel.com/640/480/?57706', '2015-08-14 15:25:29', '2015-08-17 14:35:07');
INSERT INTO `tracks` VALUES ('36', '20', 'Recusandae officiis.', 'http://lorempixel.com/640/480/?41134', '2015-09-04 13:28:43', '2015-08-20 22:23:22');
INSERT INTO `tracks` VALUES ('37', '46', 'Ea dolores.', 'http://lorempixel.com/640/480/?16106', '2015-08-09 08:32:56', '2015-08-20 01:59:59');
INSERT INTO `tracks` VALUES ('38', '24', 'Ex ut.', 'http://lorempixel.com/640/480/?17357', '2015-09-08 04:08:52', '2015-08-18 03:23:17');
INSERT INTO `tracks` VALUES ('39', '14', 'Veritatis deserunt.', 'http://lorempixel.com/640/480/?68271', '2015-08-30 06:27:18', '2015-08-19 18:58:11');
INSERT INTO `tracks` VALUES ('40', '26', 'Excepturi asperiores dolorem.', 'http://lorempixel.com/640/480/?46284', '2015-08-29 01:34:59', '2015-08-18 05:41:55');
INSERT INTO `tracks` VALUES ('41', '35', 'Dolorem tenetur.', 'http://lorempixel.com/640/480/?39568', '2015-08-30 02:08:02', '2015-08-25 02:29:26');
INSERT INTO `tracks` VALUES ('42', '4', 'Delectus beatae.', 'http://lorempixel.com/640/480/?70324', '2015-09-06 19:18:04', '2015-09-03 09:59:05');
INSERT INTO `tracks` VALUES ('43', '48', 'Adipisci quos.', 'http://lorempixel.com/640/480/?59691', '2015-08-11 11:08:38', '2015-09-04 14:42:23');
INSERT INTO `tracks` VALUES ('44', '47', 'Inventore consequatur.', 'http://lorempixel.com/640/480/?94183', '2015-09-07 12:31:46', '2015-09-02 01:28:57');
INSERT INTO `tracks` VALUES ('45', '40', 'Non quas.', 'http://lorempixel.com/640/480/?65654', '2015-09-06 21:25:22', '2015-08-21 21:23:32');
INSERT INTO `tracks` VALUES ('46', '45', 'Rerum eveniet quia.', 'http://lorempixel.com/640/480/?15840', '2015-08-22 21:08:09', '2015-08-14 16:27:24');
INSERT INTO `tracks` VALUES ('47', '3', 'Laboriosam et.', 'http://lorempixel.com/640/480/?94277', '2015-08-15 22:09:00', '2015-08-18 12:55:08');
INSERT INTO `tracks` VALUES ('48', '17', 'Odit saepe consequatur.', 'http://lorempixel.com/640/480/?96165', '2015-08-15 18:38:50', '2015-08-14 14:43:44');
INSERT INTO `tracks` VALUES ('49', '15', 'Quo reprehenderit officiis.', 'http://lorempixel.com/640/480/?49830', '2015-08-12 10:36:12', '2015-08-14 09:30:14');
INSERT INTO `tracks` VALUES ('50', '14', 'Magnam ut.', 'http://lorempixel.com/640/480/?90072', '2015-08-09 00:35:42', '2015-08-23 14:30:54');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `country_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `profile_language` int(10) unsigned NOT NULL,
  `employer_type_id` int(10) unsigned DEFAULT NULL,
  `actor_type_id` int(10) unsigned DEFAULT NULL,
  `active` tinyint(1) NOT NULL,
  `approved` tinyint(1) NOT NULL,
  `complete` tinyint(1) NOT NULL,
  `username` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `fname` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `lname` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `mobile` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `sex` enum('M','F') COLLATE utf8_unicode_ci DEFAULT NULL,
  `logins` smallint(5) unsigned NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `birth_date` date DEFAULT NULL,
  `code` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `thumb_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_role_id_foreign` (`role_id`),
  KEY `users_profile_language_foreign` (`profile_language`),
  KEY `users_employer_type_id_foreign` (`employer_type_id`),
  KEY `users_actor_type_id_foreign` (`actor_type_id`),
  KEY `users_country_id_index` (`country_id`),
  CONSTRAINT `users_actor_type_id_foreign` FOREIGN KEY (`actor_type_id`) REFERENCES `actor_types` (`id`) ON DELETE CASCADE,
  CONSTRAINT `users_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE CASCADE,
  CONSTRAINT `users_employer_type_id_foreign` FOREIGN KEY (`employer_type_id`) REFERENCES `employer_types` (`id`) ON DELETE CASCADE,
  CONSTRAINT `users_profile_language_foreign` FOREIGN KEY (`profile_language`) REFERENCES `languages` (`id`) ON DELETE CASCADE,
  CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', '32', '1', '2', null, null, '1', '1', '1', 'mPaun', 'qTrif@Stancu.com', '$2y$10$35/JkbKQGVssY9lwQ6MmI.4mxV7EfOz0g1BK7QZOF.55rIlqXOiZ6', 'Amelia', 'Panait', 'Mun. Brezoi', '0350025293', 'F', '7', '2015-02-04 08:59:44', '1988-11-23', null, 'JxK7BWdvsUwkqwA2rjtkldLGF6skMDCCcbbH8eWAMaBxSBKfWTlrlgw4kNXS', '2015-04-25 01:28:47', '2015-09-08 22:30:47', null);
INSERT INTO `users` VALUES ('2', '86', '3', '2', null, null, '1', '1', '0', 'Ghita.Catinca', 'pMoga@gmail.com', '$2y$10$o0koTqxSy3wQDCyHn9RtM.UOKuMtvHcDmaQ4cVGRitrzYaoGTX.Fu', 'Ionuț', 'Cristea', 'Mun. Pantelimon', '0316477887', 'F', '10', '2014-10-16 19:12:33', '2001-11-02', null, null, '2015-03-26 13:56:17', '2015-08-16 02:01:16', null);
INSERT INTO `users` VALUES ('3', '189', '4', '2', null, null, '0', '1', '1', 'Nagy.Eustatiu', 'Silvia98@Mazilu.info', '$2y$10$.VlJrYu2y/orkGtgN/pmPu9tXe5Of5CQZzIvwNqTgxPAs/shj5Oaa', 'Carla', 'Paraschiv', 'Câmpia Turzii', '0247897781', 'F', '8', '2015-04-19 23:35:37', '1989-10-23', null, null, '2015-03-22 01:24:01', '2015-08-24 14:39:01', null);
INSERT INTO `users` VALUES ('4', '226', '2', '1', null, null, '0', '0', '1', 'Catalina.Petrea', 'Antonela.Oprea@hotmail.com', '$2y$10$FB.2g27k2cy0XuUnTnSNTOtIMy4.6X5qI8f8Gg.LIJQu632s0RY.y', 'Remus', 'Fratila', 'Mun. Hârșova', '0230731396', 'F', '10', '2015-07-01 16:38:32', '1981-03-20', null, null, '2008-07-21 04:46:34', '2015-09-07 08:10:59', null);
INSERT INTO `users` VALUES ('5', '122', '2', '2', null, null, '0', '0', '1', 'Profira33', 'Sabin.Pintilie@Oprea.com', '$2y$10$ve9hV.eV6uzE7PF1GlYyLOSZv23MJvPSsNTwpLqcf9bMIAXkih5xm', 'Romeo', 'Tudorache', 'Mun. Deva', '0787655349', 'M', '3', '2014-12-25 20:17:26', '1999-09-25', null, null, '2008-08-05 15:28:11', '2015-08-18 04:23:22', null);
INSERT INTO `users` VALUES ('6', '126', '3', '1', null, null, '0', '0', '1', 'Cristofor.Pavel', 'Beatrice99@Preda.com', '$2y$10$xyH4VBqxrjYnvdMkuU9ri.ST7HOAa6mFaaK4G.2xaBLoA4bqN8b42', 'Liana', 'Niculescu', 'Mun. Bălan', '0728509697', 'M', '7', '2014-10-04 12:25:18', '1982-12-15', null, null, '2014-07-31 12:53:33', '2015-09-06 14:42:25', null);
INSERT INTO `users` VALUES ('7', '209', '3', '1', null, null, '0', '1', '0', 'Zaharia.Eusebia', 'Damian.Ilinca@gmail.com', '$2y$10$.wjFU.q9x8RjyDJnmG..TuTVfZ0onR351dKmoX79KC5DaG4EyPQT6', 'Julia', 'Trif', 'Oradea', '0238680117', 'M', '8', '2015-08-07 07:56:17', '2009-03-18', null, null, '2014-04-06 03:50:54', '2015-08-31 02:11:01', null);
INSERT INTO `users` VALUES ('8', '97', '4', '2', null, null, '0', '1', '1', 'Carla.Trif', 'Iuliana.Coman@Cornea.com', '$2y$10$6x6aovxyWI0XY8Ixbfl./uDt3RhWVAZN6WZhLSwg01UdhaqtXLIKG', 'Sandu', 'Nastase', 'Deva', '0734625127', 'F', '4', '2014-11-18 13:02:41', '2010-05-27', null, null, '2012-01-12 18:30:18', '2015-08-12 01:38:10', null);
INSERT INTO `users` VALUES ('9', '163', '2', '1', null, null, '0', '0', '1', 'Peter.Andra', 'Hortensia.Teodorescu@Dascalu.com', '$2y$10$PBI/QglvxIZwuftzEKjQk.SBdnsh7mOBoh3FWW/rPdg2Gck6/5TwG', 'Mihai', 'Dogaru', 'Cristuru Secuiesc', '0233421544', 'F', '6', '2014-09-15 16:03:04', '1972-05-29', null, null, '2012-12-23 04:49:29', '2015-08-10 05:18:59', null);
INSERT INTO `users` VALUES ('10', '215', '4', '2', null, null, '1', '0', '0', 'Codrin.Danciu', 'Nechifor29@hotmail.com', '$2y$10$BlAZ.ABXS2Xvyqa.VLCQKOMUIUbXylMuglkGI90VcjYaBapZCpRvO', 'Lelia', 'Zaharia', 'Mun. Deva', '0715274983', 'F', '1', '2014-09-25 22:45:15', '1998-12-31', null, null, '2011-08-03 01:00:47', '2015-09-03 16:24:01', null);
INSERT INTO `users` VALUES ('11', '15', '1', '1', null, null, '0', '1', '0', 'Stanciu.Coralia', 'Mihailescu.Emanoil@gmail.com', '$2y$10$ygP/EVdpbZ1stPLoP7lqBOIoYxCtVDGLdb5Rmg7RQssYOPn27G4CK', 'Cristobal', 'Pasca', 'Vatra Dornei', '0766721513', 'M', '3', '2015-03-19 04:18:37', '1986-11-02', null, null, '2012-03-03 20:52:55', '2015-08-11 19:43:16', null);
INSERT INTO `users` VALUES ('12', '45', '4', '2', null, null, '1', '0', '0', 'Florentin60', 'Crisan.Cristian@hotmail.com', '$2y$10$jiboIB0xAgPHtc8QnMUwGuOo3ThWdo3glHiVXFUGREe.VZqhNMl5S', 'Cristea', 'Oprea', 'Mun. Bistrița', '0731320337', 'F', '9', '2014-12-23 11:28:19', '1979-10-22', null, null, '2014-09-12 02:51:17', '2015-08-09 02:44:52', null);
INSERT INTO `users` VALUES ('13', '114', '3', '1', null, null, '1', '1', '0', 'Istrate.Dumitrana', 'Adina.Matei@Munteanu.com', '$2y$10$j7pBX6hJEOsK3KOxO5qHwexlKoA0fP5TkaX3gKCyGWci1gxqmAw9G', 'David', 'Dumitrescu', 'Rădăuți', '0361975165', 'M', '9', '2015-08-28 13:16:14', '1980-11-14', null, null, '2010-02-28 03:08:12', '2015-08-26 15:08:07', null);
INSERT INTO `users` VALUES ('14', '121', '4', '1', null, null, '0', '0', '1', 'Timotei76', 'Marin44@Ilie.com', '$2y$10$WzLZ5.R7GB4YC6OFn2Di8.MaXgLXCGDvBzsQNoti2FQtNoCGfgqjS', 'Stanca', 'Sava', 'Făget', '0734577482', 'M', '9', '2015-02-03 22:53:52', '1977-12-05', null, null, '2015-05-05 06:55:27', '2015-08-30 11:05:53', null);
INSERT INTO `users` VALUES ('15', '208', '1', '2', null, null, '0', '0', '0', 'aAdam', 'Lili07@Ispas.biz', '$2y$10$bjC3B9KrMzAVQn8WUfGuiu8qOh9C7z9LUKGc3ZpaLB0bd1WIlFKuG', 'Eusebia', 'Mateescu', 'Rădăuți', '0352298115', 'M', '2', '2014-11-26 12:55:17', '1971-09-27', null, null, '2009-10-12 08:28:13', '2015-08-17 11:53:54', null);
INSERT INTO `users` VALUES ('16', '134', '4', '2', null, null, '0', '0', '1', 'Nicodim.Rosca', 'dIon@Moraru.net', '$2y$10$vAYjrlBoPwtBQZmRwI5hVunV/UsG/POLB1hEnTFZaTMw3EOgwy5h.', 'Cornel', 'Nicolescu', 'Cristuru Secuiesc', '0241274513', 'F', '5', '2015-01-11 11:40:05', '2002-09-04', null, null, '2015-07-02 22:18:58', '2015-09-04 12:05:19', null);
INSERT INTO `users` VALUES ('17', '127', '1', '2', null, null, '1', '1', '1', 'Calin.Serban', 'Valentina98@Iosif.biz', '$2y$10$qbjoCZjzuF8GfCnVjLlOPeK9wdJA/Bx.oxY9YTbLsclE0yZvJ207S', 'Ștefana', 'Bodea', 'Codlea', '0364302198', 'M', '5', '2015-08-15 15:02:09', '1996-06-17', null, null, '2012-03-21 06:22:42', '2015-09-05 08:06:27', null);
INSERT INTO `users` VALUES ('18', '207', '4', '1', null, null, '0', '0', '0', 'Saveta.Ivascu', 'Alida.Lupu@Tudose.com', '$2y$10$LlZiHh2DNlRZuLVnJsEmneKzLRQoIKDvMTnA7bavR6KPaamYtm5Fm', 'Inocențiu', 'Stroe', 'Mun. Borsec', '0780532220', 'M', '3', '2015-07-19 19:19:35', '1997-12-14', null, null, '2007-04-14 05:16:03', '2015-08-28 00:46:40', null);
INSERT INTO `users` VALUES ('19', '127', '2', '2', null, null, '0', '0', '1', 'eChis', 'Banica.Filofteia@Peter.com', '$2y$10$iDKjDhop61JMpfZ/x.Ux7u6KMLD1dSX1vHmSdaz0/L0ZDVviR27bK', 'Eduard', 'Gherman', 'Mun. Câmpia Turzii', '0749373193', 'F', '2', '2014-11-27 17:34:07', '1987-11-06', null, null, '2012-09-16 20:47:26', '2015-09-03 09:34:30', null);
INSERT INTO `users` VALUES ('20', '215', '4', '1', null, null, '1', '1', '0', 'Caterina26', 'Simion.Lucian@Luca.net', '$2y$10$41YQC6oeEPSn2iCA5Gao2O0.LUVhMcpDtboxddIbdNCCXQPxmh1eO', 'Dorina', 'Grigorescu', 'Alexandria', '0771196082', 'M', '3', '2015-06-10 23:34:28', '2009-03-19', null, null, '2013-10-27 20:56:18', '2015-08-26 12:17:45', null);
INSERT INTO `users` VALUES ('21', '18', '2', '1', null, null, '1', '1', '1', 'Livia.Simion', 'Geanina41@gmail.com', '$2y$10$7mQK/COIZ6vWbDOtmghxiOtjTxsErj31QNGayevPeXxOkONz0NNlm', 'Estera', 'Rosca', 'Fălticeni', '0344992675', 'M', '7', '2015-06-02 21:57:50', '2004-08-30', null, '7eG87X2JQT4b243YXxDSxf4ud0a5iZ5tN14LXKgbaAwN54rmZVyZYVgNkzUc', '2009-05-09 12:43:15', '2015-09-10 19:43:08', null);
INSERT INTO `users` VALUES ('22', '108', '3', '1', null, null, '1', '1', '1', 'Serban.Smaranda', 'Giorgiana70@yahoo.com', '$2y$10$kBRUYmOIZg02ZLVdDcih8OC8neag74cHEKxYdkzii.Coeh9MGyZpS', 'Fiona', 'Puiu', 'Mun. Vlăhița', '0752815822', 'M', '3', '2014-10-18 14:59:06', '1984-06-24', null, null, '2006-04-15 10:34:53', '2015-08-22 10:37:57', null);
INSERT INTO `users` VALUES ('23', '134', '2', '1', null, null, '0', '0', '0', 'kLungu', 'Lili.Paduraru@Popa.net', '$2y$10$oNDpMaMfTs.f9nXnGuC/2.QxE//oNkalhH5QbEk90EzgSqetIO2Ny', 'Geta', 'Stanescu', 'Mun. Sântana', '0765518100', 'M', '10', '2014-12-13 12:40:43', '1998-10-04', null, null, '2005-09-16 21:17:42', '2015-08-22 19:34:59', null);
INSERT INTO `users` VALUES ('24', '214', '2', '1', null, null, '1', '0', '1', 'Raluca.Neagu', 'Iacob29@Gavrila.com', '$2y$10$zwj1xGGHohBdxBaHmfUf4OC75Gwdybsszdc1vTE3nNMJesqEs6lZ6', 'Teodosia', 'Ivascu', 'Mun. Bumbești-Jiu', '0276283514', 'M', '8', '2015-07-05 03:25:59', '1981-10-04', null, null, '2015-08-22 21:46:47', '2015-09-06 20:53:07', null);
INSERT INTO `users` VALUES ('25', '160', '1', '2', null, null, '0', '1', '0', 'cSoare', 'Musat.Varvara@Dumitriu.net', '$2y$10$exovR.ZhVdabndmIM61CCuKh5SSP97t/axnFyhRNIEOaY5h5NV4gm', 'Luca', 'Militaru', 'Mun. Calafat', '0703331586', 'M', '8', '2015-04-11 11:33:01', '1995-02-26', null, null, '2013-02-28 21:19:15', '2015-08-13 22:07:49', null);
INSERT INTO `users` VALUES ('26', '124', '1', '1', null, null, '0', '0', '0', 'Fabia.Opris', 'Dida.Badea@Micu.biz', '$2y$10$pxCbpyd8F4bPCcFqeyvzRuFR/2rlD4Cpo75hbqu/LGoC5oGIcxVgW', 'Ștefan', 'Stan', 'Sângeorz-Băi', '0369622988', 'F', '8', '2015-05-12 06:16:03', '2013-03-14', null, null, '2008-07-14 19:01:04', '2015-08-10 09:29:22', null);
INSERT INTO `users` VALUES ('27', '206', '4', '2', null, null, '1', '1', '0', 'dTamas', 'Daiana22@Burlacu.info', '$2y$10$eRxH/d4YhrkQZi.xGx1h2uYsRTQTEcxHNnjLxN5D2UHPNs3fZ0ZpG', 'Daiana', 'Sabau', 'Săliștea de Sus', '0715399242', 'F', '6', '2014-11-06 15:51:18', '1971-07-25', null, null, '2014-06-02 22:04:10', '2015-08-30 00:25:56', null);
INSERT INTO `users` VALUES ('28', '225', '1', '1', null, null, '1', '0', '1', 'iDanila', 'mDumitriu@gmail.com', '$2y$10$YEhYhUG76krSuGAgphq6Ke5xx.gC7eD5MXpwvtZrgjoBbTRBptXoe', 'Doru', 'Florea', 'Piatra-Olt', '0751773745', 'M', '4', '2015-01-17 15:54:51', '1974-05-23', null, null, '2010-12-02 15:06:57', '2015-08-22 13:47:57', null);
INSERT INTO `users` VALUES ('29', '16', '3', '2', null, null, '0', '0', '0', 'wStoica', 'Coman.Aida@Pintilie.biz', '$2y$10$UqXxEQ90NguMZNAohDOzleXuHIXwg/a4Pv8aJJhOXA350dktPggtC', 'Olimpia', 'Paduraru', 'Mun. Râmnicu Sărat', '0793162746', 'M', '6', '2014-10-05 15:33:24', '2015-05-31', null, null, '2014-01-14 07:29:47', '2015-08-21 19:06:18', null);
INSERT INTO `users` VALUES ('30', '5', '3', '2', null, null, '0', '1', '0', 'Marcu.Marinescu', 'Dariana84@gmail.com', '$2y$10$zfR8wu7oO8t7xjV3Dx9HluOkfBxGoanBAzFw3u1LEHszuUYz2v0Hm', 'Ariana', 'Nistor', 'Mun. Bocșa', '0252383023', 'F', '6', '2014-12-03 21:37:41', '1977-12-17', null, null, '2009-08-28 16:10:15', '2015-08-13 14:33:14', null);
INSERT INTO `users` VALUES ('31', '121', '3', '2', null, null, '0', '0', '0', 'Dragomir.Carina', 'Mihaila.Nicuta@yahoo.com', '$2y$10$RPKL83CnjYwZUiigwKc60uzkD8JaUiIrqJ2lFT7ejdNJy2lJl.mga', 'Evanghelina', 'Bratu', 'Zimnicea', '0314816813', 'M', '6', '2014-09-28 13:44:43', '1999-08-25', null, null, '2006-08-30 07:28:53', '2015-09-03 18:30:46', null);
INSERT INTO `users` VALUES ('32', '162', '3', '2', null, null, '1', '0', '1', 'Nidia.Matei', 'Lungu.Sorin@Chis.biz', '$2y$10$Xm3XLoDxY5OnKVEDSiqKEuliH.No1LgWyyWdUnAMwfUgD2Bffcrii', 'Paulina', 'Radoi', 'Mun. Sângeorz-Băi', '0780687103', 'M', '5', '2015-03-09 10:18:53', '1994-12-05', null, null, '2005-11-21 11:38:12', '2015-08-11 23:40:56', null);
INSERT INTO `users` VALUES ('33', '230', '3', '1', null, null, '1', '1', '1', 'Marcu13', 'Marian.Georgian@gmail.com', '$2y$10$odc.o4fm0/qAr9b1D3Gzbuuk97L1hMGqz1E8eEoJjRsq0rVRXkre6', 'Aurelia', 'Nechita', 'Mun. Șomcuta Mare', '0720221922', 'F', '4', '2014-11-03 19:49:25', '1985-05-08', null, null, '2006-11-22 05:24:25', '2015-08-16 12:35:53', null);
INSERT INTO `users` VALUES ('34', '74', '1', '2', null, null, '0', '1', '0', 'hPetrea', 'Alexa.Ieremia@gmail.com', '$2y$10$eEfqOIy2P4A5j4w5/lICM.VnKrTVYHeelYMjQ0ObZj0Qyy.pPKA6O', 'Ducu', 'Marcu', 'Mun. Bicaz', '0739997848', 'M', '2', '2014-12-10 08:15:06', '2007-06-01', null, null, '2014-04-01 21:30:42', '2015-08-10 08:19:58', null);
INSERT INTO `users` VALUES ('35', '13', '3', '2', null, null, '0', '1', '0', 'Madalin.Chivu', 'vMihailescu@Suciu.com', '$2y$10$8FO.xV.DHFat6VK1xc/ocu0rth/Lv3C5KZINCtUtUwYymdDK/wlOO', 'Zenobia', 'Nicolescu', 'Abrud', '0706886314', 'F', '5', '2015-02-05 06:14:20', '1978-06-17', null, null, '2014-01-22 02:34:48', '2015-09-06 19:29:16', null);
INSERT INTO `users` VALUES ('36', '184', '3', '1', null, null, '0', '0', '1', 'Larisa.Calin', 'Alin.Draghici@gmail.com', '$2y$10$N1jAxrs4w9a15l1/5CFDHuNHVXZxEbUyUZFmPAhHgOjPo1f3hidGm', 'Arsenie', 'Mirea', 'Hunedoara', '0709637955', 'F', '9', '2015-01-18 03:22:47', '1978-02-28', null, null, '2011-06-18 08:16:44', '2015-08-10 23:25:28', null);
INSERT INTO `users` VALUES ('37', '136', '1', '2', null, null, '0', '0', '0', 'Zamfir59', 'Burlacu.Octaviana@gmail.com', '$2y$10$DFpSezZ3cDuWIZ.0/oy1FOo6fMw5CvPKpT2T1rfiMUId30yohlWIS', 'Robert', 'Mirea', 'Mun. Mioveni', '0758180383', 'M', '6', '2015-06-07 16:56:05', '2005-09-04', null, null, '2008-12-28 22:01:16', '2015-08-14 17:06:53', null);
INSERT INTO `users` VALUES ('38', '152', '3', '1', null, null, '0', '1', '1', 'Anghel.Adelin', 'mIrimia@hotmail.com', '$2y$10$//NQdeHJH9JVmefHkBNzqucVLmLs3asv.X0DoKNwMUGvk2tVLrgsu', 'Lucrețiu', 'Militaru', 'Mun. Bocșa', '0784451128', 'F', '3', '2015-07-29 16:26:41', '2014-08-30', null, null, '2014-08-12 08:43:47', '2015-08-28 22:46:45', null);
INSERT INTO `users` VALUES ('39', '185', '1', '2', null, null, '1', '0', '1', 'jPetre', 'iMartin@Nita.com', '$2y$10$/8geHdN8lxh058YhZHY8X.4OOHElPq7eRurjTgRhMTlUnYz4ViR16', 'Ludovica', 'Nagy', 'Mun. Constanța', '0783670493', 'F', '7', '2015-05-20 05:42:23', '1980-05-11', null, null, '2005-09-25 19:10:00', '2015-09-04 17:46:42', null);
INSERT INTO `users` VALUES ('40', '213', '1', '2', null, null, '0', '1', '1', 'Iliescu.Cristian', 'gAlbu@Marin.org', '$2y$10$keimRqmXFmRgbm6S2v960.Yhv4uRkeU8rnArOLWeG67rtbmeDNpFG', 'Isabela', 'Dima', 'Mun. Murfatlar', '0234248670', 'F', '1', '2015-03-27 07:46:46', '1977-08-17', null, null, '2011-09-14 20:37:14', '2015-08-19 08:25:18', null);
INSERT INTO `users` VALUES ('41', '236', '1', '1', null, null, '0', '1', '1', 'Aurel86', 'Silviu.Manea@yahoo.com', '$2y$10$lwVY1y.Cv1UVsGVnExuwA.5M0QhAm9/4VOsxLvfJxr0Tn1SaSELem', 'Daniel', 'Gherman', 'Slatina', '0796407179', 'F', '9', '2015-07-15 11:08:09', '1975-03-25', null, null, '2014-01-27 12:48:38', '2015-09-07 22:48:10', null);
INSERT INTO `users` VALUES ('42', '49', '3', '1', null, null, '0', '0', '0', 'Patrascu.Georgel', 'Alexandrina48@Fratila.com', '$2y$10$eEhrak6mta9YMnyqKngVaud8QPABw6OMAlr2T3FJ6W9ehPFmEPhc6', 'Radu', 'Stanescu', 'Roman', '0260561814', 'M', '8', '2015-02-11 18:37:42', '2001-06-13', null, null, '2013-04-22 13:35:50', '2015-08-24 05:47:54', null);
INSERT INTO `users` VALUES ('43', '236', '1', '2', null, null, '1', '1', '1', 'Chiriac.Georgeta', 'Roman.Bogdan@Cristescu.com', '$2y$10$QMqkA2pP0hOqtwdob1BqZ.RKrjbPltLtW.tvDtg482tJTS5GCgivO', 'Ioan', 'Mihai', 'Mizil', '0798349185', 'F', '2', '2014-12-22 04:36:17', '1987-10-27', null, null, '2012-05-30 18:06:37', '2015-08-15 06:17:36', null);
INSERT INTO `users` VALUES ('44', '221', '3', '2', null, null, '1', '0', '1', 'Crin85', 'qMoldovan@gmail.com', '$2y$10$Y0WUN0uG1FbHspeQnnJKVeh2fLezhnUl5J1CMxH0jkLNB.QsYLIi.', 'Fabiana', 'Ciobanu', 'Mun. Fundulea', '0266846641', 'F', '4', '2015-08-28 07:15:59', '2003-05-04', null, null, '2010-07-18 04:05:24', '2015-08-17 15:34:58', null);
INSERT INTO `users` VALUES ('45', '114', '1', '1', null, null, '0', '0', '1', 'mNicoara', 'Arian08@yahoo.com', '$2y$10$bp4XennQPnKfOrmHZf1d5uyJNZ8dX.jWc8PKSkQAdFuunJsXP5bCe', 'Angel', 'Nicolae', 'Mun. Strehaia', '0254694728', 'F', '9', '2015-05-05 23:50:45', '2003-10-30', null, null, '2007-12-20 04:49:07', '2015-08-25 09:00:30', null);
INSERT INTO `users` VALUES ('46', '71', '2', '1', null, null, '0', '0', '0', 'Stefania.Puiu', 'Leontina74@yahoo.com', '$2y$10$K5fx...7sFY4q7Jg3BcIuO.vNykGnIwc3Azj1//Vn9Fokf8/04MYy', 'Dorel', 'Damian', 'Borsec', '0794781754', 'F', '9', '2015-05-03 04:10:43', '1974-01-03', null, null, '2013-10-09 22:09:38', '2015-08-24 04:37:38', null);
INSERT INTO `users` VALUES ('47', '91', '2', '1', null, null, '1', '1', '0', 'tSzasz', 'Valentin.Nita@yahoo.com', '$2y$10$NlllDW/FJXJDyy3sIqHqNukQduLdqJw.LKztgWabdyUuuv4uYec6K', 'Beniamin', 'Biro', 'Mun. Bechet', '0330135319', 'M', '7', '2015-01-31 03:13:07', '1993-10-12', null, null, '2009-12-09 13:56:22', '2015-08-29 20:45:54', null);
INSERT INTO `users` VALUES ('48', '54', '1', '2', null, null, '0', '0', '1', 'Norman.Filimon', 'Ciprian68@Gheorghe.com', '$2y$10$2ZX5cORJgHa.ErRI2ppsAOwLDkWnTcwZPoRv8NJ2POxoVDfwLHkxi', 'Cerasela', 'Savu', 'Deva', '0791181921', 'M', '7', '2014-12-14 23:39:34', '1972-03-26', null, null, '2010-03-29 07:01:34', '2015-09-01 18:26:55', null);
INSERT INTO `users` VALUES ('49', '13', '1', '2', null, null, '0', '1', '0', 'Adonis.Savu', 'Radoi.Petronela@Muresan.com', '$2y$10$EH9UE0e5DwEqmiwc6INoaOEx1.VmyJdR3pVk3UxX.x8Lftoqn1gcC', 'Antim', 'Gheorghita', 'Mun. Tulcea', '0344793496', 'M', '9', '2014-09-24 12:32:42', '1992-07-26', null, null, '2007-01-21 13:05:07', '2015-09-06 04:05:12', null);
INSERT INTO `users` VALUES ('50', '43', '2', '2', null, null, '0', '0', '0', 'Olimpia.Diaconu', 'Ciocan.Florin@Sabau.com', '$2y$10$4JYgYchftIkqcztifi95H.sVMYseUIpiflolJZu6qZNOWp8VCl5Ri', 'Gicu', 'Munteanu', 'Babadag', '0724517787', 'M', '6', '2015-07-25 14:04:52', '1997-09-28', null, null, '2014-01-14 17:15:54', '2015-09-02 20:14:48', null);

-- ----------------------------
-- Table structure for vimeos
-- ----------------------------
DROP TABLE IF EXISTS `vimeos`;
CREATE TABLE `vimeos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `vimeos_user_id_index` (`user_id`),
  CONSTRAINT `vimeos_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of vimeos
-- ----------------------------
INSERT INTO `vimeos` VALUES ('1', '37', '96790902', '2015-08-28 21:23:09', '2015-08-23 04:44:59');
INSERT INTO `vimeos` VALUES ('2', '34', '96790902', '2015-08-22 04:14:30', '2015-09-08 04:20:54');
INSERT INTO `vimeos` VALUES ('3', '48', '110526130', '2015-08-14 13:34:46', '2015-08-30 02:54:56');
INSERT INTO `vimeos` VALUES ('4', '15', '44770592', '2015-08-31 01:51:09', '2015-08-11 04:38:02');
INSERT INTO `vimeos` VALUES ('5', '9', '65737096', '2015-09-07 12:43:55', '2015-08-23 09:38:39');
INSERT INTO `vimeos` VALUES ('6', '45', '65737096', '2015-09-07 19:41:31', '2015-08-21 01:50:00');
INSERT INTO `vimeos` VALUES ('7', '3', '65737096', '2015-08-12 07:42:17', '2015-08-22 07:35:19');
INSERT INTO `vimeos` VALUES ('8', '14', '65737096', '2015-09-02 03:39:55', '2015-08-14 12:00:08');
INSERT INTO `vimeos` VALUES ('9', '22', '59813619', '2015-08-17 14:27:30', '2015-08-25 19:15:01');
INSERT INTO `vimeos` VALUES ('10', '7', '96790902', '2015-08-13 14:08:33', '2015-09-02 04:23:08');
INSERT INTO `vimeos` VALUES ('11', '31', '96790902', '2015-08-25 19:11:59', '2015-08-27 23:31:35');
INSERT INTO `vimeos` VALUES ('12', '14', '101911461', '2015-09-04 17:12:37', '2015-08-09 12:55:53');
INSERT INTO `vimeos` VALUES ('13', '9', '110526130', '2015-08-20 22:43:31', '2015-08-18 02:53:23');
INSERT INTO `vimeos` VALUES ('14', '45', '65737096', '2015-08-10 20:50:12', '2015-08-31 06:32:33');
INSERT INTO `vimeos` VALUES ('15', '36', '65737096', '2015-08-21 12:27:06', '2015-09-04 02:53:03');
INSERT INTO `vimeos` VALUES ('16', '33', '101911461', '2015-09-06 01:47:21', '2015-09-02 11:26:51');
INSERT INTO `vimeos` VALUES ('17', '31', '110526130', '2015-08-13 12:12:44', '2015-08-11 18:49:03');
INSERT INTO `vimeos` VALUES ('18', '18', '101911461', '2015-09-01 07:12:28', '2015-08-10 05:38:42');
INSERT INTO `vimeos` VALUES ('19', '13', '65737096', '2015-08-14 06:28:37', '2015-08-23 04:31:57');
INSERT INTO `vimeos` VALUES ('20', '3', '44770592', '2015-08-18 09:10:50', '2015-08-13 20:12:18');
INSERT INTO `vimeos` VALUES ('21', '50', '110526130', '2015-08-24 22:39:45', '2015-08-15 22:05:32');
INSERT INTO `vimeos` VALUES ('22', '42', '78062622', '2015-08-26 03:22:33', '2015-08-26 11:01:22');
INSERT INTO `vimeos` VALUES ('23', '18', '59813619', '2015-08-21 01:31:56', '2015-09-08 10:09:37');
INSERT INTO `vimeos` VALUES ('24', '34', '96862989', '2015-08-30 11:51:11', '2015-08-25 23:56:04');
INSERT INTO `vimeos` VALUES ('25', '20', '65737096', '2015-08-30 20:50:15', '2015-08-28 02:18:01');
INSERT INTO `vimeos` VALUES ('26', '21', '96862989', '2015-08-11 19:21:39', '2015-08-17 16:45:55');
INSERT INTO `vimeos` VALUES ('27', '21', '110526130', '2015-09-02 01:01:12', '2015-08-28 10:11:08');
INSERT INTO `vimeos` VALUES ('28', '5', '96790902', '2015-08-15 20:38:55', '2015-08-15 14:53:48');
INSERT INTO `vimeos` VALUES ('29', '25', '59813619', '2015-08-23 10:11:08', '2015-08-27 07:56:19');
INSERT INTO `vimeos` VALUES ('30', '42', '44770592', '2015-08-28 02:36:02', '2015-09-08 13:11:41');
INSERT INTO `vimeos` VALUES ('31', '3', '110526130', '2015-08-26 06:25:29', '2015-09-01 12:54:45');
INSERT INTO `vimeos` VALUES ('32', '31', '65737096', '2015-09-04 00:39:14', '2015-08-27 02:01:19');
INSERT INTO `vimeos` VALUES ('33', '36', '101911461', '2015-08-24 20:57:37', '2015-08-25 04:38:03');
INSERT INTO `vimeos` VALUES ('34', '9', '96862989', '2015-08-31 15:06:46', '2015-08-09 09:38:34');
INSERT INTO `vimeos` VALUES ('35', '50', '96862989', '2015-08-13 10:32:48', '2015-08-21 23:31:51');
INSERT INTO `vimeos` VALUES ('36', '49', '110526130', '2015-08-27 07:15:21', '2015-08-12 01:37:57');
INSERT INTO `vimeos` VALUES ('37', '20', '44770592', '2015-09-08 03:07:10', '2015-08-09 14:39:43');
INSERT INTO `vimeos` VALUES ('38', '40', '96862989', '2015-09-08 00:57:08', '2015-08-23 00:46:05');
INSERT INTO `vimeos` VALUES ('39', '5', '59813619', '2015-09-03 05:32:12', '2015-08-29 11:36:15');
INSERT INTO `vimeos` VALUES ('40', '35', '65737096', '2015-09-03 07:20:04', '2015-08-29 11:20:45');
INSERT INTO `vimeos` VALUES ('41', '17', '78062622', '2015-09-01 05:36:11', '2015-09-07 05:05:24');
INSERT INTO `vimeos` VALUES ('42', '6', '96862989', '2015-08-27 06:24:46', '2015-08-26 23:15:13');
INSERT INTO `vimeos` VALUES ('43', '18', '96862989', '2015-08-31 10:17:21', '2015-08-23 00:39:06');
INSERT INTO `vimeos` VALUES ('44', '19', '96790902', '2015-08-26 14:06:47', '2015-08-09 17:13:05');
INSERT INTO `vimeos` VALUES ('45', '32', '44770592', '2015-08-24 23:08:08', '2015-08-12 06:34:24');
INSERT INTO `vimeos` VALUES ('46', '19', '78062622', '2015-08-22 10:19:10', '2015-08-15 03:11:33');
INSERT INTO `vimeos` VALUES ('47', '18', '78062622', '2015-08-10 01:13:37', '2015-08-09 04:47:55');
INSERT INTO `vimeos` VALUES ('48', '42', '101911461', '2015-08-09 10:21:17', '2015-08-28 22:05:04');
INSERT INTO `vimeos` VALUES ('49', '18', '65737096', '2015-08-11 01:25:30', '2015-09-06 05:13:39');
INSERT INTO `vimeos` VALUES ('50', '34', '65737096', '2015-09-05 02:13:43', '2015-08-16 20:55:56');

-- ----------------------------
-- Table structure for voices
-- ----------------------------
DROP TABLE IF EXISTS `voices`;
CREATE TABLE `voices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `ethnicity_id` int(10) unsigned NOT NULL,
  `accent` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `spoken_languages` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `voice_style` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `other_accents` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `profesional_studies` tinyint(1) DEFAULT NULL,
  `speciality_studies` tinyint(1) DEFAULT NULL,
  `speciality_year` smallint(5) unsigned DEFAULT NULL,
  `speciality_institution` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `faculty` tinyint(1) DEFAULT NULL,
  `faculty_student` tinyint(1) DEFAULT NULL,
  `faculty_year` smallint(5) unsigned DEFAULT NULL,
  `faculty_institution` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `faculty_domain` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `equipment` text COLLATE utf8_unicode_ci,
  `info` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `voices_user_id_index` (`user_id`),
  KEY `voices_ethnicity_id_index` (`ethnicity_id`),
  CONSTRAINT `voices_ethnicity_id_foreign` FOREIGN KEY (`ethnicity_id`) REFERENCES `ethnicities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `voices_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of voices
-- ----------------------------
INSERT INTO `voices` VALUES ('1', '5', '5', 'Moldovean', 'Engleza', 'Prietenos, Rapid, senzual', 'fara accent', '0', '1', '1981', 'UNATC I.L. Caragiale sectia Arta Actorului', '1', '1', '1981', 'Bucuresti - Universitatea Hyperion - Facultatea de Arte', 'filologie intensiv limba engleza', 'Casti S5', 'Iure fuga distinctio deserunt non voluptate. Fugiat amet illo vero ab accusantium velit. Adipisci exercitationem voluptatibus amet ut omnis quis. Et quo corporis ab deserunt. Dolores est sit consequatur nisi omnis. Molestiae blanditiis vel ipsum consequatur. Neque et non qui consectetur tempore non. Quia est adipisci a molestiae. Perspiciatis inventore quia ad sequi quae. Est aut cupiditate enim in sed molestias voluptatem. Inventore aut veritatis totam. Neque cum aliquid eos sit est aliquam earum. Laudantium eaque in nam soluta neque voluptatem.', '2015-09-08 13:25:39', '2015-08-11 23:42:47');
INSERT INTO `voices` VALUES ('2', '18', '165', 'Nu', 'Franceza', 'Mezzosoprana, joasa, grava', 'Moldovean', '0', '1', '2013', 'Colegiul National de Arte', '1', '0', '1994', 'Cluj -Universitatea Babes-Bolyai - Facultatea de Film si TV', 'Cinema and Television', 'Nu am, pot apela oricand la cateva studiouri profesionale sau studiouri de radio', 'Velit repellat sunt accusamus cumque tempora et. Velit dignissimos quidem aut delectus culpa reprehenderit recusandae ratione. Dolorum sed soluta repellendus unde. Sint nostrum quibusdam quia. Dolor voluptatem voluptatem maiores expedita fuga. Earum sit pariatur molestiae nam molestiae. Voluptatem deserunt quia dolores est molestiae et. Laborum aliquid consequatur et. Veniam vel qui tenetur voluptas aut ullam enim quae. Ad sed iure numquam unde. Numquam ut sed nesciunt debitis aut et.', '2015-08-31 20:26:54', '2015-08-18 07:56:59');
INSERT INTO `voices` VALUES ('3', '4', '145', 'Romana', 'Engleza', 'Prietenos, Rapid, senzual', 'Moldovean', '1', '1', '2013', 'Mandragora Movies Film Academy', '1', '0', '1984', 'Bucuresti - Universitatea Hyperion - Facultatea de Arte', 'Facultatea de Teatru, Specializarea Papusi si Marionete', 'Adobe Audition', 'Ipsam qui cupiditate at quia sint qui. Ipsum vitae iure natus accusantium alias quos laboriosam reprehenderit. Omnis necessitatibus dolor quo tenetur id totam. Et quia minima molestiae molestiae et aut. Vel explicabo sint et odio esse quam. Quibusdam dolorem voluptate aut rerum sed. Enim cum qui nobis non aut. Unde dolore modi impedit quia. Corrupti ipsa iusto in ut. Cupiditate eum reprehenderit quia voluptatum est. Consequatur qui facere animi quo ad et. Consequatur voluptatem quas molestiae in.', '2015-08-23 12:57:02', '2015-09-01 15:14:29');
INSERT INTO `voices` VALUES ('4', '15', '54', 'Romana', 'Engleza incepator, Franceza avansat si Spaniola mediu', 'Stiri, documentar, reclame', 'Nu', '1', '1', '2001', 'Ivana Chubbuck-Mandragora Movies Acting Studio', '0', '0', '2004', 'Facultatea de Informatica', 'IT', 'Adobe Audition', 'Dolor et voluptas minima quam deserunt. Sit voluptatem accusamus tempore ipsum quia. Eius earum ratione est laborum est provident. Quod commodi dolorem eius nemo unde neque. Quasi minima dolorem dolor et aut in. Sit eum molestiae error quo est aut rerum ullam. Ut doloribus ea voluptas repudiandae incidunt corrupti. Dignissimos laborum possimus quia ea consequuntur. Facere illum perspiciatis minima dolores. Nihil sint sit sequi vero sit. Debitis qui esse veritatis assumenda iusto aut.', '2015-09-04 18:36:23', '2015-08-21 14:35:18');
INSERT INTO `voices` VALUES ('5', '30', '99', 'Romana', 'Franceza', 'Contemporary Hit Radio', 'fara accent', '1', '1', '1994', 'UNATC I.L. Caragiale sectia Arta Actorului', '1', '1', '1994', 'Cluj -Universitatea Babes-Bolyai - Facultatea de Film si TV', 'IT', 'Nu am, pot apela oricand la cateva studiouri profesionale sau studiouri de radio', 'Vitae facilis nesciunt quos repudiandae non pariatur fuga et. Repudiandae nisi eius blanditiis quam. Illo molestias distinctio et libero sit quasi. Rem officia occaecati et corporis consequatur sequi vel. A consequuntur aspernatur iusto in a eaque quia. Voluptatem assumenda tempora eum fugit. Saepe fuga et rerum rerum. Molestias architecto sit officia. Minima officiis natus mollitia illum consectetur qui. Quasi quo voluptatem consequatur molestiae. Et dignissimos consectetur officia odio ea. Dolorem aut enim et repudiandae odit optio. Qui vitae natus est accusantium. Saepe blanditiis non alias.', '2015-08-21 18:18:46', '2015-08-23 22:41:48');
INSERT INTO `voices` VALUES ('6', '5', '128', 'Oltean', 'Italiana', 'POVESTE , sexi, romantica', 'Moldovean', '1', '1', '1980', 'Spiru Haret Bucuresti', '0', '1', '2003', 'Bucuresti - Universitatea Hyperion - Facultatea de Arte', 'filologie intensiv limba engleza', 'Casti S5', 'Aut quae quidem sequi atque ut. Voluptate qui repudiandae aut voluptatem saepe ut commodi quae. Deserunt ab illo cupiditate officia quos non modi. Rerum omnis aperiam sunt. Voluptatem dicta molestias facilis commodi. Vel ut quae quam mollitia perspiciatis omnis incidunt exercitationem. Dicta nesciunt laborum velit et in voluptatibus odit. Sed et fuga vel aut odit et. Est dolor repudiandae vitae aut fugit. Ratione ut consequuntur qui voluptatibus odio culpa sunt qui. Voluptatem est ut exercitationem sint. Nesciunt nostrum omnis dolores deleniti necessitatibus omnis. Voluptatem et quisquam dolores similique accusamus delectus rem.', '2015-08-24 16:41:54', '2015-08-29 19:54:36');
INSERT INTO `voices` VALUES ('7', '23', '161', 'Oltean', 'Engleza', 'Contemporary Hit Radio', 'Oltean', '0', '1', '2013', 'Universitatea Nationala de Arta Teatrala si Cinematografica Ion Luca Caragiale Bucuresti', '1', '0', '2008', 'Bucuresti - Universitatea Nationala de Arta Teatrala si Cinematografica “I.L.Caragiale” (UNATC)', 'filologie intensiv limba engleza', 'Nu am', 'Earum expedita et aut est. Ex aut non ipsa dicta rem. Et enim ut minima dolores neque et. Ut excepturi repellendus veniam possimus officia est neque ut. Id adipisci ipsa rerum et et doloribus ut. Voluptatibus assumenda quia repellat pariatur voluptas sit odio quas. Consequatur in ea omnis illo eum neque totam. Id in facere atque quis. Et error eos iusto reiciendis sit fugiat necessitatibus. Et ducimus dolore officia fugiat a quibusdam. Provident et dolores iure qui fuga necessitatibus. Natus voluptatem earum saepe qui id minus soluta. Quis excepturi exercitationem distinctio tempora aliquid tempora. Nam aut sed aut quam ipsa.', '2015-09-06 23:31:44', '2015-08-10 15:18:02');
INSERT INTO `voices` VALUES ('8', '41', '91', 'Oltean', 'Engleza', 'Inspaimantatoare, serioase, voce groasa', 'Oltean', '1', '0', '1991', 'UNATC I.L. Caragiale sectia Arta Actorului', '1', '0', '1999', 'Bucuresti - Universitatea Hyperion - Facultatea de Arte', 'filologie intensiv limba engleza', 'Casti S5', 'Sit consequatur nobis dolor et. Impedit odit laboriosam ut ipsa perspiciatis. Aut nostrum assumenda vitae recusandae rerum alias animi. Nostrum molestiae soluta quaerat vitae. Rerum amet sapiente a totam neque. Illo impedit reprehenderit veniam reiciendis sit. Quia tempora eum expedita et officiis qui. Dolorem dignissimos distinctio voluptatem qui adipisci. Voluptatem hic ut commodi natus eligendi. Aliquam sint ea natus magni. Ea consequuntur inventore aut aut.', '2015-08-25 00:27:22', '2015-08-19 08:02:50');
INSERT INTO `voices` VALUES ('9', '10', '138', 'Romana', 'Engleza', 'Mezzosoprana, joasa, grava', 'Moldovean', '0', '0', '1995', 'UNATC I.L. Caragiale sectia Arta Actorului', '0', '1', '1995', 'Facultatea de Informatica', 'filologie intensiv limba engleza', 'Mini studio', 'Eaque doloribus qui sunt aut a. Et dolore est deserunt earum expedita eos dolore. Quis fugit a et quo. Sunt deserunt nesciunt eaque repellat sed. Omnis voluptatibus neque voluptatum at. Quidem exercitationem voluptate unde aliquam est sapiente tempora. Ut sit enim dolorem et. Eaque non incidunt deleniti voluptate saepe aut officia.', '2015-09-03 17:10:14', '2015-08-09 18:59:46');
INSERT INTO `voices` VALUES ('10', '31', '90', 'Nu am', 'Engleza incepator, Franceza avansat si Spaniola mediu', 'Mezzosoprana, joasa, grava', 'Oltean', '0', '1', '1980', 'Spiru Haret Bucuresti', '1', '0', '1990', 'Cluj -Universitatea Babes-Bolyai - Facultatea de Film si TV', 'IT', 'Mini studio', 'Voluptas autem eaque fugit aliquid quis. Quia sunt consectetur odio et numquam. Accusantium ut modi similique officia officiis qui totam et. Reiciendis reprehenderit porro dolorem in. Tenetur cumque repellat tempora quia ratione provident quasi fugit. Laudantium ut sed facilis illo laborum quidem tempore qui. Exercitationem deserunt nihil pariatur numquam et odit.', '2015-09-06 11:52:22', '2015-08-13 02:33:13');

-- ----------------------------
-- Table structure for workshops
-- ----------------------------
DROP TABLE IF EXISTS `workshops`;
CREATE TABLE `workshops` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `award` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `year` smallint(5) unsigned DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `workshops_user_id_index` (`user_id`),
  CONSTRAINT `workshops_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of workshops
-- ----------------------------
INSERT INTO `workshops` VALUES ('1', '20', 'Enim quo nihil blanditiis minus.', 'Dicta aperiam iusto sequi est ea. Deleniti modi neque ut et.', '1988', 'Sit sit enim non voluptatem. Et assumenda blanditiis veniam unde vel. Autem debitis tenetur neque tempore perspiciatis. Fugit numquam velit vel velit quam ipsam. Non repellendus hic veniam illo ipsa. Sit reprehenderit itaque non et odio illo. Cumque consequuntur et ut saepe ut quia ratione.', '2015-08-14 11:59:59', '2015-08-28 13:35:56');
INSERT INTO `workshops` VALUES ('2', '35', 'Magnam quis provident consectetur labore.', 'Molestiae recusandae dolor fugit tempora voluptatem dignissimos. Veniam voluptatibus voluptas inventore. Assumenda voluptate repellendus aut maxime et dolore.', '2015', 'Fugit non deleniti sint cupiditate voluptatem. Nam sint quia eum ut. Corporis est earum inventore consectetur illo deserunt. Amet aut sint harum accusamus sit et. Dolores ipsa adipisci molestias sed nihil. Enim aliquid exercitationem temporibus eos quo aut. Eos sapiente enim cupiditate enim hic voluptas consectetur accusantium. Dolores pariatur soluta nihil nihil. Aut ullam ut hic labore commodi libero.', '2015-08-22 15:54:49', '2015-08-14 05:11:19');
INSERT INTO `workshops` VALUES ('3', '19', 'Mollitia voluptate ad omnis.', 'Illum deleniti facilis sunt aliquam. Consequatur facilis doloremque qui voluptatibus eaque provident et. Laborum perferendis architecto quasi qui voluptate iusto in sint.', '2016', 'Minima blanditiis impedit dicta sit est quos. Incidunt ipsum ut voluptatibus saepe voluptatem. Rerum exercitationem incidunt rerum repellat. Saepe nihil veniam tempora. Dolorum aut minus mollitia nesciunt sunt. Perferendis qui dolorem occaecati velit. Architecto aut soluta officia qui eum qui et. Reprehenderit cupiditate expedita adipisci in vero saepe alias. Qui omnis commodi accusantium. Esse amet esse deserunt inventore ut. Exercitationem consectetur rerum assumenda consequatur suscipit culpa.', '2015-08-13 14:01:46', '2015-08-26 17:06:32');
INSERT INTO `workshops` VALUES ('4', '11', 'Nisi debitis est minima est.', 'Tenetur fugit voluptate iste odit et quia. At eveniet molestiae sunt qui cum ratione laborum.', '1993', 'Molestias in officia debitis sed eum rerum. Et voluptas perspiciatis quo quam. Eos qui facilis maxime vel omnis quis id. Delectus saepe et et autem occaecati earum id. In aut eum sit perspiciatis eos aut est iure. Facilis est nulla voluptas fugit. Et id autem aliquam rem eos deserunt quis. Id earum non voluptate perferendis rerum culpa. Maxime libero commodi consequuntur. Ducimus ut perferendis labore rerum rerum.', '2015-08-30 00:11:03', '2015-08-17 09:59:38');
INSERT INTO `workshops` VALUES ('5', '44', 'Exercitationem eius illo optio.', 'Dolor error architecto saepe pariatur repellat quia. Facere est corrupti provident quia rem.', '2011', 'Aut aut aspernatur numquam sunt dolor odio et. Nesciunt accusamus repudiandae deserunt enim perspiciatis fugiat sit. Ut qui optio est ut sunt similique impedit nisi. Consequatur eos culpa quo impedit et sed. Quas commodi quaerat sunt sint et qui. Aut deserunt et sed tempora. Vel voluptatem modi eius tempore. A et voluptatem totam iste hic deleniti. Libero sed incidunt omnis eos dolorum. Unde sapiente voluptatibus molestias earum aut totam velit. Quis veritatis ipsum laborum magni. Recusandae ullam ad a delectus.', '2015-09-01 18:36:50', '2015-08-09 22:40:23');
INSERT INTO `workshops` VALUES ('6', '45', 'Eos sit natus provident laboriosam.', 'Optio ad possimus dolore quo at veniam perferendis libero. Et aut natus sed et totam.', '2013', 'Dolore nemo tempore consequatur et. Ipsum exercitationem debitis eligendi qui dolorum sequi nobis. Molestias quo a autem exercitationem ab exercitationem. Sit ad corporis dolor consequuntur qui neque. Accusamus nulla officia deleniti consequatur. Impedit aut numquam voluptatem. Rem possimus provident consequatur qui. Expedita sequi veniam laudantium magni exercitationem molestias libero quaerat. Veritatis aspernatur nulla itaque non voluptatem. Aut iure unde ab nemo libero debitis.', '2015-08-26 15:27:10', '2015-08-15 14:34:32');
INSERT INTO `workshops` VALUES ('7', '17', 'Et et sapiente inventore iure.', 'Vitae impedit fugit aut. Dolorum rerum libero et quia placeat consequatur.', '2012', 'Amet ea adipisci ut error. Vel recusandae illo aut officiis. Accusantium dolor omnis magni sed dolor ut nam. Magni porro omnis omnis dolore qui ipsum nulla voluptatem. Saepe necessitatibus numquam aspernatur blanditiis vel nisi et. Architecto corrupti error odio quisquam maiores dolorem. Ut ut aliquid praesentium vero qui quidem accusamus dolor. Reprehenderit ullam quibusdam a ullam doloribus. Ipsa facere voluptas ut. Laudantium molestiae et adipisci aliquam. Aut fugit tenetur maiores fuga sunt mollitia fugiat. Itaque corporis atque similique qui rem. Dolores odit aspernatur eaque id.', '2015-09-01 03:50:54', '2015-08-12 01:47:57');
INSERT INTO `workshops` VALUES ('8', '50', 'Qui ut autem et eligendi.', 'Blanditiis quidem cupiditate sapiente consectetur. Sunt recusandae sint rerum quos.', '2000', 'Id tempora sit aspernatur et. Ut ipsa atque sunt dolores. Omnis iste enim et labore laudantium quam. Beatae eos modi porro labore dolorum nulla ullam. Velit molestiae ut voluptatibus consectetur. Impedit expedita facilis incidunt et molestiae quaerat quis facere. Enim vel ipsam id dolorem magnam debitis. Minima omnis iste dignissimos mollitia et qui in sed. Minima dicta nulla consectetur earum illum fugiat. Ut dicta occaecati et est.', '2015-08-15 15:19:01', '2015-08-31 02:32:47');
INSERT INTO `workshops` VALUES ('9', '19', 'Ea aut voluptate.', 'Non quia qui cumque. Quas nihil similique fugiat veritatis voluptas. Error molestiae sed sed voluptatem in sequi.', '2009', 'Voluptatibus sunt et modi dolorem explicabo ducimus impedit. Excepturi cum voluptatem est eligendi possimus necessitatibus. Laboriosam quia vitae delectus voluptas dolorem. Libero ducimus quam molestias sint sit ex accusantium. Modi esse voluptatibus ab sunt praesentium. Tempora a voluptatem et assumenda reprehenderit pariatur repellat. Rerum eius sequi ad. Incidunt iusto dolorum nobis mollitia tenetur nam ab. A veniam sit accusamus et accusantium. Et magnam hic sunt rerum beatae odio molestiae et. Adipisci ut magni molestiae sit. Id necessitatibus sit expedita porro aut.', '2015-09-05 13:01:15', '2015-09-07 03:25:03');
INSERT INTO `workshops` VALUES ('10', '37', 'Neque consequatur ipsam in dolore.', 'Sunt nesciunt suscipit rerum tempora aut neque. Porro et aut nisi error rerum omnis velit quae.', '2010', 'Sint dolorem sapiente eum laborum enim. Necessitatibus ut amet voluptatem quo est beatae quidem. In non animi aut quo. Sit veniam velit repellendus molestiae. Voluptas eum officiis necessitatibus ipsum officiis perferendis quaerat. In nobis deleniti vel sapiente laboriosam culpa et iusto. Sed quisquam qui velit et rerum non. Possimus veritatis impedit sit impedit. Aut ea et quae id unde.', '2015-08-31 14:18:35', '2015-08-11 19:07:34');
INSERT INTO `workshops` VALUES ('11', '8', 'Illo ut aut et iure dolorum.', 'Dicta ipsa ullam necessitatibus. Omnis voluptatem fuga eveniet est provident asperiores non. Nihil impedit iste harum dicta dolor ab saepe recusandae.', '2001', 'Velit delectus magnam cum inventore incidunt. Autem voluptate in sint ratione molestias voluptas excepturi dolores. Distinctio necessitatibus qui iste consequatur et. Magni rerum corrupti qui. Unde perferendis similique quam praesentium soluta nulla voluptatem error. Ea delectus enim aliquid est. Explicabo nulla deserunt totam beatae totam. Nihil dignissimos deleniti nemo voluptas qui impedit. Minima eaque qui laboriosam nostrum eligendi. Distinctio laudantium nemo corrupti. Ea vel dolorem soluta dignissimos velit.', '2015-08-31 13:16:54', '2015-08-30 23:12:07');
INSERT INTO `workshops` VALUES ('12', '17', 'Architecto quasi temporibus consequatur.', 'Et et ipsa et qui impedit id. Aut facere reprehenderit praesentium eum sed. Rerum mollitia et eaque nobis sit esse sed corrupti.', '2015', 'Sit nulla ab minima et autem impedit eum. Magnam suscipit autem porro. Aut expedita exercitationem voluptas consequatur in. Quod fuga aut commodi enim ullam. Voluptatem quaerat ipsum autem hic totam omnis animi. Qui est accusantium voluptatem eos corrupti. Et qui voluptatem facere perspiciatis corrupti. Doloribus excepturi temporibus ipsam temporibus quos. Recusandae sunt est similique voluptas incidunt omnis. Asperiores odio perspiciatis laboriosam dolores voluptas. Libero non nobis qui et consequuntur quo laudantium. Consequuntur deleniti hic molestias quas ut molestias. Et veniam dolores sint architecto autem aliquid.', '2015-09-04 01:03:33', '2015-09-08 12:20:16');
INSERT INTO `workshops` VALUES ('13', '37', 'Mollitia consequatur aut.', 'Voluptas rerum esse dolorum omnis. Consectetur quo odit in est soluta nesciunt autem.', '2004', 'Ea culpa dolorem eveniet veritatis tenetur. Odio facilis impedit saepe et repudiandae aut amet dicta. Dolor iusto a sunt distinctio. Nihil maxime ut nisi dignissimos voluptatem autem. Qui eos doloribus veniam tempora. Saepe qui beatae et sint autem et quia. Sint voluptate odio accusamus temporibus aut expedita excepturi. Vitae tenetur doloribus similique in. Ab non qui qui ratione. Cumque ipsa aut dolor consequatur quasi. Quos cumque dolor est error.', '2015-09-05 18:35:57', '2015-08-13 22:51:59');
INSERT INTO `workshops` VALUES ('14', '11', 'Aspernatur occaecati aut molestiae fugit velit.', 'Ipsam in doloribus voluptatem eaque a quo nihil vel. Excepturi modi aut in fugit exercitationem est assumenda. Earum doloremque sed perferendis earum et nostrum.', '2016', 'Eum cupiditate iste quia sequi. Ipsum non sequi iusto adipisci ut perferendis. Quo sed animi non eum tempora et nulla. Explicabo est sunt laboriosam debitis excepturi qui. Vero numquam ea nihil occaecati beatae accusamus et. In qui quo soluta est et sed. Ducimus nihil culpa omnis ut. Quia et tempora sunt suscipit rerum. Veritatis totam fugit impedit labore explicabo officiis molestiae deleniti. Libero quis et et quis non rerum repellat. Nisi nobis voluptatem ut dolorem tempore nisi. Explicabo ad eum enim sint. Omnis et id omnis assumenda molestiae maiores expedita.', '2015-08-19 13:23:31', '2015-09-03 04:40:35');
INSERT INTO `workshops` VALUES ('15', '29', 'Aut in aut et eligendi nihil.', 'Ratione neque voluptatem est dolore consequuntur. Neque dolore quae rem maiores qui. Perspiciatis mollitia magnam aut culpa deleniti amet.', '2002', 'Impedit ullam minus molestiae aut. Accusantium impedit earum ipsa architecto. Quia sunt exercitationem quidem aut numquam. Ab non quisquam ex quibusdam repellendus illum ut voluptas. Iusto eligendi dolorem distinctio itaque quia quasi eveniet. Recusandae eum consequatur expedita sunt officia. Doloremque aspernatur qui qui cumque. Blanditiis quo perferendis est in laboriosam error. Recusandae aut laborum cum voluptas blanditiis. Cumque eaque et corporis unde voluptatem dicta commodi magnam. Fuga veniam molestias fuga. Occaecati fugit et velit consequatur ut fuga voluptatem qui. Et molestiae hic animi asperiores minus voluptate.', '2015-09-06 23:11:54', '2015-09-03 07:15:54');
INSERT INTO `workshops` VALUES ('16', '1', 'Eum qui quis qui.', 'Veritatis facere eos sed eveniet. Tenetur neque in totam eligendi. Ea itaque enim id quod nesciunt.', '2014', 'Recusandae excepturi aut eaque earum deleniti alias. Sit est sapiente illum accusantium blanditiis voluptatem quis. Dolor voluptatem laudantium minus quisquam voluptatem porro et dolorem. Omnis consequatur illum autem culpa omnis. Aut reiciendis officia voluptate iusto et. Qui tempore dolor ullam molestiae fugit. Perferendis enim repudiandae sint omnis. Est consequuntur qui beatae minima. Repellendus qui maiores magni quisquam.', '2015-08-11 02:04:41', '2015-09-04 19:07:39');
INSERT INTO `workshops` VALUES ('17', '42', 'Asperiores voluptatem nisi ut.', 'Quod iste sint consequatur aut quam explicabo suscipit. Sit quo eos fuga qui consectetur vero architecto error. A ut occaecati quo tenetur.', '2005', 'Ut laborum aliquid dolores quidem. Similique minima iusto dolores autem magni. Dolorem sint sed sed eos consequatur. Cum distinctio consequatur sapiente. Id autem doloremque asperiores nam sint doloremque. Mollitia officiis nihil sed aut rerum cumque temporibus nam. Officia cupiditate cum fugiat distinctio molestiae. Quis quidem recusandae eos est quisquam aut. Facere dolor et qui sequi atque dolor quia rerum. Sapiente et accusantium eum. Placeat architecto soluta ea unde sit ipsa alias. Esse eum omnis incidunt sequi omnis delectus fugiat. Sit et accusamus delectus distinctio. Accusamus et eligendi et autem.', '2015-08-24 21:25:17', '2015-08-24 22:49:56');
INSERT INTO `workshops` VALUES ('18', '46', 'Ut iste est quia.', 'In assumenda natus vitae est repellat saepe hic corrupti. Nemo est mollitia vero consequatur error placeat laudantium.', '1987', 'Dolor vero minus impedit aut. Velit aut qui maxime expedita et. Repellat cupiditate a consectetur sunt enim ipsa. Beatae laboriosam non perferendis. Porro sed voluptas qui occaecati quod rem sequi. Cumque est quis quia quaerat laboriosam odit laborum et. Quos optio voluptatem sed rerum quibusdam explicabo. Ducimus neque et delectus porro. Ipsum ea illum in sunt consequatur voluptatem. Minima veritatis pariatur nulla ut labore doloribus consequatur odio.', '2015-08-18 18:22:57', '2015-09-07 22:13:14');
INSERT INTO `workshops` VALUES ('19', '35', 'Quia quas incidunt.', 'Qui veniam ea totam quas et et ea. Assumenda et officia est numquam quis consequuntur. Qui quia fuga enim nihil facere quis.', '1984', 'Ullam accusamus enim nihil mollitia voluptas vero. Nam esse architecto maxime itaque laborum praesentium corrupti. Qui facilis temporibus sit in. Aut laborum quis sunt officiis et deleniti. Saepe nulla dolorem iste sint et pariatur debitis. Non totam et non dolor necessitatibus. Consequuntur porro qui eos et et et. Assumenda deserunt fugiat non. Omnis et dolore molestias ut rerum esse. Architecto debitis qui libero quis voluptatem. Quis alias ea illum sint recusandae enim et. Nam odit sed rerum veritatis occaecati. Est et rerum temporibus ullam inventore exercitationem aut.', '2015-08-29 07:54:25', '2015-08-12 19:04:08');
INSERT INTO `workshops` VALUES ('20', '28', 'Est molestiae porro eius voluptatem.', 'Dignissimos at voluptates temporibus cupiditate dignissimos rerum ullam. Sint ut aut iure dolores autem. Molestiae minima aut molestiae voluptas dignissimos dolorem.', '2000', 'Atque expedita pariatur enim cum. Esse et minima quidem minus. Et sapiente voluptas aliquam ut cum et praesentium. Perferendis et ipsa voluptas aut. Recusandae illo reprehenderit nesciunt quod aut tenetur. Voluptatibus nisi officia a quo autem. Ut omnis ea rem repellendus ad. Ab iste quo ab. Tempore unde doloribus corporis voluptatibus commodi aperiam distinctio velit. Eum non est aut dolorem facilis rerum voluptate. Non voluptatem et et est ea ut incidunt. Natus enim voluptatum nihil nulla. Et sunt earum quis omnis corporis nihil. Nobis aperiam odit minus consequatur voluptatibus.', '2015-08-14 01:09:58', '2015-08-11 02:08:41');
INSERT INTO `workshops` VALUES ('21', '48', 'Error quod voluptatum accusantium quos quas.', 'Ipsam voluptatem sed quia et. Cumque placeat praesentium eos saepe alias molestiae veniam accusamus. Non sequi explicabo sapiente eos repellat totam quasi eum.', '1996', 'Vel similique nulla dolorem. Aliquid unde cumque qui sed qui blanditiis ut. Non tempore voluptas sint sapiente ratione. Veritatis ut voluptatem voluptatibus placeat libero ut amet. Labore in eos excepturi optio ipsam ut. Quae deserunt iusto voluptatum et ut sunt. Dolore eveniet quos corporis est temporibus quod minima.', '2015-09-07 16:29:50', '2015-09-06 13:43:50');
INSERT INTO `workshops` VALUES ('22', '25', 'Quia sequi labore ut.', 'Ea debitis ut delectus officia consequatur veniam magni possimus. Distinctio corporis qui soluta possimus sed molestias magnam omnis. Velit voluptatem velit nihil velit excepturi mollitia.', '1990', 'Aut et commodi voluptatem est ut. Exercitationem ab adipisci perspiciatis minus qui consequatur quod laboriosam. Totam necessitatibus nisi qui odio debitis qui. Quo officiis laboriosam dolore. Expedita rem nostrum quia sed est laboriosam. Qui accusantium neque eveniet nobis illum corrupti ex. Officiis similique ea impedit id quaerat qui laborum. Ut iste sit tenetur rem. Iste esse et sit autem in. Nulla dolore sed id pariatur consequuntur fuga.', '2015-09-03 16:26:31', '2015-09-04 10:16:16');
INSERT INTO `workshops` VALUES ('23', '17', 'Sequi et molestias ab adipisci.', 'Veniam ea voluptate corporis vitae omnis doloremque tempore ad. Eaque architecto numquam sed aut.', '1997', 'Hic quia eius tempora quod provident placeat odio. Non placeat unde odit alias. Voluptatem ut eos aliquam voluptate. Est et est ut. Necessitatibus velit voluptate hic aut laudantium. Sed minus quaerat dolore facilis velit sit. Mollitia at reprehenderit doloribus qui. Molestiae qui quod rerum ducimus. Fugit quis architecto soluta voluptatibus quidem inventore quo.', '2015-08-29 06:01:17', '2015-08-31 01:21:13');
INSERT INTO `workshops` VALUES ('24', '43', 'Rem et et fugiat nulla.', 'Et quae amet ab. Rerum magni rem ut provident optio nemo exercitationem. Aut corrupti pariatur dolorem deserunt facere pariatur voluptates.', '2005', 'Et hic vero adipisci ipsum molestiae sed. Et officiis rerum voluptates dolore. Quae cumque debitis ea id ad aut iusto repudiandae. Et unde iusto vitae laboriosam quo quia. Modi optio architecto vel voluptatum tempore non optio architecto. Unde dolorem sunt est. Expedita nesciunt excepturi quis. Vero ut hic natus cum. Et qui at totam placeat minus modi. Eius est nihil eaque corporis assumenda rem. Aut vel culpa provident fuga magnam commodi facilis. Quis qui nesciunt aut voluptas veritatis numquam eaque.', '2015-08-25 01:17:46', '2015-08-12 10:08:16');
INSERT INTO `workshops` VALUES ('25', '14', 'Modi sed eaque ipsam dicta.', 'Porro animi nobis magnam. Voluptas a quia harum voluptatem est placeat saepe. Sint illum animi iste qui.', '2001', 'Culpa nam accusamus magnam iste quia excepturi. Dolor fugit numquam dolorem nihil et. Minima vel ipsum facilis nisi rerum. Odio nobis blanditiis non et ipsa. Doloremque sit ab aliquam quae. Perspiciatis rerum hic quibusdam voluptatibus. Non laborum ipsa nihil blanditiis itaque placeat vel. Minima et quas sint.', '2015-08-16 11:32:59', '2015-08-23 04:50:08');
INSERT INTO `workshops` VALUES ('26', '26', 'Nobis commodi inventore voluptate iure.', 'Quo ullam id qui facere itaque tempore suscipit. Et eveniet non et qui optio voluptatem cumque. Temporibus odio asperiores ipsum fuga.', '1995', 'Quia eos vel dignissimos omnis tempore. Aut laborum illo rerum architecto vel sint. Esse sint nihil iure iusto pariatur. Culpa est quo id perspiciatis quam. Commodi sapiente ut autem temporibus. Qui nemo quas odio laborum et ad eum inventore. Autem quisquam ut ipsam qui sed et voluptates. Ipsam voluptatum est explicabo inventore voluptatibus sit tempora ut. Numquam voluptatibus distinctio minima aspernatur. Recusandae rem ipsa veniam laborum.', '2015-09-03 01:10:14', '2015-08-21 14:25:52');
INSERT INTO `workshops` VALUES ('27', '9', 'Veritatis et quibusdam est.', 'Id sed est a dignissimos. Sed sit laudantium dolor voluptatem. Atque reiciendis numquam autem unde incidunt quia alias.', '2011', 'Voluptatem qui voluptas facilis qui. Excepturi fugit commodi iusto labore quis. Debitis culpa omnis eum voluptatem exercitationem fugiat deserunt nihil. Quis quo sequi illum ut. Qui maxime qui perferendis nesciunt vel. Nemo quis et possimus incidunt sequi sed qui recusandae. Eos omnis nostrum perspiciatis blanditiis. Tempora veniam voluptas quae qui rem vitae est.', '2015-08-26 11:13:33', '2015-09-05 05:03:16');
INSERT INTO `workshops` VALUES ('28', '19', 'Quos dolorem magnam saepe.', 'Est voluptatem fuga reprehenderit voluptatem minus qui minima. Enim repellat veniam facere reiciendis expedita officiis. Beatae fugiat voluptatem animi.', '2013', 'Saepe minus omnis natus iste distinctio repudiandae. Et eos ut et dolor temporibus eos maiores. Reiciendis voluptatem repellat ea nisi. Autem vero voluptatem numquam similique nesciunt accusantium perspiciatis. Id ut autem quae tempora consectetur velit quidem velit. Laudantium inventore placeat quisquam natus iure. Iusto dignissimos placeat minus sed velit. Voluptas magni sit ratione consequatur. Accusantium nobis voluptas omnis dignissimos. Non laborum enim assumenda porro consequuntur vel hic ea. Animi itaque corporis impedit dolor repellendus. Reprehenderit cum et enim iure atque. Ut omnis minus rerum dolorem modi. Placeat nam aut esse aut assumenda numquam sed ducimus.', '2015-09-03 14:08:09', '2015-09-05 01:34:34');
INSERT INTO `workshops` VALUES ('29', '25', 'Explicabo non omnis enim.', 'Voluptate fugit a ut. Eum consequuntur cupiditate nihil magnam. Et recusandae ut aut.', '2002', 'Ullam odio numquam iure error magni natus enim. Et quis laudantium sed. Nesciunt libero est sint aliquam totam quia. Perspiciatis maxime reprehenderit ut sed voluptas eos qui. Sit a quisquam eius occaecati aliquam sunt dolorem. Id quaerat magnam quo porro totam nemo. Veniam aut perspiciatis eius et saepe. Repellendus eum et delectus dolor rerum explicabo qui dolore.', '2015-08-09 02:55:00', '2015-09-06 09:46:44');
INSERT INTO `workshops` VALUES ('30', '45', 'Molestias consequuntur sed iure quisquam libero.', 'Voluptas quis beatae et voluptatem vel quam. Nulla molestiae dolor fuga at minima voluptatum. Qui reprehenderit voluptate et et ea commodi.', '1982', 'Quibusdam rerum sed molestiae et rerum impedit illum. Temporibus iusto vero dolorem excepturi recusandae dolorem vel. Sed cum asperiores ratione qui ducimus sint. Omnis officiis quia architecto nam reiciendis eligendi. Laudantium ad esse reprehenderit quas. Nam architecto nostrum sunt ipsum numquam est repudiandae. Quasi quas recusandae exercitationem vero illo odit. Quisquam id officia temporibus reiciendis et sit. Veritatis nobis debitis ut ullam.', '2015-09-02 10:34:33', '2015-08-24 08:51:56');
INSERT INTO `workshops` VALUES ('31', '10', 'Esse iste sint.', 'Necessitatibus excepturi placeat nam necessitatibus voluptatum assumenda saepe. Id et quidem ipsum quibusdam.', '2013', 'Et temporibus dignissimos illum aperiam qui. Dolorum eius reprehenderit est iste deleniti tempora. A nesciunt odio voluptate enim voluptas quas quia. Quia ipsum et blanditiis. Quod labore facere natus voluptatem quos quos. Eligendi id ducimus necessitatibus praesentium. In tempore voluptas ea. Harum doloremque sed dicta. Nihil quos omnis quisquam. Consequatur aliquam culpa nostrum corrupti. Natus iste non consequuntur ipsam velit laboriosam. Sit pariatur itaque et ut nulla itaque esse. Tenetur blanditiis aspernatur voluptatem porro omnis officiis.', '2015-08-12 05:43:37', '2015-08-13 20:02:16');
INSERT INTO `workshops` VALUES ('32', '1', 'Et impedit officiis.', 'Ut aut molestias autem consequatur. Sit in ea fuga provident aut reprehenderit. Aut nostrum illum expedita nemo quis deleniti.', '1980', 'Possimus id eum aut dolore cumque. Dolor illo et voluptatem adipisci minima. Maxime amet voluptatem voluptatem voluptate non. Quidem id voluptatem incidunt sit sit est. Voluptates quibusdam qui et voluptate. Ab dolore dolorem consequatur fuga excepturi qui recusandae. Architecto omnis aut nemo nihil eum eum perferendis repellendus.', '2015-08-13 12:24:33', '2015-09-04 14:42:53');
INSERT INTO `workshops` VALUES ('33', '31', 'Molestiae ut consequatur eius rerum.', 'Veniam dolorem assumenda omnis voluptates exercitationem quod impedit fugit. Et architecto numquam alias corrupti occaecati autem eligendi. Aliquid est ab nobis inventore illo ea.', '2013', 'In quia excepturi odio corporis qui rerum sapiente. Consequuntur sed quidem quam nostrum nihil. Aut architecto consequuntur alias consequuntur magni et. Sit quasi corrupti dolores cum. Voluptates qui repellat omnis veritatis tenetur labore molestiae odio. Ut eos quia molestias assumenda. Numquam architecto sunt rem provident sunt saepe sed. Enim soluta officia eum accusamus voluptatum voluptatem ut. Sequi fugit similique quos tempora. Dolorem sunt aut sed expedita accusamus consequatur non. Est voluptatem quos porro ipsum repellat sed illo. Laudantium minus laborum quo at. Est inventore nam vitae aut est. Tempora rem deleniti molestiae amet illum.', '2015-09-07 18:56:28', '2015-08-23 08:44:50');
INSERT INTO `workshops` VALUES ('34', '12', 'Libero voluptas ut corporis dicta qui.', 'Quisquam aut velit quo dolores quod. Commodi non eum eius eos quia veritatis.', '1990', 'Repellat saepe ex hic culpa doloribus veritatis magni. Repellendus quia qui doloremque eos dolores sunt rem. Maiores possimus iste est iure esse ratione. Vero qui voluptatem itaque suscipit adipisci aperiam. Voluptas consequatur qui ut quam. Sint beatae nisi praesentium rerum magnam. Possimus rerum incidunt sequi aut. Porro ut omnis iste repellat. Tempore eaque provident fugiat numquam.', '2015-08-19 18:47:16', '2015-08-18 10:58:43');
INSERT INTO `workshops` VALUES ('35', '1', 'Vero necessitatibus sapiente tempore.', 'Maxime consequatur voluptatem placeat repudiandae. Rem animi vitae occaecati sunt. Fugiat enim rerum quo odio vel.', '2008', 'Eius consequuntur eaque non quod maxime voluptas ratione. Unde cumque voluptate quam. Ratione nulla atque enim quia esse. Sunt dicta voluptatem laborum et nihil consequuntur autem. Magnam eaque hic soluta et porro ut. Exercitationem eligendi dolorum et nisi veritatis. Dolorem consequatur architecto et non dolores.', '2015-08-28 19:20:46', '2015-09-06 14:10:04');
INSERT INTO `workshops` VALUES ('36', '44', 'Ducimus blanditiis dolor laborum itaque.', 'Quo qui quis qui sint. Aut expedita aut et voluptatem sit natus aut.', '2015', 'Pariatur sapiente corrupti nulla accusamus facere soluta. Consectetur saepe nemo autem autem. Non odit quis dolorem. Et earum modi id cupiditate eligendi. Exercitationem fugiat ea dicta nisi id et corrupti. At reprehenderit necessitatibus asperiores expedita unde itaque totam. Enim nihil enim minima a. Voluptatem quis animi et.', '2015-09-05 07:04:15', '2015-09-08 20:58:21');
INSERT INTO `workshops` VALUES ('37', '11', 'Porro vitae reprehenderit.', 'Vero eveniet amet perspiciatis dolore voluptatem accusantium velit. Perspiciatis eos qui beatae consequatur at. Dignissimos quo explicabo nihil ex rerum quas.', '1992', 'Sint nihil distinctio labore dolore non officia numquam. Eos quis quae minus labore praesentium aut voluptate. Non praesentium et qui eos autem. Facere eligendi corporis et qui perspiciatis voluptatem. Ipsam velit architecto quam consequatur praesentium architecto. Necessitatibus velit dolorem aliquam dolor. Architecto alias asperiores ab ad dicta dolores.', '2015-08-27 11:24:24', '2015-09-04 07:08:02');
INSERT INTO `workshops` VALUES ('38', '13', 'Eius nulla alias labore.', 'Et omnis rem recusandae. Occaecati nihil ipsam deleniti aut consequatur optio.', '2000', 'Mollitia autem qui placeat. Fuga voluptates ratione quas voluptatem totam qui. Est repudiandae sed quo deleniti praesentium quam vel. Velit velit modi distinctio tempora. Voluptates maxime hic dicta alias molestiae est dignissimos. Ipsam aut est quia modi. Voluptate cum quae velit quos dolore mollitia sequi. Facilis explicabo magni ducimus fuga. Nihil ad similique unde iure voluptas.', '2015-08-31 01:02:00', '2015-08-19 08:44:42');
INSERT INTO `workshops` VALUES ('39', '41', 'Eligendi amet est iure quaerat architecto.', 'Accusamus dolorem deleniti velit dolores et quos. Fugiat qui voluptas ut porro. Repellat eos perferendis libero voluptates architecto delectus natus.', '1983', 'Harum laudantium sint accusamus quidem quia expedita laborum. Rerum id blanditiis et dolor ea ea ipsam. Vel voluptas molestias nemo aspernatur. Sed fugit est error ullam. Quia voluptatem sapiente maxime ut non aut quo. Possimus rerum sunt sit ut qui. Dolorum enim beatae saepe quasi. Odit occaecati ipsum repellendus saepe non. Aut ea minima ex et quidem voluptas animi adipisci. Id doloremque recusandae nihil architecto rerum aut. Velit quis non delectus veritatis magni odio. Et numquam dolores sit impedit cupiditate. Ut tempore fuga rem dolores. Et et impedit incidunt fugit alias sed magnam neque.', '2015-08-19 14:33:00', '2015-08-31 11:29:48');
INSERT INTO `workshops` VALUES ('40', '12', 'Voluptatibus ut tenetur et.', 'Modi vel unde nulla vero ipsam nihil. Non dolor rerum voluptatem quisquam sint eos. Ut est sunt aliquam voluptatum cupiditate.', '1982', 'Vel culpa repellendus ullam et. Accusamus voluptatem vel a ratione non distinctio distinctio. Suscipit qui officia rerum qui repudiandae explicabo deserunt. Quisquam doloremque repellendus quidem exercitationem. Repellat iste odit culpa cum qui facere asperiores. Doloribus nam quia quia sit. Voluptates impedit tenetur nobis rerum. Tempora voluptatem sit blanditiis qui. Numquam numquam ullam id voluptatem veniam incidunt. Iste mollitia commodi dolore illum inventore quos.', '2015-08-15 23:03:12', '2015-08-21 01:10:41');
INSERT INTO `workshops` VALUES ('41', '34', 'Doloremque corrupti voluptates.', 'Sit enim aut quas. Nihil fuga nihil provident voluptates nesciunt deleniti sed.', '2007', 'Ea reiciendis dolor laborum aut a quia. Id perferendis itaque dolores voluptatem consequatur placeat possimus. Sapiente quam vero nihil nesciunt nobis. Maiores vel aut minus. Et dolorum est beatae aperiam. Quos tempora recusandae rerum quia quae. Voluptas qui eos et adipisci. Cumque porro quod recusandae ut labore et cupiditate.', '2015-08-30 08:15:25', '2015-08-23 06:32:10');
INSERT INTO `workshops` VALUES ('42', '25', 'Cumque ex nihil soluta nisi et.', 'Cupiditate id natus fugit. Perspiciatis soluta aut officia aliquid sapiente. Corrupti temporibus et aliquid delectus quia.', '2011', 'Quis qui aspernatur quis repellat laboriosam aliquid aut. Est nobis vel neque quasi accusamus. Soluta et autem dolores voluptatem iure quidem cum. Praesentium esse atque aliquid nesciunt. Est molestiae error sit ut. Minima unde iste impedit excepturi. Rem rerum ex ut voluptatibus ad aut. Et veritatis consequatur recusandae id. Totam voluptatem quia sint facere delectus est deserunt dignissimos. Nesciunt ut vitae atque culpa. Aliquid aut excepturi dolorem minus tempore. Doloremque et magnam rerum molestias. Occaecati rerum ea distinctio quam sed vero nulla ut. Saepe voluptatibus velit quis ea officiis voluptas maxime.', '2015-08-16 07:08:09', '2015-08-18 05:43:34');
INSERT INTO `workshops` VALUES ('43', '31', 'Totam perspiciatis dicta itaque.', 'Vero exercitationem at enim non. Id dolor dolorum repellendus qui enim natus expedita et. Est rem corrupti fugit totam omnis eum necessitatibus numquam.', '2012', 'Tempore beatae eos exercitationem praesentium assumenda modi sunt. Quis facere non ex placeat quam voluptates ipsam. Quisquam facere id expedita accusamus labore. Tempore debitis perspiciatis voluptate quia asperiores. Dolorem laborum dolores quasi laborum autem. Natus saepe dicta rerum delectus. Nam in iure quam corrupti illo consequatur. Officiis aut et nihil.', '2015-09-03 11:33:55', '2015-08-12 12:42:05');
INSERT INTO `workshops` VALUES ('44', '38', 'Ea corrupti dolore reiciendis magni.', 'Facere consequatur quia odio et. Esse eligendi ut excepturi rerum rerum est voluptates corporis.', '1983', 'Vel quis voluptatibus optio enim rerum. Repellendus rerum aut numquam ut voluptatem laboriosam officia dolor. Asperiores odio optio quo voluptatem. Sint quam doloremque omnis saepe omnis. Cumque labore quibusdam et sint provident magni. Dolores sed accusamus qui est eum quasi voluptatem. Aperiam saepe ut eaque eum quo fugiat. Optio est sit et temporibus voluptas. Accusamus sequi aliquam aut quos non corrupti et. Rerum perferendis ipsa est neque qui autem perspiciatis.', '2015-08-12 18:38:07', '2015-08-27 10:27:46');
INSERT INTO `workshops` VALUES ('45', '24', 'Amet qui consectetur dolore.', 'Cum rem aspernatur rerum voluptatum. Facilis rerum aut quod perferendis molestiae ut. Ex excepturi esse quia autem voluptatem excepturi voluptates.', '2003', 'Assumenda qui quia qui est aliquid. Dolorum qui alias tempore aut ratione repudiandae aut rerum. Amet molestias fugit animi accusamus qui. Magnam quaerat aut at optio similique. Officiis animi laboriosam ratione alias est sunt et. Impedit explicabo voluptatem illum cupiditate ut recusandae aperiam. Ullam distinctio omnis et velit. Magni nostrum voluptate expedita eius veritatis expedita.', '2015-08-29 14:13:54', '2015-08-13 01:07:12');
INSERT INTO `workshops` VALUES ('46', '8', 'Neque libero optio optio.', 'Recusandae molestias deleniti quae consequatur. Id qui quisquam aperiam. Aliquam tempora qui quia porro est.', '2004', 'Incidunt voluptatem voluptatem sed deleniti sunt similique. Consequatur exercitationem doloribus et. Officia quae repudiandae minima aut. Corporis odit officiis minima sed. Quia aut totam et placeat aliquam temporibus adipisci omnis. Dolorem commodi eius assumenda unde sed quisquam. Minus officia quos et est fugiat. Ut eos ipsum exercitationem aut. Ab laudantium ea dignissimos aperiam sunt.', '2015-08-12 10:29:08', '2015-08-31 04:32:50');
INSERT INTO `workshops` VALUES ('47', '28', 'Repellendus quia illo non consectetur pariatur.', 'Nemo accusamus aut blanditiis quas in inventore minima. Maiores laudantium sed ipsum sit expedita ut numquam repellendus. Sed at commodi placeat quia vel culpa consequatur exercitationem.', '1986', 'Voluptas distinctio culpa itaque. Enim sed ex sit qui molestiae eligendi labore. Culpa voluptatum ullam aut mollitia soluta. Ipsum ut voluptas distinctio est quia saepe minus. Dolor dolorem voluptatem aut consequatur dicta ut et. Provident harum quaerat quidem consequatur. Qui in veniam a quis eligendi eaque accusantium odit.', '2015-08-19 04:00:24', '2015-08-18 10:32:41');
INSERT INTO `workshops` VALUES ('48', '35', 'Ullam eum distinctio earum nostrum id.', 'Tempora quia quisquam quia sit consectetur soluta. Omnis dolores illo unde culpa cum.', '1985', 'Non earum harum quisquam. Dolores eius quidem et impedit ea quos. Beatae soluta suscipit officiis quod sit ut. Aut necessitatibus illo quis molestiae totam. Aut tempore earum repellendus magnam. Nostrum qui et dicta molestias. Nihil et quisquam atque facere quia quia. Ipsam numquam voluptas tempore ipsa voluptatem ut iste. Culpa magni ipsam odio repudiandae porro nisi.', '2015-08-11 19:38:35', '2015-08-11 23:31:28');
INSERT INTO `workshops` VALUES ('49', '4', 'Voluptate ipsa aut occaecati enim ab.', 'Vitae eligendi provident fuga. Quo quis facilis consectetur aut aut labore et.', '2013', 'Dicta beatae rerum consequatur facere eligendi et blanditiis et. Dolore debitis hic aut ut. Et ad aut quae nemo et facilis. Magnam aut velit aperiam asperiores. Dicta alias iure modi. Natus aliquid quam eaque quas. Ullam repellat dolorem nesciunt quo quibusdam quasi. Aspernatur aliquam enim et veritatis. Et amet repellat vero omnis non laboriosam nihil dolor. Nostrum qui incidunt pariatur et.', '2015-09-01 14:08:15', '2015-08-27 08:20:00');
INSERT INTO `workshops` VALUES ('50', '23', 'Quis ad reiciendis.', 'Commodi magni doloremque consequuntur quam ipsa quidem. Voluptas quis consequatur quidem esse atque.', '1983', 'Officiis velit nostrum aperiam ut aut at enim. Omnis voluptas dolorum earum animi aut accusantium earum. Excepturi inventore iste quos magni hic nostrum dignissimos. Qui vero vel quae impedit rem eaque rerum nisi. Omnis sit voluptatem natus. Aut beatae omnis nesciunt ab ut necessitatibus accusamus. Accusantium expedita voluptatum quae recusandae. Aut voluptatem laudantium voluptas accusantium ipsa in nisi. Ea ut recusandae est harum saepe. Aliquam cum commodi ducimus quia voluptatum dolores.', '2015-09-02 13:35:35', '2015-09-06 10:01:21');
INSERT INTO `workshops` VALUES ('51', '35', 'Excepturi minus repellendus iure quidem.', 'Quos quod qui ut eaque. Ut explicabo facilis eveniet ex ratione cupiditate eveniet.', '1991', 'Explicabo optio ad accusantium. Sit dolorem maxime cumque debitis cum provident et. Corporis est rerum exercitationem nihil dolorem. Voluptas dicta dolorem amet facere accusantium accusantium. Deleniti ut quis adipisci eos. Sunt rerum voluptas iure accusamus sunt eligendi. Voluptate earum fugit ut numquam cum impedit est. Deserunt recusandae architecto aperiam sunt. Est voluptates atque autem. Sit distinctio nemo consequatur est eius ad. Quidem esse hic voluptas adipisci et totam qui.', '2015-08-11 13:02:33', '2015-08-27 07:12:17');
INSERT INTO `workshops` VALUES ('52', '4', 'Minima laboriosam quis voluptates recusandae.', 'A voluptatem ipsam autem enim non accusantium et. Aut libero est eum dignissimos. Perferendis quia illo quod sit est saepe.', '2013', 'Aut ea saepe culpa. Minima aut sapiente est explicabo ut quisquam. Animi rerum ut hic vitae. Cum magni sit reprehenderit non consectetur. Temporibus ut a laborum in provident nemo voluptas. Temporibus aut velit suscipit eum et accusamus. Voluptas alias quia iure in. Fuga eligendi autem magnam recusandae expedita iusto qui. Dignissimos repellendus excepturi rerum a velit esse. Corporis perferendis omnis voluptates. Ab sunt illo adipisci quae consequuntur eius. Incidunt omnis a numquam est. Sint dolorem dolorum quae vel in accusamus rerum. Fugiat dolor molestiae inventore sint aut vel est praesentium.', '2015-08-26 13:47:54', '2015-08-26 01:43:41');
INSERT INTO `workshops` VALUES ('53', '33', 'Illo soluta perferendis.', 'Sint voluptatem qui quod exercitationem. Eligendi aut rerum repudiandae voluptate eos dolore et. Iure repellat sapiente fuga nostrum voluptatum.', '1991', 'Nam neque impedit qui veniam. Voluptatem sit veritatis voluptatem hic ut eos voluptatibus. Totam perferendis dolores distinctio debitis. Nostrum necessitatibus provident architecto officiis. Est voluptas corrupti mollitia ut. Illo veritatis iusto in optio natus ipsum tempore. Saepe consequuntur ipsam et. Autem necessitatibus distinctio vel molestias quaerat quia vero nemo. Aut voluptas nemo eligendi placeat. Aspernatur unde voluptatibus aliquam eum laboriosam assumenda similique voluptates. Et dolores nisi ipsam. Saepe aut blanditiis repellat in iste dolor. Omnis asperiores id alias. Magni voluptatibus necessitatibus doloremque labore commodi neque vitae.', '2015-09-03 18:13:35', '2015-08-22 23:00:02');
INSERT INTO `workshops` VALUES ('54', '23', 'Magnam laboriosam repudiandae nam.', 'Magni odit vero harum labore iusto ut sunt. Consectetur qui consectetur sint possimus.', '1997', 'Provident aspernatur quisquam dolorem aut illum. Sint ut asperiores dolorem ut occaecati fugit. Inventore tempora quo illum ipsum consequatur. Ex minima nesciunt ea eum dolorum aliquid. Ut autem harum voluptas vel omnis nostrum officiis. Sint et at quas quos. Soluta explicabo sed dicta. Consequatur voluptas quidem sed reiciendis vitae aliquid iusto. Et quos non iste rerum placeat recusandae. Molestiae odio aut veritatis voluptatem qui. In perspiciatis tempore dignissimos dolorem consequatur. Repellat animi autem explicabo voluptatem ut cum sit.', '2015-09-08 12:02:36', '2015-09-02 18:53:54');
INSERT INTO `workshops` VALUES ('55', '21', 'Vero tempore impedit.', 'Ut ex accusantium ipsum iusto suscipit. Et qui totam sit dicta eius at eos. Magnam eos voluptas dolores reiciendis corrupti enim.', '2005', 'Nisi unde voluptate libero dolor esse et tempore qui. Dolorum doloremque rem atque exercitationem minus quia. Qui eum neque sit velit officia aliquid quia. Quas quia dolores corporis quidem perspiciatis pariatur. Praesentium id ratione quia omnis repellat fugit. Architecto eos maiores voluptatem aperiam. Aut fuga eaque et facere. Nam delectus velit error autem voluptatum.', '2015-09-05 10:22:50', '2015-08-11 17:43:00');
INSERT INTO `workshops` VALUES ('56', '28', 'Quia molestias molestiae corporis rerum.', 'Quisquam consequatur dicta doloribus molestias pariatur inventore est. Dignissimos praesentium repellendus dolor.', '1983', 'Ut nesciunt consectetur expedita aut sint sunt. Voluptas totam magnam inventore tenetur quidem pariatur eum. Totam et mollitia fuga dicta. Iure eius suscipit aliquam corrupti qui hic inventore. Commodi fuga non qui omnis cupiditate. Alias quidem ipsam sunt adipisci quos et. Facilis in error minima est et consequatur dolores. Atque ut ducimus omnis rerum consequatur dolorem. Eligendi corporis enim nulla sed ducimus numquam voluptates. Dicta a hic voluptatum similique quas. Beatae ut neque ipsum rerum animi quibusdam accusamus atque. Assumenda voluptatem corporis molestiae sed. Dignissimos qui amet sint quia.', '2015-08-22 04:37:35', '2015-08-16 08:32:48');
INSERT INTO `workshops` VALUES ('57', '22', 'Ut illum ad eius modi.', 'Mollitia ut doloribus consequatur accusamus. Rerum ratione rerum soluta vel rerum eum perferendis aut.', '1982', 'Voluptatem quia odit ab aperiam dicta consequatur totam. Id sit nemo veniam. Eveniet vitae ipsam nihil tenetur dolore ea. Dolor aut eaque aut et tempore asperiores. Sunt ex porro error voluptates culpa. Qui rerum perferendis ipsam. Accusantium veritatis quia occaecati quisquam ea maiores. Voluptate sint optio corrupti earum. Hic et voluptates officiis. Sunt quos quam porro perferendis. Eum ea animi ratione culpa ullam libero. Quia commodi totam molestiae nulla. Error harum sint consequuntur voluptatem consequatur sequi.', '2015-09-07 15:20:02', '2015-08-23 12:37:55');
INSERT INTO `workshops` VALUES ('58', '47', 'Est et ut quod quia esse.', 'Id corrupti facere suscipit dolor. Voluptas nihil dolorem omnis dicta quibusdam iste voluptatem deserunt. Qui doloribus vero doloribus esse blanditiis quis recusandae itaque.', '1981', 'Aut repudiandae sit sint. Assumenda rerum est et voluptatem sed vel ullam. Delectus magni voluptatem eaque. Facilis quasi ea voluptates aut unde ab. Architecto veritatis consequatur ratione architecto. Natus porro tenetur perferendis veniam dolores tempore odit. Odit fugit ut alias sed consequatur.', '2015-09-08 10:02:07', '2015-08-13 19:17:02');
INSERT INTO `workshops` VALUES ('59', '47', 'Est reiciendis deserunt reiciendis.', 'Velit aliquid odit nihil. Praesentium necessitatibus laborum veniam praesentium repellat distinctio. Vitae voluptatem voluptates et commodi unde.', '2005', 'Sed aut veritatis veniam possimus. Eius qui laborum modi consequatur et. Omnis ipsum suscipit facere consequatur eum provident nemo. Necessitatibus impedit ea culpa consectetur qui. Deserunt saepe reiciendis et fugiat qui ut aut. Quia aut ut molestiae nihil. Vero eum qui et corporis temporibus iste placeat et. Quidem est voluptates qui ipsam. Saepe quaerat voluptas reprehenderit omnis earum. Illum officia autem repellendus facere fugiat perspiciatis. Necessitatibus possimus maxime mollitia eos. Voluptatem commodi voluptas at ut dolorem laboriosam eaque rem.', '2015-08-24 05:42:22', '2015-09-04 02:02:43');
INSERT INTO `workshops` VALUES ('60', '7', 'Ut cumque nobis eius.', 'Occaecati ea at et illo sint. Numquam minima commodi voluptatum libero.', '1995', 'Enim error eaque architecto aut nihil. Necessitatibus voluptatem enim enim unde ex cupiditate asperiores. Ullam quia explicabo adipisci doloribus quam porro dignissimos rerum. Mollitia ut quis quia reiciendis exercitationem quidem nihil tempore. Libero voluptate ratione totam ratione id quidem eaque. Ad velit voluptatem voluptas voluptatem tempora. Et sit minima autem sint. Molestiae qui eveniet reiciendis ratione accusantium distinctio. Et dolorum consequatur id et porro ullam.', '2015-08-29 14:16:40', '2015-09-02 22:48:15');
INSERT INTO `workshops` VALUES ('61', '17', 'Sit perferendis fugiat.', 'Velit asperiores impedit occaecati placeat et illum eveniet. Voluptas vel aut inventore. Et aperiam adipisci aut doloremque ut blanditiis at.', '1990', 'Laboriosam sit aperiam et blanditiis quam maxime. Pariatur aut ad cumque quas at omnis esse et. Veritatis eum eius asperiores numquam. Et minima ut ut. Error consequuntur adipisci exercitationem accusamus. Veniam odit sit ut sapiente quam. Deleniti facere dolores eius repellendus voluptate in. Provident veniam est ut eum neque. Voluptatum quisquam numquam aut iste quaerat illum quae repellat. Illum rerum sint tempora. Labore mollitia molestias omnis. Aut architecto assumenda eveniet voluptatem quis quasi. Corrupti ut voluptas adipisci ut vel quaerat et.', '2015-08-09 01:09:29', '2015-08-31 10:22:37');
INSERT INTO `workshops` VALUES ('62', '4', 'Sint voluptatibus aspernatur.', 'Corrupti est temporibus suscipit laborum. Libero unde corrupti laboriosam dolor numquam consequatur totam tenetur. Tempore sed iste aliquam et provident adipisci modi.', '2012', 'Aut sed voluptatem qui quae quisquam et blanditiis. Ea qui hic doloremque dolores impedit. Totam et magni deleniti ipsam ducimus totam quisquam esse. Cum animi neque facilis autem ea minima. Quos placeat praesentium deleniti officia occaecati tempore tempore. Quisquam doloribus architecto voluptatem ea eum quas. Et et quae saepe sint blanditiis voluptatum eos. Necessitatibus quaerat odio qui provident sint. Et similique at perferendis unde quae quia. Architecto dolor nisi doloremque consectetur distinctio. Aspernatur voluptas dicta exercitationem corporis aspernatur saepe. Doloribus in est exercitationem deserunt.', '2015-08-12 02:17:37', '2015-08-29 13:08:08');
INSERT INTO `workshops` VALUES ('63', '23', 'Sint molestias qui rerum.', 'Qui non rerum libero deleniti repudiandae eum tempora. Ea nisi est provident beatae officia qui beatae quaerat.', '2008', 'Vero atque distinctio sequi amet est sed eius nobis. Aut ipsam iste sit qui qui. Facilis nihil quia quia molestias doloremque asperiores rerum ut. Autem consequatur sint ut. Omnis qui quae corrupti quo sint. Esse quisquam repudiandae at dolorem. Sequi quo non non atque beatae optio dicta. Alias debitis architecto dolores voluptatem. Consequatur autem consectetur ut vel molestiae est. Voluptates aut neque voluptate at enim nam. Et ut quisquam eum dicta voluptatem. Quibusdam suscipit saepe voluptatem eum voluptate aliquam maiores. Similique voluptas id eum inventore repudiandae. Et minima quis facere dolorem debitis quia maxime.', '2015-08-23 11:25:57', '2015-09-08 17:39:16');
INSERT INTO `workshops` VALUES ('64', '3', 'Est sequi officia architecto voluptas.', 'Sit repellat vel earum maiores ea. Soluta praesentium rerum quasi voluptates est odit qui. Veritatis necessitatibus nihil eum dolores id.', '1999', 'In aliquid officiis molestiae blanditiis repellendus in quis. Soluta odit et eum neque dolorum minus provident. Amet quam autem laborum. Qui sit magnam tenetur illo magnam et sapiente. Autem unde sit repellendus rerum qui. Est aut rem autem ab aspernatur et beatae omnis. Unde exercitationem a incidunt enim. Voluptatum pariatur iusto et. Qui explicabo nihil eveniet aut saepe. Sunt ut quidem consectetur dignissimos a et et deleniti.', '2015-08-30 13:46:57', '2015-08-29 06:15:20');
INSERT INTO `workshops` VALUES ('65', '34', 'Voluptatem iusto sed autem.', 'Perspiciatis iusto voluptates iste velit eum voluptate dolor. Corrupti sint autem ut perferendis eligendi quibusdam libero.', '1998', 'Qui placeat cumque non rem. Libero ipsum iusto libero minima doloribus. Quaerat consequatur et perspiciatis aliquam nobis non quia ullam. Nemo quia vel deleniti vel. Non sunt sunt eius expedita nulla quis. Quo amet quis saepe maiores maxime magnam dolores. Tempore aspernatur vel dolor ex non. Unde earum suscipit porro labore molestiae excepturi sed. Quidem rerum facere earum amet dicta. Impedit iste nobis assumenda non excepturi.', '2015-08-20 22:19:39', '2015-08-18 02:47:01');
INSERT INTO `workshops` VALUES ('66', '32', 'Sapiente perspiciatis et itaque.', 'Reprehenderit pariatur doloremque voluptatem quis dolorem quasi eos. Saepe facilis illum in deserunt.', '1993', 'Minus sit nihil et. Laborum deserunt enim est sed eius reprehenderit. Corporis dolore ipsa consequatur ullam. Cumque optio sed nam. Nisi similique aspernatur dignissimos culpa rerum sit laudantium. Ipsum officia cupiditate sed aspernatur est accusamus. Omnis quidem nihil laboriosam doloremque sequi adipisci dolore quae. Quasi qui dicta eligendi est a doloremque maiores.', '2015-08-22 16:18:04', '2015-08-29 10:15:20');
INSERT INTO `workshops` VALUES ('67', '41', 'At magni voluptatem.', 'Est porro velit dolores laudantium quos a in. Saepe porro et ducimus omnis sunt sint aut.', '2005', 'Pariatur excepturi ipsam qui aperiam a nemo. Delectus sed blanditiis ut esse voluptatem odit nam. Dolores eligendi illum voluptatem consectetur eaque. Nihil et atque hic omnis non tenetur. Et voluptatem dolores exercitationem et voluptas. Quas repellendus ut praesentium qui omnis id. Iure rem quia itaque. Et temporibus qui animi necessitatibus atque voluptatem. Cum amet at repudiandae excepturi. Rem nobis similique et mollitia ipsa voluptatem in.', '2015-08-16 11:51:31', '2015-08-09 13:11:51');
INSERT INTO `workshops` VALUES ('68', '14', 'Et et dicta omnis.', 'Nulla itaque ut ea asperiores autem et. Rem ut esse reiciendis sint quis. Voluptas dolores in nemo omnis dolor qui et.', '1997', 'Maiores quasi aliquam est ad aut ipsum vitae. Ut omnis eligendi ducimus accusantium sed commodi eum. Rerum quis atque eos sunt qui corporis consequatur vel. Voluptates maxime nihil est distinctio ullam in exercitationem ut. Asperiores quis sed possimus autem nesciunt. Ut quia dignissimos ut ut voluptatem ex eum. A nesciunt nesciunt esse reiciendis culpa id. Dignissimos dolorem quia eos sint veritatis voluptas sint quibusdam. Ut non recusandae ratione enim culpa. Molestiae quia eos autem ratione. Quisquam architecto repudiandae molestiae rerum explicabo. Fugit et sed officia natus.', '2015-08-31 19:46:05', '2015-08-28 16:12:00');
INSERT INTO `workshops` VALUES ('69', '1', 'Magni fugit et maiores.', 'Sed iusto ipsum et vel repellendus cumque dicta. Ut aliquam vitae rerum ullam. Facere qui et voluptatem accusantium sunt beatae enim neque.', '2006', 'In rem dolor rem aliquid explicabo distinctio nihil. Impedit ullam et quam amet ut rerum ex perspiciatis. Vel iste est nihil suscipit molestiae excepturi ratione. Dolorem aliquam impedit voluptas qui. Nihil consectetur nemo occaecati laudantium. Magni quibusdam eum saepe facilis. Necessitatibus numquam quia mollitia non eum possimus. Voluptas inventore sunt odit officiis quia earum. Doloribus quidem exercitationem sequi omnis praesentium eos. Tempore dignissimos illum sed magni ad qui. Voluptas expedita necessitatibus sed officia illum quam. Eos est libero beatae provident accusamus ratione tenetur quia.', '2015-09-07 01:27:02', '2015-09-07 01:41:19');
INSERT INTO `workshops` VALUES ('70', '48', 'Eos atque earum debitis.', 'Qui aliquam deleniti maiores esse ipsum. Tenetur perferendis doloremque et esse.', '1995', 'Sed consequatur dolores explicabo nam ex ea. Praesentium voluptas molestiae rerum voluptatem dolores atque totam. Et vero cumque doloribus nihil sit cupiditate vel. Id sed eum molestias error quaerat suscipit est ut. Vel architecto non sit architecto. Odit et et non laboriosam. Quisquam labore illo occaecati in necessitatibus nulla. Odit cum impedit necessitatibus et voluptatem debitis officiis. Voluptas et odit sit a sed commodi. Ut dignissimos sequi at quod vitae aspernatur voluptatem. Qui exercitationem est ex aperiam adipisci inventore autem. Nemo sit est quia quae accusantium. Atque velit reiciendis omnis quisquam ut cupiditate quod. Optio ullam doloremque temporibus sed.', '2015-08-18 09:37:57', '2015-08-18 05:28:11');
INSERT INTO `workshops` VALUES ('71', '20', 'Doloribus sequi et nisi.', 'Et autem quibusdam corporis aspernatur. Aliquid explicabo natus sapiente ullam voluptate enim sunt itaque. Eum est soluta nam nulla.', '1998', 'Omnis dolores in velit. Rem quia quis tempora iste. Dignissimos ipsum ut laudantium quod neque assumenda. Voluptate culpa veniam aut suscipit quis quia esse hic. Eaque tenetur vel vero voluptatem voluptate culpa. Architecto neque nihil in aperiam. Qui veniam exercitationem inventore consequatur.', '2015-08-09 23:43:31', '2015-08-28 13:09:47');
INSERT INTO `workshops` VALUES ('72', '26', 'Fuga nulla sit quae.', 'Ut et eligendi voluptates. Ut minus quia ipsam distinctio adipisci.', '2010', 'Modi nobis dolores perspiciatis maxime quia laudantium. Perferendis commodi libero tenetur aut. Quam voluptas fugiat doloremque in. Voluptatem quia possimus eum quod non qui. Beatae doloremque amet delectus repellat repellat itaque explicabo quidem. Doloribus facere saepe labore et voluptatum veniam. Ad officia necessitatibus cum eaque illum molestias aut. Dolore facere sit rem quae qui voluptas et quasi. Nulla minus facilis hic dolore nisi nemo.', '2015-08-22 20:47:54', '2015-08-23 00:59:05');
INSERT INTO `workshops` VALUES ('73', '28', 'Totam labore cum vero quia.', 'Architecto molestiae esse aut ad voluptatem quo fugit. Ut rem dolorem itaque rerum.', '1996', 'Laudantium provident deserunt facere quaerat. Perspiciatis sunt et id iusto blanditiis. Distinctio vero voluptatem in aut provident. Quia laudantium ea commodi dolorem. Sed accusantium veritatis consequuntur non. Odio eius nobis perferendis unde. Consectetur omnis ab et nihil qui quo. At qui quas numquam id ducimus. Et est accusamus magni blanditiis et illum. Reprehenderit rem aut molestiae ut.', '2015-08-23 10:08:31', '2015-08-12 22:35:38');
INSERT INTO `workshops` VALUES ('74', '43', 'Eligendi iure sint non.', 'Sed velit consectetur animi eos aut animi molestiae. Totam veritatis mollitia minus voluptates facilis dolore quaerat. Eveniet at amet tempora quas ipsum illum nihil.', '1998', 'Reiciendis neque incidunt sint tempora. Nulla molestiae vel quasi quia et veniam. In sunt dolorem tempora et. Ullam dolorum impedit quia error. Nemo et optio corrupti. Rem ut asperiores error rerum repudiandae. Quam sit totam dolorem repudiandae in autem. Odio qui voluptatem quibusdam eligendi veniam doloremque labore. Quam nostrum qui iusto delectus commodi.', '2015-08-27 18:48:28', '2015-08-28 12:17:21');
INSERT INTO `workshops` VALUES ('75', '7', 'Quo quasi reiciendis modi.', 'Consequuntur accusamus itaque ut facilis odio cum natus. Vel enim et fuga voluptatem rerum dolorum.', '2016', 'Qui vel quia delectus velit. Non qui sunt ut. Incidunt sint et rem temporibus. Culpa debitis placeat a possimus debitis omnis. Perspiciatis et aut dolores magni quae possimus. Aut soluta similique impedit voluptate. Libero aliquid aut voluptatibus ea. Quis laudantium accusantium velit voluptas et laboriosam ratione. Incidunt et fugiat neque asperiores odit. Nobis reprehenderit cumque necessitatibus sit ut fugit totam amet.', '2015-08-26 19:08:17', '2015-08-24 14:18:55');
INSERT INTO `workshops` VALUES ('76', '45', 'Consequatur sit aperiam omnis non illum.', 'Ut laudantium fugit qui quia officiis. Qui eaque natus dolores. Velit illum qui ipsa laboriosam.', '1997', 'Rerum ea quo aperiam sunt. Aliquam error culpa sapiente quidem ut nihil. Possimus repellendus quo voluptatem quibusdam. Exercitationem eum sed placeat. Dolorum sunt autem corporis dolores. Voluptas ex ad dolore et animi et iure. Distinctio et et exercitationem vitae. Quisquam sit alias non sit. Ipsa quia ab aut qui architecto voluptatem magnam. Illo soluta quia voluptatem. Repellat quam est qui vitae.', '2015-08-10 12:11:57', '2015-08-27 06:35:49');
INSERT INTO `workshops` VALUES ('77', '14', 'Qui unde nisi.', 'Qui cum itaque magni. Cumque voluptas vel enim nihil occaecati pariatur. Incidunt incidunt sed sit nemo consectetur veniam dolore.', '2016', 'Repellendus ut ad ut sint quia sint. Laboriosam ullam dolores ad provident nulla quo. Omnis hic voluptatem ut deserunt sunt. Quaerat qui ut et quis. Ad ut architecto molestiae. Perferendis aliquid dignissimos et at qui at. Facere dolores in quia hic consequatur eum. Est recusandae non totam enim quia delectus libero. Occaecati aut voluptatem velit soluta sit cum.', '2015-08-22 21:59:46', '2015-08-22 03:02:08');
INSERT INTO `workshops` VALUES ('78', '18', 'Culpa maxime accusantium tempora maxime autem.', 'Repudiandae eos est rerum nihil libero cumque vero. Explicabo eum totam eveniet quisquam cum placeat ut.', '2006', 'Officiis tempora alias ea qui rerum necessitatibus eum. Iusto quis earum necessitatibus hic eum. Porro harum voluptatem nobis quidem. Deleniti quia vitae error quia ea aliquam. Placeat quasi illo earum voluptas nisi minima. Minus velit voluptatibus corrupti reprehenderit omnis officiis est deleniti. Quisquam quis autem consectetur praesentium laudantium. Voluptatem repellat in nisi beatae minima.', '2015-08-16 06:37:20', '2015-09-07 14:31:07');
INSERT INTO `workshops` VALUES ('79', '37', 'Quos nisi non repudiandae.', 'Soluta praesentium consequatur vero placeat. Libero sit ipsa facere sit. Distinctio in voluptatem quas aut autem nobis.', '1994', 'Odio eius error voluptas magni sed similique. A totam dolorem eos fugiat vero. Cum rerum consectetur sunt sit. Nulla vero illum voluptas velit. Non culpa doloremque doloremque nihil ab eum assumenda esse. Quis doloremque laboriosam veritatis ullam. Officia nisi culpa qui laborum eum ut et. Autem voluptatem qui modi saepe occaecati.', '2015-08-16 23:45:48', '2015-09-06 09:13:00');
INSERT INTO `workshops` VALUES ('80', '13', 'Pariatur et et perspiciatis.', 'Error quidem ut molestiae iusto et. Ut et accusamus doloremque qui doloribus odio omnis.', '1995', 'Qui aut ut adipisci nesciunt qui. Non ut quod culpa adipisci omnis quia odio. Qui excepturi aut magnam ipsam dicta quos est. Sequi voluptatem esse et alias molestiae. Quia quia possimus rerum accusamus et error. Incidunt non dignissimos delectus. Voluptatibus quisquam nihil exercitationem quod ea quia molestiae. Qui illum nesciunt qui quasi. Ea laborum voluptas vel et. Aut laudantium iure quod debitis ea est quo. Tempora laboriosam dignissimos corrupti. Id consequuntur dicta beatae.', '2015-08-20 07:37:33', '2015-08-20 14:45:33');
INSERT INTO `workshops` VALUES ('81', '46', 'In magnam aut dolorem.', 'Dolorum id accusantium similique quibusdam maiores. Similique beatae id quaerat deserunt.', '2012', 'Facere unde doloribus qui provident ratione inventore excepturi labore. Velit aliquam id veniam sed. Dolorem at excepturi perspiciatis fuga. Perspiciatis quas dolore odit ullam quibusdam laborum. Deserunt non vel qui corporis. Qui facilis nostrum omnis asperiores optio non adipisci. Nemo dolore sunt eum voluptatem reprehenderit distinctio illum. Sunt aliquam placeat non eos minima cum. Velit voluptates accusantium ut sed quidem. Sit fugiat odit voluptates quia autem possimus. Minima totam natus vero iste dolorem. Tenetur ab aut incidunt tempora aliquid omnis consequuntur.', '2015-08-17 10:10:59', '2015-08-15 20:23:04');
INSERT INTO `workshops` VALUES ('82', '39', 'Expedita similique aliquid quas.', 'Officia mollitia perferendis velit sunt cum. Dignissimos ad qui aspernatur. Optio fuga et adipisci minus animi atque.', '2006', 'Facere sit et quo vel quisquam nemo officiis. Occaecati voluptatem aut sit in sed reprehenderit. Architecto voluptates in aut amet. Impedit ea quidem molestiae nam quia. Voluptatem officia accusantium consequatur incidunt. Ut aliquam ut quidem voluptates cumque. Et voluptatem soluta quo expedita dolor eos. Eligendi nihil ut ducimus ut harum eum in. Perferendis reprehenderit sequi quas blanditiis cumque vel.', '2015-08-25 13:26:00', '2015-08-09 20:36:17');
INSERT INTO `workshops` VALUES ('83', '29', 'Qui qui eligendi dolorem.', 'Culpa ad consequatur atque id et. Et reprehenderit nulla reprehenderit atque.', '1981', 'Ex necessitatibus aut aspernatur qui officiis quidem deleniti. Dolore modi quo explicabo fugiat dolores laboriosam incidunt. Aut earum est sed consequatur voluptates incidunt. Qui voluptatum qui vel blanditiis temporibus. Quia nisi esse quasi saepe. Perspiciatis porro numquam ut quia. Pariatur magnam inventore nisi et facere. Et natus aperiam autem placeat sint repudiandae qui. Culpa quibusdam blanditiis quia expedita quisquam. Nisi velit eos necessitatibus nostrum.', '2015-08-29 07:18:17', '2015-08-24 09:03:21');
INSERT INTO `workshops` VALUES ('84', '10', 'Iure qui sit hic.', 'Debitis eius dolor quae est et distinctio qui. Enim placeat rem ea impedit ea.', '2001', 'Voluptates enim dolorum illo numquam quae. Ducimus praesentium nisi iusto officiis nesciunt quia porro. Autem sint voluptatem necessitatibus voluptatem earum. Cupiditate provident suscipit error. Inventore cum sed quam repellat esse. Nulla molestiae exercitationem est voluptate itaque natus. Enim alias ut nemo voluptas consequatur tenetur voluptatem. Laborum blanditiis hic saepe architecto magnam. Consequatur voluptatem ullam voluptate dolores itaque est.', '2015-08-31 16:22:16', '2015-09-05 03:50:08');
INSERT INTO `workshops` VALUES ('85', '38', 'Odit ab et repellat.', 'Facere distinctio facere odit dolores et. Voluptatem cumque quia tempora dolorum numquam.', '1985', 'Similique dicta consequuntur ea accusamus. Quos necessitatibus recusandae quam eum iusto. Error et sunt quia et ullam cumque est. Voluptatem ullam nobis quia quas. Et accusamus a sapiente. Sapiente ut voluptatem et atque. Minus culpa quia eum ut voluptatibus ut. Mollitia quia explicabo reprehenderit placeat sint asperiores omnis quod. Impedit ut voluptas est est temporibus repellat consequatur. Ut illo eos dolores voluptatem. Consequatur ducimus quasi provident magni nemo animi consectetur. Doloribus praesentium sequi necessitatibus sequi qui repudiandae qui. Ipsam quis sequi nihil ipsa.', '2015-08-19 22:46:42', '2015-09-06 11:05:03');
INSERT INTO `workshops` VALUES ('86', '1', 'Quia asperiores repellat quidem possimus odio.', 'Qui velit dolore molestiae rerum. Sed voluptatem ut architecto nam.', '1985', 'Nostrum animi et possimus ipsum est doloribus sit. Temporibus quia consectetur tempora mollitia molestiae est nisi eum. Incidunt sunt sunt dolorem cumque autem. Tempore aperiam non non nulla. Qui enim et est dolor architecto ut ullam. Id fugiat asperiores dolore neque voluptatem odio quisquam. Quasi quisquam dolorem repellendus tenetur. Hic unde iste officia molestiae et repellat. Incidunt corporis exercitationem et voluptas ut architecto. Aut eos in ut alias illum.', '2015-09-02 13:14:55', '2015-08-29 21:15:16');
INSERT INTO `workshops` VALUES ('87', '33', 'Cupiditate aut non quia enim temporibus.', 'Labore in ipsum ipsam nihil in. Voluptatem neque aut ipsa adipisci ut et. Enim adipisci illo in officia hic velit dignissimos autem.', '1999', 'Facilis vel doloribus omnis et qui. Quibusdam velit suscipit eligendi vitae aliquid ut. Molestiae sunt et doloremque. Accusantium modi non nisi. Corrupti nobis sunt qui voluptates molestiae tempora. Aut optio officia quia consectetur quae. Sint assumenda ut quia voluptatem. Saepe non optio nulla enim aperiam aspernatur ipsam et. Dolores et sit sed non explicabo. Impedit qui veniam amet et quis aut laudantium. Sit quod omnis architecto quidem consequatur quo possimus omnis. Eligendi quasi numquam autem qui optio. Saepe nobis reprehenderit necessitatibus.', '2015-09-05 02:25:34', '2015-08-20 18:39:31');
INSERT INTO `workshops` VALUES ('88', '49', 'Vel necessitatibus enim.', 'Delectus vel quaerat eum nemo qui totam. Recusandae eius quia id vel voluptates et.', '1987', 'Tenetur sint quos quia et modi. Quaerat reprehenderit ipsam ducimus rerum dolores vitae. Totam suscipit voluptatem delectus qui. Qui maxime excepturi rerum omnis sit sint. Quisquam occaecati tempore quae quia quo sed rem qui. Sint modi dolores minus ad autem earum nesciunt quisquam. Exercitationem voluptas et magnam voluptatem tempore maiores maxime. Consequuntur dolores nostrum consequatur. Officiis eum eum nulla et. Sunt mollitia et quia ratione quod iusto alias. Corrupti adipisci saepe inventore.', '2015-08-25 07:51:22', '2015-08-23 21:23:55');
INSERT INTO `workshops` VALUES ('89', '50', 'Et qui officia dolor.', 'Nihil laudantium consequatur sit voluptatum voluptate minus. Est ut sed incidunt et qui libero qui. Nihil sint repellendus est qui quia perspiciatis non.', '2010', 'Repellat vel rerum consequatur laborum officiis perferendis. Quia et nam eum aliquam aliquam rem. Maiores unde exercitationem atque dolores id exercitationem. Vel sed hic sapiente et ipsam. Consectetur autem mollitia ratione maxime et. Soluta inventore incidunt animi voluptate qui non est. Est ut in quod aliquam. Nihil sunt voluptates aut omnis eum veritatis nihil. Quaerat ad repudiandae illum ea.', '2015-08-19 16:45:46', '2015-08-20 13:39:58');
INSERT INTO `workshops` VALUES ('90', '36', 'Perspiciatis tempora eaque sequi optio.', 'Quam et molestias aut qui id eaque. Quisquam dolores aut doloribus esse quam tempora quam. Sunt enim inventore nihil.', '1984', 'Et et ut non et officiis omnis. Fuga voluptas commodi voluptatem tempore explicabo perferendis voluptas. Maxime cumque voluptatem minima inventore fuga nihil tenetur. Inventore dicta beatae quasi velit dolor molestias. Sit sed consequatur magni. Accusamus assumenda mollitia a minus quo est. Corrupti nihil ex eveniet nobis tenetur culpa. Qui earum et molestias nemo assumenda laboriosam laborum. Soluta error sed omnis voluptas esse. Minima voluptatem saepe ea rerum. Eius nostrum blanditiis nesciunt aut. Iusto aut debitis et facere excepturi reiciendis est. Quibusdam qui et illo quo et. Quibusdam occaecati excepturi illum nam.', '2015-08-31 23:18:34', '2015-09-06 10:22:07');
INSERT INTO `workshops` VALUES ('91', '41', 'Qui tempore reiciendis dolores similique.', 'Non quaerat atque suscipit. Exercitationem unde aut optio optio impedit sint ea. Sit consequatur quaerat itaque.', '2002', 'Ut voluptatibus consectetur occaecati sunt voluptatem velit. Aut omnis sunt assumenda. Iusto veritatis esse aut ut qui est. Similique qui doloremque eum similique tempore illum eligendi. Asperiores et dolore ipsam quisquam cumque enim dolor. Nihil beatae ut omnis illum consequatur. Animi consequatur voluptatibus optio tempora autem. Ipsam modi ut ab fuga ut quaerat quaerat. Cumque architecto error mollitia nisi.', '2015-08-16 17:00:59', '2015-09-06 23:52:36');
INSERT INTO `workshops` VALUES ('92', '30', 'Nobis iste hic sunt inventore.', 'Nesciunt ut impedit et in ea nihil cupiditate. Aspernatur occaecati dolor libero dolorem temporibus quidem. Quisquam ut dolores expedita.', '1983', 'Nihil recusandae a eveniet libero optio vitae ut. Enim facilis inventore vero voluptas. Error quia dolor ea quo. Voluptas aliquid ducimus asperiores asperiores asperiores quae. Saepe modi veniam qui tempora. Omnis unde sequi dignissimos sunt. Harum tenetur tempore sunt dignissimos iste ut unde.', '2015-08-29 14:10:04', '2015-08-31 10:31:03');
INSERT INTO `workshops` VALUES ('93', '29', 'Natus illum suscipit eius minima.', 'Omnis dolor est deleniti vel impedit reprehenderit. Ullam qui eligendi molestias a est velit quo.', '2015', 'Et veritatis modi ut ipsa sint. Atque illum ut et voluptates. Veritatis voluptas quis omnis aut at ut adipisci. Fugiat in quibusdam qui dolorum omnis necessitatibus delectus. Ratione harum voluptas ut adipisci cumque non. Rerum et rerum nulla corporis aliquam. Et sit illo dolorem quia. Laboriosam sunt deleniti quis sed fuga id commodi. Voluptatem earum qui expedita in molestias qui porro quia. Libero maiores occaecati sunt excepturi magni. Molestiae accusantium magnam et quasi consectetur temporibus. Voluptatum eius qui ex sint ullam. A eligendi asperiores voluptatem aliquid aliquam voluptates recusandae. Ut earum et totam magnam id dolore sunt explicabo.', '2015-08-27 00:35:09', '2015-08-14 02:36:10');
INSERT INTO `workshops` VALUES ('94', '41', 'Expedita molestiae officiis id non.', 'Cum consequatur earum quam deleniti. Magni quia veritatis corrupti est. Illo ut rem maxime sit autem.', '1991', 'Enim officiis a sapiente ut laborum. Aut rerum eaque at officia expedita. Sed esse officiis aut numquam. Saepe tenetur tenetur nostrum magni et commodi. Qui quisquam veniam aut. Tenetur ea id omnis et dolor rerum fugiat. Est placeat repudiandae reprehenderit labore temporibus quae est est. Ipsa consequatur velit ullam ut quidem. Sapiente dolores est qui quia minus architecto. Incidunt rem dolorum fugiat. Odit eos qui velit eos itaque. Iste rerum tempora molestiae consequuntur.', '2015-09-06 00:11:22', '2015-09-02 17:09:05');
INSERT INTO `workshops` VALUES ('95', '18', 'Distinctio qui id tempora.', 'Sed qui in architecto. Voluptatem quia dolore iste non doloremque sit cupiditate. Reiciendis repudiandae eligendi velit eveniet architecto ullam quod.', '1982', 'Similique tempora ut quam odio voluptatibus sequi. Minima tenetur possimus debitis ut in. Ea at aut sint dolor sit atque praesentium. In harum ut sed optio soluta. Esse autem sequi recusandae. Rerum sit qui voluptas nulla ut veritatis vel. Tempore nostrum voluptatem omnis recusandae nobis. Fuga perspiciatis non consequatur minima accusamus tenetur impedit et. Molestiae doloremque ullam natus nemo est aut quam. Omnis repellat inventore molestias autem et. Nostrum eveniet consectetur aspernatur quo. Expedita rerum deserunt dignissimos et fugit.', '2015-08-30 01:35:56', '2015-09-03 05:48:27');
INSERT INTO `workshops` VALUES ('96', '37', 'Distinctio culpa accusamus omnis soluta.', 'Quaerat ipsa animi quaerat maxime dolorum corporis. Sint aliquam rerum tenetur quia.', '1989', 'Facere ut magni vitae velit fuga ut et. Eum voluptatibus culpa eos distinctio voluptas. Excepturi itaque sint quo deleniti ipsam consequatur quibusdam. Reiciendis quos deleniti eius nostrum esse. Est esse ex aut voluptates asperiores. Harum distinctio hic odio qui autem sint itaque repudiandae. Fuga modi quibusdam labore aut doloribus quis. Perferendis natus facilis ut et. Vel illo ut ipsam et et ipsum. Eum totam placeat consequatur.', '2015-08-25 22:02:18', '2015-08-09 09:01:31');
INSERT INTO `workshops` VALUES ('97', '48', 'Omnis magnam a incidunt quis facere.', 'Repellat omnis ut quae explicabo est. Temporibus aut dolorem dicta laborum ut. Neque hic velit itaque deleniti.', '1998', 'Voluptatem accusamus distinctio incidunt temporibus enim nulla. Dolorum dolor placeat in vel quis impedit. Id consectetur aspernatur ipsam. Pariatur soluta sed illum aliquid ad molestias. Qui quisquam ab nihil laboriosam quod quaerat sit. Iure tempora nisi rerum sit nihil voluptas expedita voluptas. Neque blanditiis ut enim sunt facilis quia ea et. Eveniet consectetur et officiis et mollitia.', '2015-08-28 17:52:26', '2015-09-05 08:20:28');
INSERT INTO `workshops` VALUES ('98', '49', 'Aut at quia.', 'Ab aspernatur odio velit veniam eius omnis aspernatur. Adipisci eos praesentium ut nobis ex rerum enim et.', '1999', 'Et aut voluptatibus magni eveniet. Consectetur a in aut accusamus illo. Pariatur ut eveniet autem illum. Officiis ducimus aspernatur vitae porro assumenda. Veniam nemo temporibus vero vel non ut. Iste illo ipsam enim quia suscipit. Blanditiis nam nobis enim laboriosam. Pariatur rerum laboriosam et laboriosam. Officiis commodi sunt illum dolor et ut. Minus neque sunt velit fuga.', '2015-08-10 19:13:53', '2015-08-25 04:07:49');
INSERT INTO `workshops` VALUES ('99', '27', 'Accusantium illo et iste eius optio.', 'Consequatur sunt nihil numquam perspiciatis optio minus dicta. Distinctio doloremque similique earum accusantium ad corporis. Enim consequatur doloribus nemo.', '1986', 'Repudiandae aut dolorem est nisi quia. Veritatis tempora nobis excepturi fugiat necessitatibus. Quisquam aliquam reiciendis consequatur minus laborum. Dolorem quisquam ipsam repellat architecto. Incidunt exercitationem aut est dolores voluptates dolor velit recusandae. Voluptatem sit voluptatem distinctio excepturi et dicta veniam. Est odio enim non quas. In illo nostrum eligendi. Rem et ut cumque dolor. Quia explicabo ad laborum quasi natus eligendi.', '2015-08-27 15:30:12', '2015-08-17 01:05:47');
INSERT INTO `workshops` VALUES ('100', '6', 'Voluptas consectetur voluptatibus corporis dolor.', 'Nulla perferendis eaque porro consequatur dignissimos ratione. Quibusdam tempore ex soluta non a sunt.', '1990', 'Necessitatibus reprehenderit non ut rerum sequi aperiam. Dignissimos omnis omnis commodi possimus illo impedit reprehenderit et. Et excepturi quis nihil aut dolores dolorum id et. Accusantium voluptas aut incidunt sint voluptas natus cumque. Modi cupiditate placeat dignissimos et totam delectus enim sit. Eligendi dolorem adipisci vitae autem. Ut nulla quas voluptas tenetur aliquam iusto a. Omnis id voluptatem cum vero laudantium autem voluptas. Maxime vitae eligendi ut libero non assumenda aspernatur aut. Voluptatum porro id dicta dolorem vero. Porro sit harum repudiandae veritatis illum eius et. Minima voluptas id libero qui.', '2015-08-18 09:50:00', '2015-08-18 17:21:39');

-- ----------------------------
-- Table structure for youtubes
-- ----------------------------
DROP TABLE IF EXISTS `youtubes`;
CREATE TABLE `youtubes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `youtubes_user_id_index` (`user_id`),
  CONSTRAINT `youtubes_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of youtubes
-- ----------------------------
INSERT INTO `youtubes` VALUES ('1', '48', 'HhqRYuH5I1I', '2015-08-29 02:57:39', '2015-08-20 08:00:35');
INSERT INTO `youtubes` VALUES ('2', '20', 'HhqRYuH5I1I', '2015-08-11 15:29:21', '2015-08-11 10:08:17');
INSERT INTO `youtubes` VALUES ('3', '37', 'UNy5v55UYmk', '2015-08-31 21:41:43', '2015-08-23 04:46:33');
INSERT INTO `youtubes` VALUES ('4', '30', 'UNy5v55UYmk', '2015-08-25 17:28:06', '2015-08-16 18:38:29');
INSERT INTO `youtubes` VALUES ('5', '45', 'HhqRYuH5I1I', '2015-09-04 08:31:01', '2015-09-04 04:14:35');
INSERT INTO `youtubes` VALUES ('6', '41', 'IkYFwsrMj5s', '2015-08-16 15:52:55', '2015-08-20 23:12:20');
INSERT INTO `youtubes` VALUES ('7', '17', 'UNy5v55UYmk', '2015-09-04 04:34:06', '2015-08-26 01:23:05');
INSERT INTO `youtubes` VALUES ('8', '37', 'HhqRYuH5I1I', '2015-09-02 23:44:06', '2015-08-19 14:24:24');
INSERT INTO `youtubes` VALUES ('9', '14', 'UNy5v55UYmk', '2015-08-17 08:25:06', '2015-09-01 12:02:04');
INSERT INTO `youtubes` VALUES ('10', '32', 'IU7vpt9EUFU', '2015-08-28 11:03:46', '2015-08-24 20:06:20');
INSERT INTO `youtubes` VALUES ('11', '36', 'UNy5v55UYmk', '2015-09-05 08:25:13', '2015-09-01 10:25:59');
INSERT INTO `youtubes` VALUES ('12', '15', 'IkYFwsrMj5s', '2015-08-26 20:42:15', '2015-09-02 18:27:35');
INSERT INTO `youtubes` VALUES ('13', '32', 'itDE5vfYQUA', '2015-08-13 14:11:50', '2015-09-07 22:59:33');
INSERT INTO `youtubes` VALUES ('14', '50', 'nI01bAB9SjM', '2015-08-25 05:24:37', '2015-08-19 22:06:18');
INSERT INTO `youtubes` VALUES ('15', '33', 'nI01bAB9SjM', '2015-08-28 20:21:19', '2015-09-05 00:13:24');
INSERT INTO `youtubes` VALUES ('16', '31', 'UNy5v55UYmk', '2015-09-02 01:16:22', '2015-09-06 00:12:14');
INSERT INTO `youtubes` VALUES ('17', '39', 'cHQdcQn6fb8', '2015-08-19 06:00:44', '2015-09-08 18:15:06');
INSERT INTO `youtubes` VALUES ('18', '29', 'nI01bAB9SjM', '2015-08-21 07:32:25', '2015-09-06 10:05:55');
INSERT INTO `youtubes` VALUES ('19', '42', 'itDE5vfYQUA', '2015-08-26 23:55:38', '2015-08-14 11:53:50');
INSERT INTO `youtubes` VALUES ('20', '4', 'UNy5v55UYmk', '2015-08-26 00:47:42', '2015-08-11 07:46:40');
INSERT INTO `youtubes` VALUES ('21', '37', 'fXgij4SOtXc', '2015-08-15 03:22:14', '2015-08-28 07:37:49');
INSERT INTO `youtubes` VALUES ('22', '35', 'IkYFwsrMj5s', '2015-08-16 17:28:58', '2015-09-03 18:50:59');
INSERT INTO `youtubes` VALUES ('23', '22', 'itDE5vfYQUA', '2015-08-18 02:16:55', '2015-09-07 03:55:14');
INSERT INTO `youtubes` VALUES ('24', '27', 'HhqRYuH5I1I', '2015-08-15 13:37:28', '2015-08-17 11:36:00');
INSERT INTO `youtubes` VALUES ('25', '15', 'IkYFwsrMj5s', '2015-08-27 07:08:09', '2015-08-30 23:20:06');
INSERT INTO `youtubes` VALUES ('26', '18', 'nI01bAB9SjM', '2015-08-16 03:14:45', '2015-08-17 17:55:58');
INSERT INTO `youtubes` VALUES ('27', '40', 'itDE5vfYQUA', '2015-08-27 22:01:09', '2015-08-17 04:54:01');
INSERT INTO `youtubes` VALUES ('28', '45', 'fXgij4SOtXc', '2015-08-17 02:53:56', '2015-08-17 15:55:45');
INSERT INTO `youtubes` VALUES ('29', '18', 'HhqRYuH5I1I', '2015-08-14 05:58:50', '2015-08-21 19:17:25');
INSERT INTO `youtubes` VALUES ('30', '26', 'fXgij4SOtXc', '2015-08-12 23:39:24', '2015-09-07 10:42:30');
INSERT INTO `youtubes` VALUES ('31', '11', 'UNy5v55UYmk', '2015-08-19 20:33:25', '2015-08-16 02:51:37');
INSERT INTO `youtubes` VALUES ('32', '29', 'itDE5vfYQUA', '2015-08-28 17:07:33', '2015-09-06 04:37:39');
INSERT INTO `youtubes` VALUES ('33', '32', 'fXgij4SOtXc', '2015-08-24 16:00:00', '2015-09-06 08:46:01');
INSERT INTO `youtubes` VALUES ('34', '21', 'IkYFwsrMj5s', '2015-08-09 15:24:46', '2015-09-01 11:29:14');
INSERT INTO `youtubes` VALUES ('35', '39', 'cHQdcQn6fb8', '2015-08-16 10:20:28', '2015-09-02 09:20:13');
INSERT INTO `youtubes` VALUES ('36', '26', 'UNy5v55UYmk', '2015-08-26 21:47:45', '2015-08-17 14:18:57');
INSERT INTO `youtubes` VALUES ('37', '14', 'UNy5v55UYmk', '2015-09-03 22:52:01', '2015-08-12 01:59:41');
INSERT INTO `youtubes` VALUES ('38', '18', 'cHQdcQn6fb8', '2015-08-31 03:23:13', '2015-08-21 11:17:50');
INSERT INTO `youtubes` VALUES ('39', '50', 'cHQdcQn6fb8', '2015-08-10 02:06:37', '2015-08-10 00:46:15');
INSERT INTO `youtubes` VALUES ('40', '21', 'nI01bAB9SjM', '2015-08-27 22:44:46', '2015-08-11 09:38:24');
INSERT INTO `youtubes` VALUES ('41', '22', 'fXgij4SOtXc', '2015-08-11 07:06:48', '2015-08-28 10:47:01');
INSERT INTO `youtubes` VALUES ('42', '39', 'IU7vpt9EUFU', '2015-08-27 07:17:40', '2015-08-14 09:26:05');
INSERT INTO `youtubes` VALUES ('43', '4', 'IU7vpt9EUFU', '2015-09-07 00:29:03', '2015-08-23 18:18:01');
INSERT INTO `youtubes` VALUES ('44', '21', 'nI01bAB9SjM', '2015-09-05 07:08:18', '2015-09-02 18:57:20');
INSERT INTO `youtubes` VALUES ('45', '24', 'IkYFwsrMj5s', '2015-08-22 09:14:55', '2015-08-09 10:46:46');
INSERT INTO `youtubes` VALUES ('46', '41', 'cHQdcQn6fb8', '2015-09-04 20:11:03', '2015-08-30 04:59:31');
INSERT INTO `youtubes` VALUES ('47', '47', 'UNy5v55UYmk', '2015-09-05 23:39:12', '2015-08-26 19:11:54');
INSERT INTO `youtubes` VALUES ('48', '15', 'itDE5vfYQUA', '2015-08-25 03:11:10', '2015-09-03 14:03:25');
INSERT INTO `youtubes` VALUES ('49', '25', 'IkYFwsrMj5s', '2015-08-19 03:14:32', '2015-08-26 01:34:54');
INSERT INTO `youtubes` VALUES ('50', '45', 'UNy5v55UYmk', '2015-08-13 09:01:11', '2015-08-18 02:10:11');
