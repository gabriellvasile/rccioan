<?php

use Illuminate\Database\Eloquent\Model;
use Faker\Factory as Faker;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LanguagePageTableSeeder extends Seeder {

	/**
	 * Run the PageTable seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$faker = Faker::create();
		DB::table('language_page')->insert([
			[
				'page_id' => 1,
				'language_id' => 1,
				'slug' => '/',
				'title' => 'Romania Casting Call',
				'content' => '"Industria media a crescut considerabil in ultimii ani, mai ales in domenii precum film si televiziune, care lanseaza productii si formate din ce in ce mai premiate in strainatate. Chiar si asa, mai avem mult pana sa atingem nivelul de profesionalism occidental. Unul dintre lucrurile esentiale care lipsesc de pe piata media din Romania, este o baza online de casting profesionala, dupa model occidental, care sa acopere multiple domenii din industria media, oferind astfel o paleta mult mai larga de job-uri.',
				'excerpt' => 'Romania Casting Call vine in ajutorul tuturor celor talentati si inca nedescoperiti',
				'created_at' => $faker->dateTimeThisMonth(),
				'updated_at' => $faker->dateTimeThisMonth()
			],
			[
				'page_id' => 2,
				'language_id' => 1,
				'slug' => 'politica-de-confidentialitate',
				'title' => 'Politica de confidentialitate',
				'content' => 'Ubi est superbus imber?Never know the lama, for you cannot facilitate it.Simmer packaged garlics in a grinder with ice water for about an hour to cut their asperity.Aye, jolly endurance!Human transformators, to the solar sphere.',
				'excerpt' => '',
				'created_at' => $faker->dateTimeThisMonth(),
				'updated_at' => $faker->dateTimeThisMonth()
			],
			[
				'page_id' => 3,
				'language_id' => 1,
				'slug' => 'termeni-si-conditii',
				'title' => 'Termeni si conditii',
				'content' => 'Termeni si conditii.... A great form of harmony is the career. Wrestle and you will be feared essentially.Salvus, raptus lixas mechanice captis de fatalis, teres valebat.',
				'excerpt' => '',
				'created_at' => $faker->dateTimeThisMonth(),
				'updated_at' => $faker->dateTimeThisMonth()
			],
			[
				'page_id' => 4,
				'language_id' => 1,
				'slug' => 'reguli-de-selectie',
				'title' => 'Reguli de selectie',
				'content' => 'Text reguli de selectie. A great form of harmony is the career. Wrestle and you will be feared essentially.Salvus, raptus lixas mechanice captis de fatalis, teres valebat.',
				'excerpt' => '',
				'created_at' => $faker->dateTimeThisMonth(),
				'updated_at' => $faker->dateTimeThisMonth()
			],
			[
				'page_id' => 5,
				'language_id' => 1,
				'slug' => 'despre',
				'title' => 'Despre',
				'content' => 'Despre Romania Casting Call',
				'excerpt' => '',
				'created_at' => $faker->dateTimeThisMonth(),
				'updated_at' => $faker->dateTimeThisMonth()
			],
			[
				'page_id' => 6,
				'language_id' => 1,
				'slug' => 'insula-actorilor',
				'title' => 'Insula Actorilor',
				'content' => '',
				'excerpt' => '',
				'created_at' => $faker->dateTimeThisMonth(),
				'updated_at' => $faker->dateTimeThisMonth()
			],
			[
				'page_id' => 7,
				'language_id' => 1,
				'slug' => 'castinguri',
				'title' => 'Castinguri',
				'content' => '�n cautare de joburi',
				'excerpt' => '',
				'created_at' => $faker->dateTimeThisMonth(),
				'updated_at' => $faker->dateTimeThisMonth()
			],
			[
				'page_id' => 8,
				'language_id' => 1,
				'slug' => 'ambasadori',
				'title' => 'Ambasadori',
				'content' => '',
				'excerpt' => '',
				'created_at' => $faker->dateTimeThisMonth(),
				'updated_at' => $faker->dateTimeThisMonth()
			],
			[
				'page_id' => 9,
				'language_id' => 1,
				'slug' => 'contact',
				'title' => 'Contact',
				'content' => 'Pentru orice alte informatii suplimentare, va rugam sa completati formularul de mai jos. <br> Vom reveni cu un r?spuns �n cel mai scurt timp!',
				'excerpt' => 'Pentru consultanta in completarea oricarui profil, va rugam sa completati acest formular si vom reveni in cel mai scurt timp!',
				'created_at' => $faker->dateTimeThisMonth(),
				'updated_at' => $faker->dateTimeThisMonth()
			],
			[
				'page_id' => 10,
				'language_id' => 1,
				'slug' => 'insula-actorilor/actori',
				'title' => 'Listare actori',
				'content' => '',
				'excerpt' => 'Actori',
				'created_at' => $faker->dateTimeThisMonth(),
				'updated_at' => $faker->dateTimeThisMonth()
			],
			[
				'page_id' => 11,
				'language_id' => 1,
				'slug' => 'insula-actorilor/actrite',
				'title' => 'Listare actrite',
				'content' => '',
				'excerpt' => 'Actrite',
				'created_at' => $faker->dateTimeThisMonth(),
				'updated_at' => $faker->dateTimeThisMonth()
			],
			[
				'page_id' => 12,
				'language_id' => 1,
				'slug' => 'insula-actorilor/studenti',
				'title' => 'Listare studenti',
				'content' => '',
				'excerpt' => 'Studenti',
				'created_at' => $faker->dateTimeThisMonth(),
				'updated_at' => $faker->dateTimeThisMonth()
			],
			[
				'page_id' => 13,
				'language_id' => 1,
				'slug' => 'insula-actorilor/actorul-lunii',
				'title' => 'Actorul lunii',
				'content' => '',
				'excerpt' => 'Actorul lunii',
				'created_at' => $faker->dateTimeThisMonth(),
				'updated_at' => $faker->dateTimeThisMonth()
			],
			[
				'page_id' => 14,
				'language_id' => 1,
				'slug' => 'insula-actorilor/filtru',
				'title' => 'Filtreaza',
				'content' => '',
				'excerpt' => 'Filtreaza actori',
				'created_at' => $faker->dateTimeThisMonth(),
				'updated_at' => $faker->dateTimeThisMonth()
			],


			[
				'page_id' => 1,
				'language_id' => 2,
				'slug' => 'en',
				'title' => 'Romania Casting Call',
				'content' => '"The media industry has been on the rise, etc etc etc in english',
				'excerpt' => 'Romania Casting Call the talented and undiscovered',
				'created_at' => $faker->dateTimeThisMonth(),
				'updated_at' => $faker->dateTimeThisMonth()
			],
			[
				'page_id' => 2,
				'language_id' => 2,
				'slug' => 'cookies-policy',
				'title' => 'Cookies policy',
				'content' => 'This site uses cookies. We\'re awesome.',
				'excerpt' => '',
				'created_at' => $faker->dateTimeThisMonth(),
				'updated_at' => $faker->dateTimeThisMonth()
			],
			[
				'page_id' => 3,
				'language_id' => 2,
				'slug' => 'terms-and-conditions',
				'title' => 'Terms & conditions',
				'content' => 'Terms and conditions. A great form of harmony is the career. Wrestle and you will be feared essentially.Salvus, raptus lixas mechanice captis de fatalis, teres valebat.',
				'excerpt' => '',
				'created_at' => $faker->dateTimeThisMonth(),
				'updated_at' => $faker->dateTimeThisMonth()
			],
			[
				'page_id' => 4,
				'language_id' => 2,
				'slug' => 'selection-rules',
				'title' => 'Selection rules',
				'content' => 'Text selection rules. A great form of harmony is the career. Wrestle and you will be feared essentially.Salvus, raptus lixas mechanice captis de fatalis, teres valebat.',
				'excerpt' => '',
				'created_at' => $faker->dateTimeThisMonth(),
				'updated_at' => $faker->dateTimeThisMonth()
			],
			[
				'page_id' => 5,
				'language_id' => 2,
				'slug' => 'about',
				'title' => 'About',
				'content' => 'About Romania Casting Call',
				'excerpt' => '',
				'created_at' => $faker->dateTimeThisMonth(),
				'updated_at' => $faker->dateTimeThisMonth()
			],
			[
				'page_id' => 6,
				'language_id' => 2,
				'slug' => 'actorland',
				'title' => 'Actorland',
				'content' => '',
				'excerpt' => '',
				'created_at' => $faker->dateTimeThisMonth(),
				'updated_at' => $faker->dateTimeThisMonth()
			],
			[
				'page_id' => 7,
				'language_id' => 2,
				'slug' => 'castings',
				'title' => 'Castings',
				'content' => 'Looking for a casting',
				'excerpt' => '',
				'created_at' => $faker->dateTimeThisMonth(),
				'updated_at' => $faker->dateTimeThisMonth()
			],
			[
				'page_id' => 8,
				'language_id' => 2,
				'slug' => 'ambassadors',
				'title' => 'Ambassadors',
				'content' => '',
				'excerpt' => '',
				'created_at' => $faker->dateTimeThisMonth(),
				'updated_at' => $faker->dateTimeThisMonth()
			],
			[
				'page_id' => 9,
				'language_id' => 2,
				'slug' => 'contact-us',
				'title' => 'Contact',
				'content' => 'For any other enquires, please use the next form. <br> We\'ll get back to you as soon as possible!',
				'excerpt' => 'If you need help in filling your profile, please use contact us using this form and we\'ll get back tot you as soon as possible!',
				'created_at' => $faker->dateTimeThisMonth(),
				'updated_at' => $faker->dateTimeThisMonth()
			],
			[
				'page_id' => 10,
				'language_id' => 2,
				'slug' => 'actorland/actors',
				'title' => 'Actors listing',
				'content' => '',
				'excerpt' => 'Actors',
				'created_at' => $faker->dateTimeThisMonth(),
				'updated_at' => $faker->dateTimeThisMonth()
			],
			[
				'page_id' => 11,
				'language_id' => 2,
				'slug' => 'actorland/actresses',
				'title' => 'Actresses listing',
				'content' => '',
				'excerpt' => 'Actresses',
				'created_at' => $faker->dateTimeThisMonth(),
				'updated_at' => $faker->dateTimeThisMonth()
			],
			[
				'page_id' => 12,
				'language_id' => 2,
				'slug' => 'actorland/students',
				'title' => 'Students listing',
				'content' => '',
				'excerpt' => 'Students',
				'created_at' => $faker->dateTimeThisMonth(),
				'updated_at' => $faker->dateTimeThisMonth()
			],
			[
				'page_id' => 13,
				'language_id' => 2,
				'slug' => 'actorland/monthly-actor',
				'title' => 'Actor of the month',
				'content' => '',
				'excerpt' => 'Monthy actors',
				'created_at' => $faker->dateTimeThisMonth(),
				'updated_at' => $faker->dateTimeThisMonth()
			],
			[
				'page_id' => 14,
				'language_id' => 2,
				'slug' => 'actorland/filter',
				'title' => 'Filter',
				'content' => '',
				'excerpt' => 'Filter actors',
				'created_at' => $faker->dateTimeThisMonth(),
				'updated_at' => $faker->dateTimeThisMonth()
			],
		]);
	}

}