<?php

use Faker\Factory as Faker;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class JobsTableSeeder extends Seeder {

	/**
	 * Run the PageTable seeds.
	 *
	 * @return void
	 */
	public function run() {
		$production_name   =
			array_flip(['Reclama video Romania Casting Call', 'Produs pentru ingrijirea parului', 'Love building', 'HaHaHa Video Production']);
		$short_description =
			array_flip(['Dupa cum probabil stiti, Romania Casting Call s-a lansat de curand. A inceput sa creasca usor usor, insa pentru a ajunge la urechile tuturor, are nevoie de o reclama (TV si Radio). Avem nevoie de o voce, insa nu cautam un stil anumite sau un sex. Dorim cat mai multa diversitate, asadar va asteptam!', 'Filmarea a doua spoturi publicitare, unul in data pe 4.11.2014, celalalt pe 5.11.2014. Dat fiind faptul ca proiectul este unul de tip testimonial, numele produsului va fi dezvalui in ziua filmarii.', 'Vrei sa participi la cele mai tari proiecte video si sa fii alaturi de artistii tai preferati? HaHaHa Video Production cauta absolventi de actorie sau studenti la o facultate de profil care vor sa participe la viitoarele proiecte video realizate pentru cei mai cunoscuti artisti romani si nu numai.', 'Castalia Productions se lanseaza in aceasta vara cu o productie romano-britanica, si anume scurtmetrajul intitulat "10 ore" . Echipa este formata numai din oameni profesionisti, cunoscuti pe piata romaneasca, si completata de prezenta catorva studenti britanici de la facultati de profil londoneze. "10 ore" urmareste povestea captivanta a Mariei, o fetita de 10-11 ani, care provine dintr-o familie de oameni instariti si ocupati. Tatal ei, un extraordinar om de afaceri in telecomunicatii, are din ce in ce mai putin timp pentru ea, fapt ce ii produce Mariei o mare suferinta. Din lipsa de atentie, dar si din dragoste fata de tatal ei, Maria porneste intr-o aventura plina de obstacole, sperand sa il faca pe tatal sau sa realizeze importanta afectiunii si atentiei unui parinte pentru un copil care nu isi doreste jucarii si tablete, pe cat de mult isi doreste un tata aproape de ea. O emotionanta drama, cu elemente comice, care transmite un mesaj actual si simplu, menit sa deschida ochii parintilor care isi neglijeaza copiii din cauza job-ului. Acest scurtmetraj va fi trimis la toate festivalurile importante, printre ele numarandu-se si Cannes / Sundance / BFI etc.', '10 ore" urmareste povestea captivanta a Mariei, o fetita de 10-11 ani, care provine dintr-o familie de oameni instariti si ocupati. Tatal ei, un extraordinar om de afaceri in telecomunicatii, are din ce in ce mai putin timp pentru ea, fapt ce ii produce Mariei o mare suferinta. Din lipsa de atentie, dar si din dragoste fata de tatal ei, Maria pleaca de acasa, lasandu-i tatalui sau un bilet, prin care il informeaza de plecarea sa si ii pune la dispozitie 10 ore pentru a o gasi. Astfel, Maria porneste intr-o aventura plina de obstacole, sperand sa il faca pe tatal sau sa realizeze importanta afectiunii si atentiei unui parinte pentru un copil care nu isi doreste jucarii si tablete, pe cat de mult isi doreste un tata aproape de ea. O emotionanta drama, cu elemente comice, care transmite un mesaj actual si simplu, menit sa deschida ochii parintilor care isi neglijeaza copiii din cauza job-ului.']);

		$telephone     = array_flip(['0758080821', '0740.121.864', '0740300290']);
		$email         =
			array_flip(['office@romaniacastingcall.com', 'rionita@sagafilm.ro', 'laura@office.ro', 'pr@hahahaproduction.com', 'laura.musat@romaniacastingcall.com', 'laura.musat10@gmail.com']);

		$faker        = Faker::create('ro_RO');
		$toBeInserted = [];

		foreach (range(1, 30) as $index) {
			$toBeInserted[] = [
				'production_name'    => array_rand($production_name),
				'actor_type_id'      => rand(1, 3),
				'production_type_id' => rand(1, 7),
				'location'           => 'Bucuresti',
				'from_date_shooting' => $faker->dateTimeThisYear(),
				'to_date_shooting'   => $faker->dateTimeThisYear(),
				'from_date_casting'  => $faker->dateTimeThisYear(),
				'to_date_casting'    => $faker->dateTimeThisMonth(),
				'employer_id'        => rand(1, 25),
				'short_description'  => array_rand($short_description),
				'student_project'    => rand(0, 1),

				'payment'            => rand(0, 1),
				'telephone'          => array_rand($telephone),
				'notifying_method'   => rand(0, 1),
				'exclusive'          => rand(0, 1),
				'university'         => '',
				'tutor_name'         => '',
				'department'         => '',
				'user_id'            => rand(6, 8),
				'email'              => array_rand($email),

				'guest_visible'      => rand(0, 1),
				'status'             => rand(0, 1),
				'complete'           => rand(0, 1),
				'approved'           => rand(0, 1),

				'created_at'         => $faker->dateTimeThisMonth(),
				'updated_at'         => $faker->dateTimeThisMonth(),
			];
		}

		DB::table('jobs')->insert(
			$toBeInserted
		);
	}

}