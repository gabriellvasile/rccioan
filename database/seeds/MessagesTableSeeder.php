<?php

use Illuminate\Database\Eloquent\Model;
use Faker\Factory as Faker;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MessagesTableSeeder extends Seeder {

	/**
	 * Run the PageTable seeds.
	 *
	 * @return void
	 */
	public function run() {
		$message =
			array_flip(['Salut,<br><br>Pai, daca beneficiarul este persoana juridica, am nevoie de detaliile pentru factura: CUI, J etc.<br>Deasemenea, putem vorbi direct prin mihail@somanescu.com si la 0745 386 971.<br><br>Multam', 'Spune-mi unde iti trimit banii? O adresa si un numar de telefon.<br><br>Merci frumos,<br>Laura', 'Absolut!', 'Ok, atunci iti trimit materialul de inregistrat si tot ce iti trebuie si ne trimiti tu 3 variante. Cred ca marti iti trimit. Poate fi gata pana vineri dimineata?<br><br>Merci', 'In seara asta (scuze, am dat send).<br>Sau, putem duminica.:)', 'Salut,<br><br>Din punctul meu de vedere pot inregistra aproape oricand, asta daca trag vocea la mine.<br>Eu propun urmatoarea varianta: imi trimiteti materialul de inregistrat, detaliile de factura si contract si pot livra 3 variante chiar&nbsp;', 'Buna Mihai,<br><br>Scuza-ma te rog, am avut un casting de dimineata la care am fost chemata in ultimul moment.<br>Spune-mi, te rog, ai putea oricand pana miercuri? Sau o lasam pe duminica viitoare?<br><br>Haide sa stabilim exact ca sa fie totul in regula.<br><br>Cu drag', 'Buna dimineata,<br><br>Costul per spot radio este de 150 Euro, studious, daca il folosim facilitatile mele, este inclus.<br>Maine seara e OK la mine, daca e vorba de un alt loc, incepand cu 12 sunt disponibil.<br><br>Multam,<br>Mihail', 'Si s-ar putea inregistra acolo maine dimineata sau seara, pe la un 6:30?<br><br>Care sunt costurile de inchiriere? Precum si ale tale de V.O? Vorbim de un spot de 20 secunde de radio', 'Eu am propriu setup unde inregistrez zi de zi. Exista optiunea aceasta si e OK pentru mine. Dar, daca vreti, pot merge pe locul ales de voi.<br><br>Buna,<br><br>Stii cumva unde am putea inregistra? Ai vreo varianta pt duminica unde putem suna maine sau cautam noi?', 'Buna,<br><br>Stii cumva unde am putea inregistra? Ai vreo varianta pt duminica unde putem suna maine sau cautam noi?', 'Buna Laura,<br><br>Daca e OK si pentru voi, prefer duminica. Pentru orice detalii, numarul meu este 0745 386 971.<br><br>Multam,<br>Mihail<br><br><br>Buna Mihail,<br><br>Spune-mi, te rog, ai timp duminica sau luni de inregistrare?&nbsp;<br><br>Vrem sa stim pentru ca dorim sa te alegem. Merci', 'Buna Razvan,<br><br>Nu prea am cum sa te aleg pe tine pentru spot, pentru ca pe profil nu ai fisiere de sunet ..<br>Casting-ul expira maine, deci daca nu vezi mesajul azi si nu completezi profilul azi, nu prea am ce face.<br><br>Merci mult', 'Buna,<br><br>Am vrea sa te folosim pentru spot-ul RCC, insa vrem sa stim daca ai avea timp duminica sau luni de inregistrare?<br><br>Merci mult!', 'Buna Mihail,<br><br>Spune-mi, te rog, ai timp duminica sau luni de inregistrare?&nbsp;<br><br>Vrem sa stim pentru ca dorim sa te alegem. Merci']);

		$faker        = Faker::create('ro_RO');
		$toBeInserted = [];
		foreach (range(1, 20) as $index) {
			$toBeInserted[] = [
				'message'      => array_rand($message),
				'is_read'      => rand(0, 1),
				'is_important' => rand(0, 1),
				'sender_id'    => rand(3, 8),
				'recipient_id' => rand(3, 8),
				'subject'      => $faker->paragraph(1),
				'status'       => rand(0, 1),
				'sent'         => rand(0, 1),
				'created_at'   => $faker->dateTimeThisMonth(),
				'updated_at'   => $faker->dateTimeThisMonth(),
			];
		}

		DB::table('messages')->insert(
			$toBeInserted
		);
	}

}