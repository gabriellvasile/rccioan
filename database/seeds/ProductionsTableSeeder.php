<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ProductionsTableSeeder extends Seeder
{
    /**
 	 * Run the PageTable seeds.
 	 *
 	 * @return void
 	 */
 	public function run()
 	{
 		$productions = ['1st AD', '3D & Animation', 'Animation', 'Ballet', 'Body parts modelling', 'Campaign', 'Clip music video', 'Commercial', 'Commercial print/on-camera', 'Commercials', 'Contemporary', 'Costumes & Art Department', 'DOP/Camera', 'Documentary', 'Editing & Color Grading', 'Events', 'Fashion', 'Fashion/Editorial', 'Fashion/Editorials', 'Festivals', 'Film (Feature)', 'Film (Short)', 'Film (Student)', 'Fitness Models', 'Interior Design/Art', 'Kids', 'Lighting Department', 'Make-up Artists & Hair Stylists', 'Michael Jackson', 'Music Video', 'Musical theater', 'Oriental dance', 'Other', 'PA', 'Popular dance', 'Portet', 'Radio', 'Reporters', 'Runway', 'Screenwriters/Script', 'Sound', 'Sport dance', 'Street dance', 'TV', 'TV Commercial', 'TV Directors', 'Theatre', 'Weddings/Christenings', 'Zorba'];

 		foreach ( $productions as $production ) {
 			DB::table('productions')->insert(
 				[	
 					'title' => $production,
 					'titlu' => $production,

 					'created_at' => Carbon::now(),
 					'updated_at' => Carbon::now()

 				]
 			);
 		}
 	}
}
