<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class LicenceUserTableSeeder extends Seeder {

	/**
	 * Run the PageTable seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$faker = Faker::create();
		$toBeInserted = [];
		foreach (range(1, 20) as $index) {
			$toBeInserted[] = [
				'licence_id'   => rand(1, 16),
				'user_id'    => rand(3, 35),
				'created_at' => $faker->dateTimeThisMonth(),
				'updated_at' => $faker->dateTimeThisMonth(),

			];
		}

		DB::table('licence_user')->insert(
			$toBeInserted
		);
	}

}