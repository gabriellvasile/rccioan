<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class LanguagesTableSeeder extends Seeder
{
    public function run()
    {
		DB::table('languages')->insert([
    		[
    			'name' => 'Romana',
    			'code' => 'ro',
    			'locale' => 'ro_RO.UTF-8',
    			'image' => 'ro.png',
    			'order' => 1,
    			'status' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
    		],
    		[
    			'name' => 'English',
    			'code' => 'en',
    			'locale' => 'en_US.UTF-8,en_US,en-gb,english',
    			'image' => 'en.png',
    			'order' => 2,
    			'status' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
    		]
    	]);
    }
}
