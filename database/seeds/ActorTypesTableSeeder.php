<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ActorTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
	{
		$actorTypes = ['actori' => 'Actori', 'actrite' => 'Actrite', 'studenti' => 'Studenti'];

		$toBeInserted = [];
		foreach ( $actorTypes as $key => $typeName ) {
			$toBeInserted[] = [
				'name'			=> $typeName,
				'slug'			=> $key,
				'created_at' => Carbon::now(),
				'updated_at' => Carbon::now()
			];
		}

		DB::table('actor_types')->insert(
			$toBeInserted
		);
	}
}
