<?php

use Illuminate\Database\Eloquent\Model;
use Faker\Factory as Faker;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ActorsTableSeeder extends Seeder {

	/**
	 * Run the PageTable seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$accent = array_flip(['Romana', 'Ardelean', 'Moldovean', 'Nu am', 'fara accent', 'Nu', 'Oltean']);
		$hair = array_flip(['S', 'M', 'L']);
		$languages = array_flip(['Engleza', 'Franceza', 'Engleza incepator, Franceza avansat si Spaniola mediu', 'Italiana']);
		$facial = array_flip(['fata mica, luminoasa, ten curat, fara imperfectiuni', 'fata rotunda, sprancene arcuite, ochi mari si expresivi, nas potrivit, gropite, buze semi-subtiri', 'normale/dragute', 'fata mica,expresiva, ochi migdalati', 'ochi mari, zambet larg, sprancene dese si bine definite', 'trasaturi orientale']);
		$dance = array_flip(['clasic, contemporan, latin, de societate, ori', 'Da. Membru in trupa de Street Dance Brothers ', 'Tango,dans modern,polka incepator', 'Latino,dansuri clasice, balet (nivel incepator)', 'notiuni balet clasic, dans spotiv, dans contemporan']);
		$instruments = array_flip(['Chitara clasica, chitara acustica, chitara electrica', 'Voce (Fosta concurenta X factor sezonul 2011)', 'Pian-foarte putin', 'nu', 'flaut , pian']);
		$sports = array_flip(['Patinaj artistic,parasutism,innot,volei,echitatie,alpinism', 'inot, snowboard, atletism, handbal, baschet, volei', 'tenis de masa, inot, ski, calarie, ciclism, volei', 'motociclism']);
		$institution = array_flip(['Ivana Chubbuck-Mandragora Movies Acting Studio', 'Universitatea Nationala de Arta Teatrala si Cinematografica Ion Luca Caragiale Bucuresti', 'UNATC I.L. Caragiale sectia Arta Actorului', 'Spiru Haret Bucuresti', 'Colegiul National de Arte', 'Mandragora Movies Film Academy']);
		$faculty = array_flip(['Bucuresti - Universitatea Hyperion - Facultatea de Arte', 'Bucuresti - Universitatea Nationala de Arta Teatrala si Cinematografica “I.L.Caragiale” (UNATC)', 'Facultatea de Informatica', 'Cluj -Universitatea Babes-Bolyai - Facultatea de Film si TV']);
		$domain = array_flip(['actorie', 'IT', 'filologie intensiv limba engleza', 'Cinema and Television', 'Facultatea de Teatru, Specializarea Papusi si Marionete']);


		$faker = Faker::create('ro_RO');
		foreach ( range(1,10) as $index ) {
			DB::table('actors')->insert(
				[	
					'user_id' => rand(1, 50),
					'ethnicity_id' => rand(1, 192),
					'eye_colour_id' => rand(1, 6),
					'hair_colour_id' => rand(1, 6),
					'height' => rand(120, 240),
					'weight' => rand(20, 300),
					'accent' => array_rand($accent),
					'hair_length' => array_rand($hair),
					'spoken_languages' => array_rand($languages),
					'facial_traits' => array_rand($facial),
					'voice' => rand(0,1),
					'dance' => array_rand($dance),
					'instruments' => array_rand($instruments),
					'sports' => array_rand($sports),
					'profesional_studies' => rand(0,1),
					'speciality_studies' => rand(0,1),
					'speciality_year' => rand(1980,2016),
					'speciality_institution' => array_rand($institution),
					'faculty' => rand(0,1),
					'faculty_student' => rand(0,1),
					'faculty_year' => rand(1980,2016),
					'faculty_institution' => array_rand($faculty),
					'faculty_domain' => array_rand($domain),
					'website' => $faker->url(),
					'info' => $faker->paragraph(10),
					'awards' => $faker->paragraph(10),
					'workshops' => $faker->paragraph(10),
					'production_types' => $faker->paragraph(10),
					'role_abroad' => rand(0,1),
					'actor_type_id' => rand(1,3),

					'created_at' => $faker->dateTimeThisMonth(),
					'updated_at' => $faker->dateTimeThisMonth()

				]
			);
		}
	}

}
