<?php

use Illuminate\Database\Eloquent\Model;
use Faker\Factory as Faker;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PagesTableSeeder extends Seeder {

	/**
	 * Run the PageTable seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$faker = Faker::create();

		$common = [
			'parent_id' => 0,
			'password' => '',
			'status' => 1,
			'order' => 0,
			'image' => '',
			'footer' => 0,
			'header' => 0,
			'published_at' => $faker->dateTimeThisMonth(),
			'created_at' => $faker->dateTimeThisMonth(),
			'updated_at' => $faker->dateTimeThisMonth()
		];
		DB::table('pages')->insert([
			$common,
			array_merge($common,
				[
					'footer' => '1',
				]
			),
			array_merge($common,
				[
					'footer' => '1',
				]
			),
			array_merge($common,
				[
					'footer' => '1',
				]
			),

			array_merge($common,
				[
					'header' => '1',
				]
			),
			array_merge($common,
				[
					'header' => '1',
				]
			),
			array_merge($common,
				[
					'header' => '1',
				]
			),
			array_merge($common,
				[
					'header' => '1',
				]
			),
			array_merge($common,
				[
					'header' => '1',
				]
			),
			array_merge($common,
				[
					'parent_id' => '6',
				]
			),
			array_merge($common,
				[
					'parent_id' => '6',
				]
			),
			array_merge($common,
				[
					'parent_id' => '6',
				]
			),
			array_merge($common,
				[
					'parent_id' => '6',
				]
			),
			array_merge($common,
				[
					'parent_id' => '6',
					'status' => 0,
				]
			),
		]);
	}

}