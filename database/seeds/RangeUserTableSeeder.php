<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class RangeUserTableSeeder extends Seeder {

	/**
	 * Run the PageTable seeds.
	 *
	 * @return void
	 */
	public function run() {
		$faker = Faker::create();

		$toBeInserted = [];
		foreach (range(1, 70) as $index) {
			$toBeInserted[] = [
				'range_id'   => rand(1, 11),
				'user_id'    => rand(3, 35),
				'created_at' => $faker->dateTimeThisMonth(),
				'updated_at' => $faker->dateTimeThisMonth(),

			];
		}

		DB::table('range_user')->insert(
			$toBeInserted
		);
	}

}