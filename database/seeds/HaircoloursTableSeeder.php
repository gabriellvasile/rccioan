<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class HaircoloursTableSeeder extends Seeder
{
    /**
	 * Run the PageTable seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$haircolours = ["Saten" => "Brown", "Roscat" => "Red", "Negru" => "Black", "Blond" => "Blond", "Alb" => "White", "Carunt" => "Gray"];

		$toBeInserted = [];
		foreach ( $haircolours as $culoare_par => $haircolour ) {
			$toBeInserted[] = [
				'nume' => $culoare_par,
				'name' => $haircolour,
				'created_at' => Carbon::now(),
				'updated_at' => Carbon::now()
			];
		}

		DB::table('hair_colours')->insert(
			$toBeInserted
		);
	}
}
