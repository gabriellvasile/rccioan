<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class RolesTableSeeder extends Seeder
{
    /**
	 * Run the PageTable seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$roles = [
			'admin'		 => 'Administrative user, has access to himself, all talents & employers',
			'superadmin' => 'Super-administrative, has access to all users',
			'talent'	 => 'One of the talents, has acces only to himself',
			'employer'	 => 'Employer, has access to himself and all talents'
		];
		$toBeInserted = [];
		foreach ( $roles as $name => $description ) {
			$toBeInserted[] = [
				'name'			=> $name,
				'description' 	=> $description,
				'created_at' 	=> Carbon::now(),
				'updated_at' 	=> Carbon::now()
			];
		}

		DB::table('roles')->insert(
			$toBeInserted
		);
	}
}