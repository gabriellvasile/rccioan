<?php

use Illuminate\Database\Eloquent\Model;
use Faker\Factory as Faker;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SlidersTableSeeder extends Seeder {

    /**
     * Run the PageTable seeds.
     *
     * @return void
     */
    public function run() {
        $toBeInserted = [];
        $faker = Faker::create('ro_RO');
        foreach (range(1, 2) as $index) {
            $toBeInserted[] = [
                'image'      => '/images/slider1.jpg',
                'title1_ro'  => 'Nu a fost niciodata mai usor sa te',
                'title2_ro'  => 'inscrii la un casting',
                'desc_ro'    => 'Inscrie-te si incepe-ti cariera acum!',
                'title1_en'  => 'It\'s never been so easy to',
                'title2_en'  => 'register to any casting',
                'desc_en'    => 'Register and start your career now!',

                'created_at' => $faker->dateTimeThisMonth(),
                'updated_at' => $faker->dateTimeThisMonth(),

            ];
        }
        DB::table('sliders')->insert(
            $toBeInserted
        );
    }

}