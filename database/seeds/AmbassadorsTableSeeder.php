<?php

use Illuminate\Database\Eloquent\Model;
use Faker\Factory as Faker;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AmbassadorsTableSeeder extends Seeder {

	/**
	 * Run the PageTable seeds.
	 *
	 * @return void
	 */
	public function run() {
		$faker        = Faker::create('ro_RO');
		$videos       =
			['https://www.youtube.com/watch?v=m_ZVJgM-bZI', 'https://www.youtube.com/watch?v=XbXr-h_oz9I', 'https://www.youtube.com/watch?v=iTkYC6-mqD0', 'https://www.youtube.com/watch?v=QHsdfeE2k5g', 'https://www.youtube.com/watch?v=pQ-i_M3dBnQ', 'https://vimeo.com/12053993', 'https://vimeo.com/117247495', 'https://vimeo.com/117790267'];
		$toBeInserted = [];
		foreach (range(1, 10) as $index) {
			$toBeInserted[] = [
				'name'        => $faker->firstName() . ' ' . $faker->lastName(),
				'video'       => $videos[ rand(0, 7) ],
				'quote'       => $faker->paragraph(1),
				'description' => $faker->paragraph(10),
				'image'       => $faker->imageUrl(640, 640, 'people', true, 'Faker'),

				'created_at'  => $faker->dateTimeThisMonth(),
				'updated_at'  => $faker->dateTimeThisMonth(),

			];
		}
		DB::table('ambassadors')->insert(
			$toBeInserted
		);
	}

}
