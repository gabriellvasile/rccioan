<?php

use Illuminate\Database\Eloquent\Model;
use Faker\Factory as Faker;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ImagesTableSeeder extends Seeder {

	/**
	 * Run the PageTable seeds.
	 *
	 * @return void
	 */
	public function run() {
		$toBeInserted = [];
		$faker        = Faker::create('ro_RO');
		foreach (range(1, 50) as $index) {
			$toBeInserted[] = [
				'user_id'    => rand(1, 50),
				'path'       => $faker->imageUrl(640,640, 'people', true, 'Faker'),
				'thumb'      => $faker->imageUrl(640,640, 'people', true, 'Faker'),

				'created_at' => $faker->dateTimeThisMonth(),
				'updated_at' => $faker->dateTimeThisMonth(),

			];
		}
		DB::table('images')->insert(
			$toBeInserted
		);
	}

}