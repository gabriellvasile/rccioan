<?php

use Illuminate\Database\Seeder;

class ProductionTypesTableSeeder extends Seeder
{
    /**
	 * Run the PageTable seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$types = ['Lungmetraj', 'Scurtmetraj', 'Filme de animatie', 'Reclame', 'Videoclipuri', 'Teatru', 'Teatru muzical', 'Lungmetraj (student)', 'Scurtmetraj (student)', 'Altele'];
		
		foreach ( $types as $name ) {
			DB::table('production_types')->insert(
				[	
					'name'			=> $name,
				]
			);
		}
	}
}

