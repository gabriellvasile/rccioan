<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class EyecoloursTableSeeder extends Seeder
{
    /**
	 * Run the PageTable seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$eyecolours = ["Albastri" => "Blue", "Verzi" => "Green", "Maro" => "Brown", "Caprui" => "Hazel", "Negri" => "Black", "Gri" => "Grey"];

		$toBeInserted = [];
		foreach ( $eyecolours as $culoare_ochi => $eyecolour ) {
			$toBeInserted[] = [
				'nume' => $culoare_ochi,
				'name' => $eyecolour,
				'created_at' => Carbon::now(),
				'updated_at' => Carbon::now()
			];
		}

		DB::table('eye_colours')->insert(
			$toBeInserted
		);
	}
}
