<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Rcc\Models\User;
use Rcc\Models\Actor;
use Rcc\Models\Employer;
use Rcc\Models\Lunar;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		Model::unguard();

		$postfix = 'TableSeeder';
		$seeders = [

			// Traits that will be referenced using foreign keys
			'Ethnicities',
			'Eyecolours',
			'Haircolours',
			'Licences',
			'Countries',
			'Roles',
			'ActorTypes',
			'EmployerTypes',
			'Languages',
			'Ranges',

			'Productions',
			'ProductionTypes',

			'Ambassadors',

			'Pages',
			'LanguagePage',
		];

		foreach ($seeders as $seeder) {
			$this->call($seeder . $postfix);
		}

		// Admin users
		factory(User::class, 'user_admin')->create();
		factory(User::class, 'user_superadmin')->create();

		// Specific Actor and Employer accounts
		factory(Actor::class, 'actor_user_actor')->create();
		factory(Actor::class, 'actrita_user_actor')->create();
		factory(Actor::class, 'student_user_actor')->create();
		factory(Employer::class, 'agency_user_employer')->create();
		factory(Employer::class, 'casa_user_employer')->create();
		factory(Employer::class, 'student_user_employer')->create();

		echo "Seeded: Accounts\n";

		// Random Actors
		factory(User::class, 'random_user_actor_actor', 10)->create()->each(function ($u) {
			$u->actor()->save(factory(Actor::class, 'actor_actor')->make());
		});
		factory(User::class, 'random_user_actor_actrita', 10)->create()->each(function ($u) {
			$u->actor()->save(factory(Actor::class, 'actor_actrita')->make());
		});
		factory(User::class, 'random_user_actor_student', 10)->create()->each(function ($u) {
			$u->actor()->save(factory(Actor::class, 'actor_student')->make());
		});
		// Random Unapproved Actors
		factory(User::class, 'random_unapproved_user_actor', 10)->create()->each(function ($u) {
			$u->actor()->save(factory(Actor::class, 'random_unapproved_actor')->make());
		});
		factory(Lunar::class, 'lunar', 30)->create();

		echo "Seeded: Actors\n";

		// Random Employers
		factory(User::class, 'random_user_employer', 10)->create()->each(function ($u) {
			$u->employer()->save(factory(Employer::class, 'employer_agency')->make());
		});
		factory(User::class, 'random_user_employer', 10)->create()->each(function ($u) {
			$u->employer()->save(factory(Employer::class, 'employer_casa')->make());
		});
		factory(User::class, 'random_user_employer', 10)->create()->each(function ($u) {
			$u->employer()->save(factory(Employer::class, 'employer_student')->make());
		});
		// Random Unapproved Employers
		factory(User::class, 'random_unapproved_user_employer', 10)->create()->each(function ($u) {
			$u->employer()->save(factory(Employer::class, 'random_unapproved_employer')->make());
		});

		echo "Seeded: Employers\n";

		$seeders = [
			'Jobs',
			'RangeUser',
			'LicenceUser',
			'Characters',
			'CharacterUser',
			'Sliders',
			'Images',
			'Messages',
		];

		foreach ($seeders as $seeder) {
			$this->call($seeder . $postfix);
		}

		Model::reguard();
	}

}
