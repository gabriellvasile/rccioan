<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class RangesTableSeeder extends Seeder {

	/**
	 * Run the PageTable seeds.
	 *
	 * @return void
	 */
	public function run() {
		$ranges =
			['18 - 25', '22 - 30', '28 - 35', '35 - 40', '35 - 45', '45 - 50', '45 - 55', '50 - 60', '55 - 65', '65 - 70', '70 - 85'];

		$toBeInserted = [];
		foreach ($ranges as $range) {
			$toBeInserted[] = [
				'name'       => $range,
				'created_at' => Carbon::now(),
				'updated_at' => Carbon::now(),
			];
		}

		DB::table('ranges')->insert(
			$toBeInserted
		);
	}
}
