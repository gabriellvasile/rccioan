<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class LicencesTableSeeder extends Seeder {

	/**
	 * Run the PageTable seeds.
	 *
	 * @return void
	 */
	public function run() {
		$licences = ['A', 'B', 'BE', 'C', 'CE', 'D', 'DE', 'Tr', 'Tb', 'Tv', 'A1', 'B1', 'C1', 'C1E', 'D1', 'D1E'];

		$toBeInserted = [];
		foreach ($licences as $licence) {
			$toBeInserted[] = [
				'name'       => $licence,
				'created_at' => Carbon::now(),
				'updated_at' => Carbon::now(),
			];
		}

		DB::table('licences')->insert(
			$toBeInserted
		);
	}
}