<?php

use Faker\Factory as Faker;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CharactersTableSeeder extends Seeder {

    /**
     * Run the PageTable seeds.
     *
     * @return void
     */
    public function run() {

        $title =
            array_flip(['Actor', 'Voice-over', 'Regizor', 'Personaj Principal', 'Copil - Scurtmetraj', 'Figuratie Scurtmetraj']);
        $extra_details =
            array_flip(['O voce placuta, dar mai ales convingatoare. Nu conteaza sexul.', 'Cautam familii reale: mama- tata  copil pentru filmarea unor spoturi publicitare pentru un produs de ingrijire a parului, Produsul este dezvaluit doar in ziua filmarii, dat fiind faptul ca este un proiect de tip testimonial si se vrea reactii reale. Mama trebuie sa aiba varsta cuprinsa intre 30-40 ani, sa aiba parul bogat, zambet frumos si sa nu mai fi aparut vreodata in reclame. Copilul ar trebui sa aiba intre 3 si 6 ani, sa nu fie rusinos si sa nu mai fi aparut vreodata in vreo reclama. Pentru tata e foarte important sa aiba par si sa fie carismatic. Pentru detalii, va rog sa trimiteti mail la adresa rionita@sagafilm.ro', 'Regizare videoclipuri muzicale', 'Cautam doi barbati pentru figuratie in scurtmetrajul "10 ore", care vor face parte dintr-un board meeting de 4 oameni: o femeie si trei barbati. Rolurile sunt minimale: nicio replica, doar prezenta. Aceste doua personaje, alaturi de inca doua - o femeie   inca un barbat, formeaza board-ul din sala de conferinte, care ii asculta prezentarea dl. Suciu, tatal Mariei. La finalul prezentarii, cei 3 barbati vor hotari daca investesc in proiectul dl Suciu sau nu. Toti 4 sunt oameni de afaceri, imbracati la costum. Vlad Suciu, tatal Mariei va fi jucat de un actor cunoscut in Romania, acest film  fiind o productie romano-britanica, care va participa la majoritatea festivalurilor mari din lume.']);
        $sex = array_flip(['M', 'F']);
        $faker = Faker::create('ro_RO');
        $toBeInserted = [];

        foreach (range(1, 60) as $index) {
            $toBeInserted[] = [
                'job_id'         => rand(1, 30),
                'range_id'       => rand(1, 11),
                'hair_colour_id' => rand(1, 6),
                'eye_colour_id'  => rand(1, 6),

                'title'          => array_rand($title),

                'sex'            => array_rand($sex),
                'from_height'    => rand(160, 175),
                'to_height'      => rand(165, 190),
                'extra_details'  => array_rand($extra_details),

                'created_at'     => $faker->dateTimeThisMonth(),
                'updated_at'     => $faker->dateTimeThisMonth(),
            ];
        }

        DB::table('characters')->insert(
            $toBeInserted
        );
    }

}