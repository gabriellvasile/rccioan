<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class EmployerTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
	{
		$employerTypes = ['Agentie de casting', 'Casa de productie', 'Student'];
		
		$toBeInserted = [];
		foreach ( $employerTypes as $typeName ) {
			$toBeInserted[] = [
				'name'			=> $typeName,
				'created_at' => Carbon::now(),
				'updated_at' => Carbon::now()
			];
		}

		DB::table('employer_types')->insert(
			$toBeInserted
		);
	}
}
