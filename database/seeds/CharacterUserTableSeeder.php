<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class CharacterUserTableSeeder extends Seeder {

	/**
	 * Run the PageTable seeds.
	 *
	 * @return void
	 */
	public function run() {
		$faker        = Faker::create();
		$toBeInserted = [];
		foreach (range(1, 30) as $index) {
			$toBeInserted[] = [
				'character_id'               => rand(1, 25),
				'user_id'              => rand(3, 5),
				'is_chosen'            => rand(0, 1),
				'is_shortlist'         => rand(0, 1),
				'is_seen'              => rand(0, 1),

				'date_applied'         => $faker->dateTimeThisMonth(),

				'is_accepted'          => rand(0, 1),
				'is_invite'            => rand(0, 1),
				'is_declined_employer' => rand(0, 1),
				'created_at'           => $faker->dateTimeThisMonth(),
				'updated_at'           => $faker->dateTimeThisMonth(),

			];
		}
		DB::table('character_user')->insert(
			$toBeInserted
		);
	}

}