<?php
use Rcc\Models\User;
use Rcc\Models\Actor;
use Rcc\Models\Employer;
use Rcc\Models\Lunar;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

// Random User
$factory->define(User::class, function ($faker) {
    $fname = $faker->firstName();
    $lname = $faker->lastName();
    return [
        'email' => $fname . '.' . $lname . '@' . $faker->freeEmailDomain(),
        'password' => bcrypt('123456'),
        'username'   => $fname . $lname . rand(1,50),

        'fname' => $fname,
        'lname' => $lname,
        'city' => $faker->city(),
        'mobile' => $faker->phoneNumber(),

        'language_id' => 1,
        'active' => 1,
        'approved' => 1,
        'complete' => 1,
        'thumb_id' => rand(1,50),

        'country_id' => 173, // Romania

        'logins' => 0,
        'last_login' => null,
        'birth_date' => $faker->date(),

        'created_at' => $faker->dateTimeThisDecade(),
        'updated_at' => $faker->dateTimeThisMonth()
    ];
});

// Admin user
$factory->defineAs(User::class, 'user_admin', function ($faker) use ($factory) {
    $user = $factory->raw(User::class);
    $admin = [
        'username' => 'admin',
        'fname' => 'admin',
        'lname' => 'admin',
        'email' => 'admin@rcc.ro',
        'role_id' => 1,
    ];
    return array_merge($user, $admin);
});
// Super admin user
$factory->defineAs(User::class, 'user_superadmin', function ($faker) use ($factory) {
    $user = $factory->raw(User::class);
    $superadmin = [
        'username' => 'superadmin',
        'fname' => 'superadmin',
        'lname' => 'superadmin',
        'email' => 'superadmin@rcc.ro',
        'role_id' => 2
    ];
    return array_merge($user, $superadmin);
});
// Talent user
$factory->defineAs(User::class, 'user_talent', function ($faker) use ($factory) {
    $user = $factory->raw(User::class);
    $talent = [
        'role_id' => 3
    ];
    return array_merge($user, $talent);
});
// Employer user
$factory->defineAs(User::class, 'user_employer', function ($faker) use ($factory) {
    $user = $factory->raw(User::class);
    $talent = [
        'role_id' => 4
    ];
    return array_merge($user, $talent);
});
// Actor user
$factory->defineAs(User::class, 'user_actor_actor', function ($faker) use ($factory) {
    $user = $factory->raw(User::class);
    $actor = [
        'username' => 'actor',
        'fname' => 'actor',
        'lname' => 'actor',
        'email' => 'actor@rcc.ro',
        'role_id' => 3,
        'sex'     => 'M'
    ];
    return array_merge($user, $actor);
});
// Actrita user
$factory->defineAs(User::class, 'user_actor_actrita', function ($faker) use ($factory) {
    $user = $factory->raw(User::class);
    $actor = [
        'username' => 'actrita',
        'fname' => 'actrita',
        'lname' => 'actrita',
        'email' => 'actrita@rcc.ro',
        'role_id' => 3,
        'sex'     => 'F'
    ];
    return array_merge($user, $actor);
});
// Student Actor user
$factory->defineAs(User::class, 'user_actor_student', function ($faker) use ($factory) {
    $user = $factory->raw(User::class);
    $actor = [
        'username' => 'student',
        'fname' => 'student',
        'lname' => 'student',
        'email' => 'student@rcc.ro',
        'role_id' => 3,
        'sex'     => array_rand(array_flip(['M', 'F']))
    ];
    return array_merge($user, $actor);
});
// Random Actor user
$factory->defineAs(User::class, 'random_user_actor_actor', function ($faker) use ($factory) {
    $user = $factory->raw(User::class);

    $fname = $faker->firstNameMale();
    $lname = $faker->lastName();
    $actor = [
        'username'   => $fname . $lname . rand(1,50),
        'role_id' => 3,
        'sex'     => 'M',
        'email' => $fname . '.' . $lname . '@' . $faker->freeEmailDomain(),
        'fname' => $fname,
        'lname' => $lname,
    ];
    return array_merge($user, $actor);
});
// Random Actrita user
$factory->defineAs(User::class, 'random_user_actor_actrita', function ($faker) use ($factory) {
    $user = $factory->raw(User::class);
    $fname = $faker->firstNameFemale();
    $lname = $faker->lastName();
    $actor = [
        'username'   => $fname . $lname . rand(1,50),
        'role_id' => 3,
        'sex'     => 'F',
        'email' => $fname . '.' . $lname . '@' . $faker->freeEmailDomain(),
        'fname' => $fname,
        'lname' => $lname,
    ];
    return array_merge($user, $actor);
});
// Random Student Actor user
$factory->defineAs(User::class, 'random_user_actor_student', function ($faker) use ($factory) {
    $user = $factory->raw(User::class);
    $actor = [
        'role_id' => 3,
        'sex'     => array_rand(array_flip(['M', 'F']))
    ];
    return array_merge($user, $actor);
});
// Random inactive user actor
$factory->defineAs(User::class, 'random_unapproved_user_actor', function ($faker) use ($factory) {
    $user = $factory->raw(User::class);
    $actor = [
        'role_id' => 3,
        'sex'     => array_rand(array_flip(['M', 'F'])),
        'approved' => 0,
    ];
    return array_merge($user, $actor);
});
// Agency user
$factory->defineAs(User::class, 'user_employer_agency', function ($faker) use ($factory) {
    $user = $factory->raw(User::class);
    $actor = [
        'username' => 'agency',
        'fname' => 'agency',
        'lname' => 'agency',
        'email' => 'agency@rcc.ro',
        'role_id' => 4,
    ];
    return array_merge($user, $actor);
});
// Casa user
$factory->defineAs(User::class, 'user_employer_casa', function ($faker) use ($factory) {
    $user = $factory->raw(User::class);
    $actor = [
        'username' => 'casa',
        'fname' => 'casa',
        'lname' => 'casa',
        'email' => 'casa@rcc.ro',
        'role_id' => 4,
    ];
    return array_merge($user, $actor);
});
// Student Employer user
$factory->defineAs(User::class, 'user_employer_student', function ($faker) use ($factory) {
    $user = $factory->raw(User::class);
    $actor = [
        'username' => 'studentemployer',
        'fname' => 'studentemployer',
        'lname' => 'studentemployer',
        'email' => 'studentemployer@rcc.ro',
        'role_id' => 4,
    ];
    return array_merge($user, $actor);
});
// Random user employer
$factory->defineAs(User::class, 'random_user_employer', function ($faker) use ($factory) {
    $user = $factory->raw(User::class);
    $actor = [
        'role_id' => 4,
    ];
    return array_merge($user, $actor);
});
// Random inactive user employer
$factory->defineAs(User::class, 'random_unapproved_user_employer', function ($faker) use ($factory) {
    $user = $factory->raw(User::class);
    $actor = [
        'role_id' => 4,
        'approved' => 0,
    ];
    return array_merge($user, $actor);
});


/***********************************/
/************** ACTORS *************/
/***********************************/

// Random actor
$factory->define(Actor::class, function ($faker) {

    $accent = array_flip(['Romana', 'Ardelean', 'Moldovean', 'Nu am', 'fara accent', 'Nu', 'Oltean']);
    $hair = array_flip(['S', 'M', 'L']);
    $languages = array_flip(['Engleza', 'Franceza', 'Engleza incepator, Franceza avansat si Spaniola mediu', 'Italiana']);
    $facial = array_flip(['fata mica, luminoasa, ten curat, fara imperfectiuni', 'fata rotunda, sprancene arcuite, ochi mari si expresivi, nas potrivit, gropite, buze semi-subtiri', 'normale/dragute', 'fata mica,expresiva, ochi migdalati', 'ochi mari, zambet larg, sprancene dese si bine definite', 'trasaturi orientale']);
    $dance = array_flip(['clasic, contemporan, latin, de societate, ori', 'Da. Membru in trupa de Street Dance Brothers ', 'Tango,dans modern,polka incepator', 'Latino,dansuri clasice, balet (nivel incepator)', 'notiuni balet clasic, dans spotiv, dans contemporan']);
    $instruments = array_flip(['Chitara clasica, chitara acustica, chitara electrica', 'Voce (Fosta concurenta X factor sezonul 2011)', 'Pian-foarte putin', 'nu', 'flaut , pian']);
    $sports = array_flip(['Patinaj artistic,parasutism,innot,volei,echitatie,alpinism', 'inot, snowboard, atletism, handbal, baschet, volei', 'tenis de masa, inot, ski, calarie, ciclism, volei', 'motociclism']);
    $institution = array_flip(['Ivana Chubbuck-Mandragora Movies Acting Studio', 'Universitatea Nationala de Arta Teatrala si Cinematografica Ion Luca Caragiale Bucuresti', 'UNATC I.L. Caragiale sectia Arta Actorului', 'Spiru Haret Bucuresti', 'Colegiul National de Arte', 'Mandragora Movies Film Academy']);
    $faculty = array_flip(['Bucuresti - Universitatea Hyperion - Facultatea de Arte', 'Bucuresti - Universitatea Nationala de Arta Teatrala si Cinematografica �I.L.Caragiale� (UNATC)', 'Facultatea de Informatica', 'Cluj -Universitatea Babes-Bolyai - Facultatea de Film si TV']);
    $domain = array_flip(['actorie', 'IT', 'filologie intensiv limba engleza', 'Cinema and Television', 'Facultatea de Teatru, Specializarea Papusi si Marionete']);

    return [
        'ethnicity_id' => rand(1, 192),
        'eye_colour_id' => rand(1, 6),
        'hair_colour_id' => rand(1, 6),
        'actor_type_id' => rand(1,3),

        'height' => rand(120, 240),
        'weight' => rand(20, 300),
        'accent' => array_rand($accent),
        'hair_length' => array_rand($hair),
        'spoken_languages' => array_rand($languages),
        'facial_traits' => array_rand($facial),
        'voice' => rand(0,1),
        'dance' => array_rand($dance),
        'instruments' => array_rand($instruments),
        'sports' => array_rand($sports),
        'profesional_studies' => rand(0,1),
        'speciality_studies' => rand(0,1),
        'speciality_year' => rand(1980,2016),
        'speciality_institution' => array_rand($institution),
        'faculty' => rand(0,1),
        'faculty_student' => rand(0,1),
        'faculty_year' => rand(1980,2016),
        'faculty_institution' => array_rand($faculty),
        'faculty_domain' => array_rand($domain),
        'website' => $faker->url(),
        'info' => $faker->paragraph(10),
        'awards' => $faker->paragraph(10),
        'workshops' => $faker->paragraph(10),
        'production_types' => $faker->paragraph(10),
        'role_abroad' => rand(0,1),

        'created_at' => $faker->dateTimeThisDecade(),
        'updated_at' => $faker->dateTimeThisMonth()
    ];
});

// Actor Actor (mascul)
$factory->defineAs(Actor::class, 'actor_actor', function ($faker) use ($factory) {
    $actor = $factory->raw(Actor::class);
    $mascul = [
        'actor_type_id' => 1,
    ];
    return array_merge($actor, $mascul);
});

// Actor Actrita
$factory->defineAs(Actor::class, 'actor_actrita', function ($faker) use ($factory) {
    $actor = $factory->raw(Actor::class);
    $actrita = [
        'actor_type_id' => 2,
    ];
    return array_merge($actor, $actrita);
});

// Actor Student
$factory->defineAs(Actor::class, 'actor_student', function ($faker) use ($factory) {
    $actor = $factory->raw(Actor::class);
    $student = [
        'actor_type_id' => 3,
    ];
    return array_merge($actor, $student);
});
// Random Unapproved Actor user
$factory->defineAs(Actor::class, 'random_unapproved_actor', function ($faker) use ($factory) {
    $actor = $factory->raw(Actor::class);
    $type = [
        'actor_type_id' => rand(1,3),
    ];
    return array_merge($actor, $type);
});

// Actor user
$factory->defineAs(Actor::class, 'actor_user_actor', function ($faker) use ($factory) {
    $actor = $factory->raw(Actor::class);
    $talent = [
        'user_id' => factory(User::class, 'user_actor_actor')->create()->id,
        'actor_type_id' => 1,
    ];
    return array_merge($actor, $talent);
});
$factory->defineAs(Actor::class, 'lunar_user_actor', function ($faker) use ($factory) {
    $actor = $factory->raw(Actor::class);
    $talent = [
        'user_id' => factory(User::class, 'user_talent')->create()->id,
    ];
    return array_merge($actor, $talent);
});

// Actrita user
$factory->defineAs(Actor::class, 'actrita_user_actor', function ($faker) use ($factory) {
    $actor = $factory->raw(Actor::class);
    $talent = [
        'user_id' => factory(User::class, 'user_actor_actrita')->create()->id,
        'actor_type_id' => 2,
    ];
    return array_merge($actor, $talent);
});

// Student user
$factory->defineAs(Actor::class, 'student_user_actor', function ($faker) use ($factory) {
    $actor = $factory->raw(Actor::class);
    $talent = [
        'user_id' => factory(User::class, 'user_actor_student')->create()->id,
        'actor_type_id' => 3,
    ];
    return array_merge($actor, $talent);
});

// lunar
$factory->define(Lunar::class, function ($faker) use ($factory) {
    $videos = ['https://www.youtube.com/watch?v=m_ZVJgM-bZI', 'https://www.youtube.com/watch?v=XbXr-h_oz9I', 'https://www.youtube.com/watch?v=iTkYC6-mqD0', 'https://www.youtube.com/watch?v=QHsdfeE2k5g', 'https://www.youtube.com/watch?v=pQ-i_M3dBnQ', 'https://vimeo.com/12053993', 'https://vimeo.com/117247495', 'https://vimeo.com/117790267'];
    return [
        'video' => $videos[rand(0, 7)],
        'description' => $faker->paragraph(10),
        'date' => $faker->dateTimeBetween($startDate = '-2 years', $endDate = 'now'),
        'created_at' => $faker->dateTimeThisDecade(),
        'updated_at' => $faker->dateTimeThisMonth(),
        'name'  => $faker->firstName() . ' ' . $faker->lastName()
    ];
});
// Lunar Actor
$factory->defineAs(Lunar::class, 'lunar', function ($faker) use ($factory) {
    $lunar = $factory->raw(Lunar::class);
    $actor = [
        'actor_id' => factory(Actor::class, 'lunar_user_actor')->create()->id,
    ];
    return array_merge($lunar, $actor);
});

/**************************************/
/************** EMPLOYERS *************/
/**************************************/

// Random Employer
$factory->define(Employer::class, function ($faker) {
    return [
        'company' => $faker->sentence(3),
        'address' => $faker->address,
        'post_code' => $faker->postcode(),
        'website' => $faker->domainName(),
        'info' => $faker->sentence(15),
        'logo_id' => null,
        'employer_type_id' => rand(1,3),

        'created_at' => $faker->dateTimeThisDecade(),
        'updated_at' => $faker->dateTimeThisMonth()
    ];
});

// Employer Agentie de casting
$factory->defineAs(Employer::class, 'employer_agency', function ($faker) use ($factory) {
    $employer = $factory->raw(Employer::class);
    $agency = [
        'employer_type_id' => 1,
    ];
    return array_merge($employer, $agency);
});

// Employer Casa de productie
$factory->defineAs(Employer::class, 'employer_casa', function ($faker) use ($factory) {
    $employer = $factory->raw(Employer::class);
    $casa = [
        'employer_type_id' => 2,
    ];
    return array_merge($employer, $casa);
});

// Employer Student
$factory->defineAs(Employer::class, 'employer_student', function ($faker) use ($factory) {
    $employer = $factory->raw(Employer::class);
    $student = [
        'employer_type_id' => 3,
    ];
    return array_merge($employer, $student);
});

// Random Unapproved Employer user
$factory->defineAs(Employer::class, 'random_unapproved_employer', function ($faker) use ($factory) {
    $employer = $factory->raw(Employer::class);
    $student = [
        'employer_type_id' => rand(1,3),
    ];
    return array_merge($employer, $student);
});


// Agency user
$factory->defineAs(Employer::class, 'agency_user_employer', function ($faker) use ($factory) {
    $employer = $factory->raw(Employer::class);
    $agency = [
        'user_id' => factory(User::class, 'user_employer_agency')->create()->id,
        'employer_type_id' => 1,
    ];
    return array_merge($employer, $agency);
});

// Casa user
$factory->defineAs(Employer::class, 'casa_user_employer', function ($faker) use ($factory) {
    $employer = $factory->raw(Employer::class);
    $casa = [
        'user_id' => factory(User::class, 'user_employer_casa')->create()->id,
        'employer_type_id' => 2,
    ];
    return array_merge($employer, $casa);
});

// Student Employer user
$factory->defineAs(Employer::class, 'student_user_employer', function ($faker) use ($factory) {
    $employer = $factory->raw(Employer::class);
    $student = [
        'user_id' => factory(User::class, 'user_employer_student')->create()->id,
        'employer_type_id' => 3,
    ];
    return array_merge($employer, $student);
});